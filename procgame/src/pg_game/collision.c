#include "procgame.h"

/******************************************/
/*  Basic collision functions             */
/******************************************/
/*  (collider, collider) -> collision
    The types of the colliders passed as arguments are in the same order as
    the types in the function name. */

typedef void (*collide_func_t)(struct pg_collision*, struct pg_collider*, struct pg_collider*);
#define COLLIDE_FUNC(NAME) \
    static void NAME(struct pg_collision* out, struct pg_collider* coll_a, struct pg_collider* coll_b)

typedef int (*collide_check_func_t)(struct pg_collider*, struct pg_collider*);
#define COLLIDE_CHECK_FUNC(NAME) \
    static int NAME(struct pg_collider* coll_a, struct pg_collider* coll_b)



/****************************/
/*  SPHERE <-> SPHERE       */

COLLIDE_FUNC(collide_sphere_sphere)
{
    sphere sph_a = SPHERE(vec3_add(coll_a->pos, coll_a->sph.p), coll_a->sph.r);
    sphere sph_b = SPHERE(vec3_add(coll_b->pos, coll_b->sph.p), coll_b->sph.r);
    vec3 sep = sep_axis_sphere_sphere(&sph_a, &sph_b);
    float sep_len2 = vec3_len2(sep);
    *out = (struct pg_collision){ .did_collide = (sep_len2 > FLT_EPSILON),
        .sep_axis = sep, .sep_len2 = sep_len2 };
}

COLLIDE_CHECK_FUNC(check_sphere_sphere)
{
    vec3 sph_a = vec3_add(coll_a->pos, coll_a->sph.p);
    vec3 sph_b = vec3_add(coll_b->pos, coll_b->sph.p);
    float dist = vec3_dist2(sph_a, sph_b);
    float radius_total = coll_a->sph.r + coll_b->sph.r;
    radius_total *= radius_total;
    if(dist > radius_total) return 1;
    else return 0;
}




/************************/
/*  SPHERE <-> AABB     */

COLLIDE_FUNC(collide_sphere_aabb)
{
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    sphere sph_a = SPHERE(sph_pos, coll_a->sph.r);
    vec3 sep = sep_axis_sphere_aabb3D(&sph_a, &coll_b->box);
    float sep_len2 = vec3_len2(sep);
    *out = (struct pg_collision){ .did_collide = (sep_len2 > FLT_EPSILON),
        .sep_axis = sep, .sep_len2 = sep_len2 };
}

COLLIDE_CHECK_FUNC(check_sphere_aabb)
{
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    vec3 closest_point = closestpt_point_aabb3D(sph_pos, &coll_b->box);
    float dist2 = vec3_dist2(closest_point, sph_pos);
    if(dist2 > coll_a->sph.r * coll_a->sph.r) return 0;
    else return 1;
}

COLLIDE_FUNC(collide_aabb_sphere) { collide_sphere_aabb(out, coll_b, coll_a); }
COLLIDE_CHECK_FUNC(check_aabb_sphere) { return check_sphere_aabb(coll_b, coll_a); }




/****************************/
/*  SPHERE <-> OBB          */

COLLIDE_FUNC(collide_sphere_obb)
{
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    sphere sph_a = SPHERE(sph_pos, coll_a->sph.r);
    vec3 sep = sep_axis_sphere_obb3D(&sph_a, &coll_b->obb);
    float sep_len2 = vec3_len2(sep);
    *out = (struct pg_collision){ .did_collide = (sep_len2 > FLT_EPSILON),
        .sep_axis = sep, .sep_len2 = sep_len2 };
}

COLLIDE_CHECK_FUNC(check_sphere_obb)
{
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    vec3 closest_point = closestpt_point_obb3D(sph_pos, &coll_b->obb);
    float dist2 = vec3_dist2(closest_point, sph_pos);
    if(dist2 > coll_a->sph.r * coll_a->sph.r) return 0;
    else return 1;
}

COLLIDE_FUNC(collide_obb_sphere) { collide_sphere_obb(out, coll_b, coll_a); }
COLLIDE_CHECK_FUNC(check_obb_sphere) { return check_sphere_obb(coll_b, coll_a); }




/****************************/
/*  SPHERE <-> HEIGHTMAP    */

COLLIDE_FUNC(collide_sphere_heightmap)
{
    const int cells_x = coll_b->hmap.map_size.x;
    const float cell_sz = coll_b->hmap.cell_size;
    const float* hmap = coll_b->hmap.map;
    /*  Transform the sphere into the heightmap's frame of reference (in x/y axes)  */
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    //sph_pos = vec3_add(sph_pos, vec3(coll_b->hmap.cell_size, coll_b->hmap.cell_size, 1));
    sphere sph = SPHERE(sph_pos, coll_a->sph.r);
    vec3 sphere_i = vec3_div(sph.p, vec3(coll_b->hmap.cell_size, coll_b->hmap.cell_size, 1));
    float radius_i = sph.r / cell_sz;
    float min_height = sph.p.z - sph.r;
    /*  bound0 and bound1 are the index range where heightmap points have to be checked */
    ivec2 bound0 = ivec2(sphere_i.x - radius_i, sphere_i.y - radius_i);
    ivec2 bound1 = ivec2(sphere_i.x + radius_i + 1, sphere_i.y + radius_i + 1);
    bound0 = ivec2_clamp(bound0, ivec2(1), ivec2_sub(coll_b->hmap.map_size, ivec2_all(2)));
    bound1 = ivec2_clamp(bound1, ivec2(1), ivec2_sub(coll_b->hmap.map_size, ivec2_all(2)));
    /*  sep and sep_len2 hold the deepest collision found so far */
    vec3 sep = vec3(0);
    float sep_len2 = 0;
    /*  Iterate through all the cells which the sphere_tx overlaps in x/y axes */
    ivec2 iter;
    for(iter.x = bound0.x; iter.x < bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y < bound1.y; ++iter.y) {
        /*  Get the height values at the four points of this cell   */
        float h[4] = {
            hmap[iter.x + iter.y * cells_x],
            hmap[(iter.x+1) + iter.y * cells_x],
            hmap[iter.x + (iter.y+1) * cells_x],
            hmap[(iter.x+1) + (iter.y+1) * cells_x] };
        if(h[0] < 0 || h[1] < 0 || h[2] < 0 || h[3] < 0) continue;
        if(h[0] < min_height && h[1] < min_height
        && h[2] < min_height && h[3] < min_height) continue;
        /*  Calculate four vertices */
        vec3 tri[4] = {
            vec3((iter.x) * cell_sz, (iter.y) * cell_sz, h[0]),
            vec3((iter.x+1) * cell_sz, (iter.y) * cell_sz, h[1]),
            vec3((iter.x) * cell_sz, (iter.y+1) * cell_sz, h[2]),
            vec3((iter.x+1) * cell_sz, (iter.y+1) * cell_sz, h[3]), };
        /*  Get separating axis for one triangle    */
        vec3 sep_tmp = sep_axis_sphere_triangle(&sph, &TRIANGLE(tri[0], tri[1], tri[2]));
        float len_tmp = vec3_len2(sep_tmp);
        if(len_tmp > sep_len2) {
            sep = sep_tmp;
            sep_len2 = len_tmp;
        }
        /*  Separating axis for the other triangle  */
        sep_tmp = sep_axis_sphere_triangle(&sph, &TRIANGLE(tri[1], tri[2], tri[3]));
        len_tmp = vec3_len2(sep_tmp);
        if(len_tmp > sep_len2) {
            sep = sep_tmp;
            sep_len2 = len_tmp;
        }
    }
    /*  Transform separating axis back to the sphere's frame    */
    /*  Return the collision    */
    *out = (struct pg_collision){ .did_collide = (sep_len2 > FLT_EPSILON),
        .sep_axis = sep, .sep_len2 = sep_len2 };
}

COLLIDE_CHECK_FUNC(check_sphere_heightmap)
{
    const int cells_x = coll_b->hmap.map_size.x;
    const float cell_sz = coll_b->hmap.cell_size;
    const float* hmap = coll_b->hmap.map;
    /*  Transform the sphere into the heightmap's frame of reference (in x/y axes)  */
    vec3 sph_pos = vec3_add(vec3_sub(coll_a->pos, coll_b->pos), coll_a->sph.p);
    sphere sph = SPHERE(sph_pos, coll_a->sph.r);
    vec3 sphere_i = vec3_div(sph.p, vec3(coll_b->hmap.cell_size, coll_b->hmap.cell_size, 1));
    float radius_i = sph.r / cell_sz;
    float min_height = sph.p.z - sph.r;
    /*  bound0 and bound1 are the index range where heightmap points have to be checked */
    ivec2 bound0 = ivec2(sphere_i.x - radius_i, sphere_i.y - radius_i);
    ivec2 bound1 = ivec2(sphere_i.x + radius_i + 1, sphere_i.y + radius_i + 1);
    bound0 = ivec2_clamp(bound0, ivec2(0), ivec2_sub(coll_b->hmap.map_size, ivec2_all(1)));
    bound1 = ivec2_clamp(bound1, ivec2(0), ivec2_sub(coll_b->hmap.map_size, ivec2_all(1)));
    /*  Iterate through all the cells which the sphere_tx overlaps in x/y axes */
    ivec2 iter;
    for(iter.x = bound0.x; iter.x < bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y < bound1.y; ++iter.y) {
        /*  Get the height values at the four points of this cell   */
        float h[4] = {
            hmap[iter.x + iter.y * cells_x],
            hmap[(iter.x+1) + iter.y * cells_x],
            hmap[iter.x + (iter.y+1) * cells_x],
            hmap[(iter.x+1) + (iter.y+1) * cells_x] };
        if(h[0] < 0 || h[1] < 0 || h[2] < 0 || h[3] < 0) continue;
        if(h[0] < min_height && h[1] < min_height
        && h[2] < min_height && h[3] < min_height) continue;
        /*  Calculate four vertices */
        vec3 tri[4] = {
            vec3((iter.x) * cell_sz, (iter.y) * cell_sz, h[0]),
            vec3((iter.x+1) * cell_sz, (iter.y) * cell_sz, h[1]),
            vec3((iter.x) * cell_sz, (iter.y+1) * cell_sz, h[2]),
            vec3((iter.x+1) * cell_sz, (iter.y+1) * cell_sz, h[3]), };
        /*  Get separating axis for one triangle    */
        vec3 sep_tmp = sep_axis_sphere_triangle(&sph, &TRIANGLE(tri[0], tri[1], tri[2]));
        if(vec3_len2(sep_tmp) > FLT_EPSILON) return 1;
        /*  Separating axis for the other triangle  */
        sep_tmp = sep_axis_sphere_triangle(&sph, &TRIANGLE(tri[1], tri[2], tri[3]));
        if(vec3_len2(sep_tmp) > FLT_EPSILON) return 1;
    }
    return 0;
}

COLLIDE_FUNC(collide_heightmap_sphere) { collide_sphere_heightmap(out, coll_b, coll_a); }
COLLIDE_CHECK_FUNC(check_heightmap_sphere) { return check_sphere_heightmap(coll_b, coll_a); }




/********************/
/*  RAY CASTING     */
/********************/

typedef float (*collide_ray_func_t)(struct pg_collider*, vec3, vec3, float max_len);
#define COLLIDE_RAY_FUNC(NAME) \
    static float NAME(struct pg_collider* coll_a, vec3 src, vec3 dir, float max_len)

COLLIDE_RAY_FUNC(ray_sphere)
{
    sphere sph = SPHERE(vec3_add(coll_a->pos, coll_a->sph.p), coll_a->sph.r);
    float len = raycast_sphere(&RAY3D(src, dir), &sph);
    return len <= max_len ? len : -1;
}

COLLIDE_RAY_FUNC(ray_aabb)
{
    src = vec3_sub(src, coll_a->pos);
    float len = raycast_aabb3D(&RAY3D(src,dir), &coll_a->box);
    return len <= max_len ? len : -1;
}

COLLIDE_RAY_FUNC(ray_obb)
{
    obb3D obb = coll_a->obb;
    src = vec3_sub(src, coll_a->pos);
    float len = raycast_obb3D(&RAY3D(src,dir), &obb);
    return len <= max_len ? len : -1;
}

COLLIDE_RAY_FUNC(ray_heightmap)
{
    vec3 start3 = vec3_sub(src, coll_a->pos);
    vec3 end3 = vec3_add(start3, vec3_scale(dir, max_len));
    vec2 start = vec2(VEC_XY(start3));
    vec2 end = vec2(VEC_XY(end3));
    vec2 grid_dims = vec2(VEC_XY(coll_a->hmap.map_size));
    //grid_dims = vec2_sub(grid_dims, vec2_all(3));
    float cell_size = coll_a->hmap.cell_size;
    aabb2D bound = AABB2D(vec2(0), vec2_mul(vec2_all(cell_size), vec2_sub(grid_dims, vec2_all(1))));
    struct raycast_grid2D_state rc;
    if(!start_raycast_grid2D(start, end, bound, vec2_all(cell_size), &rc))
        return -1;
    /*  Convenience variables for the raycast loop  */
    int cells_x = coll_a->hmap.map_size.x;
    float* hmap = coll_a->hmap.map;
    ray3D src_ray = RAY3D(start3, dir);
    /*  Do The Thing    */
    do {
        /*  Get the height values at the four points of this cell   */
        float h[4] = {
            hmap[rc.iter.x + rc.iter.y * cells_x],
            hmap[(rc.iter.x+1) + rc.iter.y * cells_x],
            hmap[rc.iter.x + (rc.iter.y+1) * cells_x],
            hmap[(rc.iter.x+1) + (rc.iter.y+1) * cells_x] };
        if(h[0] < 0 || h[1] < 0 || h[2] < 0 || h[3] < 0) continue;
        /*  Calculate four vertices */
        vec3 tri[4] = {
            vec3((rc.iter.x) * cell_size, (rc.iter.y) * cell_size, h[0]),
            vec3((rc.iter.x+1) * cell_size, (rc.iter.y) * cell_size, h[1]),
            vec3((rc.iter.x) * cell_size, (rc.iter.y+1) * cell_size, h[2]),
            vec3((rc.iter.x+1) * cell_size, (rc.iter.y+1) * cell_size, h[3]), };
        /*  Check for ray hits on both triangles    */
        float ray_len = raycast_triangle(&src_ray, &TRIANGLE(tri[0], tri[1], tri[2]));
        if(ray_len >= 0) return ray_len;
        ray_len = raycast_triangle(&src_ray, &TRIANGLE(tri[2], tri[1], tri[3]));
        if(ray_len >= 0) return ray_len;
    } while(step_raycast_grid2D(&rc));
    return -1;
}




/********************************/
/*  Function dispatch table     */
/********************************/

collide_func_t COLLIDE_DISPATCH[PG_NUM_COLLIDER_TYPES][PG_NUM_COLLIDER_TYPES] = {
    [PG_COLLIDER_SPHERE] = {
        [PG_COLLIDER_SPHERE] = collide_sphere_sphere,
        [PG_COLLIDER_AABB] = collide_sphere_aabb,
        [PG_COLLIDER_OBB] = collide_sphere_obb,
        [PG_COLLIDER_HEIGHTMAP] = collide_sphere_heightmap, },
    [PG_COLLIDER_AABB] = {
        [PG_COLLIDER_SPHERE] = collide_aabb_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL },
    [PG_COLLIDER_OBB] = {
        [PG_COLLIDER_SPHERE] = collide_obb_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL },
    [PG_COLLIDER_HEIGHTMAP] = {
        [PG_COLLIDER_SPHERE] = collide_heightmap_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL, },
};

collide_check_func_t COLLIDE_CHECK_DISPATCH[PG_NUM_COLLIDER_TYPES][PG_NUM_COLLIDER_TYPES] = {
    [PG_COLLIDER_SPHERE] = {
        [PG_COLLIDER_SPHERE] = check_sphere_sphere,
        [PG_COLLIDER_AABB] = check_sphere_aabb,
        [PG_COLLIDER_OBB] = check_sphere_obb,
        [PG_COLLIDER_HEIGHTMAP] = check_sphere_heightmap, },
    [PG_COLLIDER_AABB] = {
        [PG_COLLIDER_SPHERE] = check_aabb_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL },
    [PG_COLLIDER_OBB] = {
        [PG_COLLIDER_SPHERE] = check_obb_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL },
    [PG_COLLIDER_HEIGHTMAP] = {
        [PG_COLLIDER_SPHERE] = check_heightmap_sphere,
        [PG_COLLIDER_AABB] = NULL,
        [PG_COLLIDER_OBB] = NULL,
        [PG_COLLIDER_HEIGHTMAP] = NULL, },
};

collide_ray_func_t COLLIDE_RAY_DISPATCH[PG_NUM_COLLIDER_TYPES] = {
    [PG_NO_COLLIDER] = NULL,
    [PG_COLLIDER_SPHERE] = ray_sphere,
    [PG_COLLIDER_AABB] = ray_aabb,
    [PG_COLLIDER_OBB] = ray_obb,
    [PG_COLLIDER_HEIGHTMAP] = ray_heightmap,
};

const char* PG_COLLIDER_NAME[PG_NUM_COLLIDER_TYPES] = {
    [PG_NO_COLLIDER] = "NullCollider",
    [PG_COLLIDER_SPHERE] = "Sphere",
    [PG_COLLIDER_AABB] = "AABB",
    [PG_COLLIDER_OBB] = "OBB",
    [PG_COLLIDER_HEIGHTMAP] = "Heightmap",
};



/********************************************/
/*  Collision detection and response code   */
/********************************************/

vec3 pg_collider_sep_axis(struct pg_collider* c0, struct pg_collider* c1)
{
    struct pg_collision result;
    /*  Dispatch to the correct function with correct argument order    */
    collide_func_t func = COLLIDE_DISPATCH[c0->type][c1->type];
    if(!func) return vec3(0);
    func(&result, c0, c1);
    if(result.did_collide) return result.sep_axis;
    else return vec3(0);
}

/*  Calculate a collision between any two colliders */
void pg_collision_calculate(struct pg_collision* out,
                            struct pg_collider* c0, struct pg_collider* c1)
{
    if(c0->flags & c1->flags & PG_COLLIDER_STATIC) return;
    /*  Dispatch to the correct function with correct argument order    */
    collide_func_t func = COLLIDE_DISPATCH[c0->type][c1->type];
    if(!func) return;
    func(out, c0, c1);
    if(!out->did_collide) return;
    /*  Calculate mass differential; static = infinite mass */
    if((c0->mass == -1 && !(c1->flags & PG_COLLIDER_STATIC))
    || (c1->mass == -1 && !(c0->flags & PG_COLLIDER_STATIC))
    || (c0->mass == -1 && c1->mass == -1)) {
        out->did_collide = 0;
        out->impulse[0] = 0;
        out->impulse[1] = 0;
        return;
    }
    int any_static = 0;
    if(c0->flags & PG_COLLIDER_STATIC) {
        out->impulse[0] = 0.0f;
        out->impulse[1] = 1.0f;
        any_static = 1;
    } else if(c1->flags & PG_COLLIDER_STATIC) {
        out->impulse[0] = 1.0f;
        out->impulse[1] = 0.0f;
        any_static = 1;
    } else {
        float total_mass = c0->mass + c1->mass;
        if(total_mass < -FLT_EPSILON) {
            out->did_collide = 0;
            out->impulse[0] = 0;
            out->impulse[1] = 0;
            return;
        } else if(total_mass < FLT_EPSILON) {
            out->impulse[0] = 1;
            out->impulse[1] = -1;
        } else {
            out->impulse[0] = (c1->mass / total_mass);
            out->impulse[1] = -(c0->mass / total_mass);
        }
    }
    /*  Apply collision to both entities    */
    if(!any_static && ((c0->flags | c1->flags) & PG_COLLIDER_SOFT)) {
        /*  "Soft" collision ie. just push each other away gently   */
        if(out->impulse[0]) {
            c0->vel_impulse = vec3_add(c0->vel_impulse, vec3_tolen(out->sep_axis, out->impulse[0] * 0.01));
        }
        if(out->impulse[1]) {
            c1->vel_impulse = vec3_add(c1->vel_impulse, vec3_tolen(out->sep_axis, out->impulse[1] * 0.01));
        }
    } else {
        #if 0
        /*  "Hard" collision ie. snap to corrected positions    */
        if(out->impulse[0] && out->sep_len2 > c0->push_len2) {
            c0->push = vec3_scale(out->sep_axis, out->impulse[0]);
            c0->push_len2 = out->sep_len2;
        }
        if(out->impulse[1] && out->sep_len2 > c1->push_len2) {
            c1->push = vec3_scale(out->sep_axis, out->impulse[1]);
            c1->push_len2 = out->sep_len2;
        }
        #else
        if(out->impulse[0] && out->sep_len2 > FLT_EPSILON) {
            if(c0->push_len2 > FLT_EPSILON) {
                c0->push = vec3_tdiv(vec3_add(vec3_scale(out->sep_axis, out->impulse[0]), c0->push), 2);
            } else {
                c0->push = vec3_scale(out->sep_axis, out->impulse[0]);
            }
            c0->push_len2 = vec3_len2(c0->push);
        }
        if(out->impulse[1] && out->sep_len2 > FLT_EPSILON) {
            if(c1->push_len2 > FLT_EPSILON) {
                c1->push = vec3_tdiv(vec3_add(vec3_scale(out->sep_axis, out->impulse[1]), c1->push), 2);
            } else {
                c1->push = vec3_scale(out->sep_axis, out->impulse[1]);
            }
            c1->push_len2 = vec3_len2(c1->push);
        }
        #endif
    }
}

float pg_collider_raycast(struct pg_collider* c0, vec3 src, vec3 dir, float max_len)
{
    collide_ray_func_t func = COLLIDE_RAY_DISPATCH[c0->type];
    if(!func) return -1;
    return func(c0, src, dir, max_len);
}

aabb3D pg_collider_get_bound(struct pg_collider* c0)
{
    if(c0->type == PG_COLLIDER_SPHERE) {
        aabb3D box;
        aabb3D_from_sphere(&box, &c0->sph);
        return box;
    } else if(c0->type == PG_COLLIDER_AABB) {
        return c0->box;
    } else if(c0->type == PG_COLLIDER_OBB) {
        aabb3D box;
        aabb3D_from_obb3D(&box, &c0->obb);
        return box;
    } else if(c0->type == PG_COLLIDER_HEIGHTMAP) {
        return AABB3D(
            vec3(0,0,0),
            vec3(c0->hmap.cell_size * (c0->hmap.map_size.x - 1),
                 c0->hmap.cell_size * (c0->hmap.map_size.y - 1), 256) );
    }
    return AABB3D( vec3(0), vec3(0) );
}

/*  Finish colliding before it can respond  */
int pg_collider_finish(struct pg_collider* coll)
{
    if(vec3_len2(coll->vel_impulse) < FLT_EPSILON && coll->push_len2 < FLT_EPSILON) return 0;
    if(coll->push_len2 > FLT_EPSILON) {
        coll->push_norm = vec3_norm(coll->push);
        coll->push_len = sqrt(coll->push_len2);
    }
    return 1;
}

/*  Reset to begin a new set of collisions  */
void pg_collider_reset(struct pg_collider* coll)
{
    coll->push_norm = vec3(0);
    coll->push = vec3(0);
    coll->vel_impulse = vec3(0);
    coll->push_len2 = 0;
    coll->push_len = 0;
}

/*  Collision reaction functions    */
void pg_collider_respond(struct pg_collider* coll, float k)
{
    if(coll->push_len2 > FLT_EPSILON) {
        coll->pos = vec3_add(coll->pos, coll->push);
        vec3 response = vec3_scale(coll->push_norm, k * -vec3_dot(coll->vel, coll->push_norm));
        coll->vel = vec3_add(coll->vel, response);
    }
    coll->vel = vec3_add(coll->vel, vec3_scale(coll->vel_impulse, k));
}

void pg_collider_respond_walk(struct pg_collider* coll)
{
    if(coll->push_len2 < FLT_EPSILON || coll->push_norm.z < 0.5) {
        pg_collider_respond(coll, 1);
        return;
    }
    coll->pos.z += coll->push.z;
    if(vec2_len2(vec2(VEC_XY(coll->vel))) < FLT_EPSILON) {
        coll->vel.z = 0;
        return;
    }
    float d = vec3_dot(coll->vel, coll->push_norm);
    vec3 response = vec3_scale(coll->push_norm, d);
    vec3 newvel = vec3_sub(coll->vel, response);
    coll->vel.z = newvel.z;
    coll->vel = vec3_add(coll->vel, vec3_scale(coll->vel_impulse, 1));
}
