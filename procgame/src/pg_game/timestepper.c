#include "procgame.h"

void pg_timestepper_init(struct pg_timestepper* ts, double steps_per_second)
{
    *ts = (struct pg_timestepper) {
        .steps_per_second = steps_per_second,
        .step_length = 1.0 / steps_per_second,
        .time_speed = 1.0,
    };
}

int pg_timestepper_step(struct pg_timestepper* ts, double new_time)
{
    /*  Manage time in seconds, first   */
    double time_elapsed = new_time - ts->time_secs_real;
    ts->time_secs_real = new_time;
    time_elapsed *= ts->time_speed;
    ts->time_secs += time_elapsed;
    /*  Manage time in steps    */
    double steps_elapsed = time_elapsed / ts->step_length;
    ts->time_steps += steps_elapsed;
    /*  "Accumulate" time until enough time for another discrete step has passed    */
    double steps_acc = ts->time_steps - (double)ts->steps_done;
    int steps = floor(steps_acc);
    ts->steps_done += steps;
    return steps;
}

double pg_timestepper_deltatime(struct pg_timestepper* ts, double current_time)
{
    double time_elapsed = current_time - ts->time_secs_real;
    time_elapsed *= ts->time_speed;
    double steps_elapsed = time_elapsed / ts->step_length;
    double steps_acc = (ts->time_steps + steps_elapsed) - (double)ts->steps_done;
    return steps_acc;
}
