#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "procgame.h"

#define p_transform(in, phase, freq) vec4_add(vec4_mul(in, freq), phase)

/*  TODO: Make this non-recursive   */
static float pg_wave_sample_array(struct pg_wave* wave, int* end_idx, int n,
                                  unsigned d, vec4 p)
{
    vec4 p_ = p;
    //printf("%f %f\n", p.x, p.y);
    float scale = 1, add = 0;
    float s = 0;
    int i;
    int sub_len = 1;
    struct pg_wave* iter = wave;
    for(i = 0; i < n; i += sub_len, iter = wave + i) {
        vec4 iter_p = vec4(0);
        vec4 freq = vec4(iter->frequency[0], iter->frequency[1],
                         iter->frequency[2], iter->frequency[3]);
        vec4 phase = vec4(iter->phase[0], iter->phase[1],
                          iter->phase[2], iter->phase[3]);
        if(iter->type == PG_WAVE_POP) {
            sub_len = 2;
            break;
        } else sub_len = 1;
        switch(iter->type) {
        default: break;
        case PG_WAVE_CONSTANT: {
            s += iter->constant * iter->scale + iter->add;
            break;
        } case PG_WAVE_PUSH: {
            iter_p = p_transform(p_, phase, freq);
            s += pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, d, iter_p)
                    * iter->scale + iter->add;
            break;
        } case PG_WAVE_SWIZZ: {
            iter_p.x = p_.v[iter->swizzle_x];
            iter_p.x = p_.v[iter->swizzle_y];
            iter_p.x = p_.v[iter->swizzle_z];
            iter_p.x = p_.v[iter->swizzle_w];
            p_ = p_transform(iter_p, phase, freq);
            break;
        } case PG_WAVE_FM: {
            iter_p = p_transform(p_, phase, freq);
            float modulate = pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 4, iter_p)
                                * iter->scale + iter->add;
            p_ = vec4( iter->modulate_x ? p_.x * modulate : p_.x,
                       iter->modulate_y ? p_.y * modulate : p_.y,
                       iter->modulate_z ? p_.z * modulate : p_.z,
                       iter->modulate_w ? p_.w * modulate : p_.w );
            break;
        } case PG_WAVE_PM: {
            iter_p = p_transform(p_, phase, freq);
            float modulate = pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 4, iter_p)
                                * iter->scale + iter->add;
            p_ = vec4( iter->modulate_x ? p_.x + modulate : p_.x,
                       iter->modulate_y ? p_.y + modulate : p_.y,
                       iter->modulate_z ? p_.z + modulate : p_.z,
                       iter->modulate_w ? p_.w + modulate : p_.w );
            break;
        } case PG_WAVE_AM: {
            iter_p = p_transform(p_, phase, freq);
            float am = pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 4, iter_p);
            scale *= am;
            break;
        } case PG_WAVE_ARRAY: {
            iter_p = p_transform(p_, phase, freq);
            s += pg_wave_sample_array(iter->arr, NULL, iter->len, d, iter_p) * iter->scale + iter->add;
            break;
        } case PG_WAVE_FUNCTION: {
            float s_ = 0;
            iter_p = p_transform(p_, phase, freq);
            int d_ = d;
            while(!(iter->dimension_mask & (1 << (d_ - 1)))) --d_;
            switch(d_) {
                case 1: s_ = iter->func1(VEC_X(iter_p)); break;
                case 2: s_ = iter->func2(VEC_XY(iter_p)); break;
                case 3: s_ = iter->func3(VEC_XYZ(iter_p)); break;
                case 4: s_ = iter->func4(VEC_XYZW(iter_p)); break;
                default: break;
            }
            s += s_ * iter->scale + iter->add;
            break;
        } case PG_WAVE_MIX_FUNC: {
            iter_p = p_transform(p_, phase, freq);
            float s_ = pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, d, iter_p);
            s = iter->mix(s, s_, iter->mix_k) * iter->scale + iter->add;
            break;
        } case PG_WAVE_MODIFIER_OCTAVES: {
            iter_p = p_transform(p_, phase, freq);
            float s_ = 0;
            int j;
            float f = 1, v = 1;
            for(j = 0; j < iter->octaves; ++j) {
                s_ += v * pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, d,
                            vec4_scale(iter_p, f));
                f *= iter->ratio;
                v *= iter->decay;
            }
            s += s_ * iter->scale + iter->add;
            break;
        } case PG_WAVE_MODIFIER_SEAMLESS_1D: {
            iter_p = vec4( cos(p_.x * LM_TAU), sin(p_.x * LM_TAU) );
            iter_p = p_transform(iter_p, phase, freq);
            iter_p = vec4_scale(iter_p, 1 / LM_TAU);
            s += pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 2, iter_p)
                    * iter->scale + iter->add;
            break;
        } case PG_WAVE_MODIFIER_SEAMLESS_2D: {
            if(d < 2) return 0;
            iter_p = vec4( cos(p_.x * LM_TAU), sin(p_.x * LM_TAU),
                           cos(p_.y * LM_TAU), sin(p_.y * LM_TAU));
            iter_p = p_transform(iter_p, phase, freq);
            iter_p = vec4_scale(iter_p, 1 / LM_TAU);
            s += pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 4, iter_p)
                    * iter->scale + iter->add;
            break;
        } case PG_WAVE_MODIFIER_EXPAND: {
            if(d == 1) continue;
            iter_p = p_transform(p_, phase, freq);
            switch(iter->mode) {
            case PG_WAVE_EXPAND_BEFORE: {
                int j;
                float e = p_.v[0];
                switch(iter->op) {
                    case PG_WAVE_EXPAND_ADD: case PG_WAVE_EXPAND_AVG:
                        for(j = 1; j < d; ++j) e += p_.v[j];
                        break;
                    case PG_WAVE_EXPAND_SUB:
                        for(j = 1; j < d; ++j) e -= p_.v[j];
                        break;
                    case PG_WAVE_EXPAND_MUL:
                        for(j = 1; j < d; ++j) e *= p_.v[j];
                        break;
                    case PG_WAVE_EXPAND_DIV:
                        for(j = 1; j < d; ++j) e /= p_.v[j];
                        break;
                }
                if(iter->op == PG_WAVE_EXPAND_AVG) e /= d;
                s += pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 1, vec4(e))
                        * iter->scale + iter->add;
                break;
            } case PG_WAVE_EXPAND_AFTER: {
                float e[d];
                int j;
                for(j = 0; j < d; ++j) {
                    e[j] = pg_wave_sample_array(iter + 1, &sub_len, n - i - 1, 1, vec4(p_.v[j]));
                }
                float e_ = e[0];
                switch(iter->op) {
                    case PG_WAVE_EXPAND_ADD: case PG_WAVE_EXPAND_AVG:
                        for(j = 1; j < d; ++j) e_ += e[j];
                        break;
                    case PG_WAVE_EXPAND_SUB:
                        for(j = 1; j < d; ++j) e_ -= e[j];
                        break;
                    case PG_WAVE_EXPAND_MUL:
                        for(j = 1; j < d; ++j) e_ *= e[j];
                        break;
                    case PG_WAVE_EXPAND_DIV:
                        for(j = 1; j < d; ++j) e_ /= e[j];
                        break;
                }
                if(iter->op == PG_WAVE_EXPAND_AVG) e_ /= d;
                s += e_ * iter->scale + iter->add;
                break;
            } }
            break;
        } }
    }
    if(end_idx) *end_idx = i + sub_len;
    return s * scale + add;
}

float pg_wave_sample(struct pg_wave* wave, int d, vec4 p)
{
    int a = 1;
    return pg_wave_sample_array(wave, &a, 1, d, p);
}

/*  Built-in mixing functions   */
float pg_wave_mix_min(float a, float b, float k)
{
    return LM_MIN(a, b);
}
float pg_wave_mix_min_abs(float a, float b, float k)
{
    return LM_MIN(fabsf(a), fabsf(b)) * LM_SGN(a);
}
float pg_wave_mix_smin(float a, float b, float k)
{
    return smin(a, b, k);
}
float pg_wave_mix_smin_abs(float a, float b, float k)
{
    return smin(fabsf(a), fabsf(b), k) * LM_SGN(a);
}
float pg_wave_mix_max(float a, float b, float k)
{
    return LM_MAX(a, b);
}
float pg_wave_mix_max_abs(float a, float b, float k)
{
    return LM_MAX(fabsf(a), fabsf(b)) * LM_SGN(a);
}
float pg_wave_mix_lerp(float a, float b, float k)
{
    return LM_LERP(a, b, k);
}

/*  Function definitions for the built-in waves */
float pg_wave_rand1(float x)
{
    return (float)((double)rand() / (double)RAND_MAX) * 2 - 1;
}

float pg_wave_sin1(float x)
{
    return sin(x * M_PI * 2);
}
float pg_wave_tri1(float x)
{
    return fabsf(fmodf(x, 4.0f) - 2) - 1;
}
float pg_wave_square1(float x)
{
    return (fmodf(x, 2.0f) < 1.0) * 2 - 1;
}
float pg_wave_saw1(float x)
{
    return 1.0f - floor(x);
}

float pg_wave_dist1(float x)
{
    return fabsf(x);
}
float pg_wave_dist2(float x, float y)
{
    return sqrtf(x*x + y*y);
}
float pg_wave_dist3(float x, float y, float z)
{
    return sqrtf(x*x + y*y + z*z);
}
float pg_wave_dist4(float x, float y, float z, float w)
{
    return sqrtf(x*x + y*y + z*z + w*w);
}

float pg_wave_max1(float x)
{
    return fabs(x);
}
float pg_wave_max2(float x, float y)
{
    return LM_MAX(fabs(x), fabs(y));
}
float pg_wave_max3(float x, float y, float z)
{
    return LM_MAX(LM_MAX(fabs(x), fabs(y)), fabs(z));
}
float pg_wave_max4(float x, float y, float z, float w)
{
    return LM_MAX(LM_MAX(LM_MAX(fabs(x), fabs(y)), fabs(z)), fabs(w));
}
