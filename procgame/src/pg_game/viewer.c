#include "procgame.h"

void pg_viewer_perspective(struct pg_viewer* view, struct pg_gfx_transform* tx,
                           float aspect_ratio, float fov, vec2 near_far)
{
    view->view_tx = *tx;
    view->near_far = near_far;
    view->fov = fov;
    view->aspect_ratio = aspect_ratio;

    view->projection = mat4_perspective(fov, aspect_ratio, near_far.x, near_far.y);
    view->projview = mat4_mul(view->projection, tx->tx_inverse);
    view->projview_inverse = mat4_inverse(view->projview);
}

void pg_viewer_ortho(struct pg_viewer* view, struct pg_gfx_transform* tx, vec2 size)
{
    view->view_tx = *tx;
    vec2 size_half = vec2_scale(size, 0.5);
    view->projection = mat4_ortho(-size_half.x, size_half.x, -size_half.y, size_half.y, -1, 1);
    view->projview = mat4_mul(view->projection, tx->tx_inverse);
    view->projview_inverse = mat4_inverse(view->projview);
}

vec3 pg_viewer_project(struct pg_viewer* view, vec3 pos)
{
    vec4 pos_ = vec4(VEC_XYZ(pos), 1);
    vec4 out = mat4_mul_vec4(view->projview, pos_);
    out.x /= out.w;
    out.y /= out.w;
    out.z /= out.w;
    return vec3(VEC_XYZ(out));
}

vec3 pg_viewer_unproject(struct pg_viewer* view, vec3 pos)
{
    vec4 pos_ = vec4(VEC_XYZ(pos), 1);
    vec4 out = mat4_mul_vec4(view->projview_inverse, pos_);
    out.w = 1.0f / out.w;
    out.x *= out.w;
    out.y *= out.w;
    out.z *= out.w;
    return vec3(VEC_XYZ(out));
}

vec2 pg_viewer_unproject2(struct pg_viewer* view, vec2 pos)
{
    vec4 pos_ = vec4(VEC_XY(pos), 0, 1);
    vec4 out = mat4_mul_vec4(view->projview_inverse, pos_);
    return vec2(VEC_XY(out));
}
