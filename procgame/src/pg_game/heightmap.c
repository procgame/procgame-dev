#include "procgame.h"

void pg_heightmap_init(struct pg_heightmap* hmap, ivec2 map_size, float cell_size)
{
    hmap->map = calloc(map_size.x * map_size.y, sizeof(float));
    hmap->map_size = map_size;
    hmap->cell_size = cell_size;
}

void pg_heightmap_deinit(struct pg_heightmap* hmap)
{
    free(hmap->map);
}

void pg_heightmap_from_wave(struct pg_heightmap* hmap, struct pg_wave* wave,
                            float fx, float fy)
{
    int x, y;
    for(x = 0; x < hmap->map_size.x; ++x) {
        for(y = 0; y < hmap->map_size.y; ++y) {
            vec4 p = vec4( (float)x / hmap->map_size.x * fx, (float)y / hmap->map_size.y * fy );
            hmap->map[x + y * hmap->map_size.x] = pg_wave_sample(wave, 2, p);
        }
    }
}

void pg_heightmap_set_height(struct pg_heightmap* hmap, int x, int y, float h)
{
    x = LM_CLAMP(x, 0, hmap->map_size.x - 1);
    y = LM_CLAMP(y, 0, hmap->map_size.y - 1);
    hmap->map[x + y * hmap->map_size.x] = h;
}

float pg_heightmap_get_height(struct pg_heightmap* hmap, int x, int y)
{
    x = LM_CLAMP(x, 0, hmap->map_size.x - 1);
    y = LM_CLAMP(y, 0, hmap->map_size.y - 1);
    return hmap->map[x + y * hmap->map_size.x];
}

float pg_heightmap_get_height_lerp(struct pg_heightmap* hmap, float x, float y)
{
    x = LM_CLAMP(x, 0, hmap->map_size.x - 1);
    y = LM_CLAMP(y, 0, hmap->map_size.y - 1);
    int cells_x = hmap->map_size.x;
    float xf, yf, xi, yi;
    xf = modff(x, &xi);
    yf = modff(y, &yi);
    float tl = hmap->map[(int)xi + (int)yi * cells_x];
    float tr = hmap->map[((int)xi+1) + (int)yi * cells_x];
    float bl = hmap->map[(int)xi + ((int)yi+1) * cells_x];
    float br = hmap->map[((int)xi+1) + ((int)yi+1) * cells_x];
    float l0 = tl + xf * (tr - tl);
    float l1 = bl + xf * (br - bl);
    return l0 + yf * (l1 - l0);
}
