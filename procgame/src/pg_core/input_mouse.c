#include "pg_core/pg_core.h"

#include "pg_modules/window.h"


static enum pg_mouse_mode mouse_mode = PG_INPUT_MOUSE_FREE;
static struct pg_input_wrapper input_mouse;
struct pg_input_wrapper* pg_input_mouse(void) { return &input_mouse; }

void pg_input_mouse_mode(enum pg_mouse_mode mode)
{
    if(mode == PG_INPUT_MOUSE_CAPTURE) {
        SDL_SetRelativeMouseMode(SDL_ENABLE);
        SDL_GetRelativeMouseState(NULL, NULL);
        mouse_mode = PG_INPUT_MOUSE_CAPTURE;
    } else if(mode == PG_INPUT_MOUSE_FREE) {
        SDL_SetRelativeMouseMode(SDL_DISABLE);
        mouse_mode = PG_INPUT_MOUSE_FREE;
    }
}

enum pg_input_mouse_boolean mouse_button_from_sdl(int sdl_button)
{
    if(SDL_BUTTON_LEFT) return PG_INPUT_MOUSE_LEFT;
    else if(SDL_BUTTON_RIGHT) return PG_INPUT_MOUSE_RIGHT;
    else if(SDL_BUTTON_MIDDLE) return PG_INPUT_MOUSE_MIDDLE;
    else return PG_INPUT_MOUSE_LEFT;
}

void pg_input_mouse_poll(void)
{
    struct pg_input_wrapper* mouse = pg_input_mouse();
    pg_input_wrapper_step(mouse);

    sdl_event_queue_t* sdl_events = pg_input_sdl_events();
    SDL_Event* e;
    int i;
    ARR_FOREACH_PTR(*sdl_events, e, i) {
        if(e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP) {
            bool hit = (e->type == SDL_MOUSEBUTTONDOWN);
            pg_input_boolean_action(mouse, mouse_button_from_sdl(e->button.button), hit);
        } else if(e->type == SDL_MOUSEWHEEL) {
            if(e->wheel.y == 1) {
                pg_input_boolean_action(mouse, PG_INPUT_MOUSEWHEEL_UP, true);
                pg_input_boolean_action(mouse, PG_INPUT_MOUSEWHEEL_UP, false);
            } else if(e->wheel.y == -1) {
                pg_input_boolean_action(mouse, PG_INPUT_MOUSEWHEEL_DOWN, true);
                pg_input_boolean_action(mouse, PG_INPUT_MOUSEWHEEL_DOWN, false);
            }
        }
    }

    /*  Handle mouse position/motion. Coordinates are converted to [-1,1] in both axes,
        with (0,0) in the center of the window  */
    int mx, my;
    if(mouse_mode == PG_INPUT_MOUSE_CAPTURE) {
        SDL_GetRelativeMouseState(&mx, &my);
        vec2 motion = vec2(mx, my);
        pg_input_set_analog(mouse, 0, motion);
    } else if(mouse_mode == PG_INPUT_MOUSE_FREE) {
        SDL_GetMouseState(&mx, &my);
        vec2 pos = vec2_div(vec2(mx, my), vec2(VEC_XY(pg_window_size())));
        pg_input_set_analog(mouse, 0, pos);
    }
}

