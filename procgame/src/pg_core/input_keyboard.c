#include "pg_core/pg_core.h"

static struct pg_input_wrapper input_keyboard = {};
struct pg_input_wrapper* pg_input_keyboard(void) { return &input_keyboard; }

void pg_input_keyboard_poll(void)
{
    struct pg_input_wrapper* keyboard = pg_input_keyboard();
    pg_input_wrapper_step(keyboard);

    sdl_event_queue_t* sdl_events = pg_input_sdl_events();
    SDL_Event* e;
    int i;
    ARR_FOREACH_PTR(*sdl_events, e, i) {
        if(e->type == SDL_KEYDOWN) {
            pg_input_state_t cur_key_state = pg_input_get_boolean(keyboard, e->key.keysym.scancode);
            if(!((cur_key_state & PG_INPUT_ON))) {
                pg_input_boolean_action(keyboard, e->key.keysym.scancode, true);
            }
        } else if(e->type == SDL_KEYUP) {
            pg_input_boolean_action(keyboard, e->key.keysym.scancode, false);
        }
    }
}
