#include <pg_core/sdl.h>
#include "pg_math/math_utility.h"

/****************************/
/*  Performance testing     */
/****************************/
double pg_time(void)
{
    return SDL_GetTicks() * 0.001;
}

uint64_t pg_perf_time(void)
{
    return SDL_GetPerformanceCounter();
}

double pg_perf_time_diff(uint64_t start, uint64_t end)
{
    return (double)(end - start) / (double)SDL_GetPerformanceFrequency();
}

/*  Previous 30 frames time stamps, ring buffer */
static uint64_t frame_timestamps[30];
static int frame_timestamps_idx;
static double framerate;

void pg_frame_timestamp(void)
{
    frame_timestamps_idx = RING_IDX(30, frame_timestamps_idx, -1);
    frame_timestamps[frame_timestamps_idx] = pg_perf_time();
    uint64_t frames[4] = {
        frame_timestamps[RING_IDX(30, frame_timestamps_idx, 0)],
        frame_timestamps[RING_IDX(30, frame_timestamps_idx, 1)],
        frame_timestamps[RING_IDX(30, frame_timestamps_idx, 2)],
        frame_timestamps[RING_IDX(30, frame_timestamps_idx, 3)] };
    double time_diff[3] = {
        pg_perf_time_diff(frames[1], frames[0]),
        pg_perf_time_diff(frames[2], frames[1]),
        pg_perf_time_diff(frames[3], frames[2]), };
    double avg_diff = (time_diff[0] + time_diff[1] + time_diff[2]) / 3.0;
    framerate = 1.0 / avg_diff;
}

double pg_framerate(void)
{
    return framerate + 0.0001;
}

