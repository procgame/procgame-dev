#include "pg_core/pg_core.h"

static pg_input_state_t input_apply_action(pg_input_state_t in, bool hit)
{
    pg_input_state_t out = in;
    if(hit) {
        out |= PG_INPUT_ON;
        out |= PG_INPUT_HIT;
    } else {
        out &= ~PG_INPUT_ON;
        out |= PG_INPUT_RELEASED;
    }
    return out;
}

static pg_input_state_t input_boolean_step(pg_input_state_t in)
{
    pg_input_state_t out = in;
    if(in & PG_INPUT_ON) out |= PG_INPUT_HELD;
    else out &= ~PG_INPUT_HELD;
    out &= ~PG_INPUT_HIT;
    out &= ~PG_INPUT_RELEASED;
    return out;
}


void pg_input_boolean_action(struct pg_input_wrapper* iw, int boolean_idx, bool hit)
{
    pg_input_state_t* input = &iw->boolean_states[boolean_idx];
    *input = input_apply_action(*input, hit);
}

pg_input_state_t pg_input_get_boolean(struct pg_input_wrapper* iw, int boolean_idx)
{
    return iw->boolean_states[boolean_idx];
}

void pg_input_set_analog(struct pg_input_wrapper* iw, int analog_idx, vec2 value)
{
    struct pg_input_analog* analog = &iw->analog_states[analog_idx];
    analog->position = value;
}

void pg_input_add_analog(struct pg_input_wrapper* iw, int analog_idx, vec2 value)
{
    struct pg_input_analog* analog = &iw->analog_states[analog_idx];
    analog->position = vec2_add(analog->position, value);
}

vec2 pg_input_get_analog(struct pg_input_wrapper* iw, int analog_idx)
{
    struct pg_input_analog* analog = &iw->analog_states[analog_idx];
    vec2 value = analog->position;
    if(fabs(value.x) < analog->deadzone.x) value.x = 0;
    if(fabs(value.y) < analog->deadzone.y) value.y = 0;
    return value;
}

pg_input_state_t pg_input_get_analog_boolean(struct pg_input_wrapper* iw, int analog_idx, ivec2 dir)
{
    struct pg_input_analog* analog = &iw->analog_states[analog_idx];
    if(dir.x && dir.y) return PG_INPUT_OFF;
    if(ivec2_is_zero(dir)) return analog->press_boolean;
    dir = ivec2_sgn3(dir);
    int idx = (dir.x ? 0 : 2) + (dir.x ? dir.x : dir.y);
    return analog->analog_boolean[idx];
}

void pg_input_wrapper_step(struct pg_input_wrapper* iw)
{
    for(int i = 0; i < 256; ++i) {
        iw->boolean_states[i] = input_boolean_step(iw->boolean_states[i]);
    }
    for(int i = 0; i < 32; ++i) {
        struct pg_input_analog* analog = &iw->analog_states[i];
        analog->press_boolean = input_boolean_step(analog->press_boolean);
        for(int j = 0; j < 4; ++j) analog->analog_boolean[j] = input_boolean_step(analog->analog_boolean[j]);
    }
}

void pg_input_wrapper_reset(struct pg_input_wrapper* iw)
{
    for(int i = 0; i < 256; ++i) {
        iw->boolean_states[i] = PG_INPUT_OFF;
    }
    for(int i = 0; i < 32; ++i) {
        struct pg_input_analog* analog = &iw->analog_states[i];
        analog->position = vec2(0,0);
        analog->press_boolean = PG_INPUT_OFF;
        for(int j = 0; j < 4; ++j) analog->analog_boolean[j] = PG_INPUT_OFF;
    }
}

void pg_input_wrapper_map_booleans(struct pg_input_wrapper* dst, const struct pg_input_wrapper* src,
        int n_mappings, ivec2* mappings)
{
    for(int i = 0; i < n_mappings; ++i) {
        dst->boolean_states[mappings[i].x] = src->boolean_states[mappings[i].y];
    }
}
