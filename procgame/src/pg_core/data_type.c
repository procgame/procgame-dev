//#include <string.h>
//#include <stdlib.h>

#include "pg_core/pg_core.h"
#include "pg_math/linmath.h"

const enum pg_data_type pg_data_base_type[] = {
    [PG_NULL] = PG_NULL,
    [PG_BYTE] = PG_BYTE, [PG_BVEC2] = PG_BYTE, [PG_BVEC3] = PG_BYTE, [PG_BVEC4] = PG_BYTE,
    [PG_UBYTE] = PG_UBYTE, [PG_UBVEC2] = PG_UBYTE, [PG_UBVEC3] = PG_UBYTE, [PG_UBVEC4] = PG_UBYTE,
    [PG_SHORT] = PG_SHORT, [PG_SVEC2] = PG_SHORT, [PG_SVEC3] = PG_SHORT, [PG_SVEC4] = PG_SHORT,
    [PG_USHORT] = PG_USHORT, [PG_USVEC2] = PG_USHORT, [PG_USVEC3] = PG_USHORT, [PG_USVEC4] = PG_USHORT,
    [PG_INT] = PG_INT, [PG_IVEC2] = PG_INT, [PG_IVEC3] = PG_INT, [PG_IVEC4] = PG_INT,
    [PG_UINT] = PG_UINT, [PG_UVEC2] = PG_UINT, [PG_UVEC3] = PG_UINT, [PG_UVEC4] = PG_UINT,
    [PG_FLOAT] = PG_FLOAT, [PG_VEC2] = PG_FLOAT, [PG_VEC3] = PG_FLOAT, [PG_VEC4] = PG_FLOAT,
    [PG_MAT4] = PG_FLOAT, [PG_MAT3] = PG_FLOAT,
    [PG_POINTER] = PG_POINTER,
    [PG_STRING] = PG_BYTE,
};

const int pg_data_type_components[] = {
    [PG_NULL] = PG_NULL,
    [PG_BYTE] = 1, [PG_BVEC2] = 2, [PG_BVEC3] = 3, [PG_BVEC4] = 4,
    [PG_UBYTE] = 1, [PG_UBVEC2] = 2, [PG_UBVEC3] = 3, [PG_UBVEC4] = 4,
    [PG_SHORT] = 1, [PG_SVEC2] = 2, [PG_SVEC3] = 3, [PG_SVEC4] = 4,
    [PG_USHORT] = 1, [PG_USVEC2] = 2, [PG_USVEC3] = 3, [PG_USVEC4] = 4,
    [PG_INT] = 1, [PG_IVEC2] = 2, [PG_IVEC3] = 3, [PG_IVEC4] = 4,
    [PG_UINT] = 1, [PG_UVEC2] = 2, [PG_UVEC3] = 3, [PG_UVEC4] = 4,
    [PG_FLOAT] = 1, [PG_VEC2] = 2, [PG_VEC3] = 3, [PG_VEC4] = 4,
    [PG_MAT4] = 1, [PG_MAT3] = 2,
    [PG_POINTER] = 1,
    [PG_STRING] = 1,
};

const char* pg_data_type_string[] = {
    [PG_NULL] = "null",
    [PG_BYTE] = "byte", [PG_BVEC2] = "bvec2", [PG_BVEC3] = "bvec3", [PG_BVEC4] = "bvec4",
    [PG_UBYTE] = "ubyte", [PG_UBVEC2] = "ubvec2", [PG_UBVEC3] = "ubvec3", [PG_UBVEC4] = "ubvec4",
    [PG_SHORT] = "short", [PG_SVEC2] = "svec2", [PG_SVEC3] = "svec3", [PG_SVEC4] = "svec4",
    [PG_USHORT] = "ushort", [PG_USVEC2] = "usvec2", [PG_USVEC3] = "usvec3", [PG_USVEC4] = "usvec4",
    [PG_INT] = "int", [PG_IVEC2] = "ivec2", [PG_IVEC3] = "ivec3", [PG_IVEC4] = "ivec4",
    [PG_UINT] = "uint", [PG_UVEC2] = "uvec2", [PG_UVEC3] = "uvec3", [PG_UVEC4] = "uvec4",
    [PG_FLOAT] = "float", [PG_VEC2] = "vec2", [PG_VEC3] = "vec3", [PG_VEC4] = "vec4",
    [PG_MAT4] = "mat4", [PG_MAT3] = "mat3",
    [PG_POINTER] = "pointer",
    [PG_STRING] = "string",
};

const size_t pg_data_type_size[] = {
    [PG_NULL] = 0,
    [PG_BYTE] = sizeof(char), [PG_BVEC2] = sizeof(bvec2),
    [PG_BVEC3] = sizeof(bvec3), [PG_BVEC4] = sizeof(bvec4),
    [PG_UBYTE] = sizeof(unsigned char), [PG_UBVEC2] = sizeof(ubvec2),
    [PG_UBVEC3] = sizeof(ubvec3), [PG_UBVEC4] = sizeof(ubvec4),
    [PG_SHORT] = sizeof(short), [PG_SVEC2] = sizeof(svec2),
    [PG_SVEC3] = sizeof(svec3), [PG_SVEC4] = sizeof(svec4),
    [PG_USHORT] = sizeof(unsigned short), [PG_USVEC2] = sizeof(usvec2),
    [PG_USVEC3] = sizeof(usvec3), [PG_USVEC4] = sizeof(usvec4),
    [PG_INT] = sizeof(int), [PG_IVEC2] = sizeof(ivec2),
    [PG_IVEC3] = sizeof(ivec3), [PG_IVEC4] = sizeof(ivec4),
    [PG_UINT] = sizeof(unsigned int), [PG_UVEC2] = sizeof(uvec2),
    [PG_UVEC3] = sizeof(uvec3), [PG_UVEC4] = sizeof(uvec4),
    [PG_FLOAT] = sizeof(float), [PG_VEC2] = sizeof(vec2),
    [PG_VEC3] = sizeof(vec3), [PG_VEC4] = sizeof(vec4),
    [PG_MAT4] = sizeof(mat4), [PG_MAT3] = sizeof(mat3),
    [PG_POINTER] = sizeof(void*),
    [PG_STRING] = sizeof(char),
};

enum pg_data_type pg_data_type_from_string(const char* string)
{
    if(!string) return PG_NULL;
    for(int i = 0; i < PG_NUM_DATA_TYPES; ++i) {
        if(strncmp(pg_data_type_string[i], string, 8) == 0) return i;
    }
    return PG_NULL;
}

void* pg_data_value_ptr(pg_data_t* d, int i)
{
    if(i < 0 || i >= d->n) return NULL;
    if(d->big) return ((char*)d->ptr) + (i * pg_data_type_size[d->type]);
    return ((char*)d->small.ptr) + (i * pg_data_type_size[d->type]);
}

pg_data_t pg_data_init(enum pg_data_type type, int n, void* data)
{
    pg_data_t new_data = {0};
    new_data.type = type;
    new_data.n = n;
    if(n * pg_data_type_size[type] > 64) {
        new_data.ptr = malloc(n * pg_data_type_size[type]);
        new_data.big = true;
    }
    if(data) {
        if(type == PG_STRING) {
            strncpy(pg_data_value_ptr(&new_data, 0), data, n * pg_data_type_size[type]);
        } else {
            memcpy(pg_data_value_ptr(&new_data, 0), data, n * pg_data_type_size[type]);
        }
    }
    return new_data;
}

pg_data_t pg_data_duplicate(pg_data_t* src)
{
    pg_data_t new_data = *src;
    if(src->big) {
        new_data.ptr = malloc(src->n * pg_data_type_size[src->type]);
        memcpy(new_data.ptr, src->ptr, src->n * pg_data_type_size[src->type]);
    }
    return new_data;
}

void pg_data_copy(pg_data_t* dst, pg_data_t* src)
{
    if(dst->type == PG_NULL) *dst = pg_data_duplicate(src);
    if(src->type != dst->type || src->n != dst->n) return;
    pg_data_update(dst, dst->type, pg_data_value_ptr(src, 0));
}

void pg_data_update(pg_data_t* d, enum pg_data_type type, void* data)
{
    if(d->type != type) return;
    memcpy(pg_data_value_ptr(d, 0), data, d->n * pg_data_type_size[type]);
}

void pg_data_deinit(pg_data_t* d)
{
    if(d->big) free(d->ptr);
    *d = (pg_data_t){0};
}

pg_data_table_t pg_data_table(int n_buckets)
{
    pg_data_table_t table;
    HTABLE_INIT(table, n_buckets);
    return table;
}

void pg_data_table_deinit(pg_data_table_t* table)
{
    pg_data_t* uni;
    HTABLE_ITER uni_iter;
    HTABLE_ITER_BEGIN(*table, uni_iter);
    while(!HTABLE_ITER_END(*table, uni_iter)) {
        HTABLE_ITER_NEXT_PTR(*table, uni_iter, uni);
        if(uni) pg_data_deinit(uni);
    }
    HTABLE_DEINIT(*table);
}


bool pg_data_to_string(pg_data_t* d, char* out, int out_max)
{
    if(d->type == PG_STRING) {
        strncpy(out, pg_data_value_ptr(d, 0), out_max);
        return true;
    }
    char* iter = out;
    int out_remaining = out_max;

    iter += snprintf(iter, out_remaining, "(%s) ", pg_data_type_string[d->type]);

    /*  Open array if needed    */
    if(d->n > 1) {
        if(out_remaining <= 2) return false;
        memcpy(iter, "[ ", 2);
        iter += 2;
        out_remaining -= 2;
    }

    for(int i = 0; i < d->n; ++i) {
        void* ptr = pg_data_value_ptr(d, i);
        if(i > 0) {
            if(out_remaining <= 2) return false;
            memcpy(iter, ", ", 2);
            iter += 2;
            out_remaining -= 2;
        }
        int n_out = 0;
        switch(d->type) {
        case PG_UBYTE: n_out = snprintf(iter, out_remaining, "%u",
            (unsigned int)(((uint8_t*)ptr)[0]));
            break;
        case PG_UBVEC2: n_out = snprintf(iter, out_remaining, "{ %u, %u }",
            (unsigned int)(((uint8_t*)ptr)[0]), (unsigned int)(((uint8_t*)ptr)[1]) );
            break;
        case PG_UBVEC3: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u }",
            (unsigned int)(((uint8_t*)ptr)[0]), (unsigned int)(((uint8_t*)ptr)[1]), (unsigned int)(((uint8_t*)ptr)[2]) );
            break;
        case PG_UBVEC4: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u, %u }",
            (unsigned int)(((uint8_t*)ptr)[0]), (unsigned int)(((uint8_t*)ptr)[1]), (unsigned int)(((uint8_t*)ptr)[2]), (unsigned int)(((uint8_t*)ptr)[3]) );
            break;
    
        case PG_BYTE: n_out = snprintf(iter, out_remaining, "%d",
            (int)(((int8_t*)ptr)[0]));
            break;
        case PG_BVEC2: n_out = snprintf(iter, out_remaining, "{ %d, %d }",
            (int)(((int8_t*)ptr)[0]), (int)(((int8_t*)ptr)[1]) );
            break;
        case PG_BVEC3: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d }",
            (int)(((int8_t*)ptr)[0]), (int)(((int8_t*)ptr)[1]), (int)(((int8_t*)ptr)[2]) );
            break;
        case PG_BVEC4: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d, %d }",
            (int)(((int8_t*)ptr)[0]), (int)(((int8_t*)ptr)[1]), (int)(((int8_t*)ptr)[2]), (int)(((int8_t*)ptr)[3]) );
            break;

        case PG_USHORT: n_out = snprintf(iter, out_remaining, "%u",
            (unsigned int)(((uint16_t*)ptr)[0]));
            break;
        case PG_USVEC2: n_out = snprintf(iter, out_remaining, "{ %u, %u }",
            (unsigned int)(((uint16_t*)ptr)[0]), (unsigned int)(((uint16_t*)ptr)[1]) );
            break;
        case PG_USVEC3: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u }",
            (unsigned int)(((uint16_t*)ptr)[0]), (unsigned int)(((uint16_t*)ptr)[1]), (unsigned int)(((uint16_t*)ptr)[2]) );
            break;
        case PG_USVEC4: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u, %u }",
            (unsigned int)(((uint16_t*)ptr)[0]), (unsigned int)(((uint16_t*)ptr)[1]), (unsigned int)(((uint16_t*)ptr)[2]), (unsigned int)(((uint16_t*)ptr)[3]) );
            break;
    
        case PG_SHORT: n_out = snprintf(iter, out_remaining, "%d",
            (int)(((int16_t*)ptr)[0]));
            break;
        case PG_SVEC2: n_out = snprintf(iter, out_remaining, "{ %d, %d }",
            (int)(((int16_t*)ptr)[0]), (int)(((int16_t*)ptr)[1]) );
            break;
        case PG_SVEC3: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d }",
            (int)(((int16_t*)ptr)[0]), (int)(((int16_t*)ptr)[1]), (int)(((int16_t*)ptr)[2]) );
            break;
        case PG_SVEC4: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d, %d }",
            (int)(((int16_t*)ptr)[0]), (int)(((int16_t*)ptr)[1]), (int)(((int16_t*)ptr)[2]), (int)(((int16_t*)ptr)[3]) );
            break;
        
        case PG_UINT: n_out = snprintf(iter, out_remaining, "%u",
            (unsigned int)(((uint32_t*)ptr)[0]));
            break;
        case PG_UVEC2: n_out = snprintf(iter, out_remaining, "{ %u, %u }",
            (unsigned int)(((uint32_t*)ptr)[0]), (unsigned int)(((uint32_t*)ptr)[1]) );
            break;
        case PG_UVEC3: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u }",
            (unsigned int)(((uint32_t*)ptr)[0]), (unsigned int)(((uint32_t*)ptr)[1]), (unsigned int)(((uint32_t*)ptr)[2]) );
            break;
        case PG_UVEC4: n_out = snprintf(iter, out_remaining, "{ %u, %u, %u, %u }",
            (unsigned int)(((uint32_t*)ptr)[0]), (unsigned int)(((uint32_t*)ptr)[1]), (unsigned int)(((uint32_t*)ptr)[2]), (unsigned int)(((uint32_t*)ptr)[3]) );
            break;
    
        case PG_INT: n_out = snprintf(iter, out_remaining, "%d",
            (int)(((int32_t*)ptr)[0]));
            break;
        case PG_IVEC2: n_out = snprintf(iter, out_remaining, "{ %d, %d }",
            (int)(((int32_t*)ptr)[0]), (int)(((int32_t*)ptr)[1]) );
            break;
        case PG_IVEC3: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d }",
            (int)(((int32_t*)ptr)[0]), (int)(((int32_t*)ptr)[1]), (int)(((int32_t*)ptr)[2]) );
            break;
        case PG_IVEC4: n_out = snprintf(iter, out_remaining, "{ %d, %d, %d, %d }",
            (int)(((int32_t*)ptr)[0]), (int)(((int32_t*)ptr)[1]), (int)(((int32_t*)ptr)[2]), (int)(((int32_t*)ptr)[3]) );
            break;

        case PG_FLOAT: n_out = snprintf(iter, out_remaining, "%f",
            ((float*)ptr)[0] );
            break;
        case PG_VEC2: n_out = snprintf(iter, out_remaining, "{ %f, %f }",
            ((float*)ptr)[0], ((float*)ptr)[1] );
            break;
        case PG_VEC3: n_out = snprintf(iter, out_remaining, "{ %f, %f, %f }",
            ((float*)ptr)[0], ((float*)ptr)[1], ((float*)ptr)[2] );
            break;
        case PG_VEC4: n_out = snprintf(iter, out_remaining, "{ %f, %f, %f, %f }",
            ((float*)ptr)[0], ((float*)ptr)[1], ((float*)ptr)[2], ((float*)ptr)[3] );
            break;
        default: break;
        }
        iter += n_out;
        out_remaining -= n_out;
        if(out_remaining <= 0) return false;
    }

    if(d->n > 1) {
        if(out_remaining <= 2) return false;
        memcpy(iter, " ]", 2);
        iter += 2;
        out_remaining -= 2;
    }

    if(out_remaining < 1) return false;
    iter[0] = '\0';
    return true;
}
