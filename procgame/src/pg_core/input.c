#include "pg_core/pg_core.h"

/************/
/*  Input   */
/************/

/*  Global input state information  */
static bool user_exit = false;
bool pg_input_user_exit(void) { return user_exit; }

static sdl_event_queue_t sdl_event_queue = SARR_DATA(256, 0);
sdl_event_queue_t* pg_input_sdl_events(void) { return &sdl_event_queue; }

void pg_input_poll(void)
{
    ARR_TRUNCATE(sdl_event_queue, 0);

    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        if(e.type == SDL_QUIT) {
            user_exit = true;
            continue;
        }
        ARR_PUSH(sdl_event_queue, e);
    };

    pg_input_keyboard_poll();
    pg_input_mouse_poll();
}




