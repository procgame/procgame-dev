#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include "pg_core/logging.h"

FILE* logfile = NULL;
bool own_logfile;
enum pg_log_type log_level = PG_LOG_DEBUG;
enum pg_log_type last_log = PG_LOG_DEBUG;

void pg_log_open(const char* filename)
{
    logfile = fopen(filename, "wb");
    if(!logfile) {
        printf("[ERROR] Failed to open logfile for writing: %s", filename);
    }
    own_logfile = true;
}

void pg_log_file(FILE* file)
{
    logfile = file;
    own_logfile = false;
}

void pg_log_close(void)
{
    if(logfile && own_logfile) fclose(logfile);
}

void pg_log_level(enum pg_log_type level)
{
    log_level = level;
}

void pg_set_last_log_level(enum pg_log_type level)
{
    last_log = level;
}

static const char* log_type_string[] = {
    [PG_LOG_DEBUG_PROCGAME] = "PROCGAME",
    [PG_LOG_DEBUG] = "DEBUG",
    [PG_LOG_INFO] = "INFO",
    [PG_LOG_WARNING] = "WARNING",
    [PG_LOG_ERROR] = "ERROR",
    [PG_LOG_ERROR_FATAL] = "!!!FATAL ERROR!!!",
};

void pg_log_(enum pg_log_type type, const char* fmt,
             int src_line, const char* src_filename, ...)
{
    FILE* output = logfile ? logfile : stderr;
    /*  Handle continued messages from disabled log levels  */
    if((type == PG_LOG_CONTD && last_log < log_level)) return;
    else if(type != PG_LOG_CONTD && type < log_level) {
        last_log = type;
        return;
    }
    /*  Handle debug source info and indentation    */
    if(type != PG_LOG_CONTD) {
        fprintf(output, " [%s] %s:%d\n", log_type_string[type], src_filename, src_line);
    }
    /*  Write the log message   */
    va_list args;
    va_start(args, src_filename);
    vfprintf(output, fmt, args);
    fflush(output);
    va_end(args);
}

void pg_log_direct_(enum pg_log_type type, const char* fmt,
             int src_line, const char* src_filename, ...)
{
    FILE* output = logfile ? logfile : stderr;
    /*  Handle continued messages from disabled log levels  */
    if((type == PG_LOG_CONTD && last_log < log_level)) {
        return;
    } else if(type != PG_LOG_CONTD && type < log_level) {
        last_log = type;
        return;
    }
    /*  Handle debug source info and indentation    */
    if(type != PG_LOG_CONTD) {
        fprintf(output, " [%s] %s:%d\n", log_type_string[type], src_filename, src_line);
    }
    /*  Write the log message   */
    va_list args;
    va_start(args, src_filename);
    vfprintf(output, fmt, args);
    fflush(output);
    va_end(args);
}

void pg_vlog_(enum pg_log_type type, const char* fmt,
             int src_line, const char* src_filename, va_list args)
{
    FILE* output = logfile ? logfile : stderr;
    /*  Handle continued messages from disabled log levels  */
    if((type == PG_LOG_CONTD && last_log < log_level)) return;
    else if(type != PG_LOG_CONTD && type < log_level) {
        last_log = type;
        return;
    }
    /*  Handle debug source info and indentation    */
    if(type != PG_LOG_CONTD) {
        fprintf(output, " [%s] %s:%d\n    ", log_type_string[type], src_filename, src_line);
    } else {
        fprintf(output, "        ");
    }
    /*  Write the log message   */
    vfprintf(output, fmt, args);
    fflush(output);
}

void pg_vlog_direct_(enum pg_log_type type, const char* fmt,
             int src_line, const char* src_filename, va_list args)
{
    FILE* output = logfile ? logfile : stderr;
    /*  Handle continued messages from disabled log levels  */
    if((type == PG_LOG_CONTD && last_log < log_level)) {
        return;
    } else if(type != PG_LOG_CONTD && type < log_level) {
        last_log = type;
        return;
    }
    /*  Handle debug source info and indentation    */
    if(type != PG_LOG_CONTD) {
        fprintf(output, " [%s] %s:%d\n", log_type_string[type], src_filename, src_line);
    }
    /*  Write the log message   */
    vfprintf(output, fmt, args);
    fflush(output);
}
