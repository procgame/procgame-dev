/**
    \file file_utility.h
    \ingroup pg_util
    \brief File management utilities
*/

#pragma once

#include <stdio.h>

/** Max length of a FULL PATH (including filename)  */
#define PG_FILE_PATH_MAXLEN     1024
/** Max length of a filename only (basename)    */
#define PG_FILE_NAME_MAXLEN     256
/** Max length of a file extension  */
#define PG_FILE_EXT_MAXLEN      32
/** Max length of a line for line-reading from files  */
#define PG_FILE_LINE_MAXLEN     256

/** Helper/wrapper struct around an open file handle    */
struct pg_file {
    /** Handle to the file  */
    FILE* f;
    /** Size of the file  */
    size_t size;
    /** Full filename */
    char fullname[PG_FILE_PATH_MAXLEN];
    /** Filename without leading directories  */
    char basename[PG_FILE_NAME_MAXLEN];
    /** Basename without the file extension */
    char shortname[PG_FILE_NAME_MAXLEN];
    /** File extension only (not including the '.') */
    char extension[PG_FILE_EXT_MAXLEN];
    /** Path without the filename (include the trailing '/')    */
    char path[PG_FILE_PATH_MAXLEN];
    /** Length of the filename parts    */
    size_t fullname_len, basename_len, shortname_len, extension_len, path_len;
    /** Full contents of the file (if it has been read) */
    char* content;
    /** Whether to free the content pointer when the file is closed */
    bool own_content;
    /** Full contents of the last line read */
    char line[PG_FILE_LINE_MAXLEN];
    /** Length of the last line read    */
    int line_len;
    /** Line number of the last line read   */
    int line_no;
};

/** Populate a pg_file structure using the given filename   */
bool pg_file_open_(struct pg_file* file, const char* filename, const char* mode,
                   int src_line, const char* src_file);
#define pg_file_open(FILE_PTR, FILENAME, MODE) \
    pg_file_open_(FILE_PTR, FILENAME, MODE, __LINE__, __FILE__)
/** Populate a pg_file structure using the given filename, searching among
    multiple given paths before the file is found.  */
bool pg_file_open_search_paths_(struct pg_file* file, const char** search_paths, int n_paths,
                                const char* filename, const char* mode,
                                int src_line, const char* src_file);
#define pg_file_open_search_paths(FILE_PTR, SEARCH_DIRS, N_SEARCH_DIRS, FILENAME, MODE) \
    pg_file_open_search_paths_(FILE_PTR, SEARCH_DIRS, N_SEARCH_DIRS, FILENAME, MODE, __LINE__, __FILE__)

/** Close an opened file (required for all opened files)    */
void pg_file_close(struct pg_file* file);
/** Reset an opened file (clears read lines/content and rewinds the file)   */
void pg_file_reset(struct pg_file* file);
/** Read the next line from the file (accessible through `line*` members)
 *  \remark Line-ending characters are NOT included in the line read    */
bool pg_file_read_line(struct pg_file* file);
/** Read the full contents of the file (accessible through `content` member */
void pg_file_read_full(struct pg_file* file);
/** Take ownership of the file's contents
 *  Allows the pg_file to be closed without the content data being freed as well    */
char* pg_file_take_content(struct pg_file* file);

/** Read text from a file with '#include' directives    */

bool pg_file_read_preprocessed_(struct pg_file* file,
        const char** include_dirs, int n_include_dirs, char_arr_t* out,
        int src_line, const char* src_file);
#define pg_file_read_preprocessed(FILE_PTR, INCLUDE_DIRS, N_INCLUDE_DIRS, OUTPUT) \
    pg_file_read_preprocessed_(FILE_PTR, INCLUDE_DIRS, N_INCLUDE_DIRS, OUTPUT, __LINE__, __FILE__)

/** \name Filename and Path Utilities
 * @{   */

struct pg_filepath { char path[PG_FILE_PATH_MAXLEN]; };
typedef ARR_T(struct pg_filepath) pg_filepath_arr_t;

/** Get the file extension for a given path */
int pg_filename_extension(const char* filename, int filename_len, char* out, int out_max);
/** Get the basename for a given path (filename without leading directories)    */
int pg_filename_basename(const char* filename, int filename_len, char* out, int out_max);
/** Get the shortname for a given path (filename without leading directories, or file extension */
int pg_filename_shortname(const char* filename, int filename_len, char* out, int out_max);
/** Get the path portion a given path (no filename, only leading directories) */
int pg_filename_path(const char* filename, int filename_len, char* out, int out_max);
/** Get the absolute path to a given file   */
int pg_filename_absolute_path(const char* filename, int filename_len, char* out, int out_max);

/** @}  */
