#pragma once

#include "cJSON.h"
#include "pg_math/math_utility.h"

cJSON* json_from_file(struct pg_file* file);
cJSON* json_from_file_preprocessed(struct pg_file* file, const char** include_dirs, int n_include_dirs);
cJSON* load_json(const char* filename);
cJSON* load_json_preprocessed(const char* filename, const char** include_dirs, int n_include_dirs);
cJSON* json_get_child(cJSON* json, const char* child_string);
const char* json_type_string(cJSON_Type);
pg_data_t json_read_pg_data(cJSON* json);

/*  JSON parsers for misc. types    */

void json_read_string(cJSON* json, char* out, int len);


/*  A JSON parser interface for the basic numeric types, and the linmath.h vectors  */

float json_read_float(cJSON* json);
double json_read_double(cJSON* json);
int32_t json_read_int(cJSON* json);
uint32_t json_read_uint(cJSON* json);
int16_t json_read_short(cJSON* json);
uint16_t json_read_ushort(cJSON* json);
int8_t json_read_byte(cJSON* json);
uint8_t json_read_ubyte(cJSON* json);

void json_read_aabb3D(cJSON* json, aabb3D* out);

vec2 json_read_vec2(cJSON* json);
vec3 json_read_vec3(cJSON* json);
vec4 json_read_vec4(cJSON* json);
quat json_read_quat(cJSON* json);

mat4 json_read_mat4(cJSON* json);
mat3 json_read_mat3(cJSON* json);

dvec2 json_read_dvec2(cJSON* json);
dvec3 json_read_dvec3(cJSON* json);
dvec4 json_read_dvec4(cJSON* json);

ivec2 json_read_ivec2(cJSON* json);
ivec3 json_read_ivec3(cJSON* json);
ivec4 json_read_ivec4(cJSON* json);

uvec2 json_read_uvec2(cJSON* json);
uvec3 json_read_uvec3(cJSON* json);
uvec4 json_read_uvec4(cJSON* json);

svec2 json_read_svec2(cJSON* json);
svec3 json_read_svec3(cJSON* json);
svec4 json_read_svec4(cJSON* json);

usvec2 json_read_usvec2(cJSON* json);
usvec3 json_read_usvec3(cJSON* json);
usvec4 json_read_usvec4(cJSON* json);

bvec2 json_read_bvec2(cJSON* json);
bvec3 json_read_bvec3(cJSON* json);
bvec4 json_read_bvec4(cJSON* json);

ubvec2 json_read_ubvec2(cJSON* json);
ubvec3 json_read_ubvec3(cJSON* json);
ubvec4 json_read_ubvec4(cJSON* json);


/*  Checking/loading complex JSON layouts    */
struct pg_json_layout {
    cJSON** json_object;
    const char* key;
    cJSON_Type expect_type;
    int n_children;
    struct pg_json_layout* children;
    bool optional;
};

#define PG_JSON_LAYOUT(N_ITEMS, ...) \
    (struct pg_json_layout){ .expect_type = cJSON_Object, \
        .n_children = (N_ITEMS), .children = PG_POINTERS(struct pg_json_layout, __VA_ARGS__) }

#define PG_JSON_LAYOUT_ITEM(NAME, TYPE, JSON) \
    (struct pg_json_layout){ .key = (NAME), .expect_type = (TYPE), .json_object = (JSON) }

#define PG_JSON_LAYOUT_OPTIONAL_ITEM(NAME, TYPE, JSON, ...) \
    (struct pg_json_layout){ .key = (NAME), .expect_type = (TYPE), .json_object = (JSON), \
        .optional = true }

#define PG_JSON_LAYOUT_GROUP(NAME, JSON, N_ITEMS, ...) \
    (struct pg_json_layout){ .key = (NAME), .expect_type = cJSON_Object, .json_object = (JSON), \
        .n_children = (N_ITEMS), .children = PG_POINTERS(struct pg_json_layout, __VA_ARGS__) }

#define PG_JSON_LAYOUT_OPTIONAL_GROUP(NAME, JSON, N_ITEMS, ...) \
    (struct pg_json_layout){ .key = (NAME), .expect_type = cJSON_Object, .json_object = (JSON), \
        .n_children = (N_ITEMS), .children = PG_POINTERS(struct pg_json_layout, __VA_ARGS__), \
        .optional = true }

bool pg_load_json_layout_(cJSON* json, struct pg_json_layout* layout, int src_line, const char* src_file);
#define pg_load_json_layout(JSON, LAYOUT) \
    pg_load_json_layout_((JSON), (LAYOUT), __LINE__, __FILE__)

