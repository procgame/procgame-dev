#pragma once

#define DEBUG_ASSETS true

#define PG_ASSET_TYPE_NAME_MAXLEN 256
#define PG_ASSET_NAME_MAXLEN 256

/************************/
/*  ASSET MANAGER       */
/************************/

/** Opaque interface to an asset manager    */
typedef struct pg_asset_manager pg_asset_manager_t;

/** Opaque interface to an asset    */
typedef struct pg_asset_handle {
    pg_asset_manager_t* mgr;
    uint64_t id;
} pg_asset_handle_t;

/** Opaque handle to a logical asset group  */
typedef int pg_asset_group_t;

/** Data interface to a particular type of asset.
    The asset manager keeps a table of asset loaders provided by the user,
    which are referenced by each individual asset to specify their data
    access interface.   */
struct pg_asset_loader {
    char type_name[PG_ASSET_TYPE_NAME_MAXLEN];
    int type_name_len;
    int counter;
    void* loader_data;
    void* (*parse)(pg_asset_handle_t asset, struct pg_asset_loader* loader, cJSON* source);
    void (*unparse)(pg_asset_handle_t asset, struct pg_asset_loader* loader, void* parsed_source);
    void* (*load)(pg_asset_handle_t asset, struct pg_asset_loader* loader, void* parsed_source);
    void (*unload)(pg_asset_handle_t asset, struct pg_asset_loader* loader, void* loaded_data);
    void (*destroy)(void* loaded_data);
};


/*  Create an asset loader which just loads JSON data   */
struct pg_asset_loader* pg_json_asset_loader(void);

/** Create a new asset manager  */
pg_asset_manager_t* pg_asset_manager_create(void);
/** Add a path to an asset manager's set of search paths    */
int pg_asset_manager_add_search_path(pg_asset_manager_t* asset_mgr, const char* search_path);
/** Set the search paths the asset manager will use when finding files  */
void pg_asset_manager_set_search_paths(pg_asset_manager_t* asset_mgr, const char** search_paths, int n_search_paths);
/** Get a pointer to an asset manager's search paths    */
const char** pg_asset_manager_get_search_paths(pg_asset_manager_t* asset_mgr, int* n_search_paths_out);
/** Load a file, searching the asset manager's search paths to find it  */
bool pg_asset_manager_load_file(pg_asset_manager_t* asset_mgr, struct pg_file* file,
                                const char* filename, const char* mode);
/** Destroy an asset manager and all its assets.    */
void pg_asset_manager_destroy(pg_asset_manager_t* asset_mgr);
/** Add an asset loader to an asset manager */
void pg_asset_manager_add_loader(pg_asset_manager_t* asset_mgr, struct pg_asset_loader* asset_loader);
/** Create a named group to assign assets to    */
pg_asset_group_t pg_asset_manager_add_group(pg_asset_manager_t* asset_mgr, const char* name);
/** Get an already created group by name    */
pg_asset_group_t pg_asset_manager_get_group(pg_asset_manager_t* asset_mgr, const char* name);
/** Delete all the assets in a group (and delete the group) */
void pg_asset_manager_delete_group(pg_asset_manager_t* asset_mgr, pg_asset_group_t group);
/** Get the loader associated with a given type name    */
struct pg_asset_loader* pg_asset_manager_get_type_loader(pg_asset_manager_t* asset_mgr,
                                                         const char* type);

void pg_asset_manager_set_variable(pg_asset_manager_t* asset_mgr, const char* name, pg_data_t data);
pg_data_t* pg_asset_manager_get_variable(pg_asset_manager_t* asset_mgr, const char* name);

vec4 pg_asset_manager_get_variable_vec4(pg_asset_manager_t* asset_mgr, const char* name);
vec3 pg_asset_manager_get_variable_vec3(pg_asset_manager_t* asset_mgr, const char* name);
vec2 pg_asset_manager_get_variable_vec2(pg_asset_manager_t* asset_mgr, const char* name);
float pg_asset_manager_get_variable_float(pg_asset_manager_t* asset_mgr, const char* name);
ivec4 pg_asset_manager_get_variable_ivec4(pg_asset_manager_t* asset_mgr, const char* name);
ivec3 pg_asset_manager_get_variable_ivec3(pg_asset_manager_t* asset_mgr, const char* name);
ivec2 pg_asset_manager_get_variable_ivec2(pg_asset_manager_t* asset_mgr, const char* name);
int pg_asset_manager_get_variable_int(pg_asset_manager_t* asset_mgr, const char* name);

int pg_asset_manager_read_int(pg_asset_manager_t* asset_mgr, cJSON* json);
vec4 pg_asset_manager_read_vec4(pg_asset_manager_t* asset_mgr, cJSON* json);
vec3 pg_asset_manager_read_vec3(pg_asset_manager_t* asset_mgr, cJSON* json);
vec2 pg_asset_manager_read_vec2(pg_asset_manager_t* asset_mgr, cJSON* json);
float pg_asset_manager_read_float(pg_asset_manager_t* asset_mgr, cJSON* json);
ivec4 pg_asset_manager_read_ivec4(pg_asset_manager_t* asset_mgr, cJSON* json);
ivec3 pg_asset_manager_read_ivec3(pg_asset_manager_t* asset_mgr, cJSON* json);
ivec2 pg_asset_manager_read_ivec2(pg_asset_manager_t* asset_mgr, cJSON* json);
int pg_asset_manager_read_int(pg_asset_manager_t* asset_mgr, cJSON* json);

/************************/
/*  ASSET CREATION      */
/************************/

/** Associate an asset name with an expected type, but without any data */
pg_asset_handle_t pg_asset_declare(pg_asset_manager_t* asset_mgr, const char* type,
                                   const char* name);
/** Declare an asset as a child of another (its name will be "parent_name.child_name"
    and it will depend on its parent)   */
pg_asset_handle_t pg_asset_declare_child(pg_asset_handle_t parent, const char* type,
        const char* child_name);
/** Get a previously declared child of an asset */
pg_asset_handle_t pg_asset_get_child(pg_asset_handle_t parent, const char* name);

/** Create a new asset from loaded JSON data.
    \remark This is best for loading asset dependencies, as it can take the asset's
            name from the JSON object (the '__pg_asset_name' member), while the
            others are more strict about their arguments. This function will
            also detect when the JSON object is only a string, and fall back to
            the 'from_AID' function. This way JSON assets can have inline asset
            dependency definitions, OR have only an AID, and the loader must only
            use this 'from_json' function to support both cases.    */
pg_asset_handle_t pg_asset_from_json(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, cJSON* json);
/** Create a new asset from a JSON file on disk  */
pg_asset_handle_t pg_asset_from_file(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, const char* filename);
/** Create a new asset from data in memory  */
pg_asset_handle_t pg_asset_from_data(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, void* data, bool own_data);
/** Create a new asset from an asset identifier (AID)
    \remark the AID format is like so: (type)name:path/to.json
    \remark the type and path segments are optional */
pg_asset_handle_t pg_asset_from_AID(pg_asset_manager_t* asset_mgr, const char* string, const char* expect_type);

int pg_asset_multiple_from_json(pg_asset_manager_t* asset_mgr, const char* expect_type, cJSON* json,
        pg_asset_handle_t* handles_out, int max_handles);
int pg_asset_multiple_from_file(pg_asset_manager_t* asset_mgr, const char* expect_type, const char* filename,
        pg_asset_handle_t* handles_out, int max_handles);



/************************/
/*  ASSET MANAGEMENT    */
/************************/

/** Assign an asset to a group  */
void pg_asset_assign_group(pg_asset_handle_t asset, pg_asset_group_t group);
/** Mark an asset as static (it won't be deleted, even if its last dependent is deleted)    */
void pg_asset_static(pg_asset_handle_t asset, bool is_static);
/** Add a dependency to an asset    */
void pg_asset_depends(pg_asset_handle_t asset, pg_asset_handle_t dependency);
bool pg_asset_dependency_from_json(pg_asset_handle_t dependent, pg_asset_handle_t* dependency_out,
        const char* type, const char* name, cJSON* json);
/** Delete an asset completely and forget about it  */
void pg_asset_delete(pg_asset_handle_t asset);



/************************/
/*  ASSET INSPECTION    */
/************************/

/** Check if an asset still exists  */
bool pg_asset_exists(pg_asset_handle_t asset);
/** Get the handle of an asset by name  */
pg_asset_handle_t pg_asset_get_handle(pg_asset_manager_t* asset_mgr, const char* name);
/** Get the JSON source for an asset   */
cJSON* pg_asset_get_source(pg_asset_handle_t asset);
/** Get the handle of an asset by name  */
const char* pg_asset_get_name(pg_asset_handle_t asset);
/** Get loaded binary data from an asset    */
void* pg_asset_get_data(pg_asset_handle_t asset);
/** Get loaded binary data from an asset by name    */
void* pg_asset_get_data_by_name(pg_asset_manager_t* asset_mgr, const char* name);



/************************/
/*  ASSET HELPERS       */
/************************/

/**  Can be passed as a required type for JSON layouts  */
#define PG_JSON_ASSET       (cJSON_Object | cJSON_String)
#define PG_JSON_VARIABLE    (cJSON_Array | cJSON_String | cJSON_Number)

#define pg_asset_log(LOG_LEVEL, ASSET, MSG, ...) pg_asset_log_(LOG_LEVEL, __LINE__, __FILE__, ASSET, MSG, ## __VA_ARGS__);
void pg_asset_log_(enum pg_log_type log_level, int src_line, const char* src_file,
        pg_asset_handle_t asset, const char* fmt, ...);

#define pg_asset_log_reverse(LOG_LEVEL, ASSET, MSG, ...) pg_asset_log_reverse_(LOG_LEVEL, __LINE__, __FILE__, ASSET, MSG, ## __VA_ARGS__);
void pg_asset_log_reverse_(enum pg_log_type log_level, int src_line, const char* src_file,
        pg_asset_handle_t asset, const char* fmt, ...);

