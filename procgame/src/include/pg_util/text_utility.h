/**
    \file text_utility.h
    \ingroup pg_util
    \brief Text management utilities
*/

#pragma once

#include "pg_core/arr.h"
#include "pg_core/htable.h"

/** @name String searching  */
/** Find first char in a string that matches any char in chr array  */
int pg_strchr(const char* str, int str_len, const char* chr, int chr_len);
/** Find first char in a string that DOESN'T match any char in chr array    */
int pg_strchr_neg(const char* str, int str_len, const char* chr, int chr_len);
/** Find first occurrence of a substring in a string    */
int pg_str_find(const char *str, int str_len, const char *search, int search_len);

/** @name String tokenization
    String tokenization state   */
struct pg_text_tokenizer {
    const char* str, *seps;
    int str_len, seps_len;
    const char* token;
    int token_len, token_idx, token_start, token_end;
};

/** Generate a tokenizer state for a given string and separators
 *  @addtogroup ConstructionMacros  */
#define PG_TEXT_TOKENIZER(STR, STR_LEN, SEPS, SEPS_LEN) \
    ((struct pg_text_tokenizer){ \
        .str = (STR), .str_len = (STR_LEN), \
        .seps = (SEPS), .seps_len = (SEPS_LEN), \
        .token = (STR), .token_idx = -1 })

/** Advance to the next token   */
bool pg_text_tokenizer_next(struct pg_text_tokenizer* tok);


/** @name Text preprocessing    */

/** Maximum length a text variant's name can have   */
#define PG_TEXT_VARIANT_MAX_NAME_LENGTH     64

/** Single range of text within a string    */
struct pg_text_range { int begin, len; };
typedef ARR_T(struct pg_text_range) pg_text_range_arr_t;

/** A single variant, including all of its ranges in the text   */
struct pg_text_variant {
    /** Ranges (in lines, not chars) associated with this variant  */
    pg_text_range_arr_t lines;
    /** Identifier of this variant  */
    char name[PG_TEXT_VARIANT_MAX_NAME_LENGTH];
};

typedef ARR_T(struct pg_text_variant) pg_text_variant_arr_t;

/** Map of variations in a text */
struct pg_text_variator {
    /** Source string   */
    const char* str;
    /** Length of the source string */
    int str_len;
    /** Range in the string for every line  */
    pg_text_range_arr_t lines;
    /** Variant covering all the lines not covered by any explicit variant  */
    struct pg_text_variant base_variant;
    /** All variants in the source string   */
    pg_text_variant_arr_t variants;
    /** Hash table mapping variant names to indices in the variants array    */
    int_table_t map;
};

/** Create a text variator from a source string and the given begin/end tokens    */
void pg_text_variator_init(struct pg_text_variator* var,
                           const char* str, int str_len,
                           const char* begin_token, const char* end_token);
/** Deinit a text variator (required to avoid memory leaks) */
void pg_text_variator_deinit(struct pg_text_variator* var);
/** Generate a new string which is a variant of the original string */
int pg_text_variator_generate(struct pg_text_variator* var,
                              char* out, int out_cap,
                              const char** variants, int n_variants);
/** Get the number of variants allowed by a variator    */
int pg_text_variator_count(struct pg_text_variator* var);
/** Get the name of a variant in a variator */
const char* pg_text_variator_name(struct pg_text_variator* var, int idx);

