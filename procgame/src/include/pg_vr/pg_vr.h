#pragma once

#include "pg_gfx/transform.h"

/*  Global VR init  */
bool pg_vr_init(void);
bool pg_vr_init_fake(void);
void pg_vr_deinit(void);
bool pg_have_vr(void);
bool pg_vr_is_fake(void);



/************************/
/*  OpenVR binding      */
struct VR_IVRSystem_FnTable;
struct VR_IVRCompositor_FnTable;
struct VR_IVRInput_FnTable;

struct pg_vr_context {
    struct VR_IVRSystem_FnTable* system;
    struct VR_IVRCompositor_FnTable* compositor;
    struct VR_IVRInput_FnTable* input;
};

struct pg_vr_context* pg_vr_context(void);



/****************/
/*  Display     */
struct pg_vr_display {
    ivec2 eye_resolution;
    pg_gpu_texture_t* eye_depth[2];
    pg_gpu_texture_t* eye_textures[2];
    pg_gpu_framebuffer_t* eye_framebuffers[2];
};

bool pg_vr_display_init(vec2 render_scale);
void pg_vr_display_deinit(void);
struct pg_vr_display* pg_vr_get_display(void);
void pg_vr_display_submit(void);
mat4 pg_vr_display_get_eye_projection(int eye, vec2 near_far);
mat4 pg_vr_display_get_eye_transform(int eye);

void pg_vr_add_display_to_asset_manager(struct pg_asset_manager* asset_mgr);



/****************/
/*  Devices     */
#define PG_VR_DEVICE_STRING_MAXLEN  128
#define PG_VR_MAX_DEVICES   64
struct pg_vr_device {
    int hw_idx;
    int set_idx;
    char model_number[PG_VR_DEVICE_STRING_MAXLEN];
    char serial_number[PG_VR_DEVICE_STRING_MAXLEN];
    char mfg_name[PG_VR_DEVICE_STRING_MAXLEN];
    enum pg_vr_device_type {
        PG_VR_NO_DEVICE,
        PG_VR_HEADSET,
        PG_VR_CONTROLLER,
        PG_VR_BASE_STATION,
    } type;

    /*  Tracked position    */
    mat4 tx;
    struct pg_gfx_transform pg_tx;

    /*  Controls    */
    struct pg_input_wrapper* controls;
};

typedef SARR_T(PG_VR_MAX_DEVICES, struct pg_vr_device) pg_vr_device_set_t;

bool pg_vr_devices_init(void);
void pg_vr_devices_deinit(void);
pg_vr_device_set_t* pg_vr_get_devices(void);
struct pg_vr_device* pg_vr_get_device(int idx);
int pg_vr_get_hmd_index(void);
int pg_vr_get_controller_index(int hand);



/****************/
/*  Tracking    */
void pg_vr_track_devices(void);
void pg_vr_get_device_pose(int idx, mat4 origin, mat4* tx_out, vec3* dir_out, vec3* pos_out);

struct pg_gfx_transform pg_vr_get_device_tx(int idx);
void pg_vr_set_fake_device_tx(int idx, vec3 pos, quat orientation);



/****************/
/*  Controls    */
struct pg_vr_action_binding {
    int control_idx;
    char vr_action_name[256];
    enum pg_vr_input_type {
        PG_VR_INPUT_BOOLEAN,
        PG_VR_INPUT_ANALOG,
    } type;
    uint64_t vr_action_id;
};

#define PG_VR_BINDINGS(...) PG_POINTERS(struct pg_vr_action_binding, __VA_ARGS__)
#define PG_VR_BINDING_BOOLEAN(CONTROL, ACTION_NAME) \
    (struct pg_vr_action_binding){ \
        .control_idx = (CONTROL), \
        .vr_action_name = (ACTION_NAME), \
        .type = PG_VR_INPUT_BOOLEAN \
    }
#define PG_VR_BINDING_ANALOG(CONTROL, ACTION_NAME) \
    (struct pg_vr_action_binding){ \
        .control_idx = (CONTROL), \
        .vr_action_name = (ACTION_NAME), \
        .type = PG_VR_INPUT_ANALOG \
    }


struct pg_vr_action_set {
    char vr_action_set_name[256];
    uint64_t vr_action_set_id;
    SARR_T(64, struct pg_vr_action_binding) bindings;
    bool enabled;
};

void pg_vr_set_action_manifest(char* path);
void pg_vr_action_set_bindings(char* set_name, int n_bindings, struct pg_vr_action_binding* bindings);
void pg_vr_action_set_enable(char* name, bool enabled);
void pg_vr_poll_input(struct pg_input_wrapper* controls_out);
