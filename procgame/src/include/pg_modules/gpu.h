/**
    \file pg_gpu.h
    \brief Procgame GPU Module
*/

#pragma once
#define PG_GPU

/**
    \defgroup pg_gpu GPU
    \ingroup pg_modules
    \{
*/

#undef PG_GPU_INIT
#undef PG_GPU_DEINIT
#define PG_GPU_INIT    pg_init_gpu()
#define PG_GPU_DEINIT  pg_deinit_gpu()

/** Initialize a GPU rendering context  */
bool pg_init_gpu(void);
/** Deinitialize the GPU rendering context  */
bool pg_deinit_gpu(void);
/** Check if we have a GPU rendering context    */
bool pg_have_gpu(void);

/** \}  */

