/**
    \file pg_core.h
    \brief Required to initialize any other modules
*/

#pragma once
#define PG_CORE

#include "pg_core/pg_core.h"

/**
    \defgroup pg_core Procgame Core
    \ingroup pg_modules
    \{
*/

#undef PG_CORE_INIT
#undef PG_CORE_DEINIT
#define PG_CORE_INIT    pg_init_core()
#define PG_CORE_DEINIT  pg_deinit_core()

/** Start procgame partially - only initialize the core requirements    */
bool pg_init_core(void);
/** Deinitialize the procgame core requirements */
bool pg_deinit_core(void);
/** Check if procgame core requirements are initialized */
bool pg_have_core(void);

/** \}  */
