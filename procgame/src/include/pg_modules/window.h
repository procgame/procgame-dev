/**
    \file pg_window
    \brief Procgame Window Module
*/

#pragma once
#define PG_WINDOW

#include "pg_core/pg_core.h"
#include "pg_math/linmath.h"

/**
    \defgroup pg_module_window Window
    \ingroup pg_modules
    \{
*/

#undef PG_WINDOW_INIT
#undef PG_WINDOW_DEINIT
#define PG_WINDOW_INIT     pg_init_window(ivec2(640,480))
#define PG_WINDOW_DEINIT   pg_deinit_window()

/** \brief Start procgame partially - only create a window  */
bool pg_init_window(ivec2 size);
/** \brief Destroy window and GL context    */
bool pg_deinit_window(void);
/** \brief Return true if the video system was started successfully */
bool pg_have_window(void);

/** \brief Resize an already opened window  */
void pg_window_resize(int w, int h);
/** \brief Make an opened window fullscreen */
void pg_window_fullscreen(void);
/** \brief Get the size of the opened window    */
ivec2 pg_window_size(void);
/** \brief Get the aspect ratio of the opened window    */
float pg_window_aspect(void);
/** \brief Swap the window buffers to show the next frame   */
void pg_window_swap(void);

/** \brief Get the SDL handle for the opened window */
SDL_Window* pg_window_handle(void);

/** \}  */

