/**
    \file pg_modules.h
    Module integration
*/

#pragma once
#define PG_MODULES

/*  Unavailable modules simply return true for everything   */
#ifndef PG_CORE
#define PG_CORE_INIT        true
#define PG_CORE_DEINIT      true
#endif

#ifndef PG_WINDOW
#define PG_WINDOW_INIT      true
#define PG_WINDOW_DEINIT    true
#endif

#ifndef PG_GPU
#define PG_GPU_INIT         true
#define PG_GPU_DEINIT       true
#endif

/** Initialize all available procgame modules (and stop of one fails)   */
#define pg_init_all() \
({ \
    bool r = PG_CORE_INIT; \
    r = r && PG_WINDOW_INIT; \
    r = r && PG_GPU_INIT; \
    if(!r) pg_log(PG_LOG_ERROR, "Errors while starting up"); \
    r; \
})

/** Deinitialize all available procgame modules in reverse order (and don't stop if one fails)  */
#define pg_deinit_all() \
({ \
    bool r = PG_GPU_DEINIT; \
    r = PG_WINDOW_DEINIT && r; \
    r = PG_CORE_DEINIT && r; \
    if(!r) pg_log(PG_LOG_ERROR, "Errors while shutting down"); \
    r; \
})
