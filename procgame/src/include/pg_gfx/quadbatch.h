#include <assert.h>

/****************************************/
/*  Unified sprite/text batching shader */
/****************************************/

/*  Convenience structures which are converted to 1 (or more) ezquads when
    finally added to the batch  */
struct pg_draw_2d {
    union {
        vec2 uv[2];
        vec4 uv_v;
    };
    int tex_layer;
    vec3 pos;
    vec2 scale;
    float rotation;
    vec4 color_mul, color_add;
};

struct pg_draw_text {
    /*  Can take a font, formatter (optional), and string, to format the
        string on the fly   */
    struct pg_fontset* fonts;
    struct pg_text_formatter* formatter;
    wchar_t* str_w;
    char* str;
    int len;
    /*  Or take already-formatted text  */
    struct pg_text_form* form;
    /*  Transforms done after formatting    */
    vec4 color;
    vec3 pos;
    vec2 scale;
    quat orientation;
    vec2 anchor;
};

/*  renderdoc
vec3 pos; vec2 scale;
rgb ubyte4 color_add; rgb ubyte4 color_mul;
ushort tex_layer; ushort tex_select;
vec4 uv; vec4 quat;
*/

struct pg_ezquad {
    vec3 pos;
    vec2 scale;
    uint32_t color_add;
    uint32_t color_mul;
    uint16_t tex_layer;
    uint16_t tex_select;
    union {
        vec2 uv[2];
        vec4 uv_v;
    };
    quat orientation;
};

static_assert(sizeof(struct pg_ezquad) == 64, "pg_ezquad should be 64 bytes");

#define PG_EZDRAW_2D(...) \
    ((struct pg_draw_2d){ \
        .uv = { {{ 0, 0 }}, {{ 1, 1 }} }, .tex_layer = 0, \
        .pos = {{ 0, 0, 0 }}, .scale = {{ 1, 1 }}, .rotation = 0, \
        .color_mul = {{ 1, 1, 1, 1 }}, .color_add = {{ 0, 0, 0, 0 }}, \
        __VA_ARGS__ })

#define PG_EZDRAW_2D_RECT(color, ...) \
    ((struct pg_draw_2d){ \
        .pos = {{ 0, 0 }}, .scale = {{ 1, 1 }}, .rotation = 0, \
        .color_mul = {{ 0, 0, 0, 0 }}, .color_add = color, \
        __VA_ARGS__ })

#define PG_EZDRAW_TEXT(...) \
    ((struct pg_draw_text){ \
        .pos = {{ 0, 0 }}, .scale = {{ 1, 1 }}, .orientation = {{ 0, 0, 0, 1 }}, \
        .color = {{ 1, 1, 1, 1 }}, __VA_ARGS__ })

#define PG_EZQUAD(...) \
    ((struct pg_ezquad){ \
        .scale = {{ 1, 1 }}, .orientation = {{ 0, 0, 0, 1 }}, \
        .uv = { {{ 0, 0 }}, {{ 1, 1 }} }, \
        .color_mul = 0xFFFFFFFF, __VA_ARGS__ })

struct pg_quadbatch {
    /*  Render stage to draw the quads (add to a renderer to draw the quads)    */
    struct pg_buffer quads_buffer;      /*  Local buffer for quad data  */
    pg_gpu_buffer_t* gpu_quads_buffer;  /*  GPU buffer for quad data    */
    uint32_t cur_start; /*  Index of the first quad in the current batch    */
    uint32_t cur_idx;   /*  Index of the current quad in the current batch  */
    mat4 cur_transform; /*  Transform for the current batch */
};

void pg_quadbatch_init(struct pg_quadbatch* batch, int size);
void pg_quadbatch_deinit(struct pg_quadbatch* batch);
/*  Add sprites or text to a current batch group    */
void pg_quadbatch_add_quad(struct pg_quadbatch* batch, const struct pg_ezquad* quad);
void pg_quadbatch_add_sprite(struct pg_quadbatch* batch, const struct pg_draw_2d* draw);
void pg_quadbatch_add_text(struct pg_quadbatch* batch, const struct pg_draw_text* draw);
/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_quadbatch_draw(struct pg_quadbatch* batch, pg_gpu_command_arr_t* commands);
#define pg_gpu_cmd_draw_quadbatch(QUADS) \
        pg_quadbatch_draw((QUADS), pg_gpu_command_recording_buffer);
/*  Begin a new batch group with a given model matrix   */
void pg_quadbatch_next(struct pg_quadbatch* batch, const mat4* transform);
/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_quadbatch_upload(struct pg_quadbatch* batch);
/*  Restart batching    */
void pg_quadbatch_reset(struct pg_quadbatch* batch);

pg_gpu_resource_set_t* pg_quadbatch_create_gpu_resources(struct pg_quadbatch* batch,
        pg_gpu_texture_t* images_tex, pg_gpu_texture_t* fonts_tex);

