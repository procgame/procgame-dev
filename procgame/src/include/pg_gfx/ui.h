#define PG_UI_NAME_MAXLEN 64

struct pg_ui_context;
struct pg_ui_content;
struct pg_ui_event;

typedef int pg_ui_t;
typedef ARR_T(pg_ui_t) pg_ui_arr_t;
typedef HTABLE_T(pg_ui_t) pg_ui_table_t;


/*  Callback function type  */
typedef int (*pg_ui_callback_t)(struct pg_ui_context*, pg_ui_t elem,
                                struct pg_ui_event* event);

#define PG_UI_CALLBACK(NAME) \
    int NAME(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
#define PG_UI_CONSUME_INPUT do { return 1; } while(0)
#define PG_UI_RETURN_INPUT do { return 0; } while(0)

enum pg_ui_callback_id {
    PG_UI_CLICK,
    PG_UI_HOLD,
    PG_UI_RELEASE,
    PG_UI_SCROLL,
    PG_UI_ENTER,
    PG_UI_LEAVE,
    PG_UI_UPDATE,
    PG_UI_ELEM_CALLBACKS,
};


/*  Which component should be used for interactivity    */
enum pg_ui_action_item {
    PG_UI_ACTION_NONE,
    PG_UI_ACTION_INDEPENDENT,
    PG_UI_ACTION_IMAGE,
    PG_UI_ACTION_TEXT,
};
enum pg_ui_action_item pg_ui_action_item_from_string(const char* str);


/*  Order to draw element text/image components */
enum pg_ui_draw_order {
    PG_UI_DRAW_NONE,
    PG_UI_TEXT_ONLY,
    PG_UI_IMAGE_ONLY,
    PG_UI_TEXT_THEN_IMAGE,
    PG_UI_IMAGE_THEN_TEXT,
};
enum pg_ui_draw_order pg_ui_draw_order_from_string(const char* str);


/*  Animate-able properties */
enum pg_ui_property_id {
    PG_UI_POS,
    PG_UI_SCALE,
    PG_UI_ROTATION,
    PG_UI_CLIP_POS,
    PG_UI_CLIP_SCALE,
    PG_UI_GROUP_PROPERTIES,
    PG_UI_IMAGE_POS = PG_UI_GROUP_PROPERTIES,
    PG_UI_IMAGE_SCALE,
    PG_UI_IMAGE_ROTATION,
    PG_UI_IMAGE_COLOR_MUL,
    PG_UI_IMAGE_COLOR_ADD,
    PG_UI_TEXT_POS,
    PG_UI_TEXT_SCALE,
    PG_UI_TEXT_ROTATION,
    PG_UI_TEXT_COLOR,
    PG_UI_ACTION_POS,
    PG_UI_ACTION_SCALE,
    PG_UI_3D,
    PG_UI_RESOLUTION,
    PG_UI_ELEM_PROPERTIES,
};


/*  Initialization structure containing all the info to create a new UI group or element   */
struct pg_ui_properties {
    /*  General */
    bool enabled;
    bool fix_aspect;

    /*  Group properties    */
    vec2 resolution;
    bool enable_3d;
    bool enable_clip;
    vec3 clip_pos;
    vec3 clip_scale;

    /*  General spatial properties  */
    vec3 pos;
    vec3 scale;
    float rotation;
    float layer;

    /*  Element properties    */
    enum pg_ui_draw_order draw_order;
    enum pg_ui_action_item action_item;
    /*  Image   */
    float image_rotation;
    vec3 image_pos;
    vec3 image_scale;
    vec4 image_color_mul, image_color_add;
    /*  Text    */
    float text_rotation;
    vec2 text_rotation_anchor;
    vec3 text_pos;
    vec3 text_scale;
    vec4 text_color;
    /*  Action area */
    vec3 action_pos;
    vec3 action_scale;

    /*  Image frame/sequence info   */
    const char* image_name;
    const char* image_frame;
    const char* image_sequence;
    struct pg_image_stepper image_anim;

    /*  Text info   */
    vec2 text_anchor;
    const wchar_t* text_w;
    const char* text;
    int text_len;
    vec2 text_size;
    struct pg_text_formatter text_formatter;

    /*  Initial animations  */
    const struct pg_animation* anim[PG_UI_ELEM_PROPERTIES];

    /*  Callbacks   */
    pg_ui_callback_t cb_click, cb_hold, cb_release, cb_scroll,
                     cb_enter, cb_leave, cb_update;
};

#define PG_UI_PROPERTIES(...) \
    (&(struct pg_ui_properties){ .enabled = true, \
        .clip_scale = {{ 1, 1, 1 }}, .scale = {{ 1, 1, 1 }}, \
        .text_size = {{ 1, 1 }}, .text_scale = {{ 1, 1, 1 }}, \
        .image_scale = {{ 1, 1, 1 }}, .action_scale = {{ 1, 1, 1 }}, \
        .text_size = {{ 1, 1 }}, .text_color = {{ 1, 1, 1, 1 }}, \
        .image_color_mul = {{ 1, 1, 1, 1 }}, \
        .action_item = PG_UI_ACTION_NONE, .draw_order = PG_UI_DRAW_NONE, \
        __VA_ARGS__ })



/*  User input events to be consumed by UI elements */
struct pg_ui_event {
    union {
        struct {
            enum {
                PG_UI_MOUSE_CLICK,
                PG_UI_MOUSE_HOLD,
                PG_UI_MOUSE_RELEASE,
                PG_UI_MOUSE_SCROLL,
            } action;
            enum {
                PG_UI_MOUSE_LEFT,
                PG_UI_MOUSE_MIDDLE,
                PG_UI_MOUSE_RIGHT,
            } button;
            int scroll_direction;
        } mouse;
        struct {
            enum {
                PG_UI_HOVER_ENTER,
                PG_UI_HOVER_STAY,
                PG_UI_HOVER_LEAVE,
            } action;
        } hover;
    };
};



/*  UI Context for managing all the groups, elements, and rendering */
struct pg_ui_context {
    /*  Contents    */
    ARR_T(struct pg_ui_content) content_pool;
    HTABLE_T(pg_ui_callback_t) callback_table;
    pg_ui_t root;
    struct pg_gfx_transform origin_3d;

    /*  Rendering   */
    struct pg_quadbatch quadbatch;
    pg_gpu_resource_set_t* resources;
    pg_gpu_render_stage_t* render_stage;

    /*  Assets  */
    struct pg_imageset* images;
    struct pg_fontset* fonts;
    struct pg_text_formatter default_text_formatter;

    /*  Graphics info   */
    enum pg_ui_render_mode {
        PG_UI_RENDER_2D,
        PG_UI_RENDER_3D,
        PG_UI_RENDER_VR,
    } render_mode;
    float ar;
    mat4 screen_mat;
    mat4 projview_3d;
    mat4 projview_eye[2];

    /*  Timing  */
    float time;
    float time_delta;

    /*  Input state */
    vec3 mouse_pos;
    uint8_t mouse_ctrl;
    int mouse_scroll;
    vec3 cursor_3d_pos;
    vec3 cursor_3d_dir;

    /*  Debug   */
    bool debug_action_areas;
    bool debug_containers;
    bool debug_all;
};


/*  Helper for easily referencing a UI object and its context   */
struct pg_ui_content_reference {
    struct pg_ui_context* ctx;
    pg_ui_t ui;
};
#define PG_UI_CONTENT_REFERENCE(CTX, UI) (struct pg_ui_content_reference){ .ctx = (CTX), .ui = (UI) }





/************************/
/*  PUBLIC INTERFACE    */
/************************/

/*  Context management functions   */
void pg_ui_context_init(struct pg_ui_context* ctx, struct pg_imageset* images, struct pg_fontset* fonts);
void pg_ui_context_deinit(struct pg_ui_context* ui);
void pg_ui_context_register_callback(struct pg_ui_context* ctx, const char* name, pg_ui_callback_t cb);
pg_ui_callback_t pg_ui_context_get_callback(struct pg_ui_context* ctx, const char* name);

/*  Rendering functions */
void pg_ui_context_set_render_stage(struct pg_ui_context* ctx, pg_gpu_render_stage_t* stage);
float pg_ui_context_get_aspect_ratio(struct pg_ui_context* ctx);
void pg_ui_context_set_resolution(struct pg_ui_context* ctx, vec2 res);
void pg_ui_context_set_view_3D(struct pg_ui_context* ui, mat4 projview);
void pg_ui_context_set_view_VR(struct pg_ui_context* ui, mat4 projview_left, mat4 projview_right);

/*  Building UI hierarchy   */
pg_ui_t pg_ui_context_root(struct pg_ui_context* ctx);
pg_ui_t pg_ui_add_group(struct pg_ui_context* ctx, pg_ui_t parent, const char* name, struct pg_ui_properties* props);
pg_ui_t pg_ui_add_element(struct pg_ui_context* ctx, pg_ui_t parent, const char* name, struct pg_ui_properties* props);
void pg_ui_delete(struct pg_ui_context* ctx, pg_ui_t del);

/*  Main loop functions */
void pg_ui_context_set_origin_3D(struct pg_ui_context* ctx, vec3 origin, vec3 scale, quat rot);
void pg_ui_context_set_cursor_3D(struct pg_ui_context* ui, vec3 pt, vec3 dir);
void pg_ui_context_step(struct pg_ui_context* ctx);
void pg_ui_context_animate(struct pg_ui_context* ui, float time);
void pg_ui_context_draw(struct pg_ui_context* ui);



/*  UI group/element functions  */
/*  Options */
void pg_ui_set_enabled(struct pg_ui_context* ctx, pg_ui_t ui_ref, int enabled);
void pg_ui_set_callback(struct pg_ui_context* ctx, pg_ui_t dst, enum pg_ui_callback_id id, pg_ui_callback_t cb);

/*  Relationships   */
pg_ui_t pg_ui_get_child(struct pg_ui_context* ctx, pg_ui_t parent, const char* name);
pg_ui_t pg_ui_get_child_path(struct pg_ui_context* ctx, pg_ui_t parent, const char* name);
pg_ui_t pg_ui_get_parent(struct pg_ui_context* ctx, pg_ui_t child);
const char* pg_ui_get_name(struct pg_ui_context* ctx, pg_ui_t ui_ref);

/*  Graphical options   */
void pg_ui_set_draw_order(struct pg_ui_context* ctx, pg_ui_t ui_ref, enum pg_ui_draw_order order);
void pg_ui_set_text(struct pg_ui_context* ctx, pg_ui_t ui_ref, const char* text, int n);
void pg_ui_set_text_w(struct pg_ui_context* ctx, pg_ui_t ui_ref, const wchar_t* text, int n);
void pg_ui_set_text_formatter(struct pg_ui_context* ctx, pg_ui_t ui_ref, struct pg_text_formatter* formatter);
void pg_ui_set_image_stepper(struct pg_ui_context* ctx, pg_ui_t ui_ref, struct pg_image_stepper* anim);

/*  Query   */
vec2 pg_ui_get_mouse_pos(struct pg_ui_context* ctx, pg_ui_t ui_ref);
struct pg_animator* pg_ui_get_property(struct pg_ui_context* ctx, pg_ui_t ui_ref, enum pg_ui_property_id prop_id);
pg_data_t* pg_ui_get_variable(struct pg_ui_context* ctx, pg_ui_t ui_ref, char* name);
