


/*  Keeps a list of handles and holds user-provided data and processing callback
    to do whatever operations are necessary to render this processor's handles   */
struct pg_gfx_render_processor {
    uint64_t type;
    ARR_T(uint64_t) active_handles;
    ARR_T(uint64_t) free_handles;
    int n_handles;
    void* udata;

    uint64_t (*create_handle)(void* udata, void** handle_data_out);
    void* (*get_handle_data)(void* udata, uint64_t handle_id);
    void (*delete_handle)(void* udata, uint64_t handle_id);
    void (*process_handle)(void* udata, uint64_t handle_id);
};





/*  An object which can have handles in multiple render processors, and has
    a user-provided callback for updating its handles    */
struct pg_gfx_render_object {
    pg_mempool_id_t id;

    /*  Will be processed by processors with these types    */
    SARR_T(8, uint64_t) processor_types;
    SARR_T(8, struct pg_gfx_render_processor_handle {
        int processor_id;
        uint64_t processor_handle;
    }) handles;

    struct pg_gfx_transform tx;
    pg_data_t data;

    /*  User-provided function to update a processor handle based on the
        user-provided object data   */
    void (*update_handle)(struct pg_gfx_render_object* obj,
            struct pg_gfx_render_processor* processor, void* handle_data);
    void (*deinit)(struct pg_gfx_render_object* obj);
};

typedef void (*pg_gfx_render_object_initializer_t)(struct pg_gfx_render_object*);
PG_MEMPOOL_DECLARE(struct pg_gfx_render_object, pg_gfx_render_object);




/*  Manages associating render objects to processors and updating handle data   */
struct pg_gfx_render_manager {
    pg_gfx_render_object_pool_t object_pool;
    ARR_T(pg_gfx_render_object_id_t) active_objects;
    ARR_T(struct pg_gfx_render_processor*) processors;
};

void pg_gfx_render_manager_init(struct pg_gfx_render_manager* mgr);
void pg_gfx_render_manager_deinit(struct pg_gfx_render_manager* mgr);

void pg_gfx_render_manager_add_processor(struct pg_gfx_render_manager* mgr, struct pg_gfx_render_processor* processor);

pg_gfx_render_object_id_t pg_gfx_render_manager_new_object(struct pg_gfx_render_manager* mgr, pg_gfx_render_object_initializer_t obj_init);
void pg_gfx_render_manager_delete_object(struct pg_gfx_render_manager* mgr, pg_gfx_render_object_id_t obj_id);
struct pg_gfx_render_object* pg_gfx_render_manager_get_object(struct pg_gfx_render_manager* mgr, pg_gfx_render_object_id_t obj_id);

void pg_gfx_render_manager_update(struct pg_gfx_render_manager* mgr);
