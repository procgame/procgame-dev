/************************************/
/*  Keyframe animations             */
/************************************/

struct pg_anim_keyframe {
    bool absolute;
    vec4 value;
    pg_easing_func_t ease;
    float start;
};

#define PG_ANIM_MAX_KEYFRAMES     32
struct pg_animation {
    struct pg_anim_keyframe keyframes[PG_ANIM_MAX_KEYFRAMES];
    int num_frames;
    int loop_start;
};


#define PG_ANIMATION(...) \
    (struct pg_animation) { \
        .loop_start = -1, \
        __VA_ARGS__ \
    }

#define PG_ANIMATION_LOOP_FRAME(FRAME) .loop_start = (FRAME)

#define PG_ANIMATION_KEYFRAMES(N_FRAMES, ...) \
    .num_frames = (N_FRAMES), \
    .keyframes = { __VA_ARGS__ }

#define PG_KEYFRAME(START_TIME, EASE_FORWARD, ...) \
    (struct pg_anim_keyframe){ \
        .start = (START_TIME), \
        .ease = (EASE_FORWARD), \
        .value = {{ __VA_ARGS__ }} \
    }

#define PG_KEYFRAME_ABSOLUTE(START_TIME, EASE_FORWARD, ...) \
    (struct pg_anim_keyframe){ \
        .absolute = true, \
        .start = (START_TIME), \
        .ease = (EASE_FORWARD), \
        .value = {{ __VA_ARGS__ }} \
    }


struct pg_animator {
    vec4 base_value;
    vec4 current_value;
    float progress;
    int current_frame;
    struct pg_animation anim;
};

void pg_animator_set(struct pg_animator* prop, const struct pg_animation* anim);
vec4 pg_animator_step(struct pg_animator* prop, float step);

/************************************/
/*  Simple animations               */
/************************************/

struct pg_simple_anim {
    int absolute;
    vec4 start, end;
    float start_time, duration;
    pg_easing_func_t ease;
};

#define PG_SIMPLE_ANIM(...) (struct pg_simple_anim) { \
    .ease = pg_ease_inout_lerp, \
    .start_time = -1, .duration = 1.0f, \
    __VA_ARGS__ }

vec4 pg_simple_anim_value(struct pg_simple_anim* anim, float t);
int pg_simple_anim_finished(struct pg_simple_anim* anim, float t);
