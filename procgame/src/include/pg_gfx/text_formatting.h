/************************/
/*  Text formatting     */
/************************/


/*  A text formatter that can be used to create text forms out of strings,
    formatted with alignment, multiple fonts (with inline selection),
    scaling, wrapping, coloring, etc.   */
struct pg_text_formatter {
    struct pg_fontset* fonts;
    int default_font;
    vec2 region;
    float htab;
    vec2 size;
    vec2 spacing;
    uint32_t color_palette[8];
    float last_line_justify_threshold;
    enum pg_text_alignment {
        PG_TEXT_LEFT, PG_TEXT_CENTER, PG_TEXT_RIGHT, PG_TEXT_JUSTIFY
    } align;
    enum pg_text_wrapping {
        PG_TEXT_NO_WRAP, PG_TEXT_WRAP_LETTERS, PG_TEXT_WRAP_WORDS
    } wrap;
};

#define PG_TEXT_FORMATTER(FONTS, ...) (struct pg_text_formatter){ \
    .fonts = (FONTS), \
    .region = {{0,0}}, .htab = 0, .size = {{1,1}}, .spacing = {{1,1}}, \
    .align = PG_TEXT_LEFT, .wrap = PG_TEXT_NO_WRAP, \
    .last_line_justify_threshold = 0.75, \
    .color_palette = { 0xFFFFFFFF, 0x7F7F7FFF, 0x000000FF, \
                       0xFF0000FF, 0x00FF00FF, 0x0000FFFF, \
                       0x000000FF, 0x000000FF }, \
    __VA_ARGS__ }

#define PG_TEXT_FORMATTER_MOD(fmt, ...) (struct pg_text_formatter){ \
    .fonts = (fmt).fonts, .default_font = (fmt).default_font, \
    .region = (fmt).region, .htab = (fmt).htab, .size = (fmt).size, \
    .spacing = (fmt).spacing, .align = (fmt).align, .wrap = (fmt).wrap, \
    .last_line_justify_threshold = (fmt).last_line_justify_threshold, \
    .color_palette = { (fmt).color_palette[0], (fmt).color_palette[1], \
        (fmt).color_palette[2], (fmt).color_palette[3], (fmt).color_palette[4], \
        (fmt).color_palette[5], (fmt).color_palette[6], (fmt).color_palette[7] }, \
    __VA_ARGS__ }


/*  A list of glyphs, already typeset by a formatter.
    The coordinate space is in PIXELS (so will be different depending on the
    SIZE the font was loaded at!)   */
struct pg_text_form {
    vec2 area[2];
    struct pg_text_form_glyph {
        vec2 pos, scale, uv0, uv1;
        uint32_t color; uint8_t ctrl_flags; uint8_t tex_layer;
    } *glyphs;
    int n_glyphs, max_glyphs;
    int own_alloc;
};


void pg_text_form_init_alloc(struct pg_text_form* form, int n);
void pg_text_form_init_ptr(struct pg_text_form* form, struct pg_text_form_glyph* glyphs, int n);
void pg_text_form_deinit(struct pg_text_form* form);
void pg_text_form_alloc_reserve(struct pg_text_form* form, int n);

void pg_text_format_w(struct pg_text_form* form, struct pg_text_formatter* fmt,
                    const wchar_t* str, int len);
void pg_text_format(struct pg_text_form* form, struct pg_text_formatter* fmt,
                      const char* str, int len);

