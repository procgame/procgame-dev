struct pg_asset_loader* pg_quadbatch_asset_loader(void);
struct pg_asset_loader* pg_fontset_asset_loader(void);
struct pg_asset_loader* pg_imageset_asset_loader(void);
struct pg_asset_loader* pg_armature_asset_loader(void);
struct pg_asset_loader* pg_ui_context_asset_loader(void);
struct pg_asset_loader* pg_ui_group_asset_loader(void);

void pg_gfx_asset_loaders(pg_asset_manager_t* asset_mgr);
