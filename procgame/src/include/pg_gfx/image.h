/*  Image Assets

    A way to manage images in a logical manner. This allows full image asset
    configuration at run-time using JSON. A thorough demonstration of the
    asset format is in the files `image.json` and `imageset.json`.
  
    Image assets have 'frames', which are named sub-rectangles of the image.
    They also have 'sequences', which are named sets of frames, with associated
    duration information. These form a database for producing animated image
    frame slide-shows out of data which can be reconfigured at runtime.

    Because string lookups for every frame would be inefficient, it is
    recommended to cache the values returned from the lookup functions whenever
    possible. For animations, a pg_image_stepper structure is provided as
    a small interface to manage individual animated objects without requiring
    any lookups after initialization.

    Imgsets allow multiple images to be treated as one image with many layers.
    This means images smaller than the rest may end up in a layer with padding
    (generally 0x00000000) to fill the unused space. That image will also have
    all its frames' uv coords fixed to account for the padding, automatically.
    This is the difference between the `uv_pad` and `uv_raw` coordinate spaces;
    `uv_pad` refers to the UV space of the single image asset, and
    `uv_raw` refers to the UV space of the whole texture layer.
    So those frames were transformed from `uv_pad` space to `uv_raw` space.

    There are also grid cells, which is just a user-defined 2D factor for
    dividing an image so grid cells align to integer coordinates. It can be
    defined in terms of either `px` or `uv_pad` coordinates.

    This means images have four different coordinate systems to manage:
    * Pixel coordinates (`px`)
    * UV coordinates before accounting for padding  (`uv_pad`)
        * This is the space used in the JSON configuration files as "uv"
    * UV coordinates after accounting for padding (`uv_raw`)
    * Grid cell coordinates (`cell`)

    Here is an example of a frame's coordinates in those four coordinate spaces,
    Given an image with these dimensions:
        Image size (128,128), Texture size (256,256), Cell size (32,32)px
    `px`        (32,32) -> (64,64)
    `uv_pad`    (0.25,0.25) -> (0.5,0.5)
    `uv_raw`    (0.125,0.125) -> (0.25,0.25)
    `grid`      (1,1) -> (2,2)
*/

#define PG_IMAGE_NAME_MAXLEN    64

/*  A named sub-rectangle of the image. */
struct pg_image_frame {
    char name[PG_IMAGE_NAME_MAXLEN];
    /*  Pixel coordinates, texture coordinates, cell coordinates [min,max]  */
    vec2 px[2], grid[2];
    /*  Raw UV texture frame    */
    pg_tex_frame_t uv_raw;
};

/*  Frame indices and start time of each frame, relative to the whole sequence  */
struct pg_image_sequence {
    char name[PG_IMAGE_NAME_MAXLEN];
    SARR_T(32, struct pg_image_sequence_frame {
        int frame;
        float start;
        float duration;
    }) frames;
    float full_duration;
};

/*  Image assets. Just one image in a texture   */
struct pg_image {
    /************************/
    /*  Configured data     */
    char img_filename[1024];
    vec2 grid_px, grid_uv, grid_cells;
    ARR_T(struct pg_image_frame) frames;
    HTABLE_T(int) frames_table;
    ARR_T(struct pg_image_sequence) seqs;
    HTABLE_T(int) seqs_table;
    /************************/
    /*  Run-time data       */
    /*  img_size:   Dimensions of the pixel data
        uv_size:    UV dimensions of the pixel data within the texture (the
                        image may be smaller than texture dimensions)   */
    ivec2 img_size;
    vec2 uv_size;
    /*  Texture management  */
    struct pg_texture* tex;
    pg_gpu_texture_t* gpu_tex;
    bool own_tex, own_gpu_tex;
    int tex_layer;
};

/*  Imgset assets. A little wrapper that can keep images in texture layers. */
struct pg_imageset {
    /*  Configured data (just a bunch of pg_images, really)   */
    ARR_T(struct pg_image) imgs;
    HTABLE_T(int) imgs_table;
    /*  Texture management  */
    struct pg_texture* tex;
    pg_gpu_texture_t* gpu_tex;
    bool own_tex, own_gpu_tex;
};

/****************/
/*  Images      */
/****************/
void pg_image_init(struct pg_image* image, const char* src_filename, ivec2 img_size,
        struct pg_texture* tex, bool own_tex, int tex_layer);
void pg_image_deinit(struct pg_image* image);

/*  Image interface    */
struct pg_texture* pg_image_get_texture(struct pg_image* image);
pg_gpu_texture_t* pg_image_get_gpu_texture(struct pg_image* image);
pg_tex_frame_t pg_image_get_grid_cell(struct pg_image* image, int x, int y);
pg_tex_frame_t pg_image_get_frame(struct pg_image* image, int idx);
pg_tex_frame_t pg_image_get_frame_by_name(struct pg_image* image, const char* frame_name);
struct pg_image_sequence* pg_image_get_sequence(struct pg_image* image, int idx);
struct pg_image_sequence* pg_image_get_sequence_by_name(struct pg_image* image, const char* anim_name);

/****************/
/*  Imagesets   */
/****************/
void pg_imageset_init(struct pg_imageset* imageset, struct pg_texture* tex, bool own_tex);
void pg_imageset_deinit(struct pg_imageset* asset);

/*  Imageset interface  */
struct pg_texture* pg_imageset_get_texture(struct pg_imageset* imageset);
pg_gpu_texture_t* pg_imageset_get_gpu_texture(struct pg_imageset* imageset);
struct pg_image* pg_imageset_get_image(struct pg_imageset* imageset, const char* image_name);

/********************/
/*  Image steppers  */
/*  A "stepper" is a little tracker object for single animated items.
    It can be "stepped" through any sequence in discrete timesteps, and get
    a raw texframe from the sequence conveniently.
    Steppers that haven't been stepped (or which have, I suppose) can be cached
    and copied around later to avoid string look-ups if you want. A stepper is
    self-contained and has no dynamic allocations so copying requires no extra
    thought for memory management.  */

struct pg_image_stepper {
    int img_idx;
    int seq_idx;
    int cur_step;
    float seq_progress;
    pg_tex_frame_t uv;
};

void pg_image_make_stepper(struct pg_image* asset,
                           struct pg_image_stepper* stepper,
                           const char* seq_name);
void pg_image_make_stepper_single(struct pg_image* asset,
                                  struct pg_image_stepper* stepper,
                                  const char* frame_name);
void pg_image_step(struct pg_image* asset,
                   struct pg_image_stepper* stepper, float step);
void pg_image_stepper_set_time(struct pg_image* asset,
                               struct pg_image_stepper* stepper, float time);

void pg_imageset_make_stepper(struct pg_imageset* asset,
                                  struct pg_image_stepper* stepper,
                                  const char* img_name, const char* seq_name);
void pg_imageset_make_stepper_single(struct pg_imageset* asset,
                                     struct pg_image_stepper* stepper,
                                     const char* img_name, const char* frame_name);
void pg_imageset_step(struct pg_imageset* asset,
                      struct pg_image_stepper* stepper, float step);
void pg_imageset_stepper_set_time(struct pg_imageset* asset,
                                  struct pg_image_stepper* stepper, float time);
