#include <wchar.h>
#include "stb_truetype.h"

struct pg_font_glyph {
    vec2 uv[2];
    vec2 offset;
    vec2 scale;
    float xadvance;
};

struct pg_font_glyph_mapping {
    wchar_t ch;
    int idx;
};

struct pg_font {
    int size;
    float scale;
    float full_size;
    float ascent;
    float descent;
    float line_gap;
    float line_move;
    /*  TTF info    */
    char ttf_filename[1024];
    uint8_t* ttf;
    stbtt_fontinfo ttf_info;
    /*  Glyph packing context   */
    stbtt_pack_context pack_ctx;
    /*  Hash table mapping codepoints -> glyph array indices    */
    ARR_T(struct pg_font_glyph) chars;
    ARR_T(struct pg_font_glyph_mapping) char_buckets[256];
    /*  Texture management  */
    struct pg_texture* tex;
    pg_gpu_texture_t* gpu_tex;
    int tex_layer;
    bool own_tex;
    bool own_gpu_tex;
};

void pg_font_load(struct pg_font* font, const char* filename, int pt_size, vec2 tex_size);
void pg_font_load_search_paths(struct pg_font* font, const char* filename,
        int pt_size, vec2 tex_size,
        const char** search_paths, int n_search_paths);
void pg_font_deinit(struct pg_font* font);

struct pg_texture* pg_font_get_texture(struct pg_font* font);
pg_gpu_texture_t* pg_font_get_gpu_texture(struct pg_font* font);
int pg_font_load_chars(struct pg_font* font, const wchar_t* ch, int n);
int pg_font_get_char_index(struct pg_font* font, wchar_t ch);
const struct pg_font_glyph* pg_font_get_glyph(struct pg_font* font, wchar_t ch);
struct pg_font_glyph* pg_font_get_glyph_nc(struct pg_font* font, wchar_t ch);

struct pg_fontset {
    ARR_T(struct pg_font) fonts;
    /*  Texture management  */
    struct pg_texture* tex;
    pg_gpu_texture_t* gpu_tex;
};

void pg_fontset_load(struct pg_fontset* fonts, const char** filenames, int* sizes,
        int n_fonts, vec2 tex_size);

void pg_fontset_load_search_paths(struct pg_fontset* fonts,
        const char** filenames, int* sizes,
        int n_fonts, vec2 tex_size,
        const char** search_paths, int n_search_paths);

void pg_fontset_deinit(struct pg_fontset* fonts);

struct pg_texture* pg_fontset_get_texture(struct pg_fontset* fonts);
pg_gpu_texture_t* pg_fontset_get_gpu_texture(struct pg_fontset* fonts);
struct pg_font* pg_fontset_font(struct pg_fontset* grp, int i);

