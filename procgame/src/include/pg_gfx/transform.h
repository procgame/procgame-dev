#pragma once

#include "pg_math/math_utility.h"

/*  A transformation of a single UI group or element    */
struct pg_gfx_transform {
    vec3 pos;
    vec3 scale;
    quat rot;
    mat4 tx;
    mat4 tx_inverse;
};

void pg_gfx_create_transform(struct pg_gfx_transform* tx_out, vec3 pos, vec3 scale, quat orientation);
void pg_gfx_apply_transform(struct pg_gfx_transform* tx_out, const struct pg_gfx_transform* tx_local, const struct pg_gfx_transform* tx_parent);

struct pg_gfx_transform pg_gfx_transform_apply(const struct pg_gfx_transform* tx_local, const struct pg_gfx_transform* tx_parent);
struct pg_gfx_transform pg_gfx_transform(vec3 pos, vec3 scale, quat orientation);
struct pg_gfx_transform pg_gfx_transform_from_mat4(mat4 mat);

ray3D pg_gfx_transform_ray(const struct pg_gfx_transform* tx, vec3 local_dir);


