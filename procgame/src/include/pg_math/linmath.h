/*  Linear math library originally by github user datenwolf, under the
    Do What The Fuck You Want To Public License.

    This version is very heavily modified, to support vectors of various
    types. All types in this version are held in structs rather than typedef
    arrays, to allow using all the regular C copy/assignment syntax, and
    so pointers and passing to functions is more intuitive. All the base
    functions now take and return types *by value*, and are declared by
    default with the "always_inline" attribute to eliminate the performance
    penalty for doing so.   */


#ifndef LINMATH_H
#define LINMATH_H

#include <tgmath.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <float.h>

#include "linmath_swizzle.h"

#ifndef LM_FUNC
/*  By-value functions are always_inline by default */
#define LM_FUNC __attribute__((always_inline, warn_unused_result)) static inline
#endif

#ifndef LM_SCALAR
#define LM_SCALAR   double
#endif

/*  Basic constants */
#define LM_PI           (3.14159265359)
#define LM_PI_2         (1.57079632679)
#define LM_2_PI         (6.28318530718)
#define LM_TAU          (LM_2_PI)

#define LM_DEG_TO_RAD(DEG)  (DEG * (LM_PI/180))
#define LM_RAD_TO_DEG(RAD)  (RAD * (180/LM_PI))

/****************************/
/* Single value stuff       */
/****************************/

#define LM_MAX(a, b)            ((a) > (b) ? (a) : (b))
#define LM_MIN(a, b)            ((a) < (b) ? (a) : (b))

#define LM_MOD(a, b)                ((a) % (b) < 0 ? ((a) % (b)) + b : (a) % (b))
#define LM_MOD2(a, b0, b1)          (LM_MOD((a)-(b0), (b1)-(b0)) + (b0))
#define LM_MOD_DIFF(a, b, m)        ((abs((b)-(a)) < (m)/2) ? ((b)-(a)) : ((m) - fabs((b)-(a)) * LM_SGN((a)-(b))))
#define LM_MOD2_DIFF(a, b, m0, m1)  (LM_MOD_DIFF((a), (b), (m0) - (m1)))

#define LM_FMOD(a, b)               (fmod((a), (b)) < 0 ? (fmod((a), (b)) + (b)) : (fmod((a), (b))))
#define LM_FMOD2(a, b0, b1)         (LM_FMOD((a)-(b0), (b1)-(b0)) + (b0))
#define LM_FMOD_DIFF(a, b, m)       ((fabs((b)-(a)) < (m) * 0.5) ? ((b)-(a)) : ((m) - fabs((b)-(a)) * LM_SGN((a)-(b))))
#define LM_FMOD2_DIFF(a, b, m0, m1) (LM_FMOD_DIFF((a), (b), (m0) - (m1)))

#define LM_FFLOOR(a, b)             ((a) - LM_FMOD((a), (b)))
#define LM_FCEIL(a, b)              ((a) + (b) - LM_FMOD((a), (b)))
#define LM_FTRUNC(a, b)             ((a) - fmod((a), (b)))

#define LM_SGN(a)                   ((a) < 0 ? -1 : 1)
#define LM_SGN3(a)                  ((a) < 0 ? -1 : (a > 0 ? 1 : 0))

#define LM_CLAMP(x, a, b)           (LM_MIN(LM_MAX(x, (a)), b))
#define LM_SATURATE(x)              (LM_CLAMP(x, 0, 1))
#define LM_SATURATE2(x)             (LM_CLAMP(x, -1, 1))

#define LM_LERP(a, b, t)            ((1.0f - (t)) * (a) + (t) * (b))
LM_FUNC float smin(float a, float b, float k)
{
    float h = LM_CLAMP(0.5 + 0.5 * (b - a) / k, 0.0, 1.0);
    return LM_LERP(b, a, h) - k * h * (1.0 - h);
}

/****************************/
/* Vectors of various types */
/****************************/

#define LINMATH_DEFINE_VEC4(V, T, abs_fn, mod_fn) \
typedef union { T v[4]; struct { T x, y, z, w; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0, 0, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1, 0, 0 ); } \
LM_FUNC V V##_Z(void) { return V( 0, 0, 1, 0 ); } \
LM_FUNC V V##_W(void) { return V( 0, 0, 0, 1 ); } \
LM_FUNC V V##_X_neg(void) { return V( -1, 0, 0, 0 ); } \
LM_FUNC V V##_Y_neg(void) { return V( 0, -1, 0, 0 ); } \
LM_FUNC V V##_Z_neg(void) { return V( 0, 0, -1, 0 ); } \
LM_FUNC V V##_W_neg(void) { return V( 0, 0, 0, -1 ); } \
LM_FUNC V V##_all(float k) { return V( k, k, k, k ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y, a.z-b.z, a.w-b.w ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y, a.z*b.z, a.w*b.w ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y, a.z/b.z, a.w/b.w ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y, 1/a.z, 1/a.w ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k, a.z*k, a.w*k ); } \
LM_FUNC V V##_negative(V a) \
    { return V( -a.x, -a.y, -a.z, -a.w ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k, a.z*k, a.w*k ); } \
LM_FUNC V V##_tdiv(V a, T k) \
    { return V( a.x/k, a.y/k, a.z/k, a.w/k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0 && a.z==0 && a.w==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y && a.z==b.z && a.w==b.w); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c && a.z==c && a.w==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y && a.z<b.z && a.w<b.w); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c && a.z<c && a.w<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y && a.z<=b.z && a.w<=b.w); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c && a.z<=c && a.w<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y && a.z>b.z && a.w>b.w); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c && a.z>c && a.w>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y && a.z>=b.z && a.w>=b.w); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c && a.z>=c && a.w>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y), \
                LM_MAX(a.z,b.z), LM_MAX(a.w,b.w) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y), \
                LM_MIN(a.z,b.z), LM_MIN(a.w,b.w) ); } \
LM_FUNC V V##_tmax(V a, T b) \
    { return V( LM_MAX(a.x,b), LM_MAX(a.y,b), \
                LM_MAX(a.z,b), LM_MAX(a.w,b) ); } \
LM_FUNC V V##_tmin(V a, T b) \
    { return V( LM_MIN(a.x,b), LM_MIN(a.y,b), \
                LM_MIN(a.z,b), LM_MIN(a.w,b) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(LM_MAX(LM_MAX(a.x, a.y), a.z), a.w); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(LM_MIN(LM_MIN(a.x, a.y), a.z), a.w); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_scale(V##_add(a,b), 0.5); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y), LM_SGN(a.z), LM_SGN(a.w) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y), LM_SGN3(a.z), LM_SGN3(a.w) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y), ceil(a.z), ceil(a.w) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y), floor(a.z), floor(a.w) ); } \
LM_FUNC V V##_round(V a) \
    { return V( round(a.x), round(a.y) ); } \
LM_FUNC V V##_trunc(V a) \
    { return V( trunc(a.x), trunc(a.y) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y), \
                LM_CLAMP(a.z,b.z,c.z), LM_CLAMP(a.w,b.w,c.w) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c), \
                LM_CLAMP(a.z,b,c), LM_CLAMP(a.w,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y), \
                LM_SATURATE(a.z), LM_SATURATE(a.w) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y), \
                LM_SATURATE2(a.z), LM_SATURATE2(a.w) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y), abs_fn(a.z), abs_fn(a.w) ); } \
LM_FUNC T V##_distmax(V a, V b) \
    { return V##_vmax(V##_abs(V##_sub(a,b))); } \
\
/*  Modular arithmetic  */ \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y), \
                mod_fn(a.z, b.z), mod_fn(a.w, b.w) ); } \
LM_FUNC V V##_mod2(V a, V b0, V b1) \
    { return V( mod_fn(a.x-b0.x, b1.x-b0.x)+b0.x, \
                mod_fn(a.y-b0.y, b1.y-b0.y)+b0.y, \
                mod_fn(a.z-b0.z, b1.z-b0.z)+b0.z, \
                mod_fn(a.w-b0.w, b1.w-b0.w)+b0.w ); } \
LM_FUNC V V##_mod_diff(V a, V b, V m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m.x/2) ? (b.x-a.x) : (m.x - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m.y/2) ? (b.y-a.y) : (m.y - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))), \
        ((abs_fn(b.z-a.z) < m.z/2) ? (b.z-a.z) : (m.z - abs_fn(b.z-a.z) * LM_SGN(a.z-b.z))), \
        ((abs_fn(b.w-a.w) < m.w/2) ? (b.w-a.w) : (m.w - abs_fn(b.w-a.w) * LM_SGN(a.w-b.w))) ); } \
LM_FUNC V V##_mod2_diff(V a, V b, V m0, V m1) \
    { return V##_mod_diff(a, b, V##_sub(m1,m0)); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b), \
                mod_fn(a.z, b), mod_fn(a.w, b) ); } \
LM_FUNC V V##_vmod2(V a, T b0, T b1) \
    { return V( mod_fn(a.x-b0, b1-b0)+b0, \
                mod_fn(a.y-b0, b1-b0)+b0, \
                mod_fn(a.z-b0, b1-b0)+b0, \
                mod_fn(a.w-b0, b1-b0)+b0 ); } \
LM_FUNC V V##_vmod_diff(V a, V b, T m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m/2) ? (b.x-a.x) : (m - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m/2) ? (b.y-a.y) : (m - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))), \
        ((abs_fn(b.z-a.z) < m/2) ? (b.z-a.z) : (m - abs_fn(b.z-a.z) * LM_SGN(a.z-b.z))), \
        ((abs_fn(b.w-a.w) < m/2) ? (b.w-a.w) : (m - abs_fn(b.w-a.w) * LM_SGN(a.w-b.w))) ); } \
LM_FUNC V V##_vmod2_diff(V a, V b, T m0, T m1) \
    { return V##_vmod_diff(a, b, m1-m0); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k), \
                LM_LERP(a.z, b.z, k), LM_LERP(a.w, b.w, k) ); } \

#define dvec4(...)  ((dvec4){ .v = { __VA_ARGS__ } })
#define vec4(...)   ((vec4){ .v = { __VA_ARGS__ } })
#define ivec4(...)  ((ivec4){ .v = { __VA_ARGS__ } })
#define uvec4(...)  ((uvec4){ .v = { __VA_ARGS__ } })
#define svec4(...)  ((svec4){ .v = { __VA_ARGS__ } })
#define usvec4(...)  ((usvec4){ .v = { __VA_ARGS__ } })
#define bvec4(...)  ((bvec4){ .v = { __VA_ARGS__ } })
#define ubvec4(...) ((ubvec4){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC4(dvec4, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC4(vec4, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC4(ivec4, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(uvec4, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(svec4, int16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(usvec4, uint16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(bvec4, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(ubvec4, uint8_t, abs, LM_MOD)

#define LINMATH_DEFINE_VEC3(V, T, abs_fn, mod_fn) \
typedef union { T v[3]; struct { T x, y, z; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1, 0 ); } \
LM_FUNC V V##_Z(void) { return V( 0, 0, 1 ); } \
LM_FUNC V V##_X_neg(void) { return V( -1, 0, 0 ); } \
LM_FUNC V V##_Y_neg(void) { return V( 0, -1, 0 ); } \
LM_FUNC V V##_Z_neg(void) { return V( 0, 0, -1 ); } \
LM_FUNC V V##_all(float k) { return V( k, k, k ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y, a.z+b.z ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y, a.z-b.z ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y, a.z*b.z ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y, a.z/b.z ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y, 1/a.z ); } \
LM_FUNC V V##_negative(V a) \
    { return V( -a.x, -a.y, -a.z ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k, a.z*k ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k, a.z*k ); } \
LM_FUNC V V##_tdiv(V a, T k) \
    { return V( a.x/k, a.y/k, a.z/k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0 && a.z==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y && a.z==b.z); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c && a.z==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y && a.z<b.z); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c && a.z<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y && a.z<=b.z); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c && a.z<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y && a.z>b.z); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c && a.z>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y && a.z>=b.z); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c && a.z>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y), \
                  LM_MAX(a.z,b.z) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y), \
                  LM_MIN(a.z,b.z) ); } \
LM_FUNC V V##_tmax(V a, T b) \
    { return V( LM_MAX(a.x,b), LM_MAX(a.y,b), \
                LM_MAX(a.z,b) ); } \
LM_FUNC V V##_tmin(V a, T b) \
    { return V( LM_MIN(a.x,b), LM_MIN(a.y,b), \
                LM_MIN(a.z,b) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(LM_MAX(a.x, a.y), a.z); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(LM_MIN(a.x, a.y), a.z); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y + a.z*b.z); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_scale(V##_add(a,b), 0.5); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y), LM_SGN(a.z) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y), LM_SGN3(a.z) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y), ceil(a.z) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y), floor(a.z) ); } \
LM_FUNC V V##_round(V a) \
    { return V( round(a.x), round(a.y) ); } \
LM_FUNC V V##_trunc(V a) \
    { return V( trunc(a.x), trunc(a.y) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y), \
                LM_CLAMP(a.z,b.z,c.z) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c), \
                LM_CLAMP(a.z,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y), \
                LM_SATURATE(a.z) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y), \
                LM_SATURATE2(a.z) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y), abs_fn(a.z) ); } \
LM_FUNC T V##_distmax(V a, V b) \
    { return V##_vmax(V##_abs(V##_sub(a,b))); } \
\
/*  Modular arithmetic  */ \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y), \
                mod_fn(a.z, b.z) ); } \
LM_FUNC V V##_mod2(V a, V b0, V b1) \
    { return V( mod_fn(a.x-b0.x, b1.x-b0.x)+b0.x, \
                mod_fn(a.y-b0.y, b1.y-b0.y)+b0.y, \
                mod_fn(a.z-b0.z, b1.z-b0.z)+b0.z ); } \
LM_FUNC V V##_mod_diff(V a, V b, V m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m.x/2) ? (b.x-a.x) : (m.x - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m.y/2) ? (b.y-a.y) : (m.y - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))), \
        ((abs_fn(b.z-a.z) < m.z/2) ? (b.z-a.z) : (m.z - abs_fn(b.z-a.z) * LM_SGN(a.z-b.z))) ); } \
LM_FUNC V V##_mod2_diff(V a, V b, V m0, V m1) \
    { return V##_mod_diff(a, b, V##_sub(m1,m0)); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b), \
                mod_fn(a.z, b) ); } \
LM_FUNC V V##_vmod2(V a, T b0, T b1) \
    { return V( mod_fn(a.x-b0, b1-b0)+b0, \
                mod_fn(a.y-b0, b1-b0)+b0, \
                mod_fn(a.z-b0, b1-b0)+b0 ); } \
LM_FUNC V V##_vmod_diff(V a, V b, T m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m/2) ? (b.x-a.x) : (m - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m/2) ? (b.y-a.y) : (m - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))), \
        ((abs_fn(b.z-a.z) < m/2) ? (b.z-a.z) : (m - abs_fn(b.z-a.z) * LM_SGN(a.z-b.z))) ); } \
LM_FUNC V V##_vmod2_diff(V a, V b, T m0, T m1) \
    { return V##_vmod_diff(a, b, m1 - m0); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k), \
                LM_LERP(a.z, b.z, k) ); } \
LM_FUNC V V##_cross(V a, V b) \
    { return V( a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x ); } \
LM_FUNC V V##_reflect(V d, V n) \
    { return V##_sub(d, V##_scale(n, 2 * V##_dot(d, n))); }  \
LM_FUNC V V##_from_sph(LM_SCALAR x, LM_SCALAR y) \
{ \
    LM_SCALAR s_x = sin(x), s_y = sin(y), c_x = cos(x), c_y = cos(y); \
    return V( (s_y * s_x), (s_y * c_x), -c_y ); \
}


#define dvec3(...)  ((dvec3){ .v = { __VA_ARGS__ } })
#define vec3(...)   ((vec3){ .v = { __VA_ARGS__ } })
#define ivec3(...)  ((ivec3){ .v = { __VA_ARGS__ } })
#define uvec3(...)  ((uvec3){ .v = { __VA_ARGS__ } })
#define svec3(...)  ((svec3){ .v = { __VA_ARGS__ } })
#define usvec3(...)  ((usvec3){ .v = { __VA_ARGS__ } })
#define bvec3(...)  ((bvec3){ .v = { __VA_ARGS__ } })
#define ubvec3(...) ((ubvec3){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC3(dvec3, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC3(vec3, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC3(ivec3, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(uvec3, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(svec3, int16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(usvec3, uint16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(bvec3, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(ubvec3, uint8_t, abs, LM_MOD)


#define LINMATH_DEFINE_VEC2(V, T, abs_fn, mod_fn) \
typedef union { T v[2]; struct { T x, y; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1 ); } \
LM_FUNC V V##_all(float k) { return V( k, k ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y ); } \
LM_FUNC V V##_negative(V a) \
    { return V( -a.x, -a.y ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k ); } \
LM_FUNC V V##_tdiv(V a, T k) \
    { return V( a.x/k, a.y/k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y) ); } \
LM_FUNC V V##_tmax(V a, T b) \
    { return V( LM_MAX(a.x,b), LM_MAX(a.y,b) ); } \
LM_FUNC V V##_tmin(V a, T b) \
    { return V( LM_MIN(a.x,b), LM_MIN(a.y,b) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(a.x, a.y); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(a.x, a.y); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_scale(V##_add(a,b), 0.5); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y) ); } \
LM_FUNC V V##_round(V a) \
    { return V( round(a.x), round(a.y) ); } \
LM_FUNC V V##_trunc(V a) \
    { return V( trunc(a.x), trunc(a.y) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y) ); } \
LM_FUNC T V##_distmax(V a, V b) \
    { return V##_vmax(V##_abs(V##_sub(a,b))); } \
\
/*  Modular arithmetic  */ \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y) ); } \
LM_FUNC V V##_mod2(V a, V b0, V b1) \
    { return V( mod_fn(a.x-b0.x, b1.x-b0.x)+b0.x, \
                mod_fn(a.y-b0.y, b1.y-b0.y)+b0.y ); } \
LM_FUNC V V##_mod_diff(V a, V b, V m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m.x/2) ? (b.x-a.x) : (m.x - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m.y/2) ? (b.y-a.y) : (m.y - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))) ); } \
LM_FUNC V V##_mod2_diff(V a, V b, V m0, V m1) \
    { return V##_mod_diff(a, b, V##_sub(m1,m0)); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b) ); } \
LM_FUNC V V##_vmod2(V a, T b0, T b1) \
    { return V( mod_fn(a.x-b0, b1-b0)+b0, mod_fn(a.y-b0, b1-b0)+b0 ); } \
LM_FUNC V V##_vmod_diff(V a, V b, T m) \
    { return V( \
        ((abs_fn(b.x-a.x) < m/2) ? (b.x-a.x) : (m - abs_fn(b.x-a.x) * LM_SGN(a.x-b.x))), \
        ((abs_fn(b.y-a.y) < m/2) ? (b.y-a.y) : (m - abs_fn(b.y-a.y) * LM_SGN(a.y-b.y))) ); } \
LM_FUNC V V##_vmod2_diff(V a, V b, T m0, T m1) \
    { return V##_vmod_diff(a, b, m1 - m0); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k) ); } \
LM_FUNC V V##_rotate(V a, LM_SCALAR k) \
    { LM_SCALAR c = cos(k), s = sin(k); return V( a.x*c - a.y*s, a.x*s + a.y*c ); } \
LM_FUNC V V##_reflect(V d, V n) \
    { return V##_sub(d, V##_scale(n, 2 * V##_dot(d, n))); } 

#define dvec2(...)  ((dvec2){ .v = { __VA_ARGS__ } })
#define vec2(...)   ((vec2){ .v = { __VA_ARGS__ } })
#define ivec2(...)  ((ivec2){ .v = { __VA_ARGS__ } })
#define uvec2(...)  ((uvec2){ .v = { __VA_ARGS__ } })
#define svec2(...)  ((svec2){ .v = { __VA_ARGS__ } })
#define usvec2(...)  ((usvec2){ .v = { __VA_ARGS__ } })
#define bvec2(...)  ((bvec2){ .v = { __VA_ARGS__ } })
#define ubvec2(...) ((ubvec2){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC2(dvec2, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC2(vec2, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC2(ivec2, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(uvec2, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(svec2, int16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(usvec2, uint16_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(bvec2, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(ubvec2, uint8_t, abs, LM_MOD)

/****************************/
/* Matrix & Quaternion      */
/****************************/

typedef union { float v[4]; struct { float x,y,z,w; }; } quat;
typedef union { float v[9]; vec3 col[3]; } mat3;
typedef union { float v[16]; vec4 col[4]; } mat4;

#define quat(...)   ((quat){ .v = { __VA_ARGS__ } })
#define mat3(...)   ((mat3){ .col = { __VA_ARGS__ } })
#define mat4(...)   ((mat4){ .col = { __VA_ARGS__ } })


#define MAT(M, C, R)    (M.col[R].v[C])
#define MATP(M, C, R)   (M->col[R].v[C])

LM_FUNC quat quat_identity(void)
{
    return quat( 0, 0, 0, 1.0f );
}
LM_FUNC quat quat_norm(quat a)
{
    vec4 qv = vec4_norm(vec4(VEC_XYZW(a)));
    return quat( VEC_XYZW(qv) );
}
LM_FUNC quat quat_lerp(quat a, quat b, float k)
{
    vec4 qv = vec4_lerp(vec4(VEC_XYZW(a)), vec4(VEC_XYZW(b)), k);
    return quat( VEC_XYZW(qv) );
}
LM_FUNC quat quat_mul(quat a, quat b)
{
    return quat(
        a.w*b.x + a.y*b.z - a.z*b.y + a.x*b.w,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z - a.y*b.x + a.z*b.w + a.x*b.y,
        a.w*b.w - a.y*b.y - a.z*b.z - a.x*b.x );
}
LM_FUNC quat quat_scale(quat a, float k)
{
    return quat( a.x*k, a.y*k, a.z* k, a.w*k );
}
LM_FUNC float quat_dot(quat a, quat b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}
LM_FUNC quat quat_conj(quat a)
{
    return quat( -a.x, -a.y, -a.z, a.w );
}
LM_FUNC quat quat_rotation(vec3 axis, float angle)
{
    vec3 v = vec3_scale(axis, sin(angle * 0.5));
    return quat( VEC_XYZ(v), cos(angle * 0.5) );
}
LM_FUNC quat quat_from_to(vec3 from, vec3 to)
{
    float r = vec3_dot(from, to) + 1.0f;
    vec3 w;
    if (r > 0) w = vec3_cross(from, to);
    else {
        r = 0.0f;
        if(fabs(from.x) > fabs(from.z)) w = vec3(-from.y, from.x, 0);
        else w = vec3(0, -from.z, from.y);
    }
    return quat_norm(quat( w.x, w.y, w.z, r ));
}
LM_FUNC vec3 quat_mul_vec3(quat q, vec3 v)
{
    vec3 q_xyz = vec3( q.x, q.y, q.z );
    vec3 t = vec3_scale(vec3_cross(q_xyz, v), 2);
    vec3 u = vec3_cross(q_xyz, t);
    t = vec3_scale(t, q.w);
    return vec3_add(vec3_add(v, t), u);
}
LM_FUNC vec4 quat_mul_vec4(quat q, vec4 v)
{
    vec3 q_xyz = vec3( q.x, q.y, q.z );
    vec3 v3 = vec3( v.x, v.y, v.z );
    vec3 t = vec3_scale(vec3_cross(q_xyz, v3), 2);
    vec3 u = vec3_cross(q_xyz, t);
    t = vec3_scale(t, q.w);
    vec3 p = vec3_add(vec3_add(v3, t), u);
    return vec4(VEC_XYZ(p), v.w);
}
LM_FUNC quat quat_from_sph(float x, float y)
{
    return quat_mul(quat_rotation(vec3_Z(), x), quat_rotation(vec3_X(), y));
}
LM_FUNC quat quat_euler(vec3 angles)
{
    return quat_mul( quat_rotation(vec3_X(), angles.x),
            quat_mul( quat_rotation(vec3_Y(), angles.y),
                       quat_rotation(vec3_Z(), angles.z) ) );
}
LM_FUNC quat quat_from_mat4(mat4 m)
{
    float t;
    quat q;
    if(m.col[2].v[2] < 0) {
        if(m.col[0].v[0] >m.col[1].v[1]) {
            t = 1 + m.col[0].v[0] -m.col[1].v[1] -m.col[2].v[2];
            q = quat( t, m.col[0].v[1]+m.col[1].v[0], m.col[2].v[0]+m.col[0].v[2], m.col[1].v[2]-m.col[2].v[1] );
        } else {
            t = 1 -m.col[0].v[0] + m.col[1].v[1] -m.col[2].v[2];
            q = quat( m.col[0].v[1]+m.col[1].v[0], t, m.col[1].v[2]+m.col[2].v[1], m.col[2].v[0]-m.col[0].v[2] );
        }
    } else {
        if(m.col[0].v[0] < -m.col[1].v[1]) {
            t = 1 -m.col[0].v[0] -m.col[1].v[1] + m.col[2].v[2];
            q = quat( m.col[2].v[0]+m.col[0].v[2], m.col[1].v[2]+m.col[2].v[1], t, m.col[0].v[1]-m.col[1].v[0] );
        } else {
            t = 1 + m.col[0].v[0] + m.col[1].v[1] + m.col[2].v[2];
            q = quat( m.col[1].v[2]-m.col[2].v[1], m.col[2].v[0]-m.col[0].v[2], m.col[0].v[1]-m.col[1].v[0], t );
        }
    }
    return quat_scale(q, 0.5 / sqrt(t));
}



LM_FUNC mat4 mat4_from_array(float* ptr)
{
    mat4 ret;
    for(int i = 0; i < 16; ++i) ret.v[i] = ptr[i];
    return ret;
}
LM_FUNC mat4 mat4_from_quat(quat q)
{
    float a = q.w;
    float b = q.x;
    float c = q.y;
    float d = q.z;
    float a2 = a*a;
    float b2 = b*b;
    float c2 = c*c;
    float d2 = d*d;
    return (mat4(
        vec4(    a2+b2-c2-d2, 2.0f*(b*c-a*d), 2.0f*(b*d+a*c), 0.0f ),
        vec4( 2.0f*(b*c+a*d),    a2-b2+c2-d2, 2.0f*(c*d-a*b), 0.0f ),
        vec4( 2.0f*(b*d-a*c), 2.0f*(c*d+a*b),    a2-b2-c2+d2, 0.0f ),
        vec4(              0,              0,              0, 1.0f ) ));
}
LM_FUNC mat4 mat4_mul_quat(mat4 m, quat q)
{
    return mat4( quat_mul_vec4(q, m.col[0]),
                 quat_mul_vec4(q, m.col[1]),
                 quat_mul_vec4(q, m.col[2]),
                 m.col[3] );
}
LM_FUNC quat quat_from_mat3(mat3 m)
{
    float t = 1 + m.v[0] + m.v[4] + m.v[8];
    if(t > FLT_EPSILON) {
        float s = sqrt(t) * 2;
        return quat( ( m.v[7] - m.v[5] ) / s,
                     ( m.v[2] - m.v[6] ) / s,
                     ( m.v[3] - m.v[1] ) / s,
                     0.25 * s );
    } else if ( m.v[0] > m.v[4] && m.v[0] > m.v[8] )  {
        float s = sqrt( 1.0 + m.v[0] - m.v[4] - m.v[8] ) * 2;
        return quat( 0.25 * s,
                     (m.v[3] + m.v[1] ) / s,
                     (m.v[2] + m.v[6] ) / s,
                     (m.v[7] - m.v[5] ) / s );
    } else if ( m.v[4] > m.v[8] ) {
        float s = sqrt( 1.0 + m.v[4] - m.v[0] - m.v[8] ) * 2;
        return quat( (m.v[3] + m.v[1] ) / s,
                     0.25 * s,
                     (m.v[7] + m.v[5] ) / s,
                     (m.v[2] - m.v[6] ) / s );
    } else {
        float s = sqrt( 1.0 + m.v[8] - m.v[0] - m.v[4] ) * 2;
        return quat( (m.v[2] + m.v[6] ) / s,
                     (m.v[7] + m.v[5] ) / s,
                     0.25 * s,
                     (m.v[3] - m.v[1] ) / s );
    }
}
LM_FUNC mat3 mat3_from_quat(quat q)
{
    float a = q.w;
    float b = q.x;
    float c = q.y;
    float d = q.z;
    float a2 = a*a;
    float b2 = b*b;
    float c2 = c*c;
    float d2 = d*d;
    return mat3(
        vec3(    a2+b2-c2-d2, 2.0f*(b*c+a*d), 2.0f*(b*d-a*c) ),
        vec3( 2.0f*(b*c-a*d),    a2-b2+c2-d2, 2.0f*(c*d+a*b) ),
        vec3( 2.0f*(b*d+a*c), 2.0f*(c*d-a*b),    a2-b2-c2+d2 ) );
}
LM_FUNC mat3 mat3_mul_quat(mat3 m, quat q)
{
    return mat3( quat_mul_vec3(q, m.col[0]),
                 quat_mul_vec3(q, m.col[1]),
                 quat_mul_vec3(q, m.col[2]));
}


/*  4x4 Matrix  */

LM_FUNC mat4 mat4_zero(void)
{
    return mat4( vec4(0), vec4(0), vec4(0), vec4(0) );
}
LM_FUNC mat4 mat4_identity(void)
{
    return mat4( vec4( 1, 0, 0, 0 ),
                 vec4( 0, 1, 0, 0 ),
                 vec4( 0, 0, 1, 0 ),
                 vec4( 0, 0, 0, 1 ) );
}
LM_FUNC vec4 mat4_col(mat4 m, int r)
{
    return m.col[r];
}
LM_FUNC vec4 mat4_row(mat4 m, int c)
{
    return vec4( m.col[0].v[c], m.col[1].v[c], m.col[2].v[c], m.col[3].v[c] );
}
LM_FUNC mat4 mat4_add(mat4 a, mat4 b)
{
    return mat4( vec4_add(a.col[0], b.col[0]), vec4_add(a.col[1], b.col[1]),
                 vec4_add(a.col[2], b.col[2]), vec4_add(a.col[3], b.col[3]) );
}
LM_FUNC mat4 mat4_sub(mat4 a, mat4 b)
{
    return mat4( vec4_sub(a.col[0], b.col[0]), vec4_sub(a.col[1], b.col[1]),
                 vec4_sub(a.col[2], b.col[2]), vec4_sub(a.col[3], b.col[3]) );
}
LM_FUNC mat4 mat4_mul(mat4 a, mat4 b)
{
    mat4 tmp = mat4_zero();
    int k, r, c;
    for(c=0; c<4; ++c) for(r=0; r<4; ++r) for(k=0; k<4; ++k)
        MAT(tmp, c, r) += MAT(a, c, k) * MAT(b, k, r);
    return tmp;
}
LM_FUNC vec4 mat4_mul_vec4(mat4 m, vec4 v)
{
    vec4 tmp = vec4(0,0,0,0);
    int i, j;
    for(j=0; j<4; ++j) for(i=0; i<4; ++i)
        tmp.v[j] += MAT(m, j, i) * v.v[i];
    return tmp;
}
LM_FUNC vec3 mat4_mul_vec3(mat4 m, vec3 v, bool translate)
{
    vec4 tmp_in = vec4(VEC_XYZ(v),translate ? 1.0f : 0.0f);
    vec4 tmp_out = vec4(0,0,0,0);
    int i, j;
    for(j=0; j<4; ++j) for(i=0; i<4; ++i)
        tmp_out.v[j] += MAT(m, j, i) * tmp_in.v[i];
    return vec3(VEC_XYZ(tmp_out));
}
LM_FUNC mat4 mat4_translation(vec3 t)
{
    return mat4( vec4( 1, 0, 0, 0 ),
                 vec4( 0, 1, 0, 0 ),
                 vec4( 0, 0, 1, 0 ),
                 vec4( t.x, t.y, t.z, 1 ) );
}
LM_FUNC mat4 mat4_rotation(vec3 axis, float angle)
{
    quat r = quat_rotation(axis, angle);
    return mat4_mul_quat(mat4_identity(), r);
}
LM_FUNC mat4 mat4_scaling(vec3 scale)
{
    return mat4( vec4( scale.x, 0, 0, 0 ),
                 vec4( 0, scale.y, 0, 0 ),
                 vec4( 0, 0, scale.z, 0 ),
                 vec4( 0, 0, 0, 1 ) );
}
LM_FUNC mat4 mat4_translate(vec3 t, mat4 m)
{
    vec4 t_ = vec4(t.x, t.y, t.z, 0);
    return mat4( m.col[0], m.col[1], m.col[2],
        vec4( MAT(m, 3, 0) + vec4_dot(mat4_row(m, 0), t_),
              MAT(m, 3, 1) + vec4_dot(mat4_row(m, 1), t_),
              MAT(m, 3, 2) + vec4_dot(mat4_row(m, 2), t_),
              MAT(m, 3, 3) + vec4_dot(mat4_row(m, 3), t_) ) );
}
LM_FUNC mat4 mat4_rotate(vec3 axis, float angle, mat4 m)
{
    quat r = quat_rotation(axis, angle);
    return mat4_mul_quat(m, r);
}
LM_FUNC mat4 mat4_scale(float k, mat4 m)
{
    return mat4( vec4_scale(m.col[0], k), vec4_scale(m.col[1], k),
                 vec4_scale(m.col[2], k), m.col[3] );
}
LM_FUNC mat4 mat4_scale_aniso(vec3 s, mat4 m)
{
    return mat4( vec4_scale(m.col[0], s.x), vec4_scale(m.col[1], s.y),
                 vec4_scale(m.col[2], s.z), m.col[3] );
}
LM_FUNC mat4 mat4_euler(vec3 angles)
{
    return mat4_rotate( vec3_Y(), angles.x,
            mat4_rotate( vec3_X(), angles.y,
             mat4_rotate( vec3_Z(), angles.z, mat4_identity() ) ) );
}
LM_FUNC mat4 mat4_frustum(float l, float r, float b, float t, float n, float f)
{
    return mat4(
        vec4( 2.0f*n/(r-l),             0,                  0,     0 ),
        vec4(            0,  2.0f*n/(t-b),                  0,     0 ),
        vec4(  (r+l)/(r-l),   (t+b)/(t-b),       -(f+n)/(f-n), -1.0f ),
        vec4(            0,             0,  -2.0f*(f*n)/(f-n),  0.0f ) );
}
LM_FUNC mat4 mat4_ortho(float l, float r, float b, float t, float n, float f)
{
    return mat4(
        vec4(   2.0f/(r-l),             0,             0,  0 ),
        vec4(            0,    2.0f/(t-b),             0,  0 ),
        vec4(            0,             0,   -2.0f/(f-n),  0 ),
        vec4( -(r+l)/(r-l),  -(t+b)/(t-b),  -(f+n)/(f-n),  1.0f ) );
}
LM_FUNC mat4 mat4_perspective(float y_fov, float aspect, float n, float f)
{
    float a = 1.0f / tan(y_fov / 2.0f);
    return mat4(
        vec4(  a/aspect,  0,                  0,      0 ),
        vec4(         0,  a,                  0,      0 ),
        vec4(         0,  0,       -(f+n)/(f-n),  -1.0f ),
        vec4(         0,  0,  -(2.0f*f*n)/(f-n),      0 ) );
}
LM_FUNC mat4 mat4_look_at(vec3 eye, vec3 center, vec3 up)
{
    vec3 f = vec3_norm(vec3_sub(center, eye));
    vec3 s = vec3_norm(vec3_cross(f, up));
    vec3 t = vec3_cross(s, f);
    mat4 look = mat4( vec4( s.x,  t.x,  -f.x,  0.0f ),
                      vec4( s.y,  t.y,  -f.y,  0.0f ),
                      vec4( s.z,  t.z,  -f.z,  0.0f ),
                      vec4(   0,    0,     0,  1.0f ) );
    return mat4_translate(vec3_scale(eye, -1), look);
}
LM_FUNC mat4 mat4_transpose(mat4 m)
{
    mat4 tmp;
    int i, j;
    for(j=0; j<4; ++j) for(i=0; i<4; ++i)
        tmp.col[i].v[j] = m.col[j].v[i];
    return tmp;
}
LM_FUNC mat4 mat4_inverse(mat4 m)
{
    float s[6];
    float c[6];
    s[0] = MAT(m, 0, 0)*MAT(m, 1, 1) - MAT(m, 1, 0)*MAT(m, 0, 1);
    s[1] = MAT(m, 0, 0)*MAT(m, 1, 2) - MAT(m, 1, 0)*MAT(m, 0, 2);
    s[2] = MAT(m, 0, 0)*MAT(m, 1, 3) - MAT(m, 1, 0)*MAT(m, 0, 3);
    s[3] = MAT(m, 0, 1)*MAT(m, 1, 2) - MAT(m, 1, 1)*MAT(m, 0, 2);
    s[4] = MAT(m, 0, 1)*MAT(m, 1, 3) - MAT(m, 1, 1)*MAT(m, 0, 3);
    s[5] = MAT(m, 0, 2)*MAT(m, 1, 3) - MAT(m, 1, 2)*MAT(m, 0, 3);
    c[0] = MAT(m, 2, 0)*MAT(m, 3, 1) - MAT(m, 3, 0)*MAT(m, 2, 1);
    c[1] = MAT(m, 2, 0)*MAT(m, 3, 2) - MAT(m, 3, 0)*MAT(m, 2, 2);
    c[2] = MAT(m, 2, 0)*MAT(m, 3, 3) - MAT(m, 3, 0)*MAT(m, 2, 3);
    c[3] = MAT(m, 2, 1)*MAT(m, 3, 2) - MAT(m, 3, 1)*MAT(m, 2, 2);
    c[4] = MAT(m, 2, 1)*MAT(m, 3, 3) - MAT(m, 3, 1)*MAT(m, 2, 3);
    c[5] = MAT(m, 2, 2)*MAT(m, 3, 3) - MAT(m, 3, 2)*MAT(m, 2, 3);
    /* Assumes it is invertible */
    float idet = 1.0f/( s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0] );
    mat4 ret;
    MAT(ret, 0, 0) = ( MAT(m, 1, 1) * c[5] - MAT(m, 1, 2) * c[4] + MAT(m, 1, 3) * c[3]) * idet;
    MAT(ret, 0, 1) = (-MAT(m, 0, 1) * c[5] + MAT(m, 0, 2) * c[4] - MAT(m, 0, 3) * c[3]) * idet;
    MAT(ret, 0, 2) = ( MAT(m, 3, 1) * s[5] - MAT(m, 3, 2) * s[4] + MAT(m, 3, 3) * s[3]) * idet;
    MAT(ret, 0, 3) = (-MAT(m, 2, 1) * s[5] + MAT(m, 2, 2) * s[4] - MAT(m, 2, 3) * s[3]) * idet;
    MAT(ret, 1, 0) = (-MAT(m, 1, 0) * c[5] + MAT(m, 1, 2) * c[2] - MAT(m, 1, 3) * c[1]) * idet;
    MAT(ret, 1, 1) = ( MAT(m, 0, 0) * c[5] - MAT(m, 0, 2) * c[2] + MAT(m, 0, 3) * c[1]) * idet;
    MAT(ret, 1, 2) = (-MAT(m, 3, 0) * s[5] + MAT(m, 3, 2) * s[2] - MAT(m, 3, 3) * s[1]) * idet;
    MAT(ret, 1, 3) = ( MAT(m, 2, 0) * s[5] - MAT(m, 2, 2) * s[2] + MAT(m, 2, 3) * s[1]) * idet;
    MAT(ret, 2, 0) = ( MAT(m, 1, 0) * c[4] - MAT(m, 1, 1) * c[2] + MAT(m, 1, 3) * c[0]) * idet;
    MAT(ret, 2, 1) = (-MAT(m, 0, 0) * c[4] + MAT(m, 0, 1) * c[2] - MAT(m, 0, 3) * c[0]) * idet;
    MAT(ret, 2, 2) = ( MAT(m, 3, 0) * s[4] - MAT(m, 3, 1) * s[2] + MAT(m, 3, 3) * s[0]) * idet;
    MAT(ret, 2, 3) = (-MAT(m, 2, 0) * s[4] + MAT(m, 2, 1) * s[2] - MAT(m, 2, 3) * s[0]) * idet;
    MAT(ret, 3, 0) = (-MAT(m, 1, 0) * c[3] + MAT(m, 1, 1) * c[1] - MAT(m, 1, 2) * c[0]) * idet;
    MAT(ret, 3, 1) = ( MAT(m, 0, 0) * c[3] - MAT(m, 0, 1) * c[1] + MAT(m, 0, 2) * c[0]) * idet;
    MAT(ret, 3, 2) = (-MAT(m, 3, 0) * s[3] + MAT(m, 3, 1) * s[1] - MAT(m, 3, 2) * s[0]) * idet;
    MAT(ret, 3, 3) = ( MAT(m, 2, 0) * s[3] - MAT(m, 2, 1) * s[1] + MAT(m, 2, 2) * s[0]) * idet;
    return ret;
}
LM_FUNC mat4 mat4_orthonormalize(mat4 m)
{
    float s = 1;
    vec3 r[3] = { vec3(VEC_XYZ(m.col[0])),
                  vec3(VEC_XYZ(m.col[1])),
                  vec3(VEC_XYZ(m.col[2])) };
    vec3 h = vec3_norm(r[2]);
    r[2] = vec3_norm(r[2]);
    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[2] = vec3_norm(r[2]);
    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[1] = vec3_norm(r[1]);
    s = vec3_dot(r[0], r[1]);
    h = vec3_scale(r[1], s);
    r[0] = vec3_sub(r[0], h);
    r[0] = vec3_norm(r[0]);
    return mat4( vec4(VEC_XYZ(r[0]), m.col[0].v[3]),
                 vec4(VEC_XYZ(r[1]), m.col[1].v[3]),
                 vec4(VEC_XYZ(r[2]), m.col[2].v[3]),
                 m.col[3] );
}
LM_FUNC mat4 mat4_object_space(vec3 pos, vec3 scale, quat orientation)
{
    mat4 t = mat4_translation(pos);
    mat4 s = mat4_scaling(scale);
    mat4 r = mat4_from_quat(orientation);
    return mat4_mul(t, mat4_mul(s, r));
}

LM_FUNC mat4 mat4_object_space_inverse(vec3 pos, vec3 scale, quat orientation)
{
    mat4 t = mat4_translation(vec3_scale(pos,-1));
    mat4 s = mat4_scaling(vec3_recip(scale));
    mat4 r = mat4_from_quat(quat_conj(orientation));
    return mat4_mul(r, mat4_mul(s, t));
}


LM_FUNC quat quat_sph_from_mat4(mat4 m)
{
    vec3 p = mat4_mul_vec3(m, vec3(0,1,0), false);
    float x_angle = atan2(p.x, -p.y);
    float y_angle = asin(-p.z);
    return quat_mul(
        quat_rotation(vec3_Z(), x_angle),
        quat_rotation(vec3_X(), y_angle)
    );
}


/*  3x3 Matrix  */

LM_FUNC mat3 mat3_zero(void)
{
    return mat3( vec3(0), vec3(0), vec3(0) );
}
LM_FUNC mat3 mat3_identity(void)
{
    return mat3( vec3( 1, 0, 0 ),
                 vec3( 0, 1, 0 ),
                 vec3( 0, 0, 1 ) );
}
LM_FUNC vec3 mat3_col(mat3 m, int r)
{
    return m.col[r];
}
LM_FUNC vec3 mat3_row(mat3 m, int c)
{
    return vec3( m.col[0].v[c], m.col[1].v[c], m.col[2].v[c] );
}
LM_FUNC mat3 mat3_add(mat3 a, mat3 b)
{
    return mat3( vec3_add(a.col[0], b.col[0]), vec3_add(a.col[1], b.col[1]),
                 vec3_add(a.col[2], b.col[2]) );
}
LM_FUNC mat3 mat3_sub(mat3 a, mat3 b)
{
    return mat3( vec3_sub(a.col[0], b.col[0]), vec3_sub(a.col[1], b.col[1]),
                 vec3_sub(a.col[2], b.col[2]) );
}
LM_FUNC mat3 mat3_mul(mat3 a, mat3 b)
{
    mat3 tmp = mat3_zero();
    int k, r, c;
    for(c=0; c<3; ++c) for(r=0; r<3; ++r) for(k=0; k<3; ++k)
        MAT(tmp, c, r) += MAT(a, c, k) * MAT(b, k, r);
    return tmp;
}
LM_FUNC vec3 mat3_mul_vec3(mat3 m, vec3 v)
{
    vec3 tmp = vec3(0,0,0);
    int i, j;
    for(j=0; j<3; ++j) for(i=0; i<3; ++i)
        tmp.v[j] += MAT(m, j, i) * v.v[i];
    return tmp;
}
LM_FUNC mat3 mat3_rotation(vec3 axis, float angle)
{
    quat r = quat_rotation(axis, angle);
    return mat3_mul_quat(mat3_identity(), r);
}
LM_FUNC mat3 mat3_scaling(vec3 scale)
{
    return mat3( vec3( scale.x, 0, 0 ),
                 vec3( 0, scale.y, 0 ),
                 vec3( 0, 0, scale.z ) );
}
LM_FUNC mat3 mat3_rotate(vec3 axis, float angle, mat3 m)
{
    quat r = quat_rotation(axis, angle);
    return mat3_mul_quat(m, r);
}
LM_FUNC mat3 mat3_scale(float k, mat3 m)
{
    return mat3( vec3_scale(m.col[0], k), vec3_scale(m.col[1], k),
                 vec3_scale(m.col[2], k) );
}
LM_FUNC mat3 mat3_scale_aniso(vec3 s, mat3 m)
{
    return mat3( vec3_scale(m.col[0], s.x), vec3_scale(m.col[1], s.y),
                 vec3_scale(m.col[2], s.z) );
}
LM_FUNC mat3 mat3_look_at(vec3 eye, vec3 center, vec3 up)
{
    vec3 f = vec3_norm(vec3_sub(center, eye));
    vec3 s = vec3_norm(vec3_cross(f, up));
    vec3 t = vec3_cross(s, f);
    return mat3( vec3( s.x,  t.x,  -f.x ),
                 vec3( s.y,  t.y,  -f.y ),
                 vec3( s.z,  t.z,  -f.z ) );
}
LM_FUNC mat3 mat3_transpose(mat3 m)
{
    mat3 tmp;
    int i, j;
    for(j=0; j<3; ++j) for(i=0; i<3; ++i)
        tmp.col[i].v[j] = m.col[j].v[i];
    return tmp;
}
LM_FUNC mat3 mat3_orthonormalize(mat3 m)
{
    float s = 1;
    vec3 r[3] = { m.col[0], m.col[1], m.col[2] };
    vec3 h = vec3_norm(r[2]);
    r[2] = vec3_norm(r[2]);
    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[2] = vec3_norm(r[2]);
    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[1] = vec3_norm(r[1]);
    s = vec3_dot(r[0], r[1]);
    h = vec3_scale(r[1], s);
    r[0] = vec3_sub(r[0], h);
    r[0] = vec3_norm(r[0]);
    return mat3( r[0], r[1], r[2] );
}

#undef MAT
#undef LM_FUNC
#undef LM_SCALAR

#endif
