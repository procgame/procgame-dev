/**
    \file math_utility.h
    \brief High-level mathematics/geometry utilities
 */

#pragma once

#include "pg_math/linmath.h"

#define FORCE_INLINE    __attribute__((always_inline)) static inline

/** @name       Miscellaneous macros    */
/** Given a ring buffer with given size and zero index, evaluates to the real
 *  index in the base array */
#define RING_IDX(BUF_SIZE, ZERO_IDX, WANT_IDX) \
    ( LM_MOD((ZERO_IDX) + (WANT_IDX), (BUF_SIZE)) )

/** Given a regular 3D grid's dimensions, and a 3D coordinate, evaluates to the
 *  index in the grid's base array (assumes [x][y][z] layout)   */
#define GRID_IDX(SIZE, X, Y, Z)  ((X) + (Y) * (SIZE).x + (Z) * (SIZE).x * (SIZE).y)

/** Random integer in [0,r] */
#define RANDI(r)    (rand() % ((r)+1))
/** Random integer in [-r,r]    */
#define RANDI2(r)   (RANDI((r)*2) - (r))
/** Random float in [0,r]   */
#define RANDF(r)    ((float)rand() / RAND_MAX * (r))
/** Random float in [-r,r]  */
#define RANDF2(r)   (((float)rand() / RAND_MAX * 2 - 1) * (r))

/** uint32_t with 8 bits per member in the given vec4 (clamped to [0,1])
 *  with X in the most significant byte and W in the least  */
#define VEC4_TO_UINT(v) (uint32_t)( \
            (((uint32_t)(LM_SATURATE((v).x) * 255) & 0xFF) << 24) | \
            (((uint32_t)(LM_SATURATE((v).y) * 255) & 0xFF) << 16) | \
            (((uint32_t)(LM_SATURATE((v).z) * 255) & 0xFF) << 8) | \
            (((uint32_t)(LM_SATURATE((v).w) * 255) & 0xFF) << 0) )

/** vec4 with each member unpacked from uint32_t with 8 bits per member
 *  with X in the most significant byte and W in the least  */
#define UINT_TO_VEC4(v) (vec4( \
    ((float)((v >> 24) & 0xFF)) / 255.0f, \
    ((float)((v >> 16) & 0xFF)) / 255.0f, \
    ((float)((v >> 8) & 0xFF)) / 255.0f, \
    ((float)((v >> 0) & 0xFF)) / 255.0f ))

/** Simple iterative factorial implementation   */
FORCE_INLINE int factorial(int a);

/** @name Common 3D Graphics Matrices   */
enum pg_matrix {
    PG_MODEL_MATRIX,
    PG_NORMAL_MATRIX,
    PG_VIEW_MATRIX,
    PG_PROJECTION_MATRIX,
    PG_MODELVIEW_MATRIX,
    PG_PROJECTIONVIEW_MATRIX,
    PG_MVP_MATRIX,
    PG_COMMON_MATRICES,
};

/** @name       Direction Handling (3D) */
enum pg_direction {
    PG_NO_DIRECTION = 0,
    PG_FRONT = 1,
    PG_Y_POS = PG_FRONT,
    PG_BACK = 2,
    PG_Y_NEG = PG_BACK,
    PG_LEFT = 3,
    PG_X_POS = PG_LEFT,
    PG_RIGHT = 4,
    PG_X_NEG = PG_RIGHT,
    PG_UP = 5,
    PG_Z_POS = PG_UP,
    PG_TOP = PG_UP,
    PG_DOWN = 6,
    PG_Z_NEG = PG_DOWN,
    PG_BOTTOM = PG_DOWN,
};

static const int PG_DIR_OPPOSITE[7] = { 0, 2, 1, 4, 3, 6, 5 };
static const vec3 PG_DIR_VEC[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ 0, 1.0f, 0 }},
    [PG_BACK] = {{ 0, -1.0f, 0 }},
    [PG_LEFT] = {{ 1.0f, 0, 0 }},
    [PG_RIGHT] = {{ -1.0f, 0, 0 }},
    [PG_UP] = {{ 0, 0, 1.0f }},
    [PG_DOWN] = {{ 0, 0, -1.0f }}, };
static const vec3 PG_DIR_TAN[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ -1.0f, 0, 0 }},
    [PG_BACK] = {{ 1.0f, 0, 0 }},
    [PG_LEFT] = {{ 0, 1.0f, 0 }},
    [PG_RIGHT] = {{ 0, -1.0f, 0 }},
    [PG_UP] = {{ 1.0f, 0, 0 }},
    [PG_DOWN] = {{ -1.0f, 0, 0 }}, };
static const vec3 PG_DIR_BITAN[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ 0, 0, 1.0f }},
    [PG_BACK] = {{ 0, 0, -1.0f }},
    [PG_LEFT] = {{ 0, 0, 1.0f }},
    [PG_RIGHT] = {{ 0, 0, -1.0f }},
    [PG_UP] = {{ 0, 1.0f, 0 }},
    [PG_DOWN] = {{ 0, -1.0f, 0 }}, };
static const int PG_DIR_IDX[7][3] = {
    [PG_NO_DIRECTION] = { 0, 0, 0 },
    [PG_FRONT] = { 0, 1, 0 },
    [PG_BACK] = { 0, -1, 0 },
    [PG_LEFT] = { 1, 0, 0 },
    [PG_RIGHT] = { -1, 0, 0 },
    [PG_UP] = { 0, 0, 1 },
    [PG_DOWN] = { 0, 0, -1 }, };

/** Get the nearest cardinal direction to a given vector    */
enum pg_direction pg_get_direction(vec3 v);

/* @name   Shape primitives (3D)    */
typedef struct triangle { vec3 t[3]; } triangle;
typedef struct sphere { vec3 p; float r; } sphere;
typedef struct aabb3D { vec3 b0, b1; } aabb3D;
typedef struct obb3D { aabb3D box; quat rot; } obb3D;
typedef struct plane { vec3 p, n; } plane;
typedef struct ray3D { vec3 src; vec3 dir; } ray3D;

/** @name   Shape construction macros (3D) */
#define TRIANGLE(T0, T1, T2) ((triangle){ .t = { T0, T1, T2 } })
#define SPHERE(CENTER, RADIUS) ((sphere){ .p = CENTER, .r = RADIUS })
#define AABB3D(BMIN, BMAX) ((aabb3D){ .b0 = BMIN, .b1 = BMAX })
#define OBB3D(BOX, ROT) ((obb3D){ .box = BOX, .rot = ROT })
#define PLANE(P0, NORM) ((plane){ .p = P0, .n = NORM })
#define RAY3D(P, D) ((ray3D){ .src = P, .dir = D })

/** @name  Closest point calculations (3D) */
vec3 closestpt_point_triangle(vec3 p, const triangle* tri);
vec3 closestpt_point_sphere(vec3 p, const sphere* sph);
vec3 closestpt_point_aabb3D(vec3 p, const aabb3D* box);
vec3 closestpt_point_obb3D(vec3 p, const obb3D* box);
vec3 closestpt_point_plane(vec3 p, const plane* pln);

/** @name  Raycasts (3D) */
float raycast_triangle(const ray3D* ray, const triangle* tri);
float raycast_sphere(const ray3D* ray, const sphere* sph);
float raycast_sphere_inverted(const ray3D* ray, const sphere* sph);
float raycast_aabb3D(const ray3D* ray, const aabb3D* box);
float raycast_obb3D(const ray3D* ray, const obb3D* box);
float raycast_plane(const ray3D* ray, const plane* pln);

/** Current state of a raycast in a 3D grid */
struct raycast_grid3D_state {
    ivec3 start_cell, end_cell, step, iter;
    vec3 dist, delta;
    vec3 ray_end;
};

/** Initialize a raycast state
 *  @param start        Starting point of the ray
 *  @param end          Ending point of the ray
 *  @param bound        The bounding box of the grid to cast in
 *  @param cell_size    The size of every grid cell
 *  @param out          Raycast state to initialize
 *  @return     True if the ray enters the grid, false otherwise    */
int start_raycast_grid3D(vec3 start, vec3 end, aabb3D bound,
        vec3 cell_size, struct raycast_grid3D_state* out);
/** Step a raycaster to the next intersection
 *  @param rc           Raycaster state
 *  @return     True if the ray is still inside the grid, false otherwise   */
int step_raycast_grid3D(struct raycast_grid3D_state* rc);

/** @name  Conversions (3D) */
void aabb3D_from_sphere(aabb3D* out, const sphere* sph);
void aabb3D_from_obb3D(aabb3D* out, const obb3D* in);
vec3 aabb3D_center(aabb3D* box);
void plane_from_triangle(plane* out, const triangle* tri);

/** @name  Intersection tests (3D) */
vec3 sep_axis_sphere_triangle(const sphere* sph, const triangle* tri);
vec3 sep_axis_sphere_sphere(const sphere* sph0, const sphere* sph1);
vec3 sep_axis_sphere_aabb3D(const sphere* sph, const aabb3D* box);
vec3 sep_axis_sphere_obb3D(const sphere* sph, const obb3D* box);
FORCE_INLINE int aabb3D_intersects(aabb3D* box0, aabb3D* box1);

/** @name  Distance functions (3D) */
FORCE_INLINE float sq_dist_point_line(vec3 a, vec3 b, vec3 c);

/** @name Shape primitives (2D) */
typedef struct circle { vec2 c; float p; } circle;
typedef struct aabb2D { vec2 b0, b1; } aabb2D;
typedef struct obb2D { vec2 rot; vec2 dims; vec2 center; } obb2D;
typedef struct ray2D { vec2 src; vec2 dir; } ray2D;

/** @name Shape construction macros (2D)    */
#define CIRCLE(CENTER, RADIUS) ((circle){ .p = CENTER, .r = RADIUS })
#define AABB2D(BMIN, BMAX) ((aabb2D){ .b0 = BMIN, .b1 = BMAX })
#define OBB2D(BOX, ANGLE) \
    ((obb3D){ .box = BOX, .angle = vec2_rotate(vec2_X(), ANGLE) })
#define RAY2D(P, D) ((ray2D){ .src = P, .dir = D })

/** @name  Raycasts (2D)    */
float raycast_aabb2D(const ray2D* ray, const aabb2D* box);

/** Current state of a raycast in a 3D grid */
struct raycast_grid2D_state {
    ivec2 start_cell, end_cell, step, iter;
    vec2 dist, delta;
};

/** Initialize a raycast state
 *  @param start        Starting point of the ray
 *  @param end          Ending point of the ray
 *  @param bound        The bounding box of the grid to cast in
 *  @param cell_size    The size of every grid cell
 *  @param out          Raycast state to initialize
 *  @return     True if the ray enters the grid, false otherwise    */
int start_raycast_grid2D(vec2 start, vec2 end, aabb2D bound,
        vec2 cell_size, struct raycast_grid2D_state* out);
/** Step a raycaster to the next intersection
 *  @param rc           Raycaster state
 *  @return     True if the ray is still inside the grid, false otherwise   */
int step_raycast_grid2D(struct raycast_grid2D_state* rc);

/*  Inline functions    */

FORCE_INLINE float sq_dist_point_line(vec3 a, vec3 b, vec3 c)
{
    return vec3_len2(vec3_cross(vec3_sub(b, a), vec3_sub(a, c))) / vec3_dist2(a, b);
}

FORCE_INLINE int aabb3D_intersects(aabb3D* box0, aabb3D* box1)
{
    if(box0->b1.x < box1->b0.x || box0->b0.x > box1->b1.x
    || box0->b1.y < box1->b0.y || box0->b0.y > box1->b1.y
    || box0->b1.z < box1->b0.z || box0->b0.z > box1->b1.z) return 0;
    return 1;
}

FORCE_INLINE int factorial(int a)
{
    int n = a, m = a;
    while(--m > 0) n *= m;
    return n;
}

#undef FORCE_INLINE
