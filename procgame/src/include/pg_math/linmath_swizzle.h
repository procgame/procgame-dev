/****************************/
/*  SWIZZLE                 */
/****************************/

#define VEC_XXXX(V)    (V).x, (V).x, (V).x, (V).x
#define VEC_XXXY(V)    (V).x, (V).x, (V).x, (V).y
#define VEC_XXXZ(V)    (V).x, (V).x, (V).x, (V).z
#define VEC_XXXW(V)    (V).x, (V).x, (V).x, (V).w
#define VEC_XXYX(V)    (V).x, (V).x, (V).y, (V).x
#define VEC_XXYY(V)    (V).x, (V).x, (V).y, (V).y
#define VEC_XXYZ(V)    (V).x, (V).x, (V).y, (V).z
#define VEC_XXYW(V)    (V).x, (V).x, (V).y, (V).w
#define VEC_XXZX(V)    (V).x, (V).x, (V).z, (V).x
#define VEC_XXZY(V)    (V).x, (V).x, (V).z, (V).y
#define VEC_XXZZ(V)    (V).x, (V).x, (V).z, (V).z
#define VEC_XXZW(V)    (V).x, (V).x, (V).z, (V).w
#define VEC_XXWX(V)    (V).x, (V).x, (V).w, (V).x
#define VEC_XXWY(V)    (V).x, (V).x, (V).w, (V).y
#define VEC_XXWZ(V)    (V).x, (V).x, (V).w, (V).z
#define VEC_XXWW(V)    (V).x, (V).x, (V).w, (V).w
#define VEC_XYXX(V)    (V).x, (V).y, (V).x, (V).x
#define VEC_XYXY(V)    (V).x, (V).y, (V).x, (V).y
#define VEC_XYXZ(V)    (V).x, (V).y, (V).x, (V).z
#define VEC_XYXW(V)    (V).x, (V).y, (V).x, (V).w
#define VEC_XYYX(V)    (V).x, (V).y, (V).y, (V).x
#define VEC_XYYY(V)    (V).x, (V).y, (V).y, (V).y
#define VEC_XYYZ(V)    (V).x, (V).y, (V).y, (V).z
#define VEC_XYYW(V)    (V).x, (V).y, (V).y, (V).w
#define VEC_XYZX(V)    (V).x, (V).y, (V).z, (V).x
#define VEC_XYZY(V)    (V).x, (V).y, (V).z, (V).y
#define VEC_XYZZ(V)    (V).x, (V).y, (V).z, (V).z
#define VEC_XYZW(V)    (V).x, (V).y, (V).z, (V).w
#define VEC_XYWX(V)    (V).x, (V).y, (V).w, (V).x
#define VEC_XYWY(V)    (V).x, (V).y, (V).w, (V).y
#define VEC_XYWZ(V)    (V).x, (V).y, (V).w, (V).z
#define VEC_XYWW(V)    (V).x, (V).y, (V).w, (V).w
#define VEC_XZXX(V)    (V).x, (V).z, (V).x, (V).x
#define VEC_XZXY(V)    (V).x, (V).z, (V).x, (V).y
#define VEC_XZXZ(V)    (V).x, (V).z, (V).x, (V).z
#define VEC_XZXW(V)    (V).x, (V).z, (V).x, (V).w
#define VEC_XZYX(V)    (V).x, (V).z, (V).y, (V).x
#define VEC_XZYY(V)    (V).x, (V).z, (V).y, (V).y
#define VEC_XZYZ(V)    (V).x, (V).z, (V).y, (V).z
#define VEC_XZYW(V)    (V).x, (V).z, (V).y, (V).w
#define VEC_XZZX(V)    (V).x, (V).z, (V).z, (V).x
#define VEC_XZZY(V)    (V).x, (V).z, (V).z, (V).y
#define VEC_XZZZ(V)    (V).x, (V).z, (V).z, (V).z
#define VEC_XZZW(V)    (V).x, (V).z, (V).z, (V).w
#define VEC_XZWX(V)    (V).x, (V).z, (V).w, (V).x
#define VEC_XZWY(V)    (V).x, (V).z, (V).w, (V).y
#define VEC_XZWZ(V)    (V).x, (V).z, (V).w, (V).z
#define VEC_XZWW(V)    (V).x, (V).z, (V).w, (V).w
#define VEC_XWXX(V)    (V).x, (V).w, (V).x, (V).x
#define VEC_XWXY(V)    (V).x, (V).w, (V).x, (V).y
#define VEC_XWXZ(V)    (V).x, (V).w, (V).x, (V).z
#define VEC_XWXW(V)    (V).x, (V).w, (V).x, (V).w
#define VEC_XWYX(V)    (V).x, (V).w, (V).y, (V).x
#define VEC_XWYY(V)    (V).x, (V).w, (V).y, (V).y
#define VEC_XWYZ(V)    (V).x, (V).w, (V).y, (V).z
#define VEC_XWYW(V)    (V).x, (V).w, (V).y, (V).w
#define VEC_XWZX(V)    (V).x, (V).w, (V).z, (V).x
#define VEC_XWZY(V)    (V).x, (V).w, (V).z, (V).y
#define VEC_XWZZ(V)    (V).x, (V).w, (V).z, (V).z
#define VEC_XWZW(V)    (V).x, (V).w, (V).z, (V).w
#define VEC_XWWX(V)    (V).x, (V).w, (V).w, (V).x
#define VEC_XWWY(V)    (V).x, (V).w, (V).w, (V).y
#define VEC_XWWZ(V)    (V).x, (V).w, (V).w, (V).z
#define VEC_XWWW(V)    (V).x, (V).w, (V).w, (V).w
#define VEC_YXXX(V)    (V).y, (V).x, (V).x, (V).x
#define VEC_YXXY(V)    (V).y, (V).x, (V).x, (V).y
#define VEC_YXXZ(V)    (V).y, (V).x, (V).x, (V).z
#define VEC_YXXW(V)    (V).y, (V).x, (V).x, (V).w
#define VEC_YXYX(V)    (V).y, (V).x, (V).y, (V).x
#define VEC_YXYY(V)    (V).y, (V).x, (V).y, (V).y
#define VEC_YXYZ(V)    (V).y, (V).x, (V).y, (V).z
#define VEC_YXYW(V)    (V).y, (V).x, (V).y, (V).w
#define VEC_YXZX(V)    (V).y, (V).x, (V).z, (V).x
#define VEC_YXZY(V)    (V).y, (V).x, (V).z, (V).y
#define VEC_YXZZ(V)    (V).y, (V).x, (V).z, (V).z
#define VEC_YXZW(V)    (V).y, (V).x, (V).z, (V).w
#define VEC_YXWX(V)    (V).y, (V).x, (V).w, (V).x
#define VEC_YXWY(V)    (V).y, (V).x, (V).w, (V).y
#define VEC_YXWZ(V)    (V).y, (V).x, (V).w, (V).z
#define VEC_YXWW(V)    (V).y, (V).x, (V).w, (V).w
#define VEC_YYXX(V)    (V).y, (V).y, (V).x, (V).x
#define VEC_YYXY(V)    (V).y, (V).y, (V).x, (V).y
#define VEC_YYXZ(V)    (V).y, (V).y, (V).x, (V).z
#define VEC_YYXW(V)    (V).y, (V).y, (V).x, (V).w
#define VEC_YYYX(V)    (V).y, (V).y, (V).y, (V).x
#define VEC_YYYY(V)    (V).y, (V).y, (V).y, (V).y
#define VEC_YYYZ(V)    (V).y, (V).y, (V).y, (V).z
#define VEC_YYYW(V)    (V).y, (V).y, (V).y, (V).w
#define VEC_YYZX(V)    (V).y, (V).y, (V).z, (V).x
#define VEC_YYZY(V)    (V).y, (V).y, (V).z, (V).y
#define VEC_YYZZ(V)    (V).y, (V).y, (V).z, (V).z
#define VEC_YYZW(V)    (V).y, (V).y, (V).z, (V).w
#define VEC_YYWX(V)    (V).y, (V).y, (V).w, (V).x
#define VEC_YYWY(V)    (V).y, (V).y, (V).w, (V).y
#define VEC_YYWZ(V)    (V).y, (V).y, (V).w, (V).z
#define VEC_YYWW(V)    (V).y, (V).y, (V).w, (V).w
#define VEC_YZXX(V)    (V).y, (V).z, (V).x, (V).x
#define VEC_YZXY(V)    (V).y, (V).z, (V).x, (V).y
#define VEC_YZXZ(V)    (V).y, (V).z, (V).x, (V).z
#define VEC_YZXW(V)    (V).y, (V).z, (V).x, (V).w
#define VEC_YZYX(V)    (V).y, (V).z, (V).y, (V).x
#define VEC_YZYY(V)    (V).y, (V).z, (V).y, (V).y
#define VEC_YZYZ(V)    (V).y, (V).z, (V).y, (V).z
#define VEC_YZYW(V)    (V).y, (V).z, (V).y, (V).w
#define VEC_YZZX(V)    (V).y, (V).z, (V).z, (V).x
#define VEC_YZZY(V)    (V).y, (V).z, (V).z, (V).y
#define VEC_YZZZ(V)    (V).y, (V).z, (V).z, (V).z
#define VEC_YZZW(V)    (V).y, (V).z, (V).z, (V).w
#define VEC_YZWX(V)    (V).y, (V).z, (V).w, (V).x
#define VEC_YZWY(V)    (V).y, (V).z, (V).w, (V).y
#define VEC_YZWZ(V)    (V).y, (V).z, (V).w, (V).z
#define VEC_YZWW(V)    (V).y, (V).z, (V).w, (V).w
#define VEC_YWXX(V)    (V).y, (V).w, (V).x, (V).x
#define VEC_YWXY(V)    (V).y, (V).w, (V).x, (V).y
#define VEC_YWXZ(V)    (V).y, (V).w, (V).x, (V).z
#define VEC_YWXW(V)    (V).y, (V).w, (V).x, (V).w
#define VEC_YWYX(V)    (V).y, (V).w, (V).y, (V).x
#define VEC_YWYY(V)    (V).y, (V).w, (V).y, (V).y
#define VEC_YWYZ(V)    (V).y, (V).w, (V).y, (V).z
#define VEC_YWYW(V)    (V).y, (V).w, (V).y, (V).w
#define VEC_YWZX(V)    (V).y, (V).w, (V).z, (V).x
#define VEC_YWZY(V)    (V).y, (V).w, (V).z, (V).y
#define VEC_YWZZ(V)    (V).y, (V).w, (V).z, (V).z
#define VEC_YWZW(V)    (V).y, (V).w, (V).z, (V).w
#define VEC_YWWX(V)    (V).y, (V).w, (V).w, (V).x
#define VEC_YWWY(V)    (V).y, (V).w, (V).w, (V).y
#define VEC_YWWZ(V)    (V).y, (V).w, (V).w, (V).z
#define VEC_YWWW(V)    (V).y, (V).w, (V).w, (V).w
#define VEC_ZXXX(V)    (V).z, (V).x, (V).x, (V).x
#define VEC_ZXXY(V)    (V).z, (V).x, (V).x, (V).y
#define VEC_ZXXZ(V)    (V).z, (V).x, (V).x, (V).z
#define VEC_ZXXW(V)    (V).z, (V).x, (V).x, (V).w
#define VEC_ZXYX(V)    (V).z, (V).x, (V).y, (V).x
#define VEC_ZXYY(V)    (V).z, (V).x, (V).y, (V).y
#define VEC_ZXYZ(V)    (V).z, (V).x, (V).y, (V).z
#define VEC_ZXYW(V)    (V).z, (V).x, (V).y, (V).w
#define VEC_ZXZX(V)    (V).z, (V).x, (V).z, (V).x
#define VEC_ZXZY(V)    (V).z, (V).x, (V).z, (V).y
#define VEC_ZXZZ(V)    (V).z, (V).x, (V).z, (V).z
#define VEC_ZXZW(V)    (V).z, (V).x, (V).z, (V).w
#define VEC_ZXWX(V)    (V).z, (V).x, (V).w, (V).x
#define VEC_ZXWY(V)    (V).z, (V).x, (V).w, (V).y
#define VEC_ZXWZ(V)    (V).z, (V).x, (V).w, (V).z
#define VEC_ZXWW(V)    (V).z, (V).x, (V).w, (V).w
#define VEC_ZYXX(V)    (V).z, (V).y, (V).x, (V).x
#define VEC_ZYXY(V)    (V).z, (V).y, (V).x, (V).y
#define VEC_ZYXZ(V)    (V).z, (V).y, (V).x, (V).z
#define VEC_ZYXW(V)    (V).z, (V).y, (V).x, (V).w
#define VEC_ZYYX(V)    (V).z, (V).y, (V).y, (V).x
#define VEC_ZYYY(V)    (V).z, (V).y, (V).y, (V).y
#define VEC_ZYYZ(V)    (V).z, (V).y, (V).y, (V).z
#define VEC_ZYYW(V)    (V).z, (V).y, (V).y, (V).w
#define VEC_ZYZX(V)    (V).z, (V).y, (V).z, (V).x
#define VEC_ZYZY(V)    (V).z, (V).y, (V).z, (V).y
#define VEC_ZYZZ(V)    (V).z, (V).y, (V).z, (V).z
#define VEC_ZYZW(V)    (V).z, (V).y, (V).z, (V).w
#define VEC_ZYWX(V)    (V).z, (V).y, (V).w, (V).x
#define VEC_ZYWY(V)    (V).z, (V).y, (V).w, (V).y
#define VEC_ZYWZ(V)    (V).z, (V).y, (V).w, (V).z
#define VEC_ZYWW(V)    (V).z, (V).y, (V).w, (V).w
#define VEC_ZZXX(V)    (V).z, (V).z, (V).x, (V).x
#define VEC_ZZXY(V)    (V).z, (V).z, (V).x, (V).y
#define VEC_ZZXZ(V)    (V).z, (V).z, (V).x, (V).z
#define VEC_ZZXW(V)    (V).z, (V).z, (V).x, (V).w
#define VEC_ZZYX(V)    (V).z, (V).z, (V).y, (V).x
#define VEC_ZZYY(V)    (V).z, (V).z, (V).y, (V).y
#define VEC_ZZYZ(V)    (V).z, (V).z, (V).y, (V).z
#define VEC_ZZYW(V)    (V).z, (V).z, (V).y, (V).w
#define VEC_ZZZX(V)    (V).z, (V).z, (V).z, (V).x
#define VEC_ZZZY(V)    (V).z, (V).z, (V).z, (V).y
#define VEC_ZZZZ(V)    (V).z, (V).z, (V).z, (V).z
#define VEC_ZZZW(V)    (V).z, (V).z, (V).z, (V).w
#define VEC_ZZWX(V)    (V).z, (V).z, (V).w, (V).x
#define VEC_ZZWY(V)    (V).z, (V).z, (V).w, (V).y
#define VEC_ZZWZ(V)    (V).z, (V).z, (V).w, (V).z
#define VEC_ZZWW(V)    (V).z, (V).z, (V).w, (V).w
#define VEC_ZWXX(V)    (V).z, (V).w, (V).x, (V).x
#define VEC_ZWXY(V)    (V).z, (V).w, (V).x, (V).y
#define VEC_ZWXZ(V)    (V).z, (V).w, (V).x, (V).z
#define VEC_ZWXW(V)    (V).z, (V).w, (V).x, (V).w
#define VEC_ZWYX(V)    (V).z, (V).w, (V).y, (V).x
#define VEC_ZWYY(V)    (V).z, (V).w, (V).y, (V).y
#define VEC_ZWYZ(V)    (V).z, (V).w, (V).y, (V).z
#define VEC_ZWYW(V)    (V).z, (V).w, (V).y, (V).w
#define VEC_ZWZX(V)    (V).z, (V).w, (V).z, (V).x
#define VEC_ZWZY(V)    (V).z, (V).w, (V).z, (V).y
#define VEC_ZWZZ(V)    (V).z, (V).w, (V).z, (V).z
#define VEC_ZWZW(V)    (V).z, (V).w, (V).z, (V).w
#define VEC_ZWWX(V)    (V).z, (V).w, (V).w, (V).x
#define VEC_ZWWY(V)    (V).z, (V).w, (V).w, (V).y
#define VEC_ZWWZ(V)    (V).z, (V).w, (V).w, (V).z
#define VEC_ZWWW(V)    (V).z, (V).w, (V).w, (V).w
#define VEC_WXXX(V)    (V).w, (V).x, (V).x, (V).x
#define VEC_WXXY(V)    (V).w, (V).x, (V).x, (V).y
#define VEC_WXXZ(V)    (V).w, (V).x, (V).x, (V).z
#define VEC_WXXW(V)    (V).w, (V).x, (V).x, (V).w
#define VEC_WXYX(V)    (V).w, (V).x, (V).y, (V).x
#define VEC_WXYY(V)    (V).w, (V).x, (V).y, (V).y
#define VEC_WXYZ(V)    (V).w, (V).x, (V).y, (V).z
#define VEC_WXYW(V)    (V).w, (V).x, (V).y, (V).w
#define VEC_WXZX(V)    (V).w, (V).x, (V).z, (V).x
#define VEC_WXZY(V)    (V).w, (V).x, (V).z, (V).y
#define VEC_WXZZ(V)    (V).w, (V).x, (V).z, (V).z
#define VEC_WXZW(V)    (V).w, (V).x, (V).z, (V).w
#define VEC_WXWX(V)    (V).w, (V).x, (V).w, (V).x
#define VEC_WXWY(V)    (V).w, (V).x, (V).w, (V).y
#define VEC_WXWZ(V)    (V).w, (V).x, (V).w, (V).z
#define VEC_WXWW(V)    (V).w, (V).x, (V).w, (V).w
#define VEC_WYXX(V)    (V).w, (V).y, (V).x, (V).x
#define VEC_WYXY(V)    (V).w, (V).y, (V).x, (V).y
#define VEC_WYXZ(V)    (V).w, (V).y, (V).x, (V).z
#define VEC_WYXW(V)    (V).w, (V).y, (V).x, (V).w
#define VEC_WYYX(V)    (V).w, (V).y, (V).y, (V).x
#define VEC_WYYY(V)    (V).w, (V).y, (V).y, (V).y
#define VEC_WYYZ(V)    (V).w, (V).y, (V).y, (V).z
#define VEC_WYYW(V)    (V).w, (V).y, (V).y, (V).w
#define VEC_WYZX(V)    (V).w, (V).y, (V).z, (V).x
#define VEC_WYZY(V)    (V).w, (V).y, (V).z, (V).y
#define VEC_WYZZ(V)    (V).w, (V).y, (V).z, (V).z
#define VEC_WYZW(V)    (V).w, (V).y, (V).z, (V).w
#define VEC_WYWX(V)    (V).w, (V).y, (V).w, (V).x
#define VEC_WYWY(V)    (V).w, (V).y, (V).w, (V).y
#define VEC_WYWZ(V)    (V).w, (V).y, (V).w, (V).z
#define VEC_WYWW(V)    (V).w, (V).y, (V).w, (V).w
#define VEC_WZXX(V)    (V).w, (V).z, (V).x, (V).x
#define VEC_WZXY(V)    (V).w, (V).z, (V).x, (V).y
#define VEC_WZXZ(V)    (V).w, (V).z, (V).x, (V).z
#define VEC_WZXW(V)    (V).w, (V).z, (V).x, (V).w
#define VEC_WZYX(V)    (V).w, (V).z, (V).y, (V).x
#define VEC_WZYY(V)    (V).w, (V).z, (V).y, (V).y
#define VEC_WZYZ(V)    (V).w, (V).z, (V).y, (V).z
#define VEC_WZYW(V)    (V).w, (V).z, (V).y, (V).w
#define VEC_WZZX(V)    (V).w, (V).z, (V).z, (V).x
#define VEC_WZZY(V)    (V).w, (V).z, (V).z, (V).y
#define VEC_WZZZ(V)    (V).w, (V).z, (V).z, (V).z
#define VEC_WZZW(V)    (V).w, (V).z, (V).z, (V).w
#define VEC_WZWX(V)    (V).w, (V).z, (V).w, (V).x
#define VEC_WZWY(V)    (V).w, (V).z, (V).w, (V).y
#define VEC_WZWZ(V)    (V).w, (V).z, (V).w, (V).z
#define VEC_WZWW(V)    (V).w, (V).z, (V).w, (V).w
#define VEC_WWXX(V)    (V).w, (V).w, (V).x, (V).x
#define VEC_WWXY(V)    (V).w, (V).w, (V).x, (V).y
#define VEC_WWXZ(V)    (V).w, (V).w, (V).x, (V).z
#define VEC_WWXW(V)    (V).w, (V).w, (V).x, (V).w
#define VEC_WWYX(V)    (V).w, (V).w, (V).y, (V).x
#define VEC_WWYY(V)    (V).w, (V).w, (V).y, (V).y
#define VEC_WWYZ(V)    (V).w, (V).w, (V).y, (V).z
#define VEC_WWYW(V)    (V).w, (V).w, (V).y, (V).w
#define VEC_WWZX(V)    (V).w, (V).w, (V).z, (V).x
#define VEC_WWZY(V)    (V).w, (V).w, (V).z, (V).y
#define VEC_WWZZ(V)    (V).w, (V).w, (V).z, (V).z
#define VEC_WWZW(V)    (V).w, (V).w, (V).z, (V).w
#define VEC_WWWX(V)    (V).w, (V).w, (V).w, (V).x
#define VEC_WWWY(V)    (V).w, (V).w, (V).w, (V).y
#define VEC_WWWZ(V)    (V).w, (V).w, (V).w, (V).z
#define VEC_WWWW(V)    (V).w, (V).w, (V).w, (V).w
#define VEC_XXX(V)    (V).x, (V).x, (V).x
#define VEC_XXY(V)    (V).x, (V).x, (V).y
#define VEC_XXZ(V)    (V).x, (V).x, (V).z
#define VEC_XXW(V)    (V).x, (V).x, (V).w
#define VEC_XYX(V)    (V).x, (V).y, (V).x
#define VEC_XYY(V)    (V).x, (V).y, (V).y
#define VEC_XYZ(V)    (V).x, (V).y, (V).z
#define VEC_XYW(V)    (V).x, (V).y, (V).w
#define VEC_XZX(V)    (V).x, (V).z, (V).x
#define VEC_XZY(V)    (V).x, (V).z, (V).y
#define VEC_XZZ(V)    (V).x, (V).z, (V).z
#define VEC_XZW(V)    (V).x, (V).z, (V).w
#define VEC_XWX(V)    (V).x, (V).w, (V).x
#define VEC_XWY(V)    (V).x, (V).w, (V).y
#define VEC_XWZ(V)    (V).x, (V).w, (V).z
#define VEC_XWW(V)    (V).x, (V).w, (V).w
#define VEC_YXX(V)    (V).y, (V).x, (V).x
#define VEC_YXY(V)    (V).y, (V).x, (V).y
#define VEC_YXZ(V)    (V).y, (V).x, (V).z
#define VEC_YXW(V)    (V).y, (V).x, (V).w
#define VEC_YYX(V)    (V).y, (V).y, (V).x
#define VEC_YYY(V)    (V).y, (V).y, (V).y
#define VEC_YYZ(V)    (V).y, (V).y, (V).z
#define VEC_YYW(V)    (V).y, (V).y, (V).w
#define VEC_YZX(V)    (V).y, (V).z, (V).x
#define VEC_YZY(V)    (V).y, (V).z, (V).y
#define VEC_YZZ(V)    (V).y, (V).z, (V).z
#define VEC_YZW(V)    (V).y, (V).z, (V).w
#define VEC_YWX(V)    (V).y, (V).w, (V).x
#define VEC_YWY(V)    (V).y, (V).w, (V).y
#define VEC_YWZ(V)    (V).y, (V).w, (V).z
#define VEC_YWW(V)    (V).y, (V).w, (V).w
#define VEC_ZXX(V)    (V).z, (V).x, (V).x
#define VEC_ZXY(V)    (V).z, (V).x, (V).y
#define VEC_ZXZ(V)    (V).z, (V).x, (V).z
#define VEC_ZXW(V)    (V).z, (V).x, (V).w
#define VEC_ZYX(V)    (V).z, (V).y, (V).x
#define VEC_ZYY(V)    (V).z, (V).y, (V).y
#define VEC_ZYZ(V)    (V).z, (V).y, (V).z
#define VEC_ZYW(V)    (V).z, (V).y, (V).w
#define VEC_ZZX(V)    (V).z, (V).z, (V).x
#define VEC_ZZY(V)    (V).z, (V).z, (V).y
#define VEC_ZZZ(V)    (V).z, (V).z, (V).z
#define VEC_ZZW(V)    (V).z, (V).z, (V).w
#define VEC_ZWX(V)    (V).z, (V).w, (V).x
#define VEC_ZWY(V)    (V).z, (V).w, (V).y
#define VEC_ZWZ(V)    (V).z, (V).w, (V).z
#define VEC_ZWW(V)    (V).z, (V).w, (V).w
#define VEC_WXX(V)    (V).w, (V).x, (V).x
#define VEC_WXY(V)    (V).w, (V).x, (V).y
#define VEC_WXZ(V)    (V).w, (V).x, (V).z
#define VEC_WXW(V)    (V).w, (V).x, (V).w
#define VEC_WYX(V)    (V).w, (V).y, (V).x
#define VEC_WYY(V)    (V).w, (V).y, (V).y
#define VEC_WYZ(V)    (V).w, (V).y, (V).z
#define VEC_WYW(V)    (V).w, (V).y, (V).w
#define VEC_WZX(V)    (V).w, (V).z, (V).x
#define VEC_WZY(V)    (V).w, (V).z, (V).y
#define VEC_WZZ(V)    (V).w, (V).z, (V).z
#define VEC_WZW(V)    (V).w, (V).z, (V).w
#define VEC_WWX(V)    (V).w, (V).w, (V).x
#define VEC_WWY(V)    (V).w, (V).w, (V).y
#define VEC_WWZ(V)    (V).w, (V).w, (V).z
#define VEC_WWW(V)    (V).w, (V).w, (V).w
#define VEC_XX(V)    (V).x, (V).x
#define VEC_XY(V)    (V).x, (V).y
#define VEC_XZ(V)    (V).x, (V).z
#define VEC_XW(V)    (V).x, (V).w
#define VEC_YX(V)    (V).y, (V).x
#define VEC_YY(V)    (V).y, (V).y
#define VEC_YZ(V)    (V).y, (V).z
#define VEC_YW(V)    (V).y, (V).w
#define VEC_ZX(V)    (V).z, (V).x
#define VEC_ZY(V)    (V).z, (V).y
#define VEC_ZZ(V)    (V).z, (V).z
#define VEC_ZW(V)    (V).z, (V).w
#define VEC_WX(V)    (V).w, (V).x
#define VEC_WY(V)    (V).w, (V).y
#define VEC_WZ(V)    (V).w, (V).z
#define VEC_WW(V)    (V).w, (V).w
#define VEC_X(V)    (V).x
#define VEC_Y(V)    (V).y
#define VEC_Z(V)    (V).z
#define VEC_W(V)    (V).w

