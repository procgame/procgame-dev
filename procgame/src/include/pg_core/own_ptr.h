#ifndef INCLUDED_OWN_PTR
#define INCLUDED_OWN_PTR

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define OWNED_PTR_T(T) \
struct { \
    T* ptr; \
    bool own; \
    bool own_alloc; \
}

#define OWNED_NULL(P) ((P){0})

#define ALLOC_OWNED(P) \
    ((P){ .ptr = malloc(sizeof(*((P*)NULL)->ptr)), .own = 1, .own_alloc = 1 })

#define MAKE_OWNED(P, PTR) \
    ((P){ .ptr = PTR, .own = 1, .own_alloc = 0 })

#define MAKE_UNOWNED(P, PTR) \
    ((P){ .ptr = PTR, .own = 0, .own_alloc = 0 })

#define DEINIT_OWNED(P, F) \
do { \
    if(P.own) { F(P.ptr); } \
    if(P.own_alloc && P.ptr) { free(P.ptr); } \
} while(0)

#define FREE_OWNED(P) \
do { \
    if(P.own_alloc) { free(P.ptr); } \
} while(0)

#define GIVE_OWNED(P) \
    ({  typeof(P) P_ = { .ptr = P.ptr, .own = P.own, .own_alloc = P.own_alloc }; \
        P.own = 0; P.own_alloc = 0; P_; })

#define GIVE_UNOWNED(P) \
    ((typeof(P)){ .ptr = P.ptr, .own = 0, .own_alloc = 0 })

#endif
