/** \file inline.h
    Miscellaneous inline utilities that don't really fit anywhere else
*/



/** \detail Evaluates to a array containing all the objects in the input    */
#define PG_POINTERS(T, ...) ((T[]){ __VA_ARGS__ })


/** Get the index of the least significant bit set in the input */
#define PG_LEAST_SIGNIFICANT_BIT(a) \
    (debruijnposition[((uint32_t)(((a) & -(a)) * 0x077CB531U)) >> 27])

static const int debruijnposition[32] = 
{
  0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 
  31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
};



/** Get the number of set bits in the input  */
#define PG_POPCOUNT(a) popcount_impl(a)
#define PG_BITS_SET PG_POPCOUNT

#ifndef __GNUC__
    static inline unsigned int popcount_impl(unsigned int i)
    {
        /*  It isn't for us to understand how this works, but it returns
            the number of set bits in an integer    */
        i = i - ((i >> 1) & 0x55555555);
        i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
        return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
    }
#else
    #define popcount_impl __builtin_popcount
#endif
