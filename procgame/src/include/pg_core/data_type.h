/**
    \file data_type.h
    \ingroup pg_core
    \brief Runtime typed data storage
*/


#pragma once

typedef struct pg_data pg_data_t;
enum pg_data_type {
    PG_NULL,
    PG_BYTE, PG_INT8 = PG_BYTE, PG_BVEC2, PG_BVEC3, PG_BVEC4,
    PG_UBYTE, PG_UINT8 = PG_UBYTE, PG_UBVEC2, PG_UBVEC3, PG_UBVEC4,
    PG_SHORT, PG_INT16 = PG_SHORT, PG_SVEC2, PG_SVEC3, PG_SVEC4,
    PG_USHORT, PG_UINT16 = PG_USHORT, PG_USVEC2, PG_USVEC3, PG_USVEC4,
    PG_INT, PG_IVEC2, PG_IVEC3, PG_IVEC4,
    PG_UINT, PG_UVEC2, PG_UVEC3, PG_UVEC4,
    PG_FLOAT, PG_VEC2, PG_VEC3, PG_VEC4, PG_MAT4, PG_MAT3,
    PG_POINTER, PG_STRING,
    PG_NUM_DATA_TYPES,
};



/** \brief Base type of compound data types (ie. PG_IVEC3 base type is PG_INT)  */
extern const enum pg_data_type pg_data_base_type[PG_NUM_DATA_TYPES];
/** \brief Number of components in compound data types (ie. PG_IVEC3 components is 3)   */
extern const int pg_data_type_components[PG_NUM_DATA_TYPES];
/** \brief String representation of a data type name (ie. PG_IVEC3 string is "vec3")    */
extern const char* pg_data_type_string[PG_NUM_DATA_TYPES];
/** \brief Data size of a data type (ie. PG_IVEC3 size is sizeof(vec3)) */
extern const size_t pg_data_type_size[PG_NUM_DATA_TYPES];
/** \brief Get type from its string representation  */
enum pg_data_type pg_data_type_from_string(const char* string);



/**
    \brief Runtime typed data storage
    \detail Transparently handles data which is small enough to hold in
            the struct vs. data large enough to warrant heap allocation.
*/
typedef struct pg_data pg_data_t;

struct pg_data {
    /** \brief Type of the data */
    enum pg_data_type type;
    /** \brief Number of elements   */
    int n;
    bool big;
    union {
        int8_t b[64]; uint8_t ub[64]; int16_t s[32]; uint16_t us[32];
        int32_t i[16]; uint32_t u[16]; float f[16];
        char str[64];
        void* ptr[8];
    } small;
    union {
        int8_t* b; uint8_t* ub; int16_t s; uint16_t us; int32_t* i;
        uint32_t* u; float* f;
        char* str;
        void** ptr;
    };
};

/** Create a new data object with the given type, elements, and (optional) initial data */
pg_data_t pg_data_init(enum pg_data_type type, int n, void* data);
/** Deinitialize a data object  */
void pg_data_deinit(pg_data_t* d);

/** Create a new data object from a source object   */
pg_data_t pg_data_duplicate(pg_data_t* src);
/** Copy data from one object to another (must have matching type and element count)    */
void pg_data_copy(pg_data_t* dst, pg_data_t* src);
/** Update ALL data in a data object    */
void pg_data_update(pg_data_t* d, enum pg_data_type type, void* data);

/** Get a pointer to the data at a given index  */
void* pg_data_value_ptr(pg_data_t* d, int i);

bool pg_data_to_string(pg_data_t* d, char* out, int out_max);



/********************************/
/*  Data hash-tables            */
/********************************/

/** Hash table of pg_data objects   */
typedef HTABLE_T(pg_data_t) pg_data_table_t;

/** Create a new hash table of pg_data objects  */
pg_data_table_t pg_data_table(int n_buckets);
/** Deinitialize a hash table of pg_data objects, and all contained pg_data objects */
void pg_data_table_deinit(pg_data_table_t* table);

