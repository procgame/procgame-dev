#pragma once

#include <SDL2/SDL.h>

/** \brief Check the output of an SDL-calling expression
    \return Evaluates to the raw integer return value   */
#define PG_CHECK_SDL_I(...) \
({ \
    int result = __VA_ARGS__; \
    if(result < 0) { \
        pg_log(PG_LOG_ERROR, #__VA_ARGS__ " : %s", SDL_GetError()); \
        SDL_ClearError(); \
    } \
    (result >= 0); \
})

/** \brief Check the output of an SDL-calling expression
    \return Evaluates to true if the return value is greater than 0 */
#define PG_CHECK_SDL_B(...) \
({ \
    int result = __VA_ARGS__; \
    if(result < 0) { \
        pg_log(PG_LOG_ERROR, #__VA_ARGS__ " : %s", SDL_GetError()); \
        SDL_ClearError(); \
    } \
    (result >= 0); \
})

/** \brief Check the output of an SDL-calling expression
    \return Evaluates to the raw pointer return value   */
#define PG_CHECK_SDL_P(...) \
({ \
    __auto_type result = __VA_ARGS__; \
    if(!result) { \
        pg_log(PG_LOG_ERROR, #__VA_ARGS__ " : %s", SDL_GetError()); \
        SDL_ClearError(); \
    } \
    (result); \
})
