/************/
/*  Input   */
/************/

void pg_input_poll(void);
bool pg_input_user_exit(void);

typedef SARR_T(256, SDL_Event) sdl_event_queue_t;
sdl_event_queue_t* pg_input_sdl_events(void);


/*  Keyboard    */
struct pg_input_wrapper* pg_input_keyboard(void);
void pg_input_keyboard_poll(void);

/*  Mouse   */
struct pg_input_wrapper* pg_input_mouse(void);
void pg_input_mouse_poll(void);

enum pg_input_mouse_boolean {
    PG_INPUT_MOUSEWHEEL_UP,
    PG_INPUT_MOUSEWHEEL_DOWN,
    PG_INPUT_MOUSE_LEFT,
    PG_INPUT_MOUSE_RIGHT,
    PG_INPUT_MOUSE_MIDDLE,
};

enum pg_mouse_mode {
    PG_INPUT_MOUSE_FREE,
    PG_INPUT_MOUSE_CAPTURE,
};

void pg_input_mouse_mode(enum pg_mouse_mode mode);
