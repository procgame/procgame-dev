#ifndef PG_LOGGING
#define PG_LOGGING

/** \file logging.h
    \brief Logging
*/

#include <stdio.h>
#include <stdarg.h>

/** Severity of a log message   */
enum pg_log_type {
    PG_LOG_CONTD,
    PG_LOG_DEBUG_PROCGAME,
    PG_LOG_DEBUG,
    PG_LOG_INFO,
    PG_LOG_WARNING,
    PG_LOG_ERROR,
    PG_LOG_ERROR_FATAL,
};

/****************************/
/*  Standardized logging    */
/****************************/
/** Set the output filename which should be used for logging    */
/** Open the given file on disk for log output  */
void pg_log_open(const char* log_filename);
/** Use an existing open file for log output    */
void pg_log_file(FILE* file);
/** Close the log file  */
void pg_log_close(void);
/** Set the minimum severity level to log   */
void pg_log_level(enum pg_log_type level);
/** Set the current 'last log' level    */
void pg_set_last_log_level(enum pg_log_type level);

/** Emit a log message
    \detail Automatically ignores log messages below the set logging level,
            and adds extra information about the source code location where
            the log is generated. Line breaks are also handled automatically.
    \param TYPE     The severity level of the log message
    \param MSG      A printf-style format string
    \param ...      Formatted tokens for MSG
*/
#define pg_log(TYPE, MSG, ...) \
    pg_log_(TYPE, "    " MSG "\n", __LINE__, __FILE__, ## __VA_ARGS__)
#define pg_vlog(TYPE, MSG, VA_ARGS) \
    pg_vlog_(TYPE, "    " MSG "\n", __LINE__, __FILE__, VA_ARGS)

/** Emit a log message without indentation at the front or a newline at the end.
    \detail Just like pg_log, but useful for calling multiple times (with
            PG_LOG_CONTD) to compose more complex log messages.
    \param TYPE     The severity level of the log message
    \param MSG      A printf-style format string
    \param ...      Formatted tokens for MSG
*/
#define pg_log_direct(TYPE, MSG, ...) \
    pg_log_direct_(TYPE, MSG, __LINE__, __FILE__, ## __VA_ARGS__)
#define pg_vlog_direct(TYPE, MSG, VA_ARGS) \
    pg_vlog_direct_(TYPE, MSG, __LINE__, __FILE__, VA_ARGS)


void pg_log_(enum pg_log_type type, const char* fmt,
            int src_line, const char* src_filename, ...);
void pg_vlog_(enum pg_log_type type, const char* fmt,
            int src_line, const char* src_filename, va_list args);
void pg_log_direct_(enum pg_log_type type, const char* fmt,
            int src_line, const char* src_filename, ...);
void pg_vlog_direct_(enum pg_log_type type, const char* fmt,
            int src_line, const char* src_filename, va_list args);

#endif
