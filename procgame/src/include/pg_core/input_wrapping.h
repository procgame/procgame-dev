#pragma once

/*  Boolean input state (with extra info to indicate rising/falling edges   */
typedef uint8_t pg_input_state_t;
#define PG_INPUT_OFF        (0)
#define PG_INPUT_ON         (1 << 0)    /*  Control is on at all (hit OR held)  */
#define PG_INPUT_HIT        (1 << 1)    /*  Control was pressed on this frame   */
#define PG_INPUT_HELD       (1 << 2)    /*  Control is being held down  */
#define PG_INPUT_RELEASED   (1 << 3)    /*  Control was released on this frame  */

/*  Analog input state, with boolean states included for the four cardinal directions
    and a 'press' boolean input */
struct pg_input_analog {
    vec2 position;
    vec2 deadzone;
    vec2 boolean_threshold;
    pg_input_state_t analog_boolean[4];
    pg_input_state_t press_boolean;
};


/*  Generic input wrapper. Implement arbitrary input schemes by polling whatever
    input source (SDL, OpenVR, keybind re-mapper...) and updating the individual
    input states in the wrapper, so it can be checked with the interface below
    in a consistent way between input sources   */
struct pg_input_wrapper {
    pg_input_state_t boolean_states[256];
    struct pg_input_analog analog_states[32];
};

void pg_input_wrapper_step(struct pg_input_wrapper* iw);
void pg_input_wrapper_reset(struct pg_input_wrapper* iw);

/*  Boolean inputs (ie. buttons/button-like controls)   */
void pg_input_boolean_action(struct pg_input_wrapper* iw, int boolean_idx, bool hit);
pg_input_state_t pg_input_get_boolean(struct pg_input_wrapper* iw, int boolean_idx);

/*  Analog inputs (1-2 axis)  */
void pg_input_set_analog(struct pg_input_wrapper* iw, int analog_idx, vec2 value);
void pg_input_add_analog(struct pg_input_wrapper* iw, int analog_idx, vec2 value);
vec2 pg_input_get_analog(struct pg_input_wrapper* iw, int analog_idx);
pg_input_state_t pg_input_get_analog_boolean(struct pg_input_wrapper* iw, int analog_idx, ivec2 dir);

void pg_input_wrapper_map_booleans(struct pg_input_wrapper* dst, const struct pg_input_wrapper* src,
        int n_mappings, ivec2* mappings);
