/** @file time.h
 *  @brief Timing for logic or performance metrics
 */

#include <stdint.h>

/** @name       Time and Timing
 *  @{  */

double pg_time(void); // Time in seconds
double pg_frame_timestamp(void); // Call once per frame (for framerate and perf metrics)
double pg_framerate(void);   // Get framerate without recalculating it
/*  Granular performance testing using SDL2's performance counter   */
uint64_t pg_perf_time(void);
double pg_perf_time_diff(uint64_t start, uint64_t end);

#define PG_PERF_DURATION(...) ({ \
    uint64_t perf_start = pg_perf_time(); \
    __VA_ARGS__; \
    uint64_t perf_end = pg_perf_time(); \
    pg_perf_time_diff(perf_start, perf_end); })

/*  @}  */
