#ifndef PG_MEMPOOL_INCLUDED
#define PG_MEMPOOL_INCLUDED

#include <stdint.h>

typedef uint64_t pg_mempool_id_t;
#define PG_MEMPOOL_NULL     0

/************************************************/
/*  REGULAR MEMORY POOL                         */
/************************************************/

#define PG_MEMPOOL_DECLARE(TYPE, PREFIX) \
    typedef struct { \
        uint32_t uid; \
        ARR_T(TYPE) pool; \
        ARR_T(uint32_t) free_slots; \
    } PREFIX##_pool_t; \
    /*  Top 32 bits = unique ID; Bottom 32 bits = pool index    */ \
    typedef uint64_t    PREFIX##_id_t; \
    void                PREFIX##_pool_init(PREFIX##_pool_t* pool); \
    void                PREFIX##_pool_deinit(PREFIX##_pool_t* pool); \
    void                PREFIX##_pool_clear(PREFIX##_pool_t* pool); \
    PREFIX##_id_t       PREFIX##_alloc(PREFIX##_pool_t* pool, TYPE** ptr); \
    void                PREFIX##_free(PREFIX##_pool_t* pool, PREFIX##_id_t id); \
    TYPE*               PREFIX##_get(PREFIX##_pool_t* pool, PREFIX##_id_t id); \
    PREFIX##_id_t       PREFIX##_id(PREFIX##_pool_t* pool, TYPE* ptr);

#define PG_MEMPOOL_DEFINE(TYPE, PREFIX, ALLOC_FIELD) \
void            PREFIX##_pool_init(PREFIX##_pool_t* pool) \
{ \
    pool->uid = 1; \
    ARR_INIT(pool->pool); \
    ARR_INIT(pool->free_slots); \
} \
void            PREFIX##_pool_deinit(PREFIX##_pool_t* pool) \
{ \
    ARR_DEINIT(pool->pool); \
    ARR_DEINIT(pool->free_slots); \
} \
\
void            PREFIX##_pool_clear(PREFIX##_pool_t* pool) \
{ \
    pool->uid = 1; \
    ARR_TRUNCATE(pool->free_slots, 0); \
    uint32_t i; \
    for(i = 0; i < pool->pool.cap; ++i) { \
        pool->pool.data[i] = (TYPE){}; \
        ARR_PUSH(pool->free_slots, i); \
    } \
} \
\
PREFIX##_id_t   PREFIX##_alloc(PREFIX##_pool_t* pool, TYPE** ptr) \
{ \
    TYPE new_item = (TYPE){0}; \
    TYPE* new_item_ptr = NULL; \
    uint32_t idx = 0; \
    if(!pool->free_slots.len) { \
        idx = pool->pool.len; \
        new_item.ALLOC_FIELD = ((uint64_t)pool->uid << 32) | (uint64_t)idx; \
        ARR_PUSH(pool->pool, new_item); \
        new_item_ptr = &pool->pool.data[pool->pool.len-1]; \
    } else { \
        idx = ARR_POP(pool->free_slots); \
        new_item.ALLOC_FIELD = ((uint64_t)pool->uid << 32) | (uint64_t)idx; \
        pool->pool.data[idx] = new_item; \
        new_item_ptr = &pool->pool.data[idx]; \
    } \
    PREFIX##_id_t final_id = ((uint64_t)pool->uid << 32) | (uint64_t)idx; \
    ++pool->uid; \
    if(ptr) *ptr = new_item_ptr; \
    return final_id; \
} \
\
void            PREFIX##_free(PREFIX##_pool_t* pool, PREFIX##_id_t id) \
{ \
    if(!id) return; \
    uint32_t idx = id & 0xFFFFFFFF; \
    if(idx >= pool->pool.cap) return; \
    if(pool->pool.data[idx].ALLOC_FIELD != id) return; \
    pool->pool.data[idx].ALLOC_FIELD = 0; \
    ARR_PUSH(pool->free_slots, idx); \
} \
\
TYPE*           PREFIX##_get(PREFIX##_pool_t* pool, PREFIX##_id_t id) \
{ \
    if(!id) return NULL; \
    uint32_t idx = id & 0xFFFFFFFF; \
    if(idx >= pool->pool.cap) return NULL; \
    if(pool->pool.data[idx].ALLOC_FIELD != id) return NULL; \
    return &pool->pool.data[idx]; \
} \
\
PREFIX##_id_t   PREFIX##_id(PREFIX##_pool_t* pool, TYPE* ptr) \
{ \
    return ptr->ALLOC_FIELD; \
}

/************************************************/
/*  GLOBAL MEMORY POOL                          */
/************************************************/

#define PG_MEMPOOL_DECLARE_GLOBAL(TYPE, PREFIX) \
    /*  Top 32 bits = unique ID; Bottom 32 bits = pool index    */ \
    typedef uint64_t    PREFIX##_id_t; \
    void                PREFIX##_pool_init(void); \
    void                PREFIX##_pool_deinit(void); \
    void                PREFIX##_pool_clear(void); \
    PREFIX##_id_t       PREFIX##_alloc(TYPE** ptr); \
    void                PREFIX##_free(PREFIX##_id_t id); \
    TYPE*               PREFIX##_get(PREFIX##_id_t id); \
    PREFIX##_id_t       PREFIX##_id(TYPE* ptr);

#define PG_MEMPOOL_DEFINE_GLOBAL(TYPE, PREFIX, ALLOC_FIELD) \
static struct { \
    uint32_t uid; \
    ARR_T(TYPE) pool; \
    ARR_T(uint32_t) free_slots; \
} pool; \
void            PREFIX##_pool_init(void) \
{ \
    pool.uid = 1; \
    ARR_INIT(pool.pool); \
    ARR_INIT(pool.free_slots); \
} \
void            PREFIX##_pool_deinit(void) \
{ \
    ARR_DEINIT(pool.pool); \
    ARR_DEINIT(pool.free_slots); \
} \
\
void            PREFIX##_pool_clear(void) \
{ \
    ARR_TRUNCATE(pool.free_slots, 0); \
    uint32_t i; \
    for(i = 0; i < pool.pool.cap; ++i) { \
        pool.pool.data[i] = (TYPE){}; \
        ARR_PUSH(pool.free_slots, i); \
    } \
} \
\
PREFIX##_id_t   PREFIX##_alloc(TYPE** ptr) \
{ \
    TYPE new_item = (TYPE){ .ALLOC_FIELD = pool.uid }; \
    TYPE* new_item_ptr = NULL; \
    uint32_t idx = 0; \
    if(!pool.free_slots.len) { \
        idx = pool.pool.len; \
        new_item.ALLOC_FIELD = ((uint64_t)pool.uid << 32) | (uint64_t)idx; \
        ARR_PUSH(pool.pool, new_item); \
        new_item_ptr = &pool.pool.data[pool.pool.len-1]; \
    } else { \
        idx = ARR_POP(pool.free_slots); \
        new_item.ALLOC_FIELD = ((uint64_t)pool.uid << 32) | (uint64_t)idx; \
        pool.pool.data[idx] = new_item; \
        new_item_ptr = &pool.pool.data[idx]; \
    } \
    PREFIX##_id_t final_id = ((uint64_t)pool.uid << 32) | (uint64_t)idx; \
    ++pool.uid; \
    if(ptr) *ptr = new_item_ptr; \
    return final_id; \
} \
\
void            PREFIX##_free(PREFIX##_id_t id) \
{ \
    if(!id) return; \
    uint32_t idx = id & 0xFFFFFFFF; \
    if(idx >= pool.pool.cap) return; \
    if(pool.pool.data[idx].ALLOC_FIELD != id) return; \
    pool.pool.data[idx].ALLOC_FIELD = 0; \
    ARR_PUSH(pool.free_slots, idx); \
} \
\
TYPE*           PREFIX##_get(PREFIX##_id_t id) \
{ \
    if(!id) return NULL; \
    uint32_t idx = id & 0xFFFFFFFF; \
    if(idx >= pool.pool.cap) return NULL; \
    if(pool.pool.data[idx].ALLOC_FIELD != id) return NULL; \
    return &pool.pool.data[idx]; \
} \
\
PREFIX##_id_t   PREFIX##_id(TYPE* ptr) \
{ \
    return ptr->ALLOC_FIELD; \
}

#endif
