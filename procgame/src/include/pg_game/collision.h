
struct pg_collision {
    int did_collide;
    float sep_len2;
    vec3 sep_axis;
    float impulse[2];
};

#define PG_COLLIDER_STATIC      (1 << 0)
#define PG_COLLIDER_SOFT        (1 << 1)

struct pg_collider {
    uint32_t flags;
    float mass;
    vec3 pos, vel;
    enum pg_collider_type {
        PG_NO_COLLIDER,
        PG_COLLIDER_SPHERE,
        PG_COLLIDER_AABB,
        PG_COLLIDER_OBB,
        PG_COLLIDER_HEIGHTMAP,
        PG_NUM_COLLIDER_TYPES,
    } type;
    union {
        sphere sph;
        aabb3D box;
        obb3D obb;
        struct pg_heightmap hmap;
    };

    vec3 push, push_norm;
    float push_len2, push_len;
    vec3 vel_impulse;
};

extern const char* PG_COLLIDER_NAME[PG_NUM_COLLIDER_TYPES];

#define PG_COLLIDER_SPHERE(P, R, ...) \
    ((struct pg_collider){ .type = PG_COLLIDER_SPHERE, .mass = 1, \
                           .sph = SPHERE(P, R), __VA_ARGS__ })

#define PG_COLLIDER_AABB(B0, B1, ...) \
    ((struct pg_collider){ .type = PG_COLLIDER_AABB, .mass = 1, \
                           .box = AABB3D(B0, B1), __VA_ARGS__ })

#define PG_COLLIDER_OBB(B0, B1, ROT, ...) \
    ((struct pg_collider){ .type = PG_COLLIDER_OBB, .mass = 1, \
                           .obb = OBB3D(AABB3D(B0, B1), ROT), __VA_ARGS__ })

#define PG_COLLIDER_HEIGHTMAP(HMAP, ...) \
    ((struct pg_collider){ .type = PG_COLLIDER_HEIGHTMAP, .mass = 1, \
                           .hmap = HMAP, __VA_ARGS__ })

/*  Calculate a collision between any two colliders */
vec3 pg_collider_sep_axis(struct pg_collider* c0, struct pg_collider* c1);
void pg_collision_calculate(struct pg_collision* out,
                            struct pg_collider* c0, struct pg_collider* c1);
float pg_collider_raycast(struct pg_collider* c0, vec3 src, vec3 dir, float max_len);

/*  Get bounding box for any collider type  */
aabb3D pg_collider_get_bound(struct pg_collider* c0);

/*  Finish colliding before it can respond  */
int pg_collider_finish(struct pg_collider* coll);

/*  Reset to begin a new set of collisions  */
void pg_collider_reset(struct pg_collider* coll);

/*  Collision responses */
void pg_collider_respond(struct pg_collider* coll, float k);
void pg_collider_respond_walk(struct pg_collider* coll);

/*  Debug   */
void pg_debug_print_collision(struct pg_collision* coll,
                              struct pg_collider* c0, char* name0,
                              struct pg_collider* c1, char* name1);
