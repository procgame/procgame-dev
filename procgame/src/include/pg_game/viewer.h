struct pg_viewer {
    struct pg_gfx_transform view_tx;
    mat4 projection;
    mat4 projview;
    mat4 projview_inverse;

    /*  Perspective projection parameters   */
    float fov;
    float aspect_ratio;
    vec2 near_far;

    /*  Orthographic projection parameters  */
    vec2 size;
};

void pg_viewer_perspective(struct pg_viewer* view, struct pg_gfx_transform* tx,
                           float aspect_ratio, float fov, vec2 near_far);
void pg_viewer_ortho(struct pg_viewer* view, struct pg_gfx_transform* tx, vec2 size);
vec3 pg_viewer_project(struct pg_viewer* view, vec3 pos);
vec3 pg_viewer_unproject(struct pg_viewer* view, vec3 pos);
vec2 pg_viewer_unproject2(struct pg_viewer* view, vec2 pos);

