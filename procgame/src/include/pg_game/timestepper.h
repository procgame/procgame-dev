struct pg_timestepper {
    double steps_per_second;
    double step_length;
    double time_speed;
    /*  Clock   */
    double time_secs;
    double time_steps;
    double time_secs_real;
    /*  Discrete step counter   */
    uint32_t steps_done;
};

void pg_timestepper_init(struct pg_timestepper* ts, double steps_per_second);
int pg_timestepper_step(struct pg_timestepper* ts, double new_time);
double pg_timestepper_deltatime(struct pg_timestepper* ts, double current_time);
