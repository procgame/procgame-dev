struct pg_heightmap {
    float* map;
    ivec2 map_size;
    float cell_size;
};

#define PG_HEIGHTMAP_FOREACH(HMAP, XVAR, YVAR, PVAR) \
    float* PVAR = (HMAP).map; \
    for(int YVAR = 0; YVAR < (HMAP).map_size.x; ++YVAR) \
    for(int XVAR = 0; XVAR < (HMAP).map_size.x; ++XVAR, ++PVAR)

void pg_heightmap_init(struct pg_heightmap* hmap, ivec2 map_size, float cell_size);
void pg_heightmap_deinit(struct pg_heightmap* hmap);
void pg_heightmap_from_wave(struct pg_heightmap* hmap, struct pg_wave* wave,
                            float fx, float fy);
void pg_heightmap_set_height(struct pg_heightmap* hmap, int x, int y, float h);
float pg_heightmap_get_height(struct pg_heightmap* hmap, int x, int y);
float pg_heightmap_get_height_lerp(struct pg_heightmap* hmap, float x, float y);
