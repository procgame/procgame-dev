#pragma once

/**
    \defgroup gpu_shader_stage GPU Shader Stages
    \ingroup gpu_io
    \{
*/

#define PG_GPU_SHADER_MAX_SHADERS   2

/** \brief Opaque pointer to a compiled GPU shader stage    */
typedef struct pg_gpu_shader_stage pg_gpu_shader_stage_t;

/** Create a shader from GLSL source code   */
pg_gpu_shader_stage_t* pg_gpu_shader_stage_from_glsl(
        const char* glsl, int glsl_len, enum pg_gpu_shader_stage_type type);
/** Destroy a shader stage  */
void pg_gpu_shader_stage_destroy(pg_gpu_shader_stage_t* sh_stage);

/** Check if a shader stage was compiled successfully   */
bool pg_gpu_shader_stage_status(pg_gpu_shader_stage_t* sh_stage);
/** Returns the GPU shader compilation log */
const char* pg_gpu_shader_stage_log(pg_gpu_shader_stage_t* sh_stage);
/** Get the length of the GPU shader compilation log   */
int pg_gpu_shader_stage_log_len(pg_gpu_shader_stage_t* sh_stage);

/** \}  */



