typedef struct pg_gpu_renderer pg_gpu_renderer_t;

pg_gpu_renderer_t* pg_gpu_renderer_create(void);
void pg_gpu_renderer_destroy(pg_gpu_renderer_t* stage);

pg_gpu_framebuffer_t* pg_gpu_renderer_get_output(pg_gpu_renderer_t* render);
void pg_gpu_renderer_add_stage(pg_gpu_renderer_t* render, pg_gpu_render_stage_t* stage);
void pg_gpu_renderer_execute(pg_gpu_renderer_t* render, uint32_t output_filter);



