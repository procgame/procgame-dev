#pragma once

#include "pg_gpu/pipeline_state.h"

typedef struct pg_gpu_pipeline pg_gpu_pipeline_t;

pg_gpu_pipeline_t* pg_gpu_pipeline_create(pg_gpu_shader_program_t* sh_prog,
        struct pg_gpu_depth_state* depth_state,
        struct pg_gpu_stencil_state* stencil_state,
        struct pg_gpu_blending_state* blending_state);
pg_gpu_shader_program_t* pg_gpu_pipeline_get_shader(pg_gpu_pipeline_t* pipeline);

void pg_gpu_pipeline_destroy(pg_gpu_pipeline_t* pipeline);

void pg_gpu_pipeline_bind(pg_gpu_pipeline_t* pipeline);
