struct pg_buffer_builder {
    struct pg_buffer* dst_buffer;
    ivec2 range;
    int index;
};

void pg_buffer_builder_init(struct pg_buffer_builder* builder, struct pg_buffer* dst_buffer, ivec2 range);
void* pg_buffer_builder_get_element(struct pg_buffer_builder* builder);
int pg_buffer_builder_push_element(struct pg_buffer_builder* builder, void* data);
int pg_buffer_builder_push_elements(struct pg_buffer_builder* builder, int n_elements, void* data);
ivec2 pg_buffer_builder_get_built_range(struct pg_buffer_builder* builder);

struct pg_vertex_buffer_builder {
    struct pg_buffer_builder verts_builder;
    struct pg_buffer_builder idx_builder;
};

void pg_vertex_buffer_builder_init(struct pg_vertex_buffer_builder* builder,
        struct pg_buffer* verts_buffer, ivec2 verts_range,
        struct pg_buffer* idx_buffer, ivec2 idx_range);
int pg_vertex_buffer_builder_add_vertex(struct pg_vertex_buffer_builder* builder, void* data);
int pg_vertex_buffer_builder_add_triangle(struct pg_vertex_buffer_builder* builder, uint32_t v0, uint32_t v1, uint32_t v2);

