/**
    \file texture.h
    \ingroup gpu
    \brief Texture management
*/

#pragma once

/**
    \defgroup texture Texture data
    \ingroup gpu
    \{
*/

typedef struct {
    union {
        vec4 v;
        vec2 uv[2];
    };
    int layer;
} pg_tex_frame_t;

static inline pg_tex_frame_t pg_tex_frame(vec2 top_left, vec2 bottom_right, int layer)
{
    return (pg_tex_frame_t){
        .uv = { top_left, bottom_right },
        .layer = layer,
    };
}

/** Texture data stored in local memory (not GPU memory)   */
struct pg_texture {
    /** Buffer storing actual pixel data    */
    struct pg_buffer buf;
    /** Data type of each pixel 
        \remark This is the type of the data buffer's 'pixel' attribute */
    enum pg_data_type pixel_type;
    /** Size of the texture (width, height, layers) */
    ivec3 dimensions;
    /** Stride in pixels across a full layer of a multi-layer texture    */
    int layer_stride;
    /** Number of color channels in the texture */
    int color_channels;
    /** Size of the source data that was used to fill the texture layers (if it existed)  */
    SARR_T(16, ivec2) layer_source_sizes;
};

/** Initialize a texture with the given pixel type and dimensions   */
bool pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int width, int height, int layers);
/** Initialize a texture from a file path   */
bool pg_texture_init_from_path(struct pg_texture* tex, const char* filename);
/** Initialize a texture from a file    */
bool pg_texture_init_from_file(struct pg_texture* tex, struct pg_file* file);
/** Initialize a texture from multiple files as layers  */
bool pg_texture_from_files(struct pg_texture* tex, struct pg_file** files, int num_files);
/** Like above but can output the sizes of all the images loaded from each file */
bool pg_texture_from_files_get_info(struct pg_texture* tex, struct pg_file** files, int num_files, ivec2* sizes);
/** Save a texture to a file    */
bool pg_texture_save_to_file(struct pg_texture* tex, const char* filename);
/** Deallocate a texture    */
void pg_texture_deinit(struct pg_texture* tex);
/** Get a pointer to texture pixel data */
uint8_t* pg_texture_get_pixel_ptr(struct pg_texture* tex, int x, int y, int layer);
/** Check that two textures are exactly equal (same size and exactly identical contents)    */
bool pg_texture_compare_equal(struct pg_texture* tex0, struct pg_texture* tex1);

/** \}  */

/**
    \defgroup gpu_texture GPU Textures
    \ingroup texture
    \ingroup gpu_io
    \{
*/
/** Parameters for a GPU texture    */
typedef struct pg_gpu_texture_params {
    enum pg_gpu_texture_param_wrap {
        PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_EDGE,
        PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_BORDER,
        PG_GPU_TEXTURE_PARAM_WRAP_REPEAT,
        PG_GPU_TEXTURE_PARAM_WRAP_MIRRORED_REPEAT,
    } wrap_x, wrap_y, wrap_z;
    enum pg_gpu_texture_param_filter {
        PG_GPU_TEXTURE_PARAM_FILTER_NEAREST,
        PG_GPU_TEXTURE_PARAM_FILTER_LINEAR,
        PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_NEAREST,
        PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_NEAREST,
        PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_LINEAR,
        PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_LINEAR,
    } filter_min, filter_mag;
    enum pg_gpu_texture_param_swizzle {
        PG_GPU_TEXTURE_PARAM_SWIZZLE_RED,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_GREEN,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_BLUE,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_ALPHA,
    } swizzle[4];
    enum pg_gpu_texture_param_ds_mode {
        PG_GPU_TEXTURE_PARAM_DS_DEPTH,
        PG_GPU_TEXTURE_PARAM_DS_STENCIL,
    } depth_stencil_mode;
    float min_lod, max_lod, lod_bias;
    int base_mipmap, max_mipmap;
    vec4 border_color;
} pg_gpu_texture_params_t;

#define PG_GPU_TEXTURE_PARAMS(...) \
    (struct pg_gpu_texture_params){ \
        .wrap_x = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT, \
        .wrap_y = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT, \
        .wrap_z = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT, \
        .filter_min = PG_GPU_TEXTURE_PARAM_FILTER_NEAREST, \
        .filter_mag = PG_GPU_TEXTURE_PARAM_FILTER_NEAREST, \
        .swizzle = { \
            PG_GPU_TEXTURE_PARAM_SWIZZLE_RED, \
            PG_GPU_TEXTURE_PARAM_SWIZZLE_GREEN, \
            PG_GPU_TEXTURE_PARAM_SWIZZLE_BLUE, \
            PG_GPU_TEXTURE_PARAM_SWIZZLE_ALPHA, \
        }, \
        .depth_stencil_mode = PG_GPU_TEXTURE_PARAM_DS_DEPTH, \
        __VA_ARGS__ \
    }



/** Opaque pointer to an uploaded GPU texture   */
typedef struct pg_gpu_texture pg_gpu_texture_t;

/** Usage type of an uploaded GPU texture   */
enum pg_gpu_texture_type {
    PG_GPU_IMAGE_TEXTURE,           /**< Regular texture  */
    PG_GPU_DEPTH_TEXTURE,           /**< Texture to store a depth buffer    */
    PG_GPU_DEPTH_STENCIL_TEXTURE,   /**< Texture to store a combined depth+stencil buffer   */
    PG_GPU_INVALID_TEXTURE,         /**< Invalid texture (not suitable for rendering with)  */
};

/** Pixel format of an uploaded GPU texture */
enum pg_gpu_texture_format {
    PG_GPU_TEXTURE_INVALID_FORMAT,
    PG_GPU_TEXTURE_R8_UNORM,    PG_GPU_TEXTURE_R16_UNORM,
    PG_GPU_TEXTURE_R8 = PG_GPU_TEXTURE_R8_UNORM, PG_GPU_TEXTURE_R16 = PG_GPU_TEXTURE_R16_UNORM,
    PG_GPU_TEXTURE_R8_SNORM,    PG_GPU_TEXTURE_R16_SNORM,
    PG_GPU_TEXTURE_R8UI,        PG_GPU_TEXTURE_R16UI,        PG_GPU_TEXTURE_R32UI,
    PG_GPU_TEXTURE_R8I,         PG_GPU_TEXTURE_R16I,         PG_GPU_TEXTURE_R32I,
    PG_GPU_TEXTURE_R32F,
    PG_GPU_TEXTURE_RG8_UNORM,   PG_GPU_TEXTURE_RG16_UNORM,
    PG_GPU_TEXTURE_RG8 = PG_GPU_TEXTURE_RG8_UNORM, PG_GPU_TEXTURE_RG16 = PG_GPU_TEXTURE_RG16_UNORM,
    PG_GPU_TEXTURE_RG8_SNORM,   PG_GPU_TEXTURE_RG16_SNORM,
    PG_GPU_TEXTURE_RG8UI,       PG_GPU_TEXTURE_RG16UI,       PG_GPU_TEXTURE_RG32UI,
    PG_GPU_TEXTURE_RG8I,        PG_GPU_TEXTURE_RG16I,        PG_GPU_TEXTURE_RG32I,
    PG_GPU_TEXTURE_RG32F,
    PG_GPU_TEXTURE_RGB8_UNORM,  PG_GPU_TEXTURE_RGB16_UNORM,
    PG_GPU_TEXTURE_RGB8 = PG_GPU_TEXTURE_RGB8_UNORM, PG_GPU_TEXTURE_RGB16 = PG_GPU_TEXTURE_RGB16_UNORM,
    PG_GPU_TEXTURE_RGB8_SNORM,  PG_GPU_TEXTURE_RGB16_SNORM,
    PG_GPU_TEXTURE_RGB8UI,      PG_GPU_TEXTURE_RGB16UI,      PG_GPU_TEXTURE_RGB32UI,
    PG_GPU_TEXTURE_RGB8I,       PG_GPU_TEXTURE_RGB16I,       PG_GPU_TEXTURE_RGB32I,
    PG_GPU_TEXTURE_RGB32F,
    PG_GPU_TEXTURE_RGBA8_UNORM, PG_GPU_TEXTURE_RGBA16_UNORM,
    PG_GPU_TEXTURE_RGBA8 = PG_GPU_TEXTURE_RGBA8_UNORM, PG_GPU_TEXTURE_RGBA16 = PG_GPU_TEXTURE_RGBA16_UNORM,
    PG_GPU_TEXTURE_RGBA8_SNORM, PG_GPU_TEXTURE_RGBA16_SNORM,
    PG_GPU_TEXTURE_RGBA8UI,     PG_GPU_TEXTURE_RGBA16UI,     PG_GPU_TEXTURE_RGBA32UI,
    PG_GPU_TEXTURE_RGBA8I,      PG_GPU_TEXTURE_RGBA16I,      PG_GPU_TEXTURE_RGBA32I,
    PG_GPU_TEXTURE_RGBA32F,
    PG_GPU_TEXTURE_DEPTH16, PG_GPU_TEXTURE_DEPTH24, PG_GPU_TEXTURE_DEPTH32, PG_GPU_TEXTURE_DEPTH32F,
    PG_GPU_TEXTURE_DEPTH24_STENCIL8, PG_GPU_TEXTURE_DEPTH32F_STENCIL8,
    PG_GPU_TEXTURE_STENCIL8,
    PG_GPU_TEXTURE_N_FORMATS,
};

/** Get a GPU texture format from its string representation (eg. "RGBA32F") */
enum pg_gpu_texture_format pg_gpu_texture_format_from_string(const char* string);
enum pg_gpu_texture_type pg_gpu_texture_format_type(enum pg_gpu_texture_format format);

/** Upload a texture to the GPU */
pg_gpu_texture_t* pg_texture_upload(struct pg_texture* tex);
/** Download a copy of the pixel data from the GPU  */
void pg_gpu_texture_download(pg_gpu_texture_t* gpu_tex, struct pg_texture* output);
/** Apply parameters to a bound texture */
void pg_gpu_texture_params_apply(struct pg_gpu_texture_params* params, pg_gpu_texture_t* tex);

/** Allocate a texture on the GPU with no local source  */
pg_gpu_texture_t* pg_gpu_texture_alloc(enum pg_gpu_texture_format format, int width, int height, int layers);
pg_gpu_texture_t* pg_gpu_texture_alloc_2D(enum pg_gpu_texture_format format, int width, int height);
/** Destroy a GPU texture   */
void pg_gpu_texture_deinit(pg_gpu_texture_t* gpu_tex);
/** Get the format of a GPU texture */
enum pg_gpu_texture_format pg_gpu_texture_get_format(pg_gpu_texture_t* gpu_tex);
/** Get the format type of a GPU texture    */
enum pg_gpu_texture_type pg_gpu_texture_get_type(pg_gpu_texture_t* gpu_tex);
/** Get the dimensions of a GPU texture */
ivec3 pg_gpu_texture_get_dimensions(pg_gpu_texture_t* gpu_tex);
bool pg_gpu_texture_is_3D(pg_gpu_texture_t* gpu_tex);

/**
    \brief  Opaque pointer to a "view" of a GPU texture
    \detail A view of a texture represents a sub-range of a full texture
            with some extra metadata specific to the view. The sub-range
            is in terms of texture layers, not sub-rectangles.
**/
typedef struct pg_gpu_texture_view pg_gpu_texture_view_t;

/** Create a view of a whole GPU texture    */
pg_gpu_texture_view_t* pg_gpu_texture_get_basic_view(pg_gpu_texture_t* gpu_tex);
/** Create a view of a single layer of a GPU texture    */
pg_gpu_texture_view_t* pg_gpu_texture_get_layer_view(pg_gpu_texture_t* gpu_tex, int layer);
/** Create a view of a range of layers of a GPU texture */
pg_gpu_texture_view_t* pg_gpu_texture_get_range_view(pg_gpu_texture_t* gpu_tex,
                                                 int layer_start, int n_layers);
/** Deinitialize a view of a GPU texture    */
void pg_gpu_texture_view_deinit(pg_gpu_texture_view_t* gpu_tex_view);

/** Get the range of layers of a GPU texture view   */
ivec2 pg_gpu_texture_view_get_range(pg_gpu_texture_view_t* gpu_tex_view);
/** Get the GPU texture associated with a view  */
pg_gpu_texture_t* pg_gpu_texture_view_get_base(pg_gpu_texture_view_t* gpu_tex_view);


/** \}  */
