/**
    \file buffer.h
    \ingroup gpu
    \brief Buffer management
*/

#pragma once

/**
    \defgroup buffer Buffer data
    \ingroup gpu
    \{
*/

/*
    {  [ ]    ,  [ ]    ,  [ ]    ,  [ ]    }   <- buffer
    --- |        ---       ----------
     |  attribute |          |
     offset       size       stride
*/

/** Maximum length of a buffer attribute's name */
#define PG_BUFFER_ATTRIBUTE_NAME_MAX_LEN    32
/** Maximum number of attributes in a buffer    */
#define PG_BUFFER_MAX_ATTRIBUTES            16

/** Minimal info to declare a buffer attribute  */
struct pg_buffer_attribute_info {
    /** Unique identifier for the attribute */
    char name[PG_BUFFER_ATTRIBUTE_NAME_MAX_LEN];
    /** Type of the attribute   */
    enum pg_data_type type;
};

/** Layout definition for a buffer to be created later  */
struct pg_buffer_layout {
    enum pg_buffer_attribute_packing {
        PG_BUFFER_PACKING_INVALID,
        PG_BUFFER_PACKING_4BYTE,
        PG_BUFFER_PACKING_PACKED,
        PG_BUFFER_PACKING_STD140,
    } attribute_packing;
    int element_alignment;
    SARR_T(PG_BUFFER_MAX_ATTRIBUTES, struct pg_buffer_attribute_info) attributes;
    enum pg_data_type packing_data_type;
};

enum pg_buffer_attribute_packing pg_buffer_attribute_packing_from_string(const char* str);

#define PG_BUFFER_LAYOUT(...) { \
    .attribute_packing = PG_BUFFER_PACKING_PACKED, \
    .packing_data_type = PG_NULL, \
    .element_alignment = 1, \
    .attributes = SARR_DATA(PG_BUFFER_MAX_ATTRIBUTES, 0, {}), \
    __VA_ARGS__ }

#define PG_BUFFER_ATTRIBUTE_PACKING(PACKING) \
    .attribute_packing = (PACKING)

#define PG_BUFFER_PACKING_DATA_TYPE(TYPE) \
    .packing_data_type = (TYPE)

#define PG_BUFFER_ELEMENT_ALIGNMENT(ALIGNMENT) \
    .element_alignment = (ALIGNMENT)

#define PG_BUFFER_ATTRIBUTES_LIST(N_ATTRIBS, ...) \
    .attributes = SARR_DATA(PG_BUFFER_MAX_ATTRIBUTES, (N_ATTRIBS), __VA_ARGS__)

#define PG_BUFFER_ATTRIBUTE(NAME, TYPE) { .name = (NAME), .type = (TYPE) }

#define PG_BUFFER_LAYOUT_SIMPLE(NAME, TYPE) \
    PG_BUFFER_LAYOUT(PG_BUFFER_ATTRIBUTES_LIST(1, PG_BUFFER_ATTRIBUTE(NAME, TYPE)))

/** Attribute in an existing buffer     */
struct pg_buffer_attribute {
    /** Declared attribute  */
    struct pg_buffer_attribute_info info;
    /** Index in the parent buffer's attributes array   */
    int index;
    /** Bytes offset from the beginning of the buffer   */
    int offset;
    /** Size of the attribute's data for one element    */
    int size;
    /** Bytes distance from the beginning of one element to the beginning of the next   */
    int stride;
    /** Bytes inserted after each element to satisfy alignment rules    */
    int pad_after;
};

typedef SARR_T(16, struct pg_buffer_attribute) pg_buffer_attribute_arr_t;
typedef SARR_T(128, struct pg_buffer_range {
    char name[128];
    union {
        struct { int first, count; };
        ivec2 range;
    };
}) pg_buffer_range_arr_t;

/** Structured buffer data in client memory (not on the GPU)    */
struct pg_buffer {
    /** Data type that the buffer attributes are packed into (for eg. texture buffers)  */
    enum pg_data_type packing_data_type;
    /** All attributes in the buffer    */
    pg_buffer_attribute_arr_t attributes;
    /** Allocated data buffer   */
    uint8_t* data;
    /** Size of an individual element in the buffer */
    int element_size;
    /** Number of elements in the buffer    */
    int data_elements;
    /** Total size of the buffer    */
    int data_size;
    /** Whether this buffer owns its data pointer (can resize, etc.)    */
    bool own_data;
    /*  List of named sub-ranges of the buffer  */
    pg_buffer_range_arr_t ranges;

};

/**
    \brief  Allocate a buffer with given attributes
    \detail Calculates necessary offsets, padding, etc. for all given
            attribute infos, and fills out full attribute structures in the
            final buffer structure.
    \return True if allocation succeeded, false otherwise.
*/
bool pg_buffer_init(struct pg_buffer* buf, const struct pg_buffer_layout* layout, int n_elements);
/** Deallocate a buffer */
void pg_buffer_deinit(struct pg_buffer* buf);

/** Resize the buffer to accommodate more or fewer elements */
bool pg_buffer_resize(struct pg_buffer* buf, int n_elements);

/** Get the index of an attribute in a buffer   */
int pg_buffer_get_attribute_index(struct pg_buffer* buf, const char* name);
/** Get a pointer to the computed info for a buffer attribute, by index  */
const struct pg_buffer_attribute* pg_buffer_get_attribute(struct pg_buffer* buf, int index);

/** Get a pointer to the first byte of a buffer element */
uint8_t* pg_buffer_get_element_ptr(struct pg_buffer* buf, int elem_idx);
/** Get a pointer to an attribute of a buffer element   */
uint8_t* pg_buffer_get_attribute_ptr(struct pg_buffer* buf, int elem_idx, int attrib_idx);

int pg_buffer_define_range(struct pg_buffer* buf, const char* name, int first, int count);
ivec2 pg_buffer_get_range_by_name(struct pg_buffer* buf, const char* name);
ivec2 pg_buffer_get_range(struct pg_buffer* buf, int idx);
int pg_buffer_get_range_count(struct pg_buffer* buf);
const char* pg_buffer_get_range_name(struct pg_buffer* buf, int idx);


/*  Initialize an empty buffer layout   */
void pg_buffer_layout_init(struct pg_buffer_layout* layout);

/*  Deinitialize a buffer layout    */
void pg_buffer_layout_deinit(struct pg_buffer_layout* layout);

/** \}  */


/**
    \defgroup gpu_buffer GPU Buffers
    \ingroup gpu_io
    \{
*/

/** Opaque pointer to an uploaded GPU buffer */
typedef struct pg_gpu_buffer pg_gpu_buffer_t;

/** Usage type for a GPU buffer */
enum pg_gpu_buffer_type {
    PG_GPU_INVALID_BUFFER,
    PG_GPU_VERTEX_BUFFER,   /**< Vertex input for a shader  */
    PG_GPU_TEXTURE_BUFFER,  /**< Data buffer for storage in a buffer texture    */
    PG_GPU_UNIFORM_BUFFER,  /**< Data buffer for uniform input to shaders   */
    PG_GPU_INDEX_BUFFER,    /**< Index buffer for vertex assembly   */
};

enum pg_gpu_buffer_type pg_gpu_buffer_type_from_string(const char* str);

/** Upload a pg_buffer to the GPU with the given usage type */
pg_gpu_buffer_t* pg_buffer_upload(struct pg_buffer* buf, enum pg_gpu_buffer_type type, bool keep_local_buffer);
/** Update data in a GPU buffer */
void pg_gpu_buffer_reupload(pg_gpu_buffer_t* gpu_buf);
/** Update data in a sub-range of a GPU buffer  */
void pg_gpu_buffer_reupload_range(pg_gpu_buffer_t* gpu_buf, int first_element, int n_elements);
void pg_gpu_buffer_set_local_data(pg_gpu_buffer_t* gpu_buf, struct pg_buffer* local_buf, bool own_local_data);

/** Allocate a GPU buffer with no initial data, with the given usage type   */
pg_gpu_buffer_t* pg_gpu_buffer_alloc(int size, enum pg_gpu_buffer_type type);
/** Destroy a GPU buffer    */
void pg_gpu_buffer_deinit(pg_gpu_buffer_t* gpu_buf);

int pg_gpu_buffer_define_range(struct pg_gpu_buffer* buf, const char* name, int first, int count);
ivec2 pg_gpu_buffer_get_range_by_name(struct pg_gpu_buffer* buf, const char* name);
ivec2 pg_gpu_buffer_get_range(struct pg_gpu_buffer* buf, int idx);
int pg_gpu_buffer_get_range_count(struct pg_gpu_buffer* buf);
const char* pg_gpu_buffer_get_range_name(struct pg_gpu_buffer* buf, int idx);

/** Get the index of an attribute in an uploaded GPU buffer */
int pg_gpu_buffer_get_attribute_index(pg_gpu_buffer_t* buf, const char* name);
/** Get number of attributes in a GPU buffer    */
int pg_gpu_buffer_get_attribute_count(pg_gpu_buffer_t* buf);
/** Get a pointer to the computed info for a buffer attribute, by index  */
const struct pg_buffer_attribute* pg_gpu_buffer_get_attribute(pg_gpu_buffer_t* buf, int index);
/** Get the size in bytes of a GPU buffer   */
int pg_gpu_buffer_get_size(pg_gpu_buffer_t* gpu_buf);
/** Get the size in bytes of a single element of a GPU buffer   */
int pg_gpu_buffer_get_element_size(pg_gpu_buffer_t* gpu_buf);
/** Get the type of a GPU buffer    */
enum pg_gpu_buffer_type pg_gpu_buffer_get_type(pg_gpu_buffer_t* gpu_buf);
/** Get the local data buffer associated with a GPU buffer  */
struct pg_buffer* pg_gpu_buffer_get_local_data(pg_gpu_buffer_t* gpu_buf);


/** \}  */
