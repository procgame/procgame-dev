/**
    \file framebuffer.h
    \ingroup gpu
    \brief Framebuffer management
*/

#pragma once

/**
    \defgroup gpu_framebuffer Framebuffers
    \ingroup gpu_io
    \{
*/

/** Attachment index for a single aspect of a framebuffer
    \remark For multiple color attachments, use PG_GPU_FRAMEBUFFER_COLOR + 'index
            of color attachment'
*/
enum pg_gpu_framebuffer_attachment_index {
    PG_GPU_FRAMEBUFFER_INVALID = 0,
    PG_GPU_FRAMEBUFFER_DEPTH,
    PG_GPU_FRAMEBUFFER_DEPTH_STENCIL,
    PG_GPU_FRAMEBUFFER_STENCIL,
    PG_GPU_FRAMEBUFFER_COLOR0,
    PG_GPU_FRAMEBUFFER_COLOR1,
    PG_GPU_FRAMEBUFFER_COLOR2,
    PG_GPU_FRAMEBUFFER_COLOR3,
    PG_GPU_FRAMEBUFFER_COLOR4,
    PG_GPU_FRAMEBUFFER_COLOR5,
    PG_GPU_FRAMEBUFFER_COLOR6,
    PG_GPU_FRAMEBUFFER_COLOR7,
};

#define PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN      14
enum pg_gpu_framebuffer_attachment_index pg_gpu_framebuffer_attachment_index_from_string(const char* str);

/** A single attachment description for a framebuffer layout    */
struct pg_gpu_framebuffer_attachment {
    enum pg_gpu_framebuffer_attachment_index index;
    enum pg_data_type type;
};

/** A full framebuffer layout
    \detail Can be used to generate a new compatible framebuffer, or to check
            compatibility of an existing framebuffer
*/
struct pg_gpu_framebuffer_layout {
    SARR_T(16, struct pg_gpu_framebuffer_attachment) attachments;
    ivec2 dimensions;
};

/** Opaque pointer to a GPU framebuffer */
typedef struct pg_gpu_framebuffer pg_gpu_framebuffer_t;

/** Create a GPU framebuffer from a set of GPU textures */
pg_gpu_framebuffer_t* pg_gpu_framebuffer_init(int n_textures, pg_gpu_texture_view_t** gpu_tex_views);
/** Create a GPU framebuffer with more detailed attachment info */
pg_gpu_framebuffer_t* pg_gpu_framebuffer_init_attachments(int n_textures,
        pg_gpu_texture_view_t** gpu_tex_views, enum pg_gpu_framebuffer_attachment_index* attachments);
/** Get a GPU framebuffer representing the actual display to the user   */
pg_gpu_framebuffer_t* pg_gpu_framebuffer_screen(void);
/** Deinitialize a GPU framebuffer  */
void pg_gpu_framebuffer_deinit(pg_gpu_framebuffer_t* gpu_fbuf);

/** Set a GPU framebuffer as the destination for subsequent draw operations */ 
void pg_gpu_framebuffer_dst(pg_gpu_framebuffer_t* gpu_fbuf);

/** Clear framebuffer contents  */
void pg_gpu_framebuffer_clear(pg_gpu_framebuffer_t* gpu_fbuf, vec4 color);

/** Get the dimensions of a GPU framebuffer */
ivec2 pg_gpu_framebuffer_get_dimensions(pg_gpu_framebuffer_t* gpu_fbuf);
float pg_gpu_framebuffer_get_aspect_ratio(pg_gpu_framebuffer_t* gpu_fbuf);
/** Get the full layout of a framebuffer    */
struct pg_gpu_framebuffer_layout* pg_gpu_framebuffer_get_layout(pg_gpu_framebuffer_t* gpu_fbuf);

/** \}  */

