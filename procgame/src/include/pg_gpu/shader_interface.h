/**
    \defgroup shader_interface_info GPU Shader Input Information
    \ingroup gpu
    \{
*/

#pragma once

/** \brief Named shader inputs (uniforms, attributes, etc.) max name length */
#define PG_SHADER_INPUT_NAME_MAXLEN  64

enum pg_shader_uniform_type {
    PG_SHADER_UNIFORM_INVALID,
    PG_SHADER_UNIFORM_TEXTURE,
    PG_SHADER_UNIFORM_TEXTURE_BUFFER,
    PG_SHADER_UNIFORM_BUFFER,
    PG_SHADER_UNIFORM_DYNAMIC,
    PG_SHADER_N_UNIFORM_TYPES,
};

const char* pg_shader_uniform_type_to_string(enum pg_shader_uniform_type type);
enum pg_shader_uniform_type pg_shader_uniform_type_from_string(const char* str, int n);

/** \brief Information (not state) about a shader uniform   */
struct pg_shader_uniform_info {
    char name[PG_SHADER_INPUT_NAME_MAXLEN];
    enum pg_shader_uniform_type type;
    int location;   // Index in the shader
    int binding;    // Binding point to take data from
    int count;      // Number of elements in a uniform array (or 1 if not an array)
    union {
        enum pg_data_type dynamic_type;
        struct pg_buffer_layout layout;
    };
};

/** \brief Information (not state) about a shader vertex input attribute    */
struct pg_shader_vertex_info {
    char name[PG_SHADER_INPUT_NAME_MAXLEN];
    enum pg_data_type type;
    int location;
};

/** \brief Information (not state) about a shader output    */
struct pg_shader_output_info {
    char name[PG_SHADER_INPUT_NAME_MAXLEN];
    enum pg_gpu_framebuffer_attachment_index location;
    enum pg_data_type type;
};

/** \brief Information about all inputs for a shader    */
struct pg_shader_interface_info {
    SARR_T(16, struct pg_shader_uniform_info) dynamic_uniforms;
    SARR_T(16, struct pg_shader_uniform_info) uniform_inputs;
    SARR_T(16, struct pg_shader_vertex_info) vertex_inputs;
    SARR_T(16, struct pg_shader_output_info) output_layout;
};

#define PG_SHADER_INTERFACE_INFO(...) { \
    .dynamic_uniforms = SARR_DATA(16, 0, {}), \
    .uniform_inputs = SARR_DATA(16, 0, {}), \
    .vertex_inputs = SARR_DATA(16, 0, {}), \
    .output_layout = SARR_DATA(16, 0, {}), \
    __VA_ARGS__ }

#define PG_SHADER_INTERFACE_DYNAMIC_UNIFORM_INFO(N, ...) \
    .uniform_inputs = SARR_DATA(16, N, __VA_ARGS__)

#define PG_SHADER_INTERFACE_UNIFORM_INFO(N, ...) \
    .uniform_inputs = SARR_DATA(16, N, __VA_ARGS__)

#define PG_SHADER_INTERFACE_VERTEX_INFO(N, ...) \
    .vertex_inputs = SARR_DATA(16, N, __VA_ARGS__)

#define PG_SHADER_INTERFACE_OUTPUT_INFO(N, ...) \
    .output_layout = SARR_DATA(16, N, __VA_ARGS__)

/** \brief Initialize a shader interface info with no contents  */
void pg_shader_interface_info_init(struct pg_shader_interface_info* sh_iface_info);
/** \brief Deinitialize the shader input info object    */
void pg_shader_interface_info_deinit(struct pg_shader_interface_info* sh_iface_info);

/** \brief Add a uniform input to a shader interface    */
bool pg_shader_interface_info_add_dynamic_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int count, enum pg_data_type type);
/** \brief Add a uniform input to a shader interface    */
bool pg_shader_interface_info_add_buffer_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int binding, struct pg_buffer_layout* layout);
/** \brief Add a uniform input to a shader interface    */
bool pg_shader_interface_info_add_texture_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int binding, bool buffer);
/** \brief Add a vertex input to a shader interface */
bool pg_shader_interface_info_add_vertex_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, enum pg_data_type type);
/** \brief Add an output to a shader interface  */
bool pg_shader_interface_info_add_output(struct pg_shader_interface_info* sh_iface_info,
        const char* name, enum pg_gpu_framebuffer_attachment_index location,
        enum pg_data_type type);


/** \brief Get index of uniform with the given name, from shader input info */
int pg_shader_interface_info_get_dynamic_uniform_index(const struct pg_shader_interface_info* sh_iface_info, const char* uni_name);
/** \brief Get index of uniform with the given name, from shader input info */
int pg_shader_interface_info_get_uniform_index(const struct pg_shader_interface_info* sh_iface_info, const char* uni_name);
/** \brief Get index of uniform with the given name, from shader input info */
int pg_shader_interface_info_get_vertex_index(const struct pg_shader_interface_info* sh_iface_info, const char* vert_name);
/** \brief Get index of uniform with the given name, from shader input info */
int pg_shader_interface_info_get_output_index(const struct pg_shader_interface_info* sh_iface_info, const char* out_name);

const struct pg_shader_uniform_info* pg_shader_interface_info_get_dynamic_uniform_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* uni_name);
const struct pg_shader_uniform_info* pg_shader_interface_info_get_uniform_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* uni_name);
const struct pg_shader_vertex_info* pg_shader_interface_info_get_vertex_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* vert_name);
const struct pg_shader_output_info* pg_shader_interface_info_get_output_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* out_name);

/** \}  */

