
/**
    \defgroup gpu_shader_program GPU Shader Programs
    \ingroup gpu_io
    \{
*/

#pragma once

/** \brief Opaque pointer to a fully constructed GPU shader program */
typedef struct pg_gpu_shader_program pg_gpu_shader_program_t;

/** Link a new GPU shader program using given shader stages */
pg_gpu_shader_program_t* pg_gpu_shader_program_init(pg_gpu_shader_stage_t** sh_stages, int n_stages,
        const struct pg_shader_interface_info* iface_info);
/** Destroy a GPU shader program    */
void pg_gpu_shader_program_destroy(pg_gpu_shader_program_t* sh_prog);
/** Get one of the shader stages in a GPU shader program    */
pg_gpu_shader_stage_t* pg_gpu_shader_program_get_stage(pg_gpu_shader_program_t* sh_prog,
                                                       enum pg_gpu_shader_stage_type stage);

/** \brief Get input information for a GPU shader program   */
const struct pg_shader_interface_info* pg_gpu_shader_program_interface_info(const pg_gpu_shader_program_t* sh_prog);

/** Check if a shader program was linked successfully   */
bool pg_gpu_shader_program_status(const pg_gpu_shader_program_t* sh_prog);
/** Begin using a shader program to emit draw calls */
void pg_gpu_shader_program_use(pg_gpu_shader_program_t* sh_prog);

/** Log debug info about a shader program   */
void pg_gpu_shader_program_print_debug(enum pg_log_type log_level, pg_gpu_shader_program_t* sh_prog);

/** \}  */


