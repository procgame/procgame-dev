/**
    \defgroup shader_source Shader source code
    \ingroup gpu
    \{
*/

#pragma once

/** Supported shader types    */
enum pg_gpu_shader_stage_type {
    PG_GPU_VERTEX_SHADER,   /**< Vertex Shader  */
    PG_GPU_FRAGMENT_SHADER, /**< Fragment Shader    */
    PG_GPU_SHADER_TYPES,    /**< Number of shader stages supported  */
};

#define PG_SHADER_VARIATION_BEGIN_TOKEN     "//!pg_variant"
#define PG_SHADER_VARIATION_END_TOKEN       "//!pg_end_variant"

/** Source code for a shader stage  */
struct pg_shader_source {
    /** Type of shader stage this is    */
    enum pg_gpu_shader_stage_type type;
    /** Raw source code for the shader stage    */
    char* src;
    /** Length of the source code   */
    int src_len;
    /** Variator for enabling/disabling shader features */
    struct pg_text_variator variator;
};

/** Create a shader stage from a file on disk  */
bool pg_shader_source_from_file(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type, struct pg_file* file);
/** Create a shader stage from a file on disk  */
bool pg_shader_source_from_file_with_includes(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type,
                                struct pg_file* file, const char** include_dirs, int n_include_dirs);
/** Create a shader stage from a string in memory  */
void pg_shader_source_from_memory(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type,
                                 const char* src, size_t src_len);

/** Free all the resources associated with a shader stage   */
void pg_shader_source_deinit(struct pg_shader_source* sh);
/** Get the number of variations a shader stage allows  */
int pg_shader_source_variation_count(struct pg_shader_source* sh);
/** Get the name of a single variation of a shader stage    */
const char* pg_shader_source_variation_name(struct pg_shader_source* sh, int var_idx);
/** Create a variant shader stage with the given variations enabled */
void pg_shader_source_variant(struct pg_shader_source* sh_new, struct pg_shader_source* sh_base,
                             const char** enabled_variations, int n_enabled);

void pg_shader_source_duplicate(struct pg_shader_source* sh_new, struct pg_shader_source* sh_base);

/** Compile shader source code on the GPU   */
pg_gpu_shader_stage_t* pg_shader_source_compile(struct pg_shader_source* sh_source);
/** Compile a variant of shader source code on the GPU  */
pg_gpu_shader_stage_t* pg_shader_source_compile_variant(
        struct pg_shader_source* sh_source, const char** variants, int n_variants);

/** \}  */
