typedef struct pg_gpu_vertex_assembly pg_gpu_vertex_assembly_t;

pg_gpu_vertex_assembly_t* pg_gpu_vertex_assembly_create(void);
void pg_gpu_vertex_assembly_destroy(pg_gpu_vertex_assembly_t* vbuf);

void pg_gpu_vertex_assembly_set_index_buffer(pg_gpu_vertex_assembly_t* vbuf,
        pg_gpu_buffer_t* index_buffer);
void pg_gpu_vertex_assembly_set_binding(pg_gpu_vertex_assembly_t* vbuf, int binding_idx,
        pg_gpu_buffer_t* source_buffer, bool instanced);
void pg_gpu_vertex_assembly_attribute(pg_gpu_vertex_assembly_t* vbuf, int binding_idx,
        int shader_attribute, const char* name);

pg_gpu_buffer_t* pg_gpu_vertex_assembly_get_attribute_buffer(pg_gpu_vertex_assembly_t* vbuf,
        int shader_attribute, struct pg_buffer_attribute* out_attrib);

void pg_gpu_vertex_assembly_bind(pg_gpu_vertex_assembly_t* vbuf);

int pg_gpu_vertex_assembly_get_range_count(pg_gpu_vertex_assembly_t* vbuf);
const char* pg_gpu_vertex_assembly_get_range_name(pg_gpu_vertex_assembly_t* vbuf, int range_idx);
int pg_gpu_vertex_assembly_get_range_index(pg_gpu_vertex_assembly_t* vbuf, const char* name);
void pg_gpu_vertex_assembly_get_range(pg_gpu_vertex_assembly_t* vbuf, int range_idx, ivec2* vert_range, ivec2* idx_range);

void pg_gpu_vertex_assembly_debug_log_ranges(pg_gpu_vertex_assembly_t* vbuf);
