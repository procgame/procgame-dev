typedef struct pg_gpu_render_stage pg_gpu_render_stage_t;

pg_gpu_render_stage_t* pg_gpu_render_stage_create(pg_gpu_pipeline_t* pipeline);
void pg_gpu_render_stage_destroy(pg_gpu_render_stage_t* stage);
void pg_gpu_render_stage_clear_commands(pg_gpu_render_stage_t* stage);

void pg_gpu_render_stage_set_debug_name(pg_gpu_render_stage_t* stage, const char* name);


#define PG_TARGET_FILTER_MAIN           (1 << 0)
#define PG_TARGET_FILTER_VR_LEFT        (1 << 1)
#define PG_TARGET_FILTER_VR_RIGHT       (1 << 2)
#define PG_TARGET_FILTER_VR_BOTH        (PG_TARGET_FILTER_VR_LEFT | PG_TARGET_FILTER_VR_RIGHT)
#define PG_TARGET_FILTER_ALL            (~0)

int pg_gpu_render_stage_add_input_group(pg_gpu_render_stage_t* stage,
        uint32_t output_filter, const char* name,
        pg_gpu_vertex_assembly_t* vertex, pg_gpu_resource_set_t* resources);
int pg_gpu_render_stage_get_input_group(pg_gpu_render_stage_t* stage, const char* name);
void pg_gpu_render_stage_get_input_range(pg_gpu_render_stage_t* stage, int group_idx,
        const char* range_name, ivec2* verts_out, ivec2* idx_out);

int pg_gpu_render_stage_add_target(pg_gpu_render_stage_t* stage, uint32_t input_filter,
        pg_gpu_framebuffer_t* output);

int pg_gpu_render_stage_get_range_index(pg_gpu_render_stage_t* stage, const char* name);

int pg_gpu_render_stage_get_uniform_location(pg_gpu_render_stage_t* stage, const char* name);
void pg_gpu_render_stage_set_initial_uniform(pg_gpu_render_stage_t* stage, int location, pg_data_t* data);
void pg_gpu_render_stage_set_group_uniform(pg_gpu_render_stage_t* stage, int group_idx, int location, pg_data_t* data);
void pg_gpu_render_stage_set_target_uniform(pg_gpu_render_stage_t* stage, int target_idx, int location, pg_data_t* data);
void pg_gpu_render_stage_set_targets_uniform(pg_gpu_render_stage_t* stage, uint32_t target_filter, int location, pg_data_t* data);

pg_gpu_command_arr_t* pg_gpu_render_stage_get_group_pre_commands(pg_gpu_render_stage_t* stage);
pg_gpu_command_arr_t* pg_gpu_render_stage_get_target_commands(pg_gpu_render_stage_t* stage, int target_idx);
pg_gpu_command_arr_t* pg_gpu_render_stage_get_group_commands(pg_gpu_render_stage_t* stage, int group_idx);
void pg_gpu_render_stage_get_range_commands(pg_gpu_render_stage_t* stage, int range_idx,
        ivec2* verts_range_out, ivec2* idx_range_out);

void pg_gpu_render_stage_execute(pg_gpu_render_stage_t* stage, uint32_t output_filter);

