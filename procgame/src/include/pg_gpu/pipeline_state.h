enum pg_gpu_comparison {
    PG_GPU_NEVER,
    PG_GPU_LESS,
    PG_GPU_LEQUAL,
    PG_GPU_GREATER,
    PG_GPU_GEQUAL,
    PG_GPU_EQUAL,
    PG_GPU_NOTEQUAL,
    PG_GPU_ALWAYS,
    PG_GPU_COMPARISON_COUNT,
};

enum pg_stencil_op {
    PG_STENCIL_KEEP,
    PG_STENCIL_ZERO,
    PG_STENCIL_REPLACE,
    PG_STENCIL_INCR,
    PG_STENCIL_INCR_WRAP,
    PG_STENCIL_DECR,
    PG_STENCIL_DECR_WRAP,
    PG_STENCIL_INVERT,
    PG_STENCIL_OP_COUNT,
};

enum pg_gpu_blend_factor {
    PG_GPU_ZERO,
    PG_GPU_ONE,
    PG_GPU_SRC_COLOR,
    PG_GPU_ONE_MINUS_SRC_COLOR,
    PG_GPU_DST_COLOR,
    PG_GPU_ONE_MINUS_DST_COLOR,
    PG_GPU_SRC_ALPHA,
    PG_GPU_ONE_MINUS_SRC_ALPHA,
    PG_GPU_DST_ALPHA,
    PG_GPU_ONE_MINUS_DST_ALPHA,
    PG_GPU_CONSTANT_COLOR,
    PG_GPU_ONE_MINUS_CONSTANT_COLOR,
    PG_GPU_CONSTANT_ALPHA,
    PG_GPU_ONE_MINUS_CONSTANT_ALPHA,
    PG_GPU_SRC_ALPHA_SATURATE,
    PG_GPU_SRC1_COLOR,
    PG_GPU_ONE_MINUS_SRC1_COLOR,
    PG_GPU_SRC1_ALPHA,
    PG_GPU_ONE_MINUS_SRC1_ALPHA,
    PG_GPU_BLEND_FACTOR_COUNT
};

enum pg_gpu_blend_equation {
    PG_GPU_FUNC_ADD,
    PG_GPU_FUNC_SUBTRACT,
    PG_GPU_FUNC_REVERSE_SUBTRACT,
    PG_GPU_MIN,
    PG_GPU_MAX,
    PG_GPU_BLEND_EQUATION_COUNT
};

struct pg_gpu_depth_state {
    bool enabled;
    enum pg_gpu_comparison func;
    uint32_t write_mask;
};

#define PG_GPU_DEPTH_STATE(...) \
    (struct pg_gpu_depth_state) { \
        .enabled = true, .func = PG_GPU_LEQUAL, .write_mask = 0xFF, \
        __VA_ARGS__ \
    }

struct pg_gpu_stencil_state {
    bool enabled;
    enum pg_gpu_comparison func;
    enum pg_stencil_op op_stencil_fail, op_depth_fail, op_both_pass;
    int func_ref;
    uint32_t func_mask;
    uint32_t write_mask;
};

#define PG_GPU_STENCIL_STATE(...)  \
    (struct pg_gpu_stencil_state) { \
        .enabled = false, \
        .func = PG_GPU_NEVER, .func_ref = 0, .func_mask = 0xFF, .write_mask = 0xFF, \
        .op_stencil_fail = PG_STENCIL_KEEP, .op_depth_fail = PG_STENCIL_KEEP, \
        .op_both_pass = PG_STENCIL_KEEP, \
        __VA_ARGS__ \
    }
    

struct pg_gpu_blending_state {
    bool enabled;
    enum pg_gpu_blend_equation equation;
    enum pg_gpu_blend_factor src_factor, dst_factor;
};

#define PG_GPU_BLENDING_STATE(...) \
    (struct pg_gpu_blending_state) { \
        .enabled = true, \
        .equation = PG_GPU_FUNC_ADD, \
        .src_factor = PG_GPU_SRC_ALPHA, .dst_factor = PG_GPU_ONE_MINUS_SRC_ALPHA, \
    }


enum pg_gpu_comparison pg_gpu_comparison_from_string(const char* str);
enum pg_stencil_op pg_stencil_op_from_string(const char* str);
enum pg_gpu_blend_factor pg_gpu_blend_factor_from_string(const char* str);
enum pg_gpu_blend_equation pg_gpu_blend_equation_from_string(const char* str);

const char* pg_gpu_comparison_to_string(enum pg_gpu_comparison func);
const char* pg_stencil_op_to_string(enum pg_stencil_op op);
const char* pg_gpu_blend_factor_to_string(enum pg_gpu_blend_factor func);
const char* pg_gpu_blend_equation_to_string(enum pg_gpu_blend_equation func);

void pg_gpu_depth_state_default(struct pg_gpu_depth_state* depth);
void pg_gpu_stencil_state_default(struct pg_gpu_stencil_state* stencil);
void pg_gpu_blending_state_default(struct pg_gpu_blending_state* blending);

void pg_gpu_depth_state_apply(struct pg_gpu_depth_state* depth);
void pg_gpu_stencil_state_apply(struct pg_gpu_stencil_state* stencil);
void pg_gpu_blending_state_apply(struct pg_gpu_blending_state* blending);
