typedef struct pg_gpu_resource_set pg_gpu_resource_set_t;

pg_gpu_resource_set_t* pg_gpu_resource_set_create(void);
void pg_gpu_resource_set_destroy(pg_gpu_resource_set_t* set);

void pg_gpu_resource_set_attach_buffer(pg_gpu_resource_set_t* set,
        int binding_idx, pg_gpu_buffer_t* gpu_buf);
void pg_gpu_resource_set_attach_texture_buffer(pg_gpu_resource_set_t* set,
        int texture_idx, pg_gpu_buffer_t* gpu_buf);
void pg_gpu_resource_set_attach_texture(pg_gpu_resource_set_t* set,
        int texture_idx, pg_gpu_texture_t* texture);
void pg_gpu_resource_set_attach_texture_params(pg_gpu_resource_set_t* set,
        int texture_idx, struct pg_gpu_texture_params* params);

pg_gpu_texture_t* pg_gpu_resource_set_get_texture(const pg_gpu_resource_set_t* set,
        int texture_idx);
pg_gpu_buffer_t* pg_gpu_resource_set_get_texture_buffer(const pg_gpu_resource_set_t* set,
        int texture_idx);
pg_gpu_buffer_t* pg_gpu_resource_set_get_buffer(const pg_gpu_resource_set_t* set,
        int binding_idx);

void pg_gpu_resource_set_bind(pg_gpu_resource_set_t* set);




