#pragma once

struct pg_buffer;
struct pg_texture;
struct pg_shader_source;
struct pg_shader_interface_info;
struct pg_gpu_command;

typedef struct pg_gpu_buffer pg_gpu_buffer_t;
typedef struct pg_gpu_texture pg_gpu_texture_t;
typedef struct pg_gpu_framebuffer pg_gpu_framebuffer_t;
typedef struct pg_gpu_resource_set pg_gpu_resource_set_t;
typedef struct pg_gpu_shader_stage pg_gpu_shader_stage_t;
typedef struct pg_gpu_shader_program pg_gpu_shader_program_t;
typedef struct pg_gpu_pipeline pg_gpu_pipeline_t;

/*  Depend on pg_util   */
#include "pg_util/pg_util.h"


#include "pg_gpu/buffer.h"
#include "pg_gpu/buffer_builder.h"
#include "pg_gpu/texture.h"
#include "pg_gpu/vertex_assembly.h"
#include "pg_gpu/framebuffer.h"
#include "pg_gpu/resource_set.h"
#include "pg_gpu/shader_source.h"
#include "pg_gpu/shader_interface.h"
#include "pg_gpu/shader_stage.h"
#include "pg_gpu/shader_program.h"
#include "pg_gpu/pipeline.h"
#include "pg_gpu/command_buffer.h"
#include "pg_gpu/render_stage.h"
#include "pg_gpu/renderer.h"
#include "pg_gpu/gpu_asset_loaders.h"

#include "pg_gpu/opengl.h"


