/**
    \file opengl.h
    \ingroup gpu_io
    \brief OpenGL Interface
*/

#pragma once

#include <GL/glew.h>

/** \brief Wrap arbitrary OpenGL-calling code to report all generated errors    */
#define PG_CHECK_GL(...) \
    do { \
        if(!pg_gl_errors()) { \
            { __VA_ARGS__; } \
            GLenum err; \
            while((err = glGetError()) != GL_NO_ERROR) { \
                pg_gl_add_error(); \
                pg_log(PG_LOG_ERROR, "GL Error: %s\n", pg_gl_enum_string(err)); \
            } \
        } \
    } while(0)

/** \brief Get the number of errors since last cleared  */
int pg_gl_errors(void);
/** \brief Add an error to the current count    */
void pg_gl_add_error(void);
/** \brief Reset the GL error counter   */
void pg_gl_clear_errors(void);

/** \brief Get a string representation of OpenGL's error constants  */
const char* pg_gl_enum_string(GLenum gl);
/** \brief Get the procgame data type from a GL data type   */
enum pg_data_type pg_data_type_from_gl(GLenum type);
/** \brief Get the GL base type from a procgame data type (ubvec4 -> GL_UNSIGNED_BYTE)  */
GLenum pg_data_type_to_gl(enum pg_data_type type);

/** \brief Get the underlying OpenGL handle for a texture   */
GLuint pg_gpu_texture_get_handle(pg_gpu_texture_t* gpu_tex);
GLuint pg_gpu_texture_get_binding_point(pg_gpu_texture_t* gpu_tex);
/** \brief Get the underlying OpenGL handle for a buffer (VBO/EBO/UBO/...)  */
GLuint pg_gpu_buffer_get_handle(pg_gpu_buffer_t* gpu_buf);
/** \brief Get the underlying OpenGL texture handle for a texture buffer    */
GLuint pg_gpu_buffer_get_tex_handle(pg_gpu_buffer_t* gpu_buf);
/** \brief Get the internal handle used by GL for the shader program   */
GLuint pg_gpu_shader_program_get_handle(pg_gpu_shader_program_t* sh_prog);
/** \brief Get the internal handle of a shader stage    */
GLuint pg_gpu_shader_stage_get_handle(pg_gpu_shader_stage_t* sh_stage);
