#pragma once

/** GPU command definition (for local memory, not GPU memory)   */
struct pg_gpu_command {
    enum pg_gpu_command_op {
        PG_GPU_COMMAND_DRAW_TRIANGLES,
        PG_GPU_COMMAND_SET_UNIFORM,
        PG_GPU_COMMAND_CLEAR,
        PG_GPU_COMMAND_STENCIL,
        PG_GPU_COMMAND_DEPTH,
        PG_GPU_COMMAND_BLENDING,
        PG_GPU_COMMAND_COLOR_MASK,
        PG_GPU_COMMAND_VIEWPORT,
    } op;
    union {
        struct {
            ivec2 range;
            int base_vertex;
            bool indexed;
            bool strip;
        } draw_triangles;
        struct {
            int location;
            pg_data_t data;
            bool transpose_matrix;
        } set_uniform;
        struct {
            vec4 color;
        } clear;
        struct pg_gpu_stencil_state stencil;
        struct pg_gpu_depth_state depth;
        struct pg_gpu_blending_state blending;
        struct {
            ivec4 mask;
        } color_mask;
    };
};



/** Array GPU commands (local memory)   */
typedef ARR_T(struct pg_gpu_command) pg_gpu_command_arr_t;

const char* pg_gpu_command_op_to_string(enum pg_gpu_command_op op);
void pg_gpu_command_execute(struct pg_gpu_command* cmd);

#define pg_gpu_record_commands(CMDBUF) pg_gpu_command_arr_t* pg_gpu_command_recording_buffer = (CMDBUF);

#define pg_gpu_cmd_draw_triangles(FIRST, COUNT, ...) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_DRAW_TRIANGLES, \
        .draw_triangles = { \
            .range = ivec2((FIRST), (COUNT)), __VA_ARGS__ } \
        } \
    );

#define pg_gpu_cmd_set_uniform_matrix(LOCATION, TYPE, TRANSPOSE, COUNT, DATA) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_SET_UNIFORM, \
        .set_uniform = { \
            .location = (LOCATION), \
            .transpose_matrix = (TRANSPOSE), \
            .data = pg_data_init((TYPE), (COUNT), (DATA)), } \
        } \
    );

#define pg_gpu_cmd_set_uniform(LOCATION, TYPE, COUNT, DATA) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_SET_UNIFORM, \
        .set_uniform = { \
            .location = (LOCATION), \
            .data = pg_data_init((TYPE), (COUNT), (DATA)), } \
        } \
    );

#define pg_gpu_cmd_clear(COLOR) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_CLEAR, \
        .clear = { .color = (COLOR), } \
        } \
    );

#define pg_gpu_cmd_depth(ENABLED, FUNC, MASK) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_DEPTH, \
        .depth = { .enabled = (ENABLED), .func = (FUNC), .write_mask = (MASK) } \
        } \
    );

#define pg_gpu_cmd_blending(ENABLED, ...) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_BLENDING, \
        .blending = { .enabled = (ENABLED), __VA_ARGS__ } \
        } \
    );


#define pg_gpu_cmd_stencil(STENCIL) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_STENCIL, .stencil = (STENCIL) \
        } \
    );

#define pg_gpu_cmd_color_mask(RED, GREEN, BLUE, ALPHA) \
    ARR_PUSH(*pg_gpu_command_recording_buffer, (struct pg_gpu_command) { \
        .op = PG_GPU_COMMAND_COLOR_MASK, .color_mask.mask = \
            {{ (RED), (GREEN), (BLUE), (ALPHA) }} \
        } \
    );



