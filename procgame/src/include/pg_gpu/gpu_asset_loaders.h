struct pg_asset_loader* pg_gpu_buffer_asset_loader(void);
struct pg_asset_loader* pg_gpu_framebuffer_asset_loader(void);
struct pg_asset_loader* pg_gpu_pipeline_asset_loader(void);
struct pg_asset_loader* pg_gpu_render_stage_asset_loader(void);
struct pg_asset_loader* pg_gpu_renderer_asset_loader(void);
struct pg_asset_loader* pg_gpu_resource_set_asset_loader(void);
struct pg_asset_loader* pg_gpu_shader_program_asset_loader(void);
struct pg_asset_loader* pg_gpu_texture_asset_loader(void);
struct pg_asset_loader* pg_gpu_vertex_assembly_asset_loader(void);

bool pg_gpu_texture_params_from_json(struct pg_gpu_texture_params* params, cJSON* json);
bool pg_tex_frame_from_json(pg_tex_frame_t* frame, cJSON* json);
bool pg_shader_interface_info_from_json(struct pg_shader_interface_info* sh_iface_info, cJSON* json);
bool pg_buffer_data_from_json(struct pg_buffer* buf, cJSON* json);
bool pg_buffer_layout_from_json(struct pg_buffer_layout* layout, cJSON* json);
bool pg_gpu_blending_state_from_json(struct pg_gpu_blending_state* blend, cJSON* json);
bool pg_gpu_depth_state_from_json(struct pg_gpu_depth_state* ds, cJSON* json);
bool pg_gpu_stencil_state_from_json(struct pg_gpu_stencil_state* ss, cJSON* json);
bool pg_gpu_commands_from_json(pg_gpu_command_arr_t* cmds_out, cJSON* json);
bool pg_gpu_commands_from_asset_json(pg_gpu_command_arr_t* cmds_out, cJSON* json,
        pg_asset_manager_t* asset_mgr, const struct pg_shader_interface_info* sh_iface);

void pg_gpu_asset_loaders(pg_asset_manager_t* asset_mgr);
