#include "pg_core/sdl.h"
#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"

#include "pg_modules/window.h"

static bool pg_have_window_ = false;
static ivec2 pg_window_size_ = {{ 800, 450 }};
// TODO static bool pg_window_fullscreen_ = false;
static SDL_Window* pg_window_ = NULL;

bool pg_init_window(ivec2 window_size)
{
    if(pg_have_window_) return true;
    SDL_DisplayMode display;
    pg_window_size_ = window_size;

    pg_log(PG_LOG_DEBUG, "Using SDL Video Driver %d: %s", 0, SDL_GetVideoDriver(0));
    pg_log(PG_LOG_DEBUG, "Window resolution %dx%d", VEC_XY(window_size));
    /*  Init SDL gfx subsystem and get the display mode   */
    bool r = PG_CHECK_SDL_B(SDL_VideoInit(0))
          && PG_CHECK_SDL_B(SDL_GetDesktopDisplayMode(0, &display));
    if(!r) return false;

    /*  Make a window   */
    pg_window_ = PG_CHECK_SDL_P( SDL_CreateWindow("procgame",
                    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                    pg_window_size_.x, pg_window_size_.y,
                    SDL_WINDOW_OPENGL) );
    if(!pg_window_) return false;

    pg_have_window_ = true;
    return true;
}

bool pg_deinit_window(void)
{
    if(!pg_have_window_) return true;
    SDL_DestroyWindow(pg_window_);
    SDL_VideoQuit();
    pg_have_window_ = false;
    return true;
}

bool pg_have_window(void)
{
    return pg_have_window_;
}

SDL_Window* pg_window_handle(void)
{
    if(!pg_have_window_) return NULL;
    return pg_window_;
}


/*************************/
/*  Window management    */
/*************************/
void pg_window_resize(int w, int h)
{
    SDL_DisplayMode display;
    SDL_GetDesktopDisplayMode(0, &display);
    pg_window_size_ = ivec2(w, h);
    SDL_SetWindowSize(pg_window_, pg_window_size_.x, pg_window_size_.y);
    SDL_SetWindowFullscreen(pg_window_, 0);
}

void pg_window_fullscreen(void)
{
    SDL_DisplayMode display;
    SDL_GetDesktopDisplayMode(0, &display);
    pg_window_size_ = ivec2(display.w, display.h);
    SDL_SetWindowSize(pg_window_, pg_window_size_.x, pg_window_size_.y);
    SDL_SetWindowFullscreen(pg_window_, SDL_WINDOW_FULLSCREEN);
}

ivec2 pg_window_size(void)
{
    return pg_window_size_;
}

float pg_window_aspect(void)
{
    return (float)pg_window_size_.x / (float)pg_window_size_.y;
}

void pg_window_swap(void)
{
    SDL_GL_SwapWindow(pg_window_);
}

