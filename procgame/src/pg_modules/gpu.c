#include "pg_core/sdl.h"

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

#include "pg_modules/gpu.h"
#include "pg_modules/window.h"

static SDL_GLContext pg_gl_context;
static bool pg_have_gpu_ = false;

bool pg_init_gpu(void)
{
    if(pg_have_gpu_) return true;
    SDL_Window* pg_window = NULL;
    if(!pg_have_window() || !(pg_window = pg_window_handle())) {
        pg_log(PG_LOG_ERROR, "Cannot create OpenGL context without an open window");
        return false;
    }

    /*  Request an OpenGL 4.5 Core profile  */
    bool r = PG_CHECK_SDL_B(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE))
          && PG_CHECK_SDL_B(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4))
          && PG_CHECK_SDL_B(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5))
          && ((pg_gl_context = PG_CHECK_SDL_P(SDL_GL_CreateContext(pg_window))));

    if(!r) {
        pg_log(PG_LOG_ERROR, "Failed to create OpenGL context");
        return false;
    }

    /*  Get GL with GLEW    */
    glewExperimental = GL_TRUE;
    glewInit();
    glGetError();
    pg_gl_clear_errors();

    pg_have_gpu_ = true;
    return true;
}

bool pg_deinit_gpu(void)
{
    if(!pg_have_gpu_) return true;
    SDL_GL_DeleteContext(pg_gl_context);
    pg_have_gpu_ = false;
    return true;
}

bool pg_have_gpu(void)
{
    return pg_have_gpu_;
}
