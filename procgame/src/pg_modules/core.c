#include "pg_modules/core.h"

#include "pg_core/logging.h"

static bool pg_have_core_ = false;

bool pg_init_core(void)
{
    if(pg_have_core_) return true;
    if(!PG_CHECK_SDL_I(SDL_Init(0))) {
        pg_log(PG_LOG_ERROR, "Failed to initialize SDL: %s", SDL_GetError());
        return false;
    }
    pg_have_core_ = true;
    return true;
}

bool pg_deinit_core(void)
{
    if(!pg_have_core_) return true;
    SDL_Quit();
    return true;
}

bool pg_have_core(void)
{
    return pg_have_core_;
}



