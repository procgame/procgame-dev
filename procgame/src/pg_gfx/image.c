#include "procgame.h"

/********************************/
/*  SINGLE-IMAGE ASSETS         */
/********************************/

void pg_image_init(struct pg_image* image, const char* src_filename, ivec2 img_size,
        struct pg_texture* tex, bool own_tex, int tex_layer)
{
    *image = (struct pg_image){};
    strncpy(image->img_filename, src_filename, 1024);
    image->img_size = img_size;
    image->uv_size = vec2_div(vec2(VEC_XY(img_size)), vec2(VEC_XY(tex->dimensions)));
    image->own_tex = own_tex;
    image->tex = tex;
    image->tex_layer = tex_layer;
    ARR_INIT(image->frames);
    HTABLE_INIT(image->frames_table, 8);
    ARR_INIT(image->seqs);
    HTABLE_INIT(image->seqs_table, 8);
    /*  Frame 0 is always the whole image, and has the name "image" */
    struct pg_image_frame tmp_frame =
        { .name = "image",
          .uv_raw = pg_tex_frame(vec2(0,0), vec2(1,1), 0) };
    HTABLE_SET(image->frames_table, "image", 0);
    ARR_PUSH(image->frames, tmp_frame);
}

void pg_image_deinit(struct pg_image* image)
{
    ARR_DEINIT(image->frames);
    HTABLE_DEINIT(image->frames_table);
    ARR_DEINIT(image->seqs);
    HTABLE_DEINIT(image->seqs_table);
    if(image->tex && image->own_tex) {
        pg_texture_deinit(image->tex);
        free(image->tex);
    }
    if(image->gpu_tex && image->own_gpu_tex) {
        pg_gpu_texture_deinit(image->gpu_tex);
    }
}

struct pg_texture* pg_image_texture(struct pg_image* image)
{
    return image->tex;
}

pg_gpu_texture_t* pg_image_get_gpu_texture(struct pg_image* image)
{
    if(!image->gpu_tex) {
        image->gpu_tex = pg_texture_upload(image->tex);
        image->own_gpu_tex = true;
    }
    return image->gpu_tex;
}


/*  Get a raw texture frame for a grid cell based on image metadata */
pg_tex_frame_t pg_image_get_grid_cell(struct pg_image* image, int x, int y)
{
    if(!image) return pg_tex_frame(vec2(0,0), vec2(1,1), 0);
    vec2 cell_start = vec2_mul(vec2(x,y), image->grid_uv);
    vec2 cell_end = vec2_add(cell_start, image->grid_uv);
    return pg_tex_frame(cell_start, cell_end, image->tex_layer);
}

/*  Get a raw texture frame by the name in the image metadata   */
pg_tex_frame_t pg_image_get_frame_by_name(struct pg_image* image, const char* frame_name)
{
    if(!image) return pg_tex_frame(vec2(0,0), vec2(1,1), 0);
    int frame_idx = 0;
    HTABLE_GET_V(image->frames_table, frame_name, frame_idx);
    /*  frame_idx will still be 0 if frame does not exist   */
    struct pg_image_frame* frame = &image->frames.data[frame_idx];
    return frame->uv_raw;
}

pg_tex_frame_t pg_image_get_frame(struct pg_image* image, int idx)
{
    return image->frames.data[idx].uv_raw;
}

struct pg_image_sequence* pg_image_get_sequence(struct pg_image* image, int idx)
{
    return &image->seqs.data[idx];
}

/*  Get a sequence from the meta-data, to use with a pg_image_imganim   */
struct pg_image_sequence* pg_image_get_sequence_by_name(
        struct pg_image* image, const char* seq_name)
{
    int seq_idx = 0;
    HTABLE_GET_V(image->seqs_table, seq_name, seq_idx);
    /*  Returns sequence 0 on error */
    return &image->seqs.data[seq_idx];
}

/****************************/
/*  IMAGE SETS              */
/****************************/

void pg_imageset_init(struct pg_imageset* imageset, struct pg_texture* tex, bool own_tex)
{
    *imageset = (struct pg_imageset){};
    ARR_INIT(imageset->imgs);
    HTABLE_INIT(imageset->imgs_table, 4);
    imageset->tex = tex;
    imageset->own_tex = own_tex;
}

void pg_imageset_deinit(struct pg_imageset* imageset)
{
    int i;
    struct pg_image* image;
    ARR_FOREACH_PTR(imageset->imgs, image, i) {
        pg_image_deinit(image);
    }
    ARR_DEINIT(imageset->imgs);
    HTABLE_DEINIT(imageset->imgs_table);
    if(imageset->tex && imageset->own_tex) {
        pg_texture_deinit(imageset->tex);
        free(imageset->tex);
    }
    if(imageset->gpu_tex && imageset->own_gpu_tex) {
        pg_gpu_texture_deinit(imageset->gpu_tex);
    }
}

struct pg_texture* pg_imageset_get_texture(struct pg_imageset* imageset)
{
    if(!imageset) return NULL;
    return imageset->tex;
}

pg_gpu_texture_t* pg_imageset_get_gpu_texture(struct pg_imageset* imageset)
{
    if(!imageset) return NULL;
    if(!imageset->gpu_tex) {
        imageset->gpu_tex = pg_texture_upload(imageset->tex);
        imageset->own_gpu_tex = true;
        int i;
        struct pg_image* image;
        ARR_FOREACH_PTR(imageset->imgs, image, i) {
            image->gpu_tex = imageset->gpu_tex;
        }
    }
    return imageset->gpu_tex;
}


struct pg_image* pg_imageset_get_image(struct pg_imageset* imageset, const char* image_name)
{
    if(!imageset) return NULL;
    int img_idx = -1;
    HTABLE_GET_V(imageset->imgs_table, image_name, img_idx);
    if(img_idx == -1) {
        pg_log(PG_LOG_ERROR, "Attempting to get non-existent image '%s' from imageset", image_name);
        return NULL;
    }
    /*  img_idx will be 0 if the image name doesn't exist   */
    return &imageset->imgs.data[img_idx];
}

/********************/
/*  Image steppers  */
void pg_image_make_stepper(struct pg_image* asset,
                           struct pg_image_stepper* stepper,
                           const char* seq_name)
{
    stepper->img_idx = 0;
    int seq_idx = -1;
    if(seq_name) HTABLE_GET_V(asset->seqs_table, seq_name, seq_idx);
    pg_tex_frame_t uv;
    if(seq_idx != -1) {
        struct pg_image_sequence* seq_ptr = &asset->seqs.data[seq_idx];
        uv = asset->frames.data[seq_ptr->frames.data[0].frame].uv_raw;
    } else {
        uv = asset->frames.data[0].uv_raw;
    }
    *stepper = (struct pg_image_stepper){
        .img_idx = 0,
        .seq_idx = seq_idx,
        .cur_step = 0,
        .seq_progress = 0,
        .uv = uv,
    };
}

void pg_image_make_stepper_single(struct pg_image* asset,
                                  struct pg_image_stepper* stepper,
                                  const char* frame_name)
{
    int frame_idx = 0;
    if(frame_name) HTABLE_GET_V(asset->frames_table, frame_name, frame_idx);
    *stepper = (struct pg_image_stepper){
        .img_idx = 0,
        .seq_idx = -1,
        .cur_step = frame_idx,
        .seq_progress = 0,
        .uv = asset->frames.data[frame_idx].uv_raw,
    };
}

void pg_image_step(struct pg_image* asset,
                         struct pg_image_stepper* stepper, float step)
{
    if(stepper->seq_idx == -1) return;
    struct pg_image_sequence* seq = &asset->seqs.data[stepper->seq_idx];
    stepper->seq_progress += step;
    if(stepper->seq_progress >= seq->full_duration) {
        stepper->cur_step = 0;
        stepper->seq_progress = LM_FMOD(stepper->seq_progress, seq->full_duration);
    }
    while(stepper->cur_step+1 < seq->frames.len
    && seq->frames.data[stepper->cur_step+1].start < stepper->seq_progress) {
        ++stepper->cur_step;
    }
    int frame_idx = seq->frames.data[stepper->cur_step].frame;
    stepper->uv = asset->frames.data[frame_idx].uv_raw;
}

void pg_image_stepper_set_time(struct pg_image* asset,
                               struct pg_image_stepper* stepper, float time)
{
    if(stepper->seq_idx == -1) return;
    struct pg_image_sequence* seq = &asset->seqs.data[stepper->seq_idx];
    stepper->seq_progress = LM_FMOD(time, seq->full_duration);
    stepper->cur_step = 0;
    while(stepper->cur_step+1 < seq->frames.len
    && seq->frames.data[stepper->cur_step+1].start < stepper->seq_progress) {
        ++stepper->cur_step;
    }
    int frame_idx = seq->frames.data[stepper->cur_step].frame;
    stepper->uv = asset->frames.data[frame_idx].uv_raw;
}


void pg_imageset_make_stepper(struct pg_imageset* asset,
                              struct pg_image_stepper* stepper,
                              const char* img_name, const char* seq_name)
{
    int img_idx = 0, seq_idx = -1;
    if(img_name) HTABLE_GET_V(asset->imgs_table, img_name, img_idx);
    struct pg_image* img_asset = &asset->imgs.data[img_idx];
    if(seq_name) HTABLE_GET_V(img_asset->seqs_table, seq_name, seq_idx);
    pg_tex_frame_t uv;
    if(seq_idx != -1) {
        struct pg_image_sequence* seq_ptr = &img_asset->seqs.data[seq_idx];
        uv = img_asset->frames.data[seq_ptr->frames.data[0].frame].uv_raw;
    } else {
        uv = img_asset->frames.data[0].uv_raw;
    }
    *stepper = (struct pg_image_stepper){
        .img_idx = img_idx,
        .seq_idx = seq_idx,
        .cur_step = 0,
        .seq_progress = 0,
        .uv = uv,
    };
}

void pg_imageset_make_stepper_single(struct pg_imageset* asset,
                                     struct pg_image_stepper* stepper,
                                     const char* img_name, const char* frame_name)
{
    if(!asset->imgs.len) return;
    int img_idx = 0, frame_idx = 0;
    if(img_name) HTABLE_GET_V(asset->imgs_table, img_name, img_idx);
    struct pg_image* img_asset = &asset->imgs.data[img_idx];
    if(frame_name) HTABLE_GET_V(img_asset->frames_table, frame_name, frame_idx);
    *stepper = (struct pg_image_stepper){
        .img_idx = img_idx,
        .seq_idx = -1,
        .cur_step = frame_idx,
        .seq_progress = 0,
        .uv = img_asset->frames.data[frame_idx].uv_raw,
    };
}

void pg_imageset_step(struct pg_imageset* asset,
                          struct pg_image_stepper* stepper, float step)
{
    struct pg_image* img_asset = &asset->imgs.data[stepper->img_idx];
    pg_image_step(img_asset, stepper, step);
}


void pg_imageset_stepper_set_time(struct pg_imageset* asset,
                                  struct pg_image_stepper* stepper, float time)
{
    struct pg_image* img_asset = &asset->imgs.data[stepper->img_idx];
    pg_image_stepper_set_time(img_asset, stepper, time);
}
