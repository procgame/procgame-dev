#include "procgame.h"
#include "ksort.h"
#include "ui_internal.h"

/************************************************/
/*  Depth sorting                               */
/************************************************/
static int layer_comp(pg_ui_t a, pg_ui_t b, struct pg_ui_context* ctx)
{
    struct pg_ui_content* a_cont = &ctx->content_pool.data[a];
    struct pg_ui_content* b_cont = &ctx->content_pool.data[b];
    return (a_cont->layer < b_cont->layer);
}

KSORT_S_DEF(layers, pg_ui_t, struct pg_ui_context*, layer_comp)

static void group_sort_layers(struct pg_ui_context* ctx, struct pg_ui_group* grp)
{
    ks_introsort_s_layers(grp->children_arr.len, grp->children_arr.data, ctx);
    grp->children_added = 0;
}



/************************************************/
/*  Draw                                        */
/************************************************/

/*  Stack data  */
struct draw_frame {
    int clip;
    struct ui_space space;
};

static void ui_group_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
        const struct draw_frame* frame, pg_gpu_command_arr_t* commands);
static void ui_element_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
        const struct draw_frame* frame, pg_gpu_command_arr_t* commands);
static void ui_content_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
        struct draw_frame* frame, pg_gpu_command_arr_t* commands);

void pg_ui_context_draw(struct pg_ui_context* ctx)
{
    struct pg_gfx_transform tx_base = {
        .tx = mat4_identity(),
        .tx_inverse = mat4_identity(),
        .pos = vec3(0,0,0),
        .scale = vec3(1,1,1),
        .rot = quat_identity(),
    };

    struct draw_frame root_frame = {
        .clip = 0,
        .space = {
            .aspect_fix = vec3(ctx->ar,1,1),
            .origin = tx_base,
        },
    };
    pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_commands(ctx->render_stage, 0);
    ARR_TRUNCATE(*commands, 0);
    pg_quadbatch_reset(&ctx->quadbatch);
    ui_content_draw(ctx, ui_dereference(ctx, ctx->root), &root_frame, commands);
    pg_quadbatch_upload(&ctx->quadbatch);
}




static void ui_content_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
        struct draw_frame* frame, pg_gpu_command_arr_t* commands)
{
    if(cont->disabled) return;
    if(cont->type == PG_UI_GROUP) ui_group_draw(ctx, cont, frame, commands);
    else if(cont->type == PG_UI_ELEMENT) ui_element_draw(ctx, cont, frame, commands);
}



static void ui_group_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                       const struct draw_frame* frame, pg_gpu_command_arr_t* commands)
{
    struct pg_ui_group* grp = &cont->grp;
    struct draw_frame in_frame = *frame;

    struct pg_gfx_transform grp_tx, grp_tx_clip;
    ui_group_get_transforms(ctx, cont, &frame->space, &grp_tx, &grp_tx_clip, &in_frame.space);

    mat4 mat_container = mat4_mul(in_frame.space.origin.tx, mat4_scaling(in_frame.space.aspect_fix));
    mat4 mat_clip = mat4_mul(mat4_identity(), grp_tx_clip.tx);

    pg_gpu_record_commands(commands);
    /*  Draw a stencil quad if clipping is enabled  */
    if(grp->enable_clip) {
        struct pg_gpu_stencil_state begin_stencil_state = {
            .enabled = 1, .write_mask = 0xFF,
            .func = PG_GPU_EQUAL, .func_ref = in_frame.clip, .func_mask = 0xFF,
            .op_stencil_fail = PG_STENCIL_KEEP, .op_depth_fail = PG_STENCIL_KEEP,
            .op_both_pass = PG_STENCIL_INCR };
        struct pg_gpu_stencil_state end_stencil_state = {
            .enabled = 1, .write_mask = 0x00,
            .func = PG_GPU_EQUAL, .func_ref = in_frame.clip + 1, .func_mask = 0xFF,
            .op_stencil_fail = PG_STENCIL_KEEP, .op_depth_fail = PG_STENCIL_KEEP,
            .op_both_pass = PG_STENCIL_KEEP };
        in_frame.clip += 1;

        /*  Add GPU commands to the buffer  */
        pg_gpu_cmd_stencil(begin_stencil_state);
        pg_gpu_cmd_color_mask(0,0,0,0);

        /*  Draw the quad, with stencil state commands on either side   */
        pg_quadbatch_next(&ctx->quadbatch, &mat_clip);
        pg_quadbatch_add_quad(&ctx->quadbatch, &PG_EZQUAD(
            .pos = vec3(0,0,0), .scale = vec2(1,1),
            .color_add = 0xFF0000FF, .color_mul = 0x00000000 ));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);

        pg_gpu_cmd_color_mask(1,1,1,1);
        pg_gpu_cmd_stencil(end_stencil_state);
    }

    if(ctx->debug_containers && cont->parent != -1) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_container);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(0,0,0), .scale = vec2(1.025,1.025),
            .color_mul = vec4(0), .color_add = vec4_mul(cont->debug_color, vec4(0.25,0.25,0.25,0.25)) ));
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(0,0,0), .scale = vec2(1,1),
            .color_mul = vec4(0), .color_add = vec4_mul(cont->debug_color, vec4(1,1,1,0.25)) ));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }
    

    /*  Iterate through all the children and recurse    */
    if(grp->children_added) {
        group_sort_layers(ctx, grp);
    }
    int i;
    struct pg_ui_content* iter_cont;
    pg_ui_t iter_ref;
    /*  Do the elements first so they can be drawn all in one draw call */
    ARR_FOREACH(grp->children_arr, iter_ref, i) {
        iter_cont = ui_dereference(ctx, iter_ref);
        if(!iter_cont) continue;
        ui_content_draw(ctx, iter_cont, &in_frame, commands);
    }

    /*  Now redraw the stencil quad to get rid of all the potential nested
        stencil quads   */
    if(grp->enable_clip) {
        struct pg_gpu_stencil_state begin_stencil_state = {
            .enabled = 1, .write_mask = 0xFF,
            .func = PG_GPU_EQUAL, .func_ref = in_frame.clip, .func_mask = 0xFF,
            .op_stencil_fail = PG_STENCIL_KEEP, .op_depth_fail = PG_STENCIL_KEEP,
            .op_both_pass = PG_STENCIL_DECR };
        struct pg_gpu_stencil_state end_stencil_state = {
            .enabled = 1, .write_mask = 0x00,
            .func = PG_GPU_EQUAL, .func_ref = in_frame.clip - 1, .func_mask = 0xFF,
            .op_stencil_fail = PG_STENCIL_KEEP, .op_depth_fail = PG_STENCIL_KEEP,
            .op_both_pass = PG_STENCIL_KEEP };


        /*  Add GPU commands to the buffer  */
        pg_gpu_cmd_stencil(begin_stencil_state);
        pg_gpu_cmd_color_mask(0,0,0,0);

        /*  Draw the quad, with stencil state commands on either side   */
        pg_quadbatch_next(&ctx->quadbatch, &mat_clip);
        pg_quadbatch_add_quad(&ctx->quadbatch, &PG_EZQUAD(
            .pos = vec3(0,0,0), .scale = vec2(1,1),
            .color_mul = 0x00000000, .color_add = 0xFFFFFFFF ));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);

        pg_gpu_cmd_color_mask(1,1,1,1);
        pg_gpu_cmd_stencil(end_stencil_state);
    }
}



static void ui_element_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                         const struct draw_frame* frame, pg_gpu_command_arr_t* commands)
{
    if(cont->elem.draw_order == PG_UI_DRAW_NONE) return;

    /*  Get the transforms for the element components   */
    struct pg_ui_element* elem = &cont->elem;
    struct pg_gfx_transform tx_main, tx_text, tx_image, tx_action;
    ui_element_get_transforms(elem, &frame->space, &tx_main, &tx_text, &tx_image, &tx_action);
    mat4 mat_main = mat4_mul(mat4_identity(), tx_main.tx);
    mat4 mat_text = mat4_mul(mat4_identity(), tx_text.tx);
    mat4 mat_image = mat4_mul(mat4_identity(), tx_image.tx);
    mat4 mat_action = mat4_mul(mat4_identity(), tx_action.tx);

    /*  Get color properties    */
    vec4 text_color = elem->properties[PG_UI_TEXT_COLOR].current_value;
    vec4 image_color_mul = elem->properties[PG_UI_IMAGE_COLOR_MUL].current_value;
    vec4 image_color_add = elem->properties[PG_UI_IMAGE_COLOR_ADD].current_value;

    /*  Draw debugging stuff    */
    pg_gpu_record_commands(commands);
    if(ctx->debug_containers) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_main);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(0), .scale = vec2(1.1,1.1), .rotation = 0,
            .color_mul = vec4(0), .color_add = vec4_mul(cont->debug_color, vec4(0.25, 0.25, 0.25, 0.25))));
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(0), .scale = vec2(1,1), .rotation = 0,
            .color_mul = vec4(0), .color_add = vec4_mul(cont->debug_color, vec4(1,1,1,0.25))));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }
    if(ctx->debug_action_areas) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_action);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(0), .scale = vec2(1,1), .rotation = 0,
            .color_mul = vec4(0), .color_add = vec4(0,1,0,0.25)));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
        mat4 tmp_identity = mat4_identity();
        pg_quadbatch_next(&ctx->quadbatch, &tmp_identity);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(VEC_XYZ(cont->mouse_pos)), .scale = vec2(0.1,0.1), .rotation = 0,
            .color_mul = vec4(0), .color_add = vec4(0,1,0,0.25)));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
        pg_quadbatch_next(&ctx->quadbatch, &mat_action);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &PG_EZDRAW_2D(
            .pos = vec3(VEC_XYZ(cont->cursor_3d_hit_pos)), .scale = vec2(0.2,0.2), .rotation = 0,
            .color_mul = vec4(0), .color_add = vec4(1,1,0,1)));
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }

    /*  Add the quads to the batch (accounting for the desired draw ordering)   */
    const struct pg_draw_text text_draw = PG_EZDRAW_TEXT(
        .pos = vec3(0), .scale = vec2(1,1),
        .form = &elem->text_form, .color = text_color,
        .anchor = elem->text_anchor );
    const struct pg_draw_2d sprite_draw = PG_EZDRAW_2D(
        .pos = vec3(0), .scale = vec2(1,1), .rotation = 0,
        .uv_v = elem->image_anim.uv.v,
        .color_mul = image_color_mul, .color_add = image_color_add );
    if(elem->draw_order == PG_UI_TEXT_ONLY || elem->draw_order == PG_UI_TEXT_THEN_IMAGE) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_text);
        pg_quadbatch_add_text(&ctx->quadbatch, &text_draw);
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }
    if(elem->draw_order == PG_UI_IMAGE_ONLY || elem->draw_order == PG_UI_IMAGE_THEN_TEXT || elem->draw_order == PG_UI_TEXT_THEN_IMAGE) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_image);
        pg_quadbatch_add_sprite(&ctx->quadbatch, &sprite_draw);
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }
    if(elem->draw_order == PG_UI_IMAGE_THEN_TEXT) {
        pg_quadbatch_next(&ctx->quadbatch, &mat_text);
        pg_quadbatch_add_text(&ctx->quadbatch, &text_draw);
        pg_gpu_cmd_draw_quadbatch(&ctx->quadbatch);
    }
}




/*******************************/
/*  Animate                    */
/*******************************/

static void ui_content_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont);
static void ui_group_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont);
static void ui_element_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont);

void pg_ui_context_animate(struct pg_ui_context* ctx, float time)
{
    ctx->time_delta = time - ctx->time;
    ctx->time = time;
    ui_content_animate(ctx, ui_dereference(ctx, ctx->root));
}



static void ui_content_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont)
{
    if(cont->disabled) return;
    if(cont->type == PG_UI_GROUP) ui_group_animate(ctx, cont);
    else if(cont->type == PG_UI_ELEMENT) ui_element_animate(ctx, cont);
}

static void ui_group_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont)
{
    struct pg_ui_group* grp = &cont->grp;
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    pg_animator_step(&grp->properties[PG_UI_POS], ctx->time_delta);
    pg_animator_step(&grp->properties[PG_UI_SCALE], ctx->time_delta);
    pg_animator_step(&grp->properties[PG_UI_ROTATION], ctx->time_delta);
    pg_animator_step(&grp->properties[PG_UI_CLIP_POS], ctx->time_delta);
    pg_animator_step(&grp->properties[PG_UI_CLIP_SCALE], ctx->time_delta);
    /*  Iterate through all the children and recurse    */
    int i;
    struct pg_ui_content* iter_cont;
    pg_ui_t iter_ref;
    ARR_FOREACH(grp->children_arr, iter_ref, i) {
        iter_cont = ui_dereference(ctx, iter_ref);
        if(!iter_cont) continue;
        ui_content_animate(ctx, iter_cont);
    }
}

static void ui_element_animate(struct pg_ui_context* ctx, struct pg_ui_content* cont)
{
    struct pg_ui_element* elem = &cont->elem;
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    pg_animator_step(&elem->properties[PG_UI_POS], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_SCALE], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_ROTATION], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_TEXT_COLOR], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_TEXT_POS], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_TEXT_SCALE], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_TEXT_ROTATION], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_IMAGE_POS], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_IMAGE_SCALE], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_IMAGE_COLOR_MUL], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_IMAGE_COLOR_ADD], ctx->time_delta);
    pg_animator_step(&elem->properties[PG_UI_IMAGE_ROTATION], ctx->time_delta);
    pg_imageset_step(ctx->images, &elem->image_anim, ctx->time_delta);
}


