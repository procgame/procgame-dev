#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"
#include "pg_vr/pg_vr.h"

/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct quadbatch_asset_source {
    int capacity;
    pg_asset_handle_t img_texture_asset;
    pg_asset_handle_t font_texture_asset;
    pg_asset_handle_t render_stage_asset;
    bool has_single_attachments;
    pg_asset_handle_t resources_asset;
    pg_asset_handle_t output_asset;
    bool has_vr_attachments;
    pg_asset_handle_t output_left_asset;
    pg_asset_handle_t output_right_asset;
};


static void* quadbatch_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* json)
{
    cJSON* capacity_json, *img_texture_json, *font_texture_json,
        *output_json, *output_left_json, *output_right_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(6,
        PG_JSON_LAYOUT_ITEM("capacity", cJSON_Number, &capacity_json),
        PG_JSON_LAYOUT_ITEM("image_texture",  PG_JSON_ASSET, &img_texture_json),
        PG_JSON_LAYOUT_ITEM("font_texture",  PG_JSON_ASSET, &font_texture_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("output",  PG_JSON_ASSET, &output_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("vr_output_left",  PG_JSON_ASSET, &output_left_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("vr_output_right",  PG_JSON_ASSET, &output_right_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return NULL;
    }

    struct quadbatch_asset_source parsed_source = {};

    /*  Read capacity value */
    parsed_source.capacity = capacity_json->valueint;

    /*  Read the resource assets    */
    pg_asset_handle_t img_texture_asset = pg_asset_from_json(asset_handle.mgr,
            "pg_gpu_texture", NULL, img_texture_json);
    pg_asset_handle_t font_texture_asset = pg_asset_from_json(asset_handle.mgr,
            "pg_gpu_texture", NULL, font_texture_json);
    pg_asset_depends(asset_handle, img_texture_asset);
    pg_asset_depends(asset_handle, font_texture_asset);
    parsed_source.img_texture_asset = img_texture_asset;
    parsed_source.font_texture_asset = font_texture_asset;

    /*  Read output asset  */
    if(output_json) {
        parsed_source.has_single_attachments = true;
        pg_asset_handle_t output_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_json);
        pg_asset_depends(asset_handle, output_asset);
        parsed_source.output_asset = output_asset;
    }
    if(pg_have_vr() && output_left_json && output_right_json) {
        parsed_source.has_vr_attachments = true;
        pg_asset_handle_t output_left_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_left_json);
        pg_asset_handle_t output_right_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_right_json);
        pg_asset_depends(asset_handle, output_left_asset);
        pg_asset_depends(asset_handle, output_right_asset);
        parsed_source.output_left_asset = output_left_asset;
        parsed_source.output_right_asset = output_right_asset;
    }

    /*  The quadbatch creates its own resource set and renderstage  */
    parsed_source.resources_asset =
            pg_asset_declare_child(asset_handle, "pg_gpu_resource_set", "resources");
    parsed_source.render_stage_asset =
            pg_asset_declare_child(asset_handle, "pg_gpu_render_stage", "render_stage");

    /*  Allocate the return object if nothing went wrong    */
    struct quadbatch_asset_source* return_source = malloc(sizeof(*return_source));
    *return_source = parsed_source;
    return return_source;
}



static void quadbatch_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct quadbatch_asset_source* qb_source = (struct quadbatch_asset_source*)parsed_source;
    free(qb_source);
}



static void* quadbatch_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct quadbatch_asset_source* qb_source = (struct quadbatch_asset_source*)parsed_source;

    struct pg_quadbatch* qb = malloc(sizeof(*qb));
    pg_quadbatch_init(qb, qb_source->capacity);

    pg_gpu_vertex_assembly_t* dummy_verts = pg_asset_get_data_by_name(asset_handle.mgr, "__pg_empty_vertex_assembly");
    pg_gpu_pipeline_t* qb_pipeline = pg_asset_get_data_by_name(asset_handle.mgr, "__pg_quadbatch_pipeline");
    if(!qb_pipeline || !dummy_verts) {
        pg_log(PG_LOG_ERROR, "No built-in quadbatch pipeline or empty vertex asset");
        return NULL;
    }

    /*  Make resource set asset data and put it in the asset manager    */
    pg_gpu_texture_t* img_tex = pg_asset_get_data(qb_source->img_texture_asset);
    pg_gpu_texture_t* font_tex = pg_asset_get_data(qb_source->font_texture_asset);
    pg_gpu_resource_set_t* gpu_resources = pg_quadbatch_create_gpu_resources(qb, img_tex, font_tex);
    pg_asset_from_data(asset_handle.mgr, "pg_gpu_resource_set",
            pg_asset_get_name(qb_source->resources_asset), gpu_resources, true);
    
    pg_gpu_render_stage_t* stage = pg_gpu_render_stage_create(qb_pipeline);
    pg_gpu_render_stage_set_debug_name(stage, pg_asset_get_name(asset_handle));
    pg_asset_from_data(asset_handle.mgr, "pg_gpu_render_stage",
            pg_asset_get_name(qb_source->render_stage_asset), stage, true);

    pg_gpu_render_stage_add_input_group(stage, PG_TARGET_FILTER_ALL, "quads", dummy_verts, gpu_resources);

    if(qb_source->has_single_attachments) {
        pg_gpu_framebuffer_t* gpu_fbuf = pg_asset_get_data(qb_source->output_asset);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_MAIN, gpu_fbuf);
    }

    if(qb_source->has_vr_attachments) {
        pg_gpu_framebuffer_t* fbuf_left = pg_asset_get_data(qb_source->output_left_asset);
        pg_gpu_framebuffer_t* fbuf_right = pg_asset_get_data(qb_source->output_right_asset);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_VR_LEFT, fbuf_left);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_VR_RIGHT, fbuf_right);
    }

    return qb;
}



static void quadbatch_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    struct pg_quadbatch* qb = (struct pg_quadbatch*)loaded_data;
    pg_quadbatch_deinit(qb);
    free(qb);
}

struct pg_asset_loader* pg_quadbatch_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_quadbatch",
        .type_name_len = strlen("pg_quadbatch"),
        .parse = quadbatch_asset_parse,
        .unparse = quadbatch_asset_unparse,
        .load = quadbatch_asset_load,
        .unload = quadbatch_asset_unload,
    };
    return loader;
}





