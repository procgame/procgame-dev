#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"

/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct ui_group_asset_source {
    pg_asset_handle_t context;
    bool have_parent;
    pg_asset_handle_t parent;
    /*  General */
    char name[PG_UI_NAME_MAXLEN];
    bool enabled;
    bool enable_clip;
    bool enable_3d;
    vec2 resolution;
    vec3 clip_pos;
    vec3 clip_scale;
    vec3 pos;
    vec3 scale;
    float rotation;
    float layer;
    bool fix_aspect;
    ARR_T(struct ui_element_source {
        char name[PG_UI_NAME_MAXLEN];
        /*  General */
        bool enabled;
        vec3 pos;
        vec3 scale;
        float rotation;
        vec3 action_pos;
        vec3 action_scale;
        float layer;
        /*  Image   */
        float image_rotation;
        vec3 image_pos;
        vec3 image_scale;
        vec4 image_color_mul;
        vec4 image_color_add;
        /*  Text    */
        float text_rotation;
        vec2 text_rotation_anchor;
        vec3 text_pos;
        vec3 text_scale;
        vec4 text_color;
        /*  Animate-able fields above, static fields below  */
        char image_name[PG_IMAGE_NAME_MAXLEN];
        char image_frame[PG_IMAGE_NAME_MAXLEN];
        char image_sequence[PG_IMAGE_NAME_MAXLEN];
        struct pg_image_stepper image_anim;
        bool fix_aspect;
        enum pg_ui_draw_order draw_order;
        enum pg_ui_action_item action_item;
        vec2 text_anchor;
        pg_data_t text;
        vec2 text_size;
        char callback_names[PG_UI_ELEM_CALLBACKS][256];
    }) elements;
};


static bool parse_ui_element(pg_asset_handle_t asset_handle, struct ui_element_source* elem, cJSON* json)
{
    cJSON* name_json, *enabled_json, *fix_aspect_json,
        *pos_json, *scale_json, *rotation_json, *layer_json,
        *image_name_json, *image_frame_json, *image_sequence_json,
        *image_pos_json, *image_scale_json, *image_rotation_json,
        *image_color_mul_json, *image_color_add_json,
        *text_pos_json, *text_scale_json, *text_rotation_json, *text_rotation_anchor_json,
        *text_json, *text_color_json, *text_size_json,
        *action_pos_json, *action_scale_json,
        *draw_order_json, *action_item_json,
        *on_click_json, *on_hold_json, *on_release_json, *on_scroll_json,
        *on_enter_json, *on_leave_json, *on_update_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(33,
        /*  Basics  */
        PG_JSON_LAYOUT_ITEM(         "name",                    cJSON_String,   &name_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enabled",                 cJSON_Bool,     &enabled_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("fix_aspect",              cJSON_Bool,     &fix_aspect_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("pos",                     cJSON_Array,    &pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("scale",                   cJSON_Array,    &scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("rotation",                cJSON_Number,   &rotation_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("layer",                   cJSON_Number,   &layer_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("draw_order",              cJSON_String,   &draw_order_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("action_item",             cJSON_Number,   &action_item_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("action_pos",               cJSON_Array,    &action_pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("action_scale",             cJSON_Array,    &action_scale_json),
        /*  Image options   */
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_name",              cJSON_String,   &image_name_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_frame",             cJSON_String,   &image_frame_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_sequence",          cJSON_String,   &image_sequence_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_rotation",          cJSON_Number,   &image_rotation_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_pos",               cJSON_Array,    &image_pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_scale",             cJSON_Array,    &image_scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_color_mul",         cJSON_Array,    &image_color_mul_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image_color_add",         cJSON_Array,    &image_color_add_json),
        /*  Text options    */
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text",                    cJSON_String,   &text_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_rotation",           cJSON_Number,   &text_rotation_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_rotation_anchor",    cJSON_Array,    &text_rotation_anchor_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_pos",                cJSON_Array,    &text_pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_color",              cJSON_Array,    &text_color_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_scale",              cJSON_Array,    &text_scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("text_size",               cJSON_Array,    &text_size_json),
        /*  Callbacks   */
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_click",                cJSON_String,   &on_click_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_hold",                 cJSON_String,   &on_hold_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_release",              cJSON_String,   &on_release_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_scroll",               cJSON_String,   &on_scroll_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_enter",                cJSON_String,   &on_enter_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_leave",                cJSON_String,   &on_leave_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("on_update",               cJSON_String,   &on_update_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return false;
    }
    elem->enabled = enabled_json ? cJSON_IsTrue(enabled_json) : true;
    strncpy(elem->name, name_json->valuestring, PG_UI_NAME_MAXLEN);

    elem->pos =             pos_json ?              json_read_vec3(pos_json) : vec3(0);
    elem->scale =           scale_json ?            json_read_vec3(scale_json) : vec3(1,1);
    elem->rotation =        rotation_json ?         json_read_float(rotation_json) : 0;
    elem->layer =           layer_json ?            json_read_float(layer_json) : 0;
    elem->image_rotation =  image_rotation_json ?   json_read_float(image_rotation_json) : 0;
    elem->image_pos =       image_pos_json ?         json_read_vec3(image_pos_json) : vec3(0);
    elem->image_scale =     image_scale_json ?       json_read_vec3(image_scale_json) : vec3(1,1);
    elem->image_color_mul = image_color_mul_json ?   json_read_vec4(image_color_mul_json) : vec4(1,1,1,1);
    elem->image_color_add = image_color_add_json ?   json_read_vec4(image_color_add_json) : vec4(0);
    elem->text_rotation =   text_rotation_json ?    json_read_float(text_rotation_json) : 0;
    elem->text_rotation_anchor = text_rotation_anchor_json ?  json_read_vec2(text_rotation_anchor_json) : vec2(0);
    elem->text_pos =        text_pos_json ?         json_read_vec3(text_pos_json) : vec3(0);
    elem->text_color =      text_color_json ?       json_read_vec4(text_color_json) : vec4(1,1,1,1);
    elem->text_size =       text_size_json ?        json_read_vec2(text_size_json) : vec2(1,1);
    elem->text_scale =      text_scale_json ?       json_read_vec3(text_scale_json) : vec3(1,1);
    elem->action_pos =       action_pos_json ?         json_read_vec3(action_pos_json) : vec3(0);
    elem->action_scale =     action_scale_json ?       json_read_vec3(action_scale_json) : vec3(1,1);
    elem->fix_aspect = cJSON_IsTrue(fix_aspect_json);

    elem->text = text_json ? pg_data_init(PG_STRING, strlen(text_json->valuestring), text_json->valuestring) : (pg_data_t){};
    if(image_name_json)     strncpy(elem->image_name, image_name_json->valuestring, PG_IMAGE_NAME_MAXLEN);
    if(image_frame_json)    strncpy(elem->image_frame, image_frame_json->valuestring, PG_IMAGE_NAME_MAXLEN);
    if(image_sequence_json) strncpy(elem->image_sequence, image_sequence_json->valuestring, PG_IMAGE_NAME_MAXLEN);

    elem->draw_order =      draw_order_json ?       pg_ui_draw_order_from_string(draw_order_json->valuestring) : PG_UI_IMAGE_THEN_TEXT;
    elem->action_item =     action_item_json ?      pg_ui_action_item_from_string(action_item_json->valuestring) : PG_UI_ACTION_IMAGE;

    if(on_click_json) strncpy(elem->callback_names[PG_UI_CLICK], on_click_json->valuestring, 256);
    if(on_hold_json) strncpy(elem->callback_names[PG_UI_HOLD], on_hold_json->valuestring, 256);
    if(on_release_json) strncpy(elem->callback_names[PG_UI_RELEASE], on_release_json->valuestring, 256);
    if(on_scroll_json) strncpy(elem->callback_names[PG_UI_SCROLL], on_scroll_json->valuestring, 256);
    if(on_enter_json) strncpy(elem->callback_names[PG_UI_ENTER], on_enter_json->valuestring, 256);
    if(on_leave_json) strncpy(elem->callback_names[PG_UI_LEAVE], on_leave_json->valuestring, 256);
    if(on_update_json) strncpy(elem->callback_names[PG_UI_UPDATE], on_update_json->valuestring, 256);

    return true;
}

static void* ui_group_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* json)
{
    cJSON* context_json, *parent_json, *enabled_json, *enable_clip_json, *enable_3d_json,
        *resolution_json, *fix_aspect_json, *layer_json, *elements_json,
        *clip_pos_json, *clip_scale_json,
        *pos_json, *scale_json, *rotation_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(14,
        PG_JSON_LAYOUT_ITEM(         "context",         PG_JSON_ASSET,  &context_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("parent",          PG_JSON_ASSET,  &parent_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enabled",         cJSON_Bool,     &enabled_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enable_clip",     cJSON_Bool,     &enable_clip_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enable_3d",       cJSON_Bool,     &enable_3d_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("resolution",      cJSON_Array,    &resolution_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("fix_aspect",      cJSON_Bool,     &fix_aspect_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("clip_pos",        cJSON_Array,    &clip_pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("clip_scale",      cJSON_Array,    &clip_scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("pos",             cJSON_Array,    &pos_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("scale",           cJSON_Array,    &scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("rotation",        cJSON_Number,   &rotation_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("layer",           cJSON_Number,   &layer_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("elements",        cJSON_Array,    &elements_json)
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return NULL;
    }

    struct ui_group_asset_source* parsed_source = malloc(sizeof(*parsed_source));
    *parsed_source = (struct ui_group_asset_source){};
    ARR_INIT(parsed_source->elements);
    parsed_source->context = pg_asset_from_json(asset_handle.mgr, "pg_ui_context", NULL, context_json);
    pg_asset_depends(asset_handle, parsed_source->context);

    /*  This asset depends on the UI context, but the UI's render stage will depend on this
        (so all groups will be loaded before trying to render the UI)   */
    pg_asset_handle_t ui_render_asset = pg_asset_get_child(parsed_source->context, "render_stage");
    pg_asset_depends(ui_render_asset, asset_handle);

    if(parent_json) {
        parsed_source->have_parent = true;
        parsed_source->parent = pg_asset_from_json(asset_handle.mgr, "pg_ui_group", NULL, parent_json);
        pg_asset_depends(asset_handle, parsed_source->parent);
    }
    strncpy(parsed_source->name, pg_asset_get_name(asset_handle), PG_UI_NAME_MAXLEN);
    parsed_source->enabled = enabled_json ? cJSON_IsTrue(enabled_json) : true;
    parsed_source->enable_clip = cJSON_IsTrue(enable_clip_json);
    parsed_source->enable_3d = enable_3d_json ? cJSON_IsTrue(enable_3d_json) : false;
    parsed_source->clip_pos = clip_pos_json ? json_read_vec3(clip_pos_json) : vec3(0);
    parsed_source->clip_scale = clip_scale_json ? json_read_vec3(clip_scale_json) : vec3(0);
    parsed_source->pos = pos_json ? json_read_vec3(pos_json) : vec3(0);
    parsed_source->scale = scale_json ? json_read_vec3(scale_json) : vec3(1,1);
    parsed_source->rotation = rotation_json ? json_read_float(rotation_json) : 0;
    parsed_source->layer = layer_json ? json_read_float(layer_json) : 0;
    parsed_source->fix_aspect = cJSON_IsTrue(fix_aspect_json);
    parsed_source->resolution = resolution_json ? json_read_vec2(resolution_json) : vec2(0,0);

    cJSON* elem_json;
    cJSON_ArrayForEach(elem_json, elements_json) {
        struct ui_element_source* elem_source = ARR_NEW(parsed_source->elements);
        *elem_source = (struct ui_element_source){};
        if(!parse_ui_element(asset_handle, elem_source, elem_json)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid element definition");
            int i;
            struct ui_element_source* del_elem_source;
            ARR_FOREACH_PTR(parsed_source->elements, del_elem_source, i) {
                pg_data_deinit(&del_elem_source->text);
            }
            ARR_DEINIT(parsed_source->elements);
            free(parsed_source);
            return NULL;
        }
    }

    return parsed_source;
}

static void ui_group_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct ui_group_asset_source* group_source = (struct ui_group_asset_source*)parsed_source;
    int i;
    struct ui_element_source* elem_source;
    ARR_FOREACH_PTR(group_source->elements, elem_source, i) {
        pg_data_deinit(&elem_source->text);
    }
    ARR_DEINIT(group_source->elements);
    free(group_source);
}



static void* ui_group_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct ui_group_asset_source* group_source = (struct ui_group_asset_source*)parsed_source;

    struct pg_ui_context* ctx = pg_asset_get_data(group_source->context);

    pg_ui_t parent = group_source->have_parent
        ? ((struct pg_ui_content_reference*)pg_asset_get_data(group_source->parent))->ui
        : pg_ui_context_root(ctx);
    
    struct pg_ui_properties group_props = {
        .enabled = group_source->enabled,
        .layer = group_source->layer,
        .fix_aspect = group_source->fix_aspect,
        .enable_clip = group_source->enable_clip,
        .enable_3d = group_source->enable_3d,
        .resolution = group_source->resolution,
        .clip_pos = group_source->clip_pos,
        .clip_scale = group_source->clip_scale,
        .pos = group_source->pos,
        .scale = group_source->scale,
        .rotation = group_source->rotation };
    pg_ui_t new_group = pg_ui_add_group(ctx, parent, group_source->name, &group_props);
    
    int i;
    struct ui_element_source* elem_source;
    ARR_FOREACH_PTR(group_source->elements, elem_source, i) {
        struct pg_ui_properties elem_props = {
            .enabled = elem_source->enabled,
            .layer = elem_source->layer,
            .fix_aspect = elem_source->fix_aspect,
            .pos = elem_source->pos,
            .scale = elem_source->scale,
            .rotation = elem_source->rotation,
            .action_pos = elem_source->action_pos,
            .action_scale = elem_source->action_scale,
            .image_name = elem_source->image_name,
            .image_frame = elem_source->image_frame,
            .image_sequence = elem_source->image_sequence,
            .image_rotation = elem_source->image_rotation,
            .image_pos = elem_source->image_pos,
            .image_scale = elem_source->image_scale,
            .image_color_mul = elem_source->image_color_mul,
            .image_color_add = elem_source->image_color_add,
            .text_rotation = elem_source->text_rotation,
            .text_rotation_anchor = elem_source->text_rotation_anchor,
            .text_pos = elem_source->text_pos,
            .text_scale = elem_source->text_scale,
            .text_color = elem_source->text_color,
            .text_anchor = elem_source->text_anchor,
            .text = pg_data_value_ptr(&elem_source->text, 0),
            .text_len = elem_source->text.n,
            .draw_order = elem_source->draw_order,
            .action_item = elem_source->action_item,
            .text_size = elem_source->text_size,
            .cb_click = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_CLICK]),
            .cb_hold = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_HOLD]),
            .cb_release = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_RELEASE]),
            .cb_scroll = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_SCROLL]),
            .cb_enter = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_ENTER]),
            .cb_leave = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_LEAVE]),
            .cb_update = pg_ui_context_get_callback(ctx, elem_source->callback_names[PG_UI_UPDATE]),
        };
        pg_ui_add_element(ctx, new_group, elem_source->name, &elem_props);
    }

    struct pg_ui_content_reference* ui_content = malloc(sizeof(*ui_content));
    *ui_content = PG_UI_CONTENT_REFERENCE(ctx, new_group);
    return ui_content;
}

static void ui_group_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    struct pg_ui_content_reference* ui_content = (struct pg_ui_content_reference*)loaded_data;
    pg_ui_delete(ui_content->ctx, ui_content->ui);
    free(ui_content);
}

struct pg_asset_loader* pg_ui_group_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_ui_group",
        .type_name_len = strlen("pg_ui_group"),
        .parse = ui_group_asset_parse,
        .unparse = ui_group_asset_unparse,
        .load = ui_group_asset_load,
        .unload = ui_group_asset_unload,
    };
    return loader;
}






