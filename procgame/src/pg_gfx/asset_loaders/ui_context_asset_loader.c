#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"
#include "pg_vr/pg_vr.h"

/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct ui_context_asset_source {
    pg_asset_handle_t images_asset;
    pg_asset_handle_t fonts_asset;
    bool has_single_output;
    pg_asset_handle_t output_asset;
    bool has_vr_output;
    pg_asset_handle_t output_left_asset;
    pg_asset_handle_t output_right_asset;
    pg_asset_handle_t render_stage_asset;
};


static void* ui_context_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* json)
{
    cJSON* images_json, *fonts_json, *output_json, *output_left_json, *output_right_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(5,
        PG_JSON_LAYOUT_ITEM("images", PG_JSON_ASSET, &images_json),
        PG_JSON_LAYOUT_ITEM("fonts", PG_JSON_ASSET, &fonts_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("output", PG_JSON_ASSET, &output_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("vr_output_left", PG_JSON_ASSET, &output_left_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("vr_output_right", PG_JSON_ASSET, &output_right_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return NULL;
    }

    struct ui_context_asset_source parsed_source = {};

    parsed_source.images_asset = pg_asset_from_json(asset_handle.mgr, "pg_imageset", NULL, images_json);
    parsed_source.fonts_asset = pg_asset_from_json(asset_handle.mgr, "pg_fontset", NULL, fonts_json);
    pg_asset_depends(asset_handle, parsed_source.images_asset);
    pg_asset_depends(asset_handle, parsed_source.fonts_asset);

    if(output_json) {
        parsed_source.has_single_output = true;
        parsed_source.output_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_json);
        pg_asset_depends(asset_handle, parsed_source.output_asset);
    }
    if(pg_have_vr() && output_left_json && output_right_json) {
        parsed_source.has_vr_output = true;
        parsed_source.output_left_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_left_json);
        parsed_source.output_right_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_framebuffer", NULL, output_right_json);
        pg_asset_depends(asset_handle, parsed_source.output_left_asset);
        pg_asset_depends(asset_handle, parsed_source.output_right_asset);
    }

    parsed_source.render_stage_asset = pg_asset_declare_child(asset_handle, "pg_gpu_render_stage", "render_stage");

    /*  Allocate the return object if nothing went wrong    */
    struct ui_context_asset_source* return_source = malloc(sizeof(*return_source));
    *return_source = parsed_source;
    return return_source;
}



static void ui_context_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct ui_context_asset_source* ctx_source = (struct ui_context_asset_source*)parsed_source;
    free(ctx_source);
}



static void* ui_context_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct ui_context_asset_source* ctx_source = (struct ui_context_asset_source*)parsed_source;

    /*  Get the built-in empty vertex assembly, and quadbatching pipeline   */
    pg_gpu_vertex_assembly_t* dummy_verts = pg_asset_get_data_by_name(asset_handle.mgr, "__pg_empty_vertex_assembly");
    pg_gpu_pipeline_t* qb_pipeline = pg_asset_get_data_by_name(asset_handle.mgr, "__pg_ui_pipeline");
    if(!qb_pipeline || !dummy_verts) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "No built-in UI pipeline or empty vertex asset");
        return NULL;
    }

    /*  Get the dependency assets   */
    struct pg_imageset* images = pg_asset_get_data(ctx_source->images_asset);
    struct pg_fontset* fonts = pg_asset_get_data(ctx_source->fonts_asset);
    if(!images || !fonts) return NULL;

    /*  Allocate and initialize the UI context  */
    struct pg_ui_context* ctx = malloc(sizeof(*ctx));
    pg_ui_context_init(ctx, images, fonts);

    /*  Make renderstage asset data and put it in the asset manager     */
    pg_gpu_render_stage_t* stage = pg_gpu_render_stage_create(qb_pipeline);
    pg_asset_from_data(asset_handle.mgr, "pg_gpu_render_stage",
            pg_asset_get_name(ctx_source->render_stage_asset), stage, true);

    pg_gpu_render_stage_add_input_group(stage, PG_TARGET_FILTER_ALL, "ui", dummy_verts, ctx->resources);

    if(ctx_source->has_single_output) {
        pg_gpu_framebuffer_t* output_fbuf = pg_asset_get_data(ctx_source->output_asset);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_MAIN, output_fbuf);
    }
    if(ctx_source->has_vr_output) {
        pg_gpu_framebuffer_t* fbuf_left = pg_asset_get_data(ctx_source->output_left_asset);
        pg_gpu_framebuffer_t* fbuf_right = pg_asset_get_data(ctx_source->output_right_asset);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_VR_LEFT, fbuf_left);
        pg_gpu_render_stage_add_target(stage, PG_TARGET_FILTER_VR_RIGHT, fbuf_right);
    }

    pg_ui_context_set_render_stage(ctx, stage);

    return ctx;
}



static void ui_context_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    struct pg_ui_context* ctx = (struct pg_ui_context*)loaded_data;
    pg_ui_context_deinit(ctx);
    free(ctx);
}

struct pg_asset_loader* pg_ui_context_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_ui_context",
        .type_name_len = strlen("pg_ui_context"),
        .parse = ui_context_asset_parse,
        .unparse = ui_context_asset_unparse,
        .load = ui_context_asset_load,
        .unload = ui_context_asset_unload,
    };
    return loader;
}





