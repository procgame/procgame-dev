#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"

/****************************/
/*  GPU TEXTURE ASSETS      */
/****************************/

struct armature_asset_source {
    SARR_T(PG_ARMATURE_MAX_BONES, struct pg_armature_bone) bones;
    SARR_T(PG_ARMATURE_MAX_POSES, struct armature_pose_source {
        char name[PG_ARMATURE_NAME_MAXLEN];
        SARR_T(PG_ARMATURE_MAX_BONES, struct armature_posebone_source {
            char bone_name[PG_ARMATURE_NAME_MAXLEN];
            struct pg_armature_bone_tx bone_tx;
        }) bones;
    }) poses;
    SARR_T(PG_ARMATURE_MAX_SEQUENCES, struct armature_sequence_source {
        char name[PG_ARMATURE_NAME_MAXLEN];
        char pose_names[PG_ARMATURE_SEQUENCE_MAXLEN][PG_ARMATURE_NAME_MAXLEN];
        int n_poses;
    }) sequences;
    char bone_parent_names[PG_ARMATURE_MAX_BONES][PG_ARMATURE_NAME_MAXLEN];
};

static void* armature_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* loader, cJSON* json)
{
    cJSON* bones_json, *poses_json, *sequences_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(3,
        PG_JSON_LAYOUT_ITEM("bones", cJSON_Array, &bones_json),
        PG_JSON_LAYOUT_ITEM("poses", cJSON_Array, &poses_json),
        PG_JSON_LAYOUT_ITEM("sequences", cJSON_Array, &sequences_json),
    );
    if(!pg_load_json_layout(json, &layout)) return NULL;

    struct armature_asset_source parsed_source = {};
    SARR_INIT(parsed_source.bones);
    SARR_INIT(parsed_source.poses);
    SARR_INIT(parsed_source.sequences);
    
    cJSON* bone_json;
    cJSON_ArrayForEach(bone_json, bones_json) {
        cJSON* name_json, *rot_json, *pos_json, *len_json, *parent_json;
        struct pg_json_layout bone_layout = PG_JSON_LAYOUT(4,
            PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
            PG_JSON_LAYOUT_ITEM("parent", cJSON_String, &parent_json),
            PG_JSON_LAYOUT_ITEM("rot", cJSON_Array, &rot_json),
            PG_JSON_LAYOUT_ITEM("pos", cJSON_Array, &pos_json),
            PG_JSON_LAYOUT_ITEM("len", cJSON_Array, &len_json),
        );
        if(!pg_load_json_layout(bone_json, &bone_layout)) return NULL;
        struct pg_armature_bone* new_bone = ARR_NEW(parsed_source.bones);
        pg_armature_init_bone(new_bone,
            name_json->valuestring,
            json_read_quat(rot_json),
            json_read_vec3(pos_json),
            json_read_float(len_json));
        strncpy(parsed_source.bone_parent_names[parsed_source.bones.len-1],
                parent_json->valuestring, PG_ARMATURE_NAME_MAXLEN);
    }

    cJSON* pose_json;
    cJSON_ArrayForEach(pose_json, poses_json) {
        cJSON* name_json, *posebones_json;
        struct pg_json_layout pose_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
            PG_JSON_LAYOUT_ITEM("bones", cJSON_Array, &posebones_json),
        );
        if(!pg_load_json_layout(pose_json, &pose_layout)) return NULL;
        struct armature_pose_source* new_pose = ARR_NEW(parsed_source.poses);

        cJSON* posebone_json;
        cJSON_ArrayForEach(posebone_json, posebones_json) {
            cJSON* posebone_bone_json, *rot_json, *move_json, *enabled_json;
            struct pg_json_layout posebone_layout = PG_JSON_LAYOUT(4,
                PG_JSON_LAYOUT_ITEM("bone", cJSON_String, &posebone_bone_json),
                PG_JSON_LAYOUT_ITEM("rot", cJSON_Array, &rot_json),
                PG_JSON_LAYOUT_ITEM("move", cJSON_Array, &move_json),
                PG_JSON_LAYOUT_ITEM("enabled", cJSON_Bool, &enabled_json),
            );
            if(!pg_load_json_layout(posebone_json, &posebone_layout)) return NULL;
            struct armature_posebone_source* new_posebone = ARR_NEW(new_pose->bones);
            strncpy(new_posebone->bone_name, posebone_bone_json->valuestring, PG_ARMATURE_NAME_MAXLEN);
            new_posebone->bone_tx.rot = json_read_quat(rot_json);
            new_posebone->bone_tx.move = json_read_vec3(move_json);
            new_posebone->bone_tx.enabled = enabled_json ? cJSON_IsTrue(enabled_json) : true;
        }
    }

    cJSON* sequence_json;
    cJSON_ArrayForEach(sequence_json, sequences_json) {
        cJSON* name_json, *seqposes_json;
        struct pg_json_layout sequence_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
            PG_JSON_LAYOUT_ITEM("poses", cJSON_Array, &seqposes_json),
        );
        if(!pg_load_json_layout(sequence_json, &sequence_layout)) return NULL;
        struct armature_sequence_source* new_seq = ARR_NEW(parsed_source.sequences);
        strncpy(new_seq->name, name_json->valuestring, PG_ARMATURE_NAME_MAXLEN);
        int seqpose_idx = 0;
        cJSON* seqpose_json;
        cJSON_ArrayForEach(seqpose_json, seqposes_json) {
            strncpy(new_seq->pose_names[seqpose_idx], seqpose_json->valuestring, PG_ARMATURE_NAME_MAXLEN);
            ++seqpose_idx;
        }
        new_seq->n_poses = seqpose_idx;
    }

    struct armature_asset_source* output_parsed = malloc(sizeof(*output_parsed));
    *output_parsed = parsed_source;
    return output_parsed;
}

static void armature_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    free(parsed_source_ptr);
}


static void* armature_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    struct armature_asset_source* parsed_source = (struct armature_asset_source*)parsed_source_ptr;
    
    struct pg_armature* rig = malloc(sizeof(*rig));
    pg_armature_init(rig);

    int i;
    struct pg_armature_bone* src_bone;
    ARR_FOREACH_PTR(parsed_source->bones, src_bone, i) pg_armature_add_bone(rig, src_bone);

    for(i = 0; i < rig->bones.len; ++i) {
        int parent_bone = pg_armature_get_bone_by_name(rig, parsed_source->bone_parent_names[i]);
        if(parent_bone == -1) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "bone %s has non-existent parent bone '%s'",
                rig->bones.data[i].name, parsed_source->bone_parent_names[i]);
            pg_armature_deinit(rig);
            return NULL;
        }
        pg_armature_set_bone_parent(rig, i, parent_bone);
    }

    struct armature_pose_source* src_pose;
    ARR_FOREACH_PTR(parsed_source->poses, src_pose, i) {
        struct pg_armature_pose new_pose;
        pg_armature_pose_empty(&new_pose);
        strncpy(new_pose.name, src_pose->name, PG_ARMATURE_NAME_MAXLEN);
        int j;
        struct armature_posebone_source* src_posebone;
        ARR_FOREACH_PTR(src_pose->bones, src_posebone, j) {
            int posebone_bone_idx = pg_armature_get_bone_by_name(rig, src_posebone->bone_name);
            if(posebone_bone_idx == -1) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "pose %s affects non-existent bone '%s'",
                    src_pose->name, src_posebone->bone_name);
                pg_armature_deinit(rig);
                return NULL;
            }
            new_pose.bones.data[posebone_bone_idx] = src_posebone->bone_tx;
        }
        pg_armature_add_pose(rig, &new_pose);
    }

    struct armature_sequence_source* src_seq;
    ARR_FOREACH_PTR(parsed_source->sequences, src_seq, i) {
        struct pg_armature_sequence new_seq = {};
        SARR_INIT(new_seq.poses);
        strncpy(new_seq.name, src_seq->name, PG_ARMATURE_NAME_MAXLEN);
        for(int j = 0; j < src_seq->n_poses; ++j) {
            int seqpose_idx = pg_armature_get_pose_by_name(rig, src_seq->pose_names[j]);
            if(seqpose_idx == -1) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "sequence %s includes non-existent pose '%s'",
                        src_seq->name, src_seq->pose_names[j]);
                pg_armature_deinit(rig);
                return NULL;
            }
            ARR_PUSH(new_seq.poses, seqpose_idx);
        }
        pg_armature_add_sequence(rig, &new_seq);
    }

    return rig;
}

static void armature_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_armature_deinit((struct pg_armature*)loaded_data);
}

struct pg_asset_loader* pg_armature_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_armature",
        .type_name_len = strlen("pg_armature"),
        .parse = armature_asset_parse,
        .unparse = armature_asset_unparse,
        .load = armature_asset_load,
        .unload = armature_asset_unload,
    };
    return loader;
}


