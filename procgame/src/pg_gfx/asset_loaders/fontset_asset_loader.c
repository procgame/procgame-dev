#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"

/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct fontset_asset_source {
    pg_asset_handle_t texture_asset;
    SARR_T(16, struct single_font_source {
        struct pg_filepath ttf_path;
        wchar_t load_chars[2048];
        int n_load_chars;
        int size;
    }) fonts_info;
};


static void* fontset_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* json)
{
    cJSON* fonts_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(1,
        PG_JSON_LAYOUT_ITEM("fonts", cJSON_Array, &fonts_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return NULL;
    }

    struct fontset_asset_source parsed_source = {};
    SARR_INIT(parsed_source.fonts_info);
    
    /*  Read filenames  */
    int font_idx = 0;
    cJSON* font_json;
    cJSON_ArrayForEach(font_json, fonts_json) {
        cJSON* ttf_json, *size_json, *load_chars_json;
        struct pg_json_layout font_layout = PG_JSON_LAYOUT(3,
            PG_JSON_LAYOUT_ITEM("ttf", cJSON_String, &ttf_json),
            PG_JSON_LAYOUT_ITEM("size", PG_JSON_VARIABLE, &size_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("load_chars", cJSON_String, &load_chars_json),
        );
        if(!pg_load_json_layout(font_json, &font_layout)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong font JSON layout (in fonts[%d])", font_idx);
            return NULL;
        }

        /*  Convert load chars to list of unicode codepoints     */
        struct single_font_source* font_src = ARR_NEW(parsed_source.fonts_info);
        strncpy(font_src->ttf_path.path, ttf_json->valuestring, PG_FILE_PATH_MAXLEN);
        font_src->size = pg_asset_manager_read_int(asset_handle.mgr, size_json);
        mbstate_t state;
        memset(&state, 0, sizeof(state));
        if(load_chars_json) {
            const char* load_chars_str = load_chars_json->valuestring;
            font_src->n_load_chars = mbsrtowcs(font_src->load_chars, &load_chars_str, 2048, &state);
        }
        if(++font_idx >= 16) break;
    }

    /*  Declare child texture asset */
    parsed_source.texture_asset =
            pg_asset_declare_child(asset_handle, "pg_gpu_texture", "texture");

    /*  Allocate the return object if nothing went wrong    */
    struct fontset_asset_source* return_source = malloc(sizeof(*return_source));
    *return_source = parsed_source;
    return return_source;
}



static void fontset_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct fontset_asset_source* font_source = (struct fontset_asset_source*)parsed_source;
    free(font_source);
}



static void* fontset_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    int n_search_paths = 0;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_handle.mgr, &n_search_paths);

    struct fontset_asset_source* fontset_source = (struct fontset_asset_source*)parsed_source;
    const int n_fonts = fontset_source->fonts_info.len;

    /*  Reformat the source info   */
    const char* filepath_ptrs[16] = {};
    const wchar_t* load_chars[16] = {};
    int n_load_chars[16] = {};
    int sizes[16] = {};
    struct single_font_source* font_src;
    int i;
    ARR_FOREACH_PTR(fontset_source->fonts_info, font_src, i) {
        filepath_ptrs[i] = font_src->ttf_path.path;
        sizes[i] = font_src->size;
        load_chars[i] = font_src->load_chars;
        n_load_chars[i] = font_src->n_load_chars;
    }

    /*  Load the TrueType font definitions for all the fonts in the set */
    struct pg_fontset* fontset = malloc(sizeof(*fontset));
    pg_fontset_load_search_paths(fontset, filepath_ptrs, sizes, fontset_source->fonts_info.len,
            vec2(1024,1024), search_paths, n_search_paths);

    /*  Rasterize all the desired characters for each font into the (local memory) images */
    for(i = 0; i < n_fonts; ++i) {
        struct pg_font* font = pg_fontset_font(fontset, i);
        pg_font_load_chars(font, load_chars[i], n_load_chars[i]);
    }

    /*  Upload the rasterized font textures to the GPU and create the child asset to hold it    */
    pg_gpu_texture_t* gpu_tex = pg_fontset_get_gpu_texture(fontset);
    pg_asset_handle_t gpu_tex_asset = fontset_source->texture_asset;
    pg_asset_from_data(asset_handle.mgr, "pg_gpu_texture",
            pg_asset_get_name(gpu_tex_asset), gpu_tex, false);

    return fontset;
}



static void fontset_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    struct pg_fontset* fonts = (struct pg_fontset*)loaded_data;
    pg_fontset_deinit(fonts);
    free(fonts);
}

struct pg_asset_loader* pg_fontset_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_fontset",
        .type_name_len = strlen("pg_fontset"),
        .parse = fontset_asset_parse,
        .unparse = fontset_asset_unparse,
        .load = fontset_asset_load,
        .unload = fontset_asset_unload,
    };
    return loader;
}






