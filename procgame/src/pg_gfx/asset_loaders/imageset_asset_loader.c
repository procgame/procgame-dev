#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"

/*  For fixing image data when it is in a texture that requires padding */
static void image_precalc_coords(struct pg_image* image)
{
    vec2 tex_sz = vec2(VEC_XY(image->tex->dimensions));
    vec2 img_size = vec2(VEC_XY(image->img_size));
    image->uv_size = vec2_div(img_size, tex_sz);
    /*  First do the grid cell dimensions   */
    if(!vec2_is_zero(image->grid_px)) {
        /*  Grid defined in pixels  */
        image->grid_uv = vec2_div(image->grid_px, tex_sz);
        image->grid_cells = vec2_div(img_size, image->grid_px);
    } else if(!vec2_is_zero(image->grid_uv)) {
        /*  Grid defined in UV coordinates  */
        image->grid_uv = vec2_mul(image->grid_uv, image->uv_size);
        image->grid_px = vec2_mul(img_size, image->grid_uv);
        image->grid_cells = vec2_div(vec2(1,1), image->grid_uv);
    } else if(!vec2_is_zero(image->grid_cells)) {
        /*  Grid defined by own dimensions  */
        image->grid_uv = vec2_div(vec2(1,1), image->grid_cells);
        image->grid_px = vec2_div(img_size, image->grid_cells);
    } else {
        /*  Grid undefined  */
        image->grid_uv = vec2(1,1);
        image->grid_px = img_size;
        image->grid_cells = vec2(1,1);
    }
    /*  Next calculate the coordinates in all the frames    */
    int i;
    struct pg_image_frame* f;
    ARR_FOREACH_PTR(image->frames, f, i) {
        f->uv_raw.layer = image->tex_layer;
        if(!vec2_is_zero(f->px[0]) || !vec2_is_zero(f->px[1])) {
            /*  Frame defined in pixels */
            f->uv_raw.uv[0] = vec2_div(f->px[0], tex_sz);
            f->uv_raw.uv[1] = vec2_div(f->px[1], tex_sz);
            f->grid[0] = vec2_div(f->px[0], image->grid_px);
            f->grid[1] = vec2_div(f->px[0], image->grid_px);
        } else if(!vec2_is_zero(f->uv_raw.uv[0]) || !vec2_is_zero(f->uv_raw.uv[1])) {
            /*  Frame defined as UV coordinates */
            f->uv_raw.uv[0] = vec2_mul(f->uv_raw.uv[0], image->uv_size);
            f->uv_raw.uv[1] = vec2_mul(f->uv_raw.uv[1], image->uv_size);
            f->px[0] = vec2_mul(f->uv_raw.uv[0], img_size);
            f->px[1] = vec2_mul(f->uv_raw.uv[1], img_size);
            f->grid[0] = vec2_mul(f->uv_raw.uv[0], image->grid_uv);
            f->grid[1] = vec2_mul(f->uv_raw.uv[1], image->grid_uv);
        } else if(!vec2_is_zero(f->grid[0]) || !vec2_is_zero(f->grid[1])) {
            /*  Frame defined in grid cells */
            f->uv_raw.uv[0] = vec2_mul(f->grid[0], image->grid_uv);
            f->uv_raw.uv[1] = vec2_mul(f->grid[1], image->grid_uv);
            f->px[0] = vec2_mul(f->grid[0], image->grid_px);
            f->px[1] = vec2_mul(f->grid[1], image->grid_px);
        } else {
            /*  Zero-size frame */
        }
    }
}

static void imageset_precalc_coords(struct pg_imageset* imageset)
{
    if(!imageset->tex) return;
    int i;
    struct pg_image* image;
    ARR_FOREACH_PTR(imageset->imgs, image, i) {
        image->tex = imageset->tex;
        image->tex_layer = i;
        image->frames.data[0].uv_raw.layer = i;
        image_precalc_coords(image);
    }
}


/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct imageset_asset_source {
    pg_asset_handle_t texture_asset;
    SARR_T(16, struct single_image_source {
        struct pg_filepath image_path;
        char name[PG_IMAGE_NAME_MAXLEN];
        vec2 grid_uv;
        ivec2 grid_px;
        ivec2 grid_cells;
        SARR_T(256, struct pg_image_frame) frames;
        SARR_T(256, struct pg_image_sequence) sequences;
    }) images;
};


static void* imageset_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* json)
{
    cJSON* images_json = cJSON_GetObjectItem(json, "images");
    if(!cJSON_IsArray(images_json)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "requires \"images\" member");
        return NULL;
    }

    struct imageset_asset_source* parsed_source = malloc(sizeof(*parsed_source));
    SARR_INIT(parsed_source->images);

    cJSON* image_json;
    cJSON_ArrayForEach(image_json, images_json) {
        cJSON* path_json, *name_json, *grid_uv_json, *grid_px_json, *grid_cells_json, *frames_json, *seqs_json;
        struct pg_json_layout layout = PG_JSON_LAYOUT(7,
            PG_JSON_LAYOUT_ITEM("path", cJSON_String, &path_json),
            PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
            PG_JSON_LAYOUT_ITEM("frames", cJSON_Array, &frames_json),
            PG_JSON_LAYOUT_ITEM("sequences", cJSON_Array, &seqs_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("grid_uv", cJSON_Array, &grid_uv_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("grid_px", cJSON_Array, &grid_px_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("grid_cells", cJSON_Array, &grid_cells_json),
        );
        if(!pg_load_json_layout(image_json, &layout)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
            return NULL;
        }
        
        struct single_image_source* parsed_img_source = ARR_NEW(parsed_source->images);
        *parsed_img_source = (struct single_image_source){};
        SARR_INIT(parsed_img_source->frames);
        SARR_INIT(parsed_img_source->sequences);
        if(grid_uv_json) parsed_img_source->grid_uv = json_read_vec2(grid_uv_json);
        if(grid_px_json) parsed_img_source->grid_px = json_read_ivec2(grid_px_json);
        if(grid_cells_json) parsed_img_source->grid_cells = json_read_ivec2(grid_cells_json);
        strncpy(parsed_img_source->image_path.path, path_json->valuestring, PG_FILE_PATH_MAXLEN);
        strncpy(parsed_img_source->name, name_json->valuestring, PG_IMAGE_NAME_MAXLEN);

        cJSON* frame_json;
        cJSON_ArrayForEach(frame_json, frames_json) {
            cJSON* name_json, *uv_json, *px_json, *grid_json;
            struct pg_json_layout frame_layout = PG_JSON_LAYOUT(4,
                PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
                PG_JSON_LAYOUT_OPTIONAL_ITEM("uv", cJSON_Array, &uv_json),
                PG_JSON_LAYOUT_OPTIONAL_ITEM("px", cJSON_Array, &px_json),
                PG_JSON_LAYOUT_OPTIONAL_ITEM("grid", cJSON_Array, &grid_json),
            );
            if(!pg_load_json_layout(frame_json, &frame_layout)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
                free(parsed_source);
                return NULL;
            }

            struct pg_image_frame* parsed_frame = ARR_NEW(parsed_img_source->frames);
            *parsed_frame = (struct pg_image_frame){};
            strncpy(parsed_frame->name, name_json->valuestring, PG_IMAGE_NAME_MAXLEN);
            if(uv_json) {
                if(!uv_json->child || !uv_json->child->next) {
                    pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid UV coordinates");
                    free(parsed_source);
                    return NULL;
                }
                parsed_frame->uv_raw.uv[0] = json_read_vec2(uv_json->child);
                parsed_frame->uv_raw.uv[1] = json_read_vec2(uv_json->child->next);
            } else if(px_json) {
                if(!px_json->child || !px_json->child->next) {
                    pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid pixel coordinates");
                    free(parsed_source);
                    return NULL;
                }
                parsed_frame->px[0] = json_read_vec2(px_json->child);
                parsed_frame->px[1] = json_read_vec2(px_json->child->next);
            } else if(grid_json) {
                if(!grid_json->child || !grid_json->child->next) {
                    pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid grid coordinates");
                    free(parsed_source);
                    return NULL;
                }
                parsed_frame->grid[0] = json_read_vec2(grid_json->child);
                parsed_frame->grid[1] = json_read_vec2(grid_json->child->next);
            } else {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid image frame");
                free(parsed_source);
                return NULL;
            }
        }

        cJSON* seq_json;
        cJSON_ArrayForEach(seq_json, seqs_json) {
            cJSON* name_json, *seq_frames_json;
            struct pg_json_layout seq_layout = PG_JSON_LAYOUT(2,
                PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
                PG_JSON_LAYOUT_ITEM("frames", cJSON_Array, &seq_frames_json),
            );
            if(!pg_load_json_layout(seq_json, &seq_layout)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid animation sequence");
                free(parsed_source);
                return NULL;
            }

            struct pg_image_sequence* parsed_seq = ARR_NEW(parsed_img_source->sequences);
            *parsed_seq = (struct pg_image_sequence){};
            SARR_INIT(parsed_seq->frames);
            strncpy(parsed_seq->name, name_json->valuestring, PG_IMAGE_NAME_MAXLEN);

            float duration = 0;
            cJSON* seq_frame_json;
            cJSON_ArrayForEach(seq_frame_json, seq_frames_json) {
                cJSON* frame_name_json, *duration_json;
                struct pg_json_layout seq_frame_layout = PG_JSON_LAYOUT(2,
                    PG_JSON_LAYOUT_ITEM("frame", cJSON_String, &frame_name_json),
                    PG_JSON_LAYOUT_ITEM("duration", cJSON_Number, &duration_json),
                );
                if(!pg_load_json_layout(seq_frame_json, &seq_frame_layout)) {
                    pg_asset_log(PG_LOG_ERROR, asset_handle, "contains invalid animation sequence");
                    free(parsed_source);
                    return NULL;
                }

                struct pg_image_sequence_frame* parsed_seq_frame = ARR_NEW(parsed_seq->frames);
                parsed_seq_frame->start = duration;
                parsed_seq_frame->duration = duration_json->valuedouble;
                duration += parsed_seq_frame->duration;

                int i;
                for(i = 0; i < parsed_img_source->frames.len; ++i) {
                    if(strcmp(parsed_img_source->frames.data[i].name, frame_name_json->valuestring) == 0)
                        break;
                }
                if(i == parsed_img_source->frames.len) {
                    pg_asset_log(PG_LOG_ERROR, asset_handle, "contains sequence with undefined frame");
                    free(parsed_source);
                    return NULL;
                }
                parsed_seq_frame->frame = i;
            }

            parsed_seq->full_duration = duration;
        }
    }

    /*  Declare child texture asset */
    parsed_source->texture_asset = pg_asset_declare_child(asset_handle, "pg_gpu_texture", "texture");

    return parsed_source;
}



static void imageset_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct imageset_asset_source* image_source = (struct imageset_asset_source*)parsed_source;
    free(image_source);
}



static void* imageset_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    int n_search_paths = 0;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_handle.mgr, &n_search_paths);

    struct imageset_asset_source* imageset_source = (struct imageset_asset_source*)parsed_source;
    const int n_images = imageset_source->images.len;
    
    /*  Load the images into texture layers */
    struct pg_file tex_files[16] = {};
    struct pg_file* tex_file_ptrs[16] = {};
    for(int i = 0; i < n_images; ++i) {
        tex_file_ptrs[i] = &tex_files[i];
        struct single_image_source* image_source = &imageset_source->images.data[i];
        if(!pg_file_open_search_paths(&tex_files[i], search_paths, n_search_paths,
                image_source->image_path.path, "rb")) {
            return NULL;
        }
    }
    ivec2 layer_sizes[16] = {};
    struct pg_texture* new_tex = malloc(sizeof(*new_tex));
    if(!pg_texture_from_files_get_info(new_tex, tex_file_ptrs, n_images, layer_sizes)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to read image data from loaded files");
        free(new_tex);
        return NULL;
    }
    for(int i = 0; i < n_images; ++i) pg_file_close(tex_file_ptrs[i]);
    vec2 full_size = vec2(VEC_XY(new_tex->dimensions));

    /*  Initialize a new imageset object and give it ownership of the texture   */
    struct pg_imageset* new_imageset = malloc(sizeof(*new_imageset));
    pg_imageset_init(new_imageset, new_tex, false);

    /*  For each parsed image source    */
    vec2 layer_fracs[16] = {};
    for(int i = 0; i < n_images; ++i) {
        layer_fracs[i] = vec2_div(vec2(VEC_XY(layer_sizes[i])), full_size);
        struct single_image_source* image_source = &imageset_source->images.data[i];
        struct pg_image* new_image = ARR_NEW(new_imageset->imgs);
        pg_image_init(new_image, image_source->image_path.path, ivec2(VEC_XY(layer_fracs[i])), new_tex, false, i);
        new_image->grid_px = vec2(VEC_XY(image_source->grid_px));
        new_image->grid_uv = image_source->grid_uv;
        new_image->grid_cells = vec2(VEC_XY(image_source->grid_cells));
        int j;
        struct pg_image_frame* frame;
        ARR_FOREACH_PTR(image_source->frames, frame, j) {
            *ARR_NEW(new_image->frames) = *frame;
            HTABLE_SET(new_image->frames_table, frame->name, j + 1);
        }
        struct pg_image_sequence* seq;
        ARR_FOREACH_PTR(image_source->sequences, seq, j) {
            *ARR_NEW(new_image->seqs) = *seq;
            HTABLE_SET(new_image->seqs_table, seq->name, j);
        }
        HTABLE_SET(new_imageset->imgs_table, image_source->name, i);
    }

    imageset_precalc_coords(new_imageset);

    /*  Upload the rasterized image textures to the GPU and create the child asset to hold it    */
    pg_gpu_texture_t* gpu_tex = pg_imageset_get_gpu_texture(new_imageset);
    pg_asset_handle_t gpu_tex_asset = imageset_source->texture_asset;
    pg_asset_from_data(asset_handle.mgr, "pg_gpu_texture",
            pg_asset_get_name(gpu_tex_asset), gpu_tex, false);

    return new_imageset;
}



static void imageset_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    struct pg_imageset* images = (struct pg_imageset*)loaded_data;
    pg_imageset_deinit(images);
    free(images);
}



struct pg_asset_loader* pg_imageset_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_imageset",
        .type_name_len = strlen("pg_imageset"),
        .parse = imageset_asset_parse,
        .unparse = imageset_asset_unparse,
        .load = imageset_asset_load,
        .unload = imageset_asset_unload,
    };
    return loader;
}







