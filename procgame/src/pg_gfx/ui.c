#include "procgame.h"
#include "ui_internal.h"

enum pg_ui_draw_order pg_ui_draw_order_from_string(const char* str)
{
    if(strcmp(str, "none") == 0) return PG_UI_IMAGE_ONLY;
    else if(strcmp(str, "text_only") == 0) return PG_UI_TEXT_ONLY;
    else if(strcmp(str, "image_only") == 0) return PG_UI_IMAGE_ONLY;
    else if(strcmp(str, "text_then_image") == 0) return PG_UI_TEXT_THEN_IMAGE;
    else if(strcmp(str, "image_then_text") == 0) return PG_UI_IMAGE_THEN_TEXT;
    return PG_UI_DRAW_NONE;
}

enum pg_ui_action_item pg_ui_action_item_from_string(const char* str)
{
    if(strcmp(str, "none") == 0) return PG_UI_ACTION_NONE;
    else if(strcmp(str, "independent") == 0) return PG_UI_ACTION_INDEPENDENT;
    else if(strcmp(str, "image") == 0) return PG_UI_ACTION_IMAGE;
    else if(strcmp(str, "text") == 0) return PG_UI_ACTION_TEXT;
    return PG_UI_ACTION_NONE;
}

/************************************************/
/*  Memory management                           */
/************************************************/

pg_ui_t ui_alloc(struct pg_ui_context* ctx, int n, struct pg_ui_content** ptr)
{
    int run = 1;
    int i = 0;
    int alloc = 0;
    for(i = 0; i < ctx->content_pool.cap; ++i) {
        if(ctx->content_pool.data[i].type != PG_UI_FREE) continue;
        run = 1;
        while(run < n && run + i < ctx->content_pool.cap
        && ctx->content_pool.data[run + i].type == PG_UI_FREE) ++run;
        if(run < n) i += run - 1;
        else break;
    }
    alloc = i;
    if(alloc + n >= ctx->content_pool.cap)
        ARR_RESERVE(ctx->content_pool, alloc + n);
    for(i = 0; i < n; ++i) {
        struct pg_ui_content* new_cont = &ctx->content_pool.data[alloc + i];
        *new_cont = (struct pg_ui_content){ .type = PG_UI_UNINITIALIZED,
            .self = alloc + n };
        new_cont->debug_color = vec4(RANDF(1.0), RANDF(1.0), RANDF(1.0), 0.5);
        HTABLE_INIT(new_cont->vars, 4);
    }
    if(ptr) *ptr = ctx->content_pool.data + alloc;
    return (pg_ui_t)alloc;
}

void ui_free(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    if(ui_ref < 0 || ui_ref >= ctx->content_pool.cap) return;
    struct pg_ui_content* ui_cont = &ctx->content_pool.data[ui_ref];
    if(ui_cont->type == PG_UI_GROUP) {
        int i;
        pg_ui_t child;
        ARR_FOREACH(ui_cont->grp.children_arr, child, i) {
            ui_free(ctx, child);
        }
        ARR_DEINIT(ui_cont->grp.children_arr);
        HTABLE_DEINIT(ui_cont->grp.children);
    } else if(ui_cont->type == PG_UI_ELEMENT) {
        pg_text_form_deinit(&ui_cont->elem.text_form);
    }
    HTABLE_DEINIT(ui_cont->vars);
    ctx->content_pool.data[ui_ref] = (struct pg_ui_content){};
}

struct pg_ui_content* ui_dereference(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    if(ui_ref < 0 || ui_ref >= ctx->content_pool.cap) return NULL;
    return &ctx->content_pool.data[ui_ref];
}










/************************************************/
/*  Building UI hierarchy                       */
/************************************************/

#define INITPROP(PROP, VALUE) \
    [PROP] = { .base_value = (VALUE), .current_value = (VALUE) }


void ui_group_init(struct pg_ui_group* grp, struct pg_ui_properties* props)
{
    *grp = (struct pg_ui_group){
        .properties = {
            INITPROP(PG_UI_POS,             vec4(VEC_XYZ(props->pos))),
            INITPROP(PG_UI_SCALE,           vec4(VEC_XYZ(props->scale))),
            INITPROP(PG_UI_ROTATION,        vec4(props->rotation)),
            INITPROP(PG_UI_CLIP_POS,        vec4(VEC_XYZ(props->clip_pos))),
            INITPROP(PG_UI_CLIP_SCALE,      vec4(VEC_XYZ(props->clip_scale))),
        },
        .enable_clip = props->enable_clip,
        .enable_3d = props->enable_3d,
    };
    HTABLE_INIT(grp->children, 8);
    ARR_INIT(grp->children_arr);
}

void ui_elem_init(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct pg_ui_properties* props)
{
    cont->elem = (struct pg_ui_element) {
        .properties = {
            INITPROP(PG_UI_POS,             vec4(VEC_XYZ(props->pos))),
            INITPROP(PG_UI_SCALE,           vec4(VEC_XYZ(props->scale))),
            INITPROP(PG_UI_ROTATION,        vec4(props->rotation)),
            INITPROP(PG_UI_IMAGE_POS,       vec4(VEC_XYZ(props->image_pos))),
            INITPROP(PG_UI_IMAGE_SCALE,     vec4(VEC_XYZ(props->image_scale))),
            INITPROP(PG_UI_IMAGE_ROTATION,  vec4(props->image_rotation)),
            INITPROP(PG_UI_IMAGE_COLOR_MUL, props->image_color_mul),
            INITPROP(PG_UI_IMAGE_COLOR_ADD, props->image_color_add),
            INITPROP(PG_UI_TEXT_POS,        vec4(VEC_XYZ(props->text_pos))),
            INITPROP(PG_UI_TEXT_SCALE,      vec4(VEC_XYZ(props->text_scale))),
            INITPROP(PG_UI_TEXT_ROTATION,   vec4(props->text_rotation)),
            INITPROP(PG_UI_TEXT_COLOR,      props->text_color),
            INITPROP(PG_UI_ACTION_POS,      vec4(VEC_XYZ(props->action_pos))),
            INITPROP(PG_UI_ACTION_SCALE,    vec4(VEC_XYZ(props->action_scale))),
        },
        .action_item = props->action_item,
        .draw_order = props->draw_order,
        .callbacks = {
            [PG_UI_CLICK] = props->cb_click,
            [PG_UI_HOLD] = props->cb_hold,
            [PG_UI_RELEASE] = props->cb_release,
            [PG_UI_SCROLL] = props->cb_scroll,
            [PG_UI_ENTER] = props->cb_enter,
            [PG_UI_LEAVE] = props->cb_leave,
            [PG_UI_UPDATE] = props->cb_update, },
    };
    int i;
    for(i = 0; i < PG_UI_ELEM_PROPERTIES; ++i) {
        if(&props->anim[i]) pg_animator_set(&cont->elem.properties[i], props->anim[i]);
    }
    cont->layer = props->layer;
    cont->type = PG_UI_ELEMENT;
    cont->disabled = !props->enabled;
    /*  Image stuff */
    if(props->image_sequence && props->image_sequence[0]) {
        pg_imageset_make_stepper(ctx->images, &cont->elem.image_anim,
                                        props->image_name, props->image_sequence);
    } else if(props->image_frame && props->image_frame[0]) {
        pg_imageset_make_stepper_single(ctx->images, &cont->elem.image_anim,
                                        props->image_name, props->image_frame);
    } else {
        pg_imageset_make_stepper_single(ctx->images, &cont->elem.image_anim, NULL, NULL);
    }
    cont->elem.image_anim_start = ctx->time;
    /*  Text stuff  */
    cont->elem.text_anchor = props->text_anchor;
    if(props->text_formatter.fonts) {
        cont->elem.text_formatter = props->text_formatter;
    } else {
        cont->elem.text_formatter = PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .size = props->text_size);
    }
    if(props->text_w) {
        int len;
        if(props->text_len <= 0) len = wcslen(props->text_w);
        else len = wcsnlen(props->text_w, props->text_len);
        pg_text_form_init_alloc(&cont->elem.text_form, len);
        pg_text_format_w(&cont->elem.text_form, &cont->elem.text_formatter,
                       props->text_w, len);
    } else if(props->text) {
        int len;
        if(props->text_len <= 0) len = strlen(props->text);
        else len = strnlen(props->text, props->text_len);
        pg_text_form_init_alloc(&cont->elem.text_form, len);
        pg_text_format(&cont->elem.text_form, &cont->elem.text_formatter,
                         props->text, len);
    } else {
        pg_text_form_init_alloc(&cont->elem.text_form, 0);
    }
}

pg_ui_t pg_ui_context_root(struct pg_ui_context* ctx)
{
    return ctx->root;
}

pg_ui_t pg_ui_add_group(struct pg_ui_context* ctx, pg_ui_t parent,
                                  const char* name, struct pg_ui_properties* props)
{
    struct pg_ui_content* p_cont = ui_dereference(ctx, parent);
    if(!p_cont || p_cont->type != PG_UI_GROUP) {
        pg_log(PG_LOG_ERROR, "Can't create UI group %s, invalid parent", name);
        return -1;
    }
    struct pg_ui_content* new_cont;
    pg_ui_t new_ref = ui_alloc(ctx, 1, &new_cont);
    p_cont = ui_dereference(ctx, parent);
    ui_group_init(&new_cont->grp, props);
    strncpy(new_cont->name, name, PG_UI_NAME_MAXLEN);
    new_cont->layer = props->layer;
    new_cont->type = PG_UI_GROUP;
    new_cont->parent = parent;
    new_cont->disabled = !props->enabled;
    new_cont->fix_aspect = props->fix_aspect;
    HTABLE_SET(p_cont->grp.children, name, new_ref);
    ARR_PUSH(p_cont->grp.children_arr, new_ref);
    p_cont->grp.children_added += 1;
    return new_ref;
}

pg_ui_t pg_ui_add_element(struct pg_ui_context* ctx, pg_ui_t parent,
                                 const char* name, struct pg_ui_properties* props)
{
    struct pg_ui_content* p_cont = ui_dereference(ctx, parent);
    if(!p_cont || p_cont->type != PG_UI_GROUP) {
        pg_log(PG_LOG_ERROR, "Can't create UI element %s, invalid parent", name);
        return -1;
    }
    struct pg_ui_content* new_cont;
    pg_ui_t new_ref = ui_alloc(ctx, 1, &new_cont);
    p_cont = ui_dereference(ctx, parent);
    ui_elem_init(ctx, new_cont, props);
    strncpy(new_cont->name, name, PG_UI_NAME_MAXLEN);
    new_cont->parent = parent;
    new_cont->fix_aspect = props->fix_aspect;
    HTABLE_SET(p_cont->grp.children, name, new_ref);
    ARR_PUSH(p_cont->grp.children_arr, new_ref);
    p_cont->grp.children_added += 1;
    return new_ref;
}

void pg_ui_delete(struct pg_ui_context* ctx, pg_ui_t del)
{
    struct pg_ui_content* del_cont = ui_dereference(ctx, del);
    if(!del_cont) return;
    struct pg_ui_content* p_cont = ui_dereference(ctx, del_cont->parent);
    if(p_cont) {
        int i;
        pg_ui_t arr_child;
        ARR_FOREACH(p_cont->grp.children_arr, arr_child, i) {
            if(arr_child == del) {
                ARR_SPLICE(p_cont->grp.children_arr, i, 1);
                break;
            }
        }
        HTABLE_UNSET(p_cont->grp.children, del_cont->name);
    }
    del_cont->marked_to_free = 1;
}




/************************************************/
/*  Transformations                             */
/************************************************/

void ui_group_get_transforms(struct pg_ui_context* ctx, const struct pg_ui_content* cont, const struct ui_space* space,
        struct pg_gfx_transform* tx_out, struct pg_gfx_transform* tx_out_clip,
        struct ui_space* grp_space_out)
{
    const struct pg_ui_group* grp = &cont->grp;
    /*  Calculate the main element transform    */
    vec3 a_pos = vec3(VEC_XYZ(grp->properties[PG_UI_POS].current_value));
    vec3 a_scale = vec3(VEC_XY(grp->properties[PG_UI_SCALE].current_value),1);
    const float a_rot = grp->properties[PG_UI_ROTATION].current_value.x;

    /*  Calculate the clipping bound transform  */
    vec3 a_clip_pos = vec3(VEC_XYZ(grp->properties[PG_UI_CLIP_POS].current_value));
    vec3 a_clip_scale = vec3(VEC_XY(grp->properties[PG_UI_CLIP_SCALE].current_value),1);

    /*  Very fragile aspect ratio fixing logic!!!   */
    *grp_space_out = *space;
    if(grp->enable_3d) {
        grp_space_out->origin = ctx->origin_3d;
        grp_space_out->enabled_3d = true;
    }

    if(cont->fix_aspect) {
        a_pos = vec3_mul(a_pos, space->aspect_fix);
        grp_space_out->aspect_fix = vec3_recip(vec3(1,1,1));
    } else {
        a_scale = vec3_div(a_scale, space->aspect_fix);
        grp_space_out->aspect_fix = vec3_recip(vec3(a_scale.x / a_scale.y, 1, 1));
    }

    a_clip_pos = vec3_mul(a_clip_pos, grp_space_out->aspect_fix);
    a_clip_scale = vec3_mul(a_clip_scale, grp_space_out->aspect_fix);


    /*  Create the final transforms */
    struct pg_gfx_transform tx_main;
    pg_gfx_create_transform(&tx_main,
            vec3_mul(a_pos, grp_space_out->aspect_fix),
            vec3_mul(a_scale, grp_space_out->aspect_fix),
            quat_rotation(vec3_Z(), a_rot));
    pg_gfx_apply_transform(&tx_main, &tx_main, &grp_space_out->origin);
    grp_space_out->origin = tx_main;

    struct pg_gfx_transform tx_clip;
    pg_gfx_create_transform(&tx_clip,
            vec3_mul(a_clip_pos, vec3(1,1,1)),
            vec3_mul(a_clip_scale, vec3(1,1,1)),
            quat_identity());
    pg_gfx_apply_transform(&tx_clip, &tx_clip, &tx_main);

    /*  Transform local transforms to output space  */
    if(tx_out) *tx_out = tx_main;
    if(tx_out_clip) *tx_out_clip = tx_clip;
}

void ui_element_get_transforms(struct pg_ui_element* elem, const struct ui_space* space,
        struct pg_gfx_transform* tx_out,
        struct pg_gfx_transform* tx_out_text,
        struct pg_gfx_transform* tx_out_image,
        struct pg_gfx_transform* tx_out_action)
{
    /*  Calculate the main element transform    */
    vec3 a_pos = vec3(VEC_XYZ(elem->properties[PG_UI_POS].current_value));
    vec3 a_scale = vec3(VEC_XY(elem->properties[PG_UI_SCALE].current_value),1);
    const float a_rot = elem->properties[PG_UI_ROTATION].current_value.x;
    a_pos = vec3_mul(space->aspect_fix, a_pos);
    a_scale = vec3_mul(space->aspect_fix, a_scale);
    struct pg_gfx_transform tx_main;
    pg_gfx_create_transform(&tx_main, a_pos, a_scale, quat_rotation(vec3_Z(), a_rot));
    pg_gfx_apply_transform(&tx_main, &tx_main, &space->origin);

    /*  Calculate the text transform    */
    const vec3 a_text_pos =  vec3(VEC_XYZ(elem->properties[PG_UI_TEXT_POS].current_value));
    const vec3 a_text_scale = vec3(VEC_XY(elem->properties[PG_UI_TEXT_SCALE].current_value),1);
    const float a_text_rot = elem->properties[PG_UI_TEXT_ROTATION].current_value.x;
    struct pg_gfx_transform tx_text;
    pg_gfx_create_transform(&tx_text, a_text_pos, a_text_scale, quat_rotation(vec3_Z(), a_text_rot));
    pg_gfx_apply_transform(&tx_text, &tx_text, &tx_main);

    /*  Calculate the image transform   */
    const vec3 a_image_pos =  vec3(VEC_XYZ(elem->properties[PG_UI_IMAGE_POS].current_value));
    const vec3 a_image_scale = vec3(VEC_XY(elem->properties[PG_UI_IMAGE_SCALE].current_value),1);
    const float a_image_rot = elem->properties[PG_UI_IMAGE_ROTATION].current_value.x;
    struct pg_gfx_transform tx_image;
    pg_gfx_create_transform(&tx_image, a_image_pos, a_image_scale, quat_rotation(vec3_Z(), a_image_rot));
    pg_gfx_apply_transform(&tx_image, &tx_image, &tx_main);

    /*  Calculate the action area transform */
    struct pg_gfx_transform tx_action;
    if(elem->action_item == PG_UI_ACTION_INDEPENDENT) {
        const vec3 a_action_pos =  vec3(VEC_XYZ(elem->properties[PG_UI_ACTION_POS].current_value));
        const vec3 a_action_scale = vec3(VEC_XY(elem->properties[PG_UI_ACTION_SCALE].current_value),1);
        pg_gfx_create_transform(&tx_action, a_action_pos, a_action_scale, quat_rotation(vec3_Z(), 0));
        pg_gfx_apply_transform(&tx_action, &tx_action, &tx_main);
    } else if(elem->action_item == PG_UI_ACTION_TEXT) {
        vec2 area_size = vec2_sub(elem->text_form.area[1], elem->text_form.area[0]);
        vec2 area_pos = vec2_add(elem->text_form.area[0], vec2_mul(area_size, vec2(0.5, 0.5)));
        area_pos.y *= -1;
        pg_gfx_create_transform(&tx_action, vec3(VEC_XY(area_pos),0), vec3(VEC_XY(area_size),1), quat_rotation(vec3_Z(), 0));
        pg_gfx_apply_transform(&tx_action, &tx_action, &tx_text);
    } else if(elem->action_item == PG_UI_ACTION_IMAGE) {
        tx_action = tx_image;
    } else if(elem->action_item == PG_UI_ACTION_NONE) {
        tx_action = (struct pg_gfx_transform){};
    }

    /*  Transform the local transforms to the output space  */
    if(tx_out) *tx_out = tx_main;
    if(tx_out_text) *tx_out_text = tx_text;
    if(tx_out_image) *tx_out_image = tx_image;
    if(tx_out_action) *tx_out_action = tx_action;
}

