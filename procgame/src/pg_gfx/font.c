#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"
#include "pg_gfx/pg_gfx.h"
#include <errno.h>

//#define PG_FONT_DEBUG
//#define PG_FONT_DEBUG_EVERY_GLYPH

#define GLYPH_MAPPING(ch, idx) ((struct pg_font_glyph_mapping){ ch, idx })

static void font_map_char(struct pg_font* font, wchar_t ch, int idx)
{
    unsigned bucket = ((unsigned)ch) & 0xFF;
    ARR_PUSH(font->char_buckets[bucket], GLYPH_MAPPING(ch, idx));
}

static void pg_font_alloc_texture(struct pg_font* font, vec2 tex_size)
{
    /*  Allocate a texture  */
    font->tex = malloc(sizeof(*font->tex));;
    pg_texture_init(font->tex, PG_UBVEC4, tex_size.x, tex_size.y, 1);
    font->own_tex = true;
}

static void pg_font_load_internal(struct pg_font* font, const char* filename,
        int pt_size, int tex_layer,
        const char** search_paths, int n_search_paths)
{
    int i, j;
    /*  Read the TTF file   */
    strncpy(font->ttf_filename, filename, 256);
    struct pg_file file;
    if(!pg_file_open_search_paths(&file, search_paths, n_search_paths, filename, "rb")) return;
    pg_file_read_full(&file);
    font->ttf = (uint8_t*)pg_file_take_content(&file);
    pg_file_close(&file);

    /*  Allocate glyph tables   */
    ARR_RESERVE_CLEAR(font->chars, 96);
    for(i = 0; i < 256; ++i) ARR_RESERVE_CLEAR(font->char_buckets[i], 4);

    /*  Get font info   */
    stbtt_InitFont(&font->ttf_info, font->ttf, 0);
    int ascent, descent, line_gap;
    stbtt_GetFontVMetrics(&font->ttf_info, &ascent, &descent, &line_gap);
    float fsize = (float)pt_size / (float)(ascent);
    font->line_gap = fsize * line_gap;
    font->ascent = fsize * ascent;
    font->descent = -fsize * descent;
    font->full_size = font->ascent + font->descent;
    font->line_move = (font->full_size + font->line_gap) * -1;
    font->size = pt_size;
    font->tex_layer = tex_layer;

    #ifdef PG_FONT_DEBUG
    pg_log(PG_LOG_DEBUG,
           "Font details: %s\n"
           "    Requested pixel scale: %d\n"
           "    Calculated font scale: %f\n"
           "    Ascent (base|calculated): %d | %f\n"
           "    Descent (base|calculated): %d | %f\n"
           "    Line gap (base|calculated): %d | %f\n"
           "    Line move: %f",
           filename, pt_size, fsize,
           ascent, font->ascent,
           descent, font->descent,
           line_gap, font->line_gap,
           font->line_move);
    #endif

    int tex_width = font->tex->dimensions.x;
    int tex_height = font->tex->dimensions.x;

    /*  Initialize a glyph packing context  */
    uint8_t* tex_pix = pg_texture_get_pixel_ptr(font->tex, 0, 0, tex_layer);
    stbtt_PackBegin(&font->pack_ctx, tex_pix, tex_width, tex_height, 0, 4, 0, 1, NULL);

    /*  Load all ASCII glyphs   */
    stbtt_packedchar bc[96];
    int success = stbtt_PackFontRange(&font->pack_ctx, font->ttf, 0, fsize, 32, 96, bc);
    if(!success) {
        pg_log(PG_LOG_ERROR, "Failed to pack the ASCII range in glyph texture.");
        return;
    }
    for(i = 0; i < tex_width; ++i) for(j = 0; j < tex_height; ++j) {
        uint8_t* px = (tex_pix) + (i + j * tex_width) * 4;
        px[1] = px[0];
        px[2] = px[0];
        px[3] = px[0];
    }

    /*  Make glyphs procgame-friendly, make all the char=>glyph mappings    */
    stbtt_aligned_quad q;
    float x_pos = 0, y_pos = 0;
    for(i = 0; i < 96; ++i) {
        stbtt_GetPackedQuad(bc, tex_width, tex_height, i, &x_pos, &y_pos, &q, 0);
        ARR_PUSH(font->chars, (struct pg_font_glyph){
            .uv = { vec2(q.s0, q.t0), vec2(q.s1, q.t1) },
            .scale = vec2(q.x1 - q.x0, q.y1 - q.y0),
            .offset = vec2(q.x0, q.y0),
            .xadvance = x_pos });
        x_pos = 0; y_pos = 0;
        font_map_char(font, (wchar_t)i + 32, i);
        #ifdef PG_FONT_DEBUG_EVERY_GLYPH
        pg_log(PG_LOG_DEBUG, "Debugging Font glyph: %lc, mapped to %d", (wchar_t)(i + 32), i);
        pg_log(PG_LOG_CONTD, "UV: (%f %f) (%f %f)\n", VEC_XY(font->chars.data[i].uv[0]), VEC_XY(font->chars.data[i].uv[1]));
        pg_log(PG_LOG_CONTD, "Scale: (%f %f)\n", VEC_XY(font->chars.data[i].scale));
        pg_log(PG_LOG_CONTD, "Offset: (%f %f)\n", VEC_XY(font->chars.data[i].offset));
        #endif
    }
    /*  Guarantee consistent text size by using the height of the M glyph
        as a standard measure.  */
    float mheight = font->chars.data['M'-32].scale.y;
    font->scale = 1.0f / mheight;
}

/*  Public interface    */
void pg_font_load(struct pg_font* font, const char* filename, int pt_size, vec2 tex_size)
{
    const char* search_paths[] = { "./" };
    *font = (struct pg_font){0};
    pg_font_alloc_texture(font, tex_size);
    pg_font_load_internal(font, filename, pt_size, 0, search_paths, 1);
}

void pg_font_load_search_paths(struct pg_font* font, const char* filename,
        int pt_size, vec2 tex_size,
        const char** search_paths, int n_search_paths)
{
    *font = (struct pg_font){0};
    pg_font_alloc_texture(font, tex_size);
    pg_font_load_internal(font, filename, pt_size, 0, search_paths, n_search_paths);
}

void pg_font_deinit(struct pg_font* font)
{
    stbtt_PackEnd(&font->pack_ctx);
    free(font->ttf);
    ARR_DEINIT(font->chars);
    int i;
    for(i = 0; i < 256; ++i) ARR_DEINIT(font->char_buckets[i]);
    if(font->own_tex) {
        pg_texture_deinit(font->tex);
        free(font->tex);
    }
    if(font->gpu_tex && font->own_gpu_tex) {
        pg_gpu_texture_deinit(font->gpu_tex);
    }
}

struct pg_texture* pg_font_get_texture(struct pg_font* font)
{
    return font->tex;
}

pg_gpu_texture_t* pg_font_get_gpu_texture(struct pg_font* font)
{
    if(!font->gpu_tex) {
        font->gpu_tex = pg_texture_upload(font->tex);
        font->own_gpu_tex = true;
    }
    return font->gpu_tex;
}


int pg_font_load_chars(struct pg_font* font, const wchar_t* str, int n)
{
    stbtt_aligned_quad q;
    stbtt_packedchar pc;
    int tex_width = font->tex->dimensions.x;
    int tex_height = font->tex->dimensions.y;
    float x_pos = 0, y_pos = 0;
    int i;
    for(i = 0; i < n && str[i]; ++i) {
        int idx = pg_font_get_char_index(font, str[i]);
        if(idx >= 0) continue;
        idx = font->chars.len;
        int success = stbtt_PackFontRange(&font->pack_ctx, font->ttf, 0,
                                          -font->size, str[i], 1, &pc);
        if(!success) return 0;
        stbtt_GetPackedQuad(&pc, tex_width, tex_height, 0, &x_pos, &y_pos, &q, 0);
        ARR_PUSH(font->chars, (struct pg_font_glyph){
            .uv = { vec2(q.s0, q.t0), vec2(q.s1, q.t1) },
            .scale = vec2(q.x1 - q.x0, q.y1 - q.y0),
            .offset = vec2(q.x0, q.y0),
            .xadvance = x_pos });
        x_pos = 0; y_pos = 0;
        font_map_char(font, str[i], idx);
        #ifdef PG_FONT_DEBUG
        pg_log(PG_LOG_DEBUG, "Debugging Font glyph: %lc, mapped to %d", (wchar_t)(str[i]), idx);
        pg_log(PG_LOG_CONTD, "UV: (%f %f) (%f %f)\n", VEC_XY(font->chars.data[idx].uv[0]),
                                                      VEC_XY(font->chars.data[idx].uv[1]));
        pg_log(PG_LOG_CONTD, "Scale: (%f %f)\n", VEC_XY(font->chars.data[idx].scale));
        pg_log(PG_LOG_CONTD, "Offset: (%f %f)\n", VEC_XY(font->chars.data[idx].offset));
        #endif
    }
    return 1;
}

int pg_font_get_char_index(struct pg_font* font, wchar_t ch)
{
    unsigned bucket = ((unsigned)ch) & 0xFF;
    struct pg_font_glyph_mapping* m;
    int i;
    ARR_FOREACH_PTR(font->char_buckets[bucket], m, i)
        if(m->ch == ch) return m->idx;
    return -1;
}

const struct pg_font_glyph* pg_font_get_glyph(struct pg_font* font, wchar_t ch)
{
    int idx = pg_font_get_char_index(font, ch);
    if(idx < 0) return &font->chars.data[0];
    else return &font->chars.data[idx];
}

struct pg_font_glyph* pg_font_get_glyph_nc(struct pg_font* font, wchar_t ch)
{
    int idx = pg_font_get_char_index(font, ch);
    if(idx < 0) return &font->chars.data[0];
    else return &font->chars.data[idx];
}

/************************/
/*  FONT SETS           */
/************************/

void pg_fontset_load(struct pg_fontset* fonts, const char** filenames,
                     int* pt_sizes, int n_fonts, vec2 tex_size)
{
    const char* search_paths[] = { "./" };
    pg_fontset_load_search_paths(fonts, filenames, pt_sizes, n_fonts, tex_size, search_paths, 1);
}

void pg_fontset_load_search_paths(struct pg_fontset* fonts,
        const char** filenames, int* sizes,
        int n_fonts, vec2 tex_size,
        const char** search_paths, int n_search_paths)
{
    *fonts = (struct pg_fontset){};
    ARR_INIT(fonts->fonts);
    /*  Allocate a texture  */
    fonts->tex = malloc(sizeof(*fonts->tex));
    pg_texture_init(fonts->tex, PG_UBVEC4, tex_size.x, tex_size.y, n_fonts);
    int i;
    for(i = 0; i < n_fonts; ++i) {
        struct pg_font new_font = {0};
        new_font.tex = fonts->tex;
        new_font.own_tex = false;
        new_font.tex_layer = i;
        pg_font_load_internal(&new_font, filenames[i], sizes[i], i, search_paths, n_search_paths);
        ARR_PUSH(fonts->fonts, new_font);
    }
}

void pg_fontset_deinit(struct pg_fontset* fonts)
{
    int i;
    struct pg_font* font;
    ARR_FOREACH_PTR(fonts->fonts, font, i) {
        pg_font_deinit(font);
    }
    ARR_DEINIT(fonts->fonts);
    pg_texture_deinit(fonts->tex);
    if(fonts->gpu_tex) pg_gpu_texture_deinit(fonts->gpu_tex);
    free(fonts->tex);
}

struct pg_texture* pg_fontset_texture(struct pg_fontset* fontset)
{
    return fontset->tex;
}

pg_gpu_texture_t* pg_fontset_get_gpu_texture(struct pg_fontset* fontset)
{
    if(!fontset->gpu_tex) {
        fontset->gpu_tex = pg_texture_upload(fontset->tex);
        int i;
        struct pg_font* font;
        ARR_FOREACH_PTR(fontset->fonts, font, i) {
            font->gpu_tex = fontset->gpu_tex;
        }
    }
    return fontset->gpu_tex;
}


struct pg_font* pg_fontset_font(struct pg_fontset* grp, int i)
{
    if(i < 0 || i >= grp->fonts.len) return &grp->fonts.data[0];
    return &grp->fonts.data[i];
}
