#include "procgame.h"
#include "ui_internal.h"

/********************************/
/*  Update                      */
/********************************/

/*  Stack data  */
struct ui_frame {
    vec3 mouse;
    int scroll;
    uint32_t input;
    bool ray_clipped;
    int mouse_clipped;
    int mouse_hover_consumed;
    int mouse_input_consumed;
    int mouse_scroll_consumed;
    pg_ui_t current;
    pg_ui_t parent;
    struct ui_space space;
};

static void group_step(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct ui_frame* frame);
static void element_step(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct ui_frame* frame);
static void content_step(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct ui_frame* frame);

void pg_ui_context_step(struct pg_ui_context* ctx)
{
    struct pg_gfx_transform tx_base = {
        .tx = mat4_identity(),
        .tx_inverse = mat4_identity(),
        .pos = vec3(0,0,0),
        .scale = vec3(1,1,1),
        .rot = quat_identity(),
    };

    /*  Handle mouse input  */
    struct pg_input_wrapper* mouse = pg_input_mouse();
    vec2 mouse_pos = pg_input_get_analog(mouse, 0);
    ctx->mouse_pos = vec3_mul(vec3(VEC_XY(mouse_pos)), vec3(ctx->ar, 1, 1));
    ctx->mouse_ctrl = pg_input_get_boolean(mouse, PG_INPUT_MOUSE_LEFT);
    if(pg_input_get_boolean(mouse, PG_INPUT_MOUSEWHEEL_UP) & PG_INPUT_HIT) ctx->mouse_scroll = 1;
    else if(pg_input_get_boolean(mouse, PG_INPUT_MOUSEWHEEL_DOWN) & PG_INPUT_HIT) ctx->mouse_scroll = -1;
    else ctx->mouse_scroll = 0;

    struct ui_frame root_frame = {
        .mouse = ctx->mouse_pos,
        .scroll = ctx->mouse_scroll,
        .input = ctx->mouse_ctrl,
        .current = ctx->root,
        .space = {
            .aspect_fix = vec3(ctx->ar,1,1),
            .origin = tx_base,
        },
    };
    content_step(ctx, ui_dereference(ctx, ctx->root), &root_frame);
}



static void content_step(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct ui_frame* frame)
{
    if(cont->disabled) return;
    if(cont->type == PG_UI_GROUP) group_step(ctx, cont, frame);
    else if(cont->type == PG_UI_ELEMENT) element_step(ctx, cont, frame);
}

static void group_step(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                         struct ui_frame* frame)
{
    struct pg_ui_group* grp = &cont->grp;
    struct ui_frame in_frame = *frame;
    struct pg_gfx_transform grp_tx, grp_tx_clip;
    ui_group_get_transforms(ctx, cont, &frame->space, &grp_tx, &grp_tx_clip, &in_frame.space);

    /*  Get the mouse position relative to the element  */
    if(grp->enable_clip) {
        vec4 mouse_pos4 = vec4(VEC_XYZ(ctx->mouse_pos), 1);
        mouse_pos4 = mat4_mul_vec4(grp_tx_clip.tx_inverse, mouse_pos4);
        vec3 mouse_clip = vec3(VEC_XYZ(mouse_pos4));
        if(!vec3_cmp_lt(vec3_abs(mouse_clip), vec3(0.5,0.5,0.5))) {
            in_frame.mouse_clipped = true;
        }
    }

    if(in_frame.space.enabled_3d && !in_frame.ray_clipped) {
        /*  Get the 3D raycast relative to the element  */
        vec4 cursor_pos4 = vec4(VEC_XYZ(ctx->cursor_3d_pos), 1);
        vec4 cursor_dir4 = vec4(VEC_XYZ(ctx->cursor_3d_dir), 0);
        cursor_pos4 = mat4_mul_vec4(grp_tx_clip.tx_inverse, cursor_pos4);
        cursor_dir4 = mat4_mul_vec4(grp_tx_clip.tx_inverse, cursor_dir4);
        cont->cursor_3d_pos = vec3(VEC_XYZ(cursor_pos4));
        cont->cursor_3d_dir = vec3_norm(vec3(VEC_XYZ(cursor_dir4)));
        plane z_plane = PLANE(vec3(0), vec3(0,0,1));
        ray3D cursor_ray = RAY3D(cont->cursor_3d_pos, cont->cursor_3d_dir);
        float dist = raycast_plane(&cursor_ray, &z_plane);
        if(dist > 0) {
            vec3 pt = vec3_add(cont->cursor_3d_pos, vec3_scale(cont->cursor_3d_dir, -dist));
            if(!vec3_cmp_lt(vec3_abs(pt), vec3(0.5,0.5,0.5))) {
                in_frame.ray_clipped = true;
            }
        }
    }

    /*  Create a temporary array for the children id's, in case this group pointer
        invalidated while we're doing this   */
    int num_children = grp->children_arr.len;
    pg_ui_t tmp_children[num_children];
    for(int i = 0; i < num_children; ++i) tmp_children[i] = grp->children_arr.data[i];

    /*  Iterate through all the children and recurse    */
    in_frame.parent = in_frame.current;
    struct pg_ui_content* iter_cont;
    for(int i = num_children - 1; i >= 0; --i) {
        iter_cont = ui_dereference(ctx, tmp_children[i]);
        if(!iter_cont) continue;

        /*  Free children that are marked to be freed   */
        if(iter_cont->marked_to_free) {
            ui_free(ctx, tmp_children[i]);
            continue;
        }

        /*  Recurse     */
        in_frame.current = tmp_children[i];
        content_step(ctx, iter_cont, &in_frame);
    }

    /*  Propagate input consumption back upward */
    frame->mouse_input_consumed = in_frame.mouse_input_consumed;
    frame->mouse_hover_consumed = in_frame.mouse_hover_consumed;
    frame->mouse_scroll_consumed = in_frame.mouse_scroll_consumed;
}


static void element_step(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct ui_frame* frame)
{
    struct pg_ui_element* elem = &cont->elem;
    struct ui_frame in_frame = *frame;
    struct pg_gfx_transform tx_main, tx_text, tx_image, tx_action;
    ui_element_get_transforms(elem, &in_frame.space, &tx_main, &tx_text, &tx_image, &tx_action);

    bool mouse_over = false;
    /*  Check if the pointer is over this element - in 2D by checking if the mouse is over it,
        or in 3D by checking if the 3D cursor is pointing at it */
    if(!frame->space.enabled_3d) {
        /*  Transform the mouse position into the element's local coordinate space,
            and check if it is within the element boundaries    */
        vec4 mouse_pos4 = vec4(VEC_XYZ(ctx->mouse_pos), 1);
        mouse_pos4 = mat4_mul_vec4(tx_action.tx_inverse, mouse_pos4);
        cont->mouse_pos = vec3(VEC_XYZ(mouse_pos4));
        if(!in_frame.mouse_clipped && vec3_cmp_lt(vec3_abs(cont->mouse_pos), vec3(0.5,0.5,0.5))) {
            mouse_over = true;
        }
    } else if(!in_frame.ray_clipped) {
        /*  Transform the 3D cursor into the element's local coordinate space,
            and check if its ray intersects the element boundaries  */
        vec4 cursor_pos4 = vec4(VEC_XYZ(ctx->cursor_3d_pos), 1);
        vec4 cursor_dir4 = vec4(VEC_XYZ(ctx->cursor_3d_dir), 0);
        cursor_pos4 = mat4_mul_vec4(tx_action.tx_inverse, cursor_pos4);
        cursor_dir4 = mat4_mul_vec4(tx_action.tx_inverse, cursor_dir4);
        cont->cursor_3d_pos = vec3(VEC_XYZ(cursor_pos4));
        cont->cursor_3d_dir = vec3_norm(vec3(VEC_XYZ(cursor_dir4)));
        plane z_plane = PLANE(vec3(0), vec3(0,0,1));
        ray3D cursor_ray = RAY3D(cont->cursor_3d_pos, cont->cursor_3d_dir);
        float dist = raycast_plane(&cursor_ray, &z_plane);
        /*  Check if the ray intersects the element */
        if(dist > 0) {
            vec3 pt = vec3_add(cont->cursor_3d_pos, vec3_scale(cont->cursor_3d_dir, -dist));
            cont->cursor_3d_hit_pos = pt;
            cont->cursor_3d_hit = vec3_cmp_lt(vec3_abs(pt), vec3(0.5,0.5,0.5));
        } else {
            cont->cursor_3d_hit_pos = vec3(0,0,0);
            cont->cursor_3d_hit = false;
        }
        mouse_over = cont->cursor_3d_hit;
    }

    const bool was_mouse_over = elem->mouse_over;
    elem->mouse_over = mouse_over;

    /*  Get a local copy of element's callback array, and NULL the elem
        pointer, since any of the callbacks may invalidate the pointer  */
    pg_ui_callback_t cb[PG_UI_ELEM_CALLBACKS];
    for(int i = 0; i < PG_UI_ELEM_CALLBACKS; ++i) cb[i] = elem->callbacks[i];
    elem = NULL;
    cont = NULL;

    /************************************************/
    /*  CALLBACKS                                   */
    int consume_input = 0, consume_hover = 0, consume_scroll = 0;

    /*  Hover callbacks */
    if(mouse_over && !was_mouse_over && cb[PG_UI_ENTER]) {
        struct pg_ui_event event = { .hover = { PG_UI_HOVER_ENTER } };
        consume_hover = cb[PG_UI_ENTER](ctx, frame->current, &event);
    } else if(!mouse_over && was_mouse_over && cb[PG_UI_LEAVE]) {
        struct pg_ui_event event = { .hover = { PG_UI_HOVER_LEAVE } };
        consume_hover = cb[PG_UI_LEAVE](ctx, frame->current, &event);
    }

    /*  Click/release callbacks */
    if(mouse_over && !in_frame.mouse_input_consumed) {
        if(((in_frame.input & PG_INPUT_HIT)) && cb[PG_UI_CLICK]) {
            struct pg_ui_event event = { .mouse = { PG_UI_MOUSE_CLICK, PG_UI_MOUSE_LEFT } };
            consume_input = cb[PG_UI_CLICK](ctx, frame->current, &event);
        } else if(((in_frame.input & PG_INPUT_HELD)) && cb[PG_UI_HOLD]) {
            struct pg_ui_event event = { .mouse = { PG_UI_MOUSE_HOLD, PG_UI_MOUSE_LEFT } };
            consume_input = cb[PG_UI_HOLD](ctx, frame->current, &event);
        } else if(((in_frame.input & PG_INPUT_RELEASED)) && cb[PG_UI_RELEASE]) {
            struct pg_ui_event event = { .mouse = { PG_UI_MOUSE_RELEASE, PG_UI_MOUSE_LEFT } };
            consume_input = cb[PG_UI_RELEASE](ctx, frame->current, &event);
        }
    }

    /*  Scrolling callbacks */
    if(mouse_over && in_frame.scroll
    && !in_frame.mouse_scroll_consumed && cb[PG_UI_SCROLL]) {
        struct pg_ui_event event = { .mouse = {
            PG_UI_MOUSE_SCROLL, PG_UI_MOUSE_MIDDLE, in_frame.scroll } };
        consume_scroll = cb[PG_UI_SCROLL](ctx, frame->current, &event);
    }

    /*  Generic update callback     */
    if(cb[PG_UI_UPDATE]) {
        struct pg_ui_event event = {};
        cb[PG_UI_UPDATE](ctx, frame->current, &event);
    }

    /*  If this element's callbacks should consume the mouse input,
        bubble this information up through the parent's input frame */
    if(consume_input) frame->mouse_input_consumed = 1;
    if(consume_hover) frame->mouse_hover_consumed = 1;
    if(consume_scroll) frame->mouse_scroll_consumed = 1;

}

