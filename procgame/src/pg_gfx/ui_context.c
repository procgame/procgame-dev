#include "procgame.h"
#include "ui_internal.h"

/************************************************/
/* UI Context functions                         */
/************************************************/


void pg_ui_context_init(struct pg_ui_context* ctx, struct pg_imageset* images, struct pg_fontset* fonts)
{
    *ctx = (struct pg_ui_context){};
    /*  Drawing-related setup   */
    ctx->images = images;
    ctx->fonts = fonts;
    ctx->default_text_formatter = PG_TEXT_FORMATTER(fonts, .align = PG_TEXT_CENTER);
    pg_ui_context_set_origin_3D(ctx, vec3(0,0,0), vec3(1,1,1), quat_identity());
    pg_quadbatch_init(&ctx->quadbatch, 4096);
    ctx->resources = pg_quadbatch_create_gpu_resources(&ctx->quadbatch,
            pg_imageset_get_gpu_texture(images), pg_fontset_get_gpu_texture(fonts));

    /*  Memory-related setup    */
    ARR_INIT(ctx->content_pool);
    HTABLE_INIT(ctx->callback_table, 8);
    /*  Create the root UI node */
    struct pg_ui_content* root;
    ctx->root = ui_alloc(ctx, 1, &root);
    ui_group_init(&root->grp, PG_UI_PROPERTIES());
    strncpy(root->name, "UI", PG_UI_NAME_MAXLEN);
    root->parent = -1;
    root->type = PG_UI_GROUP;
}

void pg_ui_context_deinit(struct pg_ui_context* ctx)
{
    pg_quadbatch_deinit(&ctx->quadbatch);
    pg_gpu_resource_set_destroy(ctx->resources);
    int i;
    for(i = 0; i < ctx->content_pool.cap; ++i) ui_free(ctx, i);
    ARR_DEINIT(ctx->content_pool);
}

void pg_ui_context_register_callback(struct pg_ui_context* ctx, const char* name, pg_ui_callback_t cb)
{
    HTABLE_SET(ctx->callback_table, name, cb);
}

pg_ui_callback_t pg_ui_context_get_callback(struct pg_ui_context* ctx, const char* name)
{
    if(!name || !name[0]) return NULL;
    pg_ui_callback_t cb = NULL;
    HTABLE_GET_V(ctx->callback_table, name, cb);
    if(cb == NULL) {
        pg_log(PG_LOG_ERROR, "Attempting to get non-existent UI callback '%s'", name);
    }
    return cb;
}

void pg_ui_context_set_origin_3D(struct pg_ui_context* ctx, vec3 origin, vec3 scale, quat rot)
{
    pg_gfx_create_transform(&ctx->origin_3d, origin, scale, rot);
}

void pg_ui_context_set_cursor_3D(struct pg_ui_context* ctx, vec3 pt, vec3 dir)
{
    ctx->cursor_3d_pos = pt;
    /*  Why??????   */
    ctx->cursor_3d_dir = vec3_scale(vec3_norm(dir), -1);
}



void pg_ui_context_set_render_stage(struct pg_ui_context* ctx, pg_gpu_render_stage_t* stage)
{
    ctx->render_stage = stage;
}

void pg_ui_context_set_resolution(struct pg_ui_context* ctx, vec2 res)
{
    ctx->ar = res.x / res.y;
    ctx->screen_mat = mat4_ortho(-ctx->ar, ctx->ar, -1, 1, -1, 1);
    pg_data_t projview_data = pg_data_init(PG_MAT4, 1, &ctx->screen_mat);
    pg_gpu_render_stage_set_initial_uniform(ctx->render_stage, 1, &projview_data);
    ctx->render_mode = PG_UI_RENDER_2D;
}

void pg_ui_context_set_view_3D(struct pg_ui_context* ctx, mat4 projview)
{
    ctx->projview_3d = projview;
    ctx->ar = 1.0f;
    pg_data_t projview_data = pg_data_init(PG_MAT4, 1, &projview);
    pg_gpu_render_stage_set_initial_uniform(ctx->render_stage, 1, &projview_data);
    ctx->render_mode = PG_UI_RENDER_3D;
}

void pg_ui_context_set_view_VR(struct pg_ui_context* ctx, mat4 projview_left, mat4 projview_right)
{
    ctx->ar = 1.0f;
    ctx->projview_eye[0] = projview_left;
    ctx->projview_eye[1] = projview_right;
    pg_data_t projview_data_left = pg_data_init(PG_MAT4, 1, &projview_left);
    pg_data_t projview_data_right = pg_data_init(PG_MAT4, 1, &projview_right);
    pg_gpu_render_stage_set_targets_uniform(ctx->render_stage, PG_TARGET_FILTER_VR_LEFT, 1, &projview_data_left);
    pg_gpu_render_stage_set_targets_uniform(ctx->render_stage, PG_TARGET_FILTER_VR_RIGHT, 1, &projview_data_right);
    ctx->render_mode = PG_UI_RENDER_VR;
}

