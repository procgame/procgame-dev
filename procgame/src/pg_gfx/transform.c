#include "pg_math/linmath.h"
#include "pg_gfx/transform.h"

void pg_gfx_create_transform(struct pg_gfx_transform* tx_out, vec3 pos, vec3 scale, quat orientation)
{
    tx_out->pos = pos;
    tx_out->scale = scale;
    tx_out->rot = orientation;
    tx_out->tx = mat4_object_space(pos, scale, orientation);
    tx_out->tx_inverse = mat4_object_space_inverse(pos, scale, orientation);
}

void pg_gfx_apply_transform(struct pg_gfx_transform* tx_out, const struct pg_gfx_transform* tx_local, const struct pg_gfx_transform* tx_parent)
{
    *tx_out = pg_gfx_transform_apply(tx_local, tx_parent);
}

struct pg_gfx_transform pg_gfx_transform_apply(const struct pg_gfx_transform* tx_local, const struct pg_gfx_transform* tx_parent)
{
    vec3 pos = tx_local->pos;
    vec3 pos_scaled = vec3_mul(tx_parent->scale, pos);
    vec3 pos_scaled_rotated = quat_mul_vec3(tx_parent->rot, pos_scaled);
    return (struct pg_gfx_transform) {
        .pos = vec3_add(tx_parent->pos, pos_scaled_rotated),
        .scale = vec3_mul(tx_parent->scale, tx_local->scale),
        .rot = quat_mul(tx_local->rot, tx_parent->rot),
        .tx = mat4_mul(tx_parent->tx, tx_local->tx),
        .tx_inverse = mat4_mul(tx_local->tx_inverse, tx_parent->tx_inverse),
    };
}

struct pg_gfx_transform pg_gfx_transform(vec3 pos, vec3 scale, quat orientation)
{
    return (struct pg_gfx_transform){
        .pos = pos,
        .scale = scale,
        .rot = orientation,
        .tx = mat4_object_space(pos, scale, orientation),
        .tx_inverse = mat4_object_space_inverse(pos, scale, orientation),
    };
}

struct pg_gfx_transform pg_gfx_transform_from_mat4(mat4 mat)
{
    quat rot = quat_from_mat4(mat); // does it work
    vec3 pos = mat4_mul_vec3(mat, vec3(0,0,0), true);
    return pg_gfx_transform(pos, vec3(1,1,1), rot);
}

ray3D pg_gfx_transform_ray(const struct pg_gfx_transform* tx, vec3 local_dir)
{
    return RAY3D(tx->pos, mat4_mul_vec3(tx->tx, local_dir, false));
}
