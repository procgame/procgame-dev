#include "pg_core/pg_core.h"
#include "pg_gfx/pg_gfx.h"

PG_MEMPOOL_DEFINE(struct pg_gfx_render_object, pg_gfx_render_object, id);

void pg_gfx_render_manager_init(struct pg_gfx_render_manager* mgr)
{
    pg_gfx_render_object_pool_init(&mgr->object_pool);
    ARR_INIT(mgr->active_objects);
    ARR_INIT(mgr->processors);
}

void pg_gfx_render_manager_deinit(struct pg_gfx_render_manager* mgr)
{
    pg_gfx_render_object_pool_deinit(&mgr->object_pool);
    ARR_DEINIT(mgr->active_objects);
    ARR_DEINIT(mgr->processors);
}

void pg_gfx_render_manager_add_processor(
        struct pg_gfx_render_manager* mgr, struct pg_gfx_render_processor* processor)
{
    ARR_PUSH(mgr->processors, processor);
}

pg_gfx_render_object_id_t pg_gfx_render_manager_new_object(
        struct pg_gfx_render_manager* mgr, pg_gfx_render_object_initializer_t obj_init)
{
    struct pg_gfx_render_object* new_obj = NULL;
    pg_gfx_render_object_id_t new_obj_id = pg_gfx_render_object_alloc(&mgr->object_pool, &new_obj);
    if(!new_obj) {
        pg_log(PG_LOG_ERROR, "Failed to allocate pg_gfx_render_object from object pool");
        return PG_MEMPOOL_NULL;
    }
    /*  Use the provided initializer to initialize the object   */
    obj_init(new_obj);

    /*  Iterate over all the render processors to check if this object should
        have a handle associated with each one */
    int processor_id;
    struct pg_gfx_render_processor* processor;
    ARR_FOREACH(mgr->processors, processor, processor_id) {
        int i;
        uint64_t processor_type;
        ARR_FOREACH(new_obj->processor_types, processor_type, i) {
            if(processor_type == processor->type) break;
        }
        /*  This object should not be processed by this type of processor   */
        if(processor_type != processor->type) continue;
        /*  If this object should be processed by this processor, then request
            a new handle from the processor */
        void* new_handle_data = NULL;
        uint64_t new_handle_id = 0;
        if(processor->create_handle) processor->create_handle(processor, &new_handle_data);
        if(!new_handle_id || !new_handle_data) {
            pg_log(PG_LOG_ERROR, "Failed to get new handle from pg_gfx_render_processor");
            continue;
        }
        /*  Then call the object's handle update callback   */
        if(new_obj->update_handle) new_obj->update_handle(new_obj, processor, new_handle_data);
        /*  Add the handle id to the object's list of handles   */
        ARR_PUSH(new_obj->handles, (struct pg_gfx_render_processor_handle) {
                .processor_id = processor_id,
                .processor_handle = new_handle_id,
        });
    }
    return new_obj_id;
}

void pg_gfx_render_manager_delete_object(
        struct pg_gfx_render_manager* mgr, pg_gfx_render_object_id_t obj_id)
{
    struct pg_gfx_render_object* obj = pg_gfx_render_object_get(&mgr->object_pool, obj_id);
    if(!obj) return;
    int i;
    struct pg_gfx_render_processor_handle* handle_ptr;
    ARR_FOREACH_PTR(obj->handles, handle_ptr, i) {
        struct pg_gfx_render_processor* processor = mgr->processors.data[handle_ptr->processor_id];
        if(processor->delete_handle) processor->delete_handle(processor, handle_ptr->processor_handle);
    }
    if(obj->deinit) obj->deinit(obj);
    pg_gfx_render_object_free(&mgr->object_pool, obj_id);
}

struct pg_gfx_render_object* pg_gfx_render_manager_get_object(
        struct pg_gfx_render_manager* mgr, pg_gfx_render_object_id_t obj_id)
{
    return pg_gfx_render_object_get(&mgr->object_pool, obj_id);
}


void pg_gfx_render_manager_update(struct pg_gfx_render_manager* mgr)
{
    int active_object_idx;
    pg_gfx_render_object_id_t obj_id;
    ARR_FOREACH_REV(mgr->active_objects, obj_id, active_object_idx) {
        struct pg_gfx_render_object* object = pg_gfx_render_object_get(&mgr->object_pool, obj_id);
        if(!object) {
            ARR_SWAPSPLICE(mgr->active_objects, active_object_idx, 1);
            continue;
        }
        int object_handle_idx;
        struct pg_gfx_render_processor_handle* handle;
        ARR_FOREACH_PTR_REV(object->handles, handle, object_handle_idx) {
            struct pg_gfx_render_processor* processor = mgr->processors.data[handle->processor_id];
            void* handle_data = NULL;
            if(processor->get_handle_data) {
                handle_data = processor->get_handle_data(processor, handle->processor_handle);
                /*  If the processor has handle data but we got NULL, then remove this handle from the list */
                if(!handle_data) {
                    ARR_SWAPSPLICE(object->handles, object_handle_idx, 1);
                    continue;
                }
            }
            if(object->update_handle) object->update_handle(object, processor, handle_data);
        }
    }
}

