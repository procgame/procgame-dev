#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"
#include "pg_gfx/gfx_asset_loaders.h"

void pg_gfx_asset_loaders(pg_asset_manager_t* asset_mgr)
{
    pg_asset_manager_add_loader(asset_mgr, pg_quadbatch_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_fontset_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_imageset_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_armature_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_ui_context_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_ui_group_asset_loader());

    if(!pg_asset_multiple_from_file(asset_mgr, NULL, "shaders/builtin_shaders.json", NULL, 0)) {
        pg_log(PG_LOG_ERROR, "Failed to load built-in shader");
    }
}

