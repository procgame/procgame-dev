#include "procgame.h"

vec4 pg_animator_step(struct pg_animator* prop, float step)
{
    if(!prop->anim.num_frames) {
        prop->current_value = prop->base_value;
        return prop->current_value;
    }
    prop->progress += step;
    const struct pg_anim_keyframe* last_key = &prop->anim.keyframes[prop->anim.num_frames - 1];
    if(prop->progress >= last_key->start) {
        if(prop->anim.loop_start >= 0) {
            const struct pg_anim_keyframe* loop_key = &prop->anim.keyframes[prop->anim.loop_start];
            prop->progress = loop_key->start + (prop->progress - last_key->start);
            prop->current_frame = prop->anim.loop_start;
        } else {
            prop->progress = last_key->start;
            prop->current_value = last_key->absolute ? last_key->value :
                vec4_add(last_key->value, prop->base_value);
            return prop->current_value;
        }
    }
    while(prop->anim.keyframes[prop->current_frame+1].start < prop->progress) {
        ++prop->current_frame;
    }
    const struct pg_anim_keyframe* current_frame = &prop->anim.keyframes[prop->current_frame];
    const struct pg_anim_keyframe* next_frame = &prop->anim.keyframes[prop->current_frame + 1];
    float duration = next_frame->start - current_frame->start;
    float between = prop->progress - current_frame->start;
    float t = between / duration;
    vec4 f0_val = current_frame->absolute ? current_frame->value :
        vec4_add(current_frame->value, prop->base_value);
    vec4 f1_val = next_frame->absolute ? next_frame->value :
        vec4_add(next_frame->value, prop->base_value);
    pg_easing_func_t func = current_frame->ease ? current_frame->ease : pg_ease_lerp;
    prop->current_value = vec4_lerp(f0_val, f1_val, func(t));
    return prop->current_value;
}

void pg_animator_set(struct pg_animator* prop, const struct pg_animation* anim)
{
    prop->current_value = prop->base_value;
    prop->progress = 0;
    prop->current_frame = 0;
    if(!anim) prop->anim = (struct pg_animation){};
    else prop->anim = *anim;
}

vec4 pg_simple_anim_value(struct pg_simple_anim* anim, float t)
{
    if(!anim->duration) {
        return anim->start;
    } else if(anim->start_time < 0) {
        anim->start_time = t;
        return anim->start;
    }
    float p = LM_SATURATE((t - anim->start_time) / anim->duration);
    return vec4_lerp(anim->start, anim->end, anim->ease(p));
}

int pg_simple_anim_finished(struct pg_simple_anim* anim, float t)
{
    if(anim->start_time > 0 && t > anim->start_time + anim->duration) return 1;
    else return 0;
}
