#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gfx/pg_gfx.h"
#include <errno.h>

/****************************/
/*  TEXT FORMATTING         */
/****************************/

void pg_text_form_init_alloc(struct pg_text_form* form, int n)
{
    if(n == 0) {
        form->glyphs = NULL;
        form->max_glyphs = 0;
        form->n_glyphs = 0;
        form->own_alloc = 1;
    } else {
        form->glyphs = malloc(sizeof(*form->glyphs) * n);
        form->max_glyphs = n;
        form->n_glyphs = 0;
        form->own_alloc = 1;
    }
}

void pg_text_form_init_ptr(struct pg_text_form* form, struct pg_text_form_glyph* glyphs, int n)
{
    form->glyphs = glyphs;
    form->max_glyphs = n;
    form->own_alloc = 0;
}

void pg_text_form_deinit(struct pg_text_form* form)
{
    if(form->own_alloc) free(form->glyphs);
}

void pg_text_form_alloc_reserve(struct pg_text_form* form, int n)
{
    if(n <= form->max_glyphs) return;
    form->glyphs = realloc(form->glyphs, sizeof(*form->glyphs) * n);
    form->max_glyphs = n;
}

#define PG_GLYPH_FLAG_JUSTIFY_EXPAND    (1<<0)

/*  Running format state, modified by inline format codes or regular
    formatting stuff    */
struct format_state {
    struct pg_font* font;
    int font_idx;
    float scale;
    float htab;
    uint32_t color;
    float longest_line;
    int n_lines;
    int on_last_line;
    float highest_point, lowest_point;
};

/*  Format codes:
    %S(scale)           Scale all following glyphs
    %C(r,g,b,a)         Color all following glyphs (rgba 0.0-1.0)
    %Cn                 Color all following glyphs (formatter palette index n)
    %F(n)               Select font index n for all following glyphs
*/

int parse_fmt_code(const wchar_t* str, int len, struct format_state* state,
                   struct pg_text_formatter* fmt)
{
    int code_len = 0;
    wchar_t bracket[2];
    if(!str[0]) return -1;
    if(str[0] == L'S') {
        float parse_scale;
        int match = swscanf(str+1, L"%[(]%f%[)]%n",
            &bracket[0], &parse_scale, &bracket[1], &code_len);
        if(match != 3) return -1;
        state->scale = parse_scale;
    } else if(str[0] == L'C') {
        if(str[1] && str[1] >= L'0' && str[1] <= L'7') {
            state->color = fmt->color_palette[(int)(str[1]) - '0'];
            code_len = 1;
        } else {
            vec4 c;
            int match = swscanf(str+1, L"%[(]%f,%f,%f,%f%[)]%n",
                &bracket[0], &c.x, &c.y, &c.z, &c.w, &bracket[0], &code_len);
            if(match != 6) return -1;
            state->color = VEC4_TO_UINT(c);
        }
    } else if(str[0] == L'F') {
        int font_idx = 0;
        int match = swscanf(str + 1, L"%[(]%d%[)]%n",
            &bracket[0], &font_idx, &bracket[1], &code_len);
        if(match != 3) return -1;
        state->font = pg_fontset_font(fmt->fonts, font_idx);
    }
    return 1 + code_len;
}

static void format_line(struct pg_text_form_glyph* f_glyphs, int n,
                        struct pg_text_formatter* fmt, struct format_state* state)
{
    struct pg_text_form_glyph* last = &f_glyphs[n - 1];
    float line_len = last->pos.x + last->scale.x * 0.5;
    if(line_len > state->longest_line) state->longest_line = line_len;
    state->n_lines += 1;
    int i;
    switch(fmt->align) {
    case PG_TEXT_LEFT: {
        return;
    } case PG_TEXT_CENTER: {
        float line_move = (fmt->region.x - line_len) * 0.5;
        for(i = 0; i < n; ++i) f_glyphs[i].pos.x += line_move;
        break;
    } case PG_TEXT_RIGHT: {
        float line_move = (fmt->region.x - line_len);
        for(i = 0; i < n; ++i) f_glyphs[i].pos.x += line_move;
        break;
    } case PG_TEXT_JUSTIFY: {
        if(fmt->region.x <= 0) break;
        if(state->on_last_line
        && (line_len / fmt->region.x < fmt->last_line_justify_threshold)) break;
        float line_extra = (fmt->region.x - line_len);
        if(line_extra <= 0) break;
        int num_spaces = 0;
        for(i = 0; i < n; ++i) num_spaces += !!(f_glyphs[i].ctrl_flags & 1);
        if(num_spaces == 0) break;
        float space_fraction = line_extra / (float)num_spaces;
        float extra_space = 0;
        for(i = 0; i < n; ++i) {
            //f_glyphs[i].pos.x = floor(f_glyphs[i].pos.x + extra_space);
            if((f_glyphs[i].ctrl_flags & PG_GLYPH_FLAG_JUSTIFY_EXPAND))
                extra_space += space_fraction;
        }
        break;
    }
    }
}

/*  Consume all spaces and tabs, and the whole rest of the next word,
    if width is passed, skip all the spaces, and advance the line   */
static int check_word_wrap(struct pg_text_formatter* fmt, struct format_state* state,
                           const wchar_t* str, int len, float start, float width,
                           int* consume_chars)
{
    /*  Keep a temporary format state since we might hit some in blocks of
        whitespace  */
    struct format_state spc_state = *state;
    float xadvance_space = state->font->chars.data[' ' - 32].xadvance;
    float size = start;
    int space_chars = 0;
    int i;
    /*  First loop: go through all the spaces; still have to account for inline format codes */
    for(i = 0; i < len; ++i) {
        if(str[i] == L'\0') {
            *consume_chars = i;
            return 0;
        } else if(str[i] == L'%') {
            int code_len;
            if(str[i + 1] == L'\0') continue;
            else if(str[i + 1] == L'%') ++i;
            else {
                code_len = parse_fmt_code(&str[i + 1], len - i, &spc_state, fmt);
                if(code_len != -1) i += code_len;
                else return -1;
                continue;
            }
        } else if(str[i] == L' ') {
            size += xadvance_space * spc_state.scale * spc_state.font->scale * fmt->size.x;
        } else if(str[i] == L'\t' && spc_state.htab != 0) {
            float to_next = spc_state.htab - LM_FMOD(size, spc_state.htab);
            size += to_next;
        } else break;
    }
    /*  If a bunch of spaces go past the end, just skip all the whitespace and we're done  */
    if(size > width) {
        *consume_chars = i - 1;
        /*  Copy out the temporary format since we skip surrounding space   */
        *state = spc_state;
        return 1;
    } else space_chars = i - 1;
    /*  Another temp format, we still need to keep track, but also still
        might copy out the format created when looking through spaces   */
    struct format_state word_state = spc_state;
    /*  Now find the end of the word following the whitespace   */
    for(; i < len; ++i) {
        if(str[i] == L'\0' || str[i] == L' ' || str[i] == L'\n' || str[i] == L'\t') break;
        else if(str[i] == L'%') {
            int code_len;
            if(str[i + 1] == L'\0') continue;
            else if(str[i + 1] == L'%') ++i;
            else {
                code_len = parse_fmt_code(&str[i + 1], len - i, &word_state, fmt);
                if(code_len != -1) i += code_len;
                else return -1;
                continue;
            }
        }
        int glyph_idx = pg_font_get_char_index(state->font, str[i]);
        if(glyph_idx < 0) continue;
        const struct pg_font_glyph* glyph =
            &state->font->chars.data[glyph_idx];
        size += glyph->xadvance * word_state.scale * word_state.font->scale * fmt->size.x;
        /*  If a letter in the word goes past the end, skip all the whitespace
            we found, and start the next line at the beginning of the word  */
        if(size > width) {
            *consume_chars = space_chars;
            *state = spc_state;
            return 1;
        }
    }
    *consume_chars = 0;
    return 0;
}

void pg_text_format(struct pg_text_form* form, struct pg_text_formatter* fmt,
                      const char* str, int len)
{
    wchar_t wstr[len + 1];
    int i;
    for(i = 0; i < len; ++i) wstr[i] = str[i];
    wstr[len] = L'\0';
    pg_text_format_w(form, fmt, wstr, len);
}

void pg_text_format_w(struct pg_text_form* form, struct pg_text_formatter* fmt,
                    const wchar_t* str, int len)
{
    struct pg_font* def_font = pg_fontset_font(fmt->fonts, fmt->default_font);
    if(!def_font) return;
    float line_spacing = def_font->line_move * fmt->size.y * fmt->spacing.y;
    int in_word = 0;
    int g_line_start = 0;
    int glyph_i = 0;
    int i;
    struct format_state state = {
        .font = def_font, .scale = 1, .htab = def_font->size * fmt->htab,
        .color = fmt->color_palette[0] };
    vec2 pen = vec2(0,0);
    for(i = 0; i < len && glyph_i < form->max_glyphs && str[i]; ++i) {
        wchar_t cur_char = str[i];
        /*  Handle inline format codes  */
        if(cur_char == L'%') {
            int code_len;
            if(str[i + 1] == L'\0') continue;
            else if(str[i + 1] == L'%') ++i;
            else {
                code_len = parse_fmt_code(&str[i + 1], len - i, &state, fmt);
                if(code_len != -1) i += code_len;
                else return;
                continue;
            }
        /*  Handle text wrapping    */
        } else if(fmt->wrap == PG_TEXT_WRAP_WORDS && in_word
               && (cur_char == L' ' || cur_char == L'\t')) {
            in_word = 0;
            int consume = 0;
            int wrap = check_word_wrap(fmt, &state, str+i, len - i,
                                       pen.x, fmt->region.x, &consume);
            if(wrap == -1) {
                return;
            } else if(wrap) {
                format_line(form->glyphs + g_line_start, glyph_i - g_line_start, fmt, &state);
                g_line_start = glyph_i;
                pen = vec2(0, pen.y - line_spacing * state.font->scale);
                i += consume;
                continue;
            }
        }
        /*  Newline and tab characters  */
        if(cur_char == L'\n') {
            format_line(form->glyphs + g_line_start, glyph_i - g_line_start, fmt, &state);
            g_line_start = glyph_i;
            pen = vec2(0, pen.y - line_spacing * state.font->scale);
            continue;
        } else if(cur_char == L'\t' && state.htab != 0) {
            float to_next = state.htab - LM_FMOD(pen.x, state.htab);
            pen = vec2(pen.x + to_next, pen.y);
            continue;
        }

        /*  Get the glyph associated with this character    */
        int glyph_idx = pg_font_get_char_index(state.font, str[i]);
        if(glyph_idx < 0) continue;
        const struct pg_font_glyph* glyph =
            &state.font->chars.data[glyph_idx];
        struct pg_text_form_glyph* f_glyph = &form->glyphs[glyph_i];
        *f_glyph = (struct pg_text_form_glyph){};

        /*  Handle whitespace characters for formatting     */
        if(cur_char == L' ') {
            f_glyph->ctrl_flags |= PG_GLYPH_FLAG_JUSTIFY_EXPAND;
        } else in_word = 1;

        /*  Calculate transforms based on inline format and big formatter   */
        vec2 glyph_scale = vec2_mul(vec2_scale(glyph->scale, state.scale * state.font->scale), fmt->size);
        vec2 glyph_offset = vec2_mul(vec2_scale(glyph->offset, state.scale * state.font->scale), fmt->size);
        float advance = glyph->xadvance * state.scale * fmt->spacing.x * fmt->size.x * state.font->scale;

        /*  Wrapping by letter OR wrapping words longer than wrap region    */
        if(fmt->wrap && pen.x + advance > fmt->region.x) {
            format_line(form->glyphs + g_line_start, glyph_i - g_line_start, fmt, &state);
            g_line_start = glyph_i;
            pen = vec2(0, pen.y - line_spacing * state.font->scale);
        }

        /*  Finally we can just spit out a glyph!   */
        f_glyph->pos = vec2_add(vec2_add(pen, glyph_offset),
                                 vec2_scale(glyph_scale, 0.5));
        f_glyph->pos.y *= -1;
        f_glyph->pos.x = f_glyph->pos.x;
        f_glyph->scale = glyph_scale;
        f_glyph->uv0 = glyph->uv[0];
        f_glyph->uv1 = glyph->uv[1];
        f_glyph->tex_layer = state.font->tex_layer;
        f_glyph->color = state.color;

        /*  Move the "pen"  */
        pen.x += glyph->xadvance * state.scale * state.font->scale * fmt->spacing.x * fmt->size.x;
        //pen.x = ceil(pen.x);
        ++glyph_i;

        /*  Keep track of the highest and lowest point among all calculated glyphs    */
        const float high = f_glyph->pos.y + (f_glyph->scale.y * 0.5);
        const float low = f_glyph->pos.y - (f_glyph->scale.y * 0.5);
        state.highest_point = LM_MAX(high, state.highest_point);
        state.lowest_point = LM_MIN(low, state.lowest_point);
    }

    /*  Format the last line (or only line, if there is only one)   */
    state.on_last_line = 1;
    format_line(form->glyphs + g_line_start, glyph_i - g_line_start, fmt, &state);
    form->n_glyphs = glyph_i;

    /*  Calculate the bounding box of the entire text using the stored high/low points  */
    form->area[0].y = -state.lowest_point;
    form->area[1].y = -state.highest_point;
    if(fmt->align == PG_TEXT_LEFT) {
        form->area[0].x = 0;
        form->area[1].x = state.longest_line;
    } else if(fmt->align == PG_TEXT_RIGHT) {
        form->area[0].x = fmt->region.x - state.longest_line;
        form->area[1].x = 0;
    } else if(fmt->align == PG_TEXT_CENTER) {
        form->area[0].x = (fmt->region.x - state.longest_line) * 0.5;
        form->area[1].x = (fmt->region.x - state.longest_line) * 0.5 + state.longest_line;
    } else if(fmt->align == PG_TEXT_JUSTIFY) {
        form->area[0].x = 0;
        form->area[1].x = fmt->region.x;
    }
}

