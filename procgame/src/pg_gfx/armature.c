#include "procgame.h"

void pg_armature_init_bone(struct pg_armature_bone* bone, const char* name, quat rot, vec3 pos, float len)
{
    strncpy(bone->name, name, PG_ARMATURE_NAME_MAXLEN);
    bone->pos = pos;
    bone->rot = rot;
    bone->len = len;
    bone->local_space = mat3_mul_quat(mat3_identity(), rot);
    bone->parent = -1;
}

void pg_armature_init(struct pg_armature* rig)
{
    *rig = (struct pg_armature){};
    SARR_INIT(rig->bones);
    SARR_INIT(rig->poses);
    SARR_INIT(rig->sequences);
    HTABLE_INIT(rig->bone_table, 8);
    HTABLE_INIT(rig->pose_table, 8);
    HTABLE_INIT(rig->sequence_table, 8);
}

void pg_armature_deinit(struct pg_armature* rig)
{
    HTABLE_DEINIT(rig->bone_table);
    HTABLE_DEINIT(rig->pose_table);
    HTABLE_DEINIT(rig->sequence_table);
}

int pg_armature_add_bone(struct pg_armature* rig, struct pg_armature_bone* bone)
{
    ARR_PUSH(rig->bones, *bone);
    HTABLE_SET(rig->bone_table, bone->name, rig->bones.len - 1);
    return rig->bones.len - 1;
}

int pg_armature_add_pose(struct pg_armature* rig, struct pg_armature_pose* pose)
{
    ARR_PUSH(rig->poses, *pose);
    HTABLE_SET(rig->pose_table, pose->name, rig->poses.len - 1);
    return rig->poses.len - 1;
}

int pg_armature_add_sequence(struct pg_armature* rig, struct pg_armature_sequence* sequence)
{
    ARR_PUSH(rig->sequences, *sequence);
    HTABLE_SET(rig->sequence_table, sequence->name, rig->sequences.len - 1);
    return rig->sequences.len - 1;
}

void pg_armature_set_bone_parent(struct pg_armature* rig, int child_bone, int parent_bone)
{
    rig->bones.data[child_bone].parent = parent_bone;
    ARR_PUSH(rig->bones.data[parent_bone].children, child_bone);
}

struct pg_armature_sequence* pg_armature_get_sequence(struct pg_armature* rig, char* name)
{
    int* seq_idx;
    HTABLE_GET(rig->sequence_table, name, seq_idx);
    if(!seq_idx) return &rig->sequences.data[0];
    return &rig->sequences.data[*seq_idx];
}

int pg_armature_get_pose_by_name(struct pg_armature* rig, char* name)
{
    int* pose_idx;
    HTABLE_GET(rig->pose_table, name, pose_idx);
    if(!pose_idx) return 0;
    return *pose_idx;
}

int pg_armature_get_bone_by_name(struct pg_armature* rig, char* name)
{
    int* bone_idx;
    HTABLE_GET(rig->bone_table, name, bone_idx);
    if(!bone_idx) return 0;
    return *bone_idx;
}



void pg_armature_get_lerp_pose(struct pg_armature* rig, struct pg_armature_pose* out,
                               int pose_0, int pose_1, float pose_lerp)
{
    struct pg_armature_bone_tx* b0 = rig->poses.data[pose_0].bones.data;
    struct pg_armature_bone_tx* b1 = rig->poses.data[pose_1].bones.data;
    struct pg_armature_bone_tx* b_out = out->bones.data;
    int i;
    for(i = 0; i < rig->bones.len; ++i, ++b0, ++b1, ++b_out) {
        b_out->move = vec3_lerp(b0->move, b1->move, pose_lerp);
        b_out->rot = quat_norm(quat_lerp(b0->rot, b1->rot, pose_lerp));
    }
    out->bones.len = rig->bones.len;
}

void pg_armature_pose_empty(struct pg_armature_pose* pose)
{
    int i;
    for(i = 0; i < PG_ARMATURE_MAX_BONES; ++i) {
        pose->bones.data[i] = (struct pg_armature_bone_tx){
            .rot = quat_identity(),
            .move = vec3(0,0,0),
            .enabled = false,
        };
    }
}

void pg_armature_pose_lerp(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1,
                           float pose_lerp)
{
    struct pg_armature_bone_tx* b0 = p0->bones.data;
    struct pg_armature_bone_tx* b1 = p1->bones.data;
    struct pg_armature_bone_tx* b_out = out->bones.data;
    int i;
    for(i = 0; i < rig->bones.len; ++i, ++b0, ++b1, ++b_out) {
        b_out->move = vec3_lerp(b0->move, b1->move, pose_lerp);
        b_out->rot = quat_norm(quat_lerp(b0->rot, b1->rot, pose_lerp));
    }
    out->bones.len = rig->bones.len;
}

void pg_armature_pose_apply(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1)
{
    struct pg_armature_bone_tx* b0 = p0->bones.data;
    struct pg_armature_bone_tx* b1 = p1->bones.data;
    struct pg_armature_bone_tx* b_out = out->bones.data;
    int i;
    for(i = 0; i < rig->bones.len; ++i, ++b0, ++b1, ++b_out) {
        b_out->rot = quat_norm(quat_mul(b1->rot, b0->rot));
        b_out->move = vec3_add(quat_mul_vec3(b1->rot, b0->move), b1->move);
    }
    out->bones.len = rig->bones.len;
}

static int enqueue_bone_children(struct pg_armature_bone* bone, int* bq, int bq_len)
{
    int i;
    for(i = 0; i < bone->children.len; ++i)
        bq[bq_len++] = bone->children.data[i];
    return bq_len;
}

void pg_armature_pose_propagate(struct pg_armature* rig, struct pg_armature_pose* out,
                                struct pg_armature_pose* pose)
{
    int bq[PG_ARMATURE_MAX_BONES] = { 0 };
    int bq_len = 1;
    while(bq_len) {
        int bone_idx = bq[--bq_len];
        int parent_idx = rig->bones.data[bone_idx].parent;
        struct pg_armature_bone* bone = &rig->bones.data[bone_idx];
        struct pg_armature_bone_tx* b_out = &out->bones.data[bone_idx];
        struct pg_armature_bone_tx* b_pose = &pose->bones.data[bone_idx];
        if(parent_idx == -1) {
            b_out = b_pose;
            bq_len = enqueue_bone_children(bone, bq, bq_len);
            continue;
        }
        struct pg_armature_bone_tx* b_parent = &out->bones.data[parent_idx];
        struct pg_armature_bone* parent = &rig->bones.data[parent_idx];
        vec3 parent_vec = vec3_sub(bone->pos, parent->pos);
        vec3 parent_vec_rot = quat_mul_vec3(b_parent->rot, parent_vec);
        vec3 parent_diff = vec3_sub(parent_vec_rot, parent_vec);
        vec3 pose_move = quat_mul_vec3(b_parent->rot, b_pose->move);
        b_out->move = vec3_add(vec3_add(parent_diff, pose_move), b_parent->move);
        b_out->rot = quat_mul(b_parent->rot, b_pose->rot);
        bq_len = enqueue_bone_children(&rig->bones.data[bone_idx], bq, bq_len);
    }
}

void pg_armature_pose_edit_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                                int bone_idx, quat rot, vec3 move)
{
    struct pg_armature_bone_tx* b_out = &pose->bones.data[bone_idx];
    b_out->rot = quat_norm(quat_mul(rot, b_out->rot));
    b_out->move = vec3_add(quat_mul_vec3(rot, b_out->move), move);
}

void pg_armature_pose_get_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                               struct pg_armature_bone_tx* tx_out, int bone_idx)
{
    struct pg_armature_bone_tx* b_pose = &pose->bones.data[bone_idx];
    *tx_out = *b_pose;
}

void pg_armature_pose_get_attachment(struct pg_armature* rig, struct pg_armature_pose* pose,
                                  struct pg_armature_bone_tx* tx_out, int bone_idx,
                                  vec3 attach_point)
{
    struct pg_armature_bone* bone = &rig->bones.data[bone_idx];
    struct pg_armature_bone_tx* b_pose = &pose->bones.data[bone_idx];
    quat attach_quat = quat_mul(b_pose->rot, bone->rot);
    vec3 attach_move = quat_mul_vec3(attach_quat, attach_point);
    tx_out->move = vec3_add(vec3_add(bone->pos, b_pose->move), attach_move);
    tx_out->rot = attach_quat;
}

void pg_armature_bone_transform(struct pg_armature_bone_tx* tx, quat rot, vec3 move, float scale)
{
    tx->rot = quat_mul(rot, tx->rot);
    tx->move = vec3_add(move, vec3_scale(quat_mul_vec3(rot, tx->move), scale));
}
