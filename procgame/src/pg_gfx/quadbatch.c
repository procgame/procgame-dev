#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_gfx/pg_gfx.h"

/****************************************/
/*  Unified sprite/text batching shader */
/****************************************/

static const struct pg_buffer_layout quadbuffer_layout = PG_BUFFER_LAYOUT(
    PG_BUFFER_PACKING_DATA_TYPE(PG_UVEC4),
    PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
    PG_BUFFER_ELEMENT_ALIGNMENT(64),
    PG_BUFFER_ATTRIBUTES_LIST(8,
        PG_BUFFER_ATTRIBUTE("position", PG_VEC3),
        PG_BUFFER_ATTRIBUTE("scale", PG_VEC2),
        PG_BUFFER_ATTRIBUTE("color_add", PG_UBVEC4),
        PG_BUFFER_ATTRIBUTE("color_mul", PG_UBVEC4),
        PG_BUFFER_ATTRIBUTE("tex_layer", PG_UINT16),
        PG_BUFFER_ATTRIBUTE("tex_select", PG_UINT16),
        PG_BUFFER_ATTRIBUTE("uv", PG_VEC4),
        PG_BUFFER_ATTRIBUTE("orientation", PG_VEC4),
    ),
);

void pg_quadbatch_init(struct pg_quadbatch* batch, int size)
{
    pg_buffer_init(&batch->quads_buffer, &quadbuffer_layout, size);
    batch->gpu_quads_buffer = pg_buffer_upload(&batch->quads_buffer, PG_GPU_TEXTURE_BUFFER, false);
    pg_gpu_buffer_set_local_data(batch->gpu_quads_buffer, &batch->quads_buffer, false);
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}

void pg_quadbatch_deinit(struct pg_quadbatch* batch)
{
    pg_buffer_deinit(&batch->quads_buffer);
    pg_gpu_buffer_deinit(batch->gpu_quads_buffer);
}

/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_quadbatch_upload(struct pg_quadbatch* batch)
{
    pg_gpu_buffer_reupload(batch->gpu_quads_buffer);
}

void pg_quadbatch_add_quad(struct pg_quadbatch* batch, const struct pg_ezquad* quad)
{
    struct pg_ezquad* ptr = (struct pg_ezquad*)pg_buffer_get_element_ptr(
            &batch->quads_buffer, batch->cur_start + batch->cur_idx);
    if(ptr) *ptr = *quad;
    batch->cur_idx += 1;
}

/*  Add sprites or text to a current batch group    */
void pg_quadbatch_add_sprite(struct pg_quadbatch* batch, const struct pg_draw_2d* draw)
{

    uint32_t color_add = 0;
    color_add |= (uint32_t)(draw->color_add.x * 255) << 24;
    color_add |= (uint32_t)(draw->color_add.y * 255) << 16;
    color_add |= (uint32_t)(draw->color_add.z * 255) << 8;
    color_add |= (uint32_t)(draw->color_add.w * 255) << 0;
    uint32_t color_mul = 0;
    color_mul |= (uint32_t)(draw->color_mul.x * 255) << 24;
    color_mul |= (uint32_t)(draw->color_mul.y * 255) << 16;
    color_mul |= (uint32_t)(draw->color_mul.z * 255) << 8;
    color_mul |= (uint32_t)(draw->color_mul.w * 255) << 0;
    struct pg_ezquad q = {
        .pos = vec3( draw->pos.x, draw->pos.y, draw->pos.z ),
        .scale = draw->scale,
        .orientation = quat_rotation(vec3(0, 0, 1), -draw->rotation),
        .color_add = color_add, .color_mul = color_mul,
        .uv = { draw->uv[0], draw->uv[1] },
        .tex_layer = draw->tex_layer, .tex_select = 0,
    };
    pg_quadbatch_add_quad(batch, &q);
}

static inline void add_text_form(struct pg_quadbatch* batch, const struct pg_text_form* form,
                                 const struct pg_draw_text* draw)
{
    struct pg_ezquad q = { .tex_select = 1,
        .orientation = draw->orientation,
        .color_add = 0, .color_mul = VEC4_TO_UINT(draw->color),
    };
    uint32_t last_color = 0;
    uint32_t last_color_calc = 0;
    int i;
    for(i = 0; i < form->n_glyphs; ++i) {
        struct pg_text_form_glyph* fglyph = &form->glyphs[i];
        vec2 g_pos = vec2_mul(vec2_sub(fglyph->pos, draw->anchor), draw->scale);
        q.pos = quat_mul_vec3(draw->orientation, vec3(VEC_XY(g_pos)));
        q.pos = vec3_add(q.pos, vec3(VEC_XY(draw->anchor)));
        q.pos = vec3_add(q.pos, draw->pos);
        //q.pos = vec3_add(q.pos, draw->pos);
        q.scale = vec2_mul(fglyph->scale, draw->scale);
        q.uv[0] = vec2(VEC_XY(fglyph->uv0));
        q.uv[1] = vec2(VEC_XY(fglyph->uv1));
        q.tex_layer = fglyph->tex_layer;
        if(fglyph->color != last_color) {
            last_color = fglyph->color;
            vec4 color = vec4((last_color>>24)&0xFF, (last_color>>16)&0xFF,
                              (last_color>>8)&0xFF, last_color&0xFF);
            color = vec4_scale(color, (1.0f / 255.0f));
            color = vec4_mul(color, draw->color);
            last_color_calc = VEC4_TO_UINT(color);
        }
        q.color_mul = last_color_calc;
        pg_quadbatch_add_quad(batch, &q);
    }
}

void pg_quadbatch_add_text(struct pg_quadbatch* batch, const struct pg_draw_text* draw)
{
    int len = draw->len ? draw->len :
        draw->str_w ? wcslen(draw->str_w) :
        draw->str ? strlen(draw->str) : 0;
    if(draw->form) {
        add_text_form(batch, draw->form, draw);
    } else if(draw->formatter) {
        struct pg_text_form_glyph f_glyphs[len];
        struct pg_text_form form;
        pg_text_form_init_ptr(&form, f_glyphs, len);
        if(draw->str_w) pg_text_format_w(&form, draw->formatter, draw->str_w, len);
        else if(draw->str) pg_text_format(&form, draw->formatter, draw->str, len);
        add_text_form(batch, &form, draw);
    } else if(draw->fonts) {
        struct pg_text_form_glyph f_glyphs[len];
        struct pg_text_formatter formatter = PG_TEXT_FORMATTER(draw->fonts);
        struct pg_text_form form;
        pg_text_form_init_ptr(&form, f_glyphs, len);
        if(draw->str_w) pg_text_format_w(&form, &formatter, draw->str_w, len);
        else if(draw->str) pg_text_format(&form, &formatter, draw->str, len);
        add_text_form(batch, &form, draw);
    }


}

/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_quadbatch_draw(struct pg_quadbatch* batch, pg_gpu_command_arr_t* commands)
{
    mat4 mvp = batch->cur_transform;
    pg_gpu_record_commands(commands);
    pg_gpu_cmd_set_uniform_matrix(2, PG_MAT4, false, 1, mvp.v);
    int quad_buffer_base = batch->cur_start * 4;
    pg_gpu_cmd_set_uniform(3, PG_INT, 1, &quad_buffer_base);
    pg_gpu_cmd_draw_triangles(0, (batch->cur_idx) * 6);
}



/*  Begin a new batch group with a given model matrix   */
void pg_quadbatch_next(struct pg_quadbatch* batch, const mat4* transform)
{
    batch->cur_start += batch->cur_idx;
    batch->cur_idx = 0;
    batch->cur_transform = *transform;
}

/*  Restart batching    */
void pg_quadbatch_reset(struct pg_quadbatch* batch)
{
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}

pg_gpu_resource_set_t* pg_quadbatch_create_gpu_resources(struct pg_quadbatch* batch,
        pg_gpu_texture_t* images_tex, pg_gpu_texture_t* fonts_tex)
{
    /*  Make resource set asset data and put it in the asset manager    */
    pg_gpu_resource_set_t* gpu_resources = pg_gpu_resource_set_create();
    pg_gpu_resource_set_attach_texture_buffer(gpu_resources, 0, batch->gpu_quads_buffer);
    pg_gpu_resource_set_attach_texture(gpu_resources, 1, images_tex);
    pg_gpu_resource_set_attach_texture(gpu_resources, 2, fonts_tex);
    pg_gpu_resource_set_attach_texture_params(gpu_resources, 2, 
        &PG_GPU_TEXTURE_PARAMS(
            .filter_min = PG_GPU_TEXTURE_PARAM_FILTER_LINEAR,
            .filter_mag = PG_GPU_TEXTURE_PARAM_FILTER_LINEAR ));
    return gpu_resources;
}
