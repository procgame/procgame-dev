
/************************************************/
/*  Internal UI structures                      */
/************************************************/

struct pg_ui_element {
    /*  Static fields   */
    enum pg_ui_draw_order draw_order;
    int enable_clip;
    int image_layer;
    struct pg_text_formatter text_formatter;
    struct pg_text_form text_form;
    vec2 text_anchor;
    /*  Event callbacks */
    enum pg_ui_action_item action_item;
    pg_ui_callback_t callbacks[PG_UI_ELEM_CALLBACKS];
    /*  Animate-able fields */
    struct pg_animator properties[PG_UI_ELEM_PROPERTIES];
    struct pg_image_stepper image_anim;
    float image_anim_start;
    /*  Running information    */
    int mouse_over;
};

struct pg_ui_group {
    /*  Animate-able properties */
    struct pg_animator properties[PG_UI_GROUP_PROPERTIES];
    /*  Clipping enabled    */
    bool enable_clip;
    bool enable_3d;
    /*  Children hash table */
    pg_ui_table_t children;
    /*  Children stored in depth order, bottom to top   */
    ARR_T(pg_ui_t) children_arr;
    /*  Indicates if new children have been added since last sort   */
    int children_added;
};

struct pg_ui_content {
    enum { PG_UI_FREE, PG_UI_UNINITIALIZED, PG_UI_GROUP, PG_UI_ELEMENT } type;
    int marked_to_free;
    char name[PG_UI_NAME_MAXLEN];
    pg_ui_t self;
    pg_ui_t parent;
    /*  User variables hash table   */
    pg_data_table_t vars;
    int disabled;
    int fix_aspect;
    /*  Mouse pos relative to this content  */
    vec3 mouse_pos;
    vec3 cursor_3d_pos;
    vec3 cursor_3d_dir;
    vec3 cursor_3d_hit_pos;
    bool cursor_3d_hit;
    /*  Depth layer */
    int layer;
    /*  UI type */
    union {
        struct pg_ui_group grp;
        struct pg_ui_element elem;
    };
    vec4 debug_color;
};

/*  A logical space which can create (or transform) pg_gfx_transform structures
    (ie. a display rectangle with a pixel grid) */
struct ui_space {
    struct pg_gfx_transform origin;
    /*  Aspect ratio handling   */
    vec3 aspect_fix;
    bool enabled_3d;
};

void ui_group_get_transforms(struct pg_ui_context* ctx, const struct pg_ui_content* cont, const struct ui_space* space,
        struct pg_gfx_transform* tx_out, struct pg_gfx_transform* tx_out_clip,
        struct ui_space* grp_space_out);

void ui_element_get_transforms(struct pg_ui_element* elem, const struct ui_space* space,
        struct pg_gfx_transform* tx_out,
        struct pg_gfx_transform* tx_out_text,
        struct pg_gfx_transform* tx_out_image,
        struct pg_gfx_transform* tx_out_action);

pg_ui_t ui_alloc(struct pg_ui_context* ctx, int n, struct pg_ui_content** ptr);
void ui_free(struct pg_ui_context* ctx, pg_ui_t ui_ref);
struct pg_ui_content* ui_dereference(struct pg_ui_context* ctx, pg_ui_t ui_ref);
void ui_group_init(struct pg_ui_group* grp, struct pg_ui_properties* props);
void ui_elem_init(struct pg_ui_context* ctx, struct pg_ui_content* cont, struct pg_ui_properties* props);
