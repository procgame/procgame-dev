#include "procgame.h"
#include "ui_internal.h"

/************************************************/
/*  UI Element functions                        */
/************************************************/

void pg_ui_set_enabled(struct pg_ui_context* ctx, pg_ui_t ui_ref, int enabled)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(cont) cont->disabled = !enabled;
}

void pg_ui_set_callback(struct pg_ui_context* ctx, pg_ui_t ui,
                        enum pg_ui_callback_id id, pg_ui_callback_t cb)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.callbacks[id] = cb;
}

const char* pg_ui_get_name(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return "<null>";
    return cont->name;
}

pg_ui_t pg_ui_get_child(struct pg_ui_context* ctx, pg_ui_t parent,
                              const char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, parent);
    if(!cont || cont->type != PG_UI_GROUP) return -1;
    pg_ui_t* ref;
    HTABLE_GET(cont->grp.children, name, ref);
    return ref ? *ref : -1;
}

pg_ui_t pg_ui_get_child_path(struct pg_ui_context* ctx, pg_ui_t parent,
                                   const char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, parent);
    if(!cont || cont->type != PG_UI_GROUP) return -1;
    pg_ui_t* child;
    struct pg_ui_content* iter_cont = cont;
    int i = 0;
    int token_len = 0;
    int token_start = 0;
    while(name[i] != '\0') {
        if(name[i] == '.') {
            token_len = i - token_start;
            if(token_len == 0) return -1;
            HTABLE_NGET(iter_cont->grp.children, name + token_start, token_len, child);
            if(!child) return -1;
            iter_cont = ui_dereference(ctx, *child);
            if(!iter_cont || iter_cont->type != PG_UI_GROUP) return -1;
            token_start = i + 1;
        }
        ++i;
    }
    token_len = i - token_start;
    HTABLE_NGET(iter_cont->grp.children, name + token_start, token_len, child);
    if(!child) return -1;
    return *child;
}

pg_ui_t pg_ui_get_parent(struct pg_ui_context* ctx, pg_ui_t child)
{

    struct pg_ui_content* cont = ui_dereference(ctx, child);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return -1;
    return cont->parent;
}

struct pg_data* pg_ui_get_variable(struct pg_ui_context* ctx,
                               pg_ui_t ui_ref, char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return NULL;
    struct pg_data* ret = NULL;
    HTABLE_GET(cont->vars, name, ret);
    if(!ret) HTABLE_SET_GET(cont->vars, name, ret, (struct pg_data){0});
    return ret;
}

void pg_ui_set_draw_order(struct pg_ui_context* ctx, pg_ui_t ui_ref, enum pg_ui_draw_order order)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.draw_order = order;
}

void pg_ui_set_text(struct pg_ui_context* ctx, pg_ui_t ui_ref, const char* text, int n)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    int len = strnlen(text, n);
    pg_text_form_alloc_reserve(&cont->elem.text_form, len);
    pg_text_format(&cont->elem.text_form, &cont->elem.text_formatter, text, len);
}

void pg_ui_set_text_w(struct pg_ui_context* ctx, pg_ui_t ui_ref, const wchar_t* text, int n)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    int len = wcsnlen(text, n);
    pg_text_form_alloc_reserve(&cont->elem.text_form, len);
    pg_text_format_w(&cont->elem.text_form, &cont->elem.text_formatter, text, len);
}

void pg_ui_set_text_formatter(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                              struct pg_text_formatter* formatter)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.text_formatter = *formatter;
}

void pg_ui_set_image_stepper(struct pg_ui_context* ctx, pg_ui_t ui_ref, struct pg_image_stepper* anim)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.image_anim = *anim;
    cont->elem.image_anim_start = ctx->time;
}

struct pg_animator* pg_ui_get_property(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                         enum pg_ui_property_id prop_id)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont) return NULL;
    if(cont->type == PG_UI_GROUP && prop_id <= PG_UI_ROTATION) {
        return &cont->grp.properties[prop_id];
    } else if(cont->type == PG_UI_ELEMENT) {
        return &cont->elem.properties[prop_id];
    }
    return NULL;
}

vec2 pg_ui_get_mouse_pos(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return vec2(0,0);
    return vec2(VEC_XY(cont->mouse_pos));
}
