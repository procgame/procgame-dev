static inline mat4 mat4_from_openvr_mat44(HmdMatrix44_t* mat_in)
{
    return ((mat4){
        .col[0] = { mat_in->m[0][0], mat_in->m[1][0], mat_in->m[2][0], mat_in->m[3][0] },
        .col[1] = { mat_in->m[0][1], mat_in->m[1][1], mat_in->m[2][1], mat_in->m[3][1] },
        .col[2] = { mat_in->m[0][2], mat_in->m[1][2], mat_in->m[2][2], mat_in->m[3][2] },
        .col[3] = { mat_in->m[0][3], mat_in->m[1][3], mat_in->m[2][3], mat_in->m[3][3] } } );
}

static inline mat4 mat4_from_openvr_mat34(HmdMatrix34_t* mat_in)
{

    return ((mat4){
        .col[0] = { mat_in->m[0][0], mat_in->m[0][1], mat_in->m[0][2], 0 },
        .col[1] = { mat_in->m[1][0], mat_in->m[1][1], mat_in->m[1][2], 0 },
        .col[2] = { mat_in->m[2][0], mat_in->m[2][1], mat_in->m[2][2], 0 },
        .col[3] = { mat_in->m[0][3], mat_in->m[1][3], mat_in->m[2][3], 1 } } );
}


