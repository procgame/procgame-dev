#include "openvr_capi.h"
#include "pg_core/pg_core.h"
#include "pg_modules/window.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "openvr_helper.h"

bool have_vr_display = false;
static struct pg_vr_display vr_display;
struct pg_vr_display* pg_vr_get_display(void) { return have_vr_display ? &vr_display : NULL; } 


bool pg_vr_display_init(vec2 render_scale)
{
    if(!pg_have_vr()) return false;
    struct pg_vr_context* vr = pg_vr_context();
    uvec2 vr_render_size = { 1024, 1024 };

    if(!pg_vr_is_fake()) {
        vr->system->GetRecommendedRenderTargetSize(&vr_render_size.x, &vr_render_size.y);
    }

    vr_render_size.x = (uint32_t)((float)vr_render_size.x * render_scale.x);
    vr_render_size.y = (uint32_t)((float)vr_render_size.y * render_scale.y);
    vr_display.eye_resolution = ivec2(VEC_XY(vr_render_size));
    vr_display.eye_depth[0] = pg_gpu_texture_alloc_2D(PG_GPU_TEXTURE_DEPTH24_STENCIL8, vr_render_size.x, vr_render_size.x);
    vr_display.eye_depth[1] = pg_gpu_texture_alloc_2D(PG_GPU_TEXTURE_DEPTH24_STENCIL8, vr_render_size.x, vr_render_size.y);
    vr_display.eye_textures[0] = pg_gpu_texture_alloc_2D(PG_GPU_TEXTURE_RGBA8, vr_render_size.x, vr_render_size.x);
    vr_display.eye_textures[1] = pg_gpu_texture_alloc_2D(PG_GPU_TEXTURE_RGBA8, vr_render_size.x, vr_render_size.y);
    if(!vr_display.eye_textures[0] || !vr_display.eye_textures[1]) {
        pg_log(PG_LOG_ERROR, "Failed to allocate VR eye render textures");
        return false;
    }

    pg_gpu_texture_view_t* eye_texview_depth_left = pg_gpu_texture_get_basic_view(vr_display.eye_depth[0]);
    pg_gpu_texture_view_t* eye_texview_depth_right = pg_gpu_texture_get_basic_view(vr_display.eye_depth[1]);
    pg_gpu_texture_view_t* eye_texview_left = pg_gpu_texture_get_basic_view(vr_display.eye_textures[0]);
    pg_gpu_texture_view_t* eye_texview_right = pg_gpu_texture_get_basic_view(vr_display.eye_textures[1]);
    pg_gpu_texture_view_t* eye_texviews_left[2] = { eye_texview_left, eye_texview_depth_left };
    pg_gpu_texture_view_t* eye_texviews_right[2] = { eye_texview_right, eye_texview_depth_right };
    enum pg_gpu_framebuffer_attachment_index eye_attachments[2] = { PG_GPU_FRAMEBUFFER_COLOR0, PG_GPU_FRAMEBUFFER_DEPTH_STENCIL };
    vr_display.eye_framebuffers[0] = pg_gpu_framebuffer_init_attachments(2, eye_texviews_left, eye_attachments);
    vr_display.eye_framebuffers[1] = pg_gpu_framebuffer_init_attachments(2, eye_texviews_right, eye_attachments);
    if(!vr_display.eye_framebuffers[0] || !vr_display.eye_framebuffers[1]) {
        pg_log(PG_LOG_ERROR, "Failed to build VR eye framebuffers");
        return false;
    }

    have_vr_display = true;
    return true;
}


void pg_vr_display_deinit(void)
{
    if(!have_vr_display) return;
    pg_gpu_texture_deinit(vr_display.eye_textures[0]);
    pg_gpu_texture_deinit(vr_display.eye_textures[1]);
    pg_gpu_texture_deinit(vr_display.eye_depth[0]);
    pg_gpu_texture_deinit(vr_display.eye_depth[1]);
    pg_gpu_framebuffer_deinit(vr_display.eye_framebuffers[0]);
    pg_gpu_framebuffer_deinit(vr_display.eye_framebuffers[1]);
}


/*  Add the VR display to an asset manager  */
void pg_vr_add_display_to_asset_manager(struct pg_asset_manager* asset_mgr)
{
    if(!have_vr_display) return;
    pg_log(PG_LOG_INFO, "Adding VR display targets to asset manager");
    pg_asset_from_data(asset_mgr, "pg_gpu_texture", "__pg_vr_texture_left",
            vr_display.eye_textures[0], false);
    pg_asset_from_data(asset_mgr, "pg_gpu_texture", "__pg_vr_texture_right",
            vr_display.eye_textures[1], false);
    pg_asset_from_data(asset_mgr, "pg_gpu_texture", "__pg_vr_depth_left",
            vr_display.eye_depth[0], false);
    pg_asset_from_data(asset_mgr, "pg_gpu_texture", "__pg_vr_depth_right",
            vr_display.eye_depth[1], false);
    pg_asset_from_data(asset_mgr, "pg_gpu_framebuffer", "__pg_vr_framebuffer_left",
            vr_display.eye_framebuffers[0], false);
    pg_asset_from_data(asset_mgr, "pg_gpu_framebuffer", "__pg_vr_framebuffer_right",
            vr_display.eye_framebuffers[1], false);
    pg_asset_manager_set_variable(asset_mgr, "__pg_vr_eye_resolution",
            pg_data_init(PG_IVEC2, 1, &vr_display.eye_resolution));
}

mat4 pg_vr_display_get_eye_projection(int eye, vec2 near_far)
{
    if(pg_vr_is_fake()) return mat4_perspective(LM_DEG_TO_RAD(70), 1.0, near_far.x, near_far.y);
    struct pg_vr_context* vr = pg_vr_context();
    HmdMatrix44_t vr_proj = vr->system->GetProjectionMatrix(eye == 0 ? EVREye_Eye_Left : EVREye_Eye_Right, near_far.x, near_far.y);
    return mat4_from_openvr_mat44(&vr_proj);
}

mat4 pg_vr_display_get_eye_transform(int eye)
{
    if(pg_vr_is_fake()) return eye == 0 ? mat4_translation(vec3(-0.3,0,0)) : mat4_translation(vec3(0.3,0,0));
    struct pg_vr_context* vr = pg_vr_context();
    HmdMatrix34_t vr_tx = vr->system->GetEyeToHeadTransform(eye == 0 ? EVREye_Eye_Left : EVREye_Eye_Right);
    return mat4_from_openvr_mat34(&vr_tx);
}


/*  Final texture submission to the headset */

static void submit_eye(int eye, pg_gpu_texture_t* tex)
{
    struct pg_vr_context* vr = pg_vr_context();
    uintptr_t tex_as_ptr = pg_gpu_texture_get_handle(tex);
    struct Texture_t submit_tex = {
        .handle = (void*)tex_as_ptr,
        .eType = ETextureType_TextureType_OpenGL,
        .eColorSpace = EColorSpace_ColorSpace_Gamma,
    };
    EVREye vr_eye = (eye == 0) ? EVREye_Eye_Left : EVREye_Eye_Right;
    //PG_CHECK_GL(glFinish());
    //PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    VRTextureBounds_t bounds = { 0, 0, 1, 1 };
    EVRCompositorError err = vr->compositor->Submit(vr_eye, &submit_tex, &bounds, EVRSubmitFlags_Submit_Default);
    if(err != EVRCompositorError_VRCompositorError_None) {
        pg_log(PG_LOG_ERROR, "VR Eye texture submission failed: %d", err);
    }
}

void pg_vr_display_submit(void)
{
    if(!have_vr_display) return;
    if(pg_vr_is_fake()) {
        pg_window_swap();
        return;
    }
    submit_eye(0, vr_display.eye_textures[0]);
    submit_eye(1, vr_display.eye_textures[1]);
        pg_window_swap();
}


