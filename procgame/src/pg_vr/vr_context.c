#include "openvr_capi.h"

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "openvr_helper.h"

static bool vr_is_fake = false;
static bool vr_initialized = false;
static struct pg_vr_context vr_context;
struct pg_vr_context* pg_vr_context(void) { return &vr_context; }

/*  Get a single OpenVR interface by name   */
#define pg_vr_get_interface(SYSTEM_NAME, ERR_OUT) \
    (struct VR_IVR##SYSTEM_NAME##_FnTable*)pg_vr_internal_interface(IVR##SYSTEM_NAME##_Version, (ERR_OUT))
static void* pg_vr_internal_interface(const char* iface_name, EVRInitError* err_out)
{
    char iface_name_full[128];
    sprintf(iface_name_full, "FnTable:%s", iface_name);
    EVRInitError err;
    void* iface_ptr = (void*)VR_GetGenericInterface(iface_name_full, &err);
    if(err) {
        pg_log(PG_LOG_ERROR, "VR_GetGenericInterface(\"%s\") failed: %s: %s", iface_name_full,
                VR_GetVRInitErrorAsSymbol(err), VR_GetVRInitErrorAsEnglishDescription(err));
        if(err_out) *err_out = err;
        return NULL;
    }
    return iface_ptr;
}

/*  Initialize the OpenVR context with all the (used) function tables   */
static void pg_vr_init_context(EVRInitError* err_out)
{
    EVRInitError err = EVRInitError_VRInitError_None;
    if(!err) vr_context.system = pg_vr_get_interface(System, &err);
    if(!err) vr_context.compositor = pg_vr_get_interface(Compositor, &err);
    if(!err) vr_context.input = pg_vr_get_interface(Input, &err);
    if(err_out) *err_out = err;
}

/*  Initialize OpenVR   */
bool pg_vr_init(void)
{
    if(vr_initialized) return true;

    /*  Do the quick-check functions first  */
    bool vr_runtime_installed = VR_IsRuntimeInstalled();
    pg_log(PG_LOG_INFO, "OpenVR Runtime %s", vr_runtime_installed ? "found" : "not found");
    if(!vr_runtime_installed) return false;

    bool vr_hmd_present = VR_IsHmdPresent();
    pg_log(PG_LOG_INFO, "VR Headset %s", vr_hmd_present ? "found" : "not found");
    if(!vr_hmd_present) return false;

    /*  If it looks ok then we can try to init OpenVR   */
    EVRInitError err = EVRInitError_VRInitError_None;
    VR_InitInternal(&err, EVRApplicationType_VRApplication_Scene);
    if(err) {
        pg_log(PG_LOG_INFO, "OpenVR Init: %s: %s",
                VR_GetVRInitErrorAsSymbol(err), VR_GetVRInitErrorAsEnglishDescription(err));
        return false;
    }

    /*  Fill the context with the OpenVR function tables    */
    pg_vr_init_context(&err);
    if(err) return false;

    /*  Success!    */
    pg_log(PG_LOG_INFO, "VR Initialization Successful");
    vr_initialized = true;
    return true;
}

void pg_vr_deinit(void)
{
    if(vr_initialized && !vr_is_fake) VR_ShutdownInternal();
    vr_initialized = false;
}

bool pg_have_vr(void)
{
    return vr_initialized;
}

bool pg_vr_init_fake(void)
{
    vr_initialized = true;
    vr_is_fake = true;
}

bool pg_vr_is_fake(void)
{
    return vr_is_fake;
}
