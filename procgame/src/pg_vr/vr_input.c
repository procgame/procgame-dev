#include "openvr_capi.h"

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "openvr_helper.h"



static SARR_T(16, struct pg_vr_action_set) action_sets = SARR_DATA(16, 0);

void pg_vr_set_action_manifest(char* path)
{
    char fullpath[PG_FILE_PATH_MAXLEN];
    pg_filename_absolute_path(path, strlen(path), fullpath, PG_FILE_PATH_MAXLEN);
    if(!pg_have_vr() || pg_vr_is_fake()) {
        pg_log(PG_LOG_INFO, "Would set VR action manifest path to '%s'", fullpath);
        return;
    }

    struct pg_vr_context* vr = pg_vr_context();
    EVRInputError err = vr->input->SetActionManifestPath(fullpath);
    if(err) {
        pg_log(PG_LOG_ERROR, "Failed to set VR action manifest path to '%s': error %d", fullpath, err);
        return;
    }
}

void pg_vr_action_set_bindings(char* set_name, int n_bindings, struct pg_vr_action_binding* bindings)
{
    if(pg_vr_is_fake()) return;
    struct pg_vr_context* vr = pg_vr_context();

    VRActionSetHandle_t vr_action_set_id;
    EVRInputError err = vr->input->GetActionSetHandle(set_name, &vr_action_set_id);
    if(err) {
        pg_log(PG_LOG_ERROR, "Failed to get VR ActionSetHandle '%s': error %d", set_name, err);
        return;
    }

    struct pg_vr_action_set* new_set = ARR_NEW(action_sets);
    *new_set = (struct pg_vr_action_set){
        .vr_action_set_id = vr_action_set_id,
        .bindings = SARR_DATA(64, 0),
        .enabled = true,
    };
    strncpy(new_set->vr_action_set_name, set_name, 256);
    for(int i = 0; i < n_bindings; ++i) {
        struct pg_vr_action_binding* in_binding = &bindings[i];

        VRActionHandle_t vr_action_id;
        err = vr->input->GetActionHandle(in_binding->vr_action_name, &vr_action_id);
        if(err) {
            pg_log(PG_LOG_ERROR, "Failed to get VR ActionHandle '%s': error %d", in_binding->vr_action_name, err);
            ARR_POP(action_sets);
            return;
        }

        struct pg_vr_action_binding* new_binding = ARR_NEW(new_set->bindings);
        strncpy(new_binding->vr_action_name, in_binding->vr_action_name, 256);
        new_binding->control_idx = in_binding->control_idx;
        new_binding->type = in_binding->type;
        new_binding->vr_action_id = vr_action_id;
    }
}

void pg_vr_action_set_enable(char* name, bool enabled)
{
    for(int i = 0; i < action_sets.len; ++i) {
        if(strncmp(name, action_sets.data[i].vr_action_set_name, 256)) continue;
        action_sets.data[i].enabled = enabled;
        break;
    }
}

void pg_vr_poll_input(struct pg_input_wrapper* controls_out)
{
    if(!pg_have_vr() || pg_vr_is_fake()) return;
    struct pg_vr_context* vr = pg_vr_context();
    SARR_T(16, VRActiveActionSet_t) vr_active_action_sets = SARR_DATA(16, 0);
    int i;
    struct pg_vr_action_set* set;
    ARR_FOREACH_PTR(action_sets, set, i) {
        if(!set->enabled) continue;
        ARR_PUSH(vr_active_action_sets, (VRActiveActionSet_t){ .ulActionSet = set->vr_action_set_id });
    }
    EVRInputError err = vr->input->UpdateActionState(vr_active_action_sets.data,
            sizeof(VRActiveActionSet_t), vr_active_action_sets.len);
    if(err) {
        pg_log(PG_LOG_ERROR, "Failed to update VR action state: error %d", err);
        return;
    }

    ARR_FOREACH_PTR(action_sets, set, i) {
        if(!set->enabled) continue;
        struct pg_vr_action_binding* binding;
        int j;
        ARR_FOREACH_PTR(set->bindings, binding, i) {
            VRActionHandle_t vr_action = binding->vr_action_id;
            if(binding->type == PG_VR_INPUT_BOOLEAN) {
                InputDigitalActionData_t input_state;
                err = vr->input->GetDigitalActionData(binding->vr_action_id, &input_state, sizeof(input_state), k_ulInvalidInputValueHandle);
                if(err) {
                    pg_log(PG_LOG_ERROR, "Failed to get VR digital input '%s' (id %lu): error %d",
                            binding->vr_action_name, binding->vr_action_id, err);
                    return;
                }
                if(input_state.bState && input_state.bChanged) {
                    pg_input_boolean_action(controls_out, binding->control_idx, true);
                } else if(!input_state.bState && input_state.bChanged) {
                    pg_input_boolean_action(controls_out, binding->control_idx, false);
                }
            } else if(binding->type == PG_VR_INPUT_ANALOG) {
                InputAnalogActionData_t input_state;
                err = vr->input->GetAnalogActionData(vr_action, &input_state, sizeof(input_state), 0);
                if(err) {
                    pg_log(PG_LOG_ERROR, "Failed to get VR digital input '%s': error %d", binding->vr_action_name, err);
                    return;
                }
                pg_input_set_analog(controls_out, binding->control_idx, vec2(input_state.x, input_state.y));
            }
        }
    }
}

