#include "openvr_capi.h"
#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "openvr_helper.h"

typedef ARR_T(struct VREvent_t) vr_event_queue_t;
static vr_event_queue_t vr_event_queue = {};
static void vr_poll_events(void)
{
    struct pg_vr_context* vr = pg_vr_context();
    ARR_TRUNCATE(vr_event_queue, 0);
    struct VREvent_t e;
    while(vr->system->PollNextEvent(&e, sizeof(e))) {
        ARR_PUSH(vr_event_queue, e);
    };
}



static void track_controls(int hand)
{
    struct pg_vr_context* vr = pg_vr_context();
    struct pg_vr_device* device = pg_vr_get_device(pg_vr_get_controller_index(hand));
    if(!device || !device->controls) return;
    struct pg_input_wrapper* controls = device->controls;
    pg_input_wrapper_step(controls);
    int i;
    struct VREvent_t* event;
    ARR_FOREACH_PTR(vr_event_queue, event, i) {
        if(event->trackedDeviceIndex != device->hw_idx) continue;
        if(event->eventType == EVREventType_VREvent_ButtonPress) {
            pg_input_boolean_action(controls, event->data.controller.button, true);
            pg_log(PG_LOG_INFO, "Button %d pressed", event->data.controller.button);
        } else if(event->eventType == EVREventType_VREvent_ButtonUnpress) {
            pg_input_boolean_action(controls, event->data.controller.button, false);
            pg_log(PG_LOG_INFO, "Button %d released", event->data.controller.button);
        }
    }
}

void pg_vr_track_devices(void)
{
    if(!pg_have_vr() || pg_vr_is_fake()) return;
    struct pg_vr_context* vr = pg_vr_context();
    TrackedDevicePose_t tracked_poses_render[64] = {};
    TrackedDevicePose_t tracked_poses_game[64] = {};
    vr->compositor->WaitGetPoses(tracked_poses_render, k_unMaxTrackedDeviceCount, tracked_poses_game, k_unMaxTrackedDeviceCount);
    int i;
    struct pg_vr_device* device;
    pg_vr_device_set_t* devices = pg_vr_get_devices();
    ARR_FOREACH_PTR(*devices, device, i) {
        if(device->type == PG_VR_NO_DEVICE) continue;
        HmdMatrix34_t openvr_matrix = tracked_poses_render[i].mDeviceToAbsoluteTracking;
        device->tx =(mat4_from_openvr_mat34(&openvr_matrix));
        device->pg_tx = pg_gfx_transform_from_mat4(device->tx);
    }
    vr_poll_events();
    track_controls(0);
    track_controls(1);
}

struct pg_gfx_transform pg_vr_get_device_tx(int idx)
{
    struct pg_vr_device* device = &(pg_vr_get_devices()->data[idx]);
    if(!device) {
        return (struct pg_gfx_transform) {
            .pos = vec3(0,0,0), .scale = vec3(1,1,1), .rot = quat_identity(),
            .tx = mat4_identity(), .tx_inverse = mat4_identity() };
    }
    return device->pg_tx;
}

void pg_vr_set_fake_device_tx(int idx, vec3 pos, quat orientation)
{
    struct pg_vr_device* device = &(pg_vr_get_devices()->data[idx]);
    if(!device) return;
    device->tx = mat4_object_space(pos, vec3(1,1,1), orientation);
    device->pg_tx = pg_gfx_transform(pos, vec3(1,1,1), orientation);
}




