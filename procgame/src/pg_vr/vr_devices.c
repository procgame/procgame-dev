#include "openvr_capi.h"
#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "openvr_helper.h"

static pg_vr_device_set_t vr_devices;
pg_vr_device_set_t* pg_vr_get_devices(void) { return &vr_devices; } 

static int headset_device_idx = -1;
static int left_hand_device_idx = -1;
static int right_hand_device_idx = -1;

int pg_vr_get_hmd_index(void) { return headset_device_idx; }
int pg_vr_get_controller_index(int hand)
{
    return hand == 0 ? left_hand_device_idx : right_hand_device_idx;
}

struct pg_vr_device* pg_vr_get_device_hmd(void)
{
    if(headset_device_idx >= 0) return &vr_devices.data[headset_device_idx];
    else return NULL;
}

struct pg_vr_device* pg_vr_get_device_controller(int idx)
{
    if(idx == 0 && left_hand_device_idx >= 0) return &vr_devices.data[left_hand_device_idx];
    else if(idx == 1 && right_hand_device_idx >= 0) return &vr_devices.data[right_hand_device_idx];
    else return NULL;
}

struct pg_vr_device* pg_vr_get_device(int idx)
{
    if(idx < 0 || idx >= PG_VR_MAX_DEVICES) return NULL;
    return &vr_devices.data[idx];
}

static void detect_fake_devices(void);
static void vr_fill_device_list(void);
static bool vr_search_hmd(void);
static bool vr_search_controllers(void);
bool pg_vr_devices_init(void)
{
    SARR_INIT(vr_devices);
    if(!pg_have_vr()) return false;
    if(pg_vr_is_fake()) {
        detect_fake_devices();
        return true;
    }
    vr_fill_device_list();
    if(!vr_search_hmd()
    || !vr_search_controllers()) {
        return false;
    }
    return true;
}

void pg_vr_devices_deinit(void)
{
    int i;
    struct pg_vr_device* device;
    ARR_FOREACH_PTR(vr_devices, device, i) {
        if(device->controls) free(device->controls);
    }
}



/*  Static functions    */

static void detect_fake_devices(void)
{
    ARR_PUSH(vr_devices, (struct pg_vr_device){
        .hw_idx = 0, .set_idx = 0, .type = PG_VR_HEADSET,
        .mfg_name = "Fake & Co.", .model_number = "Fake HMD", .serial_number = "420" });
    ARR_PUSH(vr_devices, (struct pg_vr_device){
        .hw_idx = 1, .set_idx = 1, .type = PG_VR_CONTROLLER,
        .controls = calloc(1, sizeof(struct pg_input_wrapper)),
        .mfg_name = "Fake & Co.", .model_number = "Fake Controller", .serial_number = "69" });
    ARR_PUSH(vr_devices, (struct pg_vr_device){
        .hw_idx = 2, .set_idx = 2, .type = PG_VR_CONTROLLER,
        .controls = calloc(1, sizeof(struct pg_input_wrapper)),
        .mfg_name = "Fake & Co.", .model_number = "Fake Controller", .serial_number = "69" });
    headset_device_idx = 0;
    left_hand_device_idx = 1;
    right_hand_device_idx = 2;
}

/*  Detect all the hardware devices registered with OpenVR and fill the list of hardware devices    */
static void vr_fill_device_list(void)
{
    struct pg_vr_context* vr = pg_vr_context();

    /*  First, detect a flat list of devices  */
    ETrackedDeviceClass d_class;
    for(int d_idx = 0; d_idx < k_unMaxTrackedDeviceCount; ++d_idx) {
        if(!vr->system->IsTrackedDeviceConnected(d_idx)) continue;
        ETrackedDeviceClass d_class = vr->system->GetTrackedDeviceClass(d_idx);
        enum pg_vr_device_type d_type_pg;

        /*  Check if it is a known device type  */
        if(d_class == ETrackedDeviceClass_TrackedDeviceClass_HMD) {
            d_type_pg = PG_VR_HEADSET;
        } else if(d_class = ETrackedDeviceClass_TrackedDeviceClass_Controller) {
            d_type_pg = PG_VR_CONTROLLER;
        } else if(d_class = ETrackedDeviceClass_TrackedDeviceClass_TrackingReference) {
            d_type_pg = PG_VR_BASE_STATION;
        } else {
            continue;
        }

        /*  Add a new device to the set and query more hardware info   */
        int pg_idx = vr_devices.len;
        struct pg_vr_device* new_device = ARR_NEW(vr_devices);
        new_device->type = d_type_pg;
        new_device->hw_idx = d_idx;
        new_device->set_idx = pg_idx;
        vr->system->GetStringTrackedDeviceProperty(d_idx, ETrackedDeviceProperty_Prop_ModelNumber_String,
                new_device->model_number, PG_VR_DEVICE_STRING_MAXLEN, NULL);
        vr->system->GetStringTrackedDeviceProperty(d_idx, ETrackedDeviceProperty_Prop_SerialNumber_String,
                new_device->serial_number, PG_VR_DEVICE_STRING_MAXLEN, NULL);
        vr->system->GetStringTrackedDeviceProperty(d_idx, ETrackedDeviceProperty_Prop_ManufacturerName_String,
                new_device->mfg_name, PG_VR_DEVICE_STRING_MAXLEN, NULL);

        /*  Log detected hardware info  */
        const char* d_class_str = (d_type_pg == PG_VR_HEADSET ? "HMD" :
                (d_type_pg == PG_VR_CONTROLLER ? "Controller" : "Base station"));
        pg_log(PG_LOG_INFO, "Detected VR device index %d (procgame id %d):", d_idx, pg_idx);
        pg_log(PG_LOG_CONTD, "    Device class: %s", d_class_str);
        pg_log(PG_LOG_CONTD, "    Manufacturer: %s", new_device->mfg_name);
        pg_log(PG_LOG_CONTD, "    Model no.: %s", new_device->model_number);
        pg_log(PG_LOG_CONTD, "    Serial no.: %s", new_device->serial_number);
    }
}


/*  Search the device list for an HMD and initialize HMD-specific data  */
static bool vr_search_hmd(void)
{
    int hmd_idx;
    struct pg_vr_device* hmd_device;
    ARR_FOREACH_PTR(vr_devices, hmd_device, hmd_idx) {
        if(hmd_device->type == PG_VR_HEADSET) break;
    }
    if(hmd_idx == vr_devices.len) {
        pg_log(PG_LOG_ERROR, "VR Headset missing from hardware device list");
        return false;
    }
    headset_device_idx = hmd_idx;
    return true;
}

/*  Search the device list for controllers and initialize controller-specific data  */
static bool vr_search_controllers(void)
{
    struct pg_vr_context* vr = pg_vr_context();
    struct pg_vr_device* d_left_hand = NULL;
    struct pg_vr_device* d_right_hand = NULL;

    pg_log(PG_LOG_INFO, "wtf");

    int i;
    struct pg_vr_device* d_ptr;
    ARR_FOREACH_PTR(vr_devices, d_ptr, i) {
        if(d_ptr->type != PG_VR_CONTROLLER) continue;
        int d_idx = d_ptr->hw_idx;
        d_ptr->controls = calloc(1, sizeof(struct pg_input_wrapper));
        ETrackedControllerRole role = vr->system->GetControllerRoleForTrackedDeviceIndex(d_idx);
        if(role == ETrackedControllerRole_TrackedControllerRole_LeftHand) d_left_hand = d_ptr;
        else if(role == ETrackedControllerRole_TrackedControllerRole_RightHand) d_right_hand = d_ptr;
    }

    if(d_left_hand) {
        left_hand_device_idx = d_left_hand->set_idx;
        pg_log(PG_LOG_INFO, "foo");
    }
    if(d_right_hand) {
        right_hand_device_idx = d_right_hand->set_idx;
        pg_log(PG_LOG_INFO, "bar");
    }

    return true;
}





