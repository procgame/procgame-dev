#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

/************************************/
/*  GPU SHADER STAGES               */
/************************************/
/*  Actually compiled shader stages */

struct pg_gpu_shader_stage {
    char* src;
    int src_len;
    GLuint gl;
    GLint gl_status;
    char* gl_log;
    int gl_log_len;
};

/*  OpenGL public interface */
GLuint pg_gpu_shader_stage_get_handle(pg_gpu_shader_stage_t* sh_stage)
{
    if(!sh_stage) return 0;
    return sh_stage->gl;
}




GLenum shader_stage_type_to_gl[] = {
    [PG_GPU_VERTEX_SHADER] = GL_VERTEX_SHADER,
    [PG_GPU_FRAGMENT_SHADER] = GL_FRAGMENT_SHADER,
};

struct pg_gpu_shader_stage* pg_shader_source_compile(struct pg_shader_source* sh_source)
{
    if(!sh_source) return NULL;
    struct pg_gpu_shader_stage* sh_stage = malloc(sizeof(*sh_stage));
    sh_stage->src = calloc(sh_source->src_len + 1, sizeof(*sh_stage->src));
    sh_stage->src_len = pg_text_variator_generate(&sh_source->variator,
            sh_stage->src, sh_source->src_len + 1, NULL, 0);
    PG_CHECK_GL(sh_stage->gl = glCreateShader(shader_stage_type_to_gl[sh_source->type]));
    PG_CHECK_GL(glShaderSource(sh_stage->gl, 1, (const char**)&sh_source->src, NULL));
    PG_CHECK_GL(glCompileShader(sh_stage->gl));
    PG_CHECK_GL(glGetShaderiv(sh_stage->gl, GL_COMPILE_STATUS, &sh_stage->gl_status));
    PG_CHECK_GL(glGetShaderiv(sh_stage->gl, GL_INFO_LOG_LENGTH, &sh_stage->gl_log_len));
    sh_stage->gl_log = calloc(sh_stage->gl_log_len + 1, sizeof(char));
    PG_CHECK_GL(glGetShaderInfoLog(sh_stage->gl, sh_stage->gl_log_len + 1, NULL, sh_stage->gl_log));
    return sh_stage;
}

pg_gpu_shader_stage_t* pg_shader_source_compile_variant(
        struct pg_shader_source* sh_source, const char** variants, int n_variants)
{
    if(!sh_source) return NULL;
    struct pg_gpu_shader_stage* sh_stage = malloc(sizeof(*sh_stage));
    sh_stage->src = calloc(sh_source->src_len + 1, sizeof(*sh_stage->src));
    sh_stage->src_len = pg_text_variator_generate(&sh_source->variator,
            sh_stage->src, sh_source->src_len + 1, NULL, 0);
    PG_CHECK_GL(sh_stage->gl = glCreateShader(shader_stage_type_to_gl[sh_source->type]));
    PG_CHECK_GL(glShaderSource(sh_stage->gl, 1, (const char**)&sh_source->src, NULL));
    PG_CHECK_GL(glCompileShader(sh_stage->gl));
    PG_CHECK_GL(glGetShaderiv(sh_stage->gl, GL_COMPILE_STATUS, &sh_stage->gl_status));
    PG_CHECK_GL(glGetShaderiv(sh_stage->gl, GL_INFO_LOG_LENGTH, &sh_stage->gl_log_len));
    sh_stage->gl_log = calloc(sh_stage->gl_log_len + 1, sizeof(char));
    PG_CHECK_GL(glGetShaderInfoLog(sh_stage->gl, sh_stage->gl_log_len + 1, NULL, sh_stage->gl_log));
    return sh_stage;
}

void pg_gpu_shader_stage_destroy(pg_gpu_shader_stage_t* sh_stage)
{
    PG_CHECK_GL(glDeleteShader(sh_stage->gl));
    free(sh_stage->gl_log);
    free(sh_stage->src);
    free(sh_stage);
}

bool pg_gpu_shader_stage_status(pg_gpu_shader_stage_t* sh_stage)
{
    return sh_stage->gl_status == GL_TRUE;
}

const char* pg_gpu_shader_stage_log(pg_gpu_shader_stage_t* sh_stage)
{
    return sh_stage->gl_log;
}

int pg_gpu_shader_stage_log_len(pg_gpu_shader_stage_t* sh_stage)
{
    return sh_stage->gl_log_len;
}


