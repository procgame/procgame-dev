#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

struct pg_gpu_resource_set {
    struct pg_gpu_resource_texture_binding {
        bool is_buffer;
        union { 
            struct {
                pg_gpu_texture_t* texture;
                struct pg_gpu_texture_params params;
            };
            pg_gpu_buffer_t* buffer;
        };
    } texture_bindings[16];
    pg_gpu_buffer_t* uniform_buffer_bindings[16];
};

static void pg_gpu_resource_set_init(pg_gpu_resource_set_t* set)
{
    *set = (pg_gpu_resource_set_t){};
}

static void pg_gpu_resource_set_deinit(pg_gpu_resource_set_t* set)
{
    *set = (pg_gpu_resource_set_t){};
}

pg_gpu_resource_set_t* pg_gpu_resource_set_create(void)
{
    pg_gpu_resource_set_t* set = malloc(sizeof(*set));
    pg_gpu_resource_set_init(set);
    return set;
}

void pg_gpu_resource_set_destroy(pg_gpu_resource_set_t* set)
{
    pg_gpu_resource_set_deinit(set);
    free(set);
}



void pg_gpu_resource_set_attach_buffer(pg_gpu_resource_set_t* set,
        int binding_idx, pg_gpu_buffer_t* gpu_buf)
{
    set->uniform_buffer_bindings[binding_idx] = gpu_buf;
}

void pg_gpu_resource_set_attach_texture_buffer(pg_gpu_resource_set_t* set,
        int texture_idx, pg_gpu_buffer_t* gpu_buf)
{
    set->texture_bindings[texture_idx].is_buffer = true;
    set->texture_bindings[texture_idx].buffer = gpu_buf;

}

void pg_gpu_resource_set_attach_texture(pg_gpu_resource_set_t* set,
        int texture_idx, pg_gpu_texture_t* texture)
{
    set->texture_bindings[texture_idx].is_buffer = false;
    set->texture_bindings[texture_idx].texture = texture;
    set->texture_bindings[texture_idx].params = PG_GPU_TEXTURE_PARAMS();
}

void pg_gpu_resource_set_attach_texture_params(pg_gpu_resource_set_t* set,
        int texture_idx, struct pg_gpu_texture_params* params)
{
    if(set->texture_bindings[texture_idx].is_buffer) {
        pg_log(PG_LOG_ERROR, "Attaching texture parameters to binding with buffer (not texture) attached");
        return;
    }
    set->texture_bindings[texture_idx].params = *params;
}

pg_gpu_texture_t* pg_gpu_resource_set_get_texture(const pg_gpu_resource_set_t* set,
        int texture_idx)
{
    if(set->texture_bindings[texture_idx].is_buffer) return NULL;
    return set->texture_bindings[texture_idx].texture;
}

pg_gpu_buffer_t* pg_gpu_resource_set_get_texture_buffer(const pg_gpu_resource_set_t* set,
        int texture_idx)
{
    if(!set->texture_bindings[texture_idx].is_buffer) return NULL;
    return set->texture_bindings[texture_idx].buffer;
}

pg_gpu_buffer_t* pg_gpu_resource_set_get_buffer(const pg_gpu_resource_set_t* set,
        int binding_idx)
{
    return set->uniform_buffer_bindings[binding_idx];
}


void pg_gpu_resource_set_bind(pg_gpu_resource_set_t* set)
{
    for(int i = 0; i < 16; ++i) {
        if(set->texture_bindings[i].is_buffer && set->texture_bindings[i].buffer) {
            GLuint gl_tex = pg_gpu_buffer_get_tex_handle(set->texture_bindings[i].buffer);
            PG_CHECK_GL(glActiveTexture(GL_TEXTURE0 + i));
            PG_CHECK_GL(glBindTexture(GL_TEXTURE_BUFFER, gl_tex));
        } else if(set->texture_bindings[i].texture) {
            GLuint gl_tex = pg_gpu_texture_get_handle(set->texture_bindings[i].texture);
            PG_CHECK_GL(glActiveTexture(GL_TEXTURE0 + i));
            PG_CHECK_GL(glBindTexture(pg_gpu_texture_get_binding_point(set->texture_bindings[i].texture), gl_tex));
            pg_gpu_texture_params_apply(&set->texture_bindings[i].params, set->texture_bindings[i].texture);
        } else {
            continue;
        }
    }

    for(int i = 0; i < 16; ++i) {
        if(set->uniform_buffer_bindings[i]) {
            GLuint buffer_handle = pg_gpu_buffer_get_handle(set->uniform_buffer_bindings[i]);
            PG_CHECK_GL(glBindBufferBase(GL_UNIFORM_BUFFER, i, buffer_handle));
        }
    }
}

