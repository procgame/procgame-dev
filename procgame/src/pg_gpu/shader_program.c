#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

/************************************/
/*  GPU SHADER PROGRAMS             */
/************************************/
/*  These structures represent actual input state for a shader program, so
    the buffers/textures attached here represent actually uploaded GPU data
    which is ready to be used for rendering. */

struct pg_gpu_shader_program {
    int n_stages;
    pg_gpu_shader_stage_t* stages[PG_GPU_SHADER_MAX_SHADERS];
    struct pg_shader_interface_info iface;
    GLuint gl;
    GLint gl_status;
    char* gl_log;
    int gl_log_len;
};

/*  OpenGL public interface */
GLuint pg_gpu_shader_program_get_handle(pg_gpu_shader_program_t* sh_prog)
{
    if(!sh_prog) return 0;
    return sh_prog->gl;
}


/*  Procgame public interface   */

void pg_gpu_shader_program_print_debug(enum pg_log_type log_level, pg_gpu_shader_program_t* sh_prog)
{
    /********************/
    /*  Compilation log */
    if(sh_prog->gl_log_len > 0) {
        pg_log(PG_LOG_CONTD, "  OpenGL shader program info log:\n%s", sh_prog->gl_log);
    } else {
        pg_log(PG_LOG_CONTD, "  OpenGL shader program info log: (empty)");
    }

    if(!sh_prog->gl_status) return;

    /********************/
    /*  Uniform inputs  */
    int simple_uniform_count, uniform_block_count;
    PG_CHECK_GL(glGetProgramInterfaceiv(sh_prog->gl, GL_UNIFORM, GL_ACTIVE_RESOURCES, &simple_uniform_count));
    PG_CHECK_GL(glGetProgramInterfaceiv(sh_prog->gl, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &uniform_block_count));

    pg_log(log_level, "  OpenGL shader program inputs:");

    /*  Retrieve all the simple uniforms    */
    pg_log(PG_LOG_CONTD, "    Uniform inputs:");
    for(int i = 0; i < simple_uniform_count; i++)
    {
        /*  Uniform information from GL */
        GLint uni_location, uni_array_size;
        GLenum uni_type;
        GLchar uni_name[PG_SHADER_INPUT_NAME_MAXLEN];
        GLsizei uni_name_length;
        PG_CHECK_GL(glGetActiveUniform(sh_prog->gl, (GLuint)i,
                        PG_SHADER_INPUT_NAME_MAXLEN, &uni_name_length,
                        &uni_array_size, &uni_type, uni_name));
        PG_CHECK_GL(uni_location = glGetUniformLocation(sh_prog->gl, uni_name));
        /*  Ignore internal GL uniforms */
        /*  Build the actual uniform input  */
        const char* uni_type_string = pg_gl_enum_string(uni_type);
        pg_log(PG_LOG_CONTD, "      Name: \"%s\", Type: %s, Location: %d, Count: %d", uni_name, uni_type_string, uni_location, uni_array_size);
    }

    /*  Retrieve all the uniform blocks */
    for(int i = 0; i < uniform_block_count; ++i) {
        /*  Uniform information from GL */
        GLchar uni_name[PG_SHADER_INPUT_NAME_MAXLEN];
        GLsizei uni_name_length;
        GLint uni_block_size;
        GLuint uni_location;
        PG_CHECK_GL(glGetActiveUniformBlockName(sh_prog->gl, (GLuint)i, PG_SHADER_INPUT_NAME_MAXLEN, &uni_name_length, uni_name));
        PG_CHECK_GL(glGetActiveUniformBlockiv(sh_prog->gl, (GLuint)i, GL_UNIFORM_BLOCK_DATA_SIZE, &uni_block_size));
        PG_CHECK_GL(uni_location = glGetUniformBlockIndex(sh_prog->gl, uni_name));
        /*  Build the uniform info  */
        pg_log(PG_LOG_CONTD, "      Name: \"%s\", Type: UBO, Location: %d", uni_name, uni_location);
    }

    /********************/
    /*  Vertex inputs   */
    int attrib_count = 0;
    PG_CHECK_GL(glGetProgramiv(sh_prog->gl, GL_ACTIVE_ATTRIBUTES, &attrib_count));
    /*  Now iterate over every vertex input using OpenGL's shader reflection API */
    pg_log(PG_LOG_CONTD, "    Vertex inputs:");
    for(int i = 0; i < attrib_count; ++i) {
        /*  Vertex info from GL */
        GLchar vertex_input_name[PG_SHADER_INPUT_NAME_MAXLEN];
        GLsizei vertex_input_name_len;
        GLenum vertex_input_type;
        GLint vertex_input_type_size, vertex_input_location;
        PG_CHECK_GL(glGetActiveAttrib(sh_prog->gl, i, PG_SHADER_INPUT_NAME_MAXLEN,
                        &vertex_input_name_len, &vertex_input_type_size,
                        &vertex_input_type, vertex_input_name));
        PG_CHECK_GL(vertex_input_location = glGetAttribLocation(sh_prog->gl, vertex_input_name));
        /*  Log the result  */
        const char* vertex_type_string = pg_gl_enum_string(vertex_input_type);
        pg_log(PG_LOG_CONTD, "      Name: \"%s\", Type: %s, Location: %d",
            vertex_input_name, vertex_type_string, vertex_input_location);
    }
}

pg_gpu_shader_program_t* pg_gpu_shader_program_link(pg_gpu_shader_stage_t** sh_stages, int n_stages)
{
    if(n_stages > PG_GPU_SHADER_MAX_SHADERS) return NULL;
    for(int i = 0; i < n_stages; ++i) {
        if(!sh_stages[i]) return NULL;
    }
    struct pg_gpu_shader_program* sh_prog = malloc(sizeof(*sh_prog));
    PG_CHECK_GL(sh_prog->gl = glCreateProgram());
    sh_prog->n_stages = n_stages;
    memcpy(sh_prog->stages, sh_stages, sizeof(pg_gpu_shader_stage_t*) * n_stages);
    for(int i = 0; i < n_stages; ++i) {
        PG_CHECK_GL(glAttachShader(sh_prog->gl, pg_gpu_shader_stage_get_handle(sh_stages[i])));
    }
    PG_CHECK_GL(glLinkProgram(sh_prog->gl));
    PG_CHECK_GL(glGetProgramiv(sh_prog->gl, GL_LINK_STATUS, &sh_prog->gl_status));
    PG_CHECK_GL(glGetProgramiv(sh_prog->gl, GL_INFO_LOG_LENGTH, &sh_prog->gl_log_len));
    sh_prog->gl_log = calloc(sh_prog->gl_log_len + 1, sizeof(char));
    PG_CHECK_GL(glGetProgramInfoLog(sh_prog->gl, sh_prog->gl_log_len + 1, NULL, sh_prog->gl_log));
    if(!sh_prog->gl_status) {
        pg_log(PG_LOG_ERROR, "OpenGL shader program failed to link. Printing debug info.");
        pg_gpu_shader_program_print_debug(PG_LOG_CONTD, sh_prog);
        pg_gpu_shader_program_destroy(sh_prog);
        return NULL;
    } else if(sh_prog->gl_log_len) {
        pg_log(PG_LOG_INFO, "OpenGL shader program generated an info log. Printing debug info.");
        pg_gpu_shader_program_print_debug(PG_LOG_CONTD, sh_prog);
    }
    return sh_prog;
}

pg_gpu_shader_program_t* pg_gpu_shader_program_init(pg_gpu_shader_stage_t** sh_stages, int n_stages,
        const struct pg_shader_interface_info* iface_info)
{
    if(!iface_info || !sh_stages || n_stages <= 0) return NULL;
    pg_gpu_shader_program_t* sh_prog = pg_gpu_shader_program_link(sh_stages, n_stages);
    if(!sh_prog) return NULL;
    sh_prog->iface = *iface_info;

    /*  Gather up all the UBO locations because GL doesn't let us specify it in the shader source code  */
    int i;
    const struct pg_shader_uniform_info* uni_info_ptr;
    ARR_FOREACH_PTR(iface_info->uniform_inputs, uni_info_ptr, i) {
        if(uni_info_ptr->type == PG_SHADER_UNIFORM_BUFFER) {
            GLuint ubo_index = 0;
            PG_CHECK_GL(glGetUniformBlockIndex(sh_prog->gl, uni_info_ptr->name));
            sh_prog->iface.uniform_inputs.data[i].location = ubo_index;
        }
    }

    return sh_prog;
}

void pg_gpu_shader_program_destroy(pg_gpu_shader_program_t* sh_prog)
{
    if(!sh_prog) return;
    PG_CHECK_GL(glDeleteProgram(sh_prog->gl));
    free(sh_prog->gl_log);
    free(sh_prog);
}

pg_gpu_shader_stage_t* pg_gpu_shader_program_get_stage(pg_gpu_shader_program_t* sh_prog,
                                                       enum pg_gpu_shader_stage_type stage)
{
    if(stage < 0 || stage >= PG_GPU_SHADER_TYPES) return NULL;
    return sh_prog->stages[stage];
}

bool pg_gpu_shader_program_status(const pg_gpu_shader_program_t* sh_prog)
{
    if(!sh_prog) return false;
    return sh_prog->gl_status;
}

const struct pg_shader_interface_info* pg_gpu_shader_program_interface_info(const pg_gpu_shader_program_t* sh_prog)
{
    return &sh_prog->iface;
}

void pg_gpu_shader_program_use(pg_gpu_shader_program_t* sh_prog)
{
    if(!sh_prog || !sh_prog->gl_status) return;
    PG_CHECK_GL(glUseProgram(sh_prog->gl));
}

