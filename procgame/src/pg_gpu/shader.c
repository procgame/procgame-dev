#include <string.h>

#include "pg_core/inline.h"
#include "pg_core/data_type.h"
#include "pg_core/arr.h"
#include "pg_core/logging.h"
#include "pg_util/json_utility.h"
#include "pg_util/text_utility.h"
#include "pg_util/file_utility.h"
#include "pg_util/asset.h"
#include "pg_gpu/opengl.h"
#include "pg_gpu/buffer.h"
#include "pg_gpu/texture.h"
#include "pg_gpu/framebuffer.h"
#include "pg_gpu/shader.h"
#include "pg_gpu/resource_set.h"

/********************************/
/*  GPU SHADER PROGRAM ASSETS   */
/********************************/

struct shader_asset_source {
    struct pg_shader_source vertex_shader_source, fragment_shader_source;
    struct pg_shader_interface_info iface_info;
};


static void* gpu_shader_program_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* loader, cJSON* json)
{
    int n_search_paths;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_handle.mgr, &n_search_paths);

    cJSON* vertex_shader_json, *fragment_shader_json, *outputs_json,
        *vertex_inputs_json, *uniform_inputs_json;

    bool correct_layout = pg_asset_require_json_layout(asset_handle, NULL, json, 5,
        PG_POINTERS(const char*, "stages.vertex_shader", "stages.fragment_shader", "outputs",     "inputs.vertex",     "inputs.uniforms"),
        PG_POINTERS(cJSON_Type,  cJSON_String,           cJSON_String,             cJSON_Object,   cJSON_Object,        cJSON_Object),
        PG_POINTERS(cJSON**,     &vertex_shader_json,    &fragment_shader_json,    &outputs_json, &vertex_inputs_json, &uniform_inputs_json));
    if(!correct_layout) return NULL;

    struct shader_asset_source parsed_source = {0};
    pg_shader_interface_info_init(&parsed_source.iface_info);

    int iface_idx = 0;
    char iface_idx_string[32];

    /********************/
    /*  Vertex inputs   */
    cJSON* vertex_json_iter;
    cJSON_ArrayForEach(vertex_json_iter, vertex_inputs_json) {
        snprintf(iface_idx_string, 32, "vertex input %d", iface_idx++);
        /*  Validate    */
        const char* input_name = vertex_json_iter->string;
        cJSON* type_json, *location_json;
        if(!pg_asset_require_json_layout(asset_handle, iface_idx_string, vertex_json_iter, 2,
                PG_POINTERS(const char*, "type",       "location"),
                PG_POINTERS(cJSON_Type,  cJSON_String, cJSON_Number),
                PG_POINTERS(cJSON**,     &type_json,   &location_json))) {
            return NULL;
        }
        enum pg_data_type vertex_input_type = pg_data_type_from_string(type_json->valuestring);
        if(!vertex_input_type) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "%s has invalid type", iface_idx_string);
            return NULL;
        }
        /*  Add the input description   */
        pg_shader_interface_info_add_vertex_input(&parsed_source.iface_info,
                input_name, location_json->valueint, vertex_input_type);
    }

    /********************/
    /*  Uniform inputs  */
    iface_idx = 0;
    cJSON* uniform_json_iter;
    cJSON_ArrayForEach(uniform_json_iter, uniform_inputs_json) {
        snprintf(iface_idx_string, 32, "uniform input %d", iface_idx++);
        /*  Validate    */
        const char* uniform_name = uniform_json_iter->string;
        cJSON* type_json, *location_json, *binding_json, *count_json, *simple_type_json, *buffer_binding_json, *layout_json, *alignment_json, *packing_json;
        if(!pg_asset_require_json_layout(asset_handle, iface_idx_string, uniform_json_iter, 9,
                PG_POINTERS(const char*, "type",       "?location",    "?binding",    "?count",     "?simple_type",    "?layout",     "?attribute_packing", "?element_alignment"),
                PG_POINTERS(cJSON_Type,  cJSON_String, cJSON_Number,   cJSON_Number,  cJSON_Number, cJSON_String,      cJSON_Object, cJSON_String,         cJSON_Number),
                PG_POINTERS(cJSON**,     &type_json,   &location_json, &binding_json, &count_json,  &simple_type_json, &layout_json, &packing_json,        &alignment_json))) {
            return NULL;
        }
        int location = location_json ? location_json->valueint : -1;
        int count = count_json ? count_json->valueint : 1;
        enum pg_shader_uniform_type type = pg_shader_uniform_type_from_string(type_json->valuestring, strlen(type_json->valuestring));
        if(!type) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "%s has invalid type", iface_idx_string);
            return NULL;
        }

        /*  Fill out specific details based on the type */
        if(type == PG_SHADER_UNIFORM_SIMPLE) {
            /*  Validate    */
            if(!simple_type_json) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "%s 'simple' uniform type requires the 'simple_type' member be provided", iface_idx_string);
                return NULL;
            }
            enum pg_data_type simple_type = pg_data_type_from_string(cJSON_GetStringValue(simple_type_json));
            /*  Add the uniform description */
            pg_shader_interface_info_add_simple_uniform_input(&parsed_source.iface_info, 
                    uniform_name, location, simple_type);
            continue;
        } else if(type == PG_SHADER_UNIFORM_BUFFER) {
            /*  Validate    */
            if(!layout_json || !alignment_json || !packing_json) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "%s 'buffer' uniform type requires the 'layout', 'attribute_packing', and 'element_alignment' members be provided", iface_idx_string);
                return NULL;
            }
            /*  Parse layout    */
            struct pg_buffer_layout buf_layout;
            pg_buffer_layout_from_json(&buf_layout, uniform_json_iter);
            /*  Add the uniform description */
            pg_shader_interface_info_add_buffer_uniform_input(&parsed_source.iface_info,
                    uniform_name, location, &buf_layout);
        } else if(type == PG_SHADER_UNIFORM_TEXTURE || type == PG_SHADER_UNIFORM_TEXTURE_BUFFER) {
            /*  No special validation for textures (todo), just add the uniform description */
            pg_shader_interface_info_add_texture_uniform_input(&parsed_source.iface_info,
                    uniform_name, location, type == PG_SHADER_UNIFORM_TEXTURE_BUFFER);
        }
    }

    /********************/
    /*  SHADER OUTPUTS  */
    iface_idx = 0;
    cJSON* output_json_iter;
    cJSON_ArrayForEach(output_json_iter, outputs_json) {
        snprintf(iface_idx_string, 32, "output %d", iface_idx++);
        /*  Validate    */
        cJSON* type_json, *attachment_json;
        if(!pg_asset_require_json_layout(asset_handle, iface_idx_string, output_json_iter, 2,
                PG_POINTERS(const char*, "type",       "attachment"),
                PG_POINTERS(cJSON_Type,  cJSON_String, cJSON_String),
                PG_POINTERS(cJSON**,     &type_json,   &attachment_json))) {
            return NULL;
        }
        enum pg_data_type output_type = pg_data_type_from_string(type_json->valuestring);
        if(!output_type) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "%s has invalid type", iface_idx_string);
            return NULL;
        }

        enum pg_gpu_framebuffer_attachment_index attachment =
            pg_gpu_framebuffer_attachment_index_from_string(attachment_json->valuestring,
                    PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN);
        if(!attachment) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "%s has invalid attachment", iface_idx_string);
            return NULL;
        }

        /*  Add the output description  */
        pg_shader_interface_info_add_output(&parsed_source.iface_info,
                output_json_iter->string, attachment, output_type);
    }

    /************************/
    /*  SHADER SOURCE CODE  */
    const char* vs_filename = cJSON_GetStringValue(vertex_shader_json);
    const char* fs_filename = cJSON_GetStringValue(fragment_shader_json);
    struct pg_file vertex_shader_file = {}, fragment_shader_file = {};
    bool r = pg_file_open_search_paths(&vertex_shader_file, search_paths, n_search_paths, vs_filename, "r")
          && pg_file_open_search_paths(&fragment_shader_file, search_paths, n_search_paths, fs_filename, "r")
          && pg_shader_source_from_file_with_includes(&parsed_source.vertex_shader_source, PG_GPU_VERTEX_SHADER,
                                                      &vertex_shader_file, search_paths, n_search_paths)
          && pg_shader_source_from_file_with_includes(&parsed_source.fragment_shader_source, PG_GPU_FRAGMENT_SHADER,
                                                      &fragment_shader_file, search_paths, n_search_paths);
    if(!r) {
        if(vertex_shader_file.f) pg_file_close(&vertex_shader_file);
        if(fragment_shader_file.f) pg_file_close(&fragment_shader_file);
        pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to load GLSL sources");
        return NULL;
    }

    /*  If it all went well then we actually do the heap allocation and copy the result to it   */
    struct shader_asset_source* output_parsed = malloc(sizeof(*output_parsed));
    *output_parsed = parsed_source;
    return output_parsed;
}

static void gpu_shader_program_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    free(parsed_source_ptr);
}

static void* gpu_shader_program_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    struct shader_asset_source* parsed_source = (struct shader_asset_source*)parsed_source_ptr;

    /*  Compile GLSL source */
    pg_gpu_shader_stage_t* gpu_vertex_shader = pg_shader_source_compile(&parsed_source->vertex_shader_source);
    pg_gpu_shader_stage_t* gpu_fragment_shader = pg_shader_source_compile(&parsed_source->fragment_shader_source);
    if(!gpu_vertex_shader || !gpu_fragment_shader) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to compile GLSL sources");
        if(gpu_vertex_shader) pg_gpu_shader_stage_destroy(gpu_vertex_shader);
        if(gpu_fragment_shader) pg_gpu_shader_stage_destroy(gpu_fragment_shader);
        return NULL;
    }

    /*  Construct the final shader program object from the compiled sources
        and the parsed interface info   */
    pg_gpu_shader_program_t* sh_prog = pg_gpu_shader_program_init(
            (pg_gpu_shader_stage_t*[]){ gpu_vertex_shader, gpu_fragment_shader }, 2,
            &parsed_source->iface_info);
    return sh_prog;
}

static void gpu_shader_program_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_shader_program_destroy((pg_gpu_shader_program_t*)loaded_data);
}


struct pg_asset_loader* pg_gpu_shader_program_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_shader_program",
        .type_name_len = strlen("pg_gpu_shader_program"),
        .parse = gpu_shader_program_asset_parse,
        .unparse = gpu_shader_program_asset_unparse,
        .load = gpu_shader_program_asset_load,
        .unload = gpu_shader_program_asset_unload,
    };
    return loader;
}

