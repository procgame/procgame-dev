static const struct pg_gpu_texture_format_info {
    const char* string;
    GLenum gl_type;
    GLenum gl_format;
    GLenum gl_pixel_layout;
    int n_channels;
    bool normalized;
    bool supports_buftex;
    bool supports_framebuf;
    enum pg_data_type pg_format;
} gl_format_info[PG_GPU_TEXTURE_N_FORMATS] = {
    /*                                String                                OpenGL format               Channels      Supports buffer textures   Procgame type
                                                      OpenGL base type                       OpenGL layout   Normalized        Supports framebuffers        */
    [PG_GPU_TEXTURE_R8_UNORM] =     { "R8",           GL_UNSIGNED_BYTE,     GL_R8,           GL_RED,    1,   true,    true,    true,             PG_UBYTE,       },
    [PG_GPU_TEXTURE_R16_UNORM] =    { "R16",          GL_UNSIGNED_SHORT,    GL_R16,          GL_RED,    1,   true,    true,    true,             PG_USHORT,      },
    [PG_GPU_TEXTURE_R8_SNORM] =     { "R8_SNORM",     GL_BYTE,              GL_R8_SNORM,     GL_RED,    1,   true,    false,   false,            PG_BYTE,        },
    [PG_GPU_TEXTURE_R16_SNORM] =    { "R16_SNORM",    GL_SHORT,             GL_R16_SNORM,    GL_RED,    1,   true,    false,   false,            PG_SHORT,       },
    [PG_GPU_TEXTURE_R8UI] =         { "R8UI",         GL_UNSIGNED_BYTE,     GL_R8UI,         GL_RED,    1,   false,   true,    true,             PG_UBYTE,       },
    [PG_GPU_TEXTURE_R16UI] =        { "R16UI",        GL_UNSIGNED_SHORT,    GL_R16UI,        GL_RED,    1,   false,   true,    true,             PG_USHORT,      },
    [PG_GPU_TEXTURE_R32UI] =        { "R32UI",        GL_UNSIGNED_INT,      GL_R32UI,        GL_RED,    1,   false,   true,    true,             PG_UINT,        },
    [PG_GPU_TEXTURE_R8I] =          { "R8I",          GL_BYTE,              GL_R8I,          GL_RED,    1,   false,   true,    true,             PG_BYTE,        },
    [PG_GPU_TEXTURE_R16I] =         { "R16I",         GL_SHORT,             GL_R16I,         GL_RED,    1,   false,   true,    true,             PG_SHORT,       },
    [PG_GPU_TEXTURE_R32I] =         { "R32I",         GL_INT,               GL_R32I,         GL_RED,    1,   false,   true,    true,             PG_INT,         },
    [PG_GPU_TEXTURE_R32F] =         { "R32F",         GL_FLOAT,             GL_R32F,         GL_RED,    1,   false,   true,    true,             PG_FLOAT,       },
    /*  RG  */
    [PG_GPU_TEXTURE_RG8_UNORM] =    { "RG8",          GL_UNSIGNED_BYTE,     GL_RG8,          GL_RG,     2,   true,    true,    true,             PG_UBVEC2,      },
    [PG_GPU_TEXTURE_RG16_UNORM] =   { "RG16",         GL_UNSIGNED_SHORT,    GL_RG16,         GL_RG,     2,   true,    true,    true,             PG_USVEC2,      },
    [PG_GPU_TEXTURE_RG8_SNORM] =    { "RG8_SNORM",    GL_BYTE,              GL_RG8_SNORM,    GL_RG,     2,   true,    false,   false,            PG_BVEC2,       },
    [PG_GPU_TEXTURE_RG16_SNORM] =   { "RG16_SNORM",   GL_SHORT,             GL_RG16_SNORM,   GL_RG,     2,   true,    false,   false,            PG_SVEC2,       },
    [PG_GPU_TEXTURE_RG8UI] =        { "RG8UI",        GL_UNSIGNED_BYTE,     GL_RG8UI,        GL_RG,     2,   false,   true,    true,             PG_UBVEC2,      },
    [PG_GPU_TEXTURE_RG16UI] =       { "RG16UI",       GL_UNSIGNED_SHORT,    GL_RG16UI,       GL_RG,     2,   false,   true,    true,             PG_USVEC2,      },
    [PG_GPU_TEXTURE_RG32UI] =       { "RG32UI",       GL_UNSIGNED_INT,      GL_RG32UI,       GL_RG,     2,   false,   true,    true,             PG_UVEC2,       },
    [PG_GPU_TEXTURE_RG8I] =         { "RG8I",         GL_BYTE,              GL_RG8I,         GL_RG,     2,   false,   true,    true,             PG_BVEC2,       },
    [PG_GPU_TEXTURE_RG16I] =        { "RG16I",        GL_SHORT,             GL_RG16I,        GL_RG,     2,   false,   true,    true,             PG_SVEC2,       },
    [PG_GPU_TEXTURE_RG32I] =        { "RG32I",        GL_INT,               GL_RG32I,        GL_RG,     2,   false,   true,    true,             PG_IVEC2,       },
    [PG_GPU_TEXTURE_RG32F] =        { "RG32F",        GL_FLOAT,             GL_RG32F,        GL_RG,     2,   false,   true,    true,             PG_VEC2,        },
    /*  RGB */
    [PG_GPU_TEXTURE_RGB8_UNORM] =   { "RGB8",         GL_UNSIGNED_BYTE,     GL_RGB8,         GL_RGB,    3,   true,    false,   false,            PG_UBVEC3,      },
    [PG_GPU_TEXTURE_RGB16_UNORM] =  { "RGB16",        GL_UNSIGNED_SHORT,    GL_RGB16,        GL_RGB,    3,   true,    false,   false,            PG_USVEC3,      },
    [PG_GPU_TEXTURE_RGB8_SNORM] =   { "RGB8_SNORM",   GL_BYTE,              GL_RGB8_SNORM,   GL_RGB,    3,   true,    false,   false,            PG_BVEC3,       },
    [PG_GPU_TEXTURE_RGB16_SNORM] =  { "RGB16_SNORM",  GL_SHORT,             GL_RGB16_SNORM,  GL_RGB,    3,   true,    false,   false,            PG_SVEC3,       },
    [PG_GPU_TEXTURE_RGB8UI] =       { "RGB8UI",       GL_UNSIGNED_BYTE,     GL_RGB8UI,       GL_RGB,    3,   false,   false,   false,            PG_UBVEC3,      },
    [PG_GPU_TEXTURE_RGB16UI] =      { "RGB16UI",      GL_UNSIGNED_SHORT,    GL_RGB16UI,      GL_RGB,    3,   false,   false,   false,            PG_USVEC3,      },
    [PG_GPU_TEXTURE_RGB32UI] =      { "RGB32UI",      GL_UNSIGNED_INT,      GL_RGB32UI,      GL_RGB,    3,   false,   true,    false,            PG_UVEC3,       },
    [PG_GPU_TEXTURE_RGB8I] =        { "RGB8I",        GL_BYTE,              GL_RGB8I,        GL_RGB,    3,   false,   false,   false,            PG_BVEC3,       },
    [PG_GPU_TEXTURE_RGB16I] =       { "RGB16I",       GL_SHORT,             GL_RGB16I,       GL_RGB,    3,   false,   false,   false,            PG_SVEC3,       },
    [PG_GPU_TEXTURE_RGB32I] =       { "RGB32I",       GL_INT,               GL_RGB32I,       GL_RGB,    3,   false,   true,    false,            PG_IVEC3,       },
    [PG_GPU_TEXTURE_RGB32F] =       { "RGB32F",       GL_FLOAT,             GL_RGB32F,       GL_RGB,    3,   false,   true,    false,            PG_VEC3,        },
    /*  RGBA    */
    [PG_GPU_TEXTURE_RGBA8_UNORM] =  { "RGBA8",        GL_UNSIGNED_BYTE,     GL_RGBA8,        GL_RGBA,   4,   true,    true,    true,             PG_UBVEC4,      },
    [PG_GPU_TEXTURE_RGBA16_UNORM] = { "RGBA16",       GL_UNSIGNED_SHORT,    GL_RGBA16,       GL_RGBA,   4,   true,    true,    true,             PG_USVEC4,      },
    [PG_GPU_TEXTURE_RGBA8_SNORM] =  { "RGBA8_SNORM",  GL_BYTE,              GL_RGBA8_SNORM,  GL_RGBA,   4,   true,    false,   false,            PG_BVEC4,       },
    [PG_GPU_TEXTURE_RGBA16_SNORM] = { "RGBA16_SNORM", GL_SHORT,             GL_RGBA16_SNORM, GL_RGBA,   4,   true,    false,   false,            PG_SVEC4,       },
    [PG_GPU_TEXTURE_RGBA8UI] =      { "RGBA8UI",      GL_UNSIGNED_BYTE,     GL_RGBA8UI,      GL_RGBA,   4,   false,   true,    true,             PG_UBVEC4,      },
    [PG_GPU_TEXTURE_RGBA16UI] =     { "RGBA16UI",     GL_UNSIGNED_SHORT,    GL_RGBA16UI,     GL_RGBA,   4,   false,   true,    true,             PG_USVEC4,      },
    [PG_GPU_TEXTURE_RGBA32UI] =     { "RGBA32UI",     GL_UNSIGNED_INT,      GL_RGBA32UI,     GL_RGBA,   4,   false,   true,    true,             PG_UVEC4,       },
    [PG_GPU_TEXTURE_RGBA8I] =       { "RGBA8I",       GL_BYTE,              GL_RGBA8I,       GL_RGBA,   4,   false,   true,    true,             PG_BVEC4,       },
    [PG_GPU_TEXTURE_RGBA16I] =      { "RGBA16I",      GL_SHORT,             GL_RGBA16I,      GL_RGBA,   4,   false,   true,    true,             PG_SVEC4,       },
    [PG_GPU_TEXTURE_RGBA32I] =      { "RGBA32I",      GL_INT,               GL_RGBA32I,      GL_RGBA,   4,   false,   true,    true,             PG_IVEC4,       },
    [PG_GPU_TEXTURE_RGBA32F] =      { "RGBA32F",      GL_FLOAT,             GL_RGBA32F,      GL_RGBA,   4,   false,   true,    true,             PG_VEC4,        },
    /*  DEPTH   */
    [PG_GPU_TEXTURE_DEPTH16] =           { "DEPTH16",           GL_FLOAT, GL_DEPTH_COMPONENT16,  GL_DEPTH_COMPONENT, 1, true,  false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_DEPTH24] =           { "DEPTH24",           GL_FLOAT, GL_DEPTH_COMPONENT24,  GL_DEPTH_COMPONENT, 1, true,  false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_DEPTH32] =           { "DEPTH32",           GL_FLOAT, GL_DEPTH_COMPONENT32,  GL_DEPTH_COMPONENT, 1, true,  false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_DEPTH32F] =          { "DEPTH32F",          GL_FLOAT, GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, 1, false, false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_DEPTH24_STENCIL8] =  { "DEPTH24_STENCIL8",  GL_FLOAT, GL_DEPTH24_STENCIL8,   GL_DEPTH_STENCIL,   1, true,  false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_DEPTH32F_STENCIL8] = { "DEPTH32F_STENCIL8", GL_FLOAT, GL_DEPTH32F_STENCIL8,  GL_DEPTH_STENCIL,   1, false, false, true, PG_FLOAT },
    [PG_GPU_TEXTURE_STENCIL8] = { "STENCIL8", GL_UNSIGNED_BYTE, GL_STENCIL_INDEX8,  GL_STENCIL_INDEX,   1, false, false, true, PG_UBYTE },
};

GLenum gl_texture_param_wrap[] = {
    [PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_EDGE] = GL_CLAMP_TO_EDGE,
    [PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_BORDER] = GL_CLAMP_TO_BORDER,
    [PG_GPU_TEXTURE_PARAM_WRAP_REPEAT] = GL_REPEAT,
    [PG_GPU_TEXTURE_PARAM_WRAP_MIRRORED_REPEAT] = GL_MIRRORED_REPEAT,
};

GLenum gl_texture_param_filter[] = {
    [PG_GPU_TEXTURE_PARAM_FILTER_NEAREST] = GL_NEAREST,
    [PG_GPU_TEXTURE_PARAM_FILTER_LINEAR] = GL_LINEAR,
    [PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_NEAREST] = GL_NEAREST_MIPMAP_NEAREST,
    [PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_NEAREST] = GL_LINEAR_MIPMAP_NEAREST,
    [PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_LINEAR] = GL_NEAREST_MIPMAP_LINEAR,
    [PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_LINEAR] = GL_LINEAR_MIPMAP_LINEAR,
};

GLenum gl_texture_param_swizzle[] = {
    [PG_GPU_TEXTURE_PARAM_SWIZZLE_RED] = GL_RED,
    [PG_GPU_TEXTURE_PARAM_SWIZZLE_GREEN] = GL_GREEN,
    [PG_GPU_TEXTURE_PARAM_SWIZZLE_BLUE] = GL_BLUE,
    [PG_GPU_TEXTURE_PARAM_SWIZZLE_ALPHA] = GL_ALPHA,
};

GLenum gl_texture_param_ds_mode[] = {
    [PG_GPU_TEXTURE_PARAM_DS_DEPTH] = GL_DEPTH_COMPONENT,
    [PG_GPU_TEXTURE_PARAM_DS_STENCIL] = GL_STENCIL_INDEX,
};


static const struct pg_gpu_texture_params gpu_texture_params_default = {
    .wrap_x = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT,
    .wrap_y = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT,
    .wrap_z = PG_GPU_TEXTURE_PARAM_WRAP_REPEAT,
    .filter_min = PG_GPU_TEXTURE_PARAM_FILTER_NEAREST,
    .filter_mag= PG_GPU_TEXTURE_PARAM_FILTER_NEAREST,
    .swizzle = {
        PG_GPU_TEXTURE_PARAM_SWIZZLE_RED,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_GREEN,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_BLUE,
        PG_GPU_TEXTURE_PARAM_SWIZZLE_ALPHA, },
    .depth_stencil_mode = PG_GPU_TEXTURE_PARAM_DS_DEPTH,
    .base_mipmap = 0,
    .max_mipmap = 1000,
    .min_lod = -1000,
    .max_lod = 1000,
    .lod_bias = 0,
    .border_color = {{ 0, 0, 0, 0 }},
};

struct pg_gpu_texture {
    GLuint gl;  // (glGenTextures)
    GLenum gl_texture_binding;  // (glBindTexture)
    enum pg_gpu_texture_format format;
    struct pg_gpu_texture_params params;
    ivec3 dimensions;
    bool is_3D;
};

struct pg_gpu_texture_view {
    pg_gpu_texture_t* gl_tex;
    int layer_start, n_layers;
};
