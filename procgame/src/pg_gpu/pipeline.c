#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

const char* gpu_comparison_strings[] = {
    [PG_GPU_NEVER] = "never",
    [PG_GPU_LESS] = "less",
    [PG_GPU_LEQUAL] = "lequal",
    [PG_GPU_GREATER] = "greater",
    [PG_GPU_GEQUAL] = "gequal",
    [PG_GPU_EQUAL] = "equal",
    [PG_GPU_NOTEQUAL] = "notequal",
    [PG_GPU_ALWAYS] = "always",
};

const char* stencil_op_strings[] = {
    [PG_STENCIL_KEEP] = "keep",
    [PG_STENCIL_ZERO] = "zero",
    [PG_STENCIL_REPLACE] = "replace",
    [PG_STENCIL_INCR] = "increment",
    [PG_STENCIL_INCR_WRAP] = "increment_wrap",
    [PG_STENCIL_DECR] = "decrement",
    [PG_STENCIL_DECR_WRAP] = "decrement_wrap",
    [PG_STENCIL_INVERT] = "invert",
};

GLenum gl_comparisons[] = {
    [PG_GPU_NEVER] = GL_NEVER,
    [PG_GPU_LESS] = GL_LESS,
    [PG_GPU_LEQUAL] = GL_LEQUAL,
    [PG_GPU_GREATER] = GL_GREATER,
    [PG_GPU_GEQUAL] = GL_GEQUAL,
    [PG_GPU_EQUAL] = GL_EQUAL,
    [PG_GPU_NOTEQUAL] = GL_NOTEQUAL,
    [PG_GPU_ALWAYS] = GL_ALWAYS,
};

GLenum gl_stencil_ops[] = {
    [PG_STENCIL_KEEP] = GL_KEEP,
    [PG_STENCIL_ZERO] = GL_ZERO,
    [PG_STENCIL_REPLACE] = GL_REPLACE,
    [PG_STENCIL_INCR] = GL_INCR,
    [PG_STENCIL_INCR_WRAP] = GL_INCR_WRAP,
    [PG_STENCIL_DECR] = GL_DECR,
    [PG_STENCIL_DECR_WRAP] = GL_DECR_WRAP,
    [PG_STENCIL_INVERT] = GL_INVERT,
};

enum pg_gpu_comparison pg_gpu_comparison_from_string(const char* str)
{
    for(int i = 0; i < PG_GPU_COMPARISON_COUNT; ++i) {
        if(strcmp(str, gpu_comparison_strings[i]) == 0) return i;
    }
    return PG_GPU_NEVER;
}

enum pg_stencil_op pg_stencil_op_from_string(const char* str)
{
    for(int i = 0; i < PG_STENCIL_OP_COUNT; ++i) {
        if(strcmp(str, stencil_op_strings[i]) == 0) return i;
    }
    return PG_STENCIL_KEEP;
}

const char* pg_gpu_comparison_to_string(enum pg_gpu_comparison func)
{
    return gpu_comparison_strings[func];
}

const char* pg_stencil_op_to_string(enum pg_stencil_op op)
{
    return stencil_op_strings[op];
}

GLenum gl_blend_factors[PG_GPU_BLEND_FACTOR_COUNT] = {
    [PG_GPU_ZERO] =                       GL_ZERO,
    [PG_GPU_ONE] =                        GL_ONE,
    [PG_GPU_SRC_COLOR] =                  GL_SRC_COLOR,
    [PG_GPU_ONE_MINUS_SRC_COLOR] =        GL_ONE_MINUS_SRC_COLOR,
    [PG_GPU_DST_COLOR] =                  GL_DST_COLOR,
    [PG_GPU_ONE_MINUS_DST_COLOR] =        GL_ONE_MINUS_DST_COLOR,
    [PG_GPU_SRC_ALPHA] =                  GL_SRC_ALPHA,
    [PG_GPU_ONE_MINUS_SRC_ALPHA] =        GL_ONE_MINUS_SRC_ALPHA,
    [PG_GPU_DST_ALPHA] =                  GL_DST_ALPHA,
    [PG_GPU_ONE_MINUS_DST_ALPHA] =        GL_ONE_MINUS_DST_ALPHA,
    [PG_GPU_CONSTANT_COLOR] =             GL_CONSTANT_COLOR,
    [PG_GPU_ONE_MINUS_CONSTANT_COLOR] =   GL_ONE_MINUS_CONSTANT_COLOR,
    [PG_GPU_CONSTANT_ALPHA] =             GL_CONSTANT_ALPHA,
    [PG_GPU_ONE_MINUS_CONSTANT_ALPHA] =   GL_ONE_MINUS_CONSTANT_ALPHA,
    [PG_GPU_SRC_ALPHA_SATURATE] =         GL_SRC_ALPHA_SATURATE,
    [PG_GPU_SRC1_COLOR] =                 GL_SRC1_COLOR,
    [PG_GPU_ONE_MINUS_SRC1_COLOR] =       GL_ONE_MINUS_SRC1_COLOR,
    [PG_GPU_SRC1_ALPHA] =                 GL_SRC1_ALPHA,
    [PG_GPU_ONE_MINUS_SRC1_ALPHA] =       GL_ONE_MINUS_SRC1_ALPHA,
};

GLenum gl_blend_equations[PG_GPU_BLEND_EQUATION_COUNT] = {
    [PG_GPU_FUNC_ADD] =                GL_FUNC_ADD,
    [PG_GPU_FUNC_SUBTRACT] =           GL_FUNC_SUBTRACT,
    [PG_GPU_FUNC_REVERSE_SUBTRACT] =   GL_FUNC_REVERSE_SUBTRACT,
    [PG_GPU_MIN] =                     GL_MIN,
    [PG_GPU_MAX] =                     GL_MAX,
};

const char* gpu_blend_factor_strings[] = {
    [PG_GPU_ZERO] = "zero",
    [PG_GPU_ONE] = "one",
    [PG_GPU_SRC_COLOR] = "src_color",
    [PG_GPU_ONE_MINUS_SRC_COLOR] = "one_minus_src_color",
    [PG_GPU_DST_COLOR] = "dst_color",
    [PG_GPU_ONE_MINUS_DST_COLOR] = "one_minus_dst_color",
    [PG_GPU_SRC_ALPHA] = "src_alpha",
    [PG_GPU_ONE_MINUS_SRC_ALPHA] = "one_minus_src_alpha",
    [PG_GPU_DST_ALPHA] = "dst_alpha",
    [PG_GPU_ONE_MINUS_DST_ALPHA] = "one_minus_dst_alpha",
    [PG_GPU_CONSTANT_COLOR] = "constant_color",
    [PG_GPU_ONE_MINUS_CONSTANT_COLOR] = "one_minus_constant_color",
    [PG_GPU_CONSTANT_ALPHA] = "constant_alpha",
    [PG_GPU_ONE_MINUS_CONSTANT_ALPHA] = "one_minus_constant_alpha",
    [PG_GPU_SRC_ALPHA_SATURATE] = "alpha_saturate",
    [PG_GPU_SRC1_COLOR] = "src1_color",
    [PG_GPU_ONE_MINUS_SRC1_COLOR] = "one_minus_src1_color",
    [PG_GPU_SRC1_ALPHA] = "src1_alpha",
    [PG_GPU_ONE_MINUS_SRC1_ALPHA] = "one_minus_src1_alpha",
};

const char* gpu_blend_equation_strings[] = {
    [PG_GPU_FUNC_ADD] = "add",
    [PG_GPU_FUNC_SUBTRACT] = "subtract",
    [PG_GPU_FUNC_REVERSE_SUBTRACT] = "reverse_subtract",
    [PG_GPU_MIN] = "min",
    [PG_GPU_MAX] = "max",
};


enum pg_gpu_blend_factor pg_gpu_blend_factor_from_string(const char* str)
{
    for(int i = 0; i < PG_GPU_BLEND_FACTOR_COUNT; ++i) {
        if(strcmp(str, gpu_blend_factor_strings[i]) == 0) return i;
    }
    return PG_GPU_NEVER;
}

const char* pg_gpu_blend_factor_to_string(enum pg_gpu_blend_factor func)
{
    return gpu_blend_factor_strings[func];
}

enum pg_gpu_blend_equation pg_gpu_blend_equation_from_string(const char* str)
{
    for(int i = 0; i < PG_GPU_BLEND_EQUATION_COUNT; ++i) {
        if(strcmp(str, gpu_blend_equation_strings[i]) == 0) return i;
    }
    return PG_GPU_NEVER;
}

const char* pg_gpu_blend_equation_to_string(enum pg_gpu_blend_equation func)
{
    return gpu_blend_equation_strings[func];
}

void pg_gpu_stencil_state_apply(struct pg_gpu_stencil_state* stencil)
{
    PG_CHECK_GL(glStencilMask(stencil->write_mask));
    PG_CHECK_GL(glStencilFunc(gl_comparisons[stencil->func], stencil->func_ref, stencil->func_mask));
    PG_CHECK_GL(glStencilOp(
        gl_stencil_ops[stencil->op_stencil_fail],
        gl_stencil_ops[stencil->op_depth_fail],
        gl_stencil_ops[stencil->op_both_pass]));
    if(stencil->enabled) PG_CHECK_GL(glEnable(GL_STENCIL_TEST));
    else PG_CHECK_GL(glDisable(GL_STENCIL_TEST));
}

void pg_gpu_depth_state_apply(struct pg_gpu_depth_state* depth)
{
    PG_CHECK_GL(glDepthMask(depth->write_mask));
    PG_CHECK_GL(glDepthFunc(gl_comparisons[depth->func]));
    if(depth->enabled) PG_CHECK_GL(glEnable(GL_DEPTH_TEST));
    else PG_CHECK_GL(glDisable(GL_DEPTH_TEST));
}

void pg_gpu_blending_state_apply(struct pg_gpu_blending_state* blending)
{
    if(blending->enabled) {
        PG_CHECK_GL(glEnable(GL_BLEND));
        PG_CHECK_GL(glBlendEquation(gl_blend_equations[blending->equation]));
        PG_CHECK_GL(glBlendFunc(gl_blend_factors[blending->src_factor], gl_blend_factors[blending->dst_factor]));
    } else {
        PG_CHECK_GL(glDisable(GL_BLEND));
    }
}



struct pg_gpu_pipeline {
    /*  The shader program  */
    pg_gpu_shader_program_t* shader;
    struct pg_gpu_depth_state depth_state;
    struct pg_gpu_stencil_state stencil_state;
    struct pg_gpu_blending_state blending_state;
};

pg_gpu_pipeline_t* pg_gpu_pipeline_create(pg_gpu_shader_program_t* sh_prog,
        struct pg_gpu_depth_state* depth_state,
        struct pg_gpu_stencil_state* stencil_state,
        struct pg_gpu_blending_state* blending_state)
{
    struct pg_gpu_pipeline* pipeline = calloc(1, sizeof(*pipeline));
    pipeline->shader = sh_prog;
    pipeline->depth_state = *depth_state;
    pipeline->stencil_state = *stencil_state;
    pipeline->blending_state = *blending_state;
    return pipeline;
}

pg_gpu_shader_program_t* pg_gpu_pipeline_get_shader(pg_gpu_pipeline_t* pipeline)
{
    return pipeline->shader;
}

void pg_gpu_pipeline_destroy(pg_gpu_pipeline_t* pipeline)
{
    if(!pipeline) return;
    free(pipeline);
}

void pg_gpu_pipeline_bind(pg_gpu_pipeline_t* pipeline)
{
    pg_gpu_shader_program_use(pipeline->shader);
    pg_gpu_stencil_state_apply(&pipeline->stencil_state);
    pg_gpu_depth_state_apply(&pipeline->depth_state);
    pg_gpu_blending_state_apply(&pipeline->blending_state);
}
