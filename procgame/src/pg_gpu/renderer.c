#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

struct pg_gpu_renderer {
    ARR_T(pg_gpu_render_stage_t*) stages;
};

void pg_gpu_renderer_init(pg_gpu_renderer_t* render)
{
    if(!render) {
        pg_log(PG_LOG_ERROR, "Creating renderer with null inputs");
        return;
    }
    *render = (struct pg_gpu_renderer){};
    ARR_INIT(render->stages);
}

void pg_gpu_renderer_deinit(pg_gpu_renderer_t* render)
{
    ARR_DEINIT(render->stages);
}

pg_gpu_renderer_t* pg_gpu_renderer_create()
{
    pg_gpu_renderer_t* render = malloc(sizeof(*render));
    pg_gpu_renderer_init(render);
    return render;
}

void pg_gpu_renderer_destroy(pg_gpu_renderer_t* render)
{
    pg_gpu_renderer_deinit(render);
    free(render);
}

void pg_gpu_renderer_add_stage(pg_gpu_renderer_t* render, pg_gpu_render_stage_t* stage)
{
    ARR_PUSH(render->stages, stage);
}

void pg_gpu_renderer_execute(pg_gpu_renderer_t* render, uint32_t output_filter)
{
    int i;
    pg_gpu_render_stage_t* stage;
    pg_gpu_render_stage_t* prev_stage = NULL;
    ARR_FOREACH(render->stages, stage, i) {
        pg_gpu_render_stage_execute(stage, output_filter);
    }
}
