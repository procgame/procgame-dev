#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

void pg_gpu_asset_loaders(pg_asset_manager_t* asset_mgr)
{
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_texture_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_framebuffer_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_buffer_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_vertex_assembly_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_resource_set_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_shader_program_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_pipeline_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_render_stage_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_renderer_asset_loader());

    pg_gpu_framebuffer_t* screen_fbuf = pg_gpu_framebuffer_screen();
    pg_asset_handle_t screen_asset = pg_asset_from_data(asset_mgr, "pg_gpu_framebuffer",
        "__pg_render_output", screen_fbuf, true);
    if(!pg_asset_exists(screen_asset)) {
        pg_log(PG_LOG_ERROR, "Failed to create asset wrapper for screen framebuffer");
    }

    pg_gpu_vertex_assembly_t* empty_verts = pg_gpu_vertex_assembly_create();
    pg_asset_from_data(asset_mgr, "pg_gpu_vertex_assembly",
            "__pg_empty_vertex_assembly", empty_verts, true);
}
