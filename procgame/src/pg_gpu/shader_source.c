#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

/************************************/
/*  SHADER SOURCE CODE              */
/************************************/
/*  Not compiled, can still be preprocessed */

void pg_shader_source_from_memory(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type,
                                const char* src, size_t src_len)
{
    char* new_src = malloc(src_len);
    memcpy(new_src, src, src_len + 1);
    *sh = (struct pg_shader_source){
        .type = type, 
        .src = new_src,
        .src_len = src_len,
    };
    pg_text_variator_init(&sh->variator, sh->src, sh->src_len,
                          "//!pg_variant", "//!pg_end_variant");
}

bool pg_shader_source_from_file(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type, struct pg_file* file)
{
    *sh = (struct pg_shader_source){0};
    /*  Get the contents of the file first  */
    if(!file || !file->f) return false;
    pg_file_read_full(file);
    char* new_src = malloc(file->size + 1);
    memcpy(new_src, file->content, file->size);
    new_src[file->size] = '\0';
    /*  Initialize the shader stage */
    *sh = (struct pg_shader_source){
        .type = type, 
        .src = new_src,
        .src_len = file->size,
    };
    pg_text_variator_init(&sh->variator, sh->src, sh->src_len,
                          "//!pg_variant", "//!pg_end_variant");
    return true;
}

bool pg_shader_source_from_file_with_includes(struct pg_shader_source* sh, enum pg_gpu_shader_stage_type type,
                                struct pg_file* file, const char** include_dirs, int n_include_dirs)
{
    *sh = (struct pg_shader_source){0};
    /*  Get the contents of the file first  */
    if(!file || !file->f) return false;
    /*  Read the source file    */
    char_arr_t src;
    ARR_INIT(src);
    bool result = pg_file_read_preprocessed(file, include_dirs, n_include_dirs, &src);
    if(!result) return false;
    *sh = (struct pg_shader_source){
        .type = type,
        .src = src.data,    /*  Taking the data pointer from the src array, so don't deinit it! */
        .src_len = src.len,
    };
    pg_text_variator_init(&sh->variator, sh->src, sh->src_len,
                          "//!pg_variant", "//!pg_end_variant");
    /*  Don't ARR_DEINIT(src) because we passed ownership of its data to the shader_source object   */
    return true;
}

void pg_shader_source_deinit(struct pg_shader_source* sh)
{
    if(!sh) return;
    free(sh->src);
    pg_text_variator_deinit(&sh->variator);
}

int pg_shader_source_variation_count(struct pg_shader_source* sh)
{
    return sh->variator.variants.len;
}



