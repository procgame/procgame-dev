#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

const char* gpu_command_strings[] = {
    [PG_GPU_COMMAND_DRAW_TRIANGLES] = "draw_triangles",
    [PG_GPU_COMMAND_SET_UNIFORM] = "set_uniform",
    [PG_GPU_COMMAND_CLEAR] = "clear",
    [PG_GPU_COMMAND_STENCIL] = "stencil",
};

const char* pg_gpu_command_op_to_string(enum pg_gpu_command_op op)
{
    return gpu_command_strings[op];
}

bool pg_gpu_commands_from_asset_json(pg_gpu_command_arr_t* cmds_out, cJSON* json,
        pg_asset_manager_t* asset_mgr, const struct pg_shader_interface_info* sh_iface)
{
    if(!cJSON_IsArray(json)) return false;
    cJSON* cmd_json;
    cJSON_ArrayForEach(cmd_json, json) {
        cJSON* cmd_op_json = cJSON_GetObjectItem(cmd_json, "cmd");
        struct pg_gpu_command cmd = {0};
        if(!cJSON_IsString(cmd_op_json)) return false;
        /*  draw_triangles  */
        if(strcmp(cmd_op_json->valuestring, "draw_triangles") == 0) {
            cmd.op = PG_GPU_COMMAND_DRAW_TRIANGLES;
            cJSON* range_json = cJSON_GetObjectItem(cmd_json, "range");
            cJSON* base_vertex_json = cJSON_GetObjectItem(cmd_json, "base_vertex");
            if(!range_json) {
                pg_log(PG_LOG_ERROR, "draw_triangles command requires 'range' member");
                return false;
            }
            cmd.draw_triangles.range = pg_asset_manager_read_ivec2(asset_mgr, range_json);
            if(cJSON_IsTrue(cJSON_GetObjectItem(cmd_json, "indexed"))) cmd.draw_triangles.indexed = true;
            if(cJSON_IsTrue(cJSON_GetObjectItem(cmd_json, "strip"))) cmd.draw_triangles.strip = true;
            if(cJSON_IsNumber(base_vertex_json)) cmd.draw_triangles.base_vertex = base_vertex_json->valueint;
        /*  set_uniform     */
        } else if(strcmp(cmd_op_json->valuestring, "set_uniform") == 0) {
            cmd.op = PG_GPU_COMMAND_SET_UNIFORM;
            cJSON* location_json = cJSON_GetObjectItem(cmd_json, "location");
            cJSON* name_json = cJSON_GetObjectItem(cmd_json, "name");
            pg_data_t new_data = json_read_pg_data(cmd_json);
            if((!name_json && !location_json) || !new_data.type || !new_data.n) {
                pg_log(PG_LOG_ERROR, "set_uniform command requires 'location' (or 'name'), 'type', 'count', and 'data' members");
                return false;
            }
            if(new_data.big) {
                pg_log(PG_LOG_ERROR, "lots of data in recording uniform commands is not supported");
                return false;
            }
            if(name_json) {
                if(!sh_iface) {
                    pg_log(PG_LOG_ERROR, "Reading command buffer JSON with named uniforms, but called without a shader interface for context");
                    return false;
                }
                int uni_location = pg_shader_interface_info_get_dynamic_uniform_index(sh_iface, name_json->valuestring);
                if(uni_location == -1) {
                    pg_log(PG_LOG_ERROR, "Command buffer JSON includes set_uniform command with non-existent uniform name '%s'",
                            name_json->valuestring);
                    return false;
                }
                pg_log(PG_LOG_INFO, "Setting uniform (name '%s' location %d)", name_json->valuestring, uni_location);
                cmd.set_uniform.location = uni_location;
            } else if(location_json) {
                cmd.set_uniform.location = location_json->valueint;
            }
            cmd.set_uniform.data = new_data;
        /*  clear       */
        } else if(strcmp(cmd_op_json->valuestring, "clear") == 0) {
            cmd.op = PG_GPU_COMMAND_CLEAR;
            cJSON* color_json = cJSON_GetObjectItem(cmd_json, "color");
            cmd.clear.color = pg_asset_manager_read_vec4(asset_mgr, color_json);
        /*  stencil     */
        } else if(strcmp(cmd_op_json->valuestring, "stencil") == 0) {
            cmd.op = PG_GPU_COMMAND_STENCIL;
            pg_gpu_stencil_state_from_json(&cmd.stencil, cmd_json);
        /*  depth   */
        } else if(strcmp(cmd_op_json->valuestring, "depth") == 0) {
            cmd.op = PG_GPU_COMMAND_DEPTH;
            pg_gpu_depth_state_from_json(&cmd.depth, cmd_json);
        /*  depth   */
        } else if(strcmp(cmd_op_json->valuestring, "blending") == 0) {
            cmd.op = PG_GPU_COMMAND_BLENDING;
            pg_gpu_blending_state_from_json(&cmd.blending, cmd_json);
        /*  color_mask   */
        } else if(strcmp(cmd_op_json->valuestring, "color_mask") == 0) {
            cmd.op = PG_GPU_COMMAND_COLOR_MASK;
            cJSON* mask_json = cJSON_GetObjectItem(cmd_json, "mask");
            cmd.color_mask.mask = pg_asset_manager_read_ivec4(asset_mgr, mask_json);
        }
        ARR_PUSH(*cmds_out, cmd);
    }
    return true;
}

bool pg_gpu_commands_from_json(pg_gpu_command_arr_t* cmds_out, cJSON* json)
{
    return pg_gpu_commands_from_asset_json(cmds_out, json, NULL, NULL);
}

static void execute_cmd_draw_triangles(struct pg_gpu_command* cmd);
static void execute_cmd_set_uniform(struct pg_gpu_command* cmd);
static void execute_cmd_clear(struct pg_gpu_command* cmd);
static void execute_cmd_stencil(struct pg_gpu_command* cmd);
static void execute_cmd_depth(struct pg_gpu_command* cmd);
static void execute_cmd_blending(struct pg_gpu_command* cmd);
static void execute_cmd_color_mask(struct pg_gpu_command* cmd);

void pg_gpu_command_execute(struct pg_gpu_command* cmd)
{
    if(cmd->op == PG_GPU_COMMAND_DRAW_TRIANGLES) {
        execute_cmd_draw_triangles(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_SET_UNIFORM) {
        execute_cmd_set_uniform(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_CLEAR) {
        execute_cmd_clear(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_STENCIL) {
        execute_cmd_stencil(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_DEPTH) {
        execute_cmd_depth(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_BLENDING) {
        execute_cmd_blending(cmd);
    } else if(cmd->op == PG_GPU_COMMAND_COLOR_MASK) {
        execute_cmd_color_mask(cmd);
    }
}

static void execute_cmd_draw_triangles(struct pg_gpu_command* cmd)
{
    GLenum draw_mode = cmd->draw_triangles.strip ? GL_TRIANGLE_STRIP : GL_TRIANGLES;
    if(cmd->draw_triangles.indexed) {
        PG_CHECK_GL(glDrawElementsBaseVertex(draw_mode, cmd->draw_triangles.range.y,
                GL_UNSIGNED_INT, (void*)(uintptr_t)(cmd->draw_triangles.range.x * 4),
                cmd->draw_triangles.base_vertex));
    } else {
        PG_CHECK_GL(glDrawArrays(draw_mode, cmd->draw_triangles.range.x,
                cmd->draw_triangles.range.y));
    }
}

static void execute_cmd_set_uniform(struct pg_gpu_command* cmd)
{
    GLint location = cmd->set_uniform.location;
    enum pg_data_type data_type = cmd->set_uniform.data.type;
    void* data_ptr = pg_data_value_ptr(&cmd->set_uniform.data, 0);
    int data_count = cmd->set_uniform.data.n;
    bool transpose_matrix = cmd->set_uniform.transpose_matrix;
    switch(data_type) {
    case PG_INT:    PG_CHECK_GL(glUniform1iv(location,  data_count, (int32_t*)data_ptr)); break;
    case PG_IVEC2:  PG_CHECK_GL(glUniform2iv(location,  data_count, (int32_t*)data_ptr)); break;
    case PG_IVEC3:  PG_CHECK_GL(glUniform3iv(location,  data_count, (int32_t*)data_ptr)); break;
    case PG_IVEC4:  PG_CHECK_GL(glUniform4iv(location,  data_count, (int32_t*)data_ptr)); break;
    case PG_UINT:   PG_CHECK_GL(glUniform1uiv(location, data_count, (uint32_t*)data_ptr)); break;
    case PG_UVEC2:  PG_CHECK_GL(glUniform2uiv(location, data_count, (uint32_t*)data_ptr)); break;
    case PG_UVEC3:  PG_CHECK_GL(glUniform3uiv(location, data_count, (uint32_t*)data_ptr)); break;
    case PG_UVEC4:  PG_CHECK_GL(glUniform4uiv(location, data_count, (uint32_t*)data_ptr)); break;
    case PG_FLOAT:  PG_CHECK_GL(glUniform1fv(location,  data_count, (float*)data_ptr)); break;
    case PG_VEC2:   PG_CHECK_GL(glUniform2fv(location,  data_count, (float*)data_ptr)); break;
    case PG_VEC3:   PG_CHECK_GL(glUniform3fv(location,  data_count, (float*)data_ptr)); break;
    case PG_VEC4:   PG_CHECK_GL(glUniform4fv(location,  data_count, (float*)data_ptr)); break;
    case PG_MAT4:   PG_CHECK_GL(glUniformMatrix4fv(location, data_count, transpose_matrix, (float*)data_ptr)); break;
    case PG_MAT3:   PG_CHECK_GL(glUniformMatrix3fv(location, data_count, transpose_matrix, (float*)data_ptr)); break;
    default: break;
    }
}

static void execute_cmd_clear(struct pg_gpu_command* cmd)
{
    vec4 color = cmd->clear.color;
    PG_CHECK_GL(glClearColor(color.x, color.y, color.z, color.w));
    PG_CHECK_GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
}

static void execute_cmd_stencil(struct pg_gpu_command* cmd)
{
    pg_gpu_stencil_state_apply(&cmd->stencil);
}

static void execute_cmd_depth(struct pg_gpu_command* cmd)
{
    pg_gpu_depth_state_apply(&cmd->depth);
}

static void execute_cmd_blending(struct pg_gpu_command* cmd)
{
    pg_gpu_blending_state_apply(&cmd->blending);
}


static void execute_cmd_color_mask(struct pg_gpu_command* cmd)
{
    PG_CHECK_GL(glColorMask(
        cmd->color_mask.mask.v[0],
        cmd->color_mask.mask.v[1],
        cmd->color_mask.mask.v[2],
        cmd->color_mask.mask.v[3]));
}

