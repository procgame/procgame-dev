#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

typedef ARR_T(struct stage_uniform {
    int gl_location;
    pg_data_t data;
}) uniform_list_t;

struct pg_gpu_render_stage {
    pg_gpu_pipeline_t* pipeline;
    uniform_list_t initial_uniforms;
    pg_gpu_command_arr_t group_pre_commands;
    char debug_name[128];

    ARR_T(struct stage_target {
        uint32_t input_filter;
        pg_gpu_framebuffer_t* output;
        pg_gpu_command_arr_t commands;
        uniform_list_t uniforms;
    }) targets;

    ARR_T(struct stage_input_group {
        char name[128];
        uint32_t output_filter;
        pg_gpu_resource_set_t* resources;
        pg_gpu_vertex_assembly_t* vertex;
        pg_gpu_command_arr_t commands;
        uniform_list_t uniforms;
    }) input_groups;

    ARR_T(struct stage_input_range {
        char name[128];
        int group_idx;
        ivec2 vert_range;
        ivec2 idx_range;
    }) input_ranges;
};




static bool check_resources_match_shader_iface(const pg_gpu_resource_set_t* resources, const struct pg_shader_interface_info* sh_iface)
{
    bool set_matches_iface = true;
    const struct pg_shader_uniform_info* uni_info;
    int i;
    ARR_FOREACH_PTR(sh_iface->uniform_inputs, uni_info, i) {
        bool found_match = false;
        int uni_binding = uni_info->binding;
        if(uni_info->type == PG_SHADER_UNIFORM_TEXTURE) {
            if(pg_gpu_resource_set_get_texture(resources, uni_binding)) found_match = true;
        } else if(uni_info->type == PG_SHADER_UNIFORM_TEXTURE_BUFFER) {
            if(pg_gpu_resource_set_get_texture_buffer(resources, uni_binding)) found_match = true;
        } else if(uni_info->type == PG_SHADER_UNIFORM_BUFFER) {
            if(pg_gpu_resource_set_get_buffer(resources, uni_binding)) found_match = true;
        }
        if(!found_match) {
            pg_log(PG_LOG_ERROR, "Resource set missing resource in binding %d for shader input '%s'", uni_binding, uni_info->name);
            set_matches_iface = false;
        }
    }
    return set_matches_iface;
}

static bool check_vertex_assembly_matches_shader_iface(pg_gpu_vertex_assembly_t* vbuf, const struct pg_shader_interface_info* sh_iface)
{
    const struct pg_shader_vertex_info* vert_info;
    int i;
    ARR_FOREACH_PTR(sh_iface->vertex_inputs, vert_info, i) {
        struct pg_buffer_attribute attrib = {};
        pg_gpu_buffer_t* buf = pg_gpu_vertex_assembly_get_attribute_buffer(vbuf, i, &attrib);
        enum pg_data_type buf_attrib_type = attrib.info.type;
        enum pg_data_type sh_attrib_type = vert_info->type;
        if(!buf || pg_data_type_components[buf_attrib_type] != pg_data_type_components[sh_attrib_type]) {
            pg_log(PG_LOG_ERROR, "Render stage has incompatible shader and vertex assembly, in vertex attribute '%s'",
                    vert_info->name);
            return false;
        }
    }
    return true;
}

static void add_vertex_ranges_to_stage(pg_gpu_render_stage_t* stage, pg_gpu_vertex_assembly_t* vbuf, int group_idx)
{
    int n_ranges = pg_gpu_vertex_assembly_get_range_count(vbuf);
    /*  Start at 1 to skip the "all" range  */
    for(int i = 1; i < n_ranges; ++i) {
        const char* range_name = pg_gpu_vertex_assembly_get_range_name(vbuf, i);
        int j;
        struct stage_input_range* stage_range;
        ARR_FOREACH_PTR(stage->input_ranges, stage_range, j) {
            if(strncmp(stage_range->name, range_name, 128) == 0) {
                pg_log(PG_LOG_WARNING, "Duplicate input range name in render stage '%s'", stage->debug_name);
                break;
            }
        }
        if(j < stage->input_ranges.len) continue;
        struct stage_input_range* new_range = ARR_NEW(stage->input_ranges);
        *new_range = (struct stage_input_range){};
        strncpy(new_range->name, range_name, 128);
        new_range->group_idx = group_idx;
        pg_gpu_vertex_assembly_get_range(vbuf, i, &new_range->vert_range, &new_range->idx_range);
    }
}






int pg_gpu_render_stage_add_input_group(pg_gpu_render_stage_t* stage,
        uint32_t output_filter, const char* name,
        pg_gpu_vertex_assembly_t* vertex, pg_gpu_resource_set_t* resources)
{
    const struct pg_shader_interface_info* sh_iface = pg_gpu_shader_program_interface_info(
            pg_gpu_pipeline_get_shader(stage->pipeline));
    if(!check_resources_match_shader_iface(resources, sh_iface)) {
        pg_log(PG_LOG_ERROR, "Attempting to add input group to pg_gpu_render_stage with mismatched resource interface");
        return -1;
    }
    if(!check_vertex_assembly_matches_shader_iface(vertex, sh_iface)) {
        pg_log(PG_LOG_ERROR, "Attempting to add input group to pg_gpu_render_stage with mismatched vertex interface");
        return -1;
    }
    int new_group_idx = stage->input_groups.len;
    add_vertex_ranges_to_stage(stage, vertex, new_group_idx);
    struct stage_input_group* new_group = ARR_NEW(stage->input_groups);
    strncpy(new_group->name, name, 128);
    new_group->output_filter = output_filter;
    new_group->resources = resources;
    new_group->vertex = vertex;
    ARR_INIT(new_group->commands);
    ARR_INIT(new_group->uniforms);
    return new_group_idx;
}

int pg_gpu_render_stage_get_input_group(pg_gpu_render_stage_t* stage, const char* name)
{
    int i;
    struct stage_input_group* group;
    ARR_FOREACH_PTR(stage->input_groups, group, i) {
        if(strncmp(group->name, name, 128) == 0) return i;
    }
    return -1;
}

void pg_gpu_render_stage_get_input_range(pg_gpu_render_stage_t* stage, int group_idx,
        const char* range_name, ivec2* verts_out, ivec2* idx_out)
{
    struct stage_input_group* group = &stage->input_groups.data[group_idx];
    int range_idx = pg_gpu_vertex_assembly_get_range_index(group->vertex, range_name);
    ivec2 verts = {};
    ivec2 idx = {};
    if(range_idx != -1) {
        pg_gpu_vertex_assembly_get_range(group->vertex, range_idx, &verts, &idx);
    }
    if(verts_out) *verts_out = verts;
    if(idx_out) *idx_out = idx;
}


int pg_gpu_render_stage_add_target(pg_gpu_render_stage_t* stage, uint32_t input_filter,
        pg_gpu_framebuffer_t* output)
{
    int new_target_idx = stage->targets.len;
    struct stage_target* new_target = ARR_NEW(stage->targets);
    ARR_INIT(new_target->uniforms);
    ARR_INIT(new_target->commands);
    new_target->input_filter = input_filter;
    new_target->output = output;
    return new_target_idx;
}

static bool gpu_render_stage_init(pg_gpu_render_stage_t* stage, pg_gpu_pipeline_t* pipeline)
{
    *stage = (struct pg_gpu_render_stage){ .pipeline = pipeline, .debug_name = "pg_gpu_render_stage" };
    ARR_INIT(stage->group_pre_commands);
    ARR_INIT(stage->input_groups);
    ARR_INIT(stage->targets);
    ARR_INIT(stage->initial_uniforms);
    ARR_INIT(stage->input_ranges);
    return true;
}


void pg_gpu_render_stage_deinit(pg_gpu_render_stage_t* stage)
{
    int i;
    struct stage_uniform* uni;
    ARR_FOREACH_PTR(stage->initial_uniforms, uni, i) {
        pg_data_deinit(&uni->data);
    }
    ARR_DEINIT(stage->initial_uniforms);
    struct stage_input_group* group;
    ARR_FOREACH_PTR(stage->input_groups, group, i) {
        int j;
        ARR_FOREACH_PTR(group->uniforms, uni, j) pg_data_deinit(&uni->data);
        ARR_DEINIT(group->uniforms);
        ARR_DEINIT(group->commands);
    }
    struct stage_target* target;
    ARR_FOREACH_PTR(stage->targets, target, i) {
        int j;
        ARR_FOREACH_PTR(target->uniforms, uni, j) pg_data_deinit(&uni->data);
    }
    ARR_DEINIT(stage->input_groups);
    ARR_DEINIT(stage->targets);
}

pg_gpu_render_stage_t* pg_gpu_render_stage_create(pg_gpu_pipeline_t* pipeline)
{
    struct pg_gpu_render_stage tmp_stage = {};
    if(!gpu_render_stage_init(&tmp_stage, pipeline)) return NULL;
    pg_gpu_render_stage_t* new_stage = malloc(sizeof(*new_stage));
    *new_stage = tmp_stage;
    return new_stage;
}

void pg_gpu_render_stage_destroy(pg_gpu_render_stage_t* stage)
{
    pg_gpu_render_stage_deinit(stage);
    free(stage);
}

void pg_gpu_render_stage_set_debug_name(pg_gpu_render_stage_t* stage, const char* name)
{
    strncpy(stage->debug_name, name, 128);
}

void pg_gpu_render_stage_clear_commands(pg_gpu_render_stage_t* stage)
{
    int i;
    struct stage_input_group* group;
    ARR_FOREACH_PTR(stage->input_groups, group, i) {
        ARR_TRUNCATE(group->commands, 0);
    }
}



/************************************/
/*  Single-output resources/output  */

int pg_gpu_render_stage_get_uniform_location(pg_gpu_render_stage_t* stage, const char* name)
{
    const struct pg_shader_interface_info* sh_iface = pg_gpu_shader_program_interface_info(
            pg_gpu_pipeline_get_shader(stage->pipeline));
    const struct pg_shader_uniform_info* uni_info = pg_shader_interface_info_get_dynamic_uniform_info(sh_iface, name);
    return uni_info->location;
}

void pg_gpu_render_stage_set_initial_uniform(pg_gpu_render_stage_t* stage, int location, pg_data_t* data)
{
    int i;
    struct stage_uniform* uni;
    ARR_FOREACH_PTR(stage->initial_uniforms, uni, i) {
        if(uni->gl_location == location) {
            pg_data_deinit(&uni->data);
            uni->data = pg_data_duplicate(data);
            return;
        }
    }
    ARR_PUSH(stage->initial_uniforms, (struct stage_uniform){
        .gl_location = location,
        .data = pg_data_duplicate(data)
    });
}

void pg_gpu_render_stage_set_group_uniform(pg_gpu_render_stage_t* stage, int group_idx, int location, pg_data_t* data)
{
    struct stage_input_group* group = &stage->input_groups.data[group_idx];
    int i;
    struct stage_uniform* uni;
    ARR_FOREACH_PTR(group->uniforms, uni, i) {
        if(uni->gl_location == location) {
            pg_data_deinit(&uni->data);
            uni->data = pg_data_duplicate(data);
            return;
        }
    }
    ARR_PUSH(group->uniforms, (struct stage_uniform){
        .gl_location = location,
        .data = pg_data_duplicate(data)
    });
}

void pg_gpu_render_stage_set_target_uniform(pg_gpu_render_stage_t* stage, int target_idx, int location, pg_data_t* data)
{
    struct stage_target* target = &stage->targets.data[target_idx];
    int i;
    struct stage_uniform* uni;
    ARR_FOREACH_PTR(target->uniforms, uni, i) {
        if(uni->gl_location == location) {
            pg_data_deinit(&uni->data);
            uni->data = pg_data_duplicate(data);
            return;
        }
    }
    ARR_PUSH(target->uniforms, (struct stage_uniform){
        .gl_location = location,
        .data = pg_data_duplicate(data)
    });
}

void pg_gpu_render_stage_set_targets_uniform(pg_gpu_render_stage_t* stage, uint32_t target_filter, int location, pg_data_t* data)
{
    int target_idx;
    struct stage_target* target;
    ARR_FOREACH_PTR(stage->targets, target, target_idx) {
        if(!((target->input_filter & target_filter))) continue;
        int i;
        struct stage_uniform* uni;
        ARR_FOREACH_PTR(target->uniforms, uni, i) {
            if(uni->gl_location == location) {
                pg_data_deinit(&uni->data);
                uni->data = pg_data_duplicate(data);
                return;
            }
        }
        ARR_PUSH(target->uniforms, (struct stage_uniform){
            .gl_location = location,
            .data = pg_data_duplicate(data)
        });
    }
}


pg_gpu_command_arr_t* pg_gpu_render_stage_get_target_commands(pg_gpu_render_stage_t* stage, int target_idx)
{
    return &stage->targets.data[target_idx].commands;
}

pg_gpu_command_arr_t* pg_gpu_render_stage_get_group_pre_commands(pg_gpu_render_stage_t* stage)
{
    return &stage->group_pre_commands;
}

pg_gpu_command_arr_t* pg_gpu_render_stage_get_group_commands(pg_gpu_render_stage_t* stage, int group_idx)
{
    return &stage->input_groups.data[group_idx].commands;
}



/************************/
/*  Rendering           */

static void apply_uniforms(struct stage_uniform* unis, int n_unis)
{
    for(int i = 0; i < n_unis; ++i) {
        struct pg_gpu_command uni_cmd = {
            .op = PG_GPU_COMMAND_SET_UNIFORM,
            .set_uniform = { .location = unis[i].gl_location, .data = unis[i].data }
        };
        pg_gpu_command_execute(&uni_cmd);
    }
}

static void execute_commands(pg_gpu_command_arr_t* commands)
{
    int i;
    struct pg_gpu_command* command;
    ARR_FOREACH_PTR(*commands, command, i) pg_gpu_command_execute(command);
}


/*  Single-output rendering */
void pg_gpu_render_stage_execute(pg_gpu_render_stage_t* stage, uint32_t output_filter)
{
    pg_gpu_pipeline_bind(stage->pipeline);
    apply_uniforms(stage->initial_uniforms.data, stage->initial_uniforms.len);

    PG_CHECK_GL(glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, stage->debug_name));

    int target_idx;
    struct stage_target* target;
    ARR_FOREACH_PTR(stage->targets, target, target_idx) {
        if(!((target->input_filter & output_filter))) continue;
        char target_name[128];
        snprintf(target_name, 128, "Target %d", target_idx);
        PG_CHECK_GL(glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, target_name));
        pg_gpu_framebuffer_dst(target->output);
        apply_uniforms(target->uniforms.data, target->uniforms.len);
        execute_commands(&target->commands);
        int input_group_idx;
        struct stage_input_group* input_group;
        ARR_FOREACH_PTR(stage->input_groups, input_group, input_group_idx) {
            if(!((input_group->output_filter & target->input_filter))) continue;
            char target_name[128];
            snprintf(target_name, 128, "Input group %.32s", input_group->name);
            PG_CHECK_GL(glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, target_name));
            pg_gpu_vertex_assembly_bind(input_group->vertex);
            pg_gpu_resource_set_bind(input_group->resources);
            apply_uniforms(input_group->uniforms.data, input_group->uniforms.len);
            execute_commands(&stage->group_pre_commands);
            execute_commands(&input_group->commands);
            PG_CHECK_GL(glPopDebugGroup());
        }
        PG_CHECK_GL(glPopDebugGroup());
    }

    PG_CHECK_GL(glPopDebugGroup());
}

