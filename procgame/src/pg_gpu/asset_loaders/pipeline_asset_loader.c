#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

bool pg_gpu_stencil_state_from_json(struct pg_gpu_stencil_state* ss, cJSON* json)
{
    cJSON* func_json, *sfail_json, *dfail_json, *pass_json,
            *func_ref_json, *func_mask_json, *write_mask_json, *enabled_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(8,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("func", cJSON_String, &func_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("func_mask", cJSON_Number, &func_mask_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("func_ref", cJSON_Number, &func_ref_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("op_stencil_fail", cJSON_String, &sfail_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("op_depth_fail", cJSON_String, &dfail_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("op_both_pass", cJSON_String, &pass_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("write_mask", cJSON_Number, &write_mask_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enabled", cJSON_Bool, &enabled_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid GPU stencil state JSON");
        return false;
    }
    *ss = PG_GPU_STENCIL_STATE();
    if(func_json) ss->func = pg_gpu_comparison_from_string(func_json->valuestring);
    if(func_mask_json) ss->func_mask = func_mask_json->valueint;
    if(func_ref_json) ss->func_ref = func_ref_json->valueint;
    if(sfail_json) ss->op_stencil_fail = pg_stencil_op_from_string(sfail_json->valuestring);
    if(dfail_json) ss->op_depth_fail = pg_stencil_op_from_string(dfail_json->valuestring);
    if(pass_json) ss->op_both_pass = pg_stencil_op_from_string(pass_json->valuestring);
    if(write_mask_json) ss->write_mask = write_mask_json->valueint;
    if(enabled_json) ss->enabled = cJSON_IsTrue(enabled_json);
    return true;
}

bool pg_gpu_depth_state_from_json(struct pg_gpu_depth_state* ds, cJSON* json)
{
    cJSON* func_json, *write_mask_json, *enabled_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(3,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("func", cJSON_String, &func_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("write_mask", cJSON_Number, &write_mask_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enabled", cJSON_Bool, &enabled_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid GPU depth state JSON");
        return false;
    }
    *ds = PG_GPU_DEPTH_STATE();
    if(func_json) ds->func = pg_gpu_comparison_from_string(func_json->valuestring);
    if(enabled_json) ds->enabled = cJSON_IsTrue(enabled_json);
    if(write_mask_json) ds->write_mask = write_mask_json->valueint;
    return true;
}

bool pg_gpu_blending_state_from_json(struct pg_gpu_blending_state* blend, cJSON* json)
{
    cJSON* enabled_json, *equation_json, *src_factor_json, *dst_factor_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("enabled", cJSON_Bool, &enabled_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("equation", cJSON_String, &equation_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("src_factor", cJSON_String, &src_factor_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("dst_factor", cJSON_String, &dst_factor_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid GPU blending state JSON");
        return false;
    }
    *blend = PG_GPU_BLENDING_STATE();
    if(enabled_json) blend->enabled = cJSON_IsTrue(enabled_json);
    if(equation_json) blend->equation = pg_gpu_blend_equation_from_string(equation_json->valuestring);
    if(src_factor_json) blend->src_factor = pg_gpu_blend_factor_from_string(src_factor_json->valuestring);
    if(dst_factor_json) blend->dst_factor = pg_gpu_blend_factor_from_string(dst_factor_json->valuestring);
    return true;
}

/****************************/
/*  GPU PIPELINE ASSETS     */
/****************************/

struct pipeline_asset_source {
    /*  Shader  */
    pg_asset_handle_t shader_asset;
    struct pg_gpu_depth_state depth_state;
    struct pg_gpu_stencil_state stencil_state;
    struct pg_gpu_blending_state blending_state;
};


/*  JSON -> struct pipeline_asset_source    */
static void* gpu_pipeline_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* def)
{
    pg_asset_manager_t* asset_mgr = asset_handle.mgr;

    /*  Validation  */
    cJSON* shader_json, *depth_json, *stencil_json, *blending_json;
    struct pg_json_layout asset_layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_ITEM("shader", PG_JSON_ASSET, &shader_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("depth_state", cJSON_Object, &depth_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("stencil_state", cJSON_Object, &stencil_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("blending_state", cJSON_Object, &blending_json),
    );
    if(!pg_load_json_layout(def, &asset_layout)) return NULL;

    struct pipeline_asset_source parsed_source = {};
    parsed_source.depth_state = PG_GPU_DEPTH_STATE();
    parsed_source.stencil_state = PG_GPU_STENCIL_STATE();
    parsed_source.blending_state = PG_GPU_BLENDING_STATE();

    if((depth_json && !pg_gpu_depth_state_from_json(&parsed_source.depth_state, depth_json))
    || (stencil_json && !pg_gpu_stencil_state_from_json(&parsed_source.stencil_state, stencil_json))
    || (blending_json && !pg_gpu_blending_state_from_json(&parsed_source.blending_state, blending_json))) {
        return NULL;
    }

    /*  Processing  */
    /*  Shader program  */
    parsed_source.shader_asset = pg_asset_from_json(asset_mgr, "pg_gpu_shader_program", NULL, shader_json);
    if(!pg_asset_exists(parsed_source.shader_asset)) return NULL;
    pg_asset_depends(asset_handle, parsed_source.shader_asset);

    /*  If everything went well, then allocate the real parsed source, and copy
        the temporary one into it   */
    struct pipeline_asset_source* final_parsed_source = malloc(sizeof(*final_parsed_source));
    *final_parsed_source = parsed_source;
    return final_parsed_source;
}

static void gpu_pipeline_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct pipeline_asset_source* pipeline_source = (struct pipeline_asset_source*)parsed_source;
    free(pipeline_source);
}



/*  struct pipeline_asset_source -> pg_gpu_pipeline_t*  */
static void* gpu_pipeline_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct pipeline_asset_source* pipeline_source = (struct pipeline_asset_source*)parsed_source;

    /*  Get the shader program and create an interface block for it */
    pg_gpu_shader_program_t* sh_prog = pg_asset_get_data(pipeline_source->shader_asset);

    return pg_gpu_pipeline_create(sh_prog,
            &pipeline_source->depth_state,
            &pipeline_source->stencil_state,
            &pipeline_source->blending_state);
}

static void gpu_pipeline_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_pipeline_destroy((pg_gpu_pipeline_t*)loaded_data);
}



struct pg_asset_loader* pg_gpu_pipeline_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_pipeline",
        .type_name_len = strlen("pg_gpu_pipeline"),
        .parse = gpu_pipeline_asset_parse,
        .unparse = gpu_pipeline_asset_unparse,
        .load = gpu_pipeline_asset_load,
        .unload = gpu_pipeline_asset_unload,
    };
    return loader;
}


