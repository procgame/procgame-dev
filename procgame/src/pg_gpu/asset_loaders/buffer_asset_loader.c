#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

bool pg_buffer_layout_from_json(struct pg_buffer_layout* layout, cJSON* json)
{
    /*  Validate    */
    cJSON* alignment_json, *packing_json, *packing_type_json, *attribs_json;
    struct pg_json_layout json_layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_ITEM("attributes",        cJSON_Array,  &attribs_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("attribute_packing", cJSON_String, &packing_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("element_alignment", cJSON_Number, &alignment_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("packing_data_type", cJSON_String, &packing_type_json)
    );
    if(!pg_load_json_layout(json, &json_layout)) return false;

    /*  Read data alignment requirement */
    enum pg_buffer_attribute_packing attrib_packing =
        packing_json ? pg_buffer_attribute_packing_from_string(packing_json->valuestring)
                     : PG_BUFFER_PACKING_PACKED;
    if(!attrib_packing) {
        pg_log(PG_LOG_ERROR, "Invalid attribute packing method: '%s'", packing_json->valuestring);
        return NULL;
    }

    enum pg_data_type packing_data_type = packing_type_json
            ? pg_data_type_from_string(packing_type_json->valuestring) : PG_NULL;

    /*  Read attribute layout */
    *layout = (struct pg_buffer_layout){
        .attribute_packing = attrib_packing,
        .packing_data_type = packing_data_type,
        .element_alignment = alignment_json ? alignment_json->valueint : 1 };
    SARR_INIT(layout->attributes);
    cJSON* attrib_json;
    cJSON_ArrayForEach(attrib_json, attribs_json) {
        /*  Each attribute is simple a "name": "type" pair in the 'layout' JSON object  */
        cJSON* name_json, *type_json;
        struct pg_json_layout attrib_json_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("name", cJSON_Array,  &name_json),
            PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
        );
        if(!pg_load_json_layout(attrib_json, &attrib_json_layout)) return false;
        enum pg_data_type attrib_type = pg_data_type_from_string(cJSON_GetStringValue(type_json));
        if(!attrib_type) {
            pg_log(PG_LOG_ERROR, "Buffer attribute '%s' has invalid type: '%s'",
                    cJSON_GetStringValue(name_json), cJSON_GetStringValue(type_json));
            return false;
        }

        struct pg_buffer_attribute_info* attrib_info = ARR_NEW(layout->attributes);
        strncpy(attrib_info->name, cJSON_GetStringValue(name_json), PG_BUFFER_ATTRIBUTE_NAME_MAX_LEN);
        attrib_info->type = attrib_type;
    }

    return true;
}

bool pg_buffer_data_from_json(struct pg_buffer* buf, cJSON* json)
{
    /*  Temporary structure for holding parsed data */
    union {
        float f[4];
        int32_t i[4];
        uint32_t u[4];
        int16_t s[4];
        uint16_t us[4];
        int8_t b[4];
        uint8_t ub[4];
        mat4 mat;
    } tmp;
    /*  Here, we are parsing this type of JSON structure:
        [ [ [ A0.x, A0.y, ... ], [A1.x, A1.y, ... ], ... ],
        ^ [ [ A0.x, A0.y, ... ], [A1.x, A1.y, ... ], ... ], ... ]
        | ^ ^
        | | |
        | | attrib_json
        | element_json
        data_json
    */
    int element_idx = 0;
    cJSON* element_json;
    cJSON_ArrayForEach(element_json, json) {
        /*  Check array bound   */
        if(element_idx >= buf->data_elements) break;
        /*  Non-arrays are simply ignored, allowing comments (strings) in the data array  */

        if(cJSON_IsNumber(element_json)) {
            uint8_t* out_ptr = pg_buffer_get_attribute_ptr(buf, element_idx, 0);
            if(!out_ptr) {
                pg_log(PG_LOG_ERROR, "Failed to get attribute pointer for buffer element.attribute (%d.0)", element_idx);
                return false;
            }
            enum pg_data_type attrib_type = buf->attributes.data[0].info.type;
            enum pg_data_type attrib_base_type = pg_data_base_type[attrib_type];
            if(attrib_type != attrib_base_type) {
                pg_log(PG_LOG_ERROR, "Flattened buffer data for non-flat buffer attribute (%d.0)", element_idx);
                return false;
            }
            /*  Use single-value readers for single-component types */
            switch(attrib_base_type) {
            case PG_FLOAT:  *((float*)tmp.f) = json_read_float(element_json); break;
            case PG_INT:    *((int32_t*)tmp.i) = json_read_int(element_json); break;
            case PG_UINT:   *((uint32_t*)tmp.u) = json_read_uint(element_json); break;
            case PG_SHORT:  *((int16_t*)tmp.s) = json_read_short(element_json); break;
            case PG_USHORT: *((uint16_t*)tmp.us) = json_read_ushort(element_json); break;
            case PG_BYTE:   *((int8_t*)tmp.b) = json_read_byte(element_json); break;
            case PG_UBYTE:  *((uint8_t*)tmp.ub) = json_read_ubyte(element_json); break;
            default: break;
            }
            memcpy(out_ptr, &tmp, pg_data_type_size[attrib_type]);
        } else if(cJSON_IsArray(element_json)) {
            /*  Iterate over attributes in the element  */
            int attrib_idx = 0;
            cJSON* attrib_json;
            cJSON_ArrayForEach(attrib_json, element_json) {
                uint8_t* out_ptr = pg_buffer_get_attribute_ptr(buf, element_idx, attrib_idx);
                if(!out_ptr) {
                    pg_log(PG_LOG_ERROR, "Failed to get attribute pointer for buffer element.attribute (%d.%d)",
                            element_idx, attrib_idx);
                    return false;
                }
                enum pg_data_type attrib_type = buf->attributes.data[attrib_idx].info.type;
                enum pg_data_type attrib_base_type = pg_data_base_type[attrib_type];
                int attrib_size = pg_data_type_size[attrib_type];
                if(attrib_type == PG_MAT4) {
                    tmp.mat = json_read_mat4(attrib_json);
                } else {
                    /*  We take advantage of the fact that these JSON utilities
                        automatically pad everything with 0 if no data is available */
                    switch(attrib_base_type) {
                    case PG_FLOAT:  *((vec4*)tmp.f) = json_read_vec4(attrib_json); break;
                    case PG_INT:    *((ivec4*)tmp.i) = json_read_ivec4(attrib_json); break;
                    case PG_UINT:   *((uvec4*)tmp.u) = json_read_uvec4(attrib_json); break;
                    case PG_SHORT:  *((svec4*)tmp.s) = json_read_svec4(attrib_json); break;
                    case PG_USHORT: *((usvec4*)tmp.us) = json_read_usvec4(attrib_json); break;
                    case PG_BYTE:   *((bvec4*)tmp.b) = json_read_bvec4(attrib_json); break;
                    case PG_UBYTE:  *((ubvec4*)tmp.ub) = json_read_ubvec4(attrib_json); break;
                    default: break;
                    }
                }
                memcpy(out_ptr, &tmp, attrib_size);
                ++attrib_idx;
            }
        }
        ++element_idx;
    }
    return true;
}


/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct buffer_asset_source {
    /*  required    */
    struct pg_buffer_layout layout;
    enum pg_gpu_buffer_type type;
    pg_buffer_range_arr_t ranges;
    int capacity;
    /*  optional    */
    bool source_has_data;
};


static void* gpu_buffer_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* def)
{
    cJSON* layout_json, *cap_json, *type_json, *data_json, *ranges_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(5,
        PG_JSON_LAYOUT_ITEM("type",             cJSON_String, &type_json),
        PG_JSON_LAYOUT_ITEM("capacity",         cJSON_Number, &cap_json),
        PG_JSON_LAYOUT_ITEM("layout",           cJSON_Object, &layout_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("data",    cJSON_Array,  &data_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("ranges",  cJSON_Array,  &ranges_json));
    if(!pg_load_json_layout(def, &layout)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has wrong JSON layout");
        return NULL;
    }

    /*  Read buffer type  */
    enum pg_gpu_buffer_type buf_type = pg_gpu_buffer_type_from_string(type_json->valuestring);
    if(!buf_type) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid type: '%s'", type_json->valuestring);
        return NULL;
    }

    /*  Read capacity value */
    int capacity = cap_json->valueint;
    if(capacity <= 0) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid capacity: %d", capacity);
        return NULL;
    }

    /*  Read attribute layout */
    struct pg_buffer_layout buf_layout;
    if(!pg_buffer_layout_from_json(&buf_layout, layout_json)) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid buffer layout");
        return NULL;
    }

    pg_buffer_range_arr_t parsed_ranges;
    SARR_INIT(parsed_ranges);

    /*  Read ranges */
    char range_var_name[HTABLE_KEY_LEN];
    snprintf(range_var_name, HTABLE_KEY_LEN, "%s.range[all]", pg_asset_get_name(asset_handle));
    pg_asset_manager_set_variable(asset_handle.mgr, range_var_name, pg_data_init(PG_IVEC2, 1, &ivec2(0, capacity)));
    if(ranges_json) {
        struct pg_buffer_range* range;
        cJSON* range_json;
        cJSON_ArrayForEach(range_json, ranges_json) {
            cJSON* name_json, *range_first_json, *range_count_json;
            struct pg_json_layout range_layout = PG_JSON_LAYOUT(3,
                PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
                PG_JSON_LAYOUT_ITEM("first", cJSON_Number, &range_first_json),
                PG_JSON_LAYOUT_ITEM("count", cJSON_Number, &range_count_json));
            if(!pg_load_json_layout(range_json, &range_layout)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid range definition");
                return NULL;
            }
            range = ARR_NEW(parsed_ranges);
            strncpy(range->name, name_json->valuestring, 128);
            range->first = range_first_json->valueint;
            range->count = range_count_json->valueint;
            snprintf(range_var_name, HTABLE_KEY_LEN, "%s.range[%s]", pg_asset_get_name(asset_handle), range->name);
            pg_asset_manager_set_variable(asset_handle.mgr, range_var_name, pg_data_init(PG_IVEC2, 1, &range->range));
        }
    }


    /*  Create the parsed source object    */
    struct buffer_asset_source* parsed_source = malloc(sizeof(*parsed_source));
    *parsed_source = (struct buffer_asset_source){
        .layout = buf_layout,
        .type = buf_type,
        .capacity = capacity,
        .source_has_data = data_json ? true : false,
        .ranges = parsed_ranges,
    };
    return parsed_source;
}



static void gpu_buffer_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct buffer_asset_source* buf_source = (struct buffer_asset_source*)parsed_source;
    ARR_DEINIT(buf_source->layout.attributes);
    free(buf_source);
}



static void* gpu_buffer_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct buffer_asset_source* buf_source = (struct buffer_asset_source*)parsed_source;

    /*  Allocate memory for the loaded data */
    struct pg_buffer* buf = malloc(sizeof(*buf));
    pg_buffer_init(buf, &buf_source->layout, buf_source->capacity);
    ARR_CONCAT(buf->ranges, buf_source->ranges);
    bool success = true;
    
    /*  If the buffer's JSON source had the data in it, we have to get it back
        here to load the data    */
    if(buf_source->source_has_data) {
        cJSON* source_json = pg_asset_get_source(asset_handle);
        cJSON* data_json = cJSON_GetObjectItem(source_json, "data");
        if(!pg_buffer_data_from_json(buf, data_json)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to load data from JSON source");
            success = false;
        }
    }

    /*  Upload our local memory buffer to the GPU   */
    pg_gpu_buffer_t* gpu_buf = NULL;
    if(success) {
        gpu_buf = pg_buffer_upload(buf, buf_source->type, true);
        if(!gpu_buf) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to upload to GPU");
            success = false;
        }
    }

    /*  Free our memory if we failed to load the asset  */
    if(!success) {
        pg_buffer_deinit(buf);
        return NULL;
    } 

    return gpu_buf;
}



static void gpu_buffer_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_buffer_t* buf_asset = (pg_gpu_buffer_t*)loaded_data;
    pg_gpu_buffer_deinit(buf_asset);
}

struct pg_asset_loader* pg_gpu_buffer_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_buffer",
        .type_name_len = strlen("pg_gpu_buffer"),
        .parse = gpu_buffer_asset_parse,
        .unparse = gpu_buffer_asset_unparse,
        .load = gpu_buffer_asset_load,
        .unload = gpu_buffer_asset_unload,
    };
    return loader;
}




