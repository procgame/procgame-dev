#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

/****************************/
/*  GPU RESOURCE SET ASSETS */
/****************************/

struct resource_set_asset_source {
    struct {
        pg_asset_handle_t binding;
        struct pg_gpu_texture_params params;
        bool is_buffer;
    } textures[16];
    pg_asset_handle_t uniform_buffers[16];
};


/*  JSON -> struct resource_set_asset_source    */
static void* resource_set_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* def)
{
    /*  Validation  */
    cJSON* textures_json, *uniform_buffers_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_ITEM("textures", cJSON_Array, &textures_json),
        PG_JSON_LAYOUT_ITEM("uniform_buffers", cJSON_Array, &uniform_buffers_json),
    );
    if(!pg_load_json_layout(def, &layout)) return NULL;
    struct resource_set_asset_source parsed_source = {};

    /*  Parse texture unit bindings  */
    int i = 0;
    cJSON* texture_binding_json;
    cJSON_ArrayForEach(texture_binding_json, textures_json) {
        if(i >= 16) break;
        if(cJSON_IsNull(texture_binding_json)) { ++i; continue; }
        pg_asset_handle_t* bind_asset = &parsed_source.textures[i].binding;
        cJSON* texture_json = cJSON_GetObjectItem(texture_binding_json, "texture");
        cJSON* buffer_json = cJSON_GetObjectItem(texture_binding_json, "buffer");
        if(texture_json) {
            *bind_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_texture", NULL, texture_json);
            cJSON* params_json = cJSON_GetObjectItem(texture_binding_json, "params");
            if(params_json && !pg_gpu_texture_params_from_json(&parsed_source.textures[i].params, params_json)) {
                return NULL;
            }
        } else if(buffer_json) {
            *bind_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_buffer", NULL, buffer_json);
            parsed_source.textures[i].is_buffer = true;
        } else {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid texture binding description %d", i);
        }
        pg_asset_depends(asset_handle, *bind_asset);
        ++i;
    }

    /*  Parse uniform buffer bindings   */
    i = 0;
    cJSON* buffer_binding_json;
    cJSON_ArrayForEach(buffer_binding_json, uniform_buffers_json) {
        if(i >= 16) break;
        if(cJSON_IsNull(buffer_binding_json)
        || cJSON_GetArraySize(buffer_binding_json) == 0) {
            ++i; continue;
        }
        cJSON* buffer_json = cJSON_GetObjectItem(buffer_binding_json, "buffer");
        if(!buffer_json) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid uniform buffer binding description %d", i);
            ++i; continue;
        }
        pg_asset_handle_t* bind_asset = &parsed_source.uniform_buffers[i];
        *bind_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_buffer", NULL, buffer_json);
        pg_asset_depends(asset_handle, *bind_asset);
        ++i;
    }

    /*  If everything went well, then allocate the real parsed source, and copy
        the temporary one into it   */
    struct resource_set_asset_source* final_parsed_source = malloc(sizeof(*final_parsed_source));
    *final_parsed_source = parsed_source;
    return final_parsed_source;
}

static void resource_set_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct resource_set_asset_source* resource_set_source = (struct resource_set_asset_source*)parsed_source;
    free(resource_set_source);
}



static void* resource_set_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct resource_set_asset_source* source = (struct resource_set_asset_source*)parsed_source;
    pg_gpu_resource_set_t* set = pg_gpu_resource_set_create();

    int i;
    for(i = 0; i < 16; ++i) {
        if(pg_asset_exists(source->textures[i].binding)) {
            if(source->textures[i].is_buffer) {
                pg_gpu_buffer_t* buffer = (pg_gpu_buffer_t*)pg_asset_get_data(source->textures[i].binding);
                pg_gpu_resource_set_attach_texture_buffer(set, i, buffer);
            } else {
                pg_gpu_texture_t* texture = (pg_gpu_texture_t*)pg_asset_get_data(source->textures[i].binding);
                pg_gpu_resource_set_attach_texture(set, i, texture);
                pg_gpu_resource_set_attach_texture_params(set, i, &source->textures[i].params);
            }
        }
        if(pg_asset_exists(source->uniform_buffers[i])) {
            pg_gpu_buffer_t* buffer = (pg_gpu_buffer_t*)pg_asset_get_data(source->uniform_buffers[i]);
            pg_gpu_resource_set_attach_buffer(set, i, buffer);
        }
    }

    return set;
}

static void resource_set_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_resource_set_destroy((pg_gpu_resource_set_t*)loaded_data);
}



struct pg_asset_loader* pg_gpu_resource_set_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_resource_set",
        .type_name_len = strlen("pg_gpu_resource_set"),
        .parse = resource_set_asset_parse,
        .unparse = resource_set_asset_unparse,
        .load = resource_set_asset_load,
        .unload = resource_set_asset_unload,
    };
    return loader;
}


