#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

/****************************/
/*  GPU PIPELINE ASSETS     */
/****************************/

struct renderer_asset_source {
    /*  Shader  */
    SARR_T(128, pg_asset_handle_t) stage_assets;
};


/*  JSON -> struct renderer_asset_source    */
static void* gpu_renderer_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* json)
{
    pg_asset_manager_t* asset_mgr = asset_handle.mgr;

    /*  Validation  */
    cJSON* stages_json;
    struct pg_json_layout asset_layout = PG_JSON_LAYOUT(1,
        PG_JSON_LAYOUT_ITEM("stages", cJSON_Array, &stages_json),
    );
    if(!pg_load_json_layout(json, &asset_layout)) return NULL;

    struct renderer_asset_source parsed_source = {};
    SARR_INIT(parsed_source.stage_assets);
    cJSON* stage_json;
    cJSON_ArrayForEach(stage_json, stages_json) {
        pg_asset_handle_t stage_asset = pg_asset_from_json(asset_mgr, "pg_gpu_render_stage", NULL, stage_json);
        if(!pg_asset_exists(stage_asset)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "includes non-existent render stage");
            return NULL;
        }
        pg_asset_depends(asset_handle, stage_asset);
        ARR_PUSH(parsed_source.stage_assets, stage_asset);
    }

    /*  If everything went well, then allocate the real parsed source, and copy
        the temporary one into it   */
    struct renderer_asset_source* final_parsed_source = malloc(sizeof(*final_parsed_source));
    *final_parsed_source = parsed_source;
    return final_parsed_source;
}

static void gpu_renderer_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct renderer_asset_source* renderer_source = (struct renderer_asset_source*)parsed_source;
    free(renderer_source);
}



/*  struct renderer_asset_source -> pg_gpu_renderer_t*  */
static void* gpu_renderer_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct renderer_asset_source* source = (struct renderer_asset_source*)parsed_source;

    pg_gpu_renderer_t* render = pg_gpu_renderer_create();

    int i;
    pg_asset_handle_t stage_asset;
    ARR_FOREACH(source->stage_assets, stage_asset, i) {
        pg_gpu_render_stage_t* stage = pg_asset_get_data(stage_asset);
        pg_gpu_renderer_add_stage(render, stage);
    }

    return render;
}

static void gpu_renderer_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_renderer_destroy((pg_gpu_renderer_t*)loaded_data);
}



struct pg_asset_loader* pg_gpu_renderer_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_renderer",
        .type_name_len = strlen("pg_gpu_renderer"),
        .parse = gpu_renderer_asset_parse,
        .unparse = gpu_renderer_asset_unparse,
        .load = gpu_renderer_asset_load,
        .unload = gpu_renderer_asset_unload,
    };
    return loader;
}




