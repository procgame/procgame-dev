#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_vr/pg_vr.h"
#include "pg_util/pg_util.h"

/****************************/
/*  GPU PIPELINE ASSETS     */
/****************************/

typedef SARR_T(16, struct uni_source {
    char name[PG_SHADER_INPUT_NAME_MAXLEN];
    int location;
    pg_data_t data;
}) uni_source_array_t;

struct input_group_source {
    char name[128];
    uint32_t filter;
    pg_asset_handle_t resources_asset;
    pg_asset_handle_t vertex_asset;
    uni_source_array_t uniforms;
    cJSON* commands_json;
};

struct output_source {
    uint32_t filter;
    pg_asset_handle_t output_asset;
    uni_source_array_t uniforms;
    cJSON* commands_json;
};

struct render_stage_asset_source {
    pg_asset_handle_t pipeline_asset;
    uni_source_array_t initial_uniforms;
    SARR_T(16, struct input_group_source) input_groups;
    SARR_T(16, struct output_source) outputs;
    cJSON* group_pre_commands_json;
};


void parse_uni_sources(uni_source_array_t* sources_out, cJSON* uniforms_json)
{
    bool success = true;
    cJSON* uni_json;
    cJSON_ArrayForEach(uni_json, uniforms_json) {
        cJSON* location_json = cJSON_GetObjectItem(uni_json, "location");
        cJSON* name_json = cJSON_GetObjectItem(uni_json, "name");
        if(!location_json && !name_json) {
            success = false;
            break;
        }
        struct uni_source* new_uni = ARR_NEW(*sources_out);
        *new_uni = (struct uni_source){};
        if(name_json) strncpy(new_uni->name, name_json->valuestring, PG_SHADER_INPUT_NAME_MAXLEN);
        else if(location_json) new_uni->location = location_json->valueint;
        new_uni->data = json_read_pg_data(uni_json);
        if(!new_uni->data.type) {
            success = false;
            break;
        }
    }
    if(!success) {
        for(int i = 0; i < sources_out->len; ++i) {
            pg_data_deinit(&sources_out->data[i].data);
        }
    }
}

uint32_t parse_filters(cJSON* json)
{
    uint32_t out = 0;
    cJSON* filter_json;
    cJSON_ArrayForEach(filter_json, json) {
        if(!cJSON_IsString(filter_json)) continue;
        if(strcmp(filter_json->valuestring, "main") == 0) out |= PG_TARGET_FILTER_MAIN;
        else if(strcmp(filter_json->valuestring, "vr_left") == 0) out |= PG_TARGET_FILTER_VR_LEFT;
        else if(strcmp(filter_json->valuestring, "vr_right") == 0) out |= PG_TARGET_FILTER_VR_RIGHT;
        else if(strcmp(filter_json->valuestring, "vr_both") == 0) out |= PG_TARGET_FILTER_VR_BOTH;
        else if(strcmp(filter_json->valuestring, "all") == 0) out |= PG_TARGET_FILTER_ALL;
    }
    return out;
}

bool parse_input_group(pg_asset_handle_t stage_asset, struct input_group_source* group_source, cJSON* json)
{
    cJSON* name_json, *resources_json, *vertex_json, *uniforms_json, *commands_json, *filters_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(6,
        PG_JSON_LAYOUT_ITEM("resources", PG_JSON_ASSET, &resources_json),
        PG_JSON_LAYOUT_ITEM("vertex", PG_JSON_ASSET, &vertex_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("name", cJSON_String, &name_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("uniforms", cJSON_Array, &uniforms_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("commands", cJSON_Array, &commands_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("targets", cJSON_Array, &filters_json),
    );
    if(!pg_load_json_layout(json, &layout)) return false;

    struct input_group_source new_group = {};
    if(name_json) strncpy(new_group.name, name_json->valuestring, 128);

    if(filters_json) new_group.filter = parse_filters(filters_json);
    else new_group.filter = PG_TARGET_FILTER_ALL;

    if(!pg_asset_dependency_from_json(stage_asset, &new_group.resources_asset, "pg_gpu_resource_set", NULL, resources_json)
    || !pg_asset_dependency_from_json(stage_asset, &new_group.vertex_asset, "pg_gpu_vertex_assembly", NULL, vertex_json)) return false;

    if(uniforms_json) parse_uni_sources(&new_group.uniforms, uniforms_json);
    if(commands_json) new_group.commands_json = cJSON_Duplicate(commands_json, true);

    *group_source = new_group;
    return true;
}

bool parse_output(pg_asset_handle_t stage_asset, struct output_source* output_source, cJSON* json)
{
    cJSON* output_json, *filters_json, *uniforms_json, *commands_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_ITEM("output", PG_JSON_ASSET, &output_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("filter", cJSON_Array, &filters_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("uniforms", cJSON_Array, &uniforms_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("commands", cJSON_Array, &commands_json),
    );
    if(!pg_load_json_layout(json, &layout)) return false;
    if(!pg_asset_dependency_from_json(stage_asset, &output_source->output_asset, "pg_gpu_framebuffer", NULL, output_json)) return false;
    if(uniforms_json) parse_uni_sources(&output_source->uniforms, uniforms_json);
    if(commands_json) output_source->commands_json = cJSON_Duplicate(commands_json, true);
    if(filters_json) output_source->filter = parse_filters(filters_json);
    else output_source->filter = PG_TARGET_FILTER_ALL;
    return true;
}


/*  JSON -> struct render_stage_asset_source    */
static void* gpu_render_stage_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* json)
{
    /*  Validation  */
    cJSON* pipeline_json, *input_groups_json, *outputs_json, *uniforms_json, *group_pre_commands_json;
    struct pg_json_layout asset_layout = PG_JSON_LAYOUT(5,
        PG_JSON_LAYOUT_ITEM("pipeline", PG_JSON_ASSET, &pipeline_json),
        PG_JSON_LAYOUT_ITEM("targets", cJSON_Array, &outputs_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("input_groups", cJSON_Array, &input_groups_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("initial_uniforms", cJSON_Array, &uniforms_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("group_pre_commands", cJSON_Array, &group_pre_commands_json),
    );
    if(!pg_load_json_layout(json, &asset_layout)) return NULL;
    struct render_stage_asset_source parsed_source = {};
    SARR_INIT(parsed_source.initial_uniforms);
    SARR_INIT(parsed_source.outputs);
    SARR_INIT(parsed_source.input_groups);

    if(!pg_asset_dependency_from_json(asset_handle, &parsed_source.pipeline_asset,
            "pg_gpu_pipeline", NULL, pipeline_json)) return NULL;


    /*  Parse outputs   */
    cJSON* output_json;
    cJSON_ArrayForEach(output_json, outputs_json) {
        struct output_source* new_output = ARR_NEW(parsed_source.outputs);
        SARR_INIT(new_output->uniforms);
        if(!parse_output(asset_handle, new_output, output_json)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to parse output %d", parsed_source.outputs.len - 1);
            return NULL;
        }
    }


    /*  Parse input groups  */
    if(input_groups_json) {
        bool success = true;
        cJSON* input_group_json;
        cJSON_ArrayForEach(input_group_json, input_groups_json) {
            struct input_group_source* new_group_source = ARR_NEW(parsed_source.input_groups);
            SARR_INIT(new_group_source->uniforms);
            if(!parse_input_group(asset_handle, new_group_source, input_group_json)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to parse input group %d", parsed_source.input_groups.len - 1);
                success = false;
                break;
            }
        }
        if(!success) {
            int i;
            struct input_group_source* group;
            ARR_FOREACH_PTR(parsed_source.input_groups, group, i) {
                if(group->commands_json) cJSON_Delete(group->commands_json);
            }
            return NULL;
        }
    }

    /*  Parse initial uniforms  */
    if(uniforms_json) parse_uni_sources(&parsed_source.initial_uniforms, uniforms_json);
    if(group_pre_commands_json) parsed_source.group_pre_commands_json = cJSON_Duplicate(group_pre_commands_json, true);

    /*  If everything went well, then allocate the real parsed source, and copy the temporary one into it   */
    struct render_stage_asset_source* final_parsed_source = malloc(sizeof(*final_parsed_source));
    *final_parsed_source = parsed_source;
    return final_parsed_source;
}

static void gpu_render_stage_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct render_stage_asset_source* render_stage_source = (struct render_stage_asset_source*)parsed_source;
    int i;
    struct input_group_source* group;
    ARR_FOREACH_PTR(render_stage_source->input_groups, group, i) {
        if(group->commands_json) cJSON_Delete(group->commands_json);
        for(int j = 0; j < group->uniforms.len; ++j) {
            pg_data_deinit(&group->uniforms.data[j].data);
        }
    }
    for(i = 0; i < render_stage_source->initial_uniforms.len; ++i) {
        pg_data_deinit(&render_stage_source->initial_uniforms.data[i].data);
    }
    free(render_stage_source);
}



/*  struct render_stage_asset_source -> pg_gpu_render_stage_t*  */
static void* gpu_render_stage_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct render_stage_asset_source* source = (struct render_stage_asset_source*)parsed_source;

    pg_gpu_pipeline_t* pipeline = pg_asset_get_data(source->pipeline_asset);
    pg_gpu_shader_program_t* sh_prog = pg_gpu_pipeline_get_shader(pipeline);
    const struct pg_shader_interface_info* sh_iface = pg_gpu_shader_program_interface_info(sh_prog);
    pg_gpu_render_stage_t* stage = pg_gpu_render_stage_create(pipeline);
    pg_gpu_render_stage_set_debug_name(stage, pg_asset_get_name(asset_handle));

    int i;
    struct uni_source* uni;
    ARR_FOREACH_PTR(source->initial_uniforms, uni, i) {
        int location = uni->name[0] ? pg_gpu_render_stage_get_uniform_location(stage, uni->name)
                                    : uni->location;
        pg_gpu_render_stage_set_initial_uniform(stage, location, &uni->data);
    }

    struct output_source* output_source;
    ARR_FOREACH_PTR(source->outputs, output_source, i) {
        pg_gpu_framebuffer_t* output = pg_asset_get_data(output_source->output_asset);
        int target_idx = pg_gpu_render_stage_add_target(stage, output_source->filter, output);
        if(target_idx == -1) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to add target %d to the render stage", i);
            pg_gpu_render_stage_destroy(stage);
            return NULL;
        }
        if(output_source->commands_json) {
            pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_target_commands(stage, target_idx);
            if(!pg_gpu_commands_from_asset_json(commands, output_source->commands_json, asset_handle.mgr, sh_iface)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "target %d failed to parse commands from JSON", i);
                pg_gpu_render_stage_destroy(stage);
                return NULL;
            }
        }
    }

    struct input_group_source* input_group_source;
    ARR_FOREACH_PTR(source->input_groups, input_group_source, i) {
        pg_gpu_resource_set_t* resources = pg_asset_get_data(input_group_source->resources_asset);
        pg_gpu_vertex_assembly_t* vertex = pg_asset_get_data(input_group_source->vertex_asset);
        uint32_t filter = input_group_source->filter;
        int input_group_idx = pg_gpu_render_stage_add_input_group(stage, filter, input_group_source->name, vertex, resources);
        if(input_group_idx == -1) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to add input group %d to the render stage", i);
            pg_gpu_render_stage_destroy(stage);
            return NULL;
        }
        if(input_group_source->commands_json) {
            pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_commands(stage, input_group_idx);
            if(!pg_gpu_commands_from_asset_json(commands, input_group_source->commands_json, asset_handle.mgr, sh_iface)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "input group %d failed to parse commands from JSON", i);
                pg_gpu_render_stage_destroy(stage);
                return NULL;
            }
        }
        int j;
        struct uni_source* group_uni;
        ARR_FOREACH_PTR(input_group_source->uniforms, group_uni, j) {
            int location = uni->name[0] ? pg_gpu_render_stage_get_uniform_location(stage, group_uni->name)
                                        : uni->location;
            pg_gpu_render_stage_set_group_uniform(stage, input_group_idx, location, &group_uni->data);
        }
    }

    if(source->group_pre_commands_json) {
        pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_pre_commands(stage);
        if(!pg_gpu_commands_from_asset_json(commands, source->group_pre_commands_json, asset_handle.mgr, sh_iface)) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to parse group pre-commands from JSON", i);
            pg_gpu_render_stage_destroy(stage);
            return NULL;
        }
    }

    return stage;
}

static void gpu_render_stage_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_render_stage_destroy((pg_gpu_render_stage_t*)loaded_data);
}



struct pg_asset_loader* pg_gpu_render_stage_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_render_stage",
        .type_name_len = strlen("pg_gpu_render_stage"),
        .parse = gpu_render_stage_asset_parse,
        .unparse = gpu_render_stage_asset_unparse,
        .load = gpu_render_stage_asset_load,
        .unload = gpu_render_stage_asset_unload,
    };
    return loader;
}



