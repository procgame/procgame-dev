#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

static void* gpu_framebuffer_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* def);

static void gpu_framebuffer_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source);

static void* gpu_framebuffer_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source);

static void gpu_framebuffer_asset_unload(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* loaded_data);

struct pg_asset_loader* pg_gpu_framebuffer_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_framebuffer",
        .type_name_len = strlen("pg_gpu_framebuffer"),
        .parse = gpu_framebuffer_asset_parse,
        .unparse = gpu_framebuffer_asset_unparse,
        .load = gpu_framebuffer_asset_load,
        .unload = gpu_framebuffer_asset_unload,
    };
    return loader;
}



/*  Implementation  */

struct framebuffer_asset_source {
    int n_attachments;
    enum pg_gpu_framebuffer_attachment_index attachments[16];
    bool stereo;
    pg_asset_handle_t stereo_left;
    pg_asset_handle_t stereo_right;
    pg_asset_handle_t texture_assets[16];
    pg_asset_handle_t texture_assets_right[16];
    int texture_layers[16];
    ivec2 dimensions;
};


/*  JSON -> struct framebuffer_asset_source */
static void* gpu_framebuffer_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* def)
{
    /*  A single member: "attachments"  */
    cJSON* attachments_list_json, *stereo_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("stereo", cJSON_Bool, &stereo_json),
        PG_JSON_LAYOUT_ITEM("attachments", cJSON_Array, &attachments_list_json));
    if(!pg_load_json_layout(def, &layout)) return NULL;

    bool stereo = stereo_json ? cJSON_IsTrue(stereo_json) : false;

    /*  Temporary parsed data   */
    struct framebuffer_asset_source parsed_data = {0};

    if(stereo) {
        parsed_data.stereo = true;
        parsed_data.stereo_left = pg_asset_declare_child(asset_handle, "pg_gpu_framebuffer", "stereo_left");
        parsed_data.stereo_right = pg_asset_declare_child(asset_handle, "pg_gpu_framebuffer", "stereo_right");
    }

    /*  Iterate over the attachments array  */
    int attachment_idx = 0;
    char attachment_idx_string[64];
    cJSON* attachment_json;
    cJSON_ArrayForEach(attachment_json, attachments_list_json) {
        snprintf(attachment_idx_string, 64, "texture attachment %d", attachment_idx);
        if(stereo) {
            /*  Validate texture attachment JSON    */
            cJSON* tex_left_json, *tex_right_json, *layer_json, *attachment_location_json;
            struct pg_json_layout attachment_layout = PG_JSON_LAYOUT(4,
                PG_JSON_LAYOUT_ITEM("texture_left", PG_JSON_ASSET, &tex_left_json),
                PG_JSON_LAYOUT_ITEM("texture_right", PG_JSON_ASSET, &tex_right_json),
                PG_JSON_LAYOUT_OPTIONAL_ITEM("layer", cJSON_Number, &layer_json),
                PG_JSON_LAYOUT_ITEM("attachment_location", cJSON_String, &attachment_location_json),
            );
            if(!pg_load_json_layout(attachment_json, &attachment_layout)) return NULL;
            pg_asset_handle_t tex_asset_left = pg_asset_from_json(asset_handle.mgr, "pg_gpu_texture", NULL, tex_left_json);
            pg_asset_handle_t tex_asset_right = pg_asset_from_json(asset_handle.mgr, "pg_gpu_texture", NULL, tex_right_json);
            if(!pg_asset_exists(tex_asset_left) || !pg_asset_exists(tex_asset_right)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "texture attachment %d failed to parse");
                return NULL;
            }
            pg_asset_depends(asset_handle, tex_asset_left);
            pg_asset_depends(asset_handle, tex_asset_right);
            enum pg_gpu_framebuffer_attachment_index attachment_location =
                pg_gpu_framebuffer_attachment_index_from_string(attachment_location_json->valuestring);
            if(attachment_location == PG_GPU_FRAMEBUFFER_INVALID) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid texture attachment location: %s", attachment_location_json->valuestring);
                return NULL;
            }
            parsed_data.texture_assets[attachment_idx] = tex_asset_left;
            parsed_data.texture_assets_right[attachment_idx] = tex_asset_right;
            parsed_data.texture_layers[attachment_idx] = layer_json ? layer_json->valueint : 0;
            parsed_data.attachments[attachment_idx] = attachment_location;
        } else {
            /*  Validate texture attachment JSON    */
            cJSON* texture_json, *layer_json, *attachment_location_json;
            struct pg_json_layout attachment_layout = PG_JSON_LAYOUT(3,
                PG_JSON_LAYOUT_ITEM("texture", PG_JSON_ASSET, &texture_json),
                PG_JSON_LAYOUT_OPTIONAL_ITEM("layer", cJSON_Number, &layer_json),
                PG_JSON_LAYOUT_ITEM("attachment_location", cJSON_String, &attachment_location_json),
            );
            if(!pg_load_json_layout(attachment_json, &attachment_layout)) return NULL;
            pg_asset_handle_t texture_asset = pg_asset_from_json(asset_handle.mgr, "pg_gpu_texture", NULL, texture_json);
            if(!pg_asset_exists(texture_asset)) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "texture attachment %d failed to parse");
                return NULL;
            }
            pg_asset_depends(asset_handle, texture_asset);
            enum pg_gpu_framebuffer_attachment_index attachment_location =
                pg_gpu_framebuffer_attachment_index_from_string(attachment_location_json->valuestring);
            if(attachment_location == PG_GPU_FRAMEBUFFER_INVALID) {
                pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid texture attachment location: %s", attachment_location_json->valuestring);
                return NULL;
            }

            /*  Processing  */
            parsed_data.texture_assets[attachment_idx] = texture_asset;
            parsed_data.texture_layers[attachment_idx] = layer_json ? layer_json->valueint : 0;
            parsed_data.attachments[attachment_idx] = attachment_location;
        }

        if(++attachment_idx >= 16) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has too many texture attachments");
            return NULL;
        }
    }

    parsed_data.n_attachments = attachment_idx;

    /*  If everything was ok, then we allocate the parsed data here and copy the
        temporary one into it   */
    struct framebuffer_asset_source* final_parsed_data = malloc(sizeof(*final_parsed_data));
    *final_parsed_data = parsed_data;
    return final_parsed_data;
}

static void gpu_framebuffer_asset_unparse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    free(parsed_source);
}

/*  struct framebuffer_asset_source -> pg_gpu_framebuffer_t*    */
static void* gpu_framebuffer_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct framebuffer_asset_source* fbuf_source = (struct framebuffer_asset_source*)parsed_source;
    if(fbuf_source->stereo) {
        pg_gpu_texture_view_t* tex_views_left[16] = {0};
        pg_gpu_texture_view_t* tex_views_right[16] = {0};
        for(int i = 0; i < fbuf_source->n_attachments; ++i) {
            pg_asset_handle_t tex_asset_left = fbuf_source->texture_assets[i];
            pg_asset_handle_t tex_asset_right = fbuf_source->texture_assets_right[i];
            pg_gpu_texture_t* gpu_tex_left = pg_asset_get_data(tex_asset_left);
            pg_gpu_texture_t* gpu_tex_right = pg_asset_get_data(tex_asset_right);
            tex_views_left[i] = pg_gpu_texture_get_layer_view(gpu_tex_left, fbuf_source->texture_layers[i]);
            tex_views_right[i] = pg_gpu_texture_get_layer_view(gpu_tex_right, fbuf_source->texture_layers[i]);
        }
        pg_gpu_framebuffer_t* gpu_fbuf_left = pg_gpu_framebuffer_init_attachments(
                fbuf_source->n_attachments, tex_views_left, fbuf_source->attachments);
        pg_gpu_framebuffer_t* gpu_fbuf_right = pg_gpu_framebuffer_init_attachments(
                fbuf_source->n_attachments, tex_views_right, fbuf_source->attachments);
        pg_asset_from_data(asset_handle.mgr, "pg_gpu_framebuffer", pg_asset_get_name(fbuf_source->stereo_left), gpu_fbuf_left, false);
        pg_asset_from_data(asset_handle.mgr, "pg_gpu_framebuffer", pg_asset_get_name(fbuf_source->stereo_right), gpu_fbuf_right, true);
        return gpu_fbuf_left;
    } else {
        pg_gpu_texture_view_t* tex_views[16] = {0};
        for(int i = 0; i < fbuf_source->n_attachments; ++i) {
            pg_asset_handle_t tex_asset = fbuf_source->texture_assets[i];
            pg_gpu_texture_t* gpu_tex = pg_asset_get_data(tex_asset);
            tex_views[i] = pg_gpu_texture_get_layer_view(gpu_tex, fbuf_source->texture_layers[i]);
        }
        pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init_attachments(
                fbuf_source->n_attachments, tex_views, fbuf_source->attachments);
        return gpu_fbuf;
    }
}

static void gpu_framebuffer_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_framebuffer_t* gpu_fbuf = (pg_gpu_framebuffer_t*)loaded_data;
    pg_gpu_framebuffer_deinit(gpu_fbuf);
}


