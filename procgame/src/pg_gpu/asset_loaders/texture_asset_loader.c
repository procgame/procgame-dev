#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

bool pg_tex_frame_from_json(pg_tex_frame_t* frame, cJSON* json)
{
    if(!cJSON_IsArray(json) || !json->child || !json->child->next || !json->child->next->next) return false;
    frame->uv[0] = json_read_vec2(json->child);
    frame->uv[1] = json_read_vec2(json->child->next);
    frame->layer = json_read_int(json->child->next->next);
    return true;
}

enum pg_gpu_texture_param_wrap pg_gpu_texture_param_wrap_from_string(const char* str)
{
    if(strcmp(str, "clamp_to_edge") == 0) return PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_EDGE;
    else if(strcmp(str, "clamp_to_border") == 0) return PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_BORDER;
    else if(strcmp(str, "clamp_to_repeat") == 0) return PG_GPU_TEXTURE_PARAM_WRAP_REPEAT;
    else if(strcmp(str, "clamp_to_mirrored_repeat") == 0) return PG_GPU_TEXTURE_PARAM_WRAP_MIRRORED_REPEAT;
    else {
        pg_log(PG_LOG_ERROR, "Invalid GPU texture wrapping value '%s'", str);
        return PG_GPU_TEXTURE_PARAM_WRAP_CLAMP_TO_EDGE;
    }
}

enum pg_gpu_texture_param_filter pg_gpu_texture_param_filter_from_string(const char* str)
{
    if(strcmp(str, "nearest") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_NEAREST;
    else if(strcmp(str, "linear") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_LINEAR;
    else if(strcmp(str, "nearest_mipmap_nearest") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_NEAREST;
    else if(strcmp(str, "linear_mipmap_nearest") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_NEAREST;
    else if(strcmp(str, "nearest_mipmap_linear") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_NEAREST_MIPMAP_LINEAR;
    else if(strcmp(str, "linear_mipmap_linear") == 0) return PG_GPU_TEXTURE_PARAM_FILTER_LINEAR_MIPMAP_LINEAR;
    else {
        pg_log(PG_LOG_ERROR, "Invalid GPU texture filter value '%s'", str);
        return PG_GPU_TEXTURE_PARAM_FILTER_NEAREST;
    }
}

enum pg_gpu_texture_param_swizzle pg_gpu_texture_param_swizzle_from_string(const char* str)
{
    if(strcmp(str, "red") == 0) return PG_GPU_TEXTURE_PARAM_SWIZZLE_RED;
    else if(strcmp(str, "green") == 0) return PG_GPU_TEXTURE_PARAM_SWIZZLE_GREEN;
    else if(strcmp(str, "blue") == 0) return PG_GPU_TEXTURE_PARAM_SWIZZLE_BLUE;
    else if(strcmp(str, "alpha") == 0) return PG_GPU_TEXTURE_PARAM_SWIZZLE_ALPHA;
    else {
        pg_log(PG_LOG_ERROR, "Invalid GPU texture swizzle value '%s'", str);
        return PG_GPU_TEXTURE_PARAM_SWIZZLE_RED;
    }
}

enum pg_gpu_texture_param_ds_mode pg_gpu_texture_param_ds_mode_from_string(const char* str)
{
    if(strcmp(str, "depth") == 0) return PG_GPU_TEXTURE_PARAM_DS_DEPTH;
    else if(strcmp(str, "stencil") == 0) return PG_GPU_TEXTURE_PARAM_DS_STENCIL;
    else {
        pg_log(PG_LOG_ERROR, "Invalid GPU texture depth-stencil mode '%s'", str);
        return PG_GPU_TEXTURE_PARAM_DS_DEPTH;
    }
}



bool pg_gpu_texture_params_from_json(struct pg_gpu_texture_params* params, cJSON* json)
{
    cJSON* wrap_x_json, *wrap_y_json, *wrap_z_json, *filter_min_json, *filter_mag_json,
        *swizzle_red_json, *swizzle_green_json, *swizzle_blue_json, *swizzle_alpha_json,
        *depth_stencil_json, *min_lod_json, *max_lod_json, *lod_bias_json,
        *base_mipmap_json, *max_mipmap_json, *border_color_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(16,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("wrap_x",           cJSON_String,   &wrap_x_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("wrap_x",           cJSON_String,   &wrap_y_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("wrap_z",           cJSON_String,   &wrap_z_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("filter_min",       cJSON_String,   &filter_min_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("filter_mag",       cJSON_String,   &filter_mag_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("swizzle_red",      cJSON_String,   &swizzle_red_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("swizzle_green",    cJSON_String,   &swizzle_green_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("swizzle_blue",     cJSON_String,   &swizzle_blue_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("swizzle_alpha",    cJSON_String,   &swizzle_alpha_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("depth_stencil",    cJSON_String,   &depth_stencil_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("min_lod",          cJSON_Number,   &min_lod_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("max_lod",          cJSON_Number,   &max_lod_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("lod_bias",         cJSON_Number,   &lod_bias_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("base_mipmap",      cJSON_Number,   &base_mipmap_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("max_mipmap",       cJSON_Number,   &max_mipmap_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("border_color",     cJSON_Array,    &border_color_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid GPU texture parameters JSON");
        return false;
    }
    
    *params = PG_GPU_TEXTURE_PARAMS();
    if(wrap_x_json) params->wrap_x = pg_gpu_texture_param_wrap_from_string(wrap_x_json->valuestring);
    if(wrap_y_json) params->wrap_y = pg_gpu_texture_param_wrap_from_string(wrap_y_json->valuestring);
    if(wrap_z_json) params->wrap_z = pg_gpu_texture_param_wrap_from_string(wrap_z_json->valuestring);
    if(filter_min_json) params->filter_min = pg_gpu_texture_param_filter_from_string(filter_min_json->valuestring);
    if(filter_mag_json) params->filter_mag = pg_gpu_texture_param_filter_from_string(filter_mag_json->valuestring);
    if(swizzle_red_json) params->swizzle[0] = pg_gpu_texture_param_swizzle_from_string(swizzle_red_json->valuestring);
    if(swizzle_green_json) params->swizzle[1] = pg_gpu_texture_param_swizzle_from_string(swizzle_green_json->valuestring);
    if(swizzle_blue_json) params->swizzle[2] = pg_gpu_texture_param_swizzle_from_string(swizzle_blue_json->valuestring);
    if(swizzle_alpha_json) params->swizzle[3] = pg_gpu_texture_param_swizzle_from_string(swizzle_alpha_json->valuestring);
    if(depth_stencil_json) params->depth_stencil_mode = pg_gpu_texture_param_ds_mode_from_string(depth_stencil_json->valuestring);
    if(min_lod_json) params->min_lod = min_lod_json->valueint;
    if(max_lod_json) params->max_lod = max_lod_json->valueint;
    if(lod_bias_json) params->lod_bias = lod_bias_json->valueint;
    if(base_mipmap_json) params->base_mipmap = base_mipmap_json->valueint;
    if(max_mipmap_json) params->max_mipmap = max_mipmap_json->valueint;
    if(border_color_json) params->border_color = json_read_vec4(border_color_json);
    return true;
}

/****************************/
/*  GPU TEXTURE ASSETS      */
/****************************/

struct texture_asset_source {
    bool from_disk;
    union {
        SARR_T(16, struct pg_filepath) contents_paths;
        struct {
            ivec3 dimensions;
            enum pg_gpu_texture_format format;
            bool stereo;
            pg_asset_handle_t stereo_left;
            pg_asset_handle_t stereo_right;
        };
    };
};

static void* gpu_texture_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* loader, cJSON* json)
{
    cJSON* contents_json, *stereo_json, *dimensions_json, *format_json, *empty_json;
    struct pg_json_layout layout_contents = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_OPTIONAL_ITEM("contents", cJSON_Array, &contents_json),
        PG_JSON_LAYOUT_OPTIONAL_GROUP("empty", &empty_json, 3,
            PG_JSON_LAYOUT_OPTIONAL_ITEM("stereo", cJSON_Bool, &stereo_json),
            PG_JSON_LAYOUT_ITEM("dimensions", PG_JSON_VARIABLE, &dimensions_json),
            PG_JSON_LAYOUT_ITEM("format", cJSON_String, &format_json),
        ),
    );
    if(!pg_load_json_layout(json, &layout_contents)) return NULL;
    if(contents_json && empty_json) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "has mutually exclusive 'contents' and 'empty' members");
        return NULL;
    }
    if(!contents_json && !empty_json) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "requires either a 'contents' or 'empty' member");
        return NULL;
    }

    struct texture_asset_source parsed_source = {};

    if(contents_json) {
        parsed_source = (struct texture_asset_source){ .from_disk = true };
        SARR_INIT(parsed_source.contents_paths);
        int n_files = 0;
        cJSON* image_filename_json;
        cJSON_ArrayForEach(image_filename_json, contents_json) {
            const char* filename = cJSON_GetStringValue(image_filename_json);
            if(!filename) continue;
            struct pg_filepath* source_filepath = ARR_NEW(parsed_source.contents_paths);
            strncpy(source_filepath->path, filename, PG_FILE_PATH_MAXLEN);
            if(++n_files >= 16) {
                pg_asset_log(PG_LOG_ERROR, asset_handle,
                    " has too many paths in contents array, the rest will be ignored! (max 16)");
                return NULL;
            }
        }
    } else if(empty_json) {
        parsed_source = (struct texture_asset_source){ .from_disk = false,
            .stereo = stereo_json ? cJSON_IsTrue(stereo_json) : false,
            .dimensions = pg_asset_manager_read_ivec3(asset_handle.mgr, dimensions_json),
            .format = pg_gpu_texture_format_from_string(cJSON_GetStringValue(format_json)) };
        if(parsed_source.dimensions.z == 0) parsed_source.dimensions.z = 1;
        if(parsed_source.format == PG_GPU_TEXTURE_INVALID_FORMAT) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has invalid texture format '%s'", cJSON_GetStringValue(format_json));
            return NULL;
        }
        if(parsed_source.stereo) {
            parsed_source.stereo_left = pg_asset_declare_child(asset_handle, "pg_gpu_texture", "stereo_left");
            parsed_source.stereo_right = pg_asset_declare_child(asset_handle, "pg_gpu_texture", "stereo_right");
        }
    }

    struct texture_asset_source* output_parsed = malloc(sizeof(*output_parsed));
    *output_parsed = parsed_source;
    return output_parsed;
}

static void gpu_texture_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    free(parsed_source_ptr);
}


static void* gpu_texture_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    struct texture_asset_source* parsed_source = (struct texture_asset_source*)parsed_source_ptr;

    if(parsed_source->from_disk) {
        struct pg_file files[16];
        struct pg_file* file_ptrs[16];

        int contents_idx = 0;
        struct pg_filepath* contents_path;
        ARR_FOREACH_PTR(parsed_source->contents_paths, contents_path, contents_idx) {
            if(!pg_asset_manager_load_file(asset_handle.mgr, &files[contents_idx], contents_path->path, "rb")) {
                return NULL;
            }
            file_ptrs[contents_idx] = &files[contents_idx];
        }

        struct pg_texture tmp_contents;
        pg_texture_from_files(&tmp_contents, file_ptrs, parsed_source->contents_paths.len);
        pg_gpu_texture_t* gpu_tex = pg_texture_upload(&tmp_contents);
        pg_texture_deinit(&tmp_contents);
        for(int i = 0; i < contents_idx; ++i) pg_file_close(file_ptrs[i]);
        return gpu_tex;
    } else {
        if(parsed_source->stereo) {
            pg_gpu_texture_t* gpu_tex_left = pg_gpu_texture_alloc(parsed_source->format, VEC_XYZ(parsed_source->dimensions));
            pg_gpu_texture_t* gpu_tex_right = pg_gpu_texture_alloc(parsed_source->format, VEC_XYZ(parsed_source->dimensions));
            // The left texture is owned by this asset handle (so this 'left' child doesn't own it, only refers to it)
            pg_asset_from_data(asset_handle.mgr, "pg_gpu_texture", pg_asset_get_name(parsed_source->stereo_left), gpu_tex_left, false);
            pg_asset_from_data(asset_handle.mgr, "pg_gpu_texture", pg_asset_get_name(parsed_source->stereo_right), gpu_tex_right, true);
            return gpu_tex_left;
        }
        return pg_gpu_texture_alloc(parsed_source->format, VEC_XYZ(parsed_source->dimensions));
    }
}

static void gpu_texture_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_texture_deinit((pg_gpu_texture_t*)loaded_data);
}

struct pg_asset_loader* pg_gpu_texture_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_texture",
        .type_name_len = strlen("pg_gpu_texture"),
        .parse = gpu_texture_asset_parse,
        .unparse = gpu_texture_asset_unparse,
        .load = gpu_texture_asset_load,
        .unload = gpu_texture_asset_unload,
    };
    return loader;
}

