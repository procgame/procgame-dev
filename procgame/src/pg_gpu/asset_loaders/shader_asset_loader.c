#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"

/** \brief Load shader interface details from a JSON object */
bool pg_shader_interface_info_from_json(struct pg_shader_interface_info* sh_iface_info, cJSON* json)
{
    pg_shader_interface_info_init(sh_iface_info);

    cJSON* dynamic_uniforms_json, *uniform_inputs_json, *vertex_inputs_json, *outputs_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_ITEM("dynamic_uniforms", cJSON_Object, &dynamic_uniforms_json),
        PG_JSON_LAYOUT_ITEM("uniform_inputs", cJSON_Object, &uniform_inputs_json),
        PG_JSON_LAYOUT_ITEM("vertex_inputs", cJSON_Object, &vertex_inputs_json),
        PG_JSON_LAYOUT_ITEM("outputs", cJSON_Object, &outputs_json),
    );
    if(!pg_load_json_layout(json, &layout)) return false;

    /*  Uniform inputs  */
    cJSON* uni_json;
    cJSON_ArrayForEach(uni_json, uniform_inputs_json) {
        cJSON* location_json, *type_json, *binding_json, *layout_json;
        struct pg_json_layout uni_layout = PG_JSON_LAYOUT(4,
            PG_JSON_LAYOUT_ITEM("location", cJSON_Number, &location_json),
            PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
            PG_JSON_LAYOUT_ITEM("binding", cJSON_Number, &binding_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("layout", cJSON_Object, &layout_json),
        );
        if(!pg_load_json_layout(uni_json, &uni_layout)) return false;
        enum pg_shader_uniform_type type = pg_shader_uniform_type_from_string(cJSON_GetStringValue(type_json), 64);
        if(!type) {
            pg_log(PG_LOG_ERROR, "Shader interface has invalid type '%s'", type_json->valuestring);
            return false;
        } else if(type == PG_SHADER_UNIFORM_BUFFER) {
            struct pg_buffer_layout buffer_layout;
            if(!layout_json || !pg_buffer_layout_from_json(&buffer_layout, layout_json)) {
                pg_log(PG_LOG_ERROR, "Failed to parse 'layout' member required for uniform buffer input");
                return false;
            }
            pg_shader_interface_info_add_buffer_uniform_input(sh_iface_info,
                uni_json->string,
                location_json->valueint,
                binding_json->valueint,
                &buffer_layout);
        } else if(type == PG_SHADER_UNIFORM_TEXTURE) {
            pg_shader_interface_info_add_texture_uniform_input(sh_iface_info,
                uni_json->string,
                location_json->valueint,
                binding_json->valueint,
                false);
        } else if(type == PG_SHADER_UNIFORM_TEXTURE_BUFFER) {
            pg_shader_interface_info_add_texture_uniform_input(sh_iface_info,
                uni_json->string,
                location_json->valueint,
                binding_json->valueint,
                true);
        }
    }

    /*  Dynamic uniforms    */
    cJSON_ArrayForEach(uni_json, dynamic_uniforms_json) {
        cJSON* location_json, *type_json, *count_json;
        struct pg_json_layout uni_layout = PG_JSON_LAYOUT(3,
            PG_JSON_LAYOUT_ITEM("location", cJSON_Number, &location_json),
            PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("count", cJSON_String, &count_json),
        );
        if(!pg_load_json_layout(uni_json, &uni_layout)) return false;
        enum pg_data_type type = pg_data_type_from_string(cJSON_GetStringValue(type_json));
        if(!type || pg_data_type_size[pg_data_base_type[type]] != 4) {
            pg_log(PG_LOG_ERROR, "Dynamic uniform has invalid type '%s' (%s)",
                    type_json->valuestring, pg_data_type_string[type]);
            return false;
        }
        pg_shader_interface_info_add_dynamic_uniform_input(sh_iface_info,
            uni_json->string,
            location_json->valueint,
            count_json ? count_json->valueint : 1,
            type);
    }

    /*  Vertex inputs   */
    cJSON* vert_json;
    cJSON_ArrayForEach(vert_json, vertex_inputs_json) {
        cJSON* location_json, *type_json ;
        struct pg_json_layout vert_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("location", cJSON_Number, &location_json),
            PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
        );
        if(!pg_load_json_layout(vert_json, &vert_layout)) return false;
        enum pg_data_type type = pg_data_type_from_string(cJSON_GetStringValue(type_json));
        pg_shader_interface_info_add_vertex_input(sh_iface_info,
            vert_json->string,
            location_json->valueint,
            type);
    }
        
    /*  Outputs */
    cJSON* out_json;
    cJSON_ArrayForEach(out_json, outputs_json) {
        cJSON* location_json, *type_json ;
        struct pg_json_layout out_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("location", cJSON_Number, &location_json),
            PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
        );
        if(!pg_load_json_layout(out_json, &out_layout)) return false;
        enum pg_gpu_framebuffer_attachment_index location =
            pg_gpu_framebuffer_attachment_index_from_string(cJSON_GetStringValue(location_json));
        enum pg_data_type type = pg_data_type_from_string(cJSON_GetStringValue(type_json));
        pg_shader_interface_info_add_output(sh_iface_info,
            out_json->string,
            location,
            type);
    }

    return true;
}
/********************************/
/*  GPU SHADER PROGRAM ASSETS   */
/********************************/

struct shader_asset_source {
    struct pg_shader_source vertex_shader_source, fragment_shader_source;
    struct pg_shader_interface_info iface_info;
};


static void* gpu_shader_program_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* loader, cJSON* json)
{
    int n_search_paths;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_handle.mgr, &n_search_paths);

    cJSON* vertex_shader_json, *fragment_shader_json, *interface_json;

    struct pg_json_layout layout = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_GROUP("stages", NULL, 2,
            PG_JSON_LAYOUT_ITEM("vertex_shader", cJSON_String, &vertex_shader_json),
            PG_JSON_LAYOUT_ITEM("fragment_shader", cJSON_String, &fragment_shader_json),
        ),
        PG_JSON_LAYOUT_ITEM("interface", cJSON_Object, &interface_json),
    );
    if(!pg_load_json_layout(json, &layout)) return NULL;

    /*  Load interface info */
    struct shader_asset_source parsed_source = {0};
    if(!pg_shader_interface_info_from_json(&parsed_source.iface_info, interface_json))
        return NULL;

    /*  Load source code    */
    const char* vs_filename = cJSON_GetStringValue(vertex_shader_json);
    const char* fs_filename = cJSON_GetStringValue(fragment_shader_json);
    struct pg_file vertex_shader_file = {}, fragment_shader_file = {};
    bool r = pg_file_open_search_paths(&vertex_shader_file, search_paths, n_search_paths, vs_filename, "r")
          && pg_file_open_search_paths(&fragment_shader_file, search_paths, n_search_paths, fs_filename, "r")
          && pg_shader_source_from_file_with_includes(&parsed_source.vertex_shader_source, PG_GPU_VERTEX_SHADER,
                                                      &vertex_shader_file, search_paths, n_search_paths)
          && pg_shader_source_from_file_with_includes(&parsed_source.fragment_shader_source, PG_GPU_FRAGMENT_SHADER,
                                                      &fragment_shader_file, search_paths, n_search_paths);
    if(vertex_shader_file.f) pg_file_close(&vertex_shader_file);
    if(fragment_shader_file.f) pg_file_close(&fragment_shader_file);
    if(!r) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to load GLSL sources");
        return NULL;
    }

    /*  If it all went well then we actually do the heap allocation and copy the result to it   */
    struct shader_asset_source* output_parsed = malloc(sizeof(*output_parsed));
    *output_parsed = parsed_source;
    return output_parsed;
}

static void gpu_shader_program_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    struct shader_asset_source* parsed_source = (struct shader_asset_source*)parsed_source_ptr;
    pg_shader_source_deinit(&parsed_source->vertex_shader_source);
    pg_shader_source_deinit(&parsed_source->fragment_shader_source);
    free(parsed_source_ptr);
}

static void* gpu_shader_program_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source_ptr)
{
    struct shader_asset_source* parsed_source = (struct shader_asset_source*)parsed_source_ptr;

    /*  Compile GLSL source */
    pg_gpu_shader_stage_t* gpu_vertex_shader = pg_shader_source_compile(&parsed_source->vertex_shader_source);
    pg_gpu_shader_stage_t* gpu_fragment_shader = pg_shader_source_compile(&parsed_source->fragment_shader_source);
    if(!gpu_vertex_shader || !gpu_fragment_shader) {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "failed to compile GLSL sources");
        if(gpu_vertex_shader) pg_gpu_shader_stage_destroy(gpu_vertex_shader);
        if(gpu_fragment_shader) pg_gpu_shader_stage_destroy(gpu_fragment_shader);
        return NULL;
    }

    /*  Construct the final shader program object from the compiled sources
        and the parsed interface info   */
    pg_gpu_shader_program_t* sh_prog = pg_gpu_shader_program_init(
            (pg_gpu_shader_stage_t*[]){ gpu_vertex_shader, gpu_fragment_shader }, 2,
            &parsed_source->iface_info);
    return sh_prog;
}

static void gpu_shader_program_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_shader_program_t* sh_prog = (pg_gpu_shader_program_t*)loaded_data;
    pg_gpu_shader_stage_destroy(
            pg_gpu_shader_program_get_stage(sh_prog, PG_GPU_VERTEX_SHADER));
    pg_gpu_shader_stage_destroy(
            pg_gpu_shader_program_get_stage(sh_prog, PG_GPU_FRAGMENT_SHADER));
    pg_gpu_shader_program_destroy(sh_prog);
}


struct pg_asset_loader* pg_gpu_shader_program_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_shader_program",
        .type_name_len = strlen("pg_gpu_shader_program"),
        .parse = gpu_shader_program_asset_parse,
        .unparse = gpu_shader_program_asset_unparse,
        .load = gpu_shader_program_asset_load,
        .unload = gpu_shader_program_asset_unload,
    };
    return loader;
}


