#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"


/****************************/
/*  GPU PIPELINE ASSETS     */
/****************************/

struct vertex_buffer_asset_source {
    struct binding_source {
        bool used;
        pg_asset_handle_t buffer;
        bool instanced;
    } bindings[16];
    struct attrib_source {
        bool used;
        int binding;
        char attribute[64];
    } attributes[16];
    pg_asset_handle_t index_buffer;
};


/*  JSON -> struct vertex_buffer_asset_source    */
static void* gpu_vertex_assembly_asset_parse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, cJSON* def)
{
    pg_asset_manager_t* asset_mgr = asset_handle.mgr;

    /*  Validation  */
    cJSON* bindings_json, *attributes_json, *indices_json;
    struct pg_json_layout asset_layout = PG_JSON_LAYOUT(3,
        PG_JSON_LAYOUT_ITEM("bindings", cJSON_Array, &bindings_json),
        PG_JSON_LAYOUT_ITEM("attributes", cJSON_Array, &attributes_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("index_buffer", PG_JSON_ASSET, &indices_json),
    );
    if(!pg_load_json_layout(def, &asset_layout)) return NULL;
    struct vertex_buffer_asset_source parsed_source = {};

    int i = 0;
    cJSON* binding_json;
    cJSON_ArrayForEach(binding_json, bindings_json) {
        if(i >= 16) break;
        if(cJSON_IsNull(binding_json)) { ++i; continue; }
        struct binding_source* parsed_binding_source = &parsed_source.bindings[i];
        cJSON* buffer_asset_json, *instanced_json;
        struct pg_json_layout binding_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("buffer", PG_JSON_ASSET, &buffer_asset_json),
            PG_JSON_LAYOUT_OPTIONAL_ITEM("instanced", cJSON_Bool, &instanced_json),
        );
        if(!pg_load_json_layout(binding_json, &binding_layout)) return NULL;
        parsed_binding_source->used = true;
        parsed_binding_source->instanced = cJSON_IsTrue(instanced_json);
        parsed_binding_source->buffer = pg_asset_from_json(asset_mgr, "pg_gpu_buffer", NULL, buffer_asset_json);
        pg_asset_depends(asset_handle, parsed_binding_source->buffer);
        ++i;
    }

    i = 0;
    cJSON* attribute_json;
    cJSON_ArrayForEach(attribute_json, attributes_json) {
        if(i >= 16) break;
        if(cJSON_IsNull(attribute_json)) { ++i; continue; }
        struct attrib_source* parsed_attrib_source = &parsed_source.attributes[i];
        cJSON* attrib_binding_json, *attrib_name_json;
        struct pg_json_layout attrib_layout = PG_JSON_LAYOUT(2,
            PG_JSON_LAYOUT_ITEM("binding", cJSON_Number, &attrib_binding_json),
            PG_JSON_LAYOUT_ITEM("attribute", cJSON_String, &attrib_name_json),
        );
        if(!pg_load_json_layout(attribute_json, &attrib_layout)) return NULL;
        parsed_attrib_source->used = true;
        parsed_attrib_source->binding = attrib_binding_json->valueint;
        strncpy(parsed_attrib_source->attribute, cJSON_GetStringValue(attrib_name_json), 64);
        ++i;
    }

    if(indices_json) {
        parsed_source.index_buffer = pg_asset_from_json(asset_mgr, "pg_gpu_buffer", NULL, indices_json);
        pg_asset_depends(asset_handle, parsed_source.index_buffer);
    }

    /*  If everything went well, then allocate the real parsed source, and copy
        the temporary one into it   */
    struct vertex_buffer_asset_source* final_parsed_source = malloc(sizeof(*final_parsed_source));
    *final_parsed_source = parsed_source;
    return final_parsed_source;
}

static void gpu_vertex_assembly_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct vertex_buffer_asset_source* vbuf_source = (struct vertex_buffer_asset_source*)parsed_source;
    free(vbuf_source);
}



/*  struct vertex_buffer_asset_source -> pg_gpu_pipeline_t*  */
static void* gpu_vertex_assembly_asset_load(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct vertex_buffer_asset_source* vbuf_source = (struct vertex_buffer_asset_source*)parsed_source;
    pg_gpu_vertex_assembly_t* vbuf = pg_gpu_vertex_assembly_create();
    for(int i = 0; i < 16; ++i) {
        if(!vbuf_source->bindings[i].used) continue;
        pg_asset_handle_t buffer_asset = vbuf_source->bindings[i].buffer;
        pg_gpu_buffer_t* buffer = pg_asset_get_data(buffer_asset);
        if(!buffer) {
            pg_asset_log(PG_LOG_ERROR, asset_handle, "has non-existent buffer in binding %d", i);
            continue;
        }
        pg_gpu_vertex_assembly_set_binding(vbuf, i, buffer, vbuf_source->bindings[i].instanced);
    }
    for(int i = 0; i < 16; ++i) {
        if(!vbuf_source->attributes[i].used) continue;
        pg_gpu_vertex_assembly_attribute(vbuf, vbuf_source->attributes[i].binding,
            i, vbuf_source->attributes[i].attribute);
    }
    if(pg_asset_exists(vbuf_source->index_buffer)) {
        pg_gpu_buffer_t* index_buffer = pg_asset_get_data(vbuf_source->index_buffer);
        pg_gpu_vertex_assembly_set_index_buffer(vbuf, index_buffer);
    }
    pg_gpu_vertex_assembly_debug_log_ranges(vbuf);
    return vbuf;
}

static void gpu_vertex_assembly_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    pg_gpu_pipeline_destroy((pg_gpu_pipeline_t*)loaded_data);
}

struct pg_asset_loader* pg_gpu_vertex_assembly_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_gpu_vertex_assembly",
        .type_name_len = strlen("pg_gpu_vertex_assembly"),
        .parse = gpu_vertex_assembly_asset_parse,
        .unparse = gpu_vertex_assembly_asset_unparse,
        .load = gpu_vertex_assembly_asset_load,
        .unload = gpu_vertex_assembly_asset_unload,
    };
    return loader;
}



