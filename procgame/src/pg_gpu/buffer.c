#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

enum pg_buffer_attribute_packing pg_buffer_attribute_packing_from_string(const char* str)
{
    if(strncmp(str, "packed", 7) == 0) return PG_BUFFER_PACKING_PACKED;
    else if(strncmp(str, "4byte", 6) == 0) return PG_BUFFER_PACKING_4BYTE;
    else if(strncmp(str, "std140", 7) == 0) return PG_BUFFER_PACKING_STD140;
    else return PG_BUFFER_PACKING_INVALID;
}

enum pg_gpu_buffer_type pg_gpu_buffer_type_from_string(const char* str)
{
    if(strncmp(str, "vertex_buffer", 14) == 0) return PG_GPU_VERTEX_BUFFER;
    else if(strncmp(str, "texture_buffer", 15) == 0) return PG_GPU_TEXTURE_BUFFER;
    else if(strncmp(str, "uniform_buffer", 15) == 0) return PG_GPU_UNIFORM_BUFFER;
    else if(strncmp(str, "index_buffer", 13) == 0) return PG_GPU_INDEX_BUFFER;
    else return PG_GPU_INVALID_BUFFER;
}



/****************************************/
/*  LOCAL MEMORY BUFFERS (no GPU io)    */
/****************************************/

static int padding_aligned(int current_offset, int want_alignment)
{
    int rounded = current_offset + want_alignment - 1;
    int aligned = rounded - (rounded % want_alignment);
    return aligned - current_offset;
}

static int std140_alignment(enum pg_data_type type)
{
    const int base_size = pg_data_type_size[pg_data_base_type[type]];
    const int components = pg_data_type_components[type];
    const int pad_components = (components == 3) ? 4 : components;
    return base_size * pad_components;
}

static int pg_buffer_attributes_from_layout(pg_buffer_attribute_arr_t* attribs, const struct pg_buffer_layout* layout)
{
    SARR_INIT(*attribs);
    int max_required_alignment = 1;
    int running_offset = 0;
    for(int i = 0; i < layout->attributes.len; ++i) {
        struct pg_buffer_attribute* buf_attrib = ARR_NEW(*attribs);
        buf_attrib->info = layout->attributes.data[i];
        buf_attrib->index = i;
        buf_attrib->size = pg_data_type_size[layout->attributes.data[i].type];

        /*  Calculate padding   */
        if(i != 0) {
            enum pg_data_type attrib_type = buf_attrib->info.type;
            struct pg_buffer_attribute* last_attrib = &attribs->data[i - 1];
            /*  Calculate required alignment    */
            int required_alignment = 1;
            if(layout->attribute_packing == PG_BUFFER_PACKING_4BYTE) required_alignment = 4;
            else if(layout->attribute_packing == PG_BUFFER_PACKING_STD140) required_alignment = std140_alignment(attrib_type);
            /*  Keep track of the largest alignment required in the buffer  */
            if(max_required_alignment < required_alignment)  max_required_alignment = required_alignment;
            /*  Calculate the padding after the last attribute based on the alignment requirement of the current attribute  */
            int required_padding = padding_aligned(running_offset, required_alignment);
            last_attrib->pad_after = required_padding;
            /*  Add the padding required after the previous attribute to the running offset counter  */
            running_offset += required_padding;
        }

        /*  Add attribute's size to the running offset counter (it may have its padding member set when processing the next attribute)  */
        buf_attrib->offset = running_offset;
        buf_attrib->pad_after = 0;
        running_offset += buf_attrib->size;
    }

    /*  Calculate padding after the final attribute */
    struct pg_buffer_attribute* final_attrib = &attribs->data[layout->attributes.len - 1];
    int element_alignment = (layout->element_alignment > 0) ? layout->element_alignment : max_required_alignment;
    int element_padding = padding_aligned(running_offset, element_alignment);
    final_attrib->pad_after = element_padding;
    running_offset += element_padding;

    /*  We now have the final element size  */
    const int total_element_size = running_offset;

    /*  Set the stride for all the attributes   */
    for(int i = 0; i < layout->attributes.len; ++i) {
        attribs->data[i].stride = total_element_size;
    }

    return total_element_size;

}

bool pg_buffer_init(struct pg_buffer* buf, const struct pg_buffer_layout* layout, int n_elements)
{
    if(!buf || n_elements <= 0 || !layout) return false;
    *buf = (struct pg_buffer){0};
    const int element_size = pg_buffer_attributes_from_layout(&buf->attributes, layout);
    SARR_INIT(buf->ranges);
    buf->data_elements = n_elements;
    buf->element_size = element_size;
    buf->data_size = n_elements * element_size;
    buf->data = calloc(buf->data_elements, element_size);
    buf->packing_data_type = layout->packing_data_type;
    buf->own_data = true;
    pg_buffer_define_range(buf, "all", 0, n_elements);
    return !!(buf->data);
}

void pg_buffer_deinit(struct pg_buffer* buf)
{
    if(!buf || !buf->data) return;
    ARR_DEINIT(buf->attributes);
    free(buf->data);
}

bool pg_buffer_resize(struct pg_buffer* buf, int n_elements)
{
    if(n_elements <= 0 || !buf->own_data) return false;
    uint8_t* new_ptr = realloc(buf->data, buf->element_size * n_elements);
    // if realloc fails, the original block is left untouched
    if(!new_ptr) return false;
    buf->data = new_ptr;
    buf->data_elements = n_elements;
    buf->data_size = buf->element_size * n_elements;
    pg_buffer_define_range(buf, "all", 0, n_elements);
    return true;
}

int pg_buffer_get_attribute_index(struct pg_buffer* buf, const char* name)
{
    if(!buf) return -1;
    int i;
    struct pg_buffer_attribute* attrib_ptr;
    ARR_FOREACH_PTR(buf->attributes, attrib_ptr, i) {
        if(strncmp(name, attrib_ptr->info.name, PG_BUFFER_ATTRIBUTE_NAME_MAX_LEN) == 0) {
            return i;
        }
    }
    return -1;
}

const struct pg_buffer_attribute* pg_buffer_get_attribute(
        struct pg_buffer* buf, int index)
{
    if(!buf || index < 0 || index >= buf->attributes.len) return NULL;
    return &buf->attributes.data[index];
}

uint8_t* pg_buffer_get_element_ptr(struct pg_buffer* buf, int elem_idx)
{
    if(!buf || !buf->data
    || elem_idx >= buf->data_elements || elem_idx < 0) {
        return NULL;
    }
    return buf->data + (elem_idx * buf->element_size);
}

uint8_t* pg_buffer_get_attribute_ptr(struct pg_buffer* buf, int elem_idx, int attrib_idx)
{

    if(!buf || !buf->data
    || elem_idx >= buf->data_elements || elem_idx < 0
    || attrib_idx >= buf->attributes.len || attrib_idx < 0) {
        return NULL;
    }
    struct pg_buffer_attribute* attrib = &buf->attributes.data[attrib_idx];
    return buf->data + (elem_idx * buf->element_size) + attrib->offset;
}

int pg_buffer_define_range(struct pg_buffer* buf, const char* name, int first, int count)
{
    /*  Find existing range     */
    int buf_idx;
    struct pg_buffer_range* range;
    ARR_FOREACH_PTR(buf->ranges, range, buf_idx) {
        if(strncmp(range->name, name, 128) == 0) break;
    }
    /*  If there is none, create a new one  */
    if(buf_idx == buf->ranges.len) {
        range = ARR_NEW(buf->ranges);
        strncpy(range->name, name, 128);
    }
    range->range = ivec2(first, count);
    return buf_idx;
}

ivec2 pg_buffer_get_range_by_name(struct pg_buffer* buf, const char* name)
{
    int i;
    struct pg_buffer_range* range_ptr;
    ARR_FOREACH_PTR(buf->ranges, range_ptr, i) {
        if(strncmp(range_ptr->name, name, 128) == 0) return range_ptr->range;
    }
    return ivec2(0,0);
}

ivec2 pg_buffer_get_range(struct pg_buffer* buf, int idx)
{
    return buf->ranges.data[idx].range;
}

int pg_buffer_get_range_count(struct pg_buffer* buf)
{
    return buf->ranges.len;
}

const char* pg_buffer_get_range_name(struct pg_buffer* buf, int idx)
{
    return buf->ranges.data[idx].name;
}

/*  Initialize an empty buffer layout   */
void pg_buffer_layout_init(struct pg_buffer_layout* layout)
{
    if(!layout) return;
    SARR_INIT(layout->attributes);
    layout->attribute_packing = PG_BUFFER_PACKING_INVALID;
    layout->element_alignment = 0;
}

/*  Deinitialize a buffer layout    */
void pg_buffer_layout_deinit(struct pg_buffer_layout* layout)
{
    return;
}






/****************/
/*  OpenGL      */
/****************/

#include "pg_gpu/opengl.h"

struct pg_gpu_buffer {
    GLuint gl_buf;
    GLuint gl_tex; /*   Used only for texture buffers   */
    GLenum gl_buffer_type;  // GL_ARRAY_BUFFER, GL_TEXTURE_BUFFER, etc.
    enum pg_gpu_buffer_type buf_type;
    int size;
    int element_size;
    SARR_T(PG_BUFFER_MAX_ATTRIBUTES, struct pg_buffer_attribute) attributes;
    pg_buffer_range_arr_t ranges;
    struct pg_buffer* local_data;
    bool own_local_data;
};

GLenum pg_type_to_gl_format[PG_NUM_DATA_TYPES] = {
    [PG_FLOAT] = GL_R32F,
    [PG_BYTE] = GL_R8I,
    [PG_SHORT] = GL_R16I,
    [PG_INT] = GL_R32I,
    [PG_UBYTE] = GL_R8UI,
    [PG_USHORT] = GL_R16UI,
    [PG_UINT] = GL_R32UI,
    [PG_VEC2] = GL_RG32F,
    [PG_BVEC2] = GL_RG8I,
    [PG_SVEC2] = GL_RG16I,
    [PG_IVEC2] = GL_RG32I,
    [PG_UBVEC2] = GL_RG8UI,
    [PG_USVEC2] = GL_RG16UI,
    [PG_UVEC2] = GL_RG32UI,
    [PG_VEC3] = GL_RGB32F,
    [PG_IVEC3] = GL_RGB32I,
    [PG_UVEC3] = GL_RGB32UI,
    [PG_VEC4] = GL_RGBA32F,
    [PG_BVEC4] = GL_RGBA8I,
    [PG_SVEC4] = GL_RGBA16I,
    [PG_IVEC4] = GL_RGBA32I,
    [PG_UBVEC4] = GL_RGBA8UI,
    [PG_USVEC4] = GL_RGBA16UI,
    [PG_UVEC4] = GL_RGBA32UI,
};

GLenum pg_type_to_gl_format_normalized[PG_NUM_DATA_TYPES] = {
    [PG_UBYTE] = GL_R8,
    [PG_USHORT] = GL_R16,
    [PG_UBVEC2] = GL_RG8,
    [PG_USVEC2] = GL_RG16,
    [PG_UBVEC4] = GL_RGBA8,
    [PG_USVEC4] = GL_RGBA16,
};

GLenum pg_buffer_type_to_gl[] = {
    [PG_GPU_VERTEX_BUFFER] = GL_ARRAY_BUFFER,
    [PG_GPU_TEXTURE_BUFFER] = GL_TEXTURE_BUFFER,
    [PG_GPU_UNIFORM_BUFFER] = GL_UNIFORM_BUFFER,
    [PG_GPU_INDEX_BUFFER] = GL_ELEMENT_ARRAY_BUFFER,
};


/*  GPU BUFFER I/O          */

pg_gpu_buffer_t* pg_buffer_upload(struct pg_buffer* buf, enum pg_gpu_buffer_type buf_type, bool own_local_data)
{
    if(!buf || !buf->data || !buf->data_size) return NULL;
    /*  First validate input types  */
    if(buf_type == PG_GPU_TEXTURE_BUFFER) {
        GLenum internal_format = pg_type_to_gl_format[buf->packing_data_type];
        if(!internal_format) {
            pg_log(PG_LOG_ERROR, "Cannot create a texture buffer from buffer packed into type %s.",
                    pg_data_type_string[buf->packing_data_type]);
            return NULL;
        }
    } else if(buf_type == PG_GPU_INDEX_BUFFER && buf->attributes.len != 1) {
        pg_log(PG_LOG_ERROR, "Can only create a GPU index buffer from a buffer with with ONE attribute.");
        return NULL;
    }
    /*  If it is ok then we can allocate our CPU-side data for the buffer   */
    struct pg_gpu_buffer* gpu_buf = malloc(sizeof(*gpu_buf));
    *gpu_buf = (struct pg_gpu_buffer){};
    SARR_INIT(gpu_buf->attributes);
    gpu_buf->ranges = buf->ranges;
    gpu_buf->size = buf->data_size;
    gpu_buf->element_size = buf->element_size;
    gpu_buf->buf_type = buf_type;
    gpu_buf->gl_buffer_type = pg_buffer_type_to_gl[buf_type];
    /*  Keep the local data buffer if we should */
    gpu_buf->own_local_data = own_local_data;
    if(own_local_data) gpu_buf->local_data = buf;
    /*  Copy the attribute info over    */
    ARR_CONCAT(gpu_buf->attributes, buf->attributes);
    /*  Upload the raw buffer data  */
    PG_CHECK_GL(glGenBuffers(1, &gpu_buf->gl_buf));
    PG_CHECK_GL(glBindBuffer(gpu_buf->gl_buffer_type, gpu_buf->gl_buf));
    PG_CHECK_GL(glBufferData(gpu_buf->gl_buffer_type, buf->data_size, buf->data, GL_STATIC_DRAW));
    /*  For texture buffers, there is the extra step of creating the associated texture object  */
    if(buf_type == PG_GPU_TEXTURE_BUFFER) {
        PG_CHECK_GL(glGenTextures(1, &gpu_buf->gl_tex));
        PG_CHECK_GL(glBindTexture(GL_TEXTURE_BUFFER, gpu_buf->gl_tex));
        PG_CHECK_GL(glTexBuffer(GL_TEXTURE_BUFFER, pg_type_to_gl_format[buf->packing_data_type], gpu_buf->gl_buf));
    }
    /*  Cleanup */
    PG_CHECK_GL(glBindBuffer(gpu_buf->gl_buffer_type, 0));
    return gpu_buf;
}

void pg_gpu_buffer_reupload(pg_gpu_buffer_t* gpu_buf)
{
    if(!gpu_buf) return;
    if(!gpu_buf->local_data) {
        pg_log(PG_LOG_ERROR, "Attempting to reupload GPU buffer which has no local data to upload");
        return;
    }
    struct pg_buffer* buf = gpu_buf->local_data;
    int size = buf->data_size < gpu_buf->size ? buf->data_size : gpu_buf->size;
    PG_CHECK_GL(glBindBuffer(gpu_buf->gl_buffer_type, gpu_buf->gl_buf));
    PG_CHECK_GL(glBufferSubData(gpu_buf->gl_buffer_type, 0, size, buf->data));
}

void pg_gpu_buffer_reupload_range(pg_gpu_buffer_t* gpu_buf, int first_element, int n_elements)
{
    if(!gpu_buf) return;
    if(!gpu_buf->local_data) {
        pg_log(PG_LOG_ERROR, "Attempting to reupload GPU buffer which has no local data to upload");
        return;
    }
    struct pg_buffer* buf = gpu_buf->local_data;
    if(first_element < 0 || first_element >= buf->data_elements
    || n_elements <= 0 || first_element + n_elements >= buf->data_elements) {
        pg_log(PG_LOG_ERROR, "Attempting to reupload invalid range within GPU buffer");
        return;
    }
    int offset = first_element * buf->element_size;
    int size = n_elements * buf->element_size;
    PG_CHECK_GL(glBindBuffer(gpu_buf->gl_buffer_type, gpu_buf->gl_buf));
    PG_CHECK_GL(glBufferSubData(gpu_buf->gl_buffer_type, offset, size, buf->data + offset));
}

void pg_gpu_buffer_set_local_data(pg_gpu_buffer_t* gpu_buf, struct pg_buffer* local_buf, bool own_local_data)
{
    gpu_buf->local_data = local_buf;
    gpu_buf->own_local_data = own_local_data;
}

void pg_gpu_buffer_deinit(pg_gpu_buffer_t* gpu_buf)
{
    if(!gpu_buf) return;
    PG_CHECK_GL(glDeleteBuffers(1, &gpu_buf->gl_buf));
    if(gpu_buf->own_local_data && gpu_buf->local_data) {
        pg_buffer_deinit(gpu_buf->local_data);
    }
    free(gpu_buf);
}


/*  GPU BUFFER INSPECTION   */

int pg_gpu_buffer_define_range(struct pg_gpu_buffer* buf, const char* name, int first, int count)
{
    struct pg_buffer_range* new_range = ARR_NEW(buf->ranges);
    strncpy(new_range->name, name, 128);
    new_range->range = ivec2(first, count);
    return buf->ranges.len - 1;
}

ivec2 pg_gpu_buffer_get_range_by_name(struct pg_gpu_buffer* buf, const char* name)
{
    int i;
    struct pg_buffer_range* range_ptr;
    ARR_FOREACH_PTR(buf->ranges, range_ptr, i) {
        if(strncmp(range_ptr->name, name, 128) == 0) return range_ptr->range;
    }
    return ivec2(0,0);
}

ivec2 pg_gpu_buffer_get_range(struct pg_gpu_buffer* buf, int idx)
{
    return buf->ranges.data[idx].range;
}

int pg_gpu_buffer_get_range_count(struct pg_gpu_buffer* buf)
{
    return buf->ranges.len;
}

const char* pg_gpu_buffer_get_range_name(struct pg_gpu_buffer* buf, int idx)
{
    return buf->ranges.data[idx].name;
}

int pg_gpu_buffer_get_attribute_count(pg_gpu_buffer_t* gpu_buf)
{
    if(!gpu_buf) return 0;
    return gpu_buf->attributes.len;
}

int pg_gpu_buffer_get_attribute_index(pg_gpu_buffer_t* gpu_buf, const char* name)
{
    if(!gpu_buf) return -1;
    int i;
    struct pg_buffer_attribute* attrib_ptr;
    ARR_FOREACH_PTR(gpu_buf->attributes, attrib_ptr, i) {
        if(strncmp(name, attrib_ptr->info.name, PG_BUFFER_ATTRIBUTE_NAME_MAX_LEN) == 0) {
            return i;
        }
    }
    return -1;
}

const struct pg_buffer_attribute* pg_gpu_buffer_get_attribute(pg_gpu_buffer_t* gpu_buf, int index)
{
    if(!gpu_buf || index < 0 || index >= gpu_buf->attributes.len) return NULL;
    return &gpu_buf->attributes.data[index];
}

int pg_gpu_buffer_get_size(pg_gpu_buffer_t* gpu_buf)
{
    return gpu_buf->size;
}

int pg_gpu_buffer_get_element_size(pg_gpu_buffer_t* gpu_buf)
{
    return gpu_buf->element_size;
}

enum pg_gpu_buffer_type pg_gpu_buffer_get_type(pg_gpu_buffer_t* gpu_buf)
{
    return gpu_buf->buf_type;
}

struct pg_buffer* pg_gpu_buffer_get_local_data(pg_gpu_buffer_t* gpu_buf)
{
    return gpu_buf->local_data;
}

GLuint pg_gpu_buffer_get_handle(pg_gpu_buffer_t* gpu_buf)
{
    if(!gpu_buf) return 0;
    return gpu_buf->gl_buf;
}

GLuint pg_gpu_buffer_get_tex_handle(pg_gpu_buffer_t* gpu_buf)
{
    if(!gpu_buf) return 0;
    return gpu_buf->gl_tex;
}

/****************/
/*  END OPENGL  */
/****************/

