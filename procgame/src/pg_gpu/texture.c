#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

/*****************************************/
/*  LOCAL MEMORY TEXTURES (no GPU io)    */
/*****************************************/

#include "lodepng.h"

void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int x, int y, int layer, int w, int h, int row_stride);

bool pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int width, int height, int layers)
{
    if(!tex || type == PG_NULL || type > PG_VEC4
    || width <= 0 || height <= 0 || layers <= 0)
        return false;
    struct pg_buffer_layout buffer_layout = PG_BUFFER_LAYOUT(
        PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
        PG_BUFFER_ATTRIBUTES_LIST(1,
            PG_BUFFER_ATTRIBUTE("pixel", type)
        )
    );
    if(!pg_buffer_init(&tex->buf, &buffer_layout, width * height * layers))
        return false;
    tex->layer_stride = width * height;
    tex->pixel_type = type;
    tex->color_channels = pg_data_type_components[type];
    tex->dimensions = ivec3(width, height, layers);
    return true;

}

bool pg_texture_init_from_path(struct pg_texture* tex, const char* filename)
{
    struct pg_file file;
    if(!pg_file_open(&file, filename, "r")) return false;
    return pg_texture_init_from_file(tex, &file);
}

bool pg_texture_init_from_file(struct pg_texture* tex, struct pg_file* file)
{
    /*  Read the file   */
    if(!file || !file->f) return false;
    pg_file_read_full(file);
    /*  Allocate a buffer   */
    unsigned char* buf;
    unsigned w, h;
    unsigned png_error = lodepng_decode32(&buf, &w, &h, (const unsigned char*)file->content, file->size);
    if(png_error) {
        pg_log(PG_LOG_ERROR, "Failed to load texture from file ('%s') : %s",
                file->fullname, lodepng_error_text(png_error));
        return false;
    }
    bool init_result = pg_texture_init(tex, PG_UBVEC4, w, h, 1);
    if(!init_result) return false;
    memcpy(tex->buf.data, buf, w * h * sizeof(ubvec4));
    free(buf);
    return true;
}

bool pg_texture_from_files(struct pg_texture* tex, struct pg_file** files, int num_files)
{
    return pg_texture_from_files_get_info(tex, files, num_files, NULL);
}

bool pg_texture_from_files_get_info(struct pg_texture* tex, struct pg_file** files, int num_files, ivec2* sizes)
{
    char* tex_data[num_files];
    uvec2 tex_size[num_files];
    uvec2 max_size = uvec2();
    unsigned error = 0;
    /*  First load all the texture data individually    */
    int file_i;
    for(file_i = 0; file_i < num_files; ++file_i) {
        if(!files[file_i] || !files[file_i]->f) {
            pg_log(PG_LOG_ERROR, "Attempted to load a texture from invalid file object");
            error = 1;
            break;
        }
        pg_file_read_full(files[file_i]);
        error = lodepng_decode32((unsigned char**)&tex_data[file_i],
                    &tex_size[file_i].x, &tex_size[file_i].y, (const unsigned char*)files[file_i]->content, files[file_i]->size);
        if(error) {
            pg_log(PG_LOG_ERROR, "Failed to load PNG data from file '%s'. %u: %s",
                   files[file_i]->fullname, error, lodepng_error_text(error));
            break;
        }
        max_size = uvec2_max(max_size, tex_size[file_i]);
        if(sizes) sizes[file_i] = ivec2(VEC_XY(tex_size[file_i]));
    }
    if(error) {
        for(int i = 0; i < file_i; ++i) free(tex_data[i]);
        return false;
    }
    /*  Calculate the stride of the image using the largest one loaded */
    int pixel_stride = sizeof(uint8_t) * 4;
    int layer_stride = max_size.x * max_size.y;
    tex->layer_stride = layer_stride;
    tex->pixel_type = PG_UBVEC4;
    tex->color_channels = 4;
    tex->dimensions = ivec3(max_size.x, max_size.y, num_files);
    int n_pixels = max_size.x * max_size.y * num_files;
    /*  Allocate a data buffer big enough to hold all the textures (with padding
        for images smaller than the largest)    */
    struct pg_buffer_layout buffer_layout = PG_BUFFER_LAYOUT(
        PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
        PG_BUFFER_ATTRIBUTES_LIST(1,
            PG_BUFFER_ATTRIBUTE("pixel", PG_UBVEC4)
        )
    );
    if(!pg_buffer_init(&tex->buf, &buffer_layout, n_pixels)) {
        pg_log(PG_LOG_ERROR, "Failed to initialize buffer for texture.");
        for(int i = 0; i < file_i; ++i) free(tex_data[i]);
        return false;
    }
    /*  Copy the individual texture data into the big allocation and free the
        original small allocations  */
    for(int i = 0; i < num_files; ++i) {
        if(uvec2_is_zero(tex_size[i]) || !tex_data[i]) continue;
        pg_texture_write_pixels(tex, tex_data[i], 0, 0, i, tex_size[i].x, tex_size[i].y, tex_size[i].x * pixel_stride);
        free(tex_data[i]);
    }
    return true;
}

void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int x, int y, int layer, int w, int h, int src_line_stride)
{
    uint8_t* layer_data = pg_texture_get_pixel_ptr(tex, 0, 0, layer);
    int pix_stride = pg_data_type_size[tex->pixel_type];
    int line_stride = tex->dimensions.x * pix_stride;
    if(x >= tex->dimensions.x || y >= tex->dimensions.y || layer >= tex->dimensions.z) return;
    int x_dst_start = LM_MAX(0, x);
    int x_src_start = -LM_MIN(0, x);
    int x_len = LM_MIN(tex->dimensions.x - x_dst_start, w);
    int y_dst_start = LM_MAX(0, y);
    int y_src_start = -LM_MIN(0, y);
    int y_len = LM_MIN(tex->dimensions.y - y_dst_start, h);
    if(x_len <= 0 || y_len <= 0) return;
    int i;
    for(i = 0; i < y_len; ++i) {
        void* dst_line = layer_data + (x_dst_start * pix_stride) + ((y_dst_start + i) * line_stride);
        void* src_line = data + (x_src_start * pix_stride) + ((y_src_start + i) * src_line_stride);
        memcpy(dst_line, src_line, x_len * pix_stride);
    }
}

bool pg_texture_save_to_file(struct pg_texture* tex, const char* filename)
{
    int error = 0;
    if(tex->pixel_type == PG_UBVEC3) {
        error = lodepng_encode24_file(filename, (const unsigned char*)tex->buf.data,
                tex->dimensions.x, tex->dimensions.y);
    } else if(tex->pixel_type == PG_UBVEC4) {
        error = lodepng_encode32_file(filename, (const unsigned char*)tex->buf.data,
                tex->dimensions.x, tex->dimensions.y);
    } else if(tex->pixel_type == PG_UBYTE) {
        error = lodepng_encode_file(filename, (const unsigned char*)tex->buf.data,
                tex->dimensions.x, tex->dimensions.y, LCT_GREY, 8);
    } else {
        pg_log(PG_LOG_ERROR, "Cannot save image ('%s') because pixel type (%s) cannot be saved.",
                filename, pg_data_type_string[tex->pixel_type]);
        return false;
    }
    if(error) {
        pg_log(PG_LOG_ERROR, "Failed to save texture to file '%s'. lodepng error %d: %s", filename, error, lodepng_error_text(error));
        return false;
    }
    return true;
}

void pg_texture_deinit(struct pg_texture* tex)
{
    if(!tex) return;
    pg_buffer_deinit(&tex->buf);
}

uint8_t* pg_texture_get_pixel_ptr(struct pg_texture* tex, int x, int y, int layer)
{
    if(!tex) return NULL;
    return pg_buffer_get_element_ptr(&tex->buf, x + (y * tex->dimensions.x) + (layer * tex->layer_stride));
}

bool pg_texture_compare_equal(struct pg_texture* tex0, struct pg_texture* tex1)
{
    if(!tex0 || !tex1) {
        pg_log(PG_LOG_DEBUG_PROCGAME, "Texture comparison: one or both textures was NULL");
        return false;
    } else if(!ivec3_cmp_eq(tex0->dimensions, tex1->dimensions)) {
        pg_log(PG_LOG_DEBUG_PROCGAME, "Texture comparison: dimensions do not match (%dx%dx%d : %dx%dx%d)",
                VEC_XYZ(tex0->dimensions), VEC_XYZ(tex1->dimensions));
        return false;
    } else if(tex0->buf.data_size != tex1->buf.data_size) {
        pg_log(PG_LOG_DEBUG_PROCGAME, "Texture comparison: data size does not match (%d : %d)",
                tex0->buf.data_size, tex1->buf.data_size);
        return false;
    } else if (tex0->pixel_type != tex1->pixel_type) {
        pg_log(PG_LOG_DEBUG_PROCGAME, "Texture comparison: pixel type does not match (%s : %s)",
                pg_data_type_string[tex0->pixel_type], pg_data_type_string[tex1->pixel_type]);
        return false;
    } else if(memcmp(tex0->buf.data, tex1->buf.data, tex0->buf.data_size) != 0) {
        pg_log(PG_LOG_DEBUG_PROCGAME, "Texture comparison: pixel contents do not match");
        return false;
    }
    return true;
}



/********************/
/*  GPU TEXTURES    */
/********************/

static const char* pg_gpu_texture_format_strings[PG_GPU_TEXTURE_N_FORMATS] = {
    [PG_GPU_TEXTURE_R8_UNORM] =             "R8",
    [PG_GPU_TEXTURE_R16_UNORM] =            "R16",
    [PG_GPU_TEXTURE_R8_SNORM] =             "R8_SNORM",
    [PG_GPU_TEXTURE_R16_SNORM] =            "R16_SNORM",
    [PG_GPU_TEXTURE_R8UI] =                 "R8UI",
    [PG_GPU_TEXTURE_R16UI] =                "R16UI",
    [PG_GPU_TEXTURE_R32UI] =                "R32UI",
    [PG_GPU_TEXTURE_R8I] =                  "R8I",
    [PG_GPU_TEXTURE_R16I] =                 "R16I",
    [PG_GPU_TEXTURE_R32I] =                 "R32I",
    [PG_GPU_TEXTURE_R32F] =                 "R32F",
    [PG_GPU_TEXTURE_RG8_UNORM] =            "RG8",
    [PG_GPU_TEXTURE_RG16_UNORM] =           "RG16",
    [PG_GPU_TEXTURE_RG8_SNORM] =            "RG8_SNORM",
    [PG_GPU_TEXTURE_RG16_SNORM] =           "RG16_SNORM",
    [PG_GPU_TEXTURE_RG8UI] =                "RG8UI",
    [PG_GPU_TEXTURE_RG16UI] =               "RG16UI",
    [PG_GPU_TEXTURE_RG32UI] =               "RG32UI",
    [PG_GPU_TEXTURE_RG8I] =                 "RG8I",
    [PG_GPU_TEXTURE_RG16I] =                "RG16I",
    [PG_GPU_TEXTURE_RG32I] =                "RG32I",
    [PG_GPU_TEXTURE_RG32F] =                "RG32F",
    [PG_GPU_TEXTURE_RGB8_UNORM] =           "RGB8",
    [PG_GPU_TEXTURE_RGB16_UNORM] =          "RGB16",
    [PG_GPU_TEXTURE_RGB8_SNORM] =           "RGB8_SNORM",
    [PG_GPU_TEXTURE_RGB16_SNORM] =          "RGB16_SNORM",
    [PG_GPU_TEXTURE_RGB8UI] =               "RGB8UI",
    [PG_GPU_TEXTURE_RGB16UI] =              "RGB16UI",
    [PG_GPU_TEXTURE_RGB32UI] =              "RGB32UI",
    [PG_GPU_TEXTURE_RGB8I] =                "RGB8I",
    [PG_GPU_TEXTURE_RGB16I] =               "RGB16I",
    [PG_GPU_TEXTURE_RGB32I] =               "RGB32I",
    [PG_GPU_TEXTURE_RGB32F] =               "RGB32F",
    [PG_GPU_TEXTURE_RGBA8_UNORM] =          "RGBA8",
    [PG_GPU_TEXTURE_RGBA16_UNORM] =         "RGBA16",
    [PG_GPU_TEXTURE_RGBA8_SNORM] =          "RGBA8_SNORM",
    [PG_GPU_TEXTURE_RGBA16_SNORM] =         "RGBA16_SNORM",
    [PG_GPU_TEXTURE_RGBA8UI] =              "RGBA8UI",
    [PG_GPU_TEXTURE_RGBA16UI] =             "RGBA16UI",
    [PG_GPU_TEXTURE_RGBA32UI] =             "RGBA32UI",
    [PG_GPU_TEXTURE_RGBA8I] =               "RGBA8I",
    [PG_GPU_TEXTURE_RGBA16I] =              "RGBA16I",
    [PG_GPU_TEXTURE_RGBA32I] =              "RGBA32I",
    [PG_GPU_TEXTURE_RGBA32F] =              "RGBA32F",
    [PG_GPU_TEXTURE_DEPTH16] =              "DEPTH16",
    [PG_GPU_TEXTURE_DEPTH24] =              "DEPTH24",
    [PG_GPU_TEXTURE_DEPTH32] =              "DEPTH32",
    [PG_GPU_TEXTURE_DEPTH32F] =             "DEPTH32F",
    [PG_GPU_TEXTURE_DEPTH24_STENCIL8] =     "DEPTH24_STENCIL8",
    [PG_GPU_TEXTURE_DEPTH32F_STENCIL8] =    "DEPTH32F_STENCIL8",
    [PG_GPU_TEXTURE_STENCIL8] =    "STENCIL8",
};

static const enum pg_gpu_texture_format pg_data_type_to_texture_format_norm[PG_NUM_DATA_TYPES] = {
    [PG_UBYTE] =    PG_GPU_TEXTURE_R8_UNORM,
    [PG_UBVEC2] =   PG_GPU_TEXTURE_RG8_UNORM,
    [PG_UBVEC3] =   PG_GPU_TEXTURE_RGB8_UNORM,
    [PG_UBVEC4] =   PG_GPU_TEXTURE_RGBA8_UNORM,
    [PG_BYTE] =     PG_GPU_TEXTURE_R8_SNORM,
    [PG_BVEC2] =    PG_GPU_TEXTURE_RG8_SNORM,
    [PG_BVEC3] =    PG_GPU_TEXTURE_RGB8_SNORM,
    [PG_BVEC4] =    PG_GPU_TEXTURE_RGBA8_SNORM,
    [PG_USHORT] =   PG_GPU_TEXTURE_R16_UNORM,
    [PG_USVEC2] =   PG_GPU_TEXTURE_RG16_UNORM,
    [PG_USVEC3] =   PG_GPU_TEXTURE_RGB16_UNORM,
    [PG_USVEC4] =   PG_GPU_TEXTURE_RGBA16_UNORM,
    [PG_SHORT] =    PG_GPU_TEXTURE_R16_SNORM,
    [PG_SVEC2] =    PG_GPU_TEXTURE_RG16_SNORM,
    [PG_SVEC3] =    PG_GPU_TEXTURE_RGB16_SNORM,
    [PG_SVEC4] =    PG_GPU_TEXTURE_RGBA16_SNORM,
    [PG_UINT] =     PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_UVEC2] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_UVEC3] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_UVEC4] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_INT] =      PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_IVEC2] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_IVEC3] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_IVEC4] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_FLOAT] =    PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_VEC2] =     PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_VEC3] =     PG_GPU_TEXTURE_INVALID_FORMAT,
    [PG_VEC4] =     PG_GPU_TEXTURE_INVALID_FORMAT,
};

//static const enum pg_gpu_texture_format pg_data_type_to_texture_format[PG_NUM_DATA_TYPES] = {
//    [PG_UBYTE] =    PG_GPU_TEXTURE_R8UI,
//    [PG_UBVEC2] =   PG_GPU_TEXTURE_RG8UI,
//    [PG_UBVEC3] =   PG_GPU_TEXTURE_RGB8UI,
//    [PG_UBVEC4] =   PG_GPU_TEXTURE_RGBA8UI,
//    [PG_BYTE] =     PG_GPU_TEXTURE_R8I,
//    [PG_BVEC2] =    PG_GPU_TEXTURE_RG8I,
//    [PG_BVEC3] =    PG_GPU_TEXTURE_RGB8I,
//    [PG_BVEC4] =    PG_GPU_TEXTURE_RGBA8I,
//    [PG_USHORT] =   PG_GPU_TEXTURE_R16UI,
//    [PG_USVEC2] =   PG_GPU_TEXTURE_RG16UI,
//    [PG_USVEC3] =   PG_GPU_TEXTURE_RGB16UI,
//    [PG_USVEC4] =   PG_GPU_TEXTURE_RGBA16UI,
//    [PG_SHORT] =    PG_GPU_TEXTURE_R16I,
//    [PG_SVEC2] =    PG_GPU_TEXTURE_RG16I,
//    [PG_SVEC3] =    PG_GPU_TEXTURE_RGB16I,
//    [PG_SVEC4] =    PG_GPU_TEXTURE_RGBA16I,
//    [PG_UINT] =     PG_GPU_TEXTURE_R32UI,
//    [PG_UVEC2] =    PG_GPU_TEXTURE_RG32UI,
//    [PG_UVEC3] =    PG_GPU_TEXTURE_RGB32UI,
//    [PG_UVEC4] =    PG_GPU_TEXTURE_RGBA32UI,
//    [PG_INT] =      PG_GPU_TEXTURE_R32I,
//    [PG_IVEC2] =    PG_GPU_TEXTURE_RG32I,
//    [PG_IVEC3] =    PG_GPU_TEXTURE_RGB32I,
//    [PG_IVEC4] =    PG_GPU_TEXTURE_RGBA32I,
//    [PG_FLOAT] =    PG_GPU_TEXTURE_R32F,
//    [PG_VEC2] =     PG_GPU_TEXTURE_RG32F,
//    [PG_VEC3] =     PG_GPU_TEXTURE_RGB32F,
//    [PG_VEC4] =     PG_GPU_TEXTURE_RGBA32F,
//};

enum pg_gpu_texture_format pg_gpu_texture_format_from_string(const char* string)
{
    if(!string) return PG_GPU_TEXTURE_INVALID_FORMAT;
    for(int i = 0; i < PG_GPU_TEXTURE_N_FORMATS; ++i) {
        if(!pg_gpu_texture_format_strings[i]) continue;
        if(strncmp(pg_gpu_texture_format_strings[i], string, 24) == 0) return i;
    }
    return PG_GPU_TEXTURE_INVALID_FORMAT;
}

enum pg_gpu_texture_type pg_gpu_texture_format_type(enum pg_gpu_texture_format format)
{
    if(format <= 0 || format >= PG_GPU_TEXTURE_N_FORMATS) return PG_GPU_INVALID_TEXTURE;
    if(format >= PG_GPU_TEXTURE_DEPTH24_STENCIL8) return PG_GPU_DEPTH_STENCIL_TEXTURE;
    if(format >= PG_GPU_TEXTURE_DEPTH16) return PG_GPU_DEPTH_TEXTURE;
    return PG_GPU_IMAGE_TEXTURE;
}


/****************/
/*  OpenGL      */
/****************/

#include "pg_gpu/gl_tex_info.h"


/*  GPU TEXTURE I/O */
void pg_gpu_texture_params_apply(struct pg_gpu_texture_params* params, pg_gpu_texture_t* tex)
{
    if(!params) return;
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_WRAP_S, gl_texture_param_wrap[params->wrap_x]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_WRAP_T, gl_texture_param_wrap[params->wrap_y]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_WRAP_R, gl_texture_param_wrap[params->wrap_z]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_MIN_FILTER, gl_texture_param_filter[params->filter_min]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_MAG_FILTER, gl_texture_param_filter[params->filter_mag]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_DEPTH_STENCIL_TEXTURE_MODE, gl_texture_param_ds_mode[params->depth_stencil_mode]));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_MIN_LOD, params->min_lod));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_MAX_LOD, params->max_lod));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_LOD_BIAS, params->lod_bias));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_BASE_LEVEL, params->base_mipmap));
    PG_CHECK_GL(glTexParameteri(tex->gl_texture_binding, GL_TEXTURE_MAX_LEVEL, params->max_mipmap));
    PG_CHECK_GL(glTexParameterfv(tex->gl_texture_binding, GL_TEXTURE_BORDER_COLOR, params->border_color.v));
    //GLenum swizzle[4] = {
    //    gl_texture_param_swizzle[params->swizzle[0]],
    //    gl_texture_param_swizzle[params->swizzle[1]],
    //    gl_texture_param_swizzle[params->swizzle[2]],
    //    gl_texture_param_swizzle[params->swizzle[3]], };
    //PG_CHECK_GL(glTexParameterIuiv(tex->gl_texture_binding, GL_TEXTURE_SWIZZLE_RGBA, swizzle));
}


pg_gpu_texture_t* pg_texture_upload(struct pg_texture* tex)
{
    if(!tex || tex->dimensions.x <= 0 || tex->dimensions.y <= 0 || tex->dimensions.z <= 0)
        return NULL;
    enum pg_gpu_texture_format tex_format = pg_data_type_to_texture_format_norm[tex->pixel_type];
    if(tex_format == PG_GPU_TEXTURE_INVALID_FORMAT) return NULL;
    const struct pg_gpu_texture_format_info* format_info = &gl_format_info[tex_format];
    struct pg_gpu_texture* gpu_tex = malloc(sizeof(*gpu_tex));
    gpu_tex->format = tex_format;
    gpu_tex->gl_texture_binding = GL_TEXTURE_2D_ARRAY;
    gpu_tex->is_3D = true;
    gpu_tex->dimensions = tex->dimensions;
    gpu_tex->params = gpu_texture_params_default;
    PG_CHECK_GL(glGenTextures(1, &gpu_tex->gl));
    PG_CHECK_GL(glActiveTexture(GL_TEXTURE0));
    PG_CHECK_GL(glBindTexture(gpu_tex->gl_texture_binding, gpu_tex->gl));
    PG_CHECK_GL(glTexImage3D(gpu_tex->gl_texture_binding,
                0, // base mip level
                format_info->gl_format, // ex. GL_RGB16UI
                tex->dimensions.x, tex->dimensions.y, tex->dimensions.z,
                0, // border (must be 0)
                format_info->gl_pixel_layout, // ex. GL_RGB
                format_info->gl_type, // ex. GL_UNSIGNED_BYTE
                tex->buf.data));
    pg_gpu_texture_params_apply(&gpu_tex->params, gpu_tex);
    return gpu_tex;
}

void pg_gpu_texture_download(pg_gpu_texture_t* gpu_tex, struct pg_texture* output)
{
    if(!gpu_tex) return;
    const struct pg_gpu_texture_format_info* format_info = &gl_format_info[gpu_tex->format];
    pg_texture_init(output, format_info->pg_format, VEC_XYZ(gpu_tex->dimensions));
    PG_CHECK_GL(glBindTexture(gpu_tex->gl_texture_binding, gpu_tex->gl));
    PG_CHECK_GL(glGetTexImage(gpu_tex->gl_texture_binding, 0,
                              format_info->gl_pixel_layout, // ex. GL_RGB
                              format_info->gl_type, // ex. GL_UNSIGNED_INT
                              output->buf.data));
}

pg_gpu_texture_t* pg_gpu_texture_alloc(enum pg_gpu_texture_format format, int width, int height, int layers)
{
    if(format == PG_GPU_TEXTURE_INVALID_FORMAT) return NULL;
    struct pg_gpu_texture* gpu_tex = malloc(sizeof(*gpu_tex));
    gpu_tex->dimensions = ivec3(width, height, layers);
    gpu_tex->is_3D = true;
    gpu_tex->params = gpu_texture_params_default;
    gpu_tex->format = format;
    const struct pg_gpu_texture_format_info* format_info = &gl_format_info[format];
    gpu_tex->gl_texture_binding = GL_TEXTURE_2D_ARRAY;
    PG_CHECK_GL(glGenTextures(1, &gpu_tex->gl));
    PG_CHECK_GL(glActiveTexture(GL_TEXTURE0));
    PG_CHECK_GL(glBindTexture(gpu_tex->gl_texture_binding, gpu_tex->gl));
    if(format >= PG_GPU_TEXTURE_DEPTH16) {
        PG_CHECK_GL(glTexStorage3D(gpu_tex->gl_texture_binding, // ex. GL_TEXTURE_2D
                    1,  // base mip level
                    format_info->gl_format,  // ex. GL_RGBA32F
                    width, height, layers)); // no initial data
    } else {
        PG_CHECK_GL(glTexImage3D(gpu_tex->gl_texture_binding, // ex. GL_TEXTURE_2D
                    0,  // base mip level
                    format_info->gl_format,  // ex. GL_RGBA32F
                    width, height, layers,
                    0,  // border (must be 0)
                    format_info->gl_pixel_layout, // ex. GL_RGBA
                    format_info->gl_type,    // ex. GL_FLOAT
                    NULL)); // no initial data
    }
    return gpu_tex;
}


pg_gpu_texture_t* pg_gpu_texture_alloc_2D(enum pg_gpu_texture_format format, int width, int height)
{
    if(format == PG_GPU_TEXTURE_INVALID_FORMAT) return NULL;
    struct pg_gpu_texture* gpu_tex = malloc(sizeof(*gpu_tex));
    gpu_tex->dimensions = ivec3(width, height, 1);
    gpu_tex->is_3D = false;
    gpu_tex->params = gpu_texture_params_default;
    gpu_tex->format = format;
    const struct pg_gpu_texture_format_info* format_info = &gl_format_info[format];
    gpu_tex->gl_texture_binding = GL_TEXTURE_2D;
    PG_CHECK_GL(glGenTextures(1, &gpu_tex->gl));
    PG_CHECK_GL(glActiveTexture(GL_TEXTURE0));
    PG_CHECK_GL(glBindTexture(gpu_tex->gl_texture_binding, gpu_tex->gl));
    if(format >= PG_GPU_TEXTURE_DEPTH16) {
        PG_CHECK_GL(glTexStorage2D(gpu_tex->gl_texture_binding, // ex. GL_TEXTURE_2D
                    1,  // base mip level
                    format_info->gl_format,  // ex. GL_RGBA32F
                    width, height)); // no initial data
    } else {
        PG_CHECK_GL(glTexImage2D(gpu_tex->gl_texture_binding, // ex. GL_TEXTURE_2D
                    0,  // base mip level
                    format_info->gl_format,  // ex. GL_RGBA32F
                    width, height,
                    0,  // border (must be 0)
                    format_info->gl_pixel_layout, // ex. GL_RGBA
                    format_info->gl_type,    // ex. GL_FLOAT
                    NULL)); // no initial data
    }
    return gpu_tex;
}


void pg_gpu_texture_deinit(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return;
    PG_CHECK_GL(glDeleteTextures(1, &gpu_tex->gl));
    free(gpu_tex);
}


/*  GPU TEXTURE INSPECTION  */

void pg_gpu_texture_get_params(pg_gpu_texture_t* gpu_tex, pg_gpu_texture_params_t* params)
{
    if(!gpu_tex) return;
    *params = gpu_tex->params;
}

GLuint pg_gpu_texture_get_handle(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return 0;
    return gpu_tex->gl;
}

GLuint pg_gpu_texture_get_binding_point(pg_gpu_texture_t* gpu_tex)
{
    return gpu_tex->gl_texture_binding;
}

enum pg_gpu_texture_format pg_gpu_texture_get_format(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return PG_GPU_TEXTURE_INVALID_FORMAT;
    return gpu_tex->format;
}

enum pg_gpu_texture_type pg_gpu_texture_get_type(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return PG_GPU_INVALID_TEXTURE;
    return pg_gpu_texture_format_type(gpu_tex->format);
}

ivec3 pg_gpu_texture_get_dimensions(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return ivec3(0,0,0);
    return gpu_tex->dimensions;
}

bool pg_gpu_texture_is_3D(pg_gpu_texture_t* gpu_tex)
{
    return gpu_tex->is_3D;
}



/****************************/
/*  GPU TEXTURE VIEWS       */
/****************************/

pg_gpu_texture_view_t* pg_gpu_texture_get_basic_view(pg_gpu_texture_t* gpu_tex)
{
    if(!gpu_tex) return NULL;
    struct pg_gpu_texture_view* gpu_tex_view = malloc(sizeof(*gpu_tex_view));
    gpu_tex_view->gl_tex = gpu_tex;
    gpu_tex_view->layer_start = 0;
    gpu_tex_view->n_layers = gpu_tex->dimensions.z;
    return gpu_tex_view;
}

pg_gpu_texture_view_t* pg_gpu_texture_get_layer_view(pg_gpu_texture_t* gpu_tex, int layer)
{
    if(!gpu_tex || layer < 0 || layer >= gpu_tex->dimensions.z) return NULL;
    struct pg_gpu_texture_view* gpu_tex_view = malloc(sizeof(*gpu_tex_view));
    gpu_tex_view->gl_tex = gpu_tex;
    gpu_tex_view->layer_start = layer;
    gpu_tex_view->n_layers = 1;
    return gpu_tex_view;
}

pg_gpu_texture_view_t* pg_gpu_texture_get_range_view(pg_gpu_texture_t* gpu_tex,
                                                 int layer_start, int n_layers)
{
    if(!gpu_tex || layer_start < 0 || n_layers <= 0) return NULL;
    int gpu_tex_layers = gpu_tex->dimensions.z;
    if(layer_start >= gpu_tex_layers || layer_start + n_layers >= gpu_tex_layers) return NULL;
    struct pg_gpu_texture_view* gpu_tex_view = malloc(sizeof(*gpu_tex_view));
    gpu_tex_view->gl_tex = gpu_tex;
    gpu_tex_view->layer_start = layer_start;
    gpu_tex_view->n_layers = n_layers;
    return gpu_tex_view;
}

pg_gpu_texture_t* pg_gpu_texture_view_get_base(pg_gpu_texture_view_t* gpu_tex_view)
{
    if(!gpu_tex_view) return NULL;
    return gpu_tex_view->gl_tex;
}

ivec2 pg_gpu_texture_view_get_range(pg_gpu_texture_view_t* gpu_tex_view)
{
    if(!gpu_tex_view) return ivec2(0,0);
    return ivec2(gpu_tex_view->layer_start, gpu_tex_view->n_layers);
}

void pg_gpu_texture_view_deinit(pg_gpu_texture_view_t* gpu_tex_view)
{
    if(!gpu_tex_view) return;
    free(gpu_tex_view);
}

/****************/
/*  END OPENGL  */
/****************/


