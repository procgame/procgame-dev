#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_util/pg_util.h"


static const char* pg_shader_uniform_type_string[PG_SHADER_N_UNIFORM_TYPES] = {
    [PG_SHADER_UNIFORM_INVALID] = "invalid",
    [PG_SHADER_UNIFORM_TEXTURE] = "texture",
    [PG_SHADER_UNIFORM_TEXTURE_BUFFER] = "texture_buffer",
    [PG_SHADER_UNIFORM_BUFFER] = "uniform_buffer",
    [PG_SHADER_UNIFORM_DYNAMIC] = "dynamic",
};

const char* pg_shader_uniform_type_to_string(enum pg_shader_uniform_type type)
{
    if(type < 0 || type >= PG_SHADER_N_UNIFORM_TYPES) return "invalid";
    return pg_shader_uniform_type_string[type];
}

enum pg_shader_uniform_type pg_shader_uniform_type_from_string(const char* str, int n)
{
    for(int i = 0; i < PG_SHADER_N_UNIFORM_TYPES; ++i) {
        if(strncmp(str, pg_shader_uniform_type_string[i], n + 1) == 0) return i;
    }
    return PG_SHADER_UNIFORM_INVALID;
}


/****************************/
/*  SHADER INTERFACE INFO   */
/****************************/
/*  Not attached to real GPU resources or shader objects yet,
    just a logical interface description  */

void pg_shader_interface_info_init(struct pg_shader_interface_info* sh_iface_info)
{
    if(!sh_iface_info) return;
    SARR_INIT(sh_iface_info->dynamic_uniforms);
    SARR_INIT(sh_iface_info->uniform_inputs);
    SARR_INIT(sh_iface_info->vertex_inputs);
    SARR_INIT(sh_iface_info->output_layout);
}

void pg_shader_interface_info_deinit(struct pg_shader_interface_info* sh_iface_info)
{
    if(!sh_iface_info) return;
}



bool pg_shader_interface_info_add_buffer_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int binding, struct pg_buffer_layout* layout)
{
    if(!layout) return false;
    struct pg_shader_uniform_info* new_uni = ARR_NEW(sh_iface_info->uniform_inputs);
    strncpy(new_uni->name, name, PG_SHADER_INPUT_NAME_MAXLEN);
    new_uni->type = PG_SHADER_UNIFORM_BUFFER;
    new_uni->location = location;
    new_uni->binding = binding;
    new_uni->layout = *layout;
    return true;
}

bool pg_shader_interface_info_add_texture_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int binding, bool buffer)
{
    struct pg_shader_uniform_info* new_uni = ARR_NEW(sh_iface_info->uniform_inputs);
    strncpy(new_uni->name, name, PG_SHADER_INPUT_NAME_MAXLEN);
    new_uni->type = buffer ? PG_SHADER_UNIFORM_TEXTURE_BUFFER : PG_SHADER_UNIFORM_TEXTURE;
    new_uni->location = location;
    new_uni->binding = binding;
    return true;
}

bool pg_shader_interface_info_add_dynamic_uniform_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, int count, enum pg_data_type type)
{
    if(!type) return false;
    struct pg_shader_uniform_info* new_uni = ARR_NEW(sh_iface_info->dynamic_uniforms);
    strncpy(new_uni->name, name, PG_SHADER_INPUT_NAME_MAXLEN);
    new_uni->type = PG_SHADER_UNIFORM_DYNAMIC;
    new_uni->count = count;
    new_uni->location = location;
    new_uni->dynamic_type = type;
    return true;
}

bool pg_shader_interface_info_add_vertex_input(struct pg_shader_interface_info* sh_iface_info,
        const char* name, int location, enum pg_data_type type)
{
    struct pg_shader_vertex_info* new_vert = ARR_NEW(sh_iface_info->vertex_inputs);
    strncpy(new_vert->name, name, PG_SHADER_INPUT_NAME_MAXLEN);
    new_vert->location = location;
    new_vert->type = type;
    return true;
}

bool pg_shader_interface_info_add_output(struct pg_shader_interface_info* sh_iface_info,
        const char* name, enum pg_gpu_framebuffer_attachment_index location,
        enum pg_data_type type)
{
    struct pg_shader_output_info* new_output = ARR_NEW(sh_iface_info->output_layout);
    strncpy(new_output->name, name, PG_SHADER_INPUT_NAME_MAXLEN);
    new_output->location = location;
    new_output->type = type;
    return true;
}

int pg_shader_interface_info_get_dynamic_uniform_index(const struct pg_shader_interface_info* sh_iface_info, const char* uni_name)
{
    if(!sh_iface_info) return -1;
    int i;
    const struct pg_shader_uniform_info* uni_info;
    ARR_FOREACH_PTR(sh_iface_info->dynamic_uniforms, uni_info, i) {
        if(strncmp(uni_info->name, uni_name, PG_SHADER_INPUT_NAME_MAXLEN) == 0) return i;
    }
    return -1;
}

int pg_shader_interface_info_get_uniform_index(const struct pg_shader_interface_info* sh_iface_info, const char* uni_name)
{
    if(!sh_iface_info) return -1;
    int i;
    const struct pg_shader_uniform_info* uni_info;
    ARR_FOREACH_PTR(sh_iface_info->uniform_inputs, uni_info, i) {
        if(strncmp(uni_info->name, uni_name, PG_SHADER_INPUT_NAME_MAXLEN) == 0) return i;
    }
    return -1;
}

int pg_shader_interface_info_get_vertex_index(const struct pg_shader_interface_info* sh_iface_info, const char* vert_name)
{
    if(!sh_iface_info) return -1;
    int i;
    const struct pg_shader_vertex_info* vert_info;
    ARR_FOREACH_PTR(sh_iface_info->vertex_inputs, vert_info, i) {
        if(strncmp(vert_info->name, vert_name, PG_SHADER_INPUT_NAME_MAXLEN) == 0) return i;
    }
    return -1;
}

int pg_shader_interface_info_get_output_index(const struct pg_shader_interface_info* sh_iface_info, const char* out_name)
{
    if(!sh_iface_info) return -1;
    int i;
    const struct pg_shader_output_info* out_info;
    ARR_FOREACH_PTR(sh_iface_info->output_layout, out_info, i) {
        if(strncmp(out_info->name, out_name, PG_SHADER_INPUT_NAME_MAXLEN) == 0) return i;
    }
    return -1;
}

const struct pg_shader_uniform_info* pg_shader_interface_info_get_dynamic_uniform_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* uni_name)
{
    int idx = pg_shader_interface_info_get_dynamic_uniform_index(sh_iface_info, uni_name);
    if(idx < 0) return NULL;
    return &sh_iface_info->dynamic_uniforms.data[idx];
}

const struct pg_shader_uniform_info* pg_shader_interface_info_get_uniform_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* uni_name)
{
    int idx = pg_shader_interface_info_get_uniform_index(sh_iface_info, uni_name);
    if(idx < 0) return NULL;
    return &sh_iface_info->uniform_inputs.data[idx];
}

const struct pg_shader_vertex_info* pg_shader_interface_info_get_vertex_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* vert_name)
{
    int idx = pg_shader_interface_info_get_vertex_index(sh_iface_info, vert_name);
    if(idx < 0) return NULL;
    return &sh_iface_info->vertex_inputs.data[idx];
}

const struct pg_shader_output_info* pg_shader_interface_info_get_output_info(
        const struct pg_shader_interface_info* sh_iface_info, const char* out_name)
{
    int idx = pg_shader_interface_info_get_output_index(sh_iface_info, out_name);
    if(idx < 0) return NULL;
    return &sh_iface_info->output_layout.data[idx];
}


