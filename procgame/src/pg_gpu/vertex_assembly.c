#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

struct pg_gpu_vertex_assembly {
    struct pg_gpu_vertex_assembly_binding {
        pg_gpu_buffer_t* buffer;
        bool is_instance_data;
    } bindings[16];
    struct pg_gpu_vertex_assembly_attribute {
        int binding;
        struct pg_buffer_attribute attribute;
    } attributes[16];
    SARR_T(32, struct pg_gpu_vertex_assembly_range {
        char name[128];
        ivec2 verts_range;
        ivec2 idx_range;
    }) ranges;
    pg_gpu_buffer_t* index_buffer;
    GLuint gl_vao;
};

static void pg_gpu_vertex_assembly_init(pg_gpu_vertex_assembly_t* vasm)
{
    *vasm = (pg_gpu_vertex_assembly_t){};
    SARR_INIT(vasm->ranges);
    PG_CHECK_GL(glGenVertexArrays(1, &vasm->gl_vao));
}

static void pg_gpu_vertex_assembly_deinit(pg_gpu_vertex_assembly_t* vasm)
{
    PG_CHECK_GL(glDeleteVertexArrays(1, &vasm->gl_vao));
    *vasm = (pg_gpu_vertex_assembly_t){};
}

pg_gpu_vertex_assembly_t* pg_gpu_vertex_assembly_create(void)
{
    pg_gpu_vertex_assembly_t* vasm = malloc(sizeof(*vasm));
    pg_gpu_vertex_assembly_init(vasm);
    return vasm;
}

void pg_gpu_vertex_assembly_destroy(pg_gpu_vertex_assembly_t* vasm)
{
    pg_gpu_vertex_assembly_deinit(vasm);
    free(vasm);
}

static void add_ranges_from_buffer(pg_gpu_vertex_assembly_t* vasm, pg_gpu_buffer_t* buf, bool idx_range)
{
    int n_ranges_buf = pg_gpu_buffer_get_range_count(buf);
    for(int i = 0; i < n_ranges_buf; ++i) {
        const char* buf_range_name = pg_gpu_buffer_get_range_name(buf, i);
        pg_log(PG_LOG_DEBUG, "Adding buffer range '%s' to %s", buf_range_name, idx_range ? "index ranges" : "vertex ranges");
        ivec2 buf_range = pg_gpu_buffer_get_range(buf, i);
        int j;
        struct pg_gpu_vertex_assembly_range* vasm_range;
        ARR_FOREACH_PTR(vasm->ranges, vasm_range, j) {
            if(strncmp(buf_range_name, vasm_range->name, 128) == 0) break;
        }
        if(j == vasm->ranges.len) {
            vasm_range = ARR_NEW(vasm->ranges);
            strncpy(vasm_range->name, buf_range_name, 128);
        }
        ivec2* vasm_range_i = idx_range ? &vasm_range->idx_range : &vasm_range->verts_range;
        if(!ivec2_is_zero(*vasm_range_i) && !ivec2_cmp_eq(*vasm_range_i, buf_range)) {
            pg_log(PG_LOG_ERROR, "Mismatched range for buffer range '%s':\n"
                                 " Original value: (%d, %d), mismatched value: (%d, %d)",
                    buf_range_name, VEC_XY(*vasm_range_i), VEC_XY(buf_range));
        }
        *vasm_range_i = buf_range;
    }
}

void pg_gpu_vertex_assembly_set_index_buffer(pg_gpu_vertex_assembly_t* vasm,
        pg_gpu_buffer_t* index_buffer)
{
    vasm->index_buffer = index_buffer;
    add_ranges_from_buffer(vasm, index_buffer, true);
}

void pg_gpu_vertex_assembly_set_binding(pg_gpu_vertex_assembly_t* vasm, int binding_idx,
        pg_gpu_buffer_t* source_buffer, bool instanced)
{
    struct pg_gpu_vertex_assembly_binding* binding = &vasm->bindings[binding_idx];
    binding->buffer = source_buffer;
    binding->is_instance_data = instanced;
    add_ranges_from_buffer(vasm, source_buffer, false);
}

void pg_gpu_vertex_assembly_attribute(pg_gpu_vertex_assembly_t* vasm, int binding_idx,
        int attribute_idx, const char* attrib_name)
{
    pg_gpu_buffer_t* source_buffer = vasm->bindings[binding_idx].buffer;
    int source_buffer_attribute_idx = pg_gpu_buffer_get_attribute_index(source_buffer, attrib_name);
    if(source_buffer_attribute_idx < 0) {
        pg_log(PG_LOG_ERROR, "Trying to attach vertex attribute '%s' but it is not present in the source buffer", attrib_name);
        return;
    }
    const struct pg_buffer_attribute* buf_attrib =
            pg_gpu_buffer_get_attribute(source_buffer, source_buffer_attribute_idx);
    struct pg_gpu_vertex_assembly_attribute* vasm_attrib = &vasm->attributes[attribute_idx];
    vasm_attrib->binding = binding_idx;
    vasm_attrib->attribute = *buf_attrib;
    PG_CHECK_GL(glBindVertexArray(vasm->gl_vao));
    PG_CHECK_GL(glBindBuffer(GL_ARRAY_BUFFER, pg_gpu_buffer_get_handle(source_buffer)));
    enum pg_data_type attrib_type = buf_attrib->info.type;
    if(pg_data_base_type[buf_attrib->info.type] == PG_INT
    || pg_data_base_type[buf_attrib->info.type] == PG_SHORT
    || pg_data_base_type[buf_attrib->info.type] == PG_BYTE
    || pg_data_base_type[buf_attrib->info.type] == PG_UINT
    || pg_data_base_type[buf_attrib->info.type] == PG_USHORT
    || pg_data_base_type[buf_attrib->info.type] == PG_UBYTE) {
        PG_CHECK_GL(glVertexAttribIPointer(
                attribute_idx,
                pg_data_type_components[attrib_type],
                pg_data_type_to_gl(attrib_type),
                buf_attrib->stride,
                (void*)(long)buf_attrib->offset));
    } else {
        PG_CHECK_GL(glVertexAttribPointer(
                attribute_idx,
                pg_data_type_components[attrib_type],
                pg_data_type_to_gl(attrib_type),
                GL_FALSE,
                buf_attrib->stride,
                (void*)(long)buf_attrib->offset));
    }
    PG_CHECK_GL(glEnableVertexAttribArray(attribute_idx));
}

pg_gpu_buffer_t* pg_gpu_vertex_assembly_get_attribute_buffer(pg_gpu_vertex_assembly_t* vasm,
        int shader_attribute, struct pg_buffer_attribute* out_attrib)
{
    struct pg_gpu_vertex_assembly_attribute* vasm_attrib = &vasm->attributes[shader_attribute];
    if(!vasm_attrib->attribute.info.type) return NULL;
    int binding = vasm_attrib->binding;
    if(out_attrib) *out_attrib = vasm_attrib->attribute;
    return vasm->bindings[binding].buffer;
}

void pg_gpu_vertex_assembly_bind(pg_gpu_vertex_assembly_t* vasm)
{
    PG_CHECK_GL(glBindVertexArray(vasm->gl_vao));
    if(vasm->index_buffer) {
        PG_CHECK_GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pg_gpu_buffer_get_handle(vasm->index_buffer)));
    }
}


int pg_gpu_vertex_assembly_get_range_count(pg_gpu_vertex_assembly_t* vbuf)
{
    return vbuf->ranges.len;
}

const char* pg_gpu_vertex_assembly_get_range_name(pg_gpu_vertex_assembly_t* vbuf, int range_idx)
{
    return vbuf->ranges.data[range_idx].name;
}

int pg_gpu_vertex_assembly_get_range_index(pg_gpu_vertex_assembly_t* vbuf, const char* name)
{
    int i;
    struct pg_gpu_vertex_assembly_range* range;
    ARR_FOREACH_PTR(vbuf->ranges, range, i) {
        if(strncmp(range->name, name, 128) == 0) return i;
    }
    return -1;
}

void pg_gpu_vertex_assembly_get_range(pg_gpu_vertex_assembly_t* vbuf, int range_idx, ivec2* vert_range, ivec2* idx_range)
{
    struct pg_gpu_vertex_assembly_range* range = &vbuf->ranges.data[range_idx];
    if(vert_range) *vert_range = range->verts_range;
    if(idx_range) *idx_range = range->idx_range;
}


void pg_gpu_vertex_assembly_debug_log_ranges(pg_gpu_vertex_assembly_t* vbuf)
{
    int n_ranges = pg_gpu_vertex_assembly_get_range_count(vbuf);
    pg_log(PG_LOG_DEBUG, "Vertex assembly has %d ranges:", n_ranges);
    for(int i = 0; i < n_ranges; ++i) {
        const char* range_name = pg_gpu_vertex_assembly_get_range_name(vbuf, i);
        ivec2 verts_range, idx_range;
        pg_gpu_vertex_assembly_get_range(vbuf, i, &verts_range, &idx_range);
        pg_log(PG_LOG_CONTD, " '%s': vertex (%d, %d), index (%d, %d)",
                range_name, VEC_XY(verts_range), VEC_XY(idx_range));
    }
}
