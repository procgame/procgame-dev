#include <string.h>

#include "pg_core/sdl.h"
#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"
#include "pg_modules/window.h"

enum pg_gpu_framebuffer_attachment_index pg_gpu_framebuffer_attachment_index_from_string(const char* str)
{
    if(strncmp(str, "color0", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR0;
    else if(strncmp(str, "color1", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR1;
    else if(strncmp(str, "color2", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR2;
    else if(strncmp(str, "color3", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR3;
    else if(strncmp(str, "color4", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR4;
    else if(strncmp(str, "color5", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR5;
    else if(strncmp(str, "color6", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR6;
    else if(strncmp(str, "color7", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_COLOR7;
    else if(strncmp(str, "depth", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_DEPTH;
    else if(strncmp(str, "depth_stencil", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_DEPTH_STENCIL;
    else if(strncmp(str, "stencil", PG_GPU_FRAMEBUFFER_ATTACHMENT_INDEX_STRING_MAX_LEN) == 0)
        return PG_GPU_FRAMEBUFFER_STENCIL;
    else
        return PG_GPU_FRAMEBUFFER_INVALID;
}

struct pg_gpu_framebuffer {
    pg_gpu_texture_view_t* tex_views[8];
    ivec2 dimensions;
    int layer[8];
    GLint attachments[8];
    GLenum drawbufs[8];
    GLuint gl;
    int n_color_attachments;
};

pg_gpu_framebuffer_t* pg_gpu_framebuffer_screen(void)
{
    // Use NULL as the screen framebuffer signifier
    struct pg_gpu_framebuffer* gpu_fbuf = malloc(sizeof(*gpu_fbuf));
    *gpu_fbuf = (struct pg_gpu_framebuffer) {
        .dimensions = pg_window_size(),
    };
    return gpu_fbuf;
}

GLint gl_attachment_from_pg_attachment(enum pg_gpu_framebuffer_attachment_index attachment)
{
    if(attachment == PG_GPU_FRAMEBUFFER_DEPTH) return GL_DEPTH_ATTACHMENT;
    else if(attachment == PG_GPU_FRAMEBUFFER_DEPTH_STENCIL) return GL_DEPTH_STENCIL_ATTACHMENT;
    else if(attachment == PG_GPU_FRAMEBUFFER_STENCIL) return GL_STENCIL_ATTACHMENT;
    else if(attachment >= PG_GPU_FRAMEBUFFER_COLOR0) return GL_COLOR_ATTACHMENT0 + (attachment - PG_GPU_FRAMEBUFFER_COLOR0);
    else return GL_NONE;
}

enum pg_gpu_framebuffer_attachment_index pg_gpu_framebuffer_attachment_for_texture_format(enum pg_gpu_texture_format tex_format)
{
    if(tex_format == PG_GPU_TEXTURE_DEPTH16 || tex_format == PG_GPU_TEXTURE_DEPTH24
    || tex_format == PG_GPU_TEXTURE_DEPTH32 || tex_format == PG_GPU_TEXTURE_DEPTH32F) {
        return PG_GPU_FRAMEBUFFER_DEPTH;
    } else if(tex_format == PG_GPU_TEXTURE_DEPTH24_STENCIL8 || tex_format == PG_GPU_TEXTURE_DEPTH32F_STENCIL8) {
        return PG_GPU_FRAMEBUFFER_DEPTH_STENCIL;
    } else if(tex_format == PG_GPU_TEXTURE_STENCIL8) {
        return PG_GPU_FRAMEBUFFER_STENCIL;
    } else {
        return PG_GPU_FRAMEBUFFER_COLOR0;
    }
}

pg_gpu_framebuffer_t* pg_gpu_framebuffer_init(int n_textures, pg_gpu_texture_view_t** gpu_tex_views)
{
    if(n_textures <= 0 || !gpu_tex_views) return NULL;
    struct pg_gpu_framebuffer* gpu_fb = calloc(1, sizeof(*gpu_fb));
    pg_gpu_texture_t* gpu_textures[8] = {0};
    int used_color_attachments = 0;
    for(int i = 0; i < n_textures; ++i) {
        if(!gpu_tex_views[i]) continue;
        gpu_textures[i] = pg_gpu_texture_view_get_base(gpu_tex_views[i]);
        ivec3 tex_dimensions = pg_gpu_texture_get_dimensions(gpu_textures[i]);
        if(!ivec2_is_zero(gpu_fb->dimensions)) {
            if(!ivec2_cmp_eq(gpu_fb->dimensions, ivec2(VEC_XY(tex_dimensions)))) {
                return NULL;
            }
        } else {
            gpu_fb->dimensions = ivec2(VEC_XY(tex_dimensions));
        }
        gpu_fb->tex_views[i] = gpu_tex_views[i];
        gpu_fb->layer[i] = pg_gpu_texture_view_get_range(gpu_tex_views[i]).x;
        enum pg_gpu_texture_format tex_format = pg_gpu_texture_get_format(gpu_textures[i]);
        enum pg_gpu_framebuffer_attachment_index attachment = pg_gpu_framebuffer_attachment_for_texture_format(tex_format);
        if(attachment == PG_GPU_FRAMEBUFFER_COLOR0) {
            attachment += used_color_attachments;
            gpu_fb->drawbufs[used_color_attachments] = gl_attachment_from_pg_attachment(attachment);
            ++used_color_attachments;
            gpu_fb->attachments[i] = gpu_fb->drawbufs[i];
        } else {
            gpu_fb->attachments[i] = gl_attachment_from_pg_attachment(attachment);
        }
    }
    gpu_fb->n_color_attachments = used_color_attachments;
    GLuint gl_fbo;
    PG_CHECK_GL(glGenFramebuffers(1, &gl_fbo));
    PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, gl_fbo));
    for(int i = 0; i < n_textures; ++i) {
        if(pg_gpu_texture_is_3D(gpu_textures[i])) {
            PG_CHECK_GL(glFramebufferTextureLayer(GL_FRAMEBUFFER, gpu_fb->attachments[i],
                                      pg_gpu_texture_get_handle(gpu_textures[i]),
                                      0, gpu_fb->layer[i]));
        } else {
            PG_CHECK_GL(glFramebufferTexture2D(GL_FRAMEBUFFER, gpu_fb->attachments[i],
                                      GL_TEXTURE_2D, pg_gpu_texture_get_handle(gpu_textures[i]),
                                      0));
        }
    };
    gpu_fb->gl = gl_fbo;
    return gpu_fb;
}

pg_gpu_framebuffer_t* pg_gpu_framebuffer_init_attachments(int n_textures,
        pg_gpu_texture_view_t** gpu_tex_views, enum pg_gpu_framebuffer_attachment_index* attachments)
{
    if(n_textures <= 0 || !gpu_tex_views) return NULL;
    struct pg_gpu_framebuffer* gpu_fb = calloc(1, sizeof(*gpu_fb));
    pg_gpu_texture_t* gpu_textures[8] = {0};
    int used_color_attachments = 0;
    for(int i = 0; i < n_textures; ++i) {
        if(!gpu_tex_views[i]) continue;
        gpu_textures[i] = pg_gpu_texture_view_get_base(gpu_tex_views[i]);
        ivec3 tex_dimensions = pg_gpu_texture_get_dimensions(gpu_textures[i]);
        if(!ivec2_is_zero(gpu_fb->dimensions)) {
            if(!ivec2_cmp_eq(gpu_fb->dimensions, ivec2(VEC_XY(tex_dimensions)))) {
                return NULL;
            }
        } else {
            gpu_fb->dimensions = ivec2(VEC_XY(tex_dimensions));
        }
        gpu_fb->tex_views[i] = gpu_tex_views[i];
        gpu_fb->layer[i] = pg_gpu_texture_view_get_range(gpu_tex_views[i]).x;
        gpu_fb->attachments[i] = gl_attachment_from_pg_attachment(attachments[i]);
        if(attachments[i] >= PG_GPU_FRAMEBUFFER_COLOR0) {
            gpu_fb->drawbufs[used_color_attachments] = gl_attachment_from_pg_attachment(attachments[i]);
            ++used_color_attachments;
        } else {
            gpu_fb->drawbufs[i] = GL_NONE;
        }
    }
    gpu_fb->n_color_attachments = used_color_attachments;
    GLuint gl_fbo;
    PG_CHECK_GL(glGenFramebuffers(1, &gl_fbo));
    PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, gl_fbo));
    for(int i = 0; i < n_textures; ++i) {
        if(pg_gpu_texture_is_3D(gpu_textures[i])) {
            PG_CHECK_GL(glFramebufferTextureLayer(GL_FRAMEBUFFER, gpu_fb->attachments[i],
                                      pg_gpu_texture_get_handle(gpu_textures[i]),
                                      0, gpu_fb->layer[i]));
        } else {
            PG_CHECK_GL(glFramebufferTexture2D(GL_FRAMEBUFFER, gpu_fb->attachments[i], GL_TEXTURE_2D,
                                      pg_gpu_texture_get_handle(gpu_textures[i]), 0));
        }
    };
    gpu_fb->gl = gl_fbo;
    return gpu_fb;
}

void pg_gpu_framebuffer_deinit(pg_gpu_framebuffer_t* gpu_fbuf)
{
    if(!gpu_fbuf) return;
    if(gpu_fbuf->gl) PG_CHECK_GL(glDeleteFramebuffers(1, &gpu_fbuf->gl));
    free(gpu_fbuf);
}

/** Set a GPU framebuffer as the destination for subsequent draw operations */ 
void pg_gpu_framebuffer_dst(pg_gpu_framebuffer_t* gpu_fbuf)
{
    if(!gpu_fbuf) {
        ivec2 window_size = pg_window_size();
        PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
        const GLenum drawbufs[] = { GL_COLOR_ATTACHMENT0 };
        PG_CHECK_GL(glDrawBuffers(1, drawbufs));
        PG_CHECK_GL(glViewport(0, 0, window_size.x, window_size.y));
    } else {
        PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, gpu_fbuf->gl));
        if(gpu_fbuf->n_color_attachments) {
            PG_CHECK_GL(glDrawBuffers(gpu_fbuf->n_color_attachments, gpu_fbuf->drawbufs));
        }
        PG_CHECK_GL(glViewport(0, 0, gpu_fbuf->dimensions.x, gpu_fbuf->dimensions.y));
    }
}

void pg_gpu_framebuffer_clear(pg_gpu_framebuffer_t* gpu_fbuf, vec4 color)
{
    PG_CHECK_GL(glBindFramebuffer(GL_FRAMEBUFFER, gpu_fbuf->gl));
    PG_CHECK_GL(glColorMask(true, true, true, true));
    PG_CHECK_GL(glDepthMask(true));
    PG_CHECK_GL(glStencilMask(true));
    PG_CHECK_GL(glClearColor(color.x, color.y, color.z, color.w));
    PG_CHECK_GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
}

ivec2 pg_gpu_framebuffer_get_dimensions(pg_gpu_framebuffer_t* gpu_fbuf)
{
    return gpu_fbuf->dimensions;
}

float pg_gpu_framebuffer_get_aspect_ratio(pg_gpu_framebuffer_t* gpu_fbuf)
{
    return (float)gpu_fbuf->dimensions.x / (float)gpu_fbuf->dimensions.y;
}




