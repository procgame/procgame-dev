#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"

void pg_buffer_builder_init(struct pg_buffer_builder* builder, struct pg_buffer* dst_buffer, ivec2 range)
{
    *builder = (struct pg_buffer_builder){
        .dst_buffer = dst_buffer,
        .range = range,
        .index = 0,
    };
}

void* pg_buffer_builder_get_element(struct pg_buffer_builder* builder)
{
    return pg_buffer_get_element_ptr(builder->dst_buffer, builder->index + builder->range.x);
}

int pg_buffer_builder_push_element(struct pg_buffer_builder* builder, void* data)
{
    const int idx = builder->index;
    if(idx >= builder->range.y) {
        pg_log(PG_LOG_ERROR, "Buffer builder went past designated buffer range");
        return -1;
    }
    memcpy(pg_buffer_builder_get_element(builder), data, builder->dst_buffer->element_size);
    ++builder->index;
    return idx;
}

int pg_buffer_builder_push_elements(struct pg_buffer_builder* builder, int n_elements, void* data)
{
    const int first_idx = builder->index;
    const int last_idx = n_elements + builder->index;
    if(last_idx >= builder->range.y) {
        pg_log(PG_LOG_ERROR, "Buffer builder went past designated buffer range");
        return -1;
    }
    memcpy(pg_buffer_builder_get_element(builder), data, builder->dst_buffer->element_size * n_elements);
    builder->index += n_elements;
    return first_idx;
}

ivec2 pg_buffer_builder_get_built_range(struct pg_buffer_builder* builder)
{
    return ivec2(builder->range.x, builder->index);
}

void pg_vertex_buffer_builder_init(struct pg_vertex_buffer_builder* builder,
        struct pg_buffer* verts_buffer, ivec2 verts_range,
        struct pg_buffer* idx_buffer, ivec2 idx_range)
{
    pg_buffer_builder_init(&builder->verts_builder, verts_buffer, verts_range);
    pg_buffer_builder_init(&builder->idx_builder, idx_buffer, idx_range);
}

int pg_vertex_buffer_builder_add_vertex(struct pg_vertex_buffer_builder* builder, void* data)
{
    return pg_buffer_builder_push_element(&builder->verts_builder, data);
}

int pg_vertex_buffer_builder_add_triangle(struct pg_vertex_buffer_builder* builder, uint32_t v0, uint32_t v1, uint32_t v2)
{
    return pg_buffer_builder_push_elements(&builder->idx_builder, 3, PG_POINTERS(uint32_t, v0, v1, v2));
}
