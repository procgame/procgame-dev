#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"


static int gl_errors = 0;

int pg_gl_errors(void)
{
    return gl_errors;
}

void pg_gl_add_error(void)
{
    ++gl_errors;
}

void pg_gl_clear_errors(void)
{
    gl_errors = 0;
}

enum pg_data_type pg_data_type_from_gl(GLenum type)
{
    switch(type) {
        case GL_BYTE:               return PG_BYTE;
        case GL_UNSIGNED_BYTE:      return PG_UBYTE;
        case GL_SHORT:              return PG_SHORT;
        case GL_UNSIGNED_SHORT:     return PG_USHORT;
        case GL_INT:                return PG_INT;
        case GL_INT_VEC2:           return PG_IVEC2;
        case GL_INT_VEC3:           return PG_IVEC3;
        case GL_INT_VEC4:           return PG_IVEC4;
        case GL_UNSIGNED_INT:       return PG_UINT;
        case GL_UNSIGNED_INT_VEC2:  return PG_UVEC2;
        case GL_UNSIGNED_INT_VEC3:  return PG_UVEC3;
        case GL_UNSIGNED_INT_VEC4:  return PG_UVEC4;
        case GL_FLOAT:              return PG_FLOAT;
        case GL_FLOAT_VEC2:         return PG_VEC2;
        case GL_FLOAT_VEC3:         return PG_VEC3;
        case GL_FLOAT_VEC4:         return PG_VEC4;
        case GL_FLOAT_MAT4:         return PG_MAT4;
        default: return PG_NULL;
    }
}

GLenum pg_data_type_to_gl(enum pg_data_type type)
{
    switch(type) {
        case PG_BYTE:       return GL_BYTE;
        case PG_BVEC2:      return GL_BYTE;
        case PG_BVEC3:      return GL_BYTE;
        case PG_BVEC4:      return GL_BYTE;
        case PG_UBYTE:      return GL_UNSIGNED_BYTE;
        case PG_UBVEC2:     return GL_UNSIGNED_BYTE;
        case PG_UBVEC3:     return GL_UNSIGNED_BYTE;
        case PG_UBVEC4:     return GL_UNSIGNED_BYTE;
        case PG_USHORT:     return GL_UNSIGNED_SHORT;
        case PG_USVEC2:     return GL_UNSIGNED_SHORT;
        case PG_USVEC3:     return GL_UNSIGNED_SHORT;
        case PG_USVEC4:     return GL_UNSIGNED_SHORT;
        case PG_SHORT:      return GL_SHORT;
        case PG_SVEC2:      return GL_SHORT;
        case PG_SVEC3:      return GL_SHORT;
        case PG_SVEC4:      return GL_SHORT;
        case PG_INT:        return GL_INT;
        case PG_IVEC2:      return GL_INT;
        case PG_IVEC3:      return GL_INT;
        case PG_IVEC4:      return GL_INT;
        case PG_UINT:       return GL_UNSIGNED_INT;
        case PG_UVEC2:      return GL_UNSIGNED_INT;
        case PG_UVEC3:      return GL_UNSIGNED_INT;
        case PG_UVEC4:      return GL_UNSIGNED_INT;
        case PG_FLOAT:      return GL_FLOAT;
        case PG_VEC2:       return GL_FLOAT;
        case PG_VEC3:       return GL_FLOAT;
        case PG_VEC4:       return GL_FLOAT;
        case PG_MAT4:       return GL_FLOAT;
        case PG_MAT3:       return GL_FLOAT;
        default: return GL_ZERO;
    }
}

const char* pg_gl_enum_string(GLenum gl)
{
    switch(gl) {
        /*  Handle duplicate names...    */
        /*case GL_FALSE: case GL_NO_ERROR: case GL_POINTS:*/
        case GL_ZERO:                                   return "GL_ZERO";
        /*case GL_TRUE: case GL_LINES:*/
        case GL_ONE:                                    return "GL_ONE";
        case GL_DEPTH_BUFFER_BIT:                       return "GL_DEPTH_BUFFER_BIT";
        case GL_STENCIL_BUFFER_BIT:                     return "GL_STENCIL_BUFFER_BIT";
        case GL_COLOR_BUFFER_BIT:                       return "GL_COLOR_BUFFER_BIT";
        case GL_LINE_LOOP:                              return "GL_LINE_LOOP";
        case GL_LINE_STRIP:                             return "GL_LINE_STRIP";
        case GL_TRIANGLES:                              return "GL_TRIANGLES";
        case GL_TRIANGLE_STRIP:                         return "GL_TRIANGLE_STRIP";
        case GL_TRIANGLE_FAN:                           return "GL_TRIANGLE_FAN";
        case GL_NEVER:                                  return "GL_NEVER";
        case GL_LESS:                                   return "GL_LESS";
        case GL_EQUAL:                                  return "GL_EQUAL";
        case GL_LEQUAL:                                 return "GL_LEQUAL";
        case GL_GREATER:                                return "GL_GREATER";
        case GL_NOTEQUAL:                               return "GL_NOTEQUAL";
        case GL_GEQUAL:                                 return "GL_GEQUAL";
        case GL_ALWAYS:                                 return "GL_ALWAYS";
        case GL_SRC_COLOR:                              return "GL_SRC_COLOR";
        case GL_ONE_MINUS_SRC_COLOR:                    return "GL_ONE_MINUS_SRC_COLOR";
        case GL_SRC_ALPHA:                              return "GL_SRC_ALPHA";
        case GL_ONE_MINUS_SRC_ALPHA:                    return "GL_ONE_MINUS_SRC_ALPHA";
        case GL_DST_ALPHA:                              return "GL_DST_ALPHA";
        case GL_ONE_MINUS_DST_ALPHA:                    return "GL_ONE_MINUS_DST_ALPHA";
        case GL_DST_COLOR:                              return "GL_DST_COLOR";
        case GL_ONE_MINUS_DST_COLOR:                    return "GL_ONE_MINUS_DST_COLOR";
        case GL_SRC_ALPHA_SATURATE:                     return "GL_SRC_ALPHA_SATURATE";
        case GL_FRONT:                                  return "GL_FRONT";
        case GL_BACK:                                   return "GL_BACK";
        case GL_FRONT_AND_BACK:                         return "GL_FRONT_AND_BACK";
        case GL_FOG:                                    return "GL_FOG";
        case GL_LIGHTING:                               return "GL_LIGHTING";
        case GL_TEXTURE_2D:                             return "GL_TEXTURE_2D";
        case GL_CULL_FACE:                              return "GL_CULL_FACE";
        case GL_ALPHA_TEST:                             return "GL_ALPHA_TEST";
        case GL_BLEND:                                  return "GL_BLEND";
        case GL_COLOR_LOGIC_OP:                         return "GL_COLOR_LOGIC_OP";
        case GL_DITHER:                                 return "GL_DITHER";
        case GL_STENCIL_TEST:                           return "GL_STENCIL_TEST";
        case GL_DEPTH_TEST:                             return "GL_DEPTH_TEST";
        case GL_POINT_SMOOTH:                           return "GL_POINT_SMOOTH";
        case GL_LINE_SMOOTH:                            return "GL_LINE_SMOOTH";
        case GL_SCISSOR_TEST:                           return "GL_SCISSOR_TEST";
        case GL_COLOR_MATERIAL:                         return "GL_COLOR_MATERIAL";
        case GL_NORMALIZE:                              return "GL_NORMALIZE";
        case GL_RESCALE_NORMAL:                         return "GL_RESCALE_NORMAL";
        case GL_POLYGON_OFFSET_FILL:                    return "GL_POLYGON_OFFSET_FILL";
        case GL_VERTEX_ARRAY:                           return "GL_VERTEX_ARRAY";
        case GL_NORMAL_ARRAY:                           return "GL_NORMAL_ARRAY";
        case GL_COLOR_ARRAY:                            return "GL_COLOR_ARRAY";
        case GL_TEXTURE_COORD_ARRAY:                    return "GL_TEXTURE_COORD_ARRAY";
        case GL_MULTISAMPLE:                            return "GL_MULTISAMPLE";
        case GL_SAMPLE_ALPHA_TO_COVERAGE:               return "GL_SAMPLE_ALPHA_TO_COVERAGE";
        case GL_SAMPLE_ALPHA_TO_ONE:                    return "GL_SAMPLE_ALPHA_TO_ONE";
        case GL_SAMPLE_COVERAGE:                        return "GL_SAMPLE_COVERAGE";
        case GL_INVALID_ENUM:                           return "GL_INVALID_ENUM";
        case GL_INVALID_VALUE:                          return "GL_INVALID_VALUE";
        case GL_INVALID_OPERATION:                      return "GL_INVALID_OPERATION";
        case GL_STACK_OVERFLOW:                         return "GL_STACK_OVERFLOW";
        case GL_STACK_UNDERFLOW:                        return "GL_STACK_UNDERFLOW";
        case GL_OUT_OF_MEMORY:                          return "GL_OUT_OF_MEMORY";
        case GL_EXP:                                    return "GL_EXP";
        case GL_EXP2:                                   return "GL_EXP2";
        case GL_FOG_DENSITY:                            return "GL_FOG_DENSITY";
        case GL_FOG_START:                              return "GL_FOG_START";
        case GL_FOG_END:                                return "GL_FOG_END";
        case GL_FOG_MODE:                               return "GL_FOG_MODE";
        case GL_FOG_COLOR:                              return "GL_FOG_COLOR";
        case GL_CW:                                     return "GL_CW";
        case GL_CCW:                                    return "GL_CCW";
        case GL_SMOOTH_POINT_SIZE_RANGE:                return "GL_SMOOTH_POINT_SIZE_RANGE";
        case GL_SMOOTH_LINE_WIDTH_RANGE:                return "GL_SMOOTH_LINE_WIDTH_RANGE";
        case GL_ALIASED_POINT_SIZE_RANGE:               return "GL_ALIASED_POINT_SIZE_RANGE";
        case GL_ALIASED_LINE_WIDTH_RANGE:               return "GL_ALIASED_LINE_WIDTH_RANGE";
        case GL_MAX_LIGHTS:                             return "GL_MAX_LIGHTS";
        case GL_MAX_TEXTURE_SIZE:                       return "GL_MAX_TEXTURE_SIZE";
        case GL_MAX_MODELVIEW_STACK_DEPTH:              return "GL_MAX_MODELVIEW_STACK_DEPTH";
        case GL_MAX_PROJECTION_STACK_DEPTH:             return "GL_MAX_PROJECTION_STACK_DEPTH";
        case GL_MAX_TEXTURE_STACK_DEPTH:                return "GL_MAX_TEXTURE_STACK_DEPTH";
        case GL_MAX_VIEWPORT_DIMS:                      return "GL_MAX_VIEWPORT_DIMS";
        case GL_MAX_ELEMENTS_VERTICES:                  return "GL_MAX_ELEMENTS_VERTICES";
        case GL_MAX_ELEMENTS_INDICES:                   return "GL_MAX_ELEMENTS_INDICES";
        case GL_MAX_TEXTURE_UNITS:                      return "GL_MAX_TEXTURE_UNITS";
        case GL_NUM_COMPRESSED_TEXTURE_FORMATS:         return "GL_NUM_COMPRESSED_TEXTURE_FORMATS";
        case GL_COMPRESSED_TEXTURE_FORMATS:             return "GL_COMPRESSED_TEXTURE_FORMATS";
        case GL_SUBPIXEL_BITS:                          return "GL_SUBPIXEL_BITS";
        case GL_RED_BITS:                               return "GL_RED_BITS";
        case GL_GREEN_BITS:                             return "GL_GREEN_BITS";
        case GL_BLUE_BITS:                              return "GL_BLUE_BITS";
        case GL_ALPHA_BITS:                             return "GL_ALPHA_BITS";
        case GL_DEPTH_BITS:                             return "GL_DEPTH_BITS";
        case GL_STENCIL_BITS:                           return "GL_STENCIL_BITS";
        case GL_DONT_CARE:                              return "GL_DONT_CARE";
        case GL_FASTEST:                                return "GL_FASTEST";
        case GL_NICEST:                                 return "GL_NICEST";
        case GL_PERSPECTIVE_CORRECTION_HINT:            return "GL_PERSPECTIVE_CORRECTION_HINT";
        case GL_POINT_SMOOTH_HINT:                      return "GL_POINT_SMOOTH_HINT";
        case GL_LINE_SMOOTH_HINT:                       return "GL_LINE_SMOOTH_HINT";
        case GL_POLYGON_SMOOTH_HINT:                    return "GL_POLYGON_SMOOTH_HINT";
        case GL_FOG_HINT:                               return "GL_FOG_HINT";
        case GL_LIGHT_MODEL_AMBIENT:                    return "GL_LIGHT_MODEL_AMBIENT";
        case GL_LIGHT_MODEL_TWO_SIDE:                   return "GL_LIGHT_MODEL_TWO_SIDE";
        case GL_AMBIENT:                                return "GL_AMBIENT";
        case GL_DIFFUSE:                                return "GL_DIFFUSE";
        case GL_SPECULAR:                               return "GL_SPECULAR";
        case GL_POSITION:                               return "GL_POSITION";
        case GL_SPOT_DIRECTION:                         return "GL_SPOT_DIRECTION";
        case GL_SPOT_EXPONENT:                          return "GL_SPOT_EXPONENT";
        case GL_SPOT_CUTOFF:                            return "GL_SPOT_CUTOFF";
        case GL_CONSTANT_ATTENUATION:                   return "GL_CONSTANT_ATTENUATION";
        case GL_LINEAR_ATTENUATION:                     return "GL_LINEAR_ATTENUATION";
        case GL_QUADRATIC_ATTENUATION:                  return "GL_QUADRATIC_ATTENUATION";
        case GL_BYTE:                                   return "GL_BYTE";
        case GL_UNSIGNED_BYTE:                          return "GL_UNSIGNED_BYTE";
        case GL_SHORT:                                  return "GL_SHORT";
        case GL_UNSIGNED_SHORT:                         return "GL_UNSIGNED_SHORT";
        case GL_FLOAT:                                  return "GL_FLOAT";
        case GL_FIXED:                                  return "GL_FIXED";
        case GL_CLEAR:                                  return "GL_CLEAR";
        case GL_AND:                                    return "GL_AND";
        case GL_AND_REVERSE:                            return "GL_AND_REVERSE";
        case GL_COPY:                                   return "GL_COPY";
        case GL_AND_INVERTED:                           return "GL_AND_INVERTED";
        case GL_NOOP:                                   return "GL_NOOP";
        case GL_XOR:                                    return "GL_XOR";
        case GL_OR:                                     return "GL_OR";
        case GL_NOR:                                    return "GL_NOR";
        case GL_EQUIV:                                  return "GL_EQUIV";
        case GL_INVERT:                                 return "GL_INVERT";
        case GL_OR_REVERSE:                             return "GL_OR_REVERSE";
        case GL_COPY_INVERTED:                          return "GL_COPY_INVERTED";
        case GL_OR_INVERTED:                            return "GL_OR_INVERTED";
        case GL_NAND:                                   return "GL_NAND";
        case GL_SET:                                    return "GL_SET";
        case GL_EMISSION:                               return "GL_EMISSION";
        case GL_SHININESS:                              return "GL_SHININESS";
        case GL_AMBIENT_AND_DIFFUSE:                    return "GL_AMBIENT_AND_DIFFUSE";
        case GL_MODELVIEW:                              return "GL_MODELVIEW";
        case GL_PROJECTION:                             return "GL_PROJECTION";
        case GL_TEXTURE:                                return "GL_TEXTURE";
        case GL_ALPHA:                                  return "GL_ALPHA";
        case GL_RGB:                                    return "GL_RGB";
        case GL_RGBA:                                   return "GL_RGBA";
        case GL_LUMINANCE:                              return "GL_LUMINANCE";
        case GL_LUMINANCE_ALPHA:                        return "GL_LUMINANCE_ALPHA";
        case GL_UNPACK_ALIGNMENT:                       return "GL_UNPACK_ALIGNMENT";
        case GL_PACK_ALIGNMENT:                         return "GL_PACK_ALIGNMENT";
        case GL_UNSIGNED_SHORT_4_4_4_4:                 return "GL_UNSIGNED_SHORT_4_4_4_4";
        case GL_UNSIGNED_SHORT_5_5_5_1:                 return "GL_UNSIGNED_SHORT_5_5_5_1";
        case GL_UNSIGNED_SHORT_5_6_5:                   return "GL_UNSIGNED_SHORT_5_6_5";
        case GL_FLAT:                                   return "GL_FLAT";
        case GL_SMOOTH:                                 return "GL_SMOOTH";
        case GL_KEEP:                                   return "GL_KEEP";
        case GL_REPLACE:                                return "GL_REPLACE";
        case GL_INCR:                                   return "GL_INCR";
        case GL_DECR:                                   return "GL_DECR";
        case GL_VENDOR:                                 return "GL_VENDOR";
        case GL_RENDERER:                               return "GL_RENDERER";
        case GL_VERSION:                                return "GL_VERSION";
        case GL_EXTENSIONS:                             return "GL_EXTENSIONS";
        case GL_MODULATE:                               return "GL_MODULATE";
        case GL_DECAL:                                  return "GL_DECAL";
        case GL_ADD:                                    return "GL_ADD";
        case GL_TEXTURE_ENV_MODE:                       return "GL_TEXTURE_ENV_MODE";
        case GL_TEXTURE_ENV_COLOR:                      return "GL_TEXTURE_ENV_COLOR";
        case GL_TEXTURE_ENV:                            return "GL_TEXTURE_ENV";
        case GL_NEAREST:                                return "GL_NEAREST";
        case GL_LINEAR:                                 return "GL_LINEAR";
        case GL_NEAREST_MIPMAP_NEAREST:                 return "GL_NEAREST_MIPMAP_NEAREST";
        case GL_LINEAR_MIPMAP_NEAREST:                  return "GL_LINEAR_MIPMAP_NEAREST";
        case GL_NEAREST_MIPMAP_LINEAR:                  return "GL_NEAREST_MIPMAP_LINEAR";
        case GL_LINEAR_MIPMAP_LINEAR:                   return "GL_LINEAR_MIPMAP_LINEAR";
        case GL_TEXTURE_MAG_FILTER:                     return "GL_TEXTURE_MAG_FILTER";
        case GL_TEXTURE_MIN_FILTER:                     return "GL_TEXTURE_MIN_FILTER";
        case GL_TEXTURE_WRAP_S:                         return "GL_TEXTURE_WRAP_S";
        case GL_TEXTURE_WRAP_T:                         return "GL_TEXTURE_WRAP_T";
        case GL_TEXTURE0:                               return "GL_TEXTURE0";
        case GL_TEXTURE1:                               return "GL_TEXTURE1";
        case GL_TEXTURE2:                               return "GL_TEXTURE2";
        case GL_TEXTURE3:                               return "GL_TEXTURE3";
        case GL_TEXTURE4:                               return "GL_TEXTURE4";
        case GL_TEXTURE5:                               return "GL_TEXTURE5";
        case GL_TEXTURE6:                               return "GL_TEXTURE6";
        case GL_TEXTURE7:                               return "GL_TEXTURE7";
        case GL_TEXTURE8:                               return "GL_TEXTURE8";
        case GL_TEXTURE9:                               return "GL_TEXTURE9";
        case GL_TEXTURE10:                              return "GL_TEXTURE10";
        case GL_TEXTURE11:                              return "GL_TEXTURE11";
        case GL_TEXTURE12:                              return "GL_TEXTURE12";
        case GL_TEXTURE13:                              return "GL_TEXTURE13";
        case GL_TEXTURE14:                              return "GL_TEXTURE14";
        case GL_TEXTURE15:                              return "GL_TEXTURE15";
        case GL_TEXTURE16:                              return "GL_TEXTURE16";
        case GL_TEXTURE17:                              return "GL_TEXTURE17";
        case GL_TEXTURE18:                              return "GL_TEXTURE18";
        case GL_TEXTURE19:                              return "GL_TEXTURE19";
        case GL_TEXTURE20:                              return "GL_TEXTURE20";
        case GL_TEXTURE21:                              return "GL_TEXTURE21";
        case GL_TEXTURE22:                              return "GL_TEXTURE22";
        case GL_TEXTURE23:                              return "GL_TEXTURE23";
        case GL_TEXTURE24:                              return "GL_TEXTURE24";
        case GL_TEXTURE25:                              return "GL_TEXTURE25";
        case GL_TEXTURE26:                              return "GL_TEXTURE26";
        case GL_TEXTURE27:                              return "GL_TEXTURE27";
        case GL_TEXTURE28:                              return "GL_TEXTURE28";
        case GL_TEXTURE29:                              return "GL_TEXTURE29";
        case GL_TEXTURE30:                              return "GL_TEXTURE30";
        case GL_TEXTURE31:                              return "GL_TEXTURE31";
        case GL_REPEAT:                                 return "GL_REPEAT";
        case GL_CLAMP_TO_EDGE:                          return "GL_CLAMP_TO_EDGE";
    }
    return "unknown";
}
