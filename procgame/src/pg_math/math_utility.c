#include <stdio.h>
#include <stdbool.h>
#include "pg_math/linmath.h"
#include "pg_math/math_utility.h"

enum pg_direction pg_get_direction(vec3 v)
{
    if(vec3_is_zero(v)) return PG_NO_DIRECTION;
    vec3 v_abs = vec3_abs(v);
    int max_axis = (v_abs.x > v_abs.y ?
                    (v_abs.x > v_abs.z ? 0 : 2) :
                    (v_abs.y > v_abs.z ? 1 : 2));
    switch(max_axis) {
    case 0: return v.x < 0 ? PG_X_NEG : PG_X_POS; // left : right
    case 1: return v.y < 0 ? PG_Y_NEG : PG_Y_POS; // front : back
    case 2: return v.z < 0 ? PG_Z_NEG : PG_Z_POS; // down : up
    default: return PG_NO_DIRECTION;
    }
}

/*  Closest point calculations  */
vec3 closestpt_point_triangle(vec3 p, const triangle* tri)
{
    // Check if P in vertex region outside A
    vec3 ab = vec3_sub(tri->t[1], tri->t[0]);
    vec3 ac = vec3_sub(tri->t[2], tri->t[0]);
    vec3 ap = vec3_sub(p, tri->t[0]);
    float d1 = vec3_dot(ab, ap);
    float d2 = vec3_dot(ac, ap);
    if (d1 <= 0.0f && d2 <= 0.0f) return tri->t[0];
    // Check if P in vertex region outside B
    vec3 bp = vec3_sub(p, tri->t[1]);
    float d3 = vec3_dot(ab, bp);
    float d4 = vec3_dot(ac, bp);
    if (d3 >= 0.0f && d4 <= d3) return tri->t[1];
    // Check if P in edge region of AB, if so return projection of P onto AB
    float vc = d1*d4 - d3*d2;
    if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
        float v = d1 / (d1 - d3);
        return vec3_add(tri->t[0], vec3_scale(ab, v));
    }
    // Check if P in vertex region outside C
    vec3 cp = vec3_sub(p, tri->t[2]);
    float d5 = vec3_dot(ab, cp);
    float d6 = vec3_dot(ac, cp);
    if (d6 >= 0.0f && d5 <= d6) return tri->t[2];
    // Check if P in edge region of AC, if so return projection of P onto AC
    float vb = d5*d2 - d1*d6;
    if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
        float w=d2/(d2- d6);
        return vec3_add(tri->t[0], vec3_scale(ac, w)); // barycentric coordinates (1-w,0,w)
    }
    // Check if P in edge region of BC, if so return projection of P onto BC
    float va = d3*d6 - d5*d4;
    if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
        float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
        return vec3_add(tri->t[1], vec3_scale(vec3_sub(tri->t[2], tri->t[1]), w));
    }
    // P inside face region. Compute Q through its barycentric coordinates (u,v,w)
    float denom = 1.0f / (va + vb + vc);
    float v= vb * denom;
    float w= vc * denom;
    return vec3_add(vec3_add(vec3_scale(ab, v), vec3_scale(ac, w)), tri->t[0]);
}

vec3 closestpt_point_sphere(vec3 p, const sphere* sph)
{
    return vec3_add(sph->p, vec3_tolen(vec3_sub(p, sph->p), sph->r));
}

vec3 closestpt_point_aabb3D(vec3 p, const aabb3D* box)
{
    return vec3_min(vec3_max(p, box->b0), box->b1);
}

vec3 closestpt_point_obb3D(vec3 p, const obb3D* obb)
{
    vec3 p_tx = p;
    p_tx = quat_mul_vec3(quat_conj(obb->rot), p);
    //aabb3D box = AABB3D(vec3_scale(obb->dims, -1), obb->dims);
    p_tx = closestpt_point_aabb3D(p_tx, &obb->box);
    p_tx = quat_mul_vec3(obb->rot, p_tx);
    return p_tx;
}

/*  Raycasts (return length of ray, or -1 if the ray misses)    */

float raycast_sphere(const ray3D* ray, const sphere* sph)
{
    vec3 m = vec3_sub(ray->src, sph->p);
    float b = vec3_dot(m, ray->dir);
    float c = vec3_dot(m, m) - (sph->r * sph->r);
    if(c > 0.0f && b > 0.0f) return -1.0f;
    float disc = b * b - c;
    if(disc < 0.0f) return -1.0f;
    float t = -b - sqrtf(disc);
    if(t < 0.0f) return 0.0f;
    return t;
}

float raycast_sphere_inverted(const ray3D* ray, const sphere* sph)
{
    vec3 m = vec3_sub(ray->src, sph->p);
    float b = vec3_dot(m, ray->dir);
    float c = vec3_dot(m, m) - (sph->r * sph->r);
    if(c > 0.0f && b > 0.0f) return 0.0f;
    float disc = b * b - c;
    if(disc < 0.0f) return 0.0f;
    float t = -b - sqrtf(disc);
    if(t > 0.0f) return 0.0f;
    return -t;
}

float raycast_plane(const ray3D* ray, const plane* pln)
{
    vec3 rp = vec3_sub(ray->src, pln->p);
    float rn = vec3_dot(ray->dir, pln->n);
    float rpn = vec3_dot(rp, pln->n);
    if(rn == 0.0f) {
        if(rpn == 0.0f) return 0.0f;    /*  Line embedded in plane  */
        else return -1.0f;              /*  No intersection */
    } else {
        float d = rpn / rn;
        if(d < 0.0f) return -1.0f;      /*  Ray points away from plane  */
        else return d;                  /*  Return ray distance */
    }
}

float raycast_aabb2D(const ray2D* ray, const aabb2D* box)
{
   float t[8];
   t[1] = (box->b0.x - ray->src.x)/ray->dir.x;
   t[2] = (box->b1.x - ray->src.x)/ray->dir.x;
   t[3] = (box->b0.y - ray->src.y)/ray->dir.y;
   t[4] = (box->b1.y - ray->src.y)/ray->dir.y;
   t[5] = LM_MAX(LM_MIN(t[1], t[2]), LM_MIN(t[3], t[4]));
   t[6] = LM_MIN(LM_MAX(t[1], t[2]), LM_MAX(t[3], t[4]));
   t[7] = (t[6] < 0 || t[5] > t[6]) ? -1 : t[5];
   return t[7];
}

float raycast_aabb3D(const ray3D* ray, const aabb3D* box)
{
   float t[10];
   t[1] = (box->b0.x - ray->src.x)/ray->dir.x;
   t[2] = (box->b1.x - ray->src.x)/ray->dir.x;
   t[3] = (box->b0.y - ray->src.y)/ray->dir.y;
   t[4] = (box->b1.y - ray->src.y)/ray->dir.y;
   t[5] = (box->b0.z - ray->src.z)/ray->dir.z;
   t[6] = (box->b1.z - ray->src.z)/ray->dir.z;
   t[7] = LM_MAX(LM_MAX(LM_MIN(t[1], t[2]), LM_MIN(t[3], t[4])), LM_MIN(t[5], t[6]));
   t[8] = LM_MIN(LM_MIN(LM_MAX(t[1], t[2]), LM_MAX(t[3], t[4])), LM_MAX(t[5], t[6]));
   t[9] = (t[8] < 0 || t[7] > t[8]) ? -1 : t[7];
   return t[9];
}

float raycast_obb3D(const ray3D* ray, const obb3D* obb)
{
    vec3 dir = ray->dir;
    vec3 src = ray->src;
    dir = quat_mul_vec3(quat_conj(obb->rot), dir);
    src = quat_mul_vec3(quat_conj(obb->rot), src);
    //aabb3D box = AABB3D(vec3_scale(obb->dims, -1), obb->dims);
    return raycast_aabb3D(&RAY3D(src, dir), &obb->box);
}

float raycast_triangle(const ray3D* ray, const triangle* tri)
{
    vec3 ab = vec3_sub(tri->t[1], tri->t[0]);
    vec3 ac = vec3_sub(tri->t[2], tri->t[0]);
    /*  Compute triangle normal */
    vec3 n = vec3_cross(ab, ac);
    /*  No hit if ray is parallel or points away from triangle  */
    float d = -vec3_dot(ray->dir, n);
    if (d <= 0.0f) return -1;
    /*  Compute ray length against triangle's plane */
    vec3 ap = vec3_sub(ray->src, tri->t[0]);
    float t = vec3_dot(ap, n);
    if (t < 0.0f) return -1;
    /*  Compute barycentric coordinates and test if within triangle */
    vec3 e = vec3_cross(ray->dir, ap);
    float v = -vec3_dot(ac, e);
    if (v < 0.0f || v > d) return -1;
    float w = vec3_dot(ab, e);
    if (w < 0.0f || v + w > d) return -1;
    /*  Return ray length   */
    return t * (1.0f / d);
}

vec3 sep_axis_sphere_triangle(const sphere* sph, const triangle* tri)
{
    vec3 sep = closestpt_point_triangle(sph->p, tri);
    sep = vec3_sub(sph->p, sep);
    float dist = vec3_dot(sep, sep);
    if(dist < (sph->r * sph->r)) {
        dist = sqrt(dist);
        return vec3_tolen(sep, sph->r - dist);
    } else {
        return vec3(0);
    }
}

vec3 sep_axis_sphere_sphere(const sphere* sph0, const sphere* sph1)
{
    vec3 sep = vec3_sub(sph0->p, sph1->p);
    float combined_radius = sph0->r+ sph1->r;
    float combined_radius_sq = combined_radius * combined_radius;
    float dist2 = vec3_len2(sep);
    if(dist2 < FLT_EPSILON || dist2 > combined_radius_sq) return vec3(0);
    return vec3_tolen(sep, combined_radius - sqrt(dist2));
}

vec3 sep_axis_sphere_aabb3D(const sphere* sph, const aabb3D* box)
{
    vec3 closest_point = closestpt_point_aabb3D(sph->p, box);
    float dist2 = vec3_dist2(closest_point, sph->p);
    if(dist2 < FLT_EPSILON || dist2 > (sph->r * sph->r)) return vec3(0);
    vec3 sep = vec3_sub(sph->p, closest_point);
    return vec3_tolen(sep, sph->r - sqrt(dist2));
}

vec3 sep_axis_sphere_obb3D(const sphere* sph, const obb3D* box)
{
    vec3 closest_point = closestpt_point_obb3D(sph->p, box);
    float dist2 = vec3_dist2(closest_point, sph->p);
    if(dist2 < FLT_EPSILON || dist2 > (sph->r * sph->r)) return vec3(0);
    vec3 sep = vec3_sub(sph->p, closest_point);
    return vec3_tolen(sep, sph->r - sqrt(dist2));
}

void aabb3D_from_sphere(aabb3D* out, const sphere* sph)
{
    vec3 r_vec = vec3_all(sph->r);
    *out = AABB3D(vec3_sub(sph->p, r_vec), vec3_add(sph->p, r_vec));
}

void aabb3D_from_obb3D(aabb3D* out, const obb3D* in)
{
    vec3 points[8] = {
        quat_mul_vec3(in->rot, vec3(in->box.b0.x, in->box.b0.y, in->box.b0.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b0.x, in->box.b0.y, in->box.b1.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b0.x, in->box.b1.y, in->box.b0.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b0.x, in->box.b1.y, in->box.b1.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b1.x, in->box.b0.y, in->box.b0.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b1.x, in->box.b0.y, in->box.b1.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b1.x, in->box.b1.y, in->box.b0.z)),
        quat_mul_vec3(in->rot, vec3(in->box.b1.x, in->box.b1.y, in->box.b1.z)) };
    vec3 min_p = points[0], max_p = points[0];
    out->b0 = points[0];
    out->b1 = points[0];
    for(int i = 1; i < 8; ++i) {
        out->b0 = vec3_min(min_p, points[i]);
        out->b1 = vec3_max(max_p, points[i]);
    }
}

vec3 aabb3D_center(aabb3D* box)
{
    return vec3_add(box->b0, vec3_scale(vec3_sub(box->b1, box->b0), 0.5));
}

void plane_from_triangle(plane* out, const triangle* tri)
{
    out->n = vec3_cross(vec3_sub(tri->t[1], tri->t[0]), vec3_sub(tri->t[2], tri->t[0]));
    out->p = tri->t[0];
}

int start_raycast_grid3D(vec3 start, vec3 end, aabb3D bound, vec3 cell_size,
                         struct raycast_grid3D_state* out)
{
    vec3 grid_div = vec3_div(vec3_sub(bound.b1, bound.b0), cell_size);
    ivec3 grid_dims = ivec3(VEC_XYZ(grid_div));
    /*  If the ray starts or ends outside the grid, then cast against its
        bounding box from either direction to form a segment that lies
        entirely within the grid    */
    if(start.x < bound.b0.x || start.y < bound.b0.y || start.z < bound.b0.z
    || start.x >= bound.b1.x || start.y >= bound.b1.y || start.z >= bound.b1.z) {
        vec3 dir = vec3_norm(vec3_sub(end, start));
        ray3D ray = RAY3D(start, dir);
        float ray_to_bound = raycast_aabb3D(&ray, &bound);
        if(ray_to_bound < 0) return 0;
        start = vec3_add(start, vec3_scale(dir, ray_to_bound + 0.0001));
    }
    if(end.x < bound.b0.x || end.y < bound.b0.y || end.z < bound.b0.z
    || end.x >= bound.b1.x || end.y >= bound.b1.y || end.z >= bound.b1.z) {
        vec3 dir = vec3_norm(vec3_sub(start, end));
        ray3D ray = RAY3D(end, dir);
        float ray_to_bound = raycast_aabb3D(&ray, &bound);
        if(ray_to_bound < 0) return 0;
        end = vec3_add(end, vec3_scale(dir, ray_to_bound + 0.0001));
    }
    /*  Transform segment into the local space of the grid  */
    start = vec3_sub(start, bound.b0);
    end = vec3_sub(end, bound.b0);
    /*  Calculate starting cell */
    out->start_cell = ivec3( (int)(floor(start.x / cell_size.x)),
                             (int)(floor(start.y / cell_size.y)),
                             (int)(floor(start.z / cell_size.z)) );
    out->start_cell = ivec3_clamp(out->start_cell, ivec3_all(0), ivec3_sub(grid_dims, ivec3_all(1)));
    out->iter = out->start_cell;
    /*  Calculate ending cell   */
    out->end_cell = ivec3( (int)(floor(end.x / cell_size.x)),
                           (int)(floor(end.y / cell_size.y)),
                           (int)(floor(end.z / cell_size.z)) );
    out->end_cell = ivec3_clamp(out->end_cell, ivec3_all(0), ivec3_sub(grid_dims, ivec3_all(1)));
    /*  Calculate step directions   */
    out->step = ivec3( ((start.x < end.x) ? 1 : ((start.x > end.x) ? -1 : 0)),
                       ((start.y < end.y) ? 1 : ((start.y > end.y) ? -1 : 0)),
                       ((start.z < end.z) ? 1 : ((start.z > end.z) ? -1 : 0)) );
    /*  Calculate distances to cross grid cells for each axis   */
    float minx = cell_size.x * floor(start.x/cell_size.x), maxx = minx + cell_size.x;
    float miny = cell_size.y * floor(start.y/cell_size.y), maxy = miny + cell_size.y;
    float minz = cell_size.z * floor(start.z/cell_size.z), maxz = minz + cell_size.z;
    out->dist = vec3(
        (((start.x > end.x) ? (start.x - minx) : (maxx - start.x)) / fabs(end.x - start.x)),
        (((start.y > end.y) ? (start.y - miny) : (maxy - start.y)) / fabs(end.y - start.y)),
        (((start.z > end.z) ? (start.z - minz) : (maxz - start.z)) / fabs(end.z - start.z)) );
    /*  Calculate full grid cell dimensions in terms of each axis   */
    out->delta = vec3( cell_size.x / fabs(end.x - start.x),
                       cell_size.y / fabs(end.y - start.y),
                       cell_size.z / fabs(end.z - start.z) );
    out->ray_end = vec3(0,0,0);
    return 1;
}

int step_raycast_grid3D(struct raycast_grid3D_state* rc)
{
    if(rc->dist.x <= rc->dist.y && rc->dist.x <= rc->dist.z) {
        if(rc->end_cell.x == rc->iter.x) return 0;
        rc->dist.x += rc->delta.x;
        rc->iter.x += rc->step.x;
        rc->ray_end.x += rc->delta.x;
    } else if(rc->dist.y <= rc->dist.x && rc->dist.y <= rc->dist.z) {
        if(rc->end_cell.y == rc->iter.y) return 0;
        rc->dist.y += rc->delta.y;
        rc->iter.y += rc->step.y;
        rc->ray_end.y += rc->delta.y;
    } else {
        if(rc->end_cell.z == rc->iter.z) return 0;
        rc->dist.z += rc->delta.z;
        rc->iter.z += rc->step.z;
        rc->ray_end.z += rc->delta.z;
    }
    return 1;
}

int start_raycast_grid2D(vec2 start, vec2 end, aabb2D bound, vec2 cell_size,
                         struct raycast_grid2D_state* out)
{
    vec2 grid_div = vec2_div(vec2_sub(bound.b1, bound.b0), cell_size);
    ivec2 grid_dims = ivec2(VEC_XY(grid_div));
    /*  If the ray starts or ends outside the grid, then cast against its
        bounding box from either direction to form a segment that lies
        entirely within the grid    */
    if(start.x < bound.b0.x || start.y < bound.b0.y
    || start.x >= bound.b1.x || start.y >= bound.b1.y) {
        vec2 dir = vec2_norm(vec2_sub(end, start));
        ray2D ray = RAY2D(start, dir);
        float ray_to_bound = raycast_aabb2D(&ray, &bound);
        if(ray_to_bound < 0) return 0;
        start = vec2_add(start, vec2_scale(dir, ray_to_bound + 0.0001));
    }
    if(end.x < bound.b0.x || end.y < bound.b0.y
    || end.x >= bound.b1.x || end.y >= bound.b1.y) {
        vec2 dir = vec2_norm(vec2_sub(start, end));
        ray2D ray = RAY2D(end, dir);
        float ray_to_bound = raycast_aabb2D(&ray, &bound);
        if(ray_to_bound < 0) return 0;
        end = vec2_add(end, vec2_scale(dir, ray_to_bound + 0.0001));
    }
    /*  Transform segment into the local space of the grid  */
    start = vec2_sub(start, bound.b0);
    end = vec2_sub(end, bound.b0);
    /*  Calculate starting cell */
    out->start_cell = ivec2( (int)(floor(start.x / cell_size.x)),
                             (int)(floor(start.y / cell_size.y)) );
    out->start_cell = ivec2_clamp(out->start_cell, ivec2_all(0), ivec2_sub(grid_dims, ivec2_all(1)));
    out->iter = out->start_cell;
    /*  Calculate ending cell   */
    out->end_cell = ivec2( (int)(floor(end.x / cell_size.x)),
                           (int)(floor(end.y / cell_size.y)) );
    out->end_cell = ivec2_clamp(out->end_cell, ivec2_all(0), ivec2_sub(grid_dims, ivec2_all(1)));
    /*  Calculate step directions   */
    out->step = ivec2( ((start.x < end.x) ? 1 : ((start.x > end.x) ? -1 : 0)),
                       ((start.y < end.y) ? 1 : ((start.y > end.y) ? -1 : 0)) );
    /*  Calculate distances to cross grid cells for each axis   */
    float minx = cell_size.x * floor(start.x/cell_size.x), maxx = minx + cell_size.x;
    float miny = cell_size.y * floor(start.y/cell_size.y), maxy = miny + cell_size.y;
    out->dist = vec2(
        (((start.x > end.x) ? (start.x - minx) : (maxx - start.x)) / fabs(end.x - start.x)),
        (((start.y > end.y) ? (start.y - miny) : (maxy - start.y)) / fabs(end.y - start.y)) );
    /*  Calculate full grid cell dimensions in terms of each axis   */
    out->delta = vec2( cell_size.x / fabs(end.x - start.x),
                       cell_size.y / fabs(end.y - start.y) );
    return 1;
}

int step_raycast_grid2D(struct raycast_grid2D_state* rc)
{
    if(rc->dist.x <= rc->dist.y) {
        if(rc->end_cell.x == rc->iter.x) return 0;
        rc->dist.x += rc->delta.x;
        rc->iter.x += rc->step.x;
    } else {
        if(rc->end_cell.y == rc->iter.y) return 0;
        rc->dist.y += rc->delta.y;
        rc->iter.y += rc->step.y;
    }
    return 1;
}
