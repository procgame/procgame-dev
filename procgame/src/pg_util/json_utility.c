#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_math/pg_math.h"

/*  Load a JSON object from a file, free everything but the parsed JSON  */
cJSON* json_from_file(struct pg_file* file)
{
    if(!file->size) return NULL;
    pg_file_read_full(file);
    cJSON* json = cJSON_Parse(file->content);
    if(!json) {
        pg_log(PG_LOG_ERROR, "Invalid JSON in file '%s', near contents:\n%32s",
                file->fullname, cJSON_GetErrorPtr());
        return NULL;
    }
    return json;
}

cJSON* json_from_file_preprocessed(struct pg_file* file, const char** include_dirs, int n_include_dirs)
{
    if(!file->size) return NULL;
    char_arr_t file_contents;
    ARR_INIT(file_contents);
    if(!pg_file_read_preprocessed(file, include_dirs, n_include_dirs, &file_contents)) {
        ARR_DEINIT(file_contents);
        return NULL;
    }
    cJSON* json = cJSON_Parse(file_contents.data);
    if(!json) {
        pg_log(PG_LOG_ERROR, "Invalid JSON in file '%s', near contents:\n%s", file->fullname, cJSON_GetErrorPtr());
    }
    ARR_DEINIT(file_contents);
    return json;
}

/*  Load a JSON object from a file, free everything but the parsed JSON  */
cJSON* load_json(const char* filename)
{
    struct pg_file file;
    if(!pg_file_open(&file, filename, "r")) return NULL;
    cJSON* json = json_from_file(&file);
    pg_file_close(&file);
    return json;
}

cJSON* load_json_preprocessed(const char* filename, const char** include_dirs, int n_include_dirs)
{
    struct pg_file file;
    if(!pg_file_open(&file, filename, "r")) return NULL;
    cJSON* json = json_from_file_preprocessed(&file, include_dirs, n_include_dirs);
    pg_file_close(&file);
    return json;
}

cJSON* json_get_child(cJSON* json, const char* child_string)
{
    struct pg_text_tokenizer child_string_tok =
        PG_TEXT_TOKENIZER(child_string, strlen(child_string), ".[]", 3);
    char key_buffer[256];
    cJSON* child_json = json;
    while(pg_text_tokenizer_next(&child_string_tok)) {
        strncpy(key_buffer, child_string_tok.token, child_string_tok.token_len);
        key_buffer[child_string_tok.token_len] = '\0';
        if(child_string_tok.token_idx == 0 || (child_string_tok.token[-1]) == '.') {
            child_json = cJSON_GetObjectItem(child_json, key_buffer);
            if(!child_json) return NULL;
            continue;
        } else if((child_string_tok.token[-1]) == '[') {
            int index = atoi(key_buffer);
            if(!pg_text_tokenizer_next(&child_string_tok)) return NULL;
            child_json = cJSON_GetArrayItem(child_json, index);
        }
    }
    return child_json;
}

const char* json_type_string(cJSON_Type type)
{
    if(type == cJSON_False || type == cJSON_True) return "bool";
    else if(type == cJSON_Number) return "number";
    else if(type == cJSON_String) return "string";
    else if(type == cJSON_Array) return "array";
    else if(type == cJSON_Object) return "object";
    else if(type == (cJSON_Object | cJSON_String)) return "asset";
    else if(type == (cJSON_String | cJSON_Number)) return "{string|number}";
    else if(type == (cJSON_String | cJSON_Array)) return "{string|array}";
    else if(type == (cJSON_Number | cJSON_Array)) return "{number|array}";
    else if(type == (cJSON_String | cJSON_Number | cJSON_Array)) return "{string|number|array}";
    else if(type == cJSON_NULL || type == cJSON_Invalid) return "null";
    else return "()";
}

void json_read_string(cJSON* json, char* out, int len)
{
    if(json && cJSON_IsString(json)) {
        strncpy(out, json->valuestring, len);
    } else if(len) {
        out[0] = '\0';
    }
}

pg_data_t json_read_pg_data(cJSON* json)
{
    cJSON* type_json, *count_json, *data_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(3,
        PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("count", cJSON_Number, &count_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("data", cJSON_Array | cJSON_String, &data_json),
    );
    if(!pg_load_json_layout(json, &layout)) return (pg_data_t){};
    enum pg_data_type type = pg_data_type_from_string(cJSON_GetStringValue(type_json));
    if(!type) {
        pg_log(PG_LOG_ERROR, "Data JSON has invalid type '%s'", cJSON_GetStringValue(type_json));
        return (pg_data_t){};
    }
    int count = count_json ? count_json->valueint : 1;
    int data_size = 0;
    if(data_json && type == PG_STRING) data_size = strlen(cJSON_GetStringValue(data_json)) + 1;
    else if(data_json) data_size = cJSON_GetArraySize(data_json);
    if(!count_json) count = data_size;
    pg_data_t new_data = pg_data_init(type, count, NULL);
    if(data_json) {
        void* new_data_ptr = pg_data_value_ptr(&new_data, 0);
        if(type == PG_STRING) {
            json_read_string(data_json, (char*)new_data_ptr, count);
        } else {
            int i = 0;
            cJSON* data_elem;
            cJSON_ArrayForEach(data_elem, data_json) {
                switch(type) {
                case PG_BYTE:   ((uint8_t*)new_data_ptr)[i] = json_read_byte(data_elem); break;
                case PG_BVEC2:  ((bvec2*)new_data_ptr)[i] = json_read_bvec2(data_elem); break;
                case PG_BVEC3:  ((bvec3*)new_data_ptr)[i] = json_read_bvec3(data_elem); break;
                case PG_BVEC4:  ((bvec4*)new_data_ptr)[i] = json_read_bvec4(data_elem); break;
                case PG_UBYTE:  ((uint8_t*)new_data_ptr)[i] = json_read_ubyte(data_elem); break;
                case PG_UBVEC2: ((ubvec2*)new_data_ptr)[i] = json_read_ubvec2(data_elem); break;
                case PG_UBVEC3: ((ubvec3*)new_data_ptr)[i] = json_read_ubvec3(data_elem); break;
                case PG_UBVEC4: ((ubvec4*)new_data_ptr)[i] = json_read_ubvec4(data_elem); break;
                case PG_SHORT:  ((int16_t*)new_data_ptr)[i] = json_read_short(data_elem); break;
                case PG_SVEC2:  ((svec2*)new_data_ptr)[i] = json_read_svec2(data_elem); break;
                case PG_SVEC3:  ((svec3*)new_data_ptr)[i] = json_read_svec3(data_elem); break;
                case PG_SVEC4:  ((svec4*)new_data_ptr)[i] = json_read_svec4(data_elem); break;
                case PG_USHORT: ((uint16_t*)new_data_ptr)[i] = json_read_ushort(data_elem); break;
                case PG_USVEC2: ((usvec2*)new_data_ptr)[i] = json_read_usvec2(data_elem); break;
                case PG_USVEC3: ((usvec3*)new_data_ptr)[i] = json_read_usvec3(data_elem); break;
                case PG_USVEC4: ((usvec4*)new_data_ptr)[i] = json_read_usvec4(data_elem); break;
                case PG_INT:    ((int32_t*)new_data_ptr)[i] = json_read_int(data_elem); break;
                case PG_IVEC2:  ((ivec2*)new_data_ptr)[i] = json_read_ivec2(data_elem); break;
                case PG_IVEC3:  ((ivec3*)new_data_ptr)[i] = json_read_ivec3(data_elem); break;
                case PG_IVEC4:  ((ivec4*)new_data_ptr)[i] = json_read_ivec4(data_elem); break;
                case PG_UINT:   ((uint32_t*)new_data_ptr)[i] = json_read_uint(data_elem); break;
                case PG_UVEC2:  ((uvec2*)new_data_ptr)[i] = json_read_uvec2(data_elem); break;
                case PG_UVEC3:  ((uvec3*)new_data_ptr)[i] = json_read_uvec3(data_elem); break;
                case PG_UVEC4:  ((uvec4*)new_data_ptr)[i] = json_read_uvec4(data_elem); break;
                case PG_FLOAT:  ((float*)new_data_ptr)[i] = json_read_float(data_elem); break;
                case PG_VEC2:   ((vec2*)new_data_ptr)[i] = json_read_vec2(data_elem); break;
                case PG_VEC3:   ((vec3*)new_data_ptr)[i] = json_read_vec3(data_elem); break;
                case PG_VEC4:   ((vec4*)new_data_ptr)[i] = json_read_vec4(data_elem); break;
                case PG_MAT4:   ((mat4*)new_data_ptr)[i] = json_read_mat4(data_elem); break;
                case PG_MAT3:   ((mat3*)new_data_ptr)[i] = json_read_mat3(data_elem); break;
                default: break;
                }
                if((++i) >= count) break;
            }
        }
    }
    return new_data;
}

#define JSON_READ_OPERATORS(JSON, ...) \
    if(cJSON_IsObject(JSON) && (JSON)->child) { \
        cJSON* operator_json = (JSON)->child; \
        __VA_ARGS__ \
    }

#define JSON_READ_OPERATOR_0(OPERATOR, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        __VA_ARGS__; \
    }

#define JSON_READ_OPERATOR_1(OPERATOR, OPERAND, OPERAND_READER, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        OPERAND = OPERAND_READER(operator_json); \
        __VA_ARGS__; \
    }
            
        
#define JSON_READ_OPERATOR_2(OPERATOR, LHS, LHS_READER, RHS, RHS_READER, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        if(operator_json->child && operator_json->child->next) { \
            LHS = LHS_READER(operator_json->child); \
            RHS = RHS_READER(operator_json->child->next); \
            __VA_ARGS__; \
        } else { \
            pg_log(PG_LOG_ERROR, "Invalid operands (2 expected) for JSON read operator '%s'", operator_json->string); \
        } \
    }

#define JSON_READ_OPERATOR_CUSTOM(OPERATOR, OPERAND, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        cJSON* OPERAND = operator_json; \
        __VA_ARGS__; \
    }

mat4 json_read_mat4(cJSON* json)
{
    JSON_READ_OPERATORS(json,
        JSON_READ_OPERATOR_0("identity", return mat4_identity());
        JSON_READ_OPERATOR_1("translation", vec3 tx, json_read_vec3, return mat4_translation(tx));
        JSON_READ_OPERATOR_1("scaling", vec3 tx, json_read_vec3, return mat4_scaling(tx));
        JSON_READ_OPERATOR_1("euler", vec3 angles, json_read_vec3,
            return mat4_rotate( vec3_Y(), angles.x,
                    mat4_rotate( vec3_X(), angles.y,
                     mat4_rotate( vec3_Z(), angles.z, mat4_identity() ) ) ) );
        JSON_READ_OPERATOR_2("mat4_mul", mat4 lhs, json_read_mat4, mat4 rhs, json_read_mat4,
            return mat4_mul(lhs, rhs));
        JSON_READ_OPERATOR_CUSTOM("mat4_mul_chain", matrices,
            if(cJSON_IsArray(matrices) && matrices->child) {
                mat4 result = mat4_identity();
                cJSON* next_mat;
                cJSON_ArrayForEach(next_mat, matrices) {
                    result = mat4_mul(result, json_read_mat4(next_mat));
                }
                return result;
            } else {
                return mat4_identity();
            }
        );
        JSON_READ_OPERATOR_CUSTOM("look_at", look_json,
            cJSON* eye_json, *pt_json, *up_json;
            struct pg_json_layout look_layout = PG_JSON_LAYOUT(3,
                PG_JSON_LAYOUT_ITEM("eye", cJSON_Array, &eye_json),
                PG_JSON_LAYOUT_ITEM("pt", cJSON_Array, &pt_json),
                PG_JSON_LAYOUT_ITEM("up", cJSON_Array, &up_json));
            if(!pg_load_json_layout(look_json, &look_layout)) {
                pg_log(PG_LOG_ERROR, "invalid parameters to JSON read operator mat4.lookat (expects 'eye', 'pt', and 'up')");
                return mat4_identity();
            }
            return mat4_look_at(
                json_read_vec3(eye_json),
                json_read_vec3(pt_json),
                json_read_vec3(up_json) );
        );
        JSON_READ_OPERATOR_CUSTOM("perspective", look_json,
            cJSON* fov_json, *aspect_json, *near_json, *far_json;
            struct pg_json_layout look_layout = PG_JSON_LAYOUT(4,
                PG_JSON_LAYOUT_ITEM("y_fov", cJSON_Number, &fov_json),
                PG_JSON_LAYOUT_ITEM("aspect", cJSON_Number, &aspect_json),
                PG_JSON_LAYOUT_ITEM("near", cJSON_Number, &near_json),
                PG_JSON_LAYOUT_ITEM("far", cJSON_Number, &far_json));
            if(!pg_load_json_layout(look_json, &look_layout)) {
                pg_log(PG_LOG_ERROR, "invalid parameters to JSON read operator mat4.perspective (expects 'eye', 'pt', and 'up')");
                return mat4_identity();
            }
            return mat4_perspective(
                fov_json->valuedouble,
                aspect_json->valuedouble,
                near_json->valuedouble,
                far_json->valuedouble);
        );
    );
    vec4 col[4] = {};
    if(json && ((json = json->child))) col[0] = json_read_vec4(json);
    if(json && ((json = json->next))) col[1] = json_read_vec4(json);
    if(json && ((json = json->next))) col[2] = json_read_vec4(json);
    if(json && ((json = json->next))) col[3] = json_read_vec4(json);
    return mat4(col[0], col[1], col[2], col[3]);
}

mat3 json_read_mat3(cJSON* json)
{
    vec3 col[3] = {};
    if(json && ((json = json->child))) col[0] = json_read_vec3(json);
    if(json && ((json = json->next))) col[1] = json_read_vec3(json);
    if(json && ((json = json->next))) col[2] = json_read_vec3(json);
    return mat3(col[0], col[1], col[2]);
}

#define JSON_DEF_READ_DOUBLE(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        if(json && cJSON_IsNumber(json)) return json->valuedouble; \
        if(json && json->child && cJSON_IsNumber(json->child)) return json->child->valuedouble; \
        else return 0; \
    }

#define JSON_DEF_READ_INT(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        if(json && cJSON_IsNumber(json)) return json->valueint; \
        if(json && json->child && cJSON_IsNumber(json->child)) return json->child->valueint; \
        else return 0; \
    }


JSON_DEF_READ_DOUBLE(json_read_float, float)
JSON_DEF_READ_DOUBLE(json_read_double, double)
JSON_DEF_READ_INT(json_read_int, int32_t)
JSON_DEF_READ_INT(json_read_uint, uint32_t)
JSON_DEF_READ_INT(json_read_short, int16_t)
JSON_DEF_READ_INT(json_read_ushort, uint16_t)
JSON_DEF_READ_INT(json_read_byte, int8_t)
JSON_DEF_READ_INT(json_read_ubyte, uint8_t)


/************************/
/*  VECTOR PARSING      */
/************************/

void json_read_aabb3D(cJSON* json, aabb3D* out)
{
    if(json && json->child && json->child->next) {
        *out = AABB3D(
            json_read_vec3(json->child), json_read_vec3(json->child->next) );
    } else {
        *out = AABB3D( vec3(0), vec3(0) );
    }
}

#define JSON_DEF_READ_DOUBLE2(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valuedouble; \
        if(!json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valuedouble; \
        return out; \
    }

#define JSON_DEF_READ_DOUBLE3(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valuedouble; \
        if(!json || !json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.z = json->valuedouble; \
        return out; \
    }

#define JSON_DEF_READ_DOUBLE4(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valuedouble; \
        if(!json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.z = json->valuedouble; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.w = json->valuedouble; \
        return out; \
    }

#define JSON_DEF_READ_INT2(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valueint; \
        if(!json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valueint; \
        return out; \
    }

#define JSON_DEF_READ_INT3(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valueint; \
        if(!json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.z = json->valueint; \
        return out; \
    }

#define JSON_DEF_READ_INT4(FUNC_NAME, RETURN_TYPE) \
    RETURN_TYPE FUNC_NAME(cJSON* json) \
    { \
        RETURN_TYPE out = {0}; \
        if(!json) return out; \
        if(cJSON_IsNumber(json)) out.x = json->valueint; \
        if(!json->child) return out; \
        if(json->child && cJSON_IsNumber(json = json->child)) out.x = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.y = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.z = json->valueint; \
        if(json->next && cJSON_IsNumber(json = json->next)) out.w = json->valueint; \
        return out; \
    }

JSON_DEF_READ_DOUBLE2(json_read_vec2, vec2)
JSON_DEF_READ_DOUBLE3(json_read_vec3, vec3)
JSON_DEF_READ_DOUBLE4(json_read_vec4, vec4)
JSON_DEF_READ_DOUBLE4(json_read_quat, quat)

JSON_DEF_READ_DOUBLE2(json_read_dvec2, dvec2)
JSON_DEF_READ_DOUBLE3(json_read_dvec3, dvec3)
JSON_DEF_READ_DOUBLE4(json_read_dvec4, dvec4)

JSON_DEF_READ_INT2(json_read_ivec2, ivec2)
JSON_DEF_READ_INT3(json_read_ivec3, ivec3)
JSON_DEF_READ_INT4(json_read_ivec4, ivec4)

JSON_DEF_READ_INT2(json_read_uvec2, uvec2)
JSON_DEF_READ_INT3(json_read_uvec3, uvec3)
JSON_DEF_READ_INT4(json_read_uvec4, uvec4)

JSON_DEF_READ_INT2(json_read_svec2, svec2)
JSON_DEF_READ_INT3(json_read_svec3, svec3)
JSON_DEF_READ_INT4(json_read_svec4, svec4)

JSON_DEF_READ_INT2(json_read_usvec2, usvec2)
JSON_DEF_READ_INT3(json_read_usvec3, usvec3)
JSON_DEF_READ_INT4(json_read_usvec4, usvec4)

JSON_DEF_READ_INT2(json_read_bvec2, bvec2)
JSON_DEF_READ_INT3(json_read_bvec3, bvec3)
JSON_DEF_READ_INT4(json_read_bvec4, bvec4)

JSON_DEF_READ_INT2(json_read_ubvec2, ubvec2)
JSON_DEF_READ_INT3(json_read_ubvec3, ubvec3)
JSON_DEF_READ_INT4(json_read_ubvec4, ubvec4)

static void zero_all_json_layout_outputs(struct pg_json_layout* layout)
{
    if(layout->json_object) *layout->json_object = NULL;
    if(layout->n_children && layout->children) {
        for(int i = 0; i < layout->n_children; ++i) {
            zero_all_json_layout_outputs(&layout->children[i]);
        }
    }
}

bool pg_load_json_layout_(cJSON* json, struct pg_json_layout* layout, int src_line, const char* src_file)
{
    if(json && layout->key) json = cJSON_GetObjectItem(json, layout->key);
    if(json && !((layout->expect_type | json->type))) {
        pg_log_(PG_LOG_ERROR, "    JSON layout item \"%s\" has expected type %s, found %s\n", src_line, src_file,
                layout->key ? layout->key : "root",
                json->type, json_type_string(layout->expect_type));
        zero_all_json_layout_outputs(layout);
        return false;
    }
    if(json && layout->n_children && layout->children) {
        bool missing_keys = false;
        for(int i = 0; i < layout->n_children; ++i) {
            if(!pg_load_json_layout(json, &layout->children[i])) missing_keys = true;
        }
        if(missing_keys) return false;
    } else {
        zero_all_json_layout_outputs(layout);
    }
    if(!json && !layout->optional) {
        pg_log_(PG_LOG_ERROR, "    Could not find JSON key \"%s\"\n", src_line, src_file, layout->key);
        return false;
    }
    if(layout->json_object) *layout->json_object = json;
    return true;
}



