#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"

bool pg_file_open_internal(struct pg_file* file, const char* filename, const char* mode)
{
    if(!filename) return false;
    *file = (struct pg_file){0};
    file->line_no = -1;
    /*  Manage filename/path members  */
    strncpy(file->fullname, filename, PG_FILE_PATH_MAXLEN);
    file->fullname_len = strnlen(filename, PG_FILE_PATH_MAXLEN);
    pg_filename_basename(filename, PG_FILE_PATH_MAXLEN, file->basename, PG_FILE_NAME_MAXLEN);
    pg_filename_shortname(filename, PG_FILE_PATH_MAXLEN, file->shortname, PG_FILE_NAME_MAXLEN);
    pg_filename_extension(filename, PG_FILE_PATH_MAXLEN, file->extension, PG_FILE_EXT_MAXLEN);
    pg_filename_path(filename, PG_FILE_PATH_MAXLEN, file->path, PG_FILE_PATH_MAXLEN);
    file->f = fopen(filename, mode);
    if(!file->f) return false;
    fseek(file->f, 0, SEEK_END);
    file->size = ftell(file->f);
    fseek(file->f, 0, SEEK_SET);
    return true;
}

bool pg_file_open_(struct pg_file* file, const char* filename, const char* mode,
                   int src_line, const char* src_file)
{
    if(!pg_file_open_internal(file, filename, mode)) {
        pg_log_(PG_LOG_ERROR, "    Failed to open file '%s' with mode '%s'\n", src_line, src_file, filename, mode);
        return false;
    }
    return true;
}

bool pg_file_open_search_paths_(struct pg_file* file, const char** search_paths, int n_paths,
                                const char* filename, const char* mode,
                                int src_line, const char* src_file)
{
    if(!filename) return false;
    int filename_len = strnlen(filename, PG_FILE_NAME_MAXLEN);
    for(int i = 0; i < n_paths; ++i) {
        int search_path_len = strnlen(search_paths[i], PG_FILE_PATH_MAXLEN);
        if(search_path_len + filename_len >= PG_FILE_PATH_MAXLEN) continue;
        char path[PG_FILE_PATH_MAXLEN] = {0};
        strncpy(path, search_paths[i], search_path_len);
        strncpy(path + search_path_len, filename, filename_len);
        if(pg_file_open_internal(file, path, mode)) return true;
    }
    pg_log_(PG_LOG_ERROR, "    Failed to open file '%s' with mode '%s' in search paths:\n",
            src_line, src_file, filename, mode);
    for(int i = 0; i < n_paths; ++i) pg_log_(PG_LOG_CONTD, "        %s\n", src_line, src_file, search_paths[i]);
    return false;
}

void pg_file_close(struct pg_file* file)
{
    if(file->f) fclose(file->f);
    if(file->content && file->own_content) free(file->content);
}

bool pg_file_read_line(struct pg_file* file)
{
    char* line_ptr = fgets(file->line, PG_FILE_LINE_MAXLEN, file->f);
    if(!line_ptr) return false;
    int line_end = strnlen(line_ptr, PG_FILE_LINE_MAXLEN);
    int line_len = line_end;
    /*  Check for all major styles of line endings  */
    if(line_len > 1
    && (line_ptr[line_end-2] == '\n' || line_ptr[line_end-2] == '\r')) {
        line_ptr[line_end-2] = '\0';
        line_len -= 2;
    } else if(line_len > 0
    && (line_ptr[line_end-1] == '\n' || line_ptr[line_end-1] == '\r')) {
        ++file->line_no;
        line_ptr[line_end-1] = '\0';
        line_len -= 1;
    }
    file->line_len = line_len;
    return true;
}

void pg_file_read_full(struct pg_file* file)
{
    if(file->content) free(file->content);
    file->content = malloc(sizeof(char) * file->size + 1);
    file->own_content = 1;
    fread(file->content, 1, file->size, file->f);
    file->content[file->size] = '\0';
}

char* pg_file_take_content(struct pg_file* file)
{
    if(!file->content) pg_file_read_full(file);
    file->own_content = 0;
    return file->content;
}

void pg_file_reset(struct pg_file* file)
{
    if(file->content && file->own_content) free(file->content);
    file->content = NULL;
    file->own_content = false;
    file->line[0] = '\0';
    file->line_len = 0;
    file->line_no = 0;
    if(file->f) rewind(file->f);
}

int pg_filename_extension(const char* filename, int filename_len, char* out, int out_max)
{
    int ext_start = 0, ext_len = 0;
    int last_char = strnlen(filename, filename_len) - 1;
    /*  Start from the end; Go back until we find a '.' or a directory separator;
     *  if '.' then get extension info, otherwise no extension  */
    for(int i = last_char; i >= 0; --i) {
        if(filename[i] == '/' || filename[i] == '\\') break;
        else if(filename[i] == '.') {
            ext_len = last_char - i;
            ext_start = i + 1;
            break;
        }
    }
    /*  Only copy if we really have an extension    */
    int max_len = out_max < ext_len ? out_max : ext_len;
    if(ext_len) strncpy(out, filename + ext_start, max_len);
    out[max_len] = '\0';
    return ext_len;
}

int pg_filename_basename(const char* filename, int filename_len, char* out, int out_max)
{
    int basename_start = 0, basename_len = strnlen(filename, filename_len);
    int last_char = basename_len - 1;
    /*  Start from the end; Go back until we find a directory separator */
    for(int i = last_char; i >= 0; --i) {
        if(filename[i] == '/' || filename[i] == '\\') {
            basename_len = last_char - i;
            basename_start = i + 1;
            break;
        }
    }
    /*  Only copy if we really have a basename  */
    int max_len = out_max < basename_len ? out_max : basename_len;
    if(basename_len) strncpy(out, filename + basename_start, max_len);
    out[max_len] = '\0';
    return basename_len;
}

int pg_filename_shortname(const char* filename, int filename_len, char* out, int out_max)
{
    int ext_start = 0, ext_len = 0;
    int basename_start = 0, basename_len = strnlen(filename, filename_len);
    int last_char = basename_len - 1;
    /*  Start from the end; Go back until we find a directory separator */
    for(int i = last_char; i >= 0; --i) {
        if(filename[i] == '/' || filename[i] == '\\') {
            basename_len = last_char - i;
            basename_start = i + 1;
            break;
        } else if(!ext_start && filename[i] == '.') {
            ext_len = last_char - i + 1;
            ext_start = i;
        }
    }
    /*  Only copy if we really have a shortname  */
    int shortname_len = basename_len - ext_len;
    int max_len = out_max < shortname_len ? out_max : shortname_len;
    if(shortname_len) strncpy(out, filename + basename_start, max_len);
    out[max_len] = '\0';
    return shortname_len;
}

int pg_filename_path(const char* filename, int filename_len, char* out, int out_max)
{
    int path_len = strnlen(filename, filename_len);
    int last_char = path_len - 1;
    /*  Start from the end; Go back until we find a directory separator */
    int i;
    for(i = last_char; i >= 0; --i) {
        if(filename[i] == '/' || filename[i] == '\\') break;
    }
    path_len = i + 1;
    int max_len = out_max < path_len ? out_max : path_len;
    if(path_len) strncpy(out, filename, max_len);
    out[max_len] = '\0';
    return path_len;
}

int pg_filename_absolute_path(const char* filename, int filename_len, char* out, int out_max)
{
#ifdef WIN32
    char* full_path = _fullpath(NULL, filename, out_max);
#else
    char* full_path = realpath(filename, NULL);
#endif
    int full_path_len = strlen(full_path);
    strncpy(out, full_path, full_path_len < out_max ? full_path_len + 1 : out_max);
    free(full_path);
    return full_path_len;
}

static bool pg_file_read_preprocessed_internal(struct pg_file* file, char_arr_t* out,
        const_str_arr_t* include_dirs, const_str_arr_t* included_files,
        int src_line, const char* src_file);

static bool include_file(struct pg_file* file, char_arr_t* out,
        const_str_arr_t* include_dirs, const_str_arr_t* included_files,
        const char* want_filename,
        int src_line, const char* src_file)
{
    pg_log(PG_LOG_INFO, "including file %s", want_filename);
    /*  Add this file's path to the end of the search_paths array, to also
        search the file's neighbors (after all other search dirs)  */
    ARR_PUSH(*include_dirs, file->path);
    /*  Try to open the file    */
    struct pg_file want_file;
    if(!pg_file_open_search_paths_(&want_file, (const char**)include_dirs->data, include_dirs->len, want_filename, "r",
            src_line, src_file)) {
        pg_log_(PG_LOG_ERROR, "    %s:%d Failed to include file '%s'\n", src_line, src_file,
                file->fullname, file->line_no + 1, want_filename);
        return false;
    }
    /*  Warning-less pop    */
    ARR_TRUNCATE(*include_dirs, include_dirs->len - 1);
    /*  Check that we aren't requesting an include loop */
    int inc_filename_idx;
    const char* inc_filename_ptr;
    ARR_FOREACH(*included_files, inc_filename_ptr, inc_filename_idx) {
        if(strncmp(want_file.fullname, inc_filename_ptr, PG_FILE_PATH_MAXLEN) == 0) {
            pg_log_(PG_LOG_ERROR, "    %s:%d Circular include while attempting to include '%s'\n",
                    src_line, src_file,
                    file->fullname, file->line_no + 1, want_file.fullname);
            pg_file_close(&want_file);
            return false;
        }
    }
    /*  Add the file to the list of included files  */
    ARR_PUSH(*included_files, want_file.fullname);
    /*  Read the included file recursively, passing the same included_files list down to it  */
    if(!pg_file_read_preprocessed_internal(&want_file, out, include_dirs, included_files, src_line, src_file)) {
        pg_log_(PG_LOG_CONTD, "    ... in file included from '%s:%d'\n", src_line, src_file, file->fullname, file->line_no + 1);
        pg_file_close(&want_file);
        return false;
    }
    /*  And remove it from the list of included files to allow repeat inclusions    */
    ARR_TRUNCATE(*included_files, included_files->len - 1);
    /*  Close the included file */
    pg_file_close(&want_file);
    return true;
}

static bool pg_file_read_preprocessed_internal(struct pg_file* file, char_arr_t* out,
        const_str_arr_t* include_dirs, const_str_arr_t* included_files,
        int src_line, const char* src_file)
{
    /*  Go through the file line-by-line.
        If it contains an include directive, then parse the filename, and read the file recursively,
        Otherwise, simply copy the line to the output and continue to the next line */
    while(pg_file_read_line(file)) {
        /*  Tokenize the line by whitespace (' ', '\t') */
        int line_start = pg_strchr_neg(file->line, file->line_len, " \t", 2);
        int line_len = file->line_len - line_start;
        const char* line = file->line + line_start;
        /*  Handle preprocessing directives in the file */
        struct pg_text_tokenizer line_tok = PG_TEXT_TOKENIZER(line, line_len, " \t", 2);
        if(line[0] == '#' && pg_text_tokenizer_next(&line_tok)) {
            /*  #pg_include */
            if(strncmp(line_tok.token, "#pg_include", 11) == 0) {
                if(!pg_text_tokenizer_next(&line_tok)) {
                    pg_log_(PG_LOG_ERROR, "    %s:%d Incomplete include directive\n",
                            src_line, src_file,
                            file->fullname, file->line_no);
                    return false;
                }
                /*  Extract the filename from the second token (handle quotation marks) */
                int want_filename_quotes_start = line_tok.token_start;
                int want_filename_quotes_len = line_len - want_filename_quotes_start;
                const char* want_filename_quotes = line + line_tok.token_start;
                if(want_filename_quotes_len <= 2
                || !(want_filename_quotes[0] == '\"' && want_filename_quotes[want_filename_quotes_len-1] == '\"')) {
                    pg_log_(PG_LOG_ERROR, "    %s:%d Invalid include directive\n", 
                            src_line, src_file,
                            file->fullname, file->line_no);
                    return false;
                }
                int want_filename_len = want_filename_quotes_len - 2;
                char want_filename[PG_FILE_NAME_MAXLEN] = {0};
                strncpy(want_filename, want_filename_quotes + 1, want_filename_len);
                if(!include_file(file, out, include_dirs, included_files, want_filename, src_line, src_file))
                    return false;
                continue;

            /*  #pg_comment */
            } else if(strncmp(line_tok.token, "#pg_comment", 11) == 0) {
                continue;
            }
        }
        /*  Copy regular lines to the output   */
        ARR_RESERVE(*out, out->len + file->line_len + 2);
        strncpy(out->data + out->len, file->line, file->line_len);
        out->data[out->len + file->line_len] = '\n';
        out->data[out->len + file->line_len + 1] = '\0';
        out->len += file->line_len + 1;
    }
    /*  Success!    */
    return true;
}

bool pg_file_read_preprocessed_(struct pg_file* file,
        const char** include_dirs, int n_include_dirs, char_arr_t* out,
        int src_line, const char* src_file)
{
    if(!file || !out) return false;
    /*  We have this wrapper to keep track of all included files even across
        sub-includes, to detect include loops   */
    const_str_arr_t included_files_arr;
    const_str_arr_t include_dirs_arr;
    ARR_INIT(included_files_arr);
    ARR_INIT(include_dirs_arr);
    ARR_PUSH(included_files_arr, file->fullname);
    for(int i = 0; i < n_include_dirs; ++i) ARR_PUSH(include_dirs_arr, include_dirs[i]);
    bool success = pg_file_read_preprocessed_internal(file, out,
            &include_dirs_arr, &included_files_arr, src_line, src_file);
    ARR_DEINIT(included_files_arr);
    ARR_DEINIT(include_dirs_arr);
    return success;
}
