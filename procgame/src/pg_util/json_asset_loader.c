#include "pg_core/pg_core.h"
#include "pg_gpu/pg_gpu.h"


/****************************/
/*  GPU BUFFER ASSETS       */
/****************************/

struct json_asset_source {
    cJSON* json;
};


static void* json_asset_parse(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, cJSON* def)
{
    /*  Create the parsed source object    */
    cJSON* new_json = cJSON_Duplicate(def, true);
    cJSON_DeleteItemFromObject(new_json, "__pg_asset_type");
    cJSON_DeleteItemFromObject(new_json, "__pg_asset_name");
    struct json_asset_source* parsed_source = malloc(sizeof(*parsed_source));
    *parsed_source = (struct json_asset_source){ .json = new_json };
    return parsed_source;
}



static void json_asset_unparse(pg_asset_handle_t asset_handle,
        struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct json_asset_source* json_source = (struct json_asset_source*)parsed_source;
    cJSON_Delete(json_source->json);
}



static void* json_asset_load(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* parsed_source)
{
    struct json_asset_source* json_source = (struct json_asset_source*)parsed_source;
    return json_source->json;
}



static void json_asset_unload(pg_asset_handle_t asset_handle, struct pg_asset_loader* asset_loader, void* loaded_data)
{
    /*  Do nothing  */
}

struct pg_asset_loader* pg_json_asset_loader(void)
{
    struct pg_asset_loader* loader = malloc(sizeof(*loader));
    *loader = (struct pg_asset_loader) {
        .type_name = "pg_json",
        .type_name_len = strlen("pg_json"),
        .parse = json_asset_parse,
        .unparse = json_asset_unparse,
        .load = json_asset_load,
        .unload = json_asset_unload,
    };
    return loader;
}





