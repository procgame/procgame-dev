#include <stdio.h>
#include <stdbool.h>
#include "pg_core/arr.h"
#include "pg_core/htable.h"
#include "pg_util/text_utility.h"

/************************/
/*  String searching    */
/************************/

/*  Find first char in a string that matches any char in chr array  */
int pg_strchr(const char* str, int str_len, const char* chr, int chr_len)
{
    if(!chr || chr_len <= 0) return -1;
    for(int str_i = 0; str_i < str_len && str[str_i]; ++str_i) {
        for(int chr_i = 0; chr_i < chr_len && chr[chr_i]; ++chr_i) {
            if(str[str_i] == chr[chr_i]) return str_i;
        }
    }
    return -1;
}

/*  Find first char in a string that DOESN'T match any char in chr array    */
int pg_strchr_neg(const char* str, int str_len, const char* chr, int chr_len)
{
    if(!chr || chr_len <= 0) return 0;
    for(int str_i = 0; str_i < str_len && str[str_i]; ++str_i) {
        int chr_i;
        for(chr_i = 0; chr_i < chr_len && chr[chr_i]; ++chr_i) {
            if(str[str_i] == chr[chr_i]) break;
        }
        if(chr_i == chr_len) return str_i;
    }
    return -1;
}

/*  Uses the Knuth–Morris–Pratt string searching algorithm  */
int pg_str_find(const char *str, int str_len, const char *search, int search_len)
{
    /*  Handle the first few easy-out cases */
    if(!str || !search || str_len <= 0 || search_len <= 0) return -1;
    int str_len_c = strnlen(str, str_len);
    int search_len_c = strnlen(search, search_len);
    if(search_len_c > str_len_c) return -1;
    /*  Failure table   */
    int i = 0, j = -1;
    int borders[search_len+1];
    borders[i] = j;
    while(i < search_len) {
        while(j >= 0 && search[i] != search[j]) j = borders[j];
        ++i;
        ++j;
        borders[i] = j;
    }
    /*  Loop vars   */
    i = 0;
    j = 0;
    int str_max = str_len - search_len;
    int str_i = 0;
    /*  Main string searching loop  */
    while(i <= str_max) {
        /*  Check forward until a non-matching char is found    */
        while(j < search_len && str[str_i] && search[j] == str[str_i]) {
            ++j;
            ++str_i;
        }
        /*  If we found matching chars and reached the search string's size,
            then we have a match!   */
        if(j == search_len) return str_i - search_len;
        /*  Otherwise, if we got to a NUL byte, no match    */
        if(!str[str_i]) return -1;
        /*  If we're at the beginning of the search string, then just move forward  */
        if (j == 0){
            ++str_i;
            ++i;
        } else {
            /*  Otherwise, set search string index based on prefix in the string    */
            do{
                i += j - borders[j];
                j = borders[j];
            } while(j > 0 && search[j] != str[str_i]);
        }
    }
    /*  No match    */
    return -1;
}


/************************/
/*  pg_text_tokenizer   */
/************************/

bool pg_text_tokenizer_next(struct pg_text_tokenizer* tok)
{
    if(!tok->str || !tok->str_len) return false;
    int search_start = tok->token_start + tok->token_len;
    int search_len = tok->str_len - search_start;
    int first_non_sep = pg_strchr_neg(tok->str + search_start, search_len,
                                      tok->seps, tok->seps_len);
    /*  If the rest of the string consists entirely of separators, then
        we have reached the end     */
    if(first_non_sep == -1) return false;
    /*  Otherwise, increment to the first non-separator character and begin
        the next token there    */
    /*  And then find the next separator    */
    search_start = search_start + first_non_sep;
    search_len = tok->str_len - search_start;
    int next_sep = pg_strchr(tok->str + search_start, search_len, tok->seps, tok->seps_len);
    if(next_sep == -1) {
        /*  If there are no more separators at all, then the token fills
            the rest of the string  */
        tok->token_len = tok->str_len - search_start;
    } else {    
        /*  Otherwise the token length goes to the next separator   */
        tok->token_len = next_sep;
    }
    tok->token = tok->str + search_start;
    tok->token_start = search_start;
    tok->token_end = search_start + tok->token_len - 1;
    ++tok->token_idx;
    return true;
}

/************************/
/*  pg_text_variator    */
/************************/


static int pg_text_variator_make_idx(struct pg_text_variator* var, const char* name, int name_len)
{
    int idx = -1;
    HTABLE_NGET_V(var->map, name, name_len, idx);
    if(idx >= 0) return idx;
    /*  Make a new variant  */
    struct pg_text_variant new_var = {0};
    strncpy(new_var.name, name, name_len);
    ARR_INIT(new_var.lines);
    ARR_PUSH(var->variants, new_var);
    /*  Add new variant index to the map    */
    HTABLE_NSET(var->map, name, name_len, var->variants.len - 1);
    /*  Return the new variant index    */
    return var->variants.len - 1;
}

static void pg_text_variant_add_range(struct pg_text_variant* variant,
                                       int begin, int len)
{
    ARR_PUSH(variant->lines, (struct pg_text_range){begin, len});
}

static void pg_text_variator_add_range(struct pg_text_variator* var, int idx,
                                       int begin, int len)
{
    if(idx < 0 || idx >= var->variants.len) return;
    pg_text_variant_add_range(&var->variants.data[idx], begin, len);
}

/*  Managing a stack of variants, for parsing the structure needed to generate
    all possible variations */
struct variant_stack {
    ARR_T(struct variant_stack_cell {
        int begin;
        int variations[8];
        int n_variations;
    }) cells;
    bool_arr_t active;
};

static void variant_stack_init(struct variant_stack* stack);
static void variant_stack_deinit(struct variant_stack* stack);
static void variant_stack_push(struct variant_stack* stack, int begin, int* vars, int n_vars);
static void variant_stack_pop(struct variant_stack* stack, int end_line,
                              struct pg_text_variator* var);

void pg_text_variator_init(struct pg_text_variator* var,
                           const char* str, int str_len,
                           const char* begin_token, const char* end_token)
{
    /*  Save these length values for convenience    */
    int str_len_c = strnlen(str, str_len);
    int begin_token_len = strlen(begin_token);
    int end_token_len = strlen(end_token);

    /*  Initialize the variator struct  */
    *var = (struct pg_text_variator){0};
    ARR_INIT(var->variants);
    HTABLE_INIT(var->map, 8);
    var->str = str;
    var->str_len = str_len_c;

    /*  Stack of variant blocks, to handle nested variants  */
    struct variant_stack var_stack;
    variant_stack_init(&var_stack);
    /*  The "base variant" ie. tracks all lines not covered by any variant  */
    int base_start = 0;

    /*  Begin parsing   */
    /*  Tokenize the string by lines    */
    struct pg_text_tokenizer line_tok = PG_TEXT_TOKENIZER(str, str_len, "\r\n", 2);
    while(pg_text_tokenizer_next(&line_tok)) {
        /*  Add line to variator regardless of variants */
        ARR_PUSH(var->lines, (struct pg_text_range){ line_tok.token_start, line_tok.token_len });
        /*  Check for variant directives    */
        struct pg_text_tokenizer word_tok = PG_TEXT_TOKENIZER(line_tok.token, line_tok.token_len, " \t", 2);
        /*  Empty line - skip it    */
        if(!pg_text_tokenizer_next(&word_tok)) continue;
        /*  First token doesn't match either variant token's length, this isn't a matching line */
        if(word_tok.token_len != begin_token_len && word_tok.token_len != end_token_len) continue;

        /*  Handle variant beginning directives */
        if(strncmp(word_tok.token, begin_token, begin_token_len) == 0) {
            int vars[8] = {-1};
            int n_vars = 0;
            int line_no = line_tok.token_idx;
            /*  Handle the base variant */
            if(var_stack.cells.len == 0 && line_no > 0) {
                ARR_PUSH(var->base_variant.lines,
                        (struct pg_text_range){ base_start, line_no - base_start });
            }
            /*  Consider every subsequent token to be a variant name    */
            while(n_vars <= 8 && pg_text_tokenizer_next(&word_tok)) {
                /*  Make the new variant if it doesn't exist, and get its index */
                int variant_idx = pg_text_variator_make_idx(var, word_tok.token, word_tok.token_len);
                if(variant_idx == -1) continue;
                /*  Add variant to the list of variants for this block  */
                vars[n_vars++] = variant_idx;
            }
            /*  Add the set of variations to the stack  */
            variant_stack_push(&var_stack, line_no + 1, vars, n_vars);
        }

        /*  Handle variant ending directives    */
        else if(strncmp(word_tok.token, end_token, end_token_len) == 0) {
            if(var_stack.cells.len <= 0) continue;
            int end = line_tok.token_idx;
            variant_stack_pop(&var_stack, end, var);
            /*  Handle the base variant, if this is a top-level variant block   */
            if(var_stack.cells.len == 0) base_start = end + 1;
        }
    }

    /*  Handle the base variant for the last lines of the source    */
    if(var_stack.cells.len == 0) {
        pg_text_variant_add_range(&var->base_variant, base_start, (line_tok.token_idx + 1) - base_start);
    }

    variant_stack_deinit(&var_stack);
}


void pg_text_variator_deinit(struct pg_text_variator* var)
{
    int i;
    struct pg_text_variant* var_iter;
    ARR_FOREACH_PTR(var->variants, var_iter, i) {
        ARR_DEINIT(var_iter->lines);
    }
    ARR_DEINIT(var->variants);
    ARR_DEINIT(var->base_variant.lines);
    ARR_DEINIT(var->lines);
    HTABLE_DEINIT(var->map);
}


static bool variant_has_line(struct pg_text_variant* variant, int line)
{
    int i;
    struct pg_text_range* range;
    ARR_FOREACH_PTR(variant->lines, range, i) {
        if(range->begin <= line && (range->begin + range->len) > line)
            return true;
    }
    return false;
}


int pg_text_variator_generate(struct pg_text_variator* var,
                              char* out, int out_cap,
                              const char** variants, int n_variants)
{
    int out_i = 0;
    struct pg_text_variant* vars[n_variants];
    /*  First get pointers to our variants  */
    for(int i = 0; i < n_variants; ++i) {
        int idx = -1;
        HTABLE_GET_V(var->map, variants[i], idx);
        if(idx == -1) vars[i] = NULL;
        else vars[i] = &var->variants.data[idx];
    }
    /*  Now go over every line of the source string */
    for(int i = 0; i < var->lines.len; ++i) {
        bool use_line = false;
        /*  Decide if this line should be included in the output or not
            by checking every enabled variant (and the base variant)    */
        if(variant_has_line(&var->base_variant, i)) {
            use_line = true;
        } else {
            for(int j = 0; j < n_variants; ++j) {
                if(variant_has_line(vars[j], i)) {
                    use_line = true;
                    break;
                }
            }
        }
        if(!use_line) continue;
        if(i != 0) out[out_i++] = '\n';
        int line_len = var->lines.data[i].len;
        strncpy(out + out_i, var->str + var->lines.data[i].begin, line_len);
        out_i += line_len;
    }
    out[out_i] = '\0';
    return out_i;
}


int pg_text_variator_count(struct pg_text_variator* var)
{
    return var->variants.len;
}


const char* pg_text_variator_name(struct pg_text_variator* var, int idx)
{
    if(idx < 0 || idx >= var->variants.len) return NULL;
    return var->variants.data[idx].name;
}

static void variant_stack_init(struct variant_stack* stack)
{
    ARR_INIT(stack->cells);
    ARR_INIT(stack->active);
}

static void variant_stack_deinit(struct variant_stack* stack)
{
    ARR_DEINIT(stack->cells);
    ARR_DEINIT(stack->active);
}

static void variant_stack_push(struct variant_stack* stack, int begin, int* vars, int n_vars)
{
    struct variant_stack_cell cell = { .begin = begin };
    for(int i = 0; i < n_vars && i < 8; ++i) {
        int variant_idx = vars[i];
        /*  Mark the variant as active (making a new entry in the list if needed)
            We also do not add the variant if it is already active (this prevents
            having multiple nested ranges for the same variant, if that is what
            is defined in the source string)    */
        if(stack->active.len <= variant_idx) ARR_PUSH(stack->active, true);
        else if(stack->active.data[variant_idx]) return;
        else stack->active.data[variant_idx] = true;
        /*  Add the variant to the stack cell   */
        cell.variations[cell.n_variations++] = variant_idx;
    }
    ARR_PUSH(stack->cells, cell);
}

static void variant_stack_pop(struct variant_stack* stack, int end_line,
                              struct pg_text_variator* var)
{
    struct variant_stack_cell cell = ARR_POP(stack->cells);
    for(int i = 0; i < cell.n_variations; ++i) {
        stack->active.data[cell.variations[i]] = false;
        pg_text_variator_add_range(var, cell.variations[i],
                                   cell.begin, end_line - cell.begin);
    }
}

