#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"

static bool asset_mgr_read_var(pg_asset_manager_t* asset_mgr, cJSON* json, enum pg_data_type type, void* out)
{
    if(!asset_mgr) return false;
    if(cJSON_IsString(json)) {
        pg_data_t* var = pg_asset_manager_get_variable(asset_mgr, json->valuestring);
        if(!var) return false;
        if(pg_data_base_type[var->type] != pg_data_base_type[type]) {
            pg_log(PG_LOG_ERROR, "Tried to read %s variable '%s' with mismatched type %s",
                    pg_data_type_string[var->type], json->valuestring, pg_data_type_string[type]);
            return false;
        }
        memcpy(out, pg_data_value_ptr(var, 0), pg_data_type_size[type]);
        return true;
    }
    return false;
}


#define MGR_READ(TYPE) pg_asset_manager_read_##TYPE

#define JSON_READ_OPERATORS(JSON, MANAGER, ...) \
    pg_asset_manager_t* json_asset_manager = (MANAGER); \
    if(cJSON_IsObject(JSON) && (JSON)->child) { \
        cJSON* operator_json = (JSON)->child; \
        if(operator_json->string) { __VA_ARGS__ } \
    }

#define JSON_READ_OPERATOR_0(OPERATOR, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        __VA_ARGS__; \
    }

#define JSON_READ_OPERATOR_1(OPERATOR, OPERAND_TYPE, OPERAND_NAME, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        OPERAND_TYPE OPERAND_NAME = MGR_READ(OPERAND_TYPE)(json_asset_manager, operator_json->child); \
        __VA_ARGS__; \
    }

#define JSON_READ_OPERATOR_2(OPERATOR, LHS_TYPE, LHS_NAME, RHS_TYPE, RHS_NAME, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        LHS_TYPE LHS_NAME = MGR_READ(LHS_TYPE)(json_asset_manager, operator_json->child); \
        RHS_TYPE RHS_NAME = MGR_READ(RHS_TYPE)(json_asset_manager, operator_json->child->next); \
        __VA_ARGS__; \
    }

#define JSON_READ_OPERATOR_CUSTOM(OPERATOR, OPERAND, ...) \
    if(strcmp(operator_json->string, (OPERATOR)) == 0) { \
        cJSON* OPERAND = operator_json; \
        __VA_ARGS__; \
    }



mat4 pg_asset_manager_read_mat4(pg_asset_manager_t* mgr, cJSON* json)
{
    JSON_READ_OPERATORS(json, mgr,
        JSON_READ_OPERATOR_0("identity", 
            return mat4_identity()
        );
        JSON_READ_OPERATOR_1("translation", vec3, tx,
            return mat4_translation(tx);
        );
        JSON_READ_OPERATOR_1("scaling", vec3, tx,
            return mat4_scaling(tx);
        );
        JSON_READ_OPERATOR_1("euler", vec3, angles,
            return mat4_euler(angles);
        );
        JSON_READ_OPERATOR_2("mat4_mul", mat4, lhs, mat4, rhs,
            return mat4_mul(lhs, rhs);
        );
        JSON_READ_OPERATOR_CUSTOM("mat4_mul_chain", matrices,
            if(cJSON_IsArray(matrices) && matrices->child) {
                mat4 result = mat4_identity();
                cJSON* next_mat;
                cJSON_ArrayForEach(next_mat, matrices) {
                    result = mat4_mul(result, json_read_mat4(next_mat));
                }
                return result;
            } else {
                return mat4_identity();
            }
        );
        JSON_READ_OPERATOR_CUSTOM("look_at", look_json,
            cJSON* eye_json, *pt_json, *up_json;
            struct pg_json_layout look_layout = PG_JSON_LAYOUT(3,
                PG_JSON_LAYOUT_ITEM("eye", cJSON_Array, &eye_json),
                PG_JSON_LAYOUT_ITEM("pt", cJSON_Array, &pt_json),
                PG_JSON_LAYOUT_ITEM("up", cJSON_Array, &up_json));
            if(!pg_load_json_layout(look_json, &look_layout)) {
                pg_log(PG_LOG_ERROR, "invalid parameters to JSON read operator mat4.lookat (expects 'eye', 'pt', and 'up')");
                return mat4_identity();
            }
            return mat4_look_at(
                json_read_vec3(eye_json),
                json_read_vec3(pt_json),
                json_read_vec3(up_json) );
        );
        JSON_READ_OPERATOR_CUSTOM("perspective", look_json,
            cJSON* fov_json, *aspect_json, *near_json, *far_json;
            struct pg_json_layout look_layout = PG_JSON_LAYOUT(4,
                PG_JSON_LAYOUT_ITEM("y_fov", cJSON_Number, &fov_json),
                PG_JSON_LAYOUT_ITEM("aspect", cJSON_Number, &aspect_json),
                PG_JSON_LAYOUT_ITEM("near", cJSON_Number, &near_json),
                PG_JSON_LAYOUT_ITEM("far", cJSON_Number, &far_json));
            if(!pg_load_json_layout(look_json, &look_layout)) {
                pg_log(PG_LOG_ERROR, "invalid parameters to JSON read operator mat4.perspective (expects 'eye', 'pt', and 'up')");
                return mat4_identity();
            }
            return mat4_perspective(
                fov_json->valuedouble,
                aspect_json->valuedouble,
                near_json->valuedouble,
                far_json->valuedouble);
        );
    );
    return json_read_mat4(json);
}


const char* pg_asset_manager_read_string(pg_asset_manager_t* asset_mgr, cJSON* json)
{
    pg_data_t* var = pg_asset_manager_get_variable(asset_mgr, json->valuestring);
    if(!var) return json->valuestring;
    else return pg_data_value_ptr(var, 0);
}

/*  ex. ASSET_MGR_READ_DEF( PG_VEC4, vec4 ) */
#define ASSET_MGR_READ_DEF(TYPE, SMALLTYPE) \
SMALLTYPE pg_asset_manager_read_##SMALLTYPE(pg_asset_manager_t* asset_mgr, cJSON* json) \
{ \
    SMALLTYPE ret; \
    memset(&ret, 0, sizeof(SMALLTYPE)); \
    if(!asset_mgr_read_var(asset_mgr, json, TYPE, &ret)) ret = json_read_##SMALLTYPE(json); \
    return ret; \
}

ASSET_MGR_READ_DEF(PG_VEC4, vec4)
ASSET_MGR_READ_DEF(PG_VEC3, vec3)
ASSET_MGR_READ_DEF(PG_VEC2, vec2)
ASSET_MGR_READ_DEF(PG_FLOAT, float)
ASSET_MGR_READ_DEF(PG_IVEC4, ivec4)
ASSET_MGR_READ_DEF(PG_IVEC3, ivec3)
ASSET_MGR_READ_DEF(PG_IVEC2, ivec2)
ASSET_MGR_READ_DEF(PG_INT, int)

#define ASSET_MGR_GET_VAR_DEF(TYPE, SMALLTYPE) \
SMALLTYPE pg_asset_manager_get_variable_##SMALLTYPE(pg_asset_manager_t* asset_mgr, const char* name) \
{  \
    SMALLTYPE ret; \
    memset(&ret, 0, sizeof(SMALLTYPE)); \
    pg_data_t* var = pg_asset_manager_get_variable(asset_mgr, name); \
    if(!var) { \
        pg_log(PG_LOG_ERROR, "Tried to get non-existent variable '%s'", name); \
    } else if(pg_data_base_type[var->type] != pg_data_base_type[TYPE]) { \
        pg_log(PG_LOG_ERROR,"Tried to get variable of type %s '%s', but it was declared as %s", \
            pg_data_type_string[var->type], pg_data_type_string[TYPE]); \
    } else { \
        ret = *((SMALLTYPE*)pg_data_value_ptr(var, 0)); \
    } \
    return ret; \
}

ASSET_MGR_GET_VAR_DEF(PG_VEC4, vec4)
ASSET_MGR_GET_VAR_DEF(PG_VEC3, vec3)
ASSET_MGR_GET_VAR_DEF(PG_VEC2, vec2)
ASSET_MGR_GET_VAR_DEF(PG_FLOAT, float)
ASSET_MGR_GET_VAR_DEF(PG_IVEC4, ivec4)
ASSET_MGR_GET_VAR_DEF(PG_IVEC3, ivec3)
ASSET_MGR_GET_VAR_DEF(PG_IVEC2, ivec2)
ASSET_MGR_GET_VAR_DEF(PG_INT, int)
