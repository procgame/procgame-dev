#include <stdio.h>
#include <stdarg.h>

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"

/****************************/
/*  INTERNAL STRUCTURES     */
/****************************/

struct pg_asset {
    /*  BOOK-KEEPING    */
    pg_mempool_id_t id; // pg_asset_id_t
    bool deleted;
    char name[PG_ASSET_NAME_MAXLEN];
    bool asset_loaded; // whether the asset is "complete" (we know its type, name, and source)
    pg_asset_group_t group; // an asset can be a part of a single group
    struct pg_asset_loader* loader; // The asset's "type" associates it with a given asset loader  */
    ARR_T(pg_mempool_id_t) dependencies;
    ARR_T(pg_mempool_id_t) dependents;
    bool is_static; // whether an asset should be deleted when its last dependent is deleted

    /*  SOURCE  */
    bool source_loaded, own_source;
    enum {
        PG_ASSET_SOURCE_UNDEFINED,
        PG_ASSET_SOURCE_FILE,
        PG_ASSET_SOURCE_JSON,
        PG_ASSET_SOURCE_DATA,
    } source_type;
    char source_path[PG_FILE_PATH_MAXLEN];
    cJSON* source_json;
    bool source_parsed;
    void* parsed_source;

    /*  DATA    */
    bool deps_loaded;
    bool data_loaded;
    bool own_data;
    void* loaded_data;
};

PG_MEMPOOL_DECLARE(struct pg_asset, pg_asset);
PG_MEMPOOL_DEFINE(struct pg_asset, pg_asset, id);
typedef HTABLE_T(pg_asset_id_t) pg_asset_table_t;
typedef ARR_T(pg_asset_id_t) pg_asset_arr_t;
typedef ARR_T(struct pg_asset_loader*) pg_asset_loader_arr_t;

struct pg_asset_group {
    char name[PG_ASSET_NAME_MAXLEN];
    pg_asset_arr_t members;
};

typedef ARR_T(struct pg_asset_group) pg_asset_group_data_arr_t;
typedef int_arr_t pg_asset_group_arr_t;

struct pg_asset_manager {
    pg_asset_pool_t asset_pool;
    pg_asset_table_t asset_table;
    pg_asset_group_data_arr_t groups;
    pg_asset_group_arr_t deleted_groups;
    pg_asset_loader_arr_t loaders;
    int_table_t group_table;
    int_table_t loader_table;
    ARR_T(char*) search_path_ptrs;
    ARR_T(struct search_path { char path[PG_FILE_PATH_MAXLEN]; }) search_paths;
    pg_data_table_t vars_table;
};



/****************************/
/*  INTERNAL FUNCTIONS      */
/****************************/

static int generate_name(struct pg_asset_loader* type_loader, char* out)
{
    snprintf(out, PG_ASSET_TYPE_NAME_MAXLEN, "%.*s_%d",
            type_loader->type_name_len, type_loader->type_name,
            type_loader->counter++);
    return strnlen(out, PG_ASSET_NAME_MAXLEN);
}

static char* asset_manager_new_search_path(pg_asset_manager_t* asset_mgr)
{
    return ARR_NEW(asset_mgr->search_paths)->path;
}

static struct pg_asset_loader* get_type_loader(pg_asset_manager_t* asset_mgr,
                                               const char* type, int type_len)
{
    if(!asset_mgr || !type) return NULL;
    int* loader_idx = NULL;
    HTABLE_NGET(asset_mgr->loader_table, type, type_len, loader_idx);
    if(!loader_idx) return NULL;
    return asset_mgr->loaders.data[*loader_idx];
}

/*  Root function for handling all references to assets. Only the name is
    required - if the asset exists already then it will be checked for
    mismatching type and path. If the existing asset was incomplete, then
    any new information given here will be used to update the existing asset.
    If the asset doesn't exist, then it will be created (even if no type or
    path are given).
    On success, asset_ptr will be updated to point to the newly allocated
    asset pointer, and a valid asset handle will be returned.   */
static pg_asset_handle_t declare_asset(pg_asset_manager_t* asset_mgr,
        const char* type, int type_len, const char* name_, int name_len_,
        const char* path, int path_len, struct pg_asset** asset_ptr)
{
    if(asset_ptr) *asset_ptr = NULL;
    if((!name_ || !name_len_) && (!type || !type_len)) {
        pg_log(PG_LOG_ERROR, "Tried to declare asset with no name and no type");
        return (pg_asset_handle_t){};
    }
    struct pg_asset_loader* loader = NULL;
    /*  Check the type  */
    if(type && type_len > 0) {
        loader = get_type_loader(asset_mgr, type, type_len);
        if(!loader) {
            if(name_ && name_len_) {
                pg_log(PG_LOG_ERROR, "Asset ('%.*s') declared with unknown type '%.*s'",
                        name_len_, name_, type_len, type);
            } else {
                pg_log(PG_LOG_ERROR, "Asset declared with unknown type '%.*s'",
                        type_len, type);
            }
            return (pg_asset_handle_t){0};
        }
    }
    /*  Handle the name (generate one if necessary) */
    int name_len = 0;
    char name[PG_ASSET_NAME_MAXLEN] = {};
    if(!name_ || !name_len_) {
        if(!loader) {
            pg_log(PG_LOG_ERROR, "Can't generate asset name without a type");
            return (pg_asset_handle_t){};
        }
        name_len = generate_name(pg_asset_manager_get_type_loader(asset_mgr, type), name);
    } else {
        strncpy(name, name_, name_len_);
        name_len = name_len_;
    }
    /*  Check if this asset is already loaded (by name) and update the unlisted fields
        (and check for mismatches, which probably indicates an error)  */
    pg_asset_id_t* check_id = NULL;
    HTABLE_NGET(asset_mgr->asset_table, name, name_len, check_id);
    if(check_id) {
        struct pg_asset* ptr = pg_asset_get(&asset_mgr->asset_pool, *check_id);
        /*  Check that the type (ie. the asset loader) matches the already-
            declared asset. If the loaded asset has unknown type, then update it    */
        if(loader && ptr->loader != loader) {
            if(ptr->loader && ptr->loader != loader) {
                pg_log(PG_LOG_ERROR,
                       "Type mismatch declaring asset ('%.*s'), "
                       "originally declared with type '%s', "
                       "redeclared with type '%.*s'",
                       name_len, name, ptr->loader->type_name, type_len, type);
                return (pg_asset_handle_t){0};
            }
            ptr->loader = loader;
        }
        /*  Check that the path matches the already-declared asset. If the loaded
            asset has an unknown path, then update it   */
        if(path) {
            if(ptr->source_path[0] != '\0'
            && strncmp(ptr->source_path, path, path_len) != 0) {
                pg_log(PG_LOG_ERROR,
                       "Asset ('%.*s') originally given path '%s', "
                       "redeclared with path '%.*s'",
                       name_len, name, ptr->source_path, path_len, path);
                return (pg_asset_handle_t){0};
            } else if(!ptr->source_path[0]) {
                /*  try to load the file, if we succeed, then record the path
                    we actually found it at    */
                strncpy(ptr->source_path, path, path_len);
                struct pg_file source_file;
                if(!pg_asset_manager_load_file(asset_mgr, &source_file, ptr->source_path, "r")) {
                    pg_log(PG_LOG_ERROR, "Asset ('%.*s') source file could not be located",
                            name_len, name);
                    return (pg_asset_handle_t){0};
                }
                strncpy(ptr->source_path, source_file.fullname, PG_FILE_PATH_MAXLEN);
            }
        }
        if(ptr->loader) ptr->asset_loaded = true;
        /*  If an asset already exists, then we have updated it, so just
            return that asset's id  */
        if(asset_ptr) *asset_ptr = ptr;
        return (pg_asset_handle_t){ asset_mgr, *check_id };
    }
    /*  Otherwise allocate and initialize a new asset   */
    struct pg_asset* ptr;
    pg_asset_id_t id = pg_asset_alloc(&asset_mgr->asset_pool, &ptr);
    *ptr = (struct pg_asset){ .id = id };
    strncpy(ptr->name, name, name_len);
    /*  Add the asset to the table so we can search it by name  */
    HTABLE_NSET(asset_mgr->asset_table, name, name_len, id);
    ARR_INIT(ptr->dependencies);
    ARR_INIT(ptr->dependents);
    /*  Type    */
    if(type && type_len > 0 && type[0]) {
        ptr->loader = loader;
    }
    /*  Path    */
    if(path && path_len > 0 && path[0]) {
        ptr->source_type = PG_ASSET_SOURCE_FILE;
        /*  try to load the file, if we succeed, then record the path
            we actually found it at    */
        strncpy(ptr->source_path, path, path_len);
        struct pg_file source_file;
        if(!pg_asset_manager_load_file(asset_mgr, &source_file, ptr->source_path, "r")) {
            pg_log(PG_LOG_ERROR, "Asset ('%.*s') source file could not be located",
                    name_len, name);
            return (pg_asset_handle_t){0};
        }
        strncpy(ptr->source_path, source_file.fullname, PG_FILE_PATH_MAXLEN);
    }
    /*  If we know the type and the path, then the asset is fully loaded
        (even if we haven't loaded its data yet)    */
    if(type_len > 0 && name_len > 0) ptr->asset_loaded = true;
    /*  Return everything   */
    if(asset_ptr) *asset_ptr = ptr;
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, (pg_asset_handle_t){ .mgr = asset_mgr, .id = id },
        "    (+  ) DECLARED                      ");
    }
    return (pg_asset_handle_t){ .mgr = asset_mgr, .id = id };
}



/*  (type)name:path */
/*  This function parses a single string in the asset ID format, and passes
    its parts to declare_asset().   */
static pg_asset_handle_t declare_asset_string(pg_asset_manager_t* asset_mgr, const char* string, int n,
                                              struct pg_asset** asset_ptr)
{
    if(!string || n <= 0) return (pg_asset_handle_t){0};
    /*  The type and path are optional, so first we have to check if they are here  */
    bool have_typename = false;
    bool have_path = false;
    /*  Take the significant characters' indices    */
    int first_paren = pg_strchr(string, n, "(", 1);
    int second_paren = pg_strchr(string, n, ")", 1);
    int colon = pg_strchr(string, n, ":", 1);
    /*  Validate the results    */
    if(pg_strchr(string + first_paren + 1, n - first_paren - 1, "(", 1) >= 0
    || pg_strchr(string + second_paren + 1, n - second_paren - 1, ")", 1) >= 0
    || pg_strchr(string + colon + 1, n - colon - 1, ":", 1) >= 0) {
        pg_log(PG_LOG_ERROR, "Invalid asset identifier ('%s')", string);
        return (pg_asset_handle_t){0};
    }
    /*  Validate the syntax relative to each other  */
    if((first_paren > second_paren) || (second_paren >= 0 && first_paren < 0)) {
        pg_log(PG_LOG_ERROR, "Mismatched parens in asset identifier ('%s')", string);
        return (pg_asset_handle_t){0};
    } else if((first_paren >= 0) && (second_paren >= 0)) {
        have_typename = true;
    }
    if((colon >= 0)) {
        if(colon < second_paren) {
            pg_log(PG_LOG_ERROR, "Unexpected ':' in type specifier in asset identifier ('%s')", string);
            return (pg_asset_handle_t){0};
        }
        have_path = true;
    }
    /*  Set up our pointers and lengths */
    const char* type = NULL, * name = NULL, * path = NULL;
    int type_len = 0, name_len = 0, path_len = 0;
    int name_start = (have_typename ? second_paren + 1 : 0);
    name = string + name_start;
    name_len = (have_path ? colon - name_start : n - name_start);
    if(have_typename) {
        type = string + first_paren + 1;
        type_len = second_paren - first_paren - 1;
    }
    if(have_path) {
        path = string + colon + 1;
        path_len = n - colon - 1;
    }
    /*  Do The Thing    */
    return declare_asset(asset_mgr, type, type_len, name, name_len, path, path_len, asset_ptr);
}



/*  For an asset with a known source path, but which has not loaded the JSON source */
static bool asset_load_source(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    struct pg_asset_manager* mgr = asset_handle.mgr;
    if(asset_ptr->source_loaded) return true;
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (+  ) loading source JSON           ");
    }

    /*  Temporarily add the asset's own path to the search paths while we are loading the asset */
    int tmp_search_path_idx = asset_handle.mgr->search_paths.len;
    char* tmp_search_path = asset_manager_new_search_path(asset_handle.mgr);
    pg_filename_path(asset_ptr->source_path, PG_FILE_PATH_MAXLEN, tmp_search_path, PG_FILE_PATH_MAXLEN);

    /*  Only for 'file' and 'json' sourced assets do we try to load a source    */
    if((asset_ptr->source_type == PG_ASSET_SOURCE_FILE || asset_ptr->source_type == PG_ASSET_SOURCE_JSON)
    && asset_ptr->source_path[0]) {
        /*  Try to load the source file */
        struct pg_file json_file;
        int n_search_paths = 0;
        const char** search_paths = pg_asset_manager_get_search_paths(mgr, &n_search_paths);
        if(!pg_file_open_search_paths(&json_file, search_paths, n_search_paths,
                    asset_ptr->source_path, "r")) {
            /*  Failed to load the file at all  */
            pg_asset_log(PG_LOG_ERROR, asset_handle, "source could not be loaded from '%s'",
                    asset_ptr->source_path);
        } else {
            /*  If we loaded the file, then parse it, accounting for include
                directives ('#include'-style)   */
            asset_ptr->source_json = json_from_file_preprocessed(&json_file,
                    search_paths, n_search_paths);
            if(!asset_ptr->source_json) {
                /*  Failed to read the file due to include directives   */
                pg_asset_log(PG_LOG_ERROR, asset_handle, "source has broken include directives");
            } else {
                /*  Success!    */
                asset_ptr->source_loaded = true;
                asset_ptr->own_source = true;
            }
        }
        pg_file_close(&json_file);
    }

    /*  Remove the asset's path from the search paths   */
    ARR_SPLICE(mgr->search_paths, tmp_search_path_idx, 1);
    return asset_ptr->source_loaded;
}



/*  Destroy an asset's JSON source object   */
static void asset_unload_source(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(!asset_ptr->own_source || !asset_ptr->source_loaded || asset_ptr->source_type == PG_ASSET_SOURCE_DATA) return;
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (-  ) Unloading source JSON         ");
    }
    cJSON_Delete(asset_ptr->source_json);
    asset_ptr->source_loaded = false;
}



/*  Parse an asset's JSON source into the asset loader's own source type, using
    the asset loader's 'parse' function pointer. We don't create the final object
    here, only an intermediate representation that will be used later to create
    the object without having to parse/validate any JSON again   */
static bool asset_parse(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(asset_ptr->source_parsed) return true;
    if(!asset_ptr->loader) return false;
    if(!asset_ptr->loader->parse) return true; /*   No parser, nothing to parse, nothing to do! */
    if(!asset_ptr->source_loaded && !asset_load_source(asset_handle, asset_ptr)) return false;
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (++ ) Parsing source data           ");
    }

    /*  Temporarily add the asset's own path to the search paths while we are loading the asset */
    int tmp_search_path_idx = asset_handle.mgr->search_paths.len;
    char* tmp_search_path = asset_manager_new_search_path(asset_handle.mgr);
    pg_filename_path(asset_ptr->source_path, PG_FILE_PATH_MAXLEN, tmp_search_path, PG_FILE_PATH_MAXLEN);

    /*  Load the dependencies using the asset loader's function pointer */
    void* parsed_source = asset_ptr->loader->parse(asset_handle, asset_ptr->loader, asset_ptr->source_json);
    asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id); // might have been invalidated
    if(parsed_source) {
        asset_ptr->source_parsed = true;
        asset_ptr->parsed_source = parsed_source;
    } else {
        pg_asset_log(PG_LOG_ERROR, asset_handle, "asset loader failed to process JSON source.");
    }

    /*  Remove the asset's path from the search paths   */
    ARR_SPLICE(asset_handle.mgr->search_paths, tmp_search_path_idx, 1);
    return asset_ptr->source_parsed;
}



/*  Destroy the intermediate source object  */
static void asset_unparse(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(!asset_ptr->source_parsed || !asset_ptr->loader || !asset_ptr->loader->unparse) return;
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (-- ) Destroying parsed source data ");
    }
    asset_ptr->loader->unparse(asset_handle, asset_ptr->loader, asset_ptr->parsed_source);
}

static bool asset_load(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr);

static bool asset_load_deps(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(asset_ptr->deps_loaded) return true;
    /*  Make sure all the dependencies exist and have a non-NULL data pointer   */
    int i;
    pg_asset_id_t dep_id;
    ARR_FOREACH(asset_ptr->dependencies, dep_id, i) {
        /*  Get the dependency asset pointer, and try to load the final object
            from the dependency asset, and check that the received pointer is
            not NULL    */
        struct pg_asset* dep_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, dep_id);
        bool got_data = asset_load((pg_asset_handle_t){ asset_handle.mgr, dep_id }, dep_ptr) && dep_ptr->loaded_data;
        asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id); // might have been invalidated
        if(!got_data) {
            pg_log(PG_LOG_ERROR, "Asset '(%s)%s:%s' has dependency asset '(%s)%s:%s' which failed to load its data",
                    asset_ptr->loader ? asset_ptr->loader->type_name : "?", asset_ptr->name, asset_ptr->source_path,
                    dep_ptr->loader ? dep_ptr->loader->type_name : "?", dep_ptr->name, dep_ptr->source_path);
            return false;
        }
    }
    asset_ptr->deps_loaded = true;
    return true;
}


/*  Load the final object from the loader's source object, using the loader's
    'load' function pointer */
static bool asset_load(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id);
    if(!asset_ptr->deps_loaded && !asset_load_deps(asset_handle, asset_ptr)) return false;
    asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id);
    if(asset_ptr->data_loaded) return true;
    if(!asset_ptr->loader) return false;

    /*  No data loader, no data to load, so all data is loaded! */
    if(!asset_ptr->loader->load) return true;
    /*  Wasn't loaded by any of the dependencies, and can't load a source, so fail  */
    if(!asset_ptr->source_loaded && !asset_load_source(asset_handle, asset_ptr)) return false;
    /*  Fail to parse source JSON   */
    if(!asset_ptr->source_parsed && !asset_parse(asset_handle, asset_ptr)) return false;

    /*  Might have been invalidated */
    asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id); // might have been invalidated

    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (+++) Loading final data            ");
    }

    /*  Temporarily add the asset's own path to the search paths while we are loading the asset */
    int tmp_search_path_idx = asset_handle.mgr->search_paths.len;
    char* tmp_search_path = asset_manager_new_search_path(asset_handle.mgr);
    pg_filename_path(asset_ptr->source_path, PG_FILE_PATH_MAXLEN, tmp_search_path, PG_FILE_PATH_MAXLEN);

    /*  Load the data using the asset loader's 'load' function pointer */
    void* data = asset_ptr->loader->load(asset_handle, asset_ptr->loader, asset_ptr->parsed_source);
    asset_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, asset_handle.id); // might have been invalidated
    if(data) {
        asset_ptr->loaded_data = data;
        asset_ptr->data_loaded = true;
    } else {
        asset_ptr->loaded_data = NULL;
        pg_log(PG_LOG_ERROR, "Asset '(%s)%s:%s' failed to load its data",
                asset_ptr->loader ? asset_ptr->loader->type_name : "?", asset_ptr->name, asset_ptr->source_path);
    }

    /*  Remove the asset's path from the search paths   */
    ARR_SPLICE(asset_handle.mgr->search_paths, tmp_search_path_idx, 1);
    return asset_ptr->data_loaded;
}


static void asset_unload(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr);
static void asset_unload_dependents(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    int i;
    pg_mempool_id_t dep_id;
    /*  Delete all of the asset's dependents first  */
    ARR_FOREACH_REV(asset_ptr->dependents, dep_id, i) {
        asset_unload((pg_asset_handle_t){ asset_handle.mgr, dep_id },
                     pg_asset_get(&asset_handle.mgr->asset_pool, dep_id));
    }
}

/*  Destroy the loaded final object using the loader's 'unload' function pointer    */
static void asset_unload(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(!asset_ptr->data_loaded || !asset_ptr->loader->unload
    || (asset_ptr->source_type == PG_ASSET_SOURCE_DATA && !asset_ptr->own_data))
        return;
    asset_unload_dependents(asset_handle, asset_ptr);
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (---) Unloading final data          ");
    }
    asset_ptr->loader->unload(asset_handle, asset_ptr->loader, asset_ptr->loaded_data);
    asset_ptr->data_loaded = false;
}


/*  Fully delete an asset, including its handle and all its dependents  */
static void asset_delete(pg_asset_handle_t asset_handle, struct pg_asset* asset_ptr)
{
    if(!asset_ptr || !pg_asset_exists(asset_handle) || asset_ptr->deleted) return;
    asset_ptr->deleted = true;
    /*  Free all the internal resources */
    /*  Remove this asset from all dependencies' lists of dependents,
        deleting any (non-static) assets whose dependents lists become empty    */
    int i;
    pg_mempool_id_t dep_id;
    ARR_FOREACH_REV(asset_ptr->dependencies, dep_id, i) {
        struct pg_asset* dep_ptr = pg_asset_get(&asset_handle.mgr->asset_pool, dep_id);
        if(!dep_ptr || dep_ptr->deleted) continue;
        int j;
        pg_mempool_id_t dep_dep_id;
        ARR_FOREACH(dep_ptr->dependents, dep_dep_id, j) {
            if(dep_dep_id == asset_handle.id) {
                ARR_SWAPSPLICE(dep_ptr->dependents, j, 1);
                break;
            }
        }
        if(dep_ptr->dependents.len == 0) {
            asset_delete((pg_asset_handle_t){ asset_handle.mgr, dep_id },
                         pg_asset_get(&asset_handle.mgr->asset_pool, dep_id));
        }
    }
    ARR_DEINIT(asset_ptr->dependents);
    ARR_DEINIT(asset_ptr->dependencies);
    /*  Unload the asset in reverse order to loading it */
    asset_unload(asset_handle, asset_ptr);
    asset_unparse(asset_handle, asset_ptr);
    asset_unload_source(asset_handle, asset_ptr);
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, asset_handle,
        "    (   ) DELETED                       ");
    }
    pg_asset_free(&asset_handle.mgr->asset_pool, asset_handle.id);
}



/************************/
/*  PUBLIC INTERFACE    */
/************************/

/************************/
/*  ASSET MANAGER       */
/************************/

pg_asset_manager_t* pg_asset_manager_create(void)
{
    struct pg_asset_manager* asset_mgr = malloc(sizeof(*asset_mgr));
    pg_asset_pool_init(&asset_mgr->asset_pool);
    ARR_INIT(asset_mgr->groups);
    ARR_INIT(asset_mgr->deleted_groups);
    ARR_INIT(asset_mgr->loaders);
    ARR_INIT(asset_mgr->search_paths);
    ARR_INIT(asset_mgr->search_path_ptrs);
    HTABLE_INIT(asset_mgr->asset_table, 12);
    HTABLE_INIT(asset_mgr->group_table, 8);
    HTABLE_INIT(asset_mgr->loader_table, 4);
    HTABLE_INIT(asset_mgr->vars_table, 8);
    const char* default_search_paths[] = { "./" };
    pg_asset_manager_set_search_paths(asset_mgr, default_search_paths, sizeof(default_search_paths) / sizeof(const char*));
    pg_asset_manager_add_loader(asset_mgr, pg_json_asset_loader());
    return asset_mgr;
}



void pg_asset_manager_destroy(pg_asset_manager_t* asset_mgr)
{
    if(!asset_mgr) return;
    pg_asset_id_t* id_ptr;
    HTABLE_ITER iter;
    HTABLE_ITER_BEGIN(asset_mgr->asset_table, iter);
    while(!HTABLE_ITER_END(asset_mgr->asset_table, iter)) {
        HTABLE_ITER_NEXT_PTR(asset_mgr->asset_table, iter, id_ptr);
        struct pg_asset* asset_ptr = pg_asset_get(&asset_mgr->asset_pool, *id_ptr);
        asset_delete((pg_asset_handle_t){ asset_mgr, *id_ptr }, asset_ptr);
    }
    HTABLE_DEINIT(asset_mgr->asset_table);
    HTABLE_DEINIT(asset_mgr->group_table);
    HTABLE_DEINIT(asset_mgr->loader_table);
    HTABLE_DEINIT(asset_mgr->vars_table);
    int i;
    struct pg_asset_group* group_ptr;
    ARR_FOREACH_PTR(asset_mgr->groups, group_ptr, i) {
        ARR_DEINIT(group_ptr->members);
    }
    ARR_DEINIT(asset_mgr->search_path_ptrs);
    ARR_DEINIT(asset_mgr->search_paths);
    ARR_DEINIT(asset_mgr->groups);
    ARR_DEINIT(asset_mgr->deleted_groups);
    struct pg_asset_loader* loader;
    ARR_FOREACH(asset_mgr->loaders, loader, i) {
        if(loader->destroy) loader->destroy(loader->loader_data);
        free(loader);
    }
    ARR_DEINIT(asset_mgr->loaders);
        
    pg_asset_pool_deinit(&asset_mgr->asset_pool);
    free(asset_mgr);
}



int pg_asset_manager_add_search_path(pg_asset_manager_t* asset_mgr, const char* search_path)
{
    int new_path_idx = asset_mgr->search_paths.len;
    char* new_path_content = asset_manager_new_search_path(asset_mgr);
    strncpy(new_path_content, search_path, PG_FILE_PATH_MAXLEN);
    return new_path_idx;
}



void pg_asset_manager_set_search_paths(pg_asset_manager_t* asset_mgr, const char** search_paths, int n_search_paths)
{
    ARR_TRUNCATE_CLEAR(asset_mgr->search_paths, 0);
    ARR_TRUNCATE_CLEAR(asset_mgr->search_path_ptrs, 0);
    for(int i = 0; i < n_search_paths; ++i) {
        pg_asset_manager_add_search_path(asset_mgr, search_paths[i]);
    }
}



const char** pg_asset_manager_get_search_paths(pg_asset_manager_t* asset_mgr, int* n_search_paths_out)
{
    /*  Copy the count out  */
    if(n_search_paths_out) *n_search_paths_out = asset_mgr->search_paths.len;
    /*  We must rebuild the search_path_ptrs array to point into the search_paths array */
    ARR_RESERVE(asset_mgr->search_path_ptrs, asset_mgr->search_paths.len);
    asset_mgr->search_path_ptrs.len = asset_mgr->search_paths.len;
    for(int i = 0; i < asset_mgr->search_paths.len; ++i) {
        asset_mgr->search_path_ptrs.data[i] = asset_mgr->search_paths.data[i].path;
    }
    return (const char**)asset_mgr->search_path_ptrs.data;
}



bool pg_asset_manager_load_file(pg_asset_manager_t* asset_mgr, struct pg_file* file,
                                const char* filename, const char* mode)
{
    int n_search_paths = 0;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_mgr, &n_search_paths);
    return pg_file_open_search_paths(file, search_paths, n_search_paths, filename, mode);
}



void pg_asset_manager_add_loader(pg_asset_manager_t* asset_mgr, struct pg_asset_loader* asset_loader)
{
    if(!asset_mgr || !asset_loader) return;
    HTABLE_SET(asset_mgr->loader_table, asset_loader->type_name, asset_mgr->loaders.len);
    ARR_PUSH(asset_mgr->loaders, asset_loader);
}



struct pg_asset_loader* pg_asset_manager_get_type_loader(pg_asset_manager_t* asset_mgr,
                                                         const char* type)
{
    if(!type) return NULL;
    return get_type_loader(asset_mgr, type, strlen(type));
}


void pg_asset_manager_set_variable(pg_asset_manager_t* asset_mgr, const char* name, pg_data_t data)
{
    if(DEBUG_ASSETS) {
        pg_set_last_log_level(PG_LOG_DEBUG);
        char tmp[256] = {};
        pg_data_to_string(&data, tmp, 256);
        pg_log_direct(PG_LOG_CONTD, "    (++ ) Setting variable:              %s = %s\n", name, tmp);
    }
    HTABLE_SET(asset_mgr->vars_table, name, data);
}

pg_data_t* pg_asset_manager_get_variable(pg_asset_manager_t* asset_mgr, const char* name)
{
    pg_data_t* var_ptr = NULL;
    HTABLE_GET(asset_mgr->vars_table, name, var_ptr);
    if(!var_ptr) {
        pg_log(PG_LOG_ERROR, "Failed to get variable '%s' from asset manager", name);
        return NULL;
    }
    return var_ptr;
}




pg_asset_group_t pg_asset_manager_add_group(pg_asset_manager_t* asset_mgr, const char* name)
{
    pg_asset_group_t group;
    struct pg_asset_group* group_data;
    if(asset_mgr->deleted_groups.len) {
        group = ARR_POP(asset_mgr->deleted_groups);
        group_data = &asset_mgr->groups.data[group];
    } else {
        group = asset_mgr->groups.len;
        group_data = ARR_NEW(asset_mgr->groups);
    }
    strncpy(group_data->name, name, PG_ASSET_NAME_MAXLEN);
    ARR_INIT(group_data->members);
    HTABLE_SET(asset_mgr->group_table, name, group);
    return group;
}



pg_asset_group_t pg_asset_manager_get_group(pg_asset_manager_t* asset_mgr, const char* name)
{
    pg_asset_group_t* group_ptr = NULL;
    HTABLE_GET(asset_mgr->group_table, name, group_ptr);
    if(!group_ptr) return -1;
    return *group_ptr;
}



void pg_asset_manager_delete_group(pg_asset_manager_t* asset_mgr, pg_asset_group_t group)
{
    ARR_PUSH(asset_mgr->deleted_groups, group);
    struct pg_asset_group* group_data = &asset_mgr->groups.data[group];
    int i;
    pg_asset_id_t asset_id;
    ARR_FOREACH(group_data->members, asset_id, i) {
        struct pg_asset* asset_ptr = pg_asset_get(&asset_mgr->asset_pool, asset_id);
        HTABLE_UNSET(asset_mgr->asset_table, asset_ptr->name);
        asset_delete((pg_asset_handle_t){ asset_mgr, asset_id }, asset_ptr);
    };
    ARR_DEINIT(group_data->members);
    HTABLE_UNSET(asset_mgr->group_table, group_data->name);
}



/************************/
/*  ASSET CREATION      */
/************************/

pg_asset_handle_t pg_asset_declare(pg_asset_manager_t* asset_mgr, const char* type,
                                   const char* name)
{
    return declare_asset(asset_mgr, type, strlen(type), name, strlen(name), NULL, 0, NULL);
}

pg_asset_handle_t pg_asset_declare_child(pg_asset_handle_t parent, const char* type,
        const char* child_name)
{
    pg_asset_manager_t* asset_mgr = parent.mgr;
    char full_child_name[PG_ASSET_NAME_MAXLEN] = "";
    snprintf(full_child_name, PG_ASSET_NAME_MAXLEN, "%s.%s",
        pg_asset_get_name(parent), child_name);
    pg_asset_handle_t child_asset = pg_asset_declare(asset_mgr, type, full_child_name);
    pg_asset_depends(child_asset, parent);
    return child_asset;
}

pg_asset_handle_t pg_asset_get_child(pg_asset_handle_t parent, const char* child_name)
{
    pg_asset_manager_t* asset_mgr = parent.mgr;
    char full_child_name[PG_ASSET_NAME_MAXLEN] = "";
    snprintf(full_child_name, PG_ASSET_NAME_MAXLEN, "%s.%s",
        pg_asset_get_name(parent), child_name);
    pg_asset_handle_t child_asset = pg_asset_get_handle(asset_mgr, full_child_name);
    return child_asset;
}


pg_asset_handle_t pg_asset_from_data(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, void* data, bool own_data)
{
    struct pg_asset* ptr = NULL;
    int name_len = name ? strlen(name) : 0;
    pg_asset_handle_t handle = declare_asset(asset_mgr, type, strlen(type), name, name_len, NULL, 0, &ptr);
    if(!ptr) return handle;
    if(ptr->source_type != PG_ASSET_SOURCE_UNDEFINED) {
        pg_log(PG_LOG_ERROR, "Asset ('%s') redefined with raw data source", name);
        return (pg_asset_handle_t){0};
    }
    if(DEBUG_ASSETS) {
        pg_asset_log_reverse(PG_LOG_DEBUG, handle,
        "    (+++) Assigned final data           ");
    }
    ptr->source_loaded = true;
    ptr->source_type = PG_ASSET_SOURCE_DATA;
    ptr->source_path[0] = '\0';
    ptr->own_source = false;
    ptr->source_parsed = false;
    ptr->parsed_source = NULL;
    ptr->data_loaded = true;
    ptr->loaded_data = data;
    ptr->asset_loaded = true;
    ptr->own_data = own_data;
    return handle;
}


int pg_asset_multiple_from_json(pg_asset_manager_t* asset_mgr, const char* expect_type, cJSON* json,
        pg_asset_handle_t* handles_out, int max_handles)
{
    int n_assets = 0;
    cJSON* assets_json = cJSON_GetObjectItem(json, "__pg_assets");
    if(!cJSON_IsArray(assets_json)) {
        pg_log(PG_LOG_ERROR, "JSON multi-asset source requires '__pg_assets' array");
        return -1;
    }
    cJSON* asset_json;
    cJSON_ArrayForEach(asset_json, assets_json) {
        /*  Ignore arrays (use for comments or whatever */
        if(cJSON_IsArray(asset_json)) continue;
        cJSON* asset_type_json = cJSON_GetObjectItem(asset_json, "__pg_asset_type");
        /*  Check if we have a sub-group to include */
        cJSON* include_assets_json = cJSON_GetObjectItem(asset_json, "__pg_assets");
        if(include_assets_json) {
            int included_assets = 0;
            if(cJSON_IsString(include_assets_json)) {
                included_assets = pg_asset_multiple_from_file(asset_mgr,
                        cJSON_GetStringValue(asset_type_json),
                        cJSON_GetStringValue(include_assets_json),
                        handles_out, max_handles);
            } else if(cJSON_IsArray(include_assets_json)) {
                included_assets = pg_asset_multiple_from_json(asset_mgr,
                        cJSON_GetStringValue(asset_type_json),
                        include_assets_json,
                        handles_out, max_handles);
            } else {
                pg_log(PG_LOG_ERROR, "__pg_assets must be either a filename OR an array of asset definitions");
                return -1;
            }
            if(included_assets == -1) {
                pg_log(PG_LOG_ERROR, "Error while importing assets");
                return -1;
            }
            continue;
        }
        if(!expect_type && !asset_type_json) {
            pg_log(PG_LOG_ERROR, "Cannot create an asset from JSON without a known type");
            return -1;
        }
        /*  Otherwise we just parse an asset from this object   */
        pg_asset_handle_t new_asset = pg_asset_from_json(asset_mgr, expect_type, NULL, asset_json);
        if(!pg_asset_exists(new_asset)) return -1;
        if(handles_out && n_assets < max_handles) handles_out[n_assets] = new_asset;
        ++n_assets;
    }
    return n_assets;
}

int pg_asset_multiple_from_file(pg_asset_manager_t* asset_mgr, const char* expect_type, const char* filename,
        pg_asset_handle_t* handles_out, int max_handles)
{
    struct pg_file file;
    if(!pg_asset_manager_load_file(asset_mgr, &file, filename, "r")) return -1;
    int n_search_paths = 0;
    const char** search_paths = pg_asset_manager_get_search_paths(asset_mgr, &n_search_paths);

    /*  Temporarily add the asset's own path to the search paths while we are loading the asset */
    int tmp_search_path_idx = asset_mgr->search_paths.len;
    char* tmp_search_path = asset_manager_new_search_path(asset_mgr);
    strncpy(tmp_search_path, file.path, PG_FILE_PATH_MAXLEN);

    bool success = true;
    int n_assets = 0;
    cJSON* json = json_from_file_preprocessed(&file, search_paths, n_search_paths);
    pg_file_close(&file);
    if(!json) {
        pg_log(PG_LOG_ERROR, "Failed to parse JSON source for assets from file '%s'", filename);
        success = false;
    } else {
        n_assets = pg_asset_multiple_from_json(asset_mgr, expect_type, json, handles_out, max_handles);
        cJSON_Delete(json);
    }
    if(!success) return -1;

    /*  Remove the asset's path from the search paths   */
    ARR_SPLICE(asset_mgr->search_paths, tmp_search_path_idx, 1);
    return n_assets;
}


pg_asset_handle_t pg_asset_from_json(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, cJSON* json)
{
    struct pg_asset* ptr = NULL;
    if(!json) {
        pg_log(PG_LOG_ERROR, "Called with null JSON object");
        return (pg_asset_handle_t){0};
    }

    /*  Check if we have an "AID" string instead of a full JSON object  */
    if(cJSON_IsString(json)) {
        return pg_asset_from_AID(asset_mgr, json->valuestring, type);
    }

    /*  Manage the asset name, we can get it from the name argument here,
        or from the '__pg_asset_name' member of the JSON, or if neither is
        present, we can generate it from the type loader    */
    char name_buf[PG_ASSET_NAME_MAXLEN] = {0};
    int name_len = 0;
    if(!name || !name[0]) {
        cJSON* name_json = cJSON_GetObjectItem(json, "__pg_asset_name");
        if(!name_json) {
            generate_name(pg_asset_manager_get_type_loader(asset_mgr, type), name_buf);
        } else if(!cJSON_IsString(name_json)) {
            pg_log(PG_LOG_ERROR, "Asset ('%s') has invalid '_pg_asset_name' field");
            return (pg_asset_handle_t){0};
        } else {
            const char* name_json_str = name_json->valuestring;
            strncpy(name_buf, name_json_str, PG_ASSET_NAME_MAXLEN);
        }
    } else {
        strncpy(name_buf, name, PG_ASSET_NAME_MAXLEN);
    }
    name_len = strnlen(name_buf, PG_ASSET_NAME_MAXLEN);

    /*  We can get the type either from the 'type' argument, or the
        '__pg_asset_type' member of the JSON    */
    char type_buf[PG_ASSET_TYPE_NAME_MAXLEN];
    cJSON* type_json = cJSON_GetObjectItem(json, "__pg_asset_type");
    if(!type || !type[0]) {
        if(!type_json) {
            pg_log(PG_LOG_ERROR, "Asset ('%s') has no deducible type",
                    name_buf, type, cJSON_GetStringValue(type_json));
            return (pg_asset_handle_t){0};
        }
        type = cJSON_GetStringValue(type_json);
    } else if(type_json) {
        const char* type_json_str = cJSON_GetStringValue(type_json);
        if((!type || !type[0]) && strncmp(type_json_str, type, PG_ASSET_TYPE_NAME_MAXLEN) != 0) {
            pg_log(PG_LOG_ERROR, "Asset ('%s') expects type '%s', found '%s'",
                    name_buf, type, type_json_str);
            return (pg_asset_handle_t){0};
        }
        strncpy(type_buf, type_json_str, PG_ASSET_TYPE_NAME_MAXLEN);
    }
    int type_len = strnlen(type, PG_ASSET_TYPE_NAME_MAXLEN);

    /*  Finally declare the asset   */
    pg_asset_handle_t handle = declare_asset(asset_mgr, type, type_len, name_buf, name_len, NULL, 0, &ptr);
    if(!ptr) return handle;
    if(ptr->source_type != PG_ASSET_SOURCE_UNDEFINED) {
        pg_log(PG_LOG_ERROR, "Asset ('%s') redefined with raw JSON source", name);
        return (pg_asset_handle_t){0};
    }
    ptr->asset_loaded = true;
    ptr->source_loaded = true;
    ptr->source_type = PG_ASSET_SOURCE_JSON;
    ptr->source_path[0] = '\0';
    ptr->source_json = cJSON_Duplicate(json, true);
    ptr->own_source = true;

    asset_parse(handle, ptr);

    return handle;
}



pg_asset_handle_t pg_asset_from_file(pg_asset_manager_t* asset_mgr, const char* type,
                                     const char* name, const char* filename)
{
    return declare_asset(asset_mgr, type, strlen(type), name, strlen(name), filename, strlen(filename), NULL);
}



pg_asset_handle_t pg_asset_from_AID(pg_asset_manager_t* asset_mgr, const char* string, const char* expect_type)
{
    struct pg_asset* ptr = NULL;
    pg_asset_handle_t handle = declare_asset_string(asset_mgr, string, strlen(string), &ptr);
    if(!ptr) return handle;
    if(expect_type) {
        if(ptr->loader && ptr->loader->type_name[0]
        && (strncmp(ptr->loader->type_name, expect_type, PG_ASSET_TYPE_NAME_MAXLEN) != 0)) {
            pg_log(PG_LOG_ERROR, "Asset ('%s') declared with unexpected type (expected '%s', got '%s')",
                    string, expect_type, ptr->loader->type_name);
            pg_asset_delete(handle);
            return (pg_asset_handle_t){0};
        } else {
            struct pg_asset_loader* loader = pg_asset_manager_get_type_loader(asset_mgr, expect_type);
            if(!loader) {
                pg_log(PG_LOG_ERROR, "Asset ('%s') declared with unknown type '%s'", string, expect_type);
                pg_asset_delete(handle);
                return (pg_asset_handle_t){0};
            }
            ptr->loader = loader;
            if(ptr->source_path[0]) ptr->asset_loaded = true;
        }
    };
    return handle;
}



/************************/
/*  ASSET MANAGEMENT    */
/************************/

void pg_asset_depends(pg_asset_handle_t asset, pg_asset_handle_t dependency)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    struct pg_asset* dep_ptr = pg_asset_get(&asset.mgr->asset_pool, dependency.id);
    if(!ptr || !dep_ptr) {
        pg_log(PG_LOG_ERROR, "Cannot assign asset dependency when one or both assets do not exist");
        return;
    }
    if(asset.mgr != dependency.mgr) {
        pg_log(PG_LOG_ERROR, "Cannot assign asset dependencies from a different asset manager");
        return;
    }
    ARR_PUSH(ptr->dependencies, dependency.id);
    ARR_PUSH(dep_ptr->dependents, asset.id);
    if(DEBUG_ASSETS) {
        pg_set_last_log_level(PG_LOG_DEBUG);
        pg_log_direct(PG_LOG_CONTD, "    (++ ) Adding dependency              %s depends on %s\n",
                pg_asset_get_name(asset), pg_asset_get_name(dependency));
    }
}

bool pg_asset_dependency_from_json(pg_asset_handle_t dependent, pg_asset_handle_t* dependency_out,
        const char* type, const char* name, cJSON* json)
{
    pg_asset_handle_t new_asset = pg_asset_from_json(dependent.mgr, type, name, json);
    if(!pg_asset_exists(new_asset)) return false;
    pg_asset_depends(dependent, new_asset);
    if(dependency_out) *dependency_out = new_asset;
    return true;
}

void pg_asset_assign_group(pg_asset_handle_t asset, pg_asset_group_t group)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) return;
    ptr->group = group;
    ARR_PUSH(asset.mgr->groups.data[group].members, asset.id);
}

void pg_asset_static(pg_asset_handle_t asset, bool is_static)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) return;
    ptr->is_static = is_static;
}

void pg_asset_delete(pg_asset_handle_t asset)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    asset_delete(asset, ptr);
    HTABLE_UNSET(asset.mgr->asset_table, ptr->name);
}



/************************/
/*  ASSET INSPECTION    */
/************************/

bool pg_asset_exists(pg_asset_handle_t asset)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    return (ptr != NULL);
}

pg_asset_handle_t pg_asset_get_handle(pg_asset_manager_t* asset_mgr, const char* name)
{
    pg_asset_id_t* id = NULL;
    HTABLE_GET(asset_mgr->asset_table, name, id);
    if(!id) return (pg_asset_handle_t){0};
    return (pg_asset_handle_t){ .mgr = asset_mgr, .id = *id };
}

const char* pg_asset_get_name(pg_asset_handle_t asset)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) return "";
    return ptr->name;
}

cJSON* pg_asset_get_source(pg_asset_handle_t asset)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) return NULL;
    if(!ptr->source_loaded && !asset_load_source(asset, ptr)) return NULL;
    return ptr->source_json;
}

void* pg_asset_get_data(pg_asset_handle_t asset)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr || !ptr->asset_loaded) {
        pg_asset_log(PG_LOG_ERROR, asset, "got request for final data but the asset is incomplete");
        return NULL;
    }
    if(!ptr->data_loaded) asset_load(asset, ptr);
    ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr->loaded_data) {
        pg_asset_log(PG_LOG_ERROR, asset, "got request for final data but the data has not been loaded");
        return NULL;
    }
    return ptr->loaded_data;
}

void* pg_asset_get_data_by_name(pg_asset_manager_t* asset_mgr, const char* name)
{
    pg_asset_handle_t asset = pg_asset_get_handle(asset_mgr, name);
    if(!pg_asset_exists(asset)) {
        pg_log(PG_LOG_ERROR, "Tried to get data from non-existent asset '%s'", name);
        return NULL;
    }
    return pg_asset_get_data(pg_asset_get_handle(asset_mgr, name));
}



/************************/
/*  ASSET HELPERS       */
/************************/

void pg_asset_log_(enum pg_log_type log_level, int src_line, const char* src_file,
        pg_asset_handle_t asset, const char* fmt, ...)
{
    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) {
        pg_log_direct_(log_level, "    Asset <undefined> ", src_line, src_file,
                ptr->loader ? ptr->loader->type_name : "?", ptr->name, ptr->source_path);
    } else {
        pg_log_direct_(log_level, "    Asset '(%s)%s:%s' ", src_line, src_file,
                ptr->loader ? ptr->loader->type_name : "?", ptr->name, ptr->source_path);
    }
    va_list args;
    va_start(args, fmt);
    pg_vlog_direct_(PG_LOG_CONTD, fmt, src_line, src_file, args);
    va_end(args);
    pg_log_direct_(PG_LOG_CONTD, "\n", src_line, src_file);
}

void pg_asset_log_reverse_(enum pg_log_type log_level, int src_line, const char* src_file,
        pg_asset_handle_t asset, const char* fmt, ...)
{
    pg_set_last_log_level(log_level);
    va_list args;
    va_start(args, fmt);
    pg_vlog_direct_(PG_LOG_CONTD, fmt, src_line, src_file, args);
    va_end(args);

    struct pg_asset* ptr = pg_asset_get(&asset.mgr->asset_pool, asset.id);
    if(!ptr) {
        pg_log_direct_(PG_LOG_CONTD, " <undefined>", src_line, src_file,
                ptr->loader ? ptr->loader->type_name : "?", ptr->name, ptr->source_path);
    } else {
        pg_log_direct_(PG_LOG_CONTD, " %s (%s)", src_line, src_file,
                ptr->name, ptr->loader ? ptr->loader->type_name : "?");
    }
    pg_log_direct_(PG_LOG_CONTD, "\n", src_line, src_file);
}
