#include "munit.h"

/*  Defined in the .c files next to this main.c */
extern MunitSuite pg_inline_tests;
extern MunitSuite pg_text_utility_tests;
extern MunitSuite pg_file_utility_tests;
extern MunitSuite pg_asset_tests;
extern MunitSuite pg_shader_tests;
extern MunitSuite pg_buffer_tests;
extern MunitSuite pg_texture_tests;
extern MunitSuite pg_framebuffer_tests;
extern MunitSuite pg_command_buffer_tests;
extern MunitSuite pg_pipeline_tests;
extern MunitSuite pg_renderer_tests;

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
    MunitSuite procgame_tests = {
        .prefix = "procgame",
        .suites = (MunitSuite[]){
            pg_inline_tests,
            pg_text_utility_tests,
            pg_file_utility_tests,
            pg_asset_tests,
            pg_shader_tests,
            pg_buffer_tests,
            pg_texture_tests,
            pg_framebuffer_tests,
            pg_command_buffer_tests,
            //pg_pipeline_tests,
            //pg_renderer_tests,
            {0},
        }
    };

    return munit_suite_main(&procgame_tests, NULL, argc, argv);
}

