#include "munit.h"
#include "pg_core/inline.h"

static MunitResult test_pg_pointers(const MunitParameter params[], void* data)
{
    munit_assert_int(PG_POINTERS(int, 3, 1, 4, 1, 5, 9)[2], ==, 4);
    munit_assert_string_equal(PG_POINTERS(const char*, "hello", "goodbye", "foobar")[1], "goodbye");
    return MUNIT_OK;
}

static MunitResult test_pg_least_sig_bit(const MunitParameter params[], void* data)
{
    munit_assert_uint(PG_LEAST_SIGNIFICANT_BIT(0b1000), ==, 3);
    munit_assert_uint(PG_LEAST_SIGNIFICANT_BIT(0b10010100000), ==, 5);
    munit_assert_uint(PG_LEAST_SIGNIFICANT_BIT(0), ==, 0);
    munit_assert_uint(PG_LEAST_SIGNIFICANT_BIT(1), ==, 0);
    munit_assert_uint(PG_LEAST_SIGNIFICANT_BIT(2), ==, 1);
    return MUNIT_OK;
}

static MunitResult test_pg_bits_popcount(const MunitParameter params[], void* data)
{
    munit_assert_uint(PG_POPCOUNT(0b10101010), ==, 4);
    munit_assert_uint(PG_POPCOUNT(0b11111111), ==, 8);
    munit_assert_uint(PG_POPCOUNT(0xFFFFFFFF), ==, 32);
    munit_assert_uint(PG_POPCOUNT(0xFFFF), ==, 16);
    return MUNIT_OK;
}

/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_inline_tests = {
    .prefix = "/inline",
    .tests = (MunitTest[]) {
        {   .name = "/pg_pointers",
            .test = test_pg_pointers },
        {   .name = "/pg_least_significant_bit",
            .test = test_pg_least_sig_bit },
        {   .name = "/pg_bits_popcount",
            .test = test_pg_bits_popcount },
    },
};

