#version 450

const vec2 quad_verts[6] = vec2[](
    vec2(-1,  1),
    vec2(-1, -1),
    vec2( 1,  1),
    vec2( 1,  1),
    vec2(-1, -1),
    vec2( 1, -1)
);

const ivec2 quad_uv[6] = ivec2[](
    ivec2(0, 1),
    ivec2(0, 0),
    ivec2(1, 1),
    ivec2(1, 1),
    ivec2(0, 0),
    ivec2(1, 0)
);


layout(location = 0) uniform samplerBuffer instance_buffer;

layout(location = 0) out vec3 f_tex_coord;

void main()
{
    /*  Retrieve raw data from the buffer texture   */
    int buf_offset = gl_VertexID / 6 * 3;
    vec4 instance[3] = vec4[](
        vec4(texelFetch(instance_buffer, 0 + buf_offset)),
        vec4(texelFetch(instance_buffer, 1 + buf_offset)),
        vec4(texelFetch(instance_buffer, 2 + buf_offset))
    );

    /*  Unpack the raw data into our real instance data */
    vec2 tex_corner[2] = vec2[](vec2(instance[0].xy), vec2(instance[0].zw));
    vec3 pos = instance[1].xyz;
    float tex_layer = instance[1].w;
    vec2 scale = instance[2].xy;
    vec2 padding = instance[2].zw;

    /*  Construct our base vertex   */
    vec2 vert = quad_verts[gl_VertexID % 6];
    ivec2 vert_uv = quad_uv[gl_VertexID % 6];

    /*  Output base vertex transformed by instance data */
    f_tex_coord = vec3(
        tex_corner[vert_uv.x].x,
        tex_corner[vert_uv.y].y,
        tex_layer
    );
    gl_Position = vec4((vert.xy * scale) + pos.xy, pos.z, 1);
}

