#version 450


/************************/
/*  Material inputs     */
/************************/

struct material {
    vec4 diffuse_map_rect;
    int diffuse_map_layer;
};

layout(std140, binding = 3) uniform material_buffer {
    material material_slots[16];
};


/************************/
/*  Light inputs        */
/************************/

struct light {
    vec3 position;
    float radius;
    vec3 color;
    float brightness;
};

layout(std140, binding = 4) uniform light_buffer {
    light lights[4];
};


/************************/
/*  Texture inputs      */
/************************/

layout(location = 0) uniform sampler2DArray tex_sampler;


/****************************************/
/*  Vertex input (from vertex shader)   */
/****************************************/

in vec3 f_position_ws;
in vec3 f_normal;
in vec3 f_diffuse_coord;


/************************/
/*  Color output        */
/************************/

layout(location = 0) out vec4 output_color;

void main()
{
    /*  Get diffuse color   */
    vec4 diffuse_map_color = texture(tex_sampler, f_diffuse_coord);

    /*  Get shading value   */
    vec3 shading = vec3(0);
    for(int i = 0; i < 4; ++i) {
        vec3 pos_to_light = f_position_ws - lights[i].position;
        float dist_to_light = length(pos_to_light);
        vec3 norm_to_light = normalize(-pos_to_light);
        float angle_to_light = max(0, dot(norm_to_light, f_normal));
        float light_attenuation = 1 - clamp(dist_to_light / lights[i].radius, 0, 1);
        shading += lights[i].color * angle_to_light * light_attenuation;
    }
    
    /*  Output diffuse color transformed by shading */
    output_color = vec4(diffuse_map_color.rgb * shading.rgb, diffuse_map_color.a);
}

