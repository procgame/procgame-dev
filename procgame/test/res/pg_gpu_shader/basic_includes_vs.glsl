#version 450

#pg_include "util.glsl"

layout(location = 0) in vec3 position;

void main()
{
    gl_Position = vec4(test_included_func(position.xyz), 1);
}

