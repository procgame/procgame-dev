#version 450

layout(location = 1) uniform sampler2DArray tex_sampler;

layout(location = 0) in vec3 f_tex_coord;

layout(location = 0) out vec4 output_color;

void main()
{
    output_color = texture(tex_sampler, f_tex_coord);
}


