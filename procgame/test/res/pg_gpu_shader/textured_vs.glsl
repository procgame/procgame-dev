#version 450

in vec3 position;
in vec3 tex_coord;

out vec3 f_tex_coord;

void main()
{
    f_tex_coord = tex_coord;
    gl_Position = vec4(position.xyz, 1);
}

