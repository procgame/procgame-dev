#version 450

layout(location = 0) uniform sampler2DArray tex_diffuse;

layout(location = 0) out vec4 output_color;

void main()
{
    output_color = vec4(texture(tex_diffuse, vec3(0,0,0)).rgb, 1);
}


