#version 450

struct color_data {
    vec4 diffuse_color;
};

layout(std140, binding = 0) uniform color_palette {
    color_data color_slots[16];
};

in vec3 position;
in int color_idx;

out vec4 f_diffuse_color;

void main()
{
    /*  Output base vertex transformed by instance data */
    f_diffuse_color = color_slots[color_idx].diffuse_color;
    gl_Position = vec4(position.xyz, 1);
}


