#version 450

in vec4 f_diffuse_color;

layout(location = 0) out vec4 output_color;

void main()
{
    output_color = f_diffuse_color;
}
