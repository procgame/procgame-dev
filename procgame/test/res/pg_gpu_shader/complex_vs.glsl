#version 450


/************************/
/*  Material inputs     */
/************************/

struct material {
    vec4 diffuse_map_rect;
    int diffuse_map_layer;
};

layout(std140, binding = 0) uniform material_buffer {
    material material_slots[16];
};


/************************/
/*  Viewport inputs     */
/************************/

struct viewport {
    mat4 projection;
    mat4 view;
    mat4 projview;
    mat4 projview_inverse;
};

layout(std140, binding = 1) uniform viewport_buffer {
    viewport view;
};


/************************/
/*  Instance inputs     */
/************************/

struct instance {
    mat4 transform;
};

layout(std140, binding = 2) uniform instance_buffer {
    instance instances[64];
};


/************************/
/*  Vertex inputs       */
/************************/

in vec3 normal;
in vec3 position;
in vec3 uv_mat;


/************************/
/*  Output              */
/************************/

out vec3 f_normal;
out vec3 f_position_ws;
out vec3 f_diffuse_coord;

void main()
{
    /*  Texture output  */
    vec4 diffuse_rect = material_slots[int(uv_mat.z)].diffuse_map_rect;
    int diffuse_layer = material_slots[int(uv_mat.z)].diffuse_map_layer;
    vec2 diffuse_coord = mix(diffuse_rect.xy, diffuse_rect.zw, uv_mat.xy);
    //vec2 diffuse_coord = uv_mat.xy;
    f_diffuse_coord = vec3(diffuse_coord.xy, uv_mat.z);

    /*  Normal vector output    */
    f_normal = mat3(instances[gl_InstanceID].transform) * normal;

    /*  World-space position output */
    vec4 position_ws = instances[gl_InstanceID].transform * vec4(position.xyz, 1);
    f_position_ws = position_ws.xyz;

    /*  GL position output */
    gl_Position = view.projview * position_ws;
}

