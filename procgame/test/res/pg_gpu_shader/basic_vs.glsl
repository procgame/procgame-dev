#version 450

struct viewport_t {
    mat4 view_matrix;
    mat4 projection_matrix;
    mat4 projectionview_matrix;
    mat4 inverse_view_matrix;
    mat4 inverse_projection_matrix;
    mat4 inverse_projectionview_matrix;
    float near_plane;
    float far_plane;
};

layout(binding = 0, std140) uniform viewport_u {
    viewport_t viewport;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 uv_mat;

layout(location = 0) out vec3 f_uv_mat;

void main()
{
    f_uv_mat = uv_mat;
    gl_Position = viewport.projectionview_matrix * vec4(position.xyz, 1);
}
