#version 450

struct instance_t {
    vec3 position;
};

layout(binding = 0, std140) uniform instances_u {
    instance_t instances[128];
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 uv_mat;

layout(location = 0) out vec3 f_uv_mat;

void main()
{
    f_uv_mat = uv_mat;
    gl_Position = vec4((position.xyz + instances[gl_InstanceID].position.xyz), 1);
}
