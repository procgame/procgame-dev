#version 450

struct instance_t {
    vec3 position;
};

layout(binding = 0, std140) uniform instances_u {
    instance_t instances[128];
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 uv_mat;

layout(location = 0) out vec2 f_quad_uv;

void main()
{
    f_quad_uv = position.xy + 0.5;
    gl_Position = vec4(((position.xyz + vec3(0.5,0.5,0)) + instances[gl_InstanceID].position.xyz), 1);
}
