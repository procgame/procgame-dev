#version 450

layout(location = 0) in vec3 uv_mat;

layout(location = 0) out vec4 output_color;

in vec2 f_quad_uv;

void main()
{
    output_color = vec4(f_quad_uv.xy, 1, 1);
}


