#define PG_TEST_REFERENCE_IMAGE_PATH "res/correct_renders/"
#define PG_TEST_OUTPUT_IMAGE_PATH "./test_renders/"

void* test_setup_video(const MunitParameter params[], void* user_data);
void test_tear_down_video(void* fixture);
bool test_image_regression(const char* test_name, struct pg_texture* test_output);
bool test_image_regression_gpu(const char* test_name, pg_gpu_texture_t* gpu_test_output);
bool test_image_regression_asset(const char* test_name, pg_asset_manager_t* asset_mgr);

