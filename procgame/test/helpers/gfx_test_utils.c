#include <stdio.h>
#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "pg_modules/window.h"
#include "pg_modules/gpu.h"

#include "gfx_test_utils.h"


void* test_setup_video(const MunitParameter params[], void* user_data)
{
    pg_log_file(stderr);
    pg_log_level(PG_LOG_DEBUG_PROCGAME);
    pg_init_window(ivec2(512,512));
    pg_init_gpu();
    pg_window_swap();
    return NULL;
}

void test_tear_down_video(void* fixture)
{
    pg_window_swap();
    pg_deinit_gpu();
    pg_deinit_window();
}

bool test_image_regression(const char* test_name, struct pg_texture* test_output)
{
    struct pg_texture draw_reference;
    char reference_path[PG_FILE_PATH_MAXLEN] = {0};
    char output_path[PG_FILE_PATH_MAXLEN] = {0};
    snprintf(reference_path, PG_FILE_PATH_MAXLEN, PG_TEST_REFERENCE_IMAGE_PATH "%s.png", test_name);
    snprintf(output_path, PG_FILE_PATH_MAXLEN, PG_TEST_OUTPUT_IMAGE_PATH "%s.png", test_name);
    pg_texture_init_from_path(&draw_reference, reference_path);
    if(!pg_texture_save_to_file(test_output, output_path)) return false;
    return pg_texture_compare_equal(&draw_reference, test_output);
}

bool test_image_regression_gpu(const char* test_name, pg_gpu_texture_t* gpu_test_output)
{
    struct pg_texture test_output;
    pg_gpu_texture_download(gpu_test_output, &test_output);
    return test_image_regression(test_name, &test_output);
}

bool test_image_regression_asset(const char* test_name, pg_asset_manager_t* asset_mgr)
{
    pg_asset_handle_t output_tex_asset = pg_asset_get_handle(asset_mgr, "pg_test_output");
    pg_gpu_texture_t* gpu_test_output = pg_asset_get_data(output_tex_asset);
    if(!gpu_test_output) return false;
    return test_image_regression_gpu(test_name, gpu_test_output);
}

