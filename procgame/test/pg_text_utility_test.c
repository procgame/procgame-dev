#include <stdio.h>
#include "munit.h"
#include "pg_core/htable.h"
#include "pg_core/arr.h"
#include "pg_util/text_utility.h"

#define TEST_STRINGS(prefix, ...) \
    const char* prefix[] = { __VA_ARGS__ }; \
    const int prefix##_count = sizeof(prefix) / sizeof(prefix[0]); \
    int prefix##_len[prefix##_count]; \
    for(int prefix##_i = 0; prefix##_i < prefix##_count; ++prefix##_i) { \
        prefix##_len[prefix##_i] = strlen(prefix[prefix##_i]); \
    }

#define TEST_INTS(prefix, ...) \
    int prefix[] = { __VA_ARGS__ }; \
    const int prefix##_count = sizeof(prefix) / sizeof(prefix[0]); 


static MunitResult test_str_find(const MunitParameter params[], void* data)
{
    const char test_str[] = "Hållo, World | Goódbye, World";
    const int test_str_len = sizeof(test_str) / sizeof(test_str[0]);
    TEST_STRINGS(search, "Hållo", "World", "|");
    TEST_INTS(expect, 0, 8, 14);
    for(int i = 0; i < search_count; ++i) {
        int result = pg_str_find(test_str, test_str_len, search[i], search_len[i]);
        fprintf(stderr, "Searching for string \"%s\" in \"%s\"\n", search[i], test_str);
        munit_assert_int(result, ==, expect[i]);
    }
    return MUNIT_OK;
}


#define TOKENIZER_TEST_SETUP(SAMPLE, SEPARATORS, ...) \
    const char sample[] = (SAMPLE); \
    const int sample_len = strnlen(sample, sizeof(sample)); \
    const char separators[] = (SEPARATORS); \
    const int separators_len = strlen(separators); \
    struct pg_text_tokenizer test_tokenizer = PG_TEXT_TOKENIZER(sample, sample_len, separators, separators_len);

static MunitResult test_tokenize_1sep_2tok(const MunitParameter params[], void* data)
{
    TOKENIZER_TEST_SETUP("Hello, World | Goodbye, World", "|");
    TEST_STRINGS(expect, "Hello, World ", " Goodbye, World");
    fprintf(stderr, "Expect %d tokens\n", expect_count);
    int i = 0;
    while(pg_text_tokenizer_next(&test_tokenizer)) {
        fprintf(stderr, "%d: \"%.*s\"\n", test_tokenizer.token_idx,
                test_tokenizer.token_len, test_tokenizer.token);
        munit_assert_int(test_tokenizer.token_idx, ==, i);
        munit_assert_int(i, <, expect_count);
        munit_assert_int(test_tokenizer.token_len, ==, expect_len[i]);
        const char* calculated_token = test_tokenizer.token;
        const char* expected_token = expect[test_tokenizer.token_idx];
        munit_assert_substring_equal(test_tokenizer.token_len, calculated_token, expected_token);
        ++i;
    }
    return MUNIT_OK;
}

static MunitResult test_tokenize_3sep_4tok(const MunitParameter params[], void* data)
{
    TOKENIZER_TEST_SETUP("Hello, World | Goodbye, World", ", |");
    TEST_STRINGS(expect, "Hello", "World", "Goodbye", "World");
    fprintf(stderr, "Expect %d tokens\n", expect_count);
    while(pg_text_tokenizer_next(&test_tokenizer)) {
        fprintf(stderr, "%d: \"%.*s\"\n", test_tokenizer.token_idx,
                test_tokenizer.token_len, test_tokenizer.token);
        munit_assert_int(test_tokenizer.token_idx, <, expect_count);
        const char* calculated_token = test_tokenizer.token;
        const char* expected_token = expect[test_tokenizer.token_idx];
        munit_assert_substring_equal(test_tokenizer.token_len, calculated_token, expected_token);
    }
    return MUNIT_OK;
}

static MunitResult test_tokenize_token_start(const MunitParameter params[], void* data)
{
    TOKENIZER_TEST_SETUP("Hello, World | Goodbye, World", ", |");
    TEST_INTS(expect, 0, 7, 15, 24);
    fprintf(stderr, "Expect %d tokens\n", expect_count);
    while(pg_text_tokenizer_next(&test_tokenizer)) {
        fprintf(stderr, "%d: \"%.*s\"\n", test_tokenizer.token_idx,
                test_tokenizer.token_len, test_tokenizer.token);
        munit_assert_int(test_tokenizer.token_idx, <, expect_count);
        int expected_offset = expect[test_tokenizer.token_idx];
        int calculated_offset = test_tokenizer.token_start;
        munit_assert_int(expected_offset, ==, calculated_offset);
    }
    return MUNIT_OK;
}

static MunitResult test_tokenize_token_end(const MunitParameter params[], void* data)
{
    TOKENIZER_TEST_SETUP("Hello, World | Goodbye, World", ", |");
    TEST_INTS(expect, 4, 11, 21, 28);
    while(pg_text_tokenizer_next(&test_tokenizer)) {
        fprintf(stderr, "%d: \"%.*s\"\n", test_tokenizer.token_idx,
                test_tokenizer.token_len, test_tokenizer.token);
        munit_assert_int(test_tokenizer.token_idx, <, expect_count);
        int expected_offset = expect[test_tokenizer.token_idx];
        int calculated_offset = test_tokenizer.token_end;
        munit_assert_int(expected_offset, ==, calculated_offset);
    }
    return MUNIT_OK;
}

#undef TOKENIZER_TEST_SETUP

#define VARIATOR_BEGIN_TOKEN   "#var"
#define VARIATOR_END_TOKEN     "#endvar"

static MunitResult test_variator_init(const MunitParameter params[], void* data)
{
    TEST_STRINGS(source,
        ("Hello, World!\n"
        VARIATOR_BEGIN_TOKEN " Spanish\n"
        "¡Göödbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        VARIATOR_BEGIN_TOKEN " English\n"
        "Goodbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        "Test!")
    );
    TEST_INTS(expected_var_count, 2);
    struct pg_text_variator test_var;
    pg_text_variator_init(&test_var, source[0], source_len[0], VARIATOR_BEGIN_TOKEN, VARIATOR_END_TOKEN);
    munit_assert_int(test_var.str_len, ==, source_len[0]);
    munit_assert_int(test_var.variants.len, ==, expected_var_count[0]);
    return MUNIT_OK;
}


static MunitResult test_variator_count(const MunitParameter params[], void* data)
{
    TEST_STRINGS(source,
        ("Hello, World!\n"
        VARIATOR_BEGIN_TOKEN " Spanish\n"
        "¡Göödbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        VARIATOR_BEGIN_TOKEN " English\n"
        "Goodbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        "Test!")
    );
    struct pg_text_variator test_var;
    pg_text_variator_init(&test_var, source[0], source_len[0], VARIATOR_BEGIN_TOKEN, VARIATOR_END_TOKEN);
    int variants_count = pg_text_variator_count(&test_var);
    int expected_count = 2;
    munit_assert_int(variants_count, ==, expected_count);
    return MUNIT_OK;
}

static MunitResult test_variator_variant_name(const MunitParameter params[], void* data)
{
    TEST_STRINGS(source,
        ("Hello, World!\n"
        VARIATOR_BEGIN_TOKEN " Spanish\n"
        "¡Göödbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        VARIATOR_BEGIN_TOKEN " English\n"
        "Goodbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        "Test!")
    );
    TEST_STRINGS(expect_names, "Spanish", "English");
    struct pg_text_variator test_var;
    pg_text_variator_init(&test_var, source[0], source_len[0], VARIATOR_BEGIN_TOKEN, VARIATOR_END_TOKEN);
    for(int i = 0; i < expect_names_count; ++i) {
        const char* variant_name = pg_text_variator_name(&test_var, i);
        munit_assert_string_equal(variant_name, expect_names[i]);
    }
    return MUNIT_OK;
}


static MunitResult test_variator_generate(const MunitParameter params[], void* data)
{
    TEST_STRINGS(source,
        ("Hello, World!\n"
        VARIATOR_BEGIN_TOKEN " Spanish\n"
        "¡Göödbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        VARIATOR_BEGIN_TOKEN " English\n"
        "Goodbye, World!\n"
        VARIATOR_END_TOKEN "\n"
        "Test!")
    );
    TEST_STRINGS(expect,
        ("Hello, World!\n"
         "¡Göödbye, World!\n"
         "Test!"),
        ("Hello, World!\n"
         "Goodbye, World!\n"
         "Test!")
    );
    TEST_STRINGS(variants, "Spanish", "English");
    struct pg_text_variator test_var;
    pg_text_variator_init(&test_var, source[0], source_len[0], VARIATOR_BEGIN_TOKEN, VARIATOR_END_TOKEN);
    char buffer[source_len[0]];
    for(int i = 0; i < test_var.variants.len; ++i) {
        pg_text_variator_generate(&test_var,
                buffer, source_len[0],
                variants + i, 1);
        munit_assert_string_equal(buffer, expect[i]);
    }
    return MUNIT_OK;
}

#undef VARIATOR_BEGIN_TOKEN
#undef VARIATOR_END_TOKEN

MunitSuite pg_text_utility_tests = {
    .prefix = "/util/text",
    .suites = (MunitSuite[]) {
        {   .prefix = "/str",
            .tests = (MunitTest[]) {
                {   .name = "/find",
                    .test = test_str_find },
                {0} } },
        {   .prefix = "/tokenizer",
            .tests = (MunitTest[]) {
                {   .name = "/1sep_2tok",
                    .test = test_tokenize_1sep_2tok },
                {   .name = "/3sep_4tok",
                    .test = test_tokenize_3sep_4tok },
                {   .name = "/token_start",
                    .test = test_tokenize_token_start },
                {   .name = "/token_end",
                    .test = test_tokenize_token_end },
                {0} } },
        {   .prefix = "/variator",
            .tests = (MunitTest[]) {
                {   .name = "/init",
                    .test = test_variator_init },
                {   .name = "/count",
                    .test = test_variator_count },
                {   .name = "/variant_name",
                    .test = test_variator_variant_name },
                {   .name = "/generate",
                    .test = test_variator_generate },
                {0} } },
        {0}
    }
};

