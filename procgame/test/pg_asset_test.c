#include "munit.h"

MunitSuite pg_asset_tests = {
    .prefix = "/util/asset",
    .tests = (MunitTest[]) { {} },
};
//
//#include "pg_util/asset.h"
//#include "pg_util/json_utility.h"
//
//struct numbers_asset_data {
//    int n;
//    int* numbers;
//};
//
//struct numbers_asset_source {
//
//
//static void* numbers_asset_load(pg_asset_handle_t asset_handle, cJSON* def, void* udata);
//static bool numbers_asset_load_deps(pg_asset_handle_t asset_handle, cJSON* def, void* udata);
//static void numbers_asset_unload(pg_asset_handle_t asset_handle, void* asset_data, void* udata);
//
//
//static MunitResult test_asset_manager_create(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    munit_assert_not_null(asset_mgr);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_manager_declare_type(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    struct pg_asset_loader* loader_ptr = pg_asset_manager_get_type_loader(asset_mgr, "numbers");
//    munit_assert_ptr(loader_ptr, ==, &numbers_loader);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_declare(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_declare(asset_mgr, "numbers", "foo");
//    const char* asset_name = pg_asset_get_name(asset);
//    munit_assert_string_equal(asset_name, "foo");
//    munit_assert_ptr(asset.mgr, ==, asset_mgr);
//    munit_assert_uint64(asset.id, !=, 0);
//    munit_assert_true(pg_asset_exists(asset));
//    void* asset_data = pg_asset_get_data(asset);
//    munit_assert_null(asset_data);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_from_json(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    cJSON* test_json = load_json("./res/pg_asset/numbers/numbers_asset.json");
//    pg_asset_handle_t asset = pg_asset_from_json(asset_mgr, "numbers", "foo", test_json);
//    const char* asset_name = pg_asset_get_name(asset);
//    munit_assert_string_equal(asset_name, "foo");
//    munit_assert_ptr(asset.mgr, ==, asset_mgr);
//    munit_assert_uint64(asset.id, !=, 0);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_not_null(asset_data);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_from_file(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_from_file(asset_mgr, "numbers", "foo", "numbers/numbers_asset.json");
//    const char* asset_name = pg_asset_get_name(asset);
//    munit_assert_string_equal(asset_name, "foo");
//    munit_assert_ptr(asset.mgr, ==, asset_mgr);
//    munit_assert_uint64(asset.id, !=, 0);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_not_null(asset_data);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_from_AID(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(numbers)foo:numbers/numbers_asset.json", NULL);
//    pg_asset_handle_t asset2 = pg_asset_from_AID(asset_mgr, "foo2:numbers/numbers_asset.json", "numbers");
//    const char* asset_name = pg_asset_get_name(asset);
//    munit_assert_string_equal(asset_name, "foo");
//    const char* asset2_name = pg_asset_get_name(asset2);
//    munit_assert_string_equal(asset2_name, "foo2");
//    munit_assert_ptr(asset.mgr, ==, asset_mgr);
//    munit_assert_uint64(asset.id, !=, 0);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_not_null(asset_data);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_not_null(asset_data);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_from_data(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    struct numbers_asset_data source_data = { .n = 4, .numbers = (int[]){ 420, 69, -1, 1312 }, };
//    pg_asset_handle_t asset = pg_asset_from_data(asset_mgr, "numbers", "foo", &source_data);
//    munit_assert_ptr(asset.mgr, ==, asset_mgr);
//    munit_assert_uint64(asset.id, !=, 0);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_not_null(asset_data);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_get_handle(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    struct numbers_asset_data source_data = { .n = 4, .numbers = (int[]){ 420, 69, -1, 1312 }, };
//    pg_asset_handle_t asset = pg_asset_from_data(asset_mgr, "numbers", "foo", &source_data);
//    pg_asset_handle_t q_asset = pg_asset_get_handle(asset_mgr, "foo");
//    munit_assert_ptr(asset.mgr, ==, q_asset.mgr);
//    munit_assert_uint64(asset.id, ==, q_asset.id);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_get_data_by_name(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    struct numbers_asset_data source_data = { .n = 4, .numbers = (int[]){ 420, 69, -1, 1312 }, };
//    pg_asset_handle_t asset = pg_asset_from_data(asset_mgr, "numbers", "foo", &source_data);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_int(asset_data->numbers[0], ==, 420);
//    munit_assert_int(asset_data->numbers[1], ==, 69);
//    munit_assert_int(asset_data->numbers[2], ==, -1);
//    munit_assert_int(asset_data->numbers[3], ==, 1312);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_manager_create_group(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_group_t group0 = pg_asset_manager_add_group(asset_mgr, "Foo");
//    pg_asset_group_t group1 = pg_asset_manager_add_group(asset_mgr, "Bar");
//    munit_assert_int(group0, !=, group1);
//    pg_asset_manager_delete_group(asset_mgr, group0);
//    pg_asset_manager_delete_group(asset_mgr, group1);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_assign_group(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_group_t group = pg_asset_manager_add_group(asset_mgr, "TestGroup");
//    pg_asset_handle_t asset = pg_asset_from_file(asset_mgr, "numbers", "foo", "numbers/numbers_asset.json");
//    pg_asset_assign_group(asset, group);
//    pg_asset_manager_delete_group(asset_mgr, group);
//    munit_assert_false(pg_asset_exists(asset));
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_dependencies_manual(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .load_deps = numbers_asset_load_deps, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(numbers)foo:numbers/numbers_asset_deps.json", NULL);
//    pg_asset_handle_t asset_header = pg_asset_from_AID(asset_mgr, "(numbers)test_header:numbers/numbers_asset_header.json", NULL);
//    pg_asset_handle_t asset_footer = pg_asset_from_AID(asset_mgr, "(numbers)test_footer:numbers/numbers_asset_footer.json", NULL);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_int(asset_data->n, ==, 10);
//    munit_assert_int(asset_data->numbers[0], ==, 0);
//    munit_assert_int(asset_data->numbers[1], ==, 1);
//    munit_assert_int(asset_data->numbers[2], ==, 2);
//    munit_assert_int(asset_data->numbers[3], ==, 420);
//    munit_assert_int(asset_data->numbers[4], ==, 69);
//    munit_assert_int(asset_data->numbers[5], ==, -1);
//    munit_assert_int(asset_data->numbers[6], ==, 1312);
//    munit_assert_int(asset_data->numbers[7], ==, 3);
//    munit_assert_int(asset_data->numbers[8], ==, 4);
//    munit_assert_int(asset_data->numbers[9], ==, 5);
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_dependencies_auto(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .load_deps = numbers_asset_load_deps, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(numbers)foo:numbers/numbers_asset_dep_paths.json", NULL);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    munit_assert_int(asset_data->n, ==, 10);
//    munit_assert_int(asset_data->numbers[0], ==, 0);
//    munit_assert_int(asset_data->numbers[1], ==, 1);
//    munit_assert_int(asset_data->numbers[2], ==, 2);
//    munit_assert_int(asset_data->numbers[3], ==, 420);
//    munit_assert_int(asset_data->numbers[4], ==, 69);
//    munit_assert_int(asset_data->numbers[5], ==, -1);
//    munit_assert_int(asset_data->numbers[6], ==, 1312);
//    munit_assert_int(asset_data->numbers[7], ==, 3);
//    munit_assert_int(asset_data->numbers[8], ==, 4);
//    munit_assert_int(asset_data->numbers[9], ==, 5);
//    pg_asset_handle_t asset_header = pg_asset_get_handle(asset_mgr, "test_header");
//    pg_asset_handle_t asset_footer = pg_asset_get_handle(asset_mgr, "test_footer");
//    munit_assert(pg_asset_exists(asset_header));
//    munit_assert(pg_asset_exists(asset_footer));
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//static MunitResult test_asset_dependencies_delete(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/");
//    struct pg_asset_loader numbers_loader = {
//        .type_name = "numbers",
//        .load = numbers_asset_load, .load_deps = numbers_asset_load_deps, .unload = numbers_asset_unload, };
//    pg_asset_manager_add_loader(asset_mgr, &numbers_loader);
//    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(numbers)foo:numbers/numbers_asset_dep_paths.json", NULL);
//    struct numbers_asset_data* asset_data = (struct numbers_asset_data*)pg_asset_get_data(asset);
//    pg_asset_handle_t asset_header = pg_asset_get_handle(asset_mgr, "test_header");
//    pg_asset_handle_t asset_footer = pg_asset_get_handle(asset_mgr, "test_footer");
//    munit_assert(pg_asset_exists(asset_header));
//    munit_assert(pg_asset_exists(asset_footer));
//    pg_asset_delete(asset_footer);
//    munit_assert_false(pg_asset_exists(asset));
//    munit_assert_false(pg_asset_exists(asset_header));
//    munit_assert_false(pg_asset_exists(asset_footer));
//    pg_asset_manager_destroy(asset_mgr);
//    return MUNIT_OK;
//}
//
//MunitSuite pg_asset_tests = {
//    .prefix = "/util/asset",
//    .tests = (MunitTest[]) {
//        {   .name = "/manager_create",
//            .test = test_asset_manager_create },
//        {   .name = "/manager_declare_type",
//            .test = test_asset_manager_declare_type },
//        {   .name = "/manager_create_group",
//            .test = test_asset_manager_create_group },
//        {   .name = "/declare",
//            .test = test_asset_declare },
//        {   .name = "/from_json",
//            .test = test_asset_from_json },
//        {   .name = "/from_file",
//            .test = test_asset_from_file },
//        {   .name = "/from_AID",
//            .test = test_asset_from_AID },
//        {   .name = "/from_data",
//            .test = test_asset_from_data },
//        {   .name = "/get_handle",
//            .test = test_asset_get_handle },
//        {   .name = "/get_data_by_name",
//            .test = test_asset_get_data_by_name },
//        {   .name = "/assign_group",
//            .test = test_asset_assign_group },
//        {   .name = "/dependencies_manual",
//            .test = test_asset_dependencies_manual },
//        {   .name = "/dependencies_auto",
//            .test = test_asset_dependencies_auto },
//        {   .name = "/dependencies_delete",
//            .test = test_asset_dependencies_delete },
//        {0} },
//    .suites = (MunitSuite[]) {
//        {0},
//    }
//};
//
//
//static void* numbers_asset_load(pg_asset_handle_t asset_handle, cJSON* def, void* udata)
//{
//    /*  Check if we have a header/footer dependency and get those assets' data  */
//    cJSON* header_AID = cJSON_GetObjectItem(def, "header");
//    cJSON* footer_AID = cJSON_GetObjectItem(def, "footer");
//    pg_asset_handle_t header_asset = {0}, footer_asset = {0};
//    struct numbers_asset_data* header_data = NULL, * footer_data = NULL;
//    int header_footer_size = 0;
//    if(header_AID) {
//        header_asset = pg_asset_from_AID(asset_handle.mgr, header_AID->valuestring, "numbers");
//        header_data = pg_asset_get_data(header_asset);
//        header_footer_size += header_data->n;
//    }
//    if(footer_AID) {
//        footer_asset = pg_asset_from_AID(asset_handle.mgr, footer_AID->valuestring, "numbers");
//        footer_data = pg_asset_get_data(footer_asset);
//        header_footer_size += footer_data->n;
//    }
//    /*  Read this asset's data  */
//    cJSON* array_json = cJSON_GetObjectItem(def, "test_data");
//    int array_len = cJSON_GetArraySize(array_json);
//    int* array_data = malloc(sizeof(*array_data) * (array_len + header_footer_size));
//    int array_idx = header_data ? header_data->n : 0;
//    cJSON* value;
//    cJSON_ArrayForEach(value, array_json) {
//        array_data[array_idx++] = value->valueint;
//    }
//    /*  Copy the header and footer data */
//    if(header_data) memcpy(array_data, header_data->numbers, sizeof(int) * header_data->n);
//    if(footer_data) memcpy(array_data + header_data->n + array_len, footer_data->numbers, sizeof(int) * footer_data->n);
//    struct numbers_asset_data* loaded_data = malloc(sizeof(*loaded_data));
//    loaded_data->n = array_len + header_footer_size;
//    loaded_data->numbers = array_data;
//    return loaded_data;
//}
//
//static bool numbers_asset_load_deps(pg_asset_handle_t asset_handle, cJSON* def, void* udata)
//{
//    cJSON* header_AID = cJSON_GetObjectItem(def, "header");
//    if(header_AID && cJSON_IsString(header_AID)) {
//        if(!cJSON_IsString(header_AID)) return false;
//        pg_asset_handle_t header_asset = pg_asset_from_AID(asset_handle.mgr, header_AID->valuestring, "numbers");
//        pg_asset_depends(asset_handle, header_asset);
//    }
//    cJSON* footer_AID = cJSON_GetObjectItem(def, "footer");
//    if(footer_AID) {
//        if(!cJSON_IsString(footer_AID)) return false;
//        pg_asset_handle_t footer_asset = pg_asset_from_AID(asset_handle.mgr, footer_AID->valuestring, "numbers");
//        pg_asset_depends(asset_handle, footer_asset);
//    }
//    return true;
//}
//
//static void numbers_asset_unload(pg_asset_handle_t asset_handle, void* asset_data, void* udata)
//{
//    if(asset_data) free(asset_data);
//}

