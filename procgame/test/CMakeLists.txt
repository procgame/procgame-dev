add_executable(pg_test
    main.c
    pg_inline_test.c
    pg_asset_test.c
    pg_file_utility_test.c
    pg_gpu_buffer_test.c
    pg_gpu_command_buffer_test.c
    pg_gpu_framebuffer_test.c
    pg_gpu_pipeline_test.c
    pg_gpu_shaders_test.c
    pg_gpu_texture_test.c
    pg_renderer_test.c
    pg_text_utility_test.c
    helpers/gfx_test_utils.c
)
target_link_libraries(pg_test PRIVATE procgame munit)

add_custom_target(pg_copy_test_data
    COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_CURRENT_BINARY_DIR}/res
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/res ${CMAKE_CURRENT_BINARY_DIR}/res
    COMMAND ${CMAKE_COMMAND} -E echo "Copied test data from ${CMAKE_CURRENT_SOURCE_DIR}/res to ${CMAKE_CURRENT_BINARY_DIR}/res"
)

add_custom_target(pg_run_tests
    DEPENDS pg_copy_test_data
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_CURRENT_BINARY_DIR}/test_renders"
    COMMAND $<TARGET_FILE:pg_test>
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

