#include <stddef.h>

#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "helpers/gfx_test_utils.h"


static void* test_setup_textures(const MunitParameter params[], void* user_data);
static void test_tear_down_textures(void* fixture);

struct test_framebuffer_base {
    pg_gpu_texture_t* color_8bit;
    pg_gpu_texture_t* color_32bit;
    pg_gpu_texture_t* depth;
    pg_gpu_texture_view_t* color_8bit_view;
    pg_gpu_texture_view_t* color_32bit_view;
    pg_gpu_texture_view_t* depth_view;
};

static MunitResult test_framebuffer_create_single_buffer(const MunitParameter params[], void* data)
{
    struct test_framebuffer_base* base = data;
    pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init(1, &base->color_8bit_view);
    munit_assert_not_null(gpu_fbuf);
    pg_gpu_framebuffer_deinit(gpu_fbuf);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_framebuffer_create_multi_buffer(const MunitParameter params[], void* data)
{
    struct test_framebuffer_base* base = data;
    pg_gpu_texture_view_t* textures[] = { base->color_8bit_view, base->color_32bit_view };
    pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init(2, textures);
    munit_assert_not_null(gpu_fbuf);
    pg_gpu_framebuffer_deinit(gpu_fbuf);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_framebuffer_create_with_depth(const MunitParameter params[], void* data)
{
    struct test_framebuffer_base* base = data;
    pg_gpu_texture_view_t* textures[] = { base->color_8bit_view, base->depth_view };
    pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init(1, textures);
    munit_assert_not_null(gpu_fbuf);
    pg_gpu_framebuffer_deinit(gpu_fbuf);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_framebuffer_create_depth_only(const MunitParameter params[], void* data)
{
    struct test_framebuffer_base* base = data;
    pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init(1, &base->depth_view);
    munit_assert_not_null(gpu_fbuf);
    pg_gpu_framebuffer_deinit(gpu_fbuf);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_framebuffer_get_dimensions(const MunitParameter params[], void* data)
{
    struct test_framebuffer_base* base = data;
    pg_gpu_framebuffer_t* gpu_fbuf = pg_gpu_framebuffer_init(1, &base->color_8bit_view);
    ivec2 dimensions = pg_gpu_framebuffer_get_dimensions(gpu_fbuf);
    munit_assert_int(dimensions.x, ==, 512);
    munit_assert_int(dimensions.y, ==, 512);
    pg_gpu_framebuffer_deinit(gpu_fbuf);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_framebuffer_asset_loader(const MunitParameter params[], void* data)
{
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_framebuffer_asset_loader());
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_texture_asset_loader());
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(pg_gpu_framebuffer)foo:test_gpu_framebuffer_asset.json", NULL);
    pg_gpu_framebuffer_t* gpu_fbuf = pg_asset_get_data(asset);
    munit_assert_not_null(gpu_fbuf);
    ivec2 dimensions = pg_gpu_framebuffer_get_dimensions(gpu_fbuf);
    munit_assert_int(dimensions.x, ==, 640);
    munit_assert_int(dimensions.y, ==, 480);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}



/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_framebuffer_tests = {
    .prefix = "/gfx/framebuffer",
    .tests = (MunitTest[]) {
        {   .name = "/create_single_buffer",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_create_single_buffer },
        {   .name = "/create_multi_buffer",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_create_multi_buffer },
        {   .name = "/create_with_depth",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_create_with_depth },
        {   .name = "/create_depth_only",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_create_depth_only },
        {   .name = "/get_dimensions",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_get_dimensions },
        {   .name = "/asset_loader",
            .setup = test_setup_textures, .tear_down = test_tear_down_textures,
            .test = test_framebuffer_asset_loader },
        {0} },
    .suites = (MunitSuite[]) {
        {0},
    }
};

static void* test_setup_textures(const MunitParameter params[], void* user_data)
{
    test_setup_video(params, user_data);
    struct test_framebuffer_base* base = malloc(sizeof(*base));
    base->color_8bit = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 512, 512, 1);
    base->color_32bit = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA32F, 512, 512, 1);
    base->depth = pg_gpu_texture_alloc(PG_GPU_TEXTURE_DEPTH32, 512, 512, 1);
    base->color_8bit_view = pg_gpu_texture_get_layer_view(base->color_8bit, 0);
    base->color_32bit_view = pg_gpu_texture_get_layer_view(base->color_32bit, 0);
    base->depth_view = pg_gpu_texture_get_layer_view(base->depth, 0);
    return base;
}

static void test_tear_down_textures(void* fixture)
{
    struct test_framebuffer_base* base = fixture;
    pg_gpu_texture_view_deinit(base->color_8bit_view);
    pg_gpu_texture_view_deinit(base->color_32bit_view);
    pg_gpu_texture_view_deinit(base->depth_view);
    pg_gpu_texture_deinit(base->color_8bit);
    pg_gpu_texture_deinit(base->color_32bit);
    pg_gpu_texture_deinit(base->depth);
    free(base);
    test_tear_down_video(NULL);
}



