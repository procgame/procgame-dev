//#include <stddef.h>
//
//#include "munit.h"
//
//#include "pg_types.h"
//#include "pg_core/arr.h"
//#include "pg_math/linmath.h"
//#include "pg_util/asset.h"
//#include "pg_util/json_utility.h"
//#include "pg_util/text_utility.h"
//#include "pg_gpu/opengl.h"
//#include "pg_gpu/buffer.h"
//#include "pg_gpu/texture.h"
//#include "pg_gpu/framebuffer.h"
//#include "pg_gpu/shader.h"
//#include "pg_gpu/pipeline.h"
//#include "pg_gfx/renderer.h"
//
//#include "helpers/gfx_test_utils.h"
//
//
//static MunitResult test_renderer_draw_single(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_renderer/");
//    pg_renderer_init_asset_loaders(asset_mgr);
//    struct pg_renderer renderer;
//    pg_renderer_init(&renderer, asset_mgr);
//    pg_renderer_add_pass(&renderer, pg_asset_from_AID(asset_mgr, "(pg_gpu_pipeline)pipeline:single/pipeline.json", NULL));
//    pg_renderer_draw(&renderer);
//    /*  Compare the render result   */
//    munit_assert(test_image_regression_asset("renderer_draw_single", asset_mgr));
//    munit_assert_false(pg_gl_errors());
//    return MUNIT_OK;
//}
//
//static MunitResult test_renderer_draw_multi(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_renderer/");
//    pg_renderer_init_asset_loaders(asset_mgr);
//    struct pg_renderer renderer;
//    pg_renderer_init(&renderer, asset_mgr);
//    pg_renderer_add_pass(&renderer, pg_asset_from_AID(asset_mgr, "(pg_gpu_pipeline)pass0_pipeline:multi/pass0/pipeline.json", NULL));
//    pg_renderer_add_pass(&renderer, pg_asset_from_AID(asset_mgr, "(pg_gpu_pipeline)pass1_pipeline:multi/pass1/pipeline.json", NULL));
//    pg_renderer_draw(&renderer);
//    /*  Compare the render result   */
//    munit_assert(test_image_regression_asset("renderer_draw_multi", asset_mgr));
//    munit_assert_false(pg_gl_errors());
//    return MUNIT_OK;
//}
//
//static MunitResult test_renderer_asset_loader(const MunitParameter params[], void* data)
//{
//    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
//    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_renderer/");
//    pg_renderer_init_asset_loaders(asset_mgr);
//    pg_asset_handle_t renderer_asset = pg_asset_from_AID(asset_mgr, "(pg_renderer)renderer:asset/renderer.json", NULL);
//    munit_assert(pg_asset_exists(renderer_asset));
//    struct pg_renderer* renderer = pg_asset_get_data(renderer_asset);
//    munit_assert_not_null(renderer);
//    pg_renderer_draw(renderer);
//    /*  Compare the render result   */
//    munit_assert(test_image_regression_asset("renderer_asset_loader", asset_mgr));
//    munit_assert_false(pg_gl_errors());
//    return MUNIT_OK;
//}
//
///****************************/
///*  Test suite definition   */
///****************************/
//
//MunitSuite pg_renderer_tests = {
//    .prefix = "/gfx/renderer",
//    .tests = (MunitTest[]) {
//        {   .name = "/draw_single",
//            .setup = test_setup_video, .tear_down = test_tear_down_video,
//            .test = test_renderer_draw_single },
//        {   .name = "/draw_multi",
//            .setup = test_setup_video, .tear_down = test_tear_down_video,
//            .test = test_renderer_draw_multi },
//        {   .name = "/asset_loader",
//            .setup = test_setup_video, .tear_down = test_tear_down_video,
//            .test = test_renderer_asset_loader },
//        {0},
//    },
//};
//
