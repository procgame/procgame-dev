#include <stddef.h>

#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "helpers/gfx_test_utils.h"


static MunitResult test_texture_init(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    munit_assert(pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, 2));
    munit_assert_int(test_texture.dimensions.x, ==, 128);
    munit_assert_int(test_texture.dimensions.y, ==, 128);
    munit_assert_int(test_texture.dimensions.z, ==, 2);
    munit_assert_int(test_texture.buf.data_size, ==, 128 * 128 * 2 * sizeof(ubvec4));
    munit_assert_int(test_texture.buf.element_size, ==, sizeof(ubvec4));
    pg_texture_deinit(&test_texture);
    return MUNIT_OK;
}

static MunitResult test_texture_init_invalid_dimensions(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 0, 0, 0));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, -1, -1, -1));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 0, 128, 128));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, -1, 128, 128));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 128, 0, 128));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 128, -1, 128));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, 0));
    munit_assert_false(pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, -1));
    return MUNIT_OK;
}

static MunitResult test_texture_get_pixel_ptr(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, 4);
    ubvec4* pixels = (ubvec4*)test_texture.buf.data;
    /*  Standard n-d array access formula: (x + (y * width) + (z * width * height))
        Set the pixel directly, then get it by the function, and check that the
        value received is correct.  */
    pixels[10 + (15 * 128) + (1 * 128 * 128)] = ubvec4(255, 128, 64, 255);
    ubvec4* get_pixel = (ubvec4*)pg_texture_get_pixel_ptr(&test_texture, 10, 15, 1);
    munit_assert_uchar(get_pixel->x, ==, 255);
    munit_assert_uchar(get_pixel->y, ==, 128);
    munit_assert_uchar(get_pixel->z, ==, 64);
    munit_assert_uchar(get_pixel->w, ==, 255);
    pg_texture_deinit(&test_texture);
    return MUNIT_OK;
}

static MunitResult test_texture_init_from_path(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    munit_assert(pg_texture_init_from_path(&test_texture, "./res/textures/test_texture.png"));
    munit_assert_int(test_texture.dimensions.x, ==, 64);
    munit_assert_int(test_texture.dimensions.y, ==, 64);
    munit_assert_int(test_texture.dimensions.z, ==, 1);
    ubvec4* black_pixel = (ubvec4*)pg_texture_get_pixel_ptr(&test_texture, 3, 3, 0);
    ubvec4* red_pixel = (ubvec4*)pg_texture_get_pixel_ptr(&test_texture, 8, 32, 0);
    munit_assert_uchar(black_pixel->x, ==, 0);
    munit_assert_uchar(black_pixel->y, ==, 0);
    munit_assert_uchar(black_pixel->z, ==, 0);
    munit_assert_uchar(black_pixel->w, ==, 255);
    munit_assert_uchar(red_pixel->x, ==, 255);
    munit_assert_uchar(red_pixel->y, ==, 0);
    munit_assert_uchar(red_pixel->z, ==, 255);
    munit_assert_uchar(red_pixel->w, ==, 255);
    pg_texture_deinit(&test_texture);
    return MUNIT_OK;
}

static MunitResult test_texture_init_from_file(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    struct pg_file test_file;
    munit_assert(pg_file_open(&test_file, "./res/textures/test_texture.png", "r"));
    munit_assert(pg_texture_init_from_file(&test_texture, &test_file));
    munit_assert_int(test_texture.dimensions.x, ==, 64);
    munit_assert_int(test_texture.dimensions.y, ==, 64);
    munit_assert_int(test_texture.dimensions.z, ==, 1);
    ubvec4* black_pixel = (ubvec4*)pg_texture_get_pixel_ptr(&test_texture, 3, 3, 0);
    ubvec4* red_pixel = (ubvec4*)pg_texture_get_pixel_ptr(&test_texture, 8, 32, 0);
    munit_assert_uchar(black_pixel->x, ==, 0);
    munit_assert_uchar(black_pixel->y, ==, 0);
    munit_assert_uchar(black_pixel->z, ==, 0);
    munit_assert_uchar(black_pixel->w, ==, 255);
    munit_assert_uchar(red_pixel->x, ==, 255);
    munit_assert_uchar(red_pixel->y, ==, 0);
    munit_assert_uchar(red_pixel->z, ==, 255);
    munit_assert_uchar(red_pixel->w, ==, 255);
    pg_texture_deinit(&test_texture);
    return MUNIT_OK;
}

static MunitResult test_texture_compare_equal(const MunitParameter params[], void* data)
{
    struct pg_texture tex0, tex1, tex2;
    pg_texture_init_from_path(&tex0, "./res/textures/test_texture.png");
    pg_texture_init_from_path(&tex1, "./res/textures/test_texture.png");
    pg_texture_init(&tex2, PG_UBVEC4, 64, 64, 1);
    munit_assert_true(pg_texture_compare_equal(&tex0, &tex1));
    munit_assert_true(pg_texture_compare_equal(&tex0, &tex0));
    munit_assert_false(pg_texture_compare_equal(&tex0, &tex2));
    munit_assert_false(pg_texture_compare_equal(&tex0, NULL));
    munit_assert_false(pg_texture_compare_equal(NULL, NULL));
    pg_texture_deinit(&tex0);
    pg_texture_deinit(&tex1);
    pg_texture_deinit(&tex2);
    return MUNIT_OK;
}



static MunitResult test_texture_upload(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, 4);
    pg_gpu_texture_t* gpu_tex = pg_texture_upload(&test_texture);
    munit_assert_not_null(gpu_tex);
    munit_assert_false(pg_gl_errors());
    pg_texture_deinit(&test_texture);
    pg_gpu_texture_deinit(gpu_tex);
    return MUNIT_OK;
}

static MunitResult test_texture_download(const MunitParameter params[], void* data)
{
    struct pg_texture test_texture = {0};
    pg_texture_init(&test_texture, PG_UBVEC4, 128, 128, 4);
    
    pg_gpu_texture_t* gpu_tex = pg_texture_upload(&test_texture);
    struct pg_texture test_download = {0};
    pg_gpu_texture_download(gpu_tex, &test_download);

    pg_gpu_texture_deinit(gpu_tex);
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_alloc(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    munit_assert_not_null(gpu_tex);
    munit_assert_false(pg_gl_errors());
    pg_gpu_texture_deinit(gpu_tex);
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_depth_buffer(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_DEPTH32F, 128, 128, 1);
    munit_assert_not_null(gpu_tex);
    munit_assert_false(pg_gl_errors());
    pg_gpu_texture_deinit(gpu_tex);
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_format(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_image_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    pg_gpu_texture_t* gpu_depth_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_DEPTH32, 128, 128, 4);
    munit_assert(pg_gpu_texture_get_format(gpu_image_tex) == PG_GPU_TEXTURE_RGBA8);
    munit_assert(pg_gpu_texture_get_format(gpu_depth_tex) == PG_GPU_TEXTURE_DEPTH32);
    pg_gpu_texture_deinit(gpu_image_tex);
    pg_gpu_texture_deinit(gpu_depth_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_dimensions(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    ivec3 dims = pg_gpu_texture_get_dimensions(gpu_tex);
    munit_assert_int(dims.x, ==, 128);
    munit_assert_int(dims.y, ==, 128);
    munit_assert_int(dims.z, ==, 4);
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_basic_view(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    pg_gpu_texture_view_t* gpu_tex_view = pg_gpu_texture_get_basic_view(gpu_tex);
    ivec2 range = pg_gpu_texture_view_get_range(gpu_tex_view);
    munit_assert_int(range.x, ==, 0);
    munit_assert_int(range.y, ==, 4);
    pg_gpu_texture_view_deinit(gpu_tex_view);
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_layer_view(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    pg_gpu_texture_view_t* gpu_tex_view = pg_gpu_texture_get_layer_view(gpu_tex, 2);
    ivec2 range = pg_gpu_texture_view_get_range(gpu_tex_view);
    munit_assert_int(range.x, ==, 2);
    munit_assert_int(range.y, ==, 1);
    pg_gpu_texture_view_deinit(gpu_tex_view);
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_layer_view_wrong(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    /*  Fail on invalid layers  */
    munit_assert_null(pg_gpu_texture_get_layer_view(gpu_tex, -1));
    munit_assert_null(pg_gpu_texture_get_layer_view(gpu_tex, 4));
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_range_view(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    pg_gpu_texture_view_t* gpu_tex_view = pg_gpu_texture_get_range_view(gpu_tex, 1, 2);
    ivec2 range = pg_gpu_texture_view_get_range(gpu_tex_view);
    munit_assert_int(range.x, ==, 1);
    munit_assert_int(range.y, ==, 2);
    pg_gpu_texture_view_deinit(gpu_tex_view);
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_get_range_view_wrong(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    /*  Fail on invalid ranges  */
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, -1, 1));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 0, 0));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 1, 0));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 4, 0));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 0, 5));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 2, 2));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 2, 2));
    munit_assert_null(pg_gpu_texture_get_range_view(gpu_tex, 2, -1));
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_view_get_base(const MunitParameter params[], void* data)
{
    pg_gpu_texture_t* gpu_tex = pg_gpu_texture_alloc(PG_GPU_TEXTURE_RGBA8, 128, 128, 4);
    pg_gpu_texture_view_t* gpu_tex_view = pg_gpu_texture_get_range_view(gpu_tex, 1, 2);
    pg_gpu_texture_t* gpu_tex_view_base = pg_gpu_texture_view_get_base(gpu_tex_view);
    munit_assert_ptr(gpu_tex, ==, gpu_tex_view_base);
    pg_gpu_texture_deinit(gpu_tex);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_texture_gpu_asset_loader(const MunitParameter params[], void* data)
{
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_search_path(asset_mgr, "./res/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_texture_asset_loader());
    // With filenames
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(pg_gpu_texture)foo:test_gpu_texture_asset_with_filenames.json", NULL);
    pg_gpu_texture_t* gpu_tex = pg_asset_get_data(asset);
    munit_assert_not_null(gpu_tex);
    enum pg_gpu_texture_format gpu_tex_format = pg_gpu_texture_get_format(gpu_tex);
    munit_assert_int(gpu_tex_format, ==, PG_GPU_TEXTURE_RGBA8);
    ivec3 dimensions = pg_gpu_texture_get_dimensions(gpu_tex);
    munit_assert_int(dimensions.x, ==, 128);
    munit_assert_int(dimensions.y, ==, 128);
    munit_assert_int(dimensions.z, ==, 2);
    //  Without filenames (direct format/dimensions in JSON)
    asset = pg_asset_from_AID(asset_mgr, "(pg_gpu_texture)foo2:test_gpu_texture_asset.json", NULL);
    gpu_tex = pg_asset_get_data(asset);
    munit_assert_not_null(gpu_tex);
    gpu_tex_format = pg_gpu_texture_get_format(gpu_tex);
    munit_assert_int(gpu_tex_format, ==, PG_GPU_TEXTURE_RG32F);
    dimensions = pg_gpu_texture_get_dimensions(gpu_tex);
    munit_assert_int(dimensions.x, ==, 64);
    munit_assert_int(dimensions.y, ==, 64);
    munit_assert_int(dimensions.z, ==, 4);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}





/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_texture_tests = {
    .prefix = "/gfx/texture",
    .tests = (MunitTest[]) {
        {   .name = "/init",
            .test = test_texture_init },
        {   .name = "/init_invalid_dimensions",
            .test = test_texture_init_invalid_dimensions },
        {   .name = "/init_from_path",
            .test = test_texture_init_from_path },
        {   .name = "/init_from_file",
            .test = test_texture_init_from_file },
        {   .name = "/get_pixel_ptr",
            .test = test_texture_get_pixel_ptr },
        {   .name = "/compare_equal",
            .test = test_texture_compare_equal },
        {0} },
    .suites = (MunitSuite[]) {
        {   .prefix = "/gpu",
            .suites = (MunitSuite[]) {
            {   .prefix = "/texture_view",
                .tests = (MunitTest[]) {
                    {   .name = "/basic",
                        .setup = test_setup_video, .tear_down = test_tear_down_video,
                        .test = test_texture_gpu_get_basic_view },
                    {   .name = "/layer",
                        .setup = test_setup_video, .tear_down = test_tear_down_video,
                        .test = test_texture_gpu_get_layer_view },
                    {   .name = "/range",
                        .setup = test_setup_video, .tear_down = test_tear_down_video,
                        .test = test_texture_gpu_get_range_view },
                    {   .name = "/get_base",
                        .setup = test_setup_video, .tear_down = test_tear_down_video,
                        .test = test_texture_gpu_view_get_base },
                    {0} } },
            {0} },
            .tests = (MunitTest[]) {
                {   .name = "/upload",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_upload },
                {   .name = "/download",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_download },
                {   .name = "/alloc",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_gpu_alloc},
                {   .name = "/depth_buffer",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_gpu_depth_buffer },
                {   .name = "/get_format",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_gpu_get_format },
                {   .name = "/get_dimensions",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_gpu_get_dimensions },
                {   .name = "/asset_loader",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_texture_gpu_asset_loader },
                {0} } },
        {0},
    }
};


