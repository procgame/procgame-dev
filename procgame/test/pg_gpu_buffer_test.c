#include <stddef.h>

#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "helpers/gfx_test_utils.h"


/****************************/
/*  Test buffer layouts     */
/****************************/

struct test_attrib_elem {
    vec3 position;
};

const struct pg_buffer_layout single_attrib_layout = PG_BUFFER_LAYOUT(
    PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
    PG_BUFFER_ELEMENT_ALIGNMENT(4),
    PG_BUFFER_ATTRIBUTES_LIST(1,
        PG_BUFFER_ATTRIBUTE("position", PG_VEC3)
    )
);



struct attribs_multiple_elem {
    vec3 position;
    float weight;
    vec4 color;
};

const struct pg_buffer_layout multi_attribs_layout = PG_BUFFER_LAYOUT(
    PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_4BYTE),
    PG_BUFFER_ELEMENT_ALIGNMENT(4),
    PG_BUFFER_ATTRIBUTES_LIST(3,
        PG_BUFFER_ATTRIBUTE("position", PG_VEC3),
        PG_BUFFER_ATTRIBUTE("weight", PG_FLOAT),  /*  Expect 1 byte padding after this    */
        PG_BUFFER_ATTRIBUTE("color", PG_VEC4)  /*  Expect 2 bytes padding after this   */
    )
);



struct attribs_padding_elem {
    vec3 position;
    ubvec3 rgb;
    char pad0[1];
    ubvec2 grid_idx;
    char pad1[2];
};

const struct pg_buffer_layout padded_attribs_layout = PG_BUFFER_LAYOUT(
    PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_4BYTE),
    PG_BUFFER_ELEMENT_ALIGNMENT(4),
    PG_BUFFER_ATTRIBUTES_LIST(3,
        PG_BUFFER_ATTRIBUTE("position", PG_VEC3),
        PG_BUFFER_ATTRIBUTE("rgb", PG_UBVEC3),  /*  Expect 1 byte padding after this    */
        PG_BUFFER_ATTRIBUTE("grid_idx", PG_UBVEC2)  /*  Expect 2 bytes padding after this   */
    )
);


/****************************/
/*  Allocation              */
/****************************/

static MunitResult test_buffer_allocate_single_attribute(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &single_attrib_layout, 2));
    munit_assert_not_null(test_buffer.data);
    munit_assert_int(test_buffer.element_size, ==, sizeof(struct test_attrib_elem));
    munit_assert_int(test_buffer.data_elements, ==, 2);
    munit_assert_int(test_buffer.data_size, ==, 2 * sizeof(struct test_attrib_elem));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_allocate_multi_attribute(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &multi_attribs_layout, 2));
    munit_assert_not_null(test_buffer.data);
    munit_assert_int(test_buffer.element_size, ==, sizeof(struct attribs_multiple_elem));
    munit_assert_int(test_buffer.data_elements, ==, 2);
    munit_assert_int(test_buffer.data_size, ==, 2 * sizeof(struct attribs_multiple_elem));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_allocate_padding(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 2));
    munit_assert_not_null(test_buffer.data);
    munit_assert_int(test_buffer.element_size, ==, sizeof(struct attribs_padding_elem));
    munit_assert_int(test_buffer.data_elements, ==, 2);
    munit_assert_int(test_buffer.data_size, ==, 2 * sizeof(struct attribs_padding_elem));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_resize(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &single_attrib_layout, 1));
    /*  Resize to 2 elements    */
    munit_assert_true(pg_buffer_resize(&test_buffer, 2));
    /*  Test the new limits */
    munit_assert_not_null(pg_buffer_get_element_ptr(&test_buffer, 0));
    munit_assert_not_null(pg_buffer_get_element_ptr(&test_buffer, 1));
    munit_assert_null(pg_buffer_get_element_ptr(&test_buffer, 2));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_resize_wrong(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &single_attrib_layout, 1));
    /*  Fail to resize with negative or zero size   */
    munit_assert_false(pg_buffer_resize(&test_buffer, 0));
    munit_assert_false(pg_buffer_resize(&test_buffer, -1));
    /*  The buffer should remain in valid state */
    munit_assert_not_null(pg_buffer_get_element_ptr(&test_buffer, 0));
    munit_assert_null(pg_buffer_get_element_ptr(&test_buffer, 1));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}


/****************************/
/*  Data access             */
/****************************/

static MunitResult test_buffer_get_attribute(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));

    int idx_pos = pg_buffer_get_attribute_index(&test_buffer, "position");
    int idx_rgb = pg_buffer_get_attribute_index(&test_buffer, "rgb");
    int idx_grid = pg_buffer_get_attribute_index(&test_buffer, "grid_idx");
    munit_assert_int(idx_pos, ==, 0);
    munit_assert_int(idx_rgb, ==, 1);
    munit_assert_int(idx_grid, ==, 2);
    const struct pg_buffer_attribute* attrib_pos = pg_buffer_get_attribute(&test_buffer, idx_pos);
    const struct pg_buffer_attribute* attrib_rgb = pg_buffer_get_attribute(&test_buffer, idx_rgb);
    const struct pg_buffer_attribute* attrib_grid = pg_buffer_get_attribute(&test_buffer, idx_grid);
    munit_assert_not_null(attrib_pos);
    munit_assert_not_null(attrib_rgb);
    munit_assert_not_null(attrib_grid);
    /*  Here we are testing known/expected values for these specific inputs */
    /*  pos */
    munit_assert_int(attrib_pos->offset, ==, 0);
    munit_assert_int(attrib_pos->size, ==, sizeof(vec3));
    munit_assert_int(attrib_pos->stride, ==, sizeof(struct attribs_padding_elem));
    munit_assert_int(attrib_pos->pad_after, ==, 0);
    munit_assert_int(attrib_pos->index, ==, 0);
    munit_assert_int(attrib_pos->info.type, ==, PG_VEC3);
    /*  rgb */
    munit_assert_int(attrib_rgb->offset, ==, sizeof(vec3));
    munit_assert_int(attrib_rgb->size, ==, sizeof(ubvec3));
    munit_assert_int(attrib_rgb->stride, ==, sizeof(struct attribs_padding_elem));
    munit_assert_int(attrib_rgb->pad_after, ==, 1);
    munit_assert_int(attrib_rgb->index, ==, 1);
    munit_assert_int(attrib_rgb->info.type, ==, PG_UBVEC3);
    /*  grid */
    munit_assert_int(attrib_grid->offset, ==, sizeof(vec3) + sizeof(ubvec4));
    munit_assert_int(attrib_grid->size, ==, sizeof(ubvec2));
    munit_assert_int(attrib_grid->stride, ==, sizeof(struct attribs_padding_elem));
    munit_assert_int(attrib_grid->pad_after, ==, 2);
    munit_assert_int(attrib_grid->index, ==, 2);
    munit_assert_int(attrib_grid->info.type, ==, PG_UBVEC2);
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_get_attribute_wrong(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    munit_assert_int(pg_buffer_get_attribute_index(&test_buffer, "does_not_exist"), ==, -1);
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_get_element_ptr_bounds(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    /*  Test the beginning of the buffer    */
    char* element_ptr = pg_buffer_get_element_ptr(&test_buffer, 0);
    char* expect_ptr = test_buffer.data;
    munit_assert_ptr(element_ptr, ==, expect_ptr);
    /*  Test something in the middle of the buffer  */
    element_ptr = pg_buffer_get_element_ptr(&test_buffer, 3);
    expect_ptr = test_buffer.data + (sizeof(struct attribs_padding_elem) * 3);
    munit_assert_ptr(element_ptr, ==, expect_ptr);
    /*  Expect NULL for out of bounds   */
    munit_assert_null(pg_buffer_get_element_ptr(&test_buffer, 6));
    munit_assert_null(pg_buffer_get_element_ptr(&test_buffer, -1));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_get_attribute_ptr_bounds(const MunitParameter params[], void* data)
{
    int attrib_offset[3] = {
        offsetof(struct attribs_padding_elem, position),
        offsetof(struct attribs_padding_elem, rgb),
        offsetof(struct attribs_padding_elem, grid_idx),
    };
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    /*  0.rgb   */
    void* attribute_ptr = pg_buffer_get_attribute_ptr(&test_buffer, 0, 1);
    char* expect_ptr = test_buffer.data + attrib_offset[1];
    munit_assert_ptr(attribute_ptr, ==, expect_ptr);
    /*  3.grid_idx  */
    attribute_ptr = pg_buffer_get_attribute_ptr(&test_buffer, 3, 2);
    expect_ptr = test_buffer.data + (sizeof(struct attribs_padding_elem) * 3) + attrib_offset[2];
    munit_assert_ptr(attribute_ptr, ==, expect_ptr);
    /*  Expect NULL for out of bounds   */
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, 0, 3));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, 6, 0));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, 6, 3));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, -1, 0));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, 0, -1));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, -1, 3));
    munit_assert_null(pg_buffer_get_attribute_ptr(&test_buffer, 6, -1));
    pg_buffer_deinit(&test_buffer);
    return MUNIT_OK;
}

static MunitResult test_buffer_struct_reflection(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    struct attribs_padding_elem elem = {
        .position = vec3(25, 50, 75),
        .rgb = ubvec3(64, 128, 255),
        .grid_idx = ubvec2(0, 1),
    };
    struct attribs_padding_elem* buf_elem = (struct attribs_padding_elem*)pg_buffer_get_element_ptr(&test_buffer, 3);
    *buf_elem = elem;
    vec3 position = *((vec3*)pg_buffer_get_attribute_ptr(&test_buffer, 3, 0));
    munit_assert_float(position.x, ==, elem.position.x);
    munit_assert_float(position.y, ==, elem.position.y);
    munit_assert_float(position.z, ==, elem.position.z);
    ubvec3 rgb = *((ubvec3*)pg_buffer_get_attribute_ptr(&test_buffer, 3, 1));
    munit_assert_uint8(rgb.x, ==, elem.rgb.x);
    munit_assert_uint8(rgb.y, ==, elem.rgb.y);
    munit_assert_uint8(rgb.z, ==, elem.rgb.z);
    ubvec2 grid_idx = *((ubvec2*)pg_buffer_get_attribute_ptr(&test_buffer, 3, 2));
    munit_assert_uint8(grid_idx.x, ==, elem.grid_idx.x);
    munit_assert_uint8(grid_idx.y, ==, elem.grid_idx.y);
    return MUNIT_OK;
}

static MunitResult test_buffer_upload_vertex_buffer(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    pg_gpu_buffer_t* gpu_buffer = pg_buffer_upload(&test_buffer, PG_GPU_VERTEX_BUFFER, false);
    munit_assert_not_null(gpu_buffer);
    pg_buffer_deinit(&test_buffer);
    pg_gpu_buffer_deinit(gpu_buffer);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_buffer_upload_texture_buffer(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    const struct pg_buffer_layout buffer_layout = PG_BUFFER_LAYOUT(
        PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
        PG_BUFFER_ELEMENT_ALIGNMENT(4),
        PG_BUFFER_ATTRIBUTES_LIST(1,
            PG_BUFFER_ATTRIBUTE("texel", PG_UBVEC4)
        )
    );
    pg_buffer_init(&test_buffer, &buffer_layout, 6);
    pg_gpu_buffer_t* gpu_buffer = pg_buffer_upload(&test_buffer, PG_GPU_TEXTURE_BUFFER, false);
    munit_assert_not_null(gpu_buffer);
    pg_buffer_deinit(&test_buffer);
    pg_gpu_buffer_deinit(gpu_buffer);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_buffer_upload_texture_buffer_wrong(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    const struct pg_buffer_layout buffer_layout = PG_BUFFER_LAYOUT(
        PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_PACKED),
        PG_BUFFER_ELEMENT_ALIGNMENT(4),
        PG_BUFFER_ATTRIBUTES_LIST(1,
            PG_BUFFER_ATTRIBUTE("wrong", PG_MAT3)
        )
    );
    pg_buffer_init(&test_buffer, &buffer_layout, 6);
    /*  Fail to upload buffer with unsupported GPU buffer attribute */
    munit_assert_null(pg_buffer_upload(&test_buffer, PG_GPU_TEXTURE_BUFFER, false));
    pg_buffer_deinit(&test_buffer);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_gpu_buffer_get_attribute(const MunitParameter params[], void* data)
{
    struct pg_buffer test_buffer = {0};
    munit_assert(pg_buffer_init(&test_buffer, &padded_attribs_layout, 6));
    pg_gpu_buffer_t* gpu_buffer = pg_buffer_upload(&test_buffer, PG_GPU_VERTEX_BUFFER, false);
    int position_index = pg_gpu_buffer_get_attribute_index(gpu_buffer, "position");
    const struct pg_buffer_attribute* position_attrib =
            pg_gpu_buffer_get_attribute(gpu_buffer, position_index);
    munit_assert_int(position_attrib->info.type, ==, PG_VEC3);
    munit_assert_int(position_attrib->offset, ==, 0);
    pg_buffer_deinit(&test_buffer);
    pg_gpu_buffer_deinit(gpu_buffer);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_gpu_buffer_asset_loader(const MunitParameter params[], void* data)
{
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_buffer_asset_loader());
    // With filenames
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr, "(pg_gpu_buffer)foo:test_gpu_buffer_asset.json", NULL);
    pg_gpu_buffer_t* gpu_buf = pg_asset_get_data(asset);
    munit_assert_not_null(gpu_buf);
    int gpu_buf_size = pg_gpu_buffer_get_size(gpu_buf);
    const struct pg_buffer_attribute* second_attrib = pg_gpu_buffer_get_attribute(gpu_buf, 1);
    munit_assert_int(gpu_buf_size, ==, 144);
    munit_assert_string_equal(second_attrib->info.name, "uv_mat");
    munit_assert(second_attrib->info.type == PG_VEC3);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}



/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_buffer_tests = {
    .prefix = "/gfx/buffer",
    .tests = (MunitTest[]) {
        {   .name = "/single_attribute",
            .test = test_buffer_allocate_single_attribute },
        {   .name = "/multi_attribute",
            .test = test_buffer_allocate_multi_attribute },
        {   .name = "/padding",
            .test = test_buffer_allocate_padding },
        {   .name = "/resize",
            .test = test_buffer_resize },
        {   .name = "/resize_wrong",
            .test = test_buffer_resize_wrong },
        {0} },
    .suites = (MunitSuite[]) {
        {   .prefix = "/get",
            .tests = (MunitTest[]) {
                {   .name = "/attribute",
                    .test = test_buffer_get_attribute },
                {   .name = "/wrong_attribute",
                    .test = test_buffer_get_attribute_wrong },
                {   .name = "/element_ptr_bounds",
                    .test = test_buffer_get_element_ptr_bounds },
                {   .name = "/attribute_ptr_bounds",
                    .test = test_buffer_get_attribute_ptr_bounds },
                {   .name = "/struct_reflection",
                    .test = test_buffer_struct_reflection },
                {0} } },
        {   .prefix = "/gpu",
            .tests = (MunitTest[]) {
                {   .name = "/upload_vertex_buffer",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_buffer_upload_vertex_buffer },
                {   .name = "/upload_texture_buffer",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_buffer_upload_vertex_buffer },
                {   .name = "/upload_texture_buffer_wrong",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_buffer_upload_vertex_buffer },
                {   .name = "/get_attribute_info",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_gpu_buffer_get_attribute },
                {   .name = "/asset_loader",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_gpu_buffer_asset_loader },
                {0} } },
        {0},
    }
};


