#include <stddef.h>

#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "helpers/gfx_test_utils.h"


static MunitResult test_command_buffer_from_json(const MunitParameter params[], void* data)
{
    cJSON* json = load_json("res/pg_gpu_command_buffer/command_buffer_basic.json");
    cJSON* cmdbuf_json = cJSON_GetObjectItem(json, "command_buffer");
    pg_gpu_command_arr_t cmds_arr;
    ARR_INIT(cmds_arr);
    munit_assert(pg_gpu_commands_from_json(&cmds_arr, cmdbuf_json));
    munit_assert_int(cmds_arr.len, ==, 4);
    struct pg_gpu_command* cmd_data = cmds_arr.data;
    munit_assert_not_null(cmd_data);
    munit_assert(cmd_data[2].op == PG_GPU_COMMAND_DRAW_TRIANGLES);
    munit_assert_uint32(cmd_data[2].draw_triangles.range.x, ==, 2);
    munit_assert_uint32(cmd_data[2].draw_triangles.range.y, ==, 1);
    munit_assert_false(pg_gl_errors());
    ARR_DEINIT(cmds_arr);
    return MUNIT_OK;
}

/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_command_buffer_tests = {
    .prefix = "/gfx/command_buffer",
    .tests = (MunitTest[]) {
        {   .name = "/from_json",
            .setup = test_setup_video, .tear_down = test_tear_down_video,
            .test = test_command_buffer_from_json },
        {0} },
    .suites = (MunitSuite[]) {
        {0},
    }
};

