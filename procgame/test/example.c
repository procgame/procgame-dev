#include "ext/munit/munit.h"

static void* test_setup_example(const MunitParameter params[], void* user_data);
static void* test_tear_down_example(const MunitParameter params[], void* user_data);

static MunitResult test_a(const MunitParameter params[], void* data)
{
    munit_assert(true);
    return MUNIT_OK;
}

/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_example_tests = {
    .prefix = "/example",
    .tests = (MunitTest[]) {
        {   .name = "/test_a",
            .setup = test_setup_example, .tear_down = test_tear_down_example,
            .test = test_a },
    .suites = (MunitSuite[]) {
        {   .prefix = "/example_suite",
            .tests = (MunitTest[]) {
                {   .name = "/test_a",
                    .setup = test_setup_example, .tear_down = test_tear_down_example,
                    .test = test_a },
                {0} } },
        {0},
    }
};

static void* test_setup_example(const MunitParameter params[], void* user_data)
{
    return calloc(10, sizeof(int));
}

static void test_tear_down_example(void* fixture)
{
    free(fixture);
}


