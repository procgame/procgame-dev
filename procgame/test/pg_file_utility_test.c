#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"



static MunitResult test_filename_extension(const MunitParameter params[], void* data)
{
    /*  Not real filenames  */
    const char* test_cases[][2] = {
        { "example.txt",                "txt" },
        { "example.txt.0",              "0" },
        { "example.0.txt",              "txt" },
        { "example.mp4",                "mp4" },
        { "example_v0.1.0_full.mkv",    "mkv" },
        { "/home/example.txt",          "txt" },
        { "./example.foo.txt",          "txt" },
        { "../example.dk64.z64",        "z64" },
        { "/usr/example.d/64.1",        "1" },
        { "/usr/example.d/64.[]",       "[]" },
        { ".example.7z",                "7z" },
        { ".example",                   "example" },
        { "example",                    "" },
        { "/usr/example",               "" },
        { "/usr/bin.d/example",         "" },
    };
    const int n_test_cases = sizeof(test_cases) / sizeof(test_cases[0]);
    /*  Check every one */
    for(int i = 0; i < n_test_cases; ++i) {
        char ext_buf[PG_FILE_EXT_MAXLEN] = "";
        int len = pg_filename_extension(test_cases[i][0], PG_FILE_PATH_MAXLEN, ext_buf, PG_FILE_EXT_MAXLEN);
        fprintf(stderr, "%d: Filename: '%s', Expect extension: '%s', Got extension: '%s'\n",
                i, test_cases[i][0], test_cases[i][1], ext_buf);
        munit_assert_int(len, ==, strnlen(test_cases[i][1], PG_FILE_EXT_MAXLEN));
        munit_assert_string_equal(ext_buf, test_cases[i][1]);
    }
    return MUNIT_OK;
}

static MunitResult test_filename_basename(const MunitParameter params[], void* data)
{
    /*  Not real filenames  */
    const char* test_cases[][2] = {
        { "example.txt",                "example.txt" },
        { "example.txt.0",              "example.txt.0" },
        { "/home/example.txt",          "example.txt" },
        { "./example.foo.txt",          "example.foo.txt" },
        { "../example.dk64.z64",        "example.dk64.z64" },
        { "/usr/example.d/64.1",        "64.1" },
        { "/usr/example.d/64.[]",       "64.[]" },
        { ".example.7z",                ".example.7z" },
        { ".example",                   ".example" },
        { "example",                    "example" },
        { "/usr/example",               "example" },
        { "/usr/bin.d/example",         "example" },
        { "/usr/bin.d/",                "" },
        { "/",                          "" },
    };
    const int n_test_cases = sizeof(test_cases) / sizeof(test_cases[0]);
    /*  Check every one */
    for(int i = 0; i < n_test_cases; ++i) {
        char basename_buf[PG_FILE_NAME_MAXLEN] = "";
        int len = pg_filename_basename(test_cases[i][0], PG_FILE_PATH_MAXLEN, basename_buf, PG_FILE_NAME_MAXLEN);
        fprintf(stderr, "%d: Filename: '%s', Expect basename: '%s', Got basename: '%s'\n",
                i, test_cases[i][0], test_cases[i][1], basename_buf);
        munit_assert_int(len, ==, strnlen(test_cases[i][1], PG_FILE_NAME_MAXLEN));
        munit_assert_string_equal(basename_buf, test_cases[i][1]);
    }
    return MUNIT_OK;
}

static MunitResult test_filename_shortname(const MunitParameter params[], void* data)
{
    /*  Not real filenames  */
    const char* test_cases[][2] = {
        { "example.txt",                "example" },
        { "example.txt.0",              "example.txt" },
        { "/home/example.txt",          "example" },
        { "./example.foo.txt",          "example.foo" },
        { "../example.dk64.z64",        "example.dk64" },
        { "/usr/example.d/64.1",        "64" },
        { "/usr/example.d/64.[]",       "64" },
        { ".example.7z",                ".example" },
        { ".example",                   "" },
        { "example",                    "example" },
        { "/usr/example",               "example" },
        { "/usr/bin.d/example",         "example" },
        { "/usr/bin.d/",                "" },
        { "/",                          "" },
    };
    const int n_test_cases = sizeof(test_cases) / sizeof(test_cases[0]);
    /*  Check every one */
    for(int i = 0; i < n_test_cases; ++i) {
        char shortname_buf[PG_FILE_NAME_MAXLEN] = "";
        int len = pg_filename_shortname(test_cases[i][0], PG_FILE_PATH_MAXLEN, shortname_buf, PG_FILE_NAME_MAXLEN);
        fprintf(stderr, "%d: Filename: '%s', Expect shortname: '%s', Got shortname: '%s'\n",
                i, test_cases[i][0], test_cases[i][1], shortname_buf);
        munit_assert_int(len, ==, strnlen(test_cases[i][1], PG_FILE_NAME_MAXLEN));
        munit_assert_string_equal(shortname_buf, test_cases[i][1]);
    }
    return MUNIT_OK;
}

static MunitResult test_filename_path(const MunitParameter params[], void* data)
{
    /*  Not real filenames  */
    const char* test_cases[][2] = {
        { "example.txt",                "" },
        { "example.txt.0",              "" },
        { "/home/example.txt",          "/home/" },
        { "./example.foo.txt",          "./" },
        { "../example.dk64.z64",        "../" },
        { "/usr/example.d/64.1",        "/usr/example.d/" },
        { "/usr/example.d/64.[]",       "/usr/example.d/" },
        { ".example.7z",                "" },
        { ".example",                   "" },
        { "example",                    "" },
        { "/usr/example",               "/usr/" },
        { "/usr/bin.d/example",         "/usr/bin.d/" },
        { "/usr/bin.d/",                "/usr/bin.d/" },
        { "/",                          "/" },
    };
    const int n_test_cases = sizeof(test_cases) / sizeof(test_cases[0]);
    /*  Check every one */
    for(int i = 0; i < n_test_cases; ++i) {
        char path_buf[PG_FILE_PATH_MAXLEN] = "";
        int len = pg_filename_path(test_cases[i][0], PG_FILE_PATH_MAXLEN, path_buf, PG_FILE_PATH_MAXLEN);
        fprintf(stderr, "%d: Filename: '%s', Expect path: '%s', Got path: '%s'\n",
                i, test_cases[i][0], test_cases[i][1], path_buf);
        munit_assert_int(len, ==, strnlen(test_cases[i][1], PG_FILE_PATH_MAXLEN));
        if(len > 0) munit_assert_string_equal(path_buf, test_cases[i][1]);
    }
    return MUNIT_OK;
}

static MunitResult test_file_open(const MunitParameter params[], void* data)
{
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    munit_assert(result);
    munit_assert_size(file.size, >, 0);
    pg_file_close(&file);
    return MUNIT_OK;
}

static MunitResult test_file_open_noexist(const MunitParameter params[], void* data)
{
    pg_log_file(stderr);
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/does not exist.txt", "r");
    munit_assert_false(result);
    return MUNIT_OK;
}

static MunitResult test_file_open_search_paths(const MunitParameter params[], void* data)
{
    pg_log_file(stderr);
    const char* search_paths[] = {
        "./res/pg_file_utility/test_search_paths/foo/",
        "./res/pg_file_utility/test_search_paths/bar/baz/",
    };
    struct pg_file file;
    bool result = pg_file_open_search_paths(&file, search_paths, 2, "goal.txt", "r");
    munit_assert(result);
    pg_file_read_full(&file);
    munit_assert_size(file.size, ==, 8);
    munit_assert_memory_equal(8, file.content, "Success\n");
    pg_file_close(&file);
    return MUNIT_OK;
}

static MunitResult test_file_read_line(const MunitParameter params[], void* data)
{
    const char* expected_lines[] = { "Hello", "World", "Foo", "Bar" };
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    munit_assert(result);
    while(pg_file_read_line(&file)) {
        fprintf(stderr, "Line no: %d, Line: \"%s\", Line len: %d\n",
                file.line_no, file.line, (int)file.line_len);
        munit_assert_int(file.line_no, <, 4);
        munit_assert_int(file.line_len, ==, strnlen(expected_lines[file.line_no], 16));
        munit_assert_string_equal(file.line, expected_lines[file.line_no]);
    }
    pg_file_close(&file);
    return MUNIT_OK;
}

static MunitResult test_file_read_full(const MunitParameter params[], void* data)
{
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    munit_assert(result);
    pg_file_read_full(&file);
    munit_assert_not_null(file.content);
    munit_assert(file.own_content);
    munit_assert_memory_equal(5, file.content, "Hello");
    pg_file_close(&file);
    return MUNIT_OK;
}

static MunitResult test_file_take_content(const MunitParameter params[], void* data)
{
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    munit_assert(result);
    pg_file_read_full(&file);
    munit_assert_not_null(file.content);
    munit_assert(file.own_content);
    char* content = pg_file_take_content(&file);
    munit_assert_false(file.own_content);
    pg_file_close(&file);
    free(content);
    /*  If the above did not cause a double-free, then this test passes */
    return MUNIT_OK;
}

static MunitResult test_file_reset_read_line(const MunitParameter params[], void* data)
{
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    pg_file_read_line(&file);
    munit_assert_string_equal(file.line, "Hello");
    pg_file_read_line(&file);
    munit_assert_string_equal(file.line, "World");
    pg_file_reset(&file);
    munit_assert_string_equal(file.line, "");
    munit_assert_int(file.line_no, ==, 0);
    pg_file_read_line(&file);
    munit_assert_string_equal(file.line, "Hello");
    pg_file_close(&file);
    return MUNIT_OK;
}

static MunitResult test_file_reset_read_full(const MunitParameter params[], void* data)
{
    struct pg_file file;
    bool result = pg_file_open(&file, "./res/pg_file_utility/tëst file.txt", "r");
    pg_file_read_full(&file);
    char* content = pg_file_take_content(&file);
    pg_file_reset(&file);
    munit_assert_null(file.content);
    munit_assert_false(file.own_content);
    free(content);
    pg_file_close(&file);
    return MUNIT_OK;
}



static const char* test_read_file_with_includes_correct_result =
"Hello\n"
"World\n"
"\n"
"FooBar\n"
"\n"
"Hello\n"
"World\n"
"Goodbye\n"
"#world\n"
"\n"
"BarFoo\n"
"\n"
"Included!\n";

static MunitResult test_read_file_with_includes(const MunitParameter params[], void* data)
{
    const char* include_dirs[] = {
        "./res/pg_file_utility/test_include/",
        "./res/pg_file_utility/",
    };
    char_arr_t text;
    ARR_INIT(text);
    struct pg_file file;
    munit_assert(pg_file_open(&file, "./res/pg_file_utility/test_includes.txt", "r"));
    bool result = pg_file_read_preprocessed(&file, include_dirs, 2, &text);
    munit_assert(result);
    munit_assert_string_equal(text.data, test_read_file_with_includes_correct_result);
    ARR_DEINIT(text);
    return MUNIT_OK;
}



MunitSuite pg_file_utility_tests = {
    .prefix = "/util/file",
    .suites = (MunitSuite[]) {
        {   .prefix = "/path",
            .tests = (MunitTest[]) {
                {   .name = "/extension",
                    .test = test_filename_extension },
                {   .name = "/basename",
                    .test = test_filename_basename },
                {   .name = "/shortname",
                    .test = test_filename_shortname },
                {   .name = "/path",
                    .test = test_filename_path },
                {0} } },
        {   .prefix = "/io",
            .tests = (MunitTest[]) {
                {   .name = "/open",
                    .test = test_file_open },
                {   .name = "/open_noexist",
                    .test = test_file_open_noexist },
                {   .name = "/open_search_paths",
                    .test = test_file_open_search_paths },
                {   .name = "/read_line",
                    .test = test_file_read_line },
                {   .name = "/read_full",
                    .test = test_file_read_full },
                {   .name = "/read_with_includes",
                    .test = test_read_file_with_includes },
                {   .name = "/reset_read_line",
                    .test = test_file_reset_read_line },
                {   .name = "/reset_read_line",
                    .test = test_file_reset_read_full },
                {0} } },
        {0}
    }
};

