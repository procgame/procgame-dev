#include <stdio.h>
#include <stddef.h>
#include <assert.h>

#include "munit.h"

#include "pg_core/pg_core.h"
#include "pg_util/pg_util.h"
#include "pg_gpu/pg_gpu.h"

#include "helpers/gfx_test_utils.h"


/*  Test utilities  */
static void* test_setup_basic_shader(const MunitParameter params[], void* user_data);
static void test_tear_down_basic_shader(void* fixture);

/*  Static test files   */
static const char test_basic_shader_vs_filename[] = "./res/pg_gpu_shader/basic_vs.glsl";
static const char test_basic_shader_vs_filename_ne[] = "./res/pg_gpu_shader/does not exist.glsl";

/*  Static test data    */
static const struct pg_shader_interface_info test_basic_shader_iface;
static const char test_basic_shader_vs[];
static const char test_basic_shader_fs[];
static const char test_basic_shader_vs_wrong[];
static const char test_variants_shader_fs[];
static const char test_variants_shader_fs_wrong[];

/*  A shader test bed structure */
struct test_shader_base {
    struct pg_shader_source fs_source, vs_source;
    pg_gpu_shader_stage_t* fs_stage, * vs_stage;
    pg_gpu_shader_program_t* program;
};

/************************/
/*  Test functions      */
/************************/

/*  Shader sources      */

static MunitResult test_shader_source_from_memory(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh;
    pg_shader_source_from_memory(&sh, PG_GPU_VERTEX_SHADER,
                    test_basic_shader_vs, strlen(test_basic_shader_vs));
    munit_assert_int(sh.type, ==, PG_GPU_VERTEX_SHADER);
    munit_assert_int(sh.src_len, ==, strlen(test_basic_shader_vs));
    pg_shader_source_deinit(&sh);
    return MUNIT_OK;
}

static MunitResult test_shader_source_from_file(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh;
    struct pg_file basic_shader_vs_file;
    munit_assert(pg_file_open(&basic_shader_vs_file, test_basic_shader_vs_filename, "r"));
    munit_assert(pg_shader_source_from_file(&sh, PG_GPU_VERTEX_SHADER, &basic_shader_vs_file));
    munit_assert_int(sh.type, ==, PG_GPU_VERTEX_SHADER);
    munit_assert_int(sh.src_len, >, 0);
    pg_file_close(&basic_shader_vs_file);
    pg_shader_source_deinit(&sh);
    return MUNIT_OK;
}

static MunitResult test_shader_source_from_file_with_includes(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh_includes;
    struct pg_shader_source sh_no_includes;
    const char* include_dirs[] = {
        "./res/pg_gpu_shader/include/",
    };
    struct pg_file sh_no_includes_file, sh_includes_file;
    munit_assert(pg_file_open(&sh_no_includes_file, "./res/pg_gpu_shader/basic_includes_vs.glsl", "r"));
    munit_assert(pg_file_open(&sh_includes_file, "./res/pg_gpu_shader/basic_includes_vs.glsl", "r"));
    munit_assert(pg_shader_source_from_file(&sh_no_includes, PG_GPU_VERTEX_SHADER, &sh_no_includes_file));
    munit_assert(pg_shader_source_from_file_with_includes(&sh_includes, PG_GPU_VERTEX_SHADER, &sh_includes_file, include_dirs, 1));
    munit_assert_int(sh_includes.type, ==, PG_GPU_VERTEX_SHADER);
    munit_assert_int(sh_includes.src_len, >, sh_no_includes.src_len);
    pg_file_close(&sh_no_includes_file);
    pg_file_close(&sh_includes_file);
    pg_shader_source_deinit(&sh_includes);
    pg_shader_source_deinit(&sh_no_includes);
    return MUNIT_OK;
}

static MunitResult test_shader_source_from_file_noexist(const MunitParameter params[], void* data)
{
    pg_log_file(stderr);
    struct pg_shader_source sh;
    struct pg_file file;
    munit_assert_false(pg_file_open(&file, test_basic_shader_vs_filename_ne, "r"));
    munit_assert_false(pg_shader_source_from_file(&sh, PG_GPU_VERTEX_SHADER, &file));
    pg_shader_source_deinit(&sh);
    return MUNIT_OK;
}

static MunitResult test_shader_source_variation_count(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh;
    pg_shader_source_from_memory(&sh, PG_GPU_FRAGMENT_SHADER,
            test_variants_shader_fs, strlen(test_variants_shader_fs));
    int n_variations = pg_shader_source_variation_count(&sh);
    munit_assert_int(n_variations, ==, 2);
    pg_shader_source_deinit(&sh);
    return MUNIT_OK;
}


/*  Shader stages       */

static MunitResult test_shader_stage_compile(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh_source;
    pg_shader_source_from_memory(&sh_source, PG_GPU_VERTEX_SHADER,
            test_basic_shader_vs, strlen(test_basic_shader_vs));
    pg_gpu_shader_stage_t* sh_stage = pg_shader_source_compile(&sh_source);
    fprintf(stderr, "%s", pg_gpu_shader_stage_log(sh_stage));
    munit_assert(pg_gpu_shader_stage_status(sh_stage));
    pg_gpu_shader_stage_destroy(sh_stage);
    pg_shader_source_deinit(&sh_source);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_stage_compile_wrong(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh_source;
    pg_shader_source_from_memory(&sh_source, PG_GPU_VERTEX_SHADER,
            test_basic_shader_vs_wrong, strlen(test_basic_shader_vs_wrong));
    pg_gpu_shader_stage_t* sh_stage = pg_shader_source_compile(&sh_source);
    fprintf(stderr, "%s", pg_gpu_shader_stage_log(sh_stage));
    munit_assert_false(pg_gpu_shader_stage_status(sh_stage));
    pg_shader_source_deinit(&sh_source);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_stage_compile_variant(const MunitParameter params[], void* data)
{
    struct pg_shader_source sh_source;
    pg_shader_source_from_memory(&sh_source, PG_GPU_FRAGMENT_SHADER,
            test_variants_shader_fs, strlen(test_variants_shader_fs));
    /*  This shader should fail to compile without a variant disabled   */
    pg_gpu_shader_stage_t* sh_stage = pg_shader_source_compile(&sh_source);
    fprintf(stderr, "%s", pg_gpu_shader_stage_log(sh_stage));
    munit_assert(pg_gpu_shader_stage_status(sh_stage));
    pg_gpu_shader_stage_destroy(sh_stage);
    pg_shader_source_deinit(&sh_source);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

/*  Shader programs     */

static MunitResult test_shader_program_init(const MunitParameter params[], void* data)
{
    struct pg_shader_source fs_source, vs_source;
    pg_shader_source_from_memory(&vs_source, PG_GPU_VERTEX_SHADER,
            test_basic_shader_vs, strlen(test_basic_shader_vs));
    pg_shader_source_from_memory(&fs_source, PG_GPU_FRAGMENT_SHADER,
            test_basic_shader_fs, strlen(test_basic_shader_fs));
    pg_gpu_shader_stage_t* fs_stage = pg_shader_source_compile(&fs_source);
    pg_gpu_shader_stage_t* vs_stage = pg_shader_source_compile(&vs_source);
    munit_assert(pg_gpu_shader_stage_status(fs_stage));
    munit_assert(pg_gpu_shader_stage_status(vs_stage));
    pg_gpu_shader_stage_t* stages[] = { fs_stage, vs_stage };

    pg_gpu_shader_program_t* sh_prog = pg_gpu_shader_program_init(stages, 2, &test_basic_shader_iface);
    munit_assert(pg_gpu_shader_program_status(sh_prog));
    pg_shader_source_deinit(&fs_source);
    pg_shader_source_deinit(&vs_source);
    pg_gpu_shader_stage_destroy(fs_stage);
    pg_gpu_shader_stage_destroy(vs_stage);
    pg_gpu_shader_program_destroy(sh_prog);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_init_wrong(const MunitParameter params[], void* data)
{
    struct pg_shader_source fs_source, vs_source_wrong;
    /*  Here we 'accidentally' compile two fragment shaders */
    pg_shader_source_from_memory(&vs_source_wrong, PG_GPU_FRAGMENT_SHADER,
            test_basic_shader_fs, strlen(test_basic_shader_fs));
    pg_shader_source_from_memory(&fs_source, PG_GPU_FRAGMENT_SHADER,
            test_basic_shader_fs, strlen(test_basic_shader_fs));
    pg_gpu_shader_stage_t* fs_stage = pg_shader_source_compile(&fs_source);
    pg_gpu_shader_stage_t* vs_stage = pg_shader_source_compile(&vs_source_wrong);
    munit_assert(pg_gpu_shader_stage_status(fs_stage));
    munit_assert(pg_gpu_shader_stage_status(vs_stage));
    pg_gpu_shader_stage_t* stages[] = { fs_stage, vs_stage };
    pg_gpu_shader_program_t* sh_prog = pg_gpu_shader_program_init(stages, 2, NULL);
    /*  Should fail to init because two fragment shaders is an invalid program  */
    munit_assert_false(pg_gpu_shader_program_status(sh_prog));
    pg_shader_source_deinit(&fs_source);
    pg_shader_source_deinit(&vs_source_wrong);
    pg_gpu_shader_stage_destroy(fs_stage);
    pg_gpu_shader_stage_destroy(vs_stage);
    pg_gpu_shader_program_destroy(sh_prog);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_use(const MunitParameter params[], void* data)
{
    struct test_shader_base* base = data;
    pg_gpu_shader_program_use(base->program);
    GLint active_program;
    glGetIntegerv(GL_CURRENT_PROGRAM, &active_program);
    munit_assert_int(active_program, ==, pg_gpu_shader_program_get_handle(base->program));
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

/*  Shader input modules    */

static MunitResult test_shader_program_uniform_info(const MunitParameter params[], void* data)
{
    struct test_shader_base* base = data;
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(base->program);
    munit_assert_not_null(sh_iface_info);
    munit_assert_int(sh_iface_info->uniform_inputs.len, ==, 2);
    int texture_uni_idx = -1, instance_uni_idx = -1;
    int i;
    const struct pg_shader_uniform_info* uni_info;
    ARR_FOREACH_PTR(sh_iface_info->uniform_inputs, uni_info, i) {
        if(strcmp(uni_info->name, "tex_diffuse") == 0) {
            munit_assert(uni_info->type == PG_SHADER_UNIFORM_TEXTURE);
            munit_assert_int(texture_uni_idx, ==, -1);
            texture_uni_idx = i;
        } else if(strcmp(uni_info->name, "instance_buffer") == 0) {
            munit_assert(uni_info->type == PG_SHADER_UNIFORM_TEXTURE_BUFFER);
            munit_assert_int(instance_uni_idx, ==, -1);
            instance_uni_idx = i;
        }
    }
    munit_assert_int(texture_uni_idx, !=, -1);
    munit_assert_int(instance_uni_idx, !=, -1);
    int queried_texture_uni_idx = pg_shader_interface_info_get_uniform_index(sh_iface_info, "tex_diffuse");
    int queried_instance_uni_idx = pg_shader_interface_info_get_uniform_index(sh_iface_info, "instance_buffer");
    munit_assert_int(queried_texture_uni_idx, ==, texture_uni_idx);
    munit_assert_int(queried_instance_uni_idx, ==, instance_uni_idx);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_vertex_interface_info(const MunitParameter params[], void* data)
{
    struct test_shader_base* base = data;
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(base->program);
    munit_assert_not_null(sh_iface_info);
    munit_assert_int(sh_iface_info->vertex_inputs.len, ==, 2);
    bool found_position_attrib = false, found_weight_attrib = false;
    int i;
    const struct pg_shader_vertex_info* vert_info;
    ARR_FOREACH_PTR(sh_iface_info->vertex_inputs, vert_info, i) {
        if(strcmp(vert_info->name, "position") == 0) {
            munit_assert(vert_info->type == PG_VEC3);
            munit_assert_false(found_position_attrib);
            found_position_attrib = true;
        } else if(strcmp(vert_info->name, "weight") == 0) {
            munit_assert(vert_info->type == PG_FLOAT);
            munit_assert_false(found_weight_attrib);
            found_weight_attrib = true;
        }
    }
    munit_assert(found_position_attrib);
    munit_assert(found_weight_attrib);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_asset_loader(const MunitParameter params[], void* data)
{
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_shader_program_asset_loader());
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr,
            "(pg_gpu_shader_program)foo:test_gpu_shader_program_asset.json", NULL);
    struct pg_gpu_shader_program* gpu_sh_prog = pg_asset_get_data(asset);
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(gpu_sh_prog);
    int pos_index = pg_shader_interface_info_get_vertex_index(sh_iface_info, "position");
    int uv_index = pg_shader_interface_info_get_vertex_index(sh_iface_info, "uv_mat");
    int tex_diffuse_index = pg_shader_interface_info_get_uniform_index(sh_iface_info, "tex_diffuse");
    int color_index = pg_shader_interface_info_get_output_index(sh_iface_info, "color");
    munit_assert_int(pos_index, !=, -1);
    munit_assert_int(uv_index, !=, -1);
    munit_assert_int(tex_diffuse_index, !=, -1);
    munit_assert_int(color_index, !=, -1);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_asset_uniform_info(const MunitParameter params[], void* data)
{
    /*  Load an asset   */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_shader_program_asset_loader());
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr,
            "(pg_gpu_shader_program)foo:test_gpu_shader_program_asset.json", NULL);
    struct pg_gpu_shader_program* gpu_sh_prog = pg_asset_get_data(asset);
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(gpu_sh_prog);
    /*  Check the uniform inputs    */
    munit_assert_int(sh_iface_info->uniform_inputs.len, ==, 1);
    int texture_uni_idx = -1, instance_uni_idx = -1;
    int i;
    const struct pg_shader_uniform_info* uni_info;
    ARR_FOREACH_PTR(sh_iface_info->uniform_inputs, uni_info, i) {
        if(strcmp(uni_info->name, "tex_diffuse") == 0) {
            munit_assert_int(uni_info->type, ==, PG_SHADER_UNIFORM_TEXTURE);
            munit_assert_int(texture_uni_idx, ==, -1);
            texture_uni_idx = i;
        } else if(strcmp(uni_info->name, "viewport") == 0) {
            munit_assert_int(uni_info->type, ==, PG_SHADER_UNIFORM_BUFFER);
            munit_assert_int(instance_uni_idx, ==, -1);
            instance_uni_idx = i;
        }
    }
    munit_assert_int(texture_uni_idx, !=, -1);
    /*  Check that the queried index matches the index in the array */
    int queried_texture_uni_idx = pg_shader_interface_info_get_uniform_index(sh_iface_info, "tex_diffuse");
    munit_assert_int(queried_texture_uni_idx, ==, texture_uni_idx);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_asset_vertex_info(const MunitParameter params[], void* data)
{
    /*  Load an asset   */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_shader_program_asset_loader());
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr,
            "(pg_gpu_shader_program)foo:test_gpu_shader_program_asset.json", NULL);
    struct pg_gpu_shader_program* gpu_sh_prog = pg_asset_get_data(asset);
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(gpu_sh_prog);
    /*  Check the uniform inputs    */
    munit_assert_int(sh_iface_info->uniform_inputs.len, ==, 1);
    int position_vert_idx = -1;
    int uv_mat_vert_idx = -1;
    int i;
    const struct pg_shader_vertex_info* vert_info;
    ARR_FOREACH_PTR(sh_iface_info->vertex_inputs, vert_info, i) {
        if(strcmp(vert_info->name, "position") == 0) {
            munit_assert(vert_info->type == PG_VEC3);
            munit_assert_int(position_vert_idx, ==, -1);
            position_vert_idx = i;
        }
        if(strcmp(vert_info->name, "uv_mat") == 0) {
            munit_assert(vert_info->type == PG_VEC3);
            munit_assert_int(uv_mat_vert_idx, ==, -1);
            uv_mat_vert_idx = i;
        }
    }
    munit_assert_int(position_vert_idx, !=, -1);
    munit_assert_int(uv_mat_vert_idx, !=, -1);
    /*  Check that the queried index matches the index in the array */
    int queried_position_vert_idx = pg_shader_interface_info_get_vertex_index(sh_iface_info, "position");
    int queried_uv_mat_vert_idx = pg_shader_interface_info_get_vertex_index(sh_iface_info, "uv_mat");
    munit_assert_int(queried_position_vert_idx, ==, position_vert_idx);
    munit_assert_int(queried_uv_mat_vert_idx, ==, uv_mat_vert_idx);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}

static MunitResult test_shader_program_asset_output_info(const MunitParameter params[], void* data)
{
    /*  Load an asset   */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_asset_manager_add_search_path(asset_mgr, "./res/pg_asset/gpu/");
    pg_asset_manager_add_loader(asset_mgr, pg_gpu_shader_program_asset_loader());
    pg_asset_handle_t asset = pg_asset_from_AID(asset_mgr,
            "(pg_gpu_shader_program)foo:test_gpu_shader_program_asset.json", NULL);
    struct pg_gpu_shader_program* gpu_sh_prog = pg_asset_get_data(asset);
    const struct pg_shader_interface_info* sh_iface_info = pg_gpu_shader_program_interface_info(gpu_sh_prog);
    /*  Check the outputs   */
    munit_assert_int(sh_iface_info->output_layout.len, ==, 2);
    int color_output_idx = -1, depth_output_idx = -1;
    int i;
    const struct pg_shader_output_info* output_info;
    ARR_FOREACH_PTR(sh_iface_info->output_layout, output_info, i) {
        if(strcmp(output_info->name, "color") == 0) {
            munit_assert(output_info->location == PG_GPU_FRAMEBUFFER_COLOR0);
            munit_assert(output_info->type == PG_VEC4);
            munit_assert_int(color_output_idx, ==, -1);
            color_output_idx = i;
        } else if(strcmp(output_info->name, "depth") == 0) {
            munit_assert(output_info->location == PG_GPU_FRAMEBUFFER_DEPTH);
            munit_assert(output_info->type == PG_FLOAT);
            munit_assert_int(depth_output_idx, ==, -1);
            depth_output_idx = i;
        }
    }
    munit_assert_int(color_output_idx, !=, -1);
    munit_assert_int(depth_output_idx, !=, -1);
    /*  Check that the queried index matches the index in the array */
    int queried_color_output_idx = pg_shader_interface_info_get_output_index(sh_iface_info, "color");
    int queried_depth_output_idx = pg_shader_interface_info_get_output_index(sh_iface_info, "depth");
    munit_assert_int(queried_color_output_idx, ==, color_output_idx);
    munit_assert_int(queried_depth_output_idx, ==, depth_output_idx);
    pg_asset_manager_destroy(asset_mgr);
    munit_assert_false(pg_gl_errors());
    return MUNIT_OK;
}



/****************************/
/*  Test suite definition   */
/****************************/

MunitSuite pg_shader_tests = {
    .prefix = "/gfx/shader",
    .suites = (MunitSuite[]) {
        {   .prefix = "/sources",
            .tests = (MunitTest[]) {
                {   .name = "/from_memory",
                    .test = test_shader_source_from_memory },
                {   .name = "/from_file",
                    .test = test_shader_source_from_file },
                {   .name = "/from_file_with_includes",
                    .test = test_shader_source_from_file_with_includes },
                {   .name = "/from_file_noexist",
                    .test = test_shader_source_from_file_noexist },
                {   .name = "/variation_count",
                    .test = test_shader_source_variation_count },
                {0}, } },
        {   .prefix = "/stages",
            .tests = (MunitTest[]) {
                {   .name = "/compile",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_stage_compile },
                {   .name = "/compile_wrong",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_stage_compile_wrong },
                {   .name = "/compile_variant",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_stage_compile_variant },
                {0}, } },
        {   .prefix = "/programs",
            .tests = (MunitTest[]) {
                {   .name = "/asset_loader",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_asset_loader },
                {   .name = "/asset_uniform_info",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_asset_uniform_info },
                {   .name = "/asset_vertex_info",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_asset_vertex_info },
                {   .name = "/asset_output_info",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_asset_output_info },
                {   .name = "/init",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_init },
                {   .name = "/init_wrong",
                    .setup = test_setup_video, .tear_down = test_tear_down_video,
                    .test = test_shader_program_init_wrong },
                {   .name = "/use",
                    .setup = test_setup_basic_shader, .tear_down = test_tear_down_basic_shader,
                    .test = test_shader_program_use },
                {0}, } },
        {   .prefix = "/input",
            .tests = (MunitTest[]) {
                {   .name = "/uniform_info",
                    .setup = test_setup_basic_shader, .tear_down = test_tear_down_basic_shader,
                    .test = test_shader_program_uniform_info },
                {   .name = "/vertex_interface_info",
                    .setup = test_setup_basic_shader, .tear_down = test_tear_down_basic_shader,
                    .test = test_shader_program_vertex_interface_info },
                {0}, } },
        {0},
    },
};

/****************************/
/*  Static definitions      */
/****************************/

static void* test_setup_basic_shader(const MunitParameter params[], void* user_data)
{
    test_setup_video(params, user_data);

    struct test_shader_base* base = malloc(sizeof(*base));
    pg_shader_source_from_memory(&base->vs_source, PG_GPU_VERTEX_SHADER, test_basic_shader_vs, strlen(test_basic_shader_vs));
    pg_shader_source_from_memory(&base->fs_source, PG_GPU_FRAGMENT_SHADER, test_basic_shader_fs, strlen(test_basic_shader_fs));
    base->fs_stage = pg_shader_source_compile(&base->fs_source);
    base->vs_stage = pg_shader_source_compile(&base->vs_source);
    pg_gpu_shader_stage_t* stages[] = { base->fs_stage, base->vs_stage };

    base->program = pg_gpu_shader_program_init(stages, 2, &test_basic_shader_iface);

    return base;
}

static void test_tear_down_basic_shader(void* fixture)
{
    struct test_shader_base* base = fixture;
    pg_shader_source_deinit(&base->fs_source);
    pg_shader_source_deinit(&base->vs_source);
    pg_gpu_shader_stage_destroy(base->fs_stage);
    pg_gpu_shader_stage_destroy(base->vs_stage);
    pg_gpu_shader_program_destroy(base->program);
    free(base);
    test_tear_down_video(NULL);
}

static const struct pg_shader_interface_info test_basic_shader_iface = PG_SHADER_INTERFACE_INFO(
    PG_SHADER_INTERFACE_UNIFORM_INFO(2,
        { .name = "instance_buffer", .location = 0, .count = 1, .type = PG_SHADER_UNIFORM_TEXTURE_BUFFER },
        { .name = "tex_diffuse", .location = 1, .count = 1, .type = PG_SHADER_UNIFORM_TEXTURE },
    ),
    PG_SHADER_INTERFACE_VERTEX_INFO(2,
        { .name = "position", .location = 0, .type = PG_VEC3 },
        { .name = "weight", .location = 1, .type = PG_FLOAT },
    ),
    PG_SHADER_INTERFACE_OUTPUT_INFO(2,
        { .name = "output_color", .location = PG_GPU_FRAMEBUFFER_COLOR0, .type = PG_VEC4 },
        { .name = "depth", .location = PG_GPU_FRAMEBUFFER_DEPTH, .type = PG_FLOAT },
    )
);

static const char test_basic_shader_vs[] =
    "#version 430\n"
    "layout(location = 0) uniform samplerBuffer instance_buffer;\n"
    "layout(location = 0) in vec3 position;\n"
    "layout(location = 1) in float weight;\n"
    "void main() {\n"
    "   float scale = texelFetch(instance_buffer, 0).r;\n"
    "   gl_Position = (scale * weight) * vec4(position.xyz, 1);\n"
    "}\n";

static const char test_basic_shader_fs[] =
    "#version 430\n"
    "layout(location = 1) uniform sampler2DArray tex_diffuse;\n"
    "layout(location = 0) out vec4 output_color;\n"
    "void main() {\n"
    "   output_color = texture(tex_diffuse, vec3(0,0,0));\n"
    "}\n";

static const char test_basic_shader_vs_wrong[] =
    "#version 430\n"
    "layout(location = 0) uniform FOOBAR! transform;\n"
    "layout(location = 0) in vec3 v_position;\n"
    "void main() {\n"
    "   gl_Position = transform * vec4(v_position.xyz, 1);\n"
    "}\n";

static const char test_variants_shader_fs[] =
    "#version 430\n"
    "//!pg_variant VERTEX_NORMAL\n"
    "layout(location = 0) in vec3 v_normal;\n"
    "//!pg_end_variant\n"
    "layout(location = 0) out vec4 output_color;\n"
    "//!pg_variant OUTPUT_NORMAL\n"
    "layout(location = 1) out vec4 output_normal;\n"
    "//!pg_end_variant\n"
    "void main() {\n"
    "   output_color = vec4(1,0,0,1);\n"
    "}\n";

static const char test_variants_shader_fs_wrong[] =
    "#version 430\n"
    "layout(location = 0) out vec4 output_color;\n"
    "//!pg_variant ERROR\n"
    "foobar!!!"
    "//!pg_end_variant\n"
    "void main() {\n"
    "   output_color = vec4(1,0,0,1);\n"
    "}\n";



