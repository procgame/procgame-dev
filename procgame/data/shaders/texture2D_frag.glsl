#version 450

/*  Texture samplers    */
layout(location = 1, binding = 0) uniform sampler2D image_texture;

/*  Pipeline interface  */
layout(location = 0) in vec4 f_tex_coord;

/*  Output  */
layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 tex_color = texture(image_texture, f_tex_coord.xy);
    frag_color = tex_color.rgba;
}



