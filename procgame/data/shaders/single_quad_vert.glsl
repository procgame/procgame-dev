#version 450

/*  Static vertex data (position and UV, the latter with a slight margin)   */
const vec4 verts[6] = vec4[](
           vec4(-0.5, 0.5, 0, 1),
           vec4(-0.5, -0.5,  0, 0),
           vec4(0.5, 0.5,  1, 1),
           vec4(0.5, 0.5,  1, 1),
           vec4(-0.5, -0.5,  0, 0),
           vec4(0.5, -0.5,   1, 0));

layout(location = 0) uniform mat4 mvp_matrix;

/*  Pipeline interface  */
layout(location = 0) out vec4 f_tex_coord;


void main()
{
    /*  Get untransformed vertex data   */
    const int vert_index = gl_VertexID % 6;
    const vec4 quad_vert = verts[vert_index];

    /*  Transform vertex data according to quad info    */
    const vec3 vert_pos = vec3(quad_vert.xy, 0);
    const vec4 final_vert = mvp_matrix * vec4(vert_pos.xyz, 1);
    gl_Position = final_vert;

    f_tex_coord = vec4(quad_vert.zw, 0, 0);
}


