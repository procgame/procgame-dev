#version 450

/*  Texture samplers    */
layout(location = 4, binding = 1) uniform sampler2DArray image_texture;
layout(location = 5, binding = 2) uniform sampler2DArray font_texture;

/*  Alpha values below this will be discarded   */
layout(location = 6) uniform float alpha_cutoff;

/*  Pipeline interface  */
layout(location = 0) in vec4 f_tex_coord;
layout(location = 1) in vec4 f_color_mod;
layout(location = 2) in vec4 f_color_add;

/*  Output  */
layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 tex_color;

    /*  Select between font or image texture    */
    if(f_tex_coord.w != 0) tex_color = texture(font_texture, f_tex_coord.xyz);
    else tex_color = texture(image_texture, f_tex_coord.xyz);

    /*  transform sampled color according to quad info  */
    tex_color = tex_color * f_color_mod + f_color_add;

    /*  discard fully transparent fragments */
    if(tex_color.a <= alpha_cutoff) discard;

    frag_color = tex_color.rgba;
}


