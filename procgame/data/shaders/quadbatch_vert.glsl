#version 450

/*  Static vertex data (position and UV, the latter with a slight margin)   */
const vec4 verts[6] = vec4[](
           vec4(-0.5, 0.5, 0.00001, 0.00001),
           vec4(-0.5, -0.5,  0.00001, 0.99999),
           vec4(0.5, 0.5,  0.99999, 0.00001),
           vec4(0.5, 0.5,  0.99999, 0.00001),
           vec4(-0.5, -0.5,  0.00001, 0.99999),
           vec4(0.5, -0.5,   0.99999, 0.99999));


/*  Texture buffer to take per-quad data from   */
layout(location = 0, binding = 0) uniform samplerBuffer quads_buffer;

/*  Dynamic uniforms for per-batch info */
layout(location = 1) uniform mat4 projview_matrix;
layout(location = 2) uniform mat4 batch_transform;
layout(location = 3) uniform int batch_base_index;

/*  Pipeline interface  */
layout(location = 0) out vec4 f_tex_coord;
layout(location = 1) out vec4 f_color_mod;
layout(location = 2) out vec4 f_color_add;



/*  Helper functions    */
vec4 unpack_uint(uint u)
{
    return vec4(
        float((u & uint(0xFF000000)) >> 24),
        float((u & uint(0x00FF0000)) >> 16),
        float((u & uint(0x0000FF00)) >> 8),
        float((u & uint(0x000000FF)) >> 0)) / 255;
}

vec3 quat_apply(vec4 q, vec3 v)
{
    return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}



void main()
{
    /*  Read (packed) quad data from buffer   */
    const int quad_index = gl_VertexID / 6 * 4;
    const int vert_index = gl_VertexID % 6;
    const vec4 batch[4] = vec4[](
        vec4(texelFetch(quads_buffer, batch_base_index + quad_index + 0)),
        vec4(texelFetch(quads_buffer, batch_base_index + quad_index + 1)),
        vec4(texelFetch(quads_buffer, batch_base_index + quad_index + 2)),
        vec4(texelFetch(quads_buffer, batch_base_index + quad_index + 3)) );

    /*  Unpack data from packed texel data  */
    const vec3 pos = batch[0].xyz;
    const vec2 scale = vec2(batch[0].w, batch[1].x);
    const vec4 orientation = batch[3];
    const uint tex_opts = floatBitsToUint(batch[1].w);
    const float tex_layer =  float(tex_opts & uint(0x0000FFFF));
    const float tex_select = float(tex_opts & uint(0xFFFF0000));
    const vec4 tex_frame = batch[2];
    f_color_add = unpack_uint(floatBitsToUint(batch[1].y));
    f_color_mod = unpack_uint(floatBitsToUint(batch[1].z));

    /*  Get untransformed vertex data   */
    const vec4 quad_vert = verts[vert_index];

    /*  Transform vertex data according to quad info    */
    const vec3 vert_pos = vec3(quad_vert.xy * scale.xy, 0);
    const vec3 rotated_vert = quat_apply(orientation, vert_pos);
    const vec3 translated_vert = pos + rotated_vert;
    const mat4 final_transform = projview_matrix * batch_transform;
    const vec4 final_vert = final_transform * vec4(translated_vert.xyz, 1);
    gl_Position = final_vert;

    f_tex_coord = vec4(
        mix(tex_frame.x, tex_frame.z, quad_vert.z),
        mix(tex_frame.y, tex_frame.w, quad_vert.w),
        tex_layer, tex_select);
}


