#!BPY
import bpy
import json
import os

bl_info = {
    "name": "Procgame: Export Model as JSON",
    "description": "Export a model as JSON, in the form of vertex attributes and array.",
    "author": "Joshua Giles",
    "version": (1, 1),
    "blender": (2, 90, 0),
    "category": "Import-Export"
}

# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator

class ExportProcgameAssets(Operator, ExportHelper):
    """Export a procgame assets file containing the model and texture files"""
    bl_idname = "procgame.export_json_model"
    bl_label = "Procgame: JSON Model"

    # ExportHelper mixin class uses this
    filename_ext = ".json"

    filter_glob = StringProperty(
            default="*.json",
            options={'HIDDEN'},
            maxlen=255,  # Max internal buffer length, longer would be clamped.
            )

    # JSON option (doesn't change written data)
    export_readable = BoolProperty(name = "Human-readable JSON")

    # Which vertex attributes to include
    should_export_uv = BoolProperty(name = "Export UV coords")
    should_export_normals = BoolProperty(name = "Export normals")
    should_export_tanspace = BoolProperty(name = "Export tangents/bitangents")
    should_export_rigging = BoolProperty(name = "Export bone weights")
    asset_name = StringProperty(name="Procgame asset name")

    # Optionally include filenames of textures in output JSON
    should_export_tex_filenames = BoolProperty(name = "Export texture filenames")
    tex_prefix = StringProperty(name="Texture file prefix", subtype='FILE_NAME')

    # Write procgame assets associated with model geometry
    def add_model_asset(self, model, name, tex_out, vbuf_out, idxbuf_out):
        # Output exported texture paths
        uv_offset = 0
        if self.should_export_tex_filenames:
            img_path = os.path.dirname(self.filepath)
            uv_offset = len(tex_out["contents"])
            for node in model.active_material.node_tree.nodes:
                if node.label == "img_export":
                    tex_out["contents"].append("{}{}".format(self.tex_prefix, node.image.name_full))
                    node.image.filepath_raw = "{}/{}".format(img_path, node.image.name_full)
                    node.image.save()
                    break
            for node in model.active_material.node_tree.nodes:
                if node.label == "norm_export":
                    tex_out["contents"].append("{}{}".format(self.tex_prefix, node.image.name_full))
                    node.image.filepath_raw = "{}/{}".format(img_path, node.image.name_full)
                    node.image.save()
                    break

        # Fill attributes in vertex buffer layout
        vbuf_attribs = vbuf_out["layout"]["attributes"]
        vbuf_attribs.append({ "name": "v_position", "type": "vec3" })
        if self.should_export_normals:
            vbuf_attribs.append({ "name": "v_normal", "type": "vec3" })
        if self.should_export_tanspace:
            model.data.calc_tangents()
            vbuf_attribs.append({ "name": "v_tangent", "type": "vec3" })
            vbuf_attribs.append({ "name": "v_bitangent", "type": "vec3" })
        if self.should_export_rigging:
            vbuf_attribs.append({ "name": "v_rigging", "type": "vec3" })
        if self.should_export_uv:
            vbuf_attribs.append({ "name": "v_tex_coord", "type": "vec3" })

        # Add vertices and indices to the data buffers
        vbuf_data = vbuf_out["data"]
        idxbuf_data = idxbuf_out["data"]
        vbuf_offset = len(vbuf_data)
        idxbuf_offset = len(idxbuf_data)
        last_face = len(model.data.polygons) - 1
        for face_idx, face in enumerate(model.data.polygons):
            for vert_idx, loop_idx in zip(face.vertices, face.loop_indices):
                loop = model.data.loops[loop_idx]
                vert = model.data.vertices[vert_idx]
                vertex = []

                # Add position
                vertex.append((vert.co.x, vert.co.y, vert.co.z))

                # Add normal
                if self.should_export_normals:
                    vertex.append((vert.normal.x, vert.normal.y, vert.normal.z))

                # Add tangent/bitangent
                if self.should_export_tanspace:
                    vertex.append((loop.tangent.x, loop.tangent.y, loop.tangent.z))
                    vertex.append((loop.bitangent.x, loop.bitangent.y, loop.bitangent.z))

                # Add UV coords
                if self.should_export_uv:
                    v_uv = model.data.uv_layers[0].data[loop_idx].uv
                    vertex.append((v_uv.x, 1.0 - v_uv.y, uv_offset))

                # Add bone weights
                if self.should_export_rigging:
                    if len(vert.groups) > 0:
                        bone0 = vert.groups[0].group
                        bone_weight = vert.groups[0].weight
                        if len(vert.groups) > 1:
                            bone1 = vert.groups[1].group
                        else:
                            bone1 = bone0
                    else:
                        bone0 = 0
                        bone1 = 0
                        bone_weight = 0
                    vertex.append((bone0, bone1, bone_weight))

                # Check if this is a duplicated vertex and deduplicate if so
                found_dup_vert = False
                for past_vert_idx, past_vert in enumerate(vbuf_data[vbuf_offset:]):
                    if past_vert == vertex:
                        found_dup_vert = True
                        idxbuf_data.append(past_vert_idx)
                        break
                if not found_dup_vert:
                    idxbuf_data.append(len(vbuf_data) - vbuf_offset)
                    vbuf_data.append(vertex)

        vbuf_out["ranges"].append({"name": name, "first": vbuf_offset, "count": len(vbuf_data) - vbuf_offset})
        idxbuf_out["ranges"].append({"name": name, "first": idxbuf_offset, "count": len(idxbuf_data) - idxbuf_offset})

        # Record the number of items we wrote
        vbuf_out["capacity"] = len(vbuf_data)
        idxbuf_out["capacity"] = len(idxbuf_data)


    def execute(self, context):
        print("Exporting Procgame Model to: {}".format(self.filepath))

        # Single array texture to hold all exported textures
        texture_asset = {
            "__pg_asset_type": "pg_gpu_texture",
            "__pg_asset_name": self.asset_name + ".texture",
            "contents": [ ]
        }

        # Vertex buffer to hold all exported models
        vertex_buffer_asset = {
            "__pg_asset_type": "pg_gpu_buffer",
            "__pg_asset_name": self.asset_name + ".vertex",
            "layout": {
                "attribute_packing": "4byte",
                "element_alignment": 4,
                "attributes": []
            },
            "type": "vertex_buffer",
            "ranges": [],
            "capacity": 0,
            "data": []
        }
        
        # Index buffer
        index_buffer_asset = {
            "__pg_asset_type": "pg_gpu_buffer",
            "__pg_asset_name": self.asset_name + ".index",
            "layout": {
                "attribute_packing": "packed",
                "element_alignment": 1,
                "attributes": [ { "name": "index", "type": "uint" } ],
            },
            "type": "index_buffer",
            "ranges": [],
            "capacity": 0,
            "data": []
        }
        

        # Find all models in the blend to export
        for model in bpy.data.objects:
            if model.name[:3] == 'pg_':
                self.add_model_asset(model, model.name[3:], texture_asset, vertex_buffer_asset, index_buffer_asset)

        # Serialize to JSON
        json_out = {}
        assets_out = json_out["__pg_assets"] = []
        assets_out.append(texture_asset)
        assets_out.append(vertex_buffer_asset)
        assets_out.append(index_buffer_asset)

        # Write JSON to output file
        f = open(self.filepath, 'w', encoding='utf-8')
        if self.export_readable:
            json.dump(json_out, f, indent=2, separators=(', ', ': '))
        else:
            json.dump(json_out, f)
        f.close()

        return {'FINISHED'}


def menu_func_export(self, context):
    self.layout.operator(ExportProcgameAssets.bl_idname, text="Procgame: JSON Assets (.json)")


def register():
    bpy.utils.register_class(ExportProcgameAssets)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_class(ExportProcgameAssets)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)


