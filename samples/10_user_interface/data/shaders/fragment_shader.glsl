#version 450

layout(location = 1) uniform vec3 color;

layout(location = 0) in vec3 f_position_worldspace;
layout(location = 1) in vec3 f_normal;

layout(location = 0) out vec4 output_color;

void main()
{
    float shading = clamp(dot(f_normal, vec3(0, 1, 0)), 0.2, 1.0);
    output_color = vec4(color * shading, 1);
}


