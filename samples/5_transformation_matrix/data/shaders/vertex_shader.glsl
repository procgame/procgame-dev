#version 450

layout(location = 0) uniform mat4 mvp_matrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 uv_mat;

layout(location = 0) out vec3 f_position_worldspace;
layout(location = 1) out vec3 f_uv_mat;

void main()
{
    f_uv_mat = uv_mat;
    vec4 final_pos = mvp_matrix * vec4(position, 1);
    f_position_worldspace = position.xyz;
    gl_Position = final_pos;
}
