#version 450

const vec3 quad_vertex_data[6] = vec3[6](
    vec3(-1, -1, 0),
    vec3(-1, 1, 0),
    vec3(1, 1, 0),
    vec3(1, -1, 0),
    vec3(-1, -1, 0),
    vec3(1, 1, 0)
);

const ivec2 quad_uv_data[6] = ivec2[6](
    ivec2(0, 0),
    ivec2(0, 1),
    ivec2(1, 1),
    ivec2(1, 0),
    ivec2(0, 0),
    ivec2(1, 1)
);

layout(location = 0, binding = 0) uniform samplerBuffer quad_instances;
layout(location = 1) uniform uint quad_instance_offset;

layout(location = 0) out vec3 f_position_worldspace;
layout(location = 1) out vec3 f_uv_mat;

void main()
{
    const uint vert_id = gl_VertexID % 6;
    const uint quad_id = gl_VertexID / 6;
    const uint quad_instance_texel = (quad_id + quad_instance_offset) * 2; // 2 texels per quad
    const vec4 quad_pos_scale = texelFetch(quad_instances, int(quad_instance_texel));
    const vec4 quad_uv_frame = texelFetch(quad_instances, int(quad_instance_texel + 1));
    const vec3 quad_center = quad_pos_scale.xyz;
    const float quad_scale = quad_pos_scale.w;
    const vec2[2] uv_corners = vec2[]( vec2(quad_uv_frame.xy), vec2(quad_uv_frame.zw) );
    const ivec2 quad_uv_corners = quad_uv_data[vert_id];
    const vec2 vertex_uv = vec2(
        uv_corners[quad_uv_corners.x].x,
        uv_corners[quad_uv_corners.y].y);
    const vec3 vertex_pos = quad_center + (quad_vertex_data[vert_id] * quad_scale);

    f_uv_mat = vec3(vertex_uv.xy, 0);
    f_position_worldspace = vertex_pos;
    gl_Position = vec4(vertex_pos.xyz, 1);
}
