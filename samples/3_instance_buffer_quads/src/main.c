#include "procgame.h"

int main(int argc, char** argv)
{
    pg_init_core();

    /*  Load the resolution from a config file (default to 640x480) */
    ivec2 resolution = ivec2(640, 480);
    cJSON* config_json = load_json("./data/config.json");
    cJSON* resolution_json = cJSON_GetObjectItem(config_json, "resolution");
    if(resolution_json) resolution = json_read_ivec2(resolution_json);
    cJSON_Delete(config_json);

    /*  Open window and init GPU with the configured resolution */
    pg_init_window(resolution);
    pg_init_gpu();

    /*  Create an asset manager with GPU asset loaders  */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_gpu_asset_loaders(asset_mgr);
    pg_asset_manager_add_search_path(asset_mgr, "./data/");

    /*  Load our renderer asset and all its dependencies    */
    bool load_renderer_assets = pg_asset_multiple_from_file(asset_mgr, NULL, "renderer.json");
    if(!load_renderer_assets) {
        pg_log(PG_LOG_ERROR, "Failed to load renderer assets");
        return 1;
    }

    /*  Get a pointer to the renderer   */
    pg_gpu_renderer_t* render = pg_asset_get_data_by_name(asset_mgr, "SampleRenderer");
    if(!render) {
        pg_log(PG_LOG_ERROR, "Got null renderer object");
        return 1;
    }

    /*  Draw    */
    if(true) {
        pg_gpu_renderer_execute(render);
    }

    /*  Present for 5 seconds   */
    pg_window_swap();
    SDL_Delay(1000);

    /*  Cleanup */
    pg_asset_manager_destroy(asset_mgr);
    pg_deinit_all();
    return 0;
}
