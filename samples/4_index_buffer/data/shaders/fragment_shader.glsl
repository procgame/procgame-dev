#version 450

layout(location = 0, binding = 0) uniform sampler2DArray texture0;

layout(location = 0) in vec3 f_position_worldspace;
layout(location = 1) in vec3 f_uv_mat;

layout(location = 0) out vec4 output_color;

void main()
{
    vec3 tex_color = texture(texture0, f_uv_mat).rgb;
    output_color = vec4(tex_color.xyz, 1);
}


