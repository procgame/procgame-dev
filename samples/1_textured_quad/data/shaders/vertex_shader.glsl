#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 uv_mat;

layout(location = 0) out vec3 f_uv_mat;

void main()
{
    f_uv_mat = uv_mat;
    vec3 final_pos = position;
    gl_Position = vec4(final_pos.xyz, 1);
}
