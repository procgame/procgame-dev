#version 450

layout(location = 0, binding = 0) uniform sampler2DArray texture0;
layout(location = 1, binding = 1) uniform sampler2DArray texture1;

layout(location = 0) in vec3 uv_mat;

layout(location = 0) out vec4 output_color;

void main()
{
    output_color = vec4(texture(texture0, uv_mat).rgb, 1);
}


