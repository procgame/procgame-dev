#version 450

layout(location = 0, binding = 0) uniform sampler2DArray texture0;

struct light_info {
    vec4 position_radius;
    vec4 color_brightness;
};

layout(binding = 0) uniform u_lights {
    light_info lights[4];
};

layout(location = 0) in vec3 f_position_worldspace;
layout(location = 1) in vec3 f_uv_mat;

layout(location = 0) out vec4 output_color;

vec3 shading_from_light(vec3 p, light_info light)
{
    float dist_to_light = length(p - light.position_radius.xyz) / (light.position_radius.w);
    float intensity = clamp(1.0 - dist_to_light, 0, 1);
    return (light.color_brightness.xyz * light.color_brightness.w) * intensity;
}

void main()
{
    vec3 tex_color = texture(texture0, f_uv_mat).rgb;
    vec3 shading = shading_from_light(f_position_worldspace, lights[0]);
    shading += shading_from_light(f_position_worldspace, lights[1]);
    shading += shading_from_light(f_position_worldspace, lights[2]);
    shading += shading_from_light(f_position_worldspace, lights[3]);
    vec3 color_shaded = tex_color.xyz * shading.xyz;
    output_color = vec4(color_shaded.xyz, 1);
}


