#include "procgame.h"

int main(int argc, char** argv)
{
    pg_init_core();

    /*  Load the resolution from a config file (default to 640x480) */
    ivec2 resolution = ivec2(640, 480);
    cJSON* config_json = load_json("./data/config.json");
    cJSON* resolution_json = cJSON_GetObjectItem(config_json, "resolution");
    if(resolution_json) resolution = json_read_ivec2(resolution_json);

    /*  Open window and init GPU with the configured resolution */
    pg_init_window(resolution);
    pg_init_gpu();

    /*  Create an asset manager with GPU asset loaders  */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    pg_gpu_asset_loaders(asset_mgr);
    pg_asset_manager_add_search_path(asset_mgr, "./data/");

    /*  Load our renderer asset and all its dependencies    */
    bool load_renderer_assets = pg_asset_multiple_from_file(asset_mgr, NULL, "renderer.json");
    if(!load_renderer_assets) {
        pg_log(PG_LOG_ERROR, "Failed to load renderer assets");
        return 1;
    }

    /*  Get a pointer to the renderer   */
    pg_gpu_renderer_t* render = pg_asset_get_data_by_name(asset_mgr, "SampleRenderer");
    if(!render) {
        pg_log(PG_LOG_ERROR, "Got null renderer object");
        return 1;
    }

    ivec2 model_range = pg_asset_manager_get_variable_ivec2(asset_mgr, "SampleTorus_indices.range[all]");

    int frame_i = 0;
    /*  Draw    */
    while(!pg_input_user_exit()) {
        pg_input_poll();
        pg_input_flush();
        int i = frame_i % (10000 / 30);
        i = frame_i;
        ++frame_i;
        /*  Get render stage from asset manager */
        pg_gpu_render_stage_t* render_stage = pg_asset_get_data_by_name(asset_mgr, "SampleRenderStage");
        if(!render_stage) {
            pg_log(PG_LOG_ERROR, "Got null renderstage object");
            return 1;
        }

        /*  Get render stage's list of commands to execute and clear it */
        pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_commands(render_stage);
        ARR_TRUNCATE(*commands, 0);

        /*  Calculate a transformation matrix   */
        mat4 translation = mat4_translation(vec3(0,0,0));
        mat4 scaling = mat4_scaling(vec3(0.5, 0.5, 0.5));
        vec3 angles = vec3(1 + (i * 0.02), 1 + (i * 0.01), 1 + (i * 0.001));
        mat4 euler = mat4_rotate( vec3_Y(), angles.x,
                    mat4_rotate( vec3_X(), angles.y,
                     mat4_rotate( vec3_Z(), angles.z, mat4_identity() ) ) );
        mat4 tx = mat4_mul(translation, mat4_mul(scaling, euler));

        /*  Record new commands with the new matrix */
        pg_gpu_record_commands(commands);
        pg_gpu_cmd_clear(vec4(0,0,0,0));
        vec3 color = vec3(1, 1, 1);
        pg_gpu_cmd_set_uniform(1, PG_VEC3, 1, color.v);
        pg_gpu_cmd_set_uniform_matrix(0, PG_MAT4, false, 1, tx.v);
        pg_gpu_cmd_draw_triangles(model_range.x, model_range.y, .indexed = true);

        /*  Excute renderer again   */
        pg_gpu_renderer_execute(render);
        
        /*  Present to the user */
        pg_window_swap();
    }

    /*  Cleanup */
    pg_asset_manager_destroy(asset_mgr);
    pg_deinit_all();
    return 0;
}
