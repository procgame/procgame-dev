#include "procgame.h"

int main(int argc, char** argv)
{
    /*  Initialize procgame core    */
    pg_init_core();

    /*  Load the resolution from a config file (default to 640x480) */
    ivec2 resolution = ivec2(640,480);
    cJSON* config_json = load_json("./data/config.json");
    cJSON* resolution_json = cJSON_GetObjectItem(config_json, "resolution");
    if(resolution_json) resolution = json_read_ivec2(resolution_json);
    cJSON_Delete(config_json);

    /*  Open window and init GPU with the configured resolution */
    pg_init_window(resolution);
    pg_init_gpu();

    /*  Create an asset manager and set the config variables    */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    ivec2 render_resolution = ivec2_scale(resolution, 2);
    int font_size = render_resolution.y / 40;
    pg_asset_manager_set_variable(asset_mgr, "cfg_GameFontSize", pg_data_init(PG_INT, 1, &font_size));
    pg_asset_manager_set_variable(asset_mgr, "cfg_GameRenderResolution", pg_data_init(PG_IVEC2, 1, &render_resolution));
    pg_asset_manager_add_search_path(asset_mgr, "./data/");
    pg_gpu_asset_loaders(asset_mgr);
    pg_gfx_asset_loaders(asset_mgr);

    /*  Load our renderer asset and all its dependencies    */
    bool load_renderer_assets = pg_asset_multiple_from_file(asset_mgr, NULL, "renderer.json");
    if(!load_renderer_assets) {
        pg_log(PG_LOG_ERROR, "Failed to load renderer assets");
        return 1;
    }

    /*  Get a pointer to the renderer   */
    pg_gpu_renderer_t* render = pg_asset_get_data_by_name(asset_mgr, "SampleRenderer");
    ivec2 model_range = pg_asset_manager_get_variable_ivec2(asset_mgr, "SampleTorus_indices.range[all]");

    int frame_i = 0;
    /*  Draw    */
    while(!pg_input_user_exit()) {
        pg_input_poll();
        int i = frame_i % (10000 / 30);
        i = frame_i;
        ++frame_i;
        /*  Get render stage from asset manager */
        pg_gpu_render_stage_t* render_stage = pg_asset_get_data_by_name(asset_mgr, "SampleRenderStage");
        if(!render_stage) {
            pg_log(PG_LOG_ERROR, "Got null renderstage object");
            return 1;
        }

        /*  Get render stage's list of commands to execute and clear it */
        pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_commands(render_stage);
        ARR_TRUNCATE(*commands, 0);
        pg_gpu_record_commands(commands);

        pg_gpu_cmd_depth(true, PG_GPU_LEQUAL, 0xFF);

        /*  Calculate a transformation matrix   */
        mat4 translation = mat4_translation(vec3(0,0,0));
        mat4 scaling = mat4_scaling(vec3(0.5, 0.5, 0.5));
        vec3 angles = vec3(1 + (i * 0.02), 1 + (i * 0.01), 1 + (i * 0.001));
        mat4 euler = mat4_rotate( vec3_Y(), angles.x,
                    mat4_rotate( vec3_X(), angles.y,
                     mat4_rotate( vec3_Z(), angles.z, mat4_identity() ) ) );
        mat4 tx = mat4_mul(translation, mat4_mul(scaling, euler));

        /*  Record new commands with the new matrix */
        pg_gpu_cmd_clear(vec4(0,0,0,0));
        vec3 color = vec3(1, 1, 1);
        pg_gpu_cmd_set_uniform(1, PG_VEC3, 1, color.v);
        pg_gpu_cmd_set_uniform_matrix(0, PG_MAT4, false, 1, tx.v);
        pg_gpu_cmd_draw_triangles(model_range.x, model_range.y, .indexed = true);

        struct pg_quadbatch* qb = pg_asset_get_data_by_name(asset_mgr, "SampleQuadbatch");
        pg_gpu_render_stage_t* qb_render_stage = pg_asset_get_data_by_name(asset_mgr, "SampleQuadbatch.render_stage");
        pg_gpu_command_arr_t* qb_commands = pg_gpu_render_stage_get_commands(qb_render_stage);
        ARR_TRUNCATE(*qb_commands, 0);
        mat4 quads_tx = mat4_identity();
        pg_quadbatch_reset(qb);

        struct pg_imageset* images = pg_asset_get_data_by_name(asset_mgr, "SampleSpritesheet");
        pg_tex_frame_t frame_RedTile = pg_image_get_frame_by_name(pg_imageset_get_image(images, "Environment"), "RedTile");
        pg_tex_frame_t frame_YellowTile = pg_image_get_frame_by_name(pg_imageset_get_image(images, "Environment"), "YellowTile");
        pg_tex_frame_t frame_WhiteTile = pg_image_get_frame_by_name(pg_imageset_get_image(images, "Environment"), "WhiteTile");
        pg_tex_frame_t frame_BlackTile = pg_image_get_frame_by_name(pg_imageset_get_image(images, "Environment"), "BlackTile");
        pg_quadbatch_next(qb, &quads_tx);
        pg_quadbatch_add_quad(qb, &PG_EZQUAD( .scale = vec2(1,1), .pos = vec3(-0.5,-0.5), .uv_v = frame_RedTile.v ));
        pg_quadbatch_add_quad(qb, &PG_EZQUAD( .scale = vec2(1,1), .pos = vec3(0.5,-0.5), .uv_v = frame_YellowTile.v ));
        pg_quadbatch_add_quad(qb, &PG_EZQUAD( .scale = vec2(1,1), .pos = vec3(-0.5,0.5), .uv_v = frame_WhiteTile.v ));
        pg_quadbatch_add_quad(qb, &PG_EZQUAD( .scale = vec2(1,1), .pos = vec3(0.5,0.5), .uv_v = frame_BlackTile.v ));
        pg_quadbatch_draw(qb, qb_commands);

        /*  Excute renderer again   */
        pg_quadbatch_upload(qb);
        pg_gpu_renderer_execute(render);
        
        /*  Present to the user */
        pg_window_swap();
        pg_input_flush();
    }

    /*  Cleanup */
    pg_asset_manager_destroy(asset_mgr);
    pg_deinit_window();
    pg_deinit_all();
    return 0;
}
