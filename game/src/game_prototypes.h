/***********************/
/*  Entity prototype   */
struct game_entity_prototype {
    char name[GAME_PROTOTYPE_NAME_MAXLEN];
    bool complete;
    bool has_component[GAME_ENTITY_N_COMPONENTS];
    bool traits[GAME_ENTITY_N_TRAITS];
    struct game_collider_prototype collider;
    struct game_drawable_prototype drawable;
    struct game_item_prototype item;
    struct game_particle_emitter_prototype particle_emitter;
    struct game_projectile_prototype projectile;
};



/*  Gameplay usage for prototypes   */
game_entity_prototype_id_t game_get_entity_prototype(struct gameplay_state* game, const char* proto_name);

struct game_entity* game_create_entity_from_prototype_name(struct gameplay_state* game,
        const char* proto_name, game_entity_id_t* id_out);

struct game_entity* game_create_entity_from_prototype_id(struct gameplay_state* game,
        game_entity_prototype_id_t proto_id, game_entity_id_t* id_out);

