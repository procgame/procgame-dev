#include "procgame.h"
#include "game.h"



game_entity_prototype_id_t game_get_entity_prototype_id(struct gameplay_state* game, const char* proto_name)
{
    game_entity_prototype_id_t proto_id = -1;
    HTABLE_GET_V(game->res->protos.entities_table, proto_name, proto_id);
    return proto_id;
}

struct game_entity* game_create_entity_from_prototype_name(struct gameplay_state* game,
        const char* proto_name, game_entity_id_t* id_out)
{
    game_entity_prototype_id_t proto_id = game_get_entity_prototype_id(game, proto_name);
    if(proto_id == -1) {
        pg_log(PG_LOG_ERROR, "Attempted to created entity from non-existent prototype '%s'", proto_name);
        return NULL;
    }
    pg_log(PG_LOG_INFO, "Creating entity from prototype '%s'", proto_name);
    return game_create_entity_from_prototype_id(game, proto_id, id_out);
}



struct game_entity* game_create_entity_from_prototype_id(struct gameplay_state* game,
        game_entity_prototype_id_t proto_id, game_entity_id_t* id_out)
{
    struct game_entity_prototype* proto = &game->res->protos.entities.data[proto_id];

    /*  Allocate entity */
    game_entity_id_t ent_id;
    struct game_entity* ent_ptr = game_entity_create(game, &ent_id);
    ent_ptr->proto_id = proto_id;

    /*  Traits      */
    int i;
    for(i = 0; i < GAME_ENTITY_N_TRAITS; ++i) {
        game_entity_set_trait(game, ent_ptr, i, proto->traits[i]);
    }

    /*  Components      */
    /*  Collider    */
    if(proto->has_component[GAME_ENTITY_COMPONENT_COLLIDER]) {
        struct game_component_collider* coll_ptr = ENT_COMPONENT_NEW(game, ent_ptr, collider, NULL);
        game_collider_from_prototype(coll_ptr, &proto->collider);
    }

    /*  Drawable    */
    if(proto->has_component[GAME_ENTITY_COMPONENT_DRAWABLE]) {
        struct game_component_drawable* draw_ptr = ENT_COMPONENT_NEW(game, ent_ptr, drawable, NULL);
        game_drawable_from_prototype(draw_ptr, &proto->drawable);
    }

    /*  Item    */
    if(proto->has_component[GAME_ENTITY_COMPONENT_ITEM]) {
        struct game_component_item* item_ptr = ENT_COMPONENT_NEW(game, ent_ptr, item, NULL);
        game_item_from_prototype(item_ptr, &proto->item);
    }

    /*  Particle emitter    */
    if(proto->has_component[GAME_ENTITY_COMPONENT_PARTICLE_EMITTER]) {
        struct game_component_particle_emitter* emit_ptr = ENT_COMPONENT_NEW(game, ent_ptr, particle_emitter, NULL);
        game_particle_emitter_from_prototype(emit_ptr, &proto->particle_emitter);
    }

    /*  Projectile  */
    if(proto->has_component[GAME_ENTITY_COMPONENT_PROJECTILE]) {
        struct game_component_projectile* proj_ptr = ENT_COMPONENT_NEW(game, ent_ptr, projectile, NULL);
        game_projectile_from_prototype(proj_ptr, &proto->projectile);
    }

    if(id_out) *id_out = ent_id;
    return ent_ptr;
}


static bool read_entity_prototype(struct game_entity_prototype* out, cJSON* json)
{
    /*  Component prototype JSONs   */
    cJSON* collider_json, *drawable_json, *item_json, *particle_emitter_json, *projectile_json;
    cJSON* name_json, *traits_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(7,
        PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
        PG_JSON_LAYOUT_ITEM("traits", cJSON_Array, &traits_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("collider", cJSON_Object, &collider_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("drawable", cJSON_Object, &drawable_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("item", cJSON_Object, &item_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("particle_emitter", cJSON_Object, &particle_emitter_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("projectile", cJSON_Object, &projectile_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid entity prototype JSON");
        return false;
    }

    strncpy(out->name, name_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    cJSON* trait_json;
    cJSON_ArrayForEach(trait_json, traits_json) {
        if(!cJSON_IsString(trait_json)) {
            pg_log(PG_LOG_ERROR, "Non-string entity trait");
            return false;
        }
        if(strncmp(trait_json->valuestring, "moves", 6) == 0) out->traits[GAME_ENTITY_MOVES] = true;
    }

    out->has_component[GAME_ENTITY_COMPONENT_COLLIDER] = !!collider_json;
    out->has_component[GAME_ENTITY_COMPONENT_DRAWABLE] = !!drawable_json;
    out->has_component[GAME_ENTITY_COMPONENT_ITEM] = !!item_json;
    out->has_component[GAME_ENTITY_COMPONENT_PARTICLE_EMITTER] = !!particle_emitter_json;
    out->has_component[GAME_ENTITY_COMPONENT_PROJECTILE] = !!projectile_json;

    bool success = true;
    if(collider_json) success = success && game_read_collider_prototype(&out->collider, collider_json);
    if(drawable_json) success = success && game_read_drawable_prototype(&out->drawable, drawable_json);
    if(item_json) success = success && game_read_item_prototype(&out->item, item_json);
    if(particle_emitter_json) success = success && game_read_particle_emitter_prototype(&out->particle_emitter, particle_emitter_json);
    if(projectile_json) success = success && game_read_projectile_prototype(&out->projectile, projectile_json);
    if(!success) {
        pg_log(PG_LOG_ERROR, "Failed to read component(s) prototype JSON for entity prototype '%s'", name_json->valuestring);
        return false;
    }

    return true;
}


bool game_read_entity_prototypes_list(struct game_prototypes* protos, const char* filename)
{
    cJSON* entity_protos_file = load_json(filename);
    if(!entity_protos_file) {
        pg_log(PG_LOG_ERROR, "Failed to read entity prototypes JSON file '%s'", filename);
        return false;
    }
    cJSON* json = cJSON_GetObjectItem(entity_protos_file, "entities");
    if(!json || !cJSON_IsArray(json)) {
        pg_log(PG_LOG_ERROR, "Failed to parse entity prototypes from JSON in file '%s'", filename);
        return false;
    }

    int n_protos = cJSON_GetArraySize(json);
    ARR_RESERVE(protos->entities, protos->entities.len + n_protos);
    cJSON* proto_json;
    cJSON_ArrayForEach(proto_json, json) {
        int proto_idx = protos->entities.len;
        struct game_entity_prototype* new_proto = ARR_NEW(protos->entities);
        *new_proto = (struct game_entity_prototype){};
        if(!read_entity_prototype(new_proto, proto_json)) {
            ARR_POP(protos->entities);
            return false;
        }
        HTABLE_SET(protos->entities_table, new_proto->name, proto_idx);
    }
    return true;
}

void game_finalize_entity_prototypes(struct game_prototypes* protos,
        struct game_entity_callbacks* callbacks, struct pg_imageset* game_images)
{
    int i;
    struct game_entity_prototype* proto;
    ARR_FOREACH_PTR(protos->entities, proto, i) {
        if(proto->complete) continue;
        pg_log(PG_LOG_INFO, "Finalizing entity prototype '%s'", proto->name);
        if(proto->has_component[GAME_ENTITY_COMPONENT_DRAWABLE]) {
            HTABLE_GET_V(callbacks->draw_fn_table, proto->drawable.draw_func_name, proto->drawable.draw_func);
            if(!proto->drawable.draw_func) {
                pg_log(PG_LOG_ERROR, "Entity prototype '%s' drawable component refers to non-existent draw function '%s'",
                        proto->name, proto->drawable.draw_func_name);
            }
            if(proto->drawable.image_name[0]) {
                struct pg_image* img = pg_imageset_get_image(game_images, proto->drawable.image_name);
                if(img) proto->drawable.tex_frame = pg_image_get_frame_by_name(img, proto->drawable.frame_name);
            }
        }
        if(proto->has_component[GAME_ENTITY_COMPONENT_ITEM]) {
            if(proto->item.projectile_name[0])
                HTABLE_GET_V(protos->entities_table, proto->item.projectile_name, proto->item.projectile_proto_id);
            if(proto->item.use_func_name[0])
                HTABLE_GET_V(callbacks->item_use_fn_table, proto->item.use_func_name, proto->item.use_func);
        }
        if(proto->has_component[GAME_ENTITY_COMPONENT_PARTICLE_EMITTER])
            HTABLE_GET_V(protos->particles_table, proto->particle_emitter.particle_name, proto->particle_emitter.particle_proto_id);
        if(proto->has_component[GAME_ENTITY_COMPONENT_PROJECTILE])
            HTABLE_GET_V(callbacks->projectile_hit_fn_table, proto->projectile.hit_func_name, proto->projectile.hit_func);
        proto->complete = true;
    }
}





/****************************/
/*  Particle prototypes     */

static bool read_particle_prototype(struct game_particle_prototype* out, cJSON* json)
{
    /*  Component prototype JSONs   */
    cJSON* name_json, *lifetime_json, *phases_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(3,
        PG_JSON_LAYOUT_ITEM("name", cJSON_String, &name_json),
        PG_JSON_LAYOUT_ITEM("lifetime", cJSON_Number, &lifetime_json),
        PG_JSON_LAYOUT_ITEM("phases", cJSON_Array, &phases_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid particle prototype JSON");
        return false;
    }

    strncpy(out->name, name_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    out->lifetime = lifetime_json->valuedouble;
    SARR_INIT(out->phases);

    cJSON* phase_json;
    cJSON_ArrayForEach(phase_json, phases_json) {
        cJSON* image_json, *frame_json, *color_mul_json, *color_add_json, *scale_json;
        struct pg_json_layout layout = PG_JSON_LAYOUT(5,
            PG_JSON_LAYOUT_ITEM("image", cJSON_String, &image_json),
            PG_JSON_LAYOUT_ITEM("frame", cJSON_String, &frame_json),
            PG_JSON_LAYOUT_ITEM("color_mul", cJSON_Array, &color_mul_json),
            PG_JSON_LAYOUT_ITEM("color_add", cJSON_Array, &color_add_json),
            PG_JSON_LAYOUT_ITEM("scale", cJSON_Array, &scale_json),
        );
        if(!pg_load_json_layout(phase_json, &layout)) {
            pg_log(PG_LOG_ERROR, "Invalid particle phase JSON in particle prototype '%s'",
                    name_json->valuestring);
            return false;
        }
        struct game_particle_phase* new_phase = ARR_NEW(out->phases);
        strncpy(new_phase->image, image_json->valuestring, 32);
        strncpy(new_phase->frame, frame_json->valuestring, 32);
        new_phase->color_mul = json_read_vec4(color_mul_json);
        new_phase->color_add = json_read_vec4(color_add_json);
        new_phase->scale = json_read_vec2(scale_json);
    }
    return true;
}

bool game_read_particle_prototypes_list(struct game_prototypes* protos, const char* filename)
{
    cJSON* particle_protos_file = load_json(filename);
    if(!particle_protos_file) {
        pg_log(PG_LOG_ERROR, "Failed to read particle prototypes JSON file '%s'", filename);
        return false;
    }
    cJSON* json = cJSON_GetObjectItem(particle_protos_file, "particles");
    if(!json || !cJSON_IsArray(json)) {
        pg_log(PG_LOG_ERROR, "Failed to parse particle prototypes from JSON in file '%s'", filename);
        return false;
    }
    int n_protos = cJSON_GetArraySize(json);
    ARR_RESERVE(protos->particles, protos->particles.len + n_protos);
    cJSON* proto_json;
    cJSON_ArrayForEach(proto_json, json) {
        int proto_idx = protos->particles.len;
        struct game_particle_prototype* new_proto = ARR_NEW(protos->particles);
        if(!read_particle_prototype(new_proto, proto_json)) {
            ARR_POP(protos->particles);
            return false;
        }
        HTABLE_SET(protos->particles_table, new_proto->name, proto_idx);
    }
    return true;
}

void game_finalize_particle_prototypes(struct game_prototypes* protos, struct pg_imageset* particle_images)
{
    int i;
    struct game_particle_prototype* proto;
    ARR_FOREACH_PTR(protos->particles, proto, i) {
        int j;
        for(j = 0; j < proto->phases.len; ++j) {
            struct game_particle_phase* phase = &proto->phases.data[j];
            phase->image_frame = pg_image_get_frame_by_name(pg_imageset_get_image(
                    particle_images, phase->image), phase->frame);
        }
    }
}


