struct game_grid {
    struct gameplay_state* game;
    int n_cells;
    ivec3 cell_idx;
    ivec3 grid_size;
    vec3 cell_size, full_size;
    vec3 grid_offset;
    aabb3D bound;
    uint32_t query_no;
    /*  Collider grid and queries   */
    game_component_collider_arr_t* active_colliders;
    game_component_collider_arr_t* inactive_colliders;
    game_component_collider_arr_t* static_colliders;
    game_component_collider_arr_t collider_query;
    game_component_collider_id_t collider_ray_hit;
    float collider_ray_len;
    /*  Item grid and queries   */
    game_component_item_arr_t* item_grid;
    game_component_item_arr_t item_query;
};

/********************/
/*  BOOKKEEPING     */
void game_grid_init(struct gameplay_state* game, ivec3 grid_size, vec3 cell_size);
void game_grid_deinit(struct gameplay_state* game);

/********************/
/*  SCROLLING       */
void game_grid_set_offset(struct gameplay_state* game, vec3 center);

/************************/
/*  COLLIDER QUERYING   */
void game_grid_new_collider_query(struct gameplay_state* game);
void game_grid_add_inactive_collider(struct gameplay_state* game,
        struct game_component_collider* coll_ptr, game_component_collider_id_t coll_id);
void game_grid_rebuild_active_colliders(struct gameplay_state* game);
void game_grid_rebuild_inactive_colliders(struct gameplay_state* game);
void game_grid_rebuild_static_colliders(struct gameplay_state* game);
int game_query_active_colliders(struct gameplay_state* game, aabb3D bound);
int game_query_inactive_colliders(struct gameplay_state* game, aabb3D bound);
int game_query_static_colliders(struct gameplay_state* game, aabb3D bound);
int game_query_all_colliders(struct gameplay_state* game, aabb3D bound);
float game_raycast_active_colliders(struct gameplay_state* game, ray3D ray, float max_len);
float game_raycast_static_colliders(struct gameplay_state* game, ray3D ray, float max_len);
float game_raycast_all_colliders(struct gameplay_state* game, ray3D ray, float max_len);
vec3 game_query_deepest_collision(struct gameplay_state* game,
        vec3 pos, float probe_radius, game_entity_id_t* out);

/********************/
/*  DEBUG           */
void game_grid_draw_cells(struct gameplay_state* game);
