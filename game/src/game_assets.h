/*  Entity callback tables
    The callbacks exist only in C code (because they are C functions),
    they are only referred to by name in entity or particle prototypes  */
struct game_entity_callbacks {
    game_draw_fn_table_t draw_fn_table;
    game_item_use_fn_table_t item_use_fn_table;
    game_projectile_hit_fn_table_t projectile_hit_fn_table;
};


void game_register_draw_func(struct game_entity_callbacks* cbs, const char* fn_name, game_draw_fn_t fn);
void game_register_item_use_func(struct game_entity_callbacks* cbs, const char* fn_name, game_item_use_fn_t fn);
void game_register_projectile_hit_func(struct game_entity_callbacks* cbs, const char* fn_name, game_projectile_hit_fn_t fn);

void game_register_draw_funcs(struct game_entity_callbacks* assets);
void game_register_item_use_funcs(struct game_entity_callbacks* assets);
void game_register_projectile_hit_funcs(struct game_entity_callbacks* assets);




/*  Entity and particle prototypes are laoded from JSON at initialization, and
    then retrieved by name during gameplay.     */
struct game_prototypes {
    /*  Entity prototypes   */
    int_table_t entities_table;
    ARR_T(struct game_entity_prototype) entities;

    /*  Particle prototypes */
    int_table_t particles_table;
    ARR_T(struct game_particle_prototype) particles;
};

bool game_read_entity_prototypes_list(struct game_prototypes* protos, const char* filename);
bool game_read_particle_prototypes_list(struct game_prototypes* protos, const char* filename);
void game_finalize_entity_prototypes(struct game_prototypes* protos,
        struct game_entity_callbacks* callbacks, struct pg_imageset* game_images);
void game_finalize_particle_prototypes(struct game_prototypes* protos, struct pg_imageset* game_images);



/*  Game assets     */
struct game_resources {
    bool debug;

    struct game_entity_callbacks entity_callbacks;
    struct game_prototypes protos;

    /*  Art assets are loaded through a pg_asset_manager
        (ie. from JSON asset definitions at runtime)    */
    pg_asset_manager_t* asset_mgr;
    struct pg_fontset* fonts;
    struct pg_imageset* ui_imgs;
    struct pg_imageset* particle_imgs;
    struct pg_imageset* game_imgs;
    struct pg_text_formatter text_fmt;
    pg_gpu_buffer_t* models;
};

bool game_resources_init(struct game_resources* assets);
void game_resources_deinit(struct game_resources* assets);

