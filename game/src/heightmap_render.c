#include "procgame.h"
#include "game.h"

struct terrain_vertex {
    vec3 pos;
    vec3 normal;
    vec3 tex_select;
};

static const struct pg_buffer_layout terrain_vertex_layout = PG_BUFFER_LAYOUT(
    PG_BUFFER_ATTRIBUTE_PACKING(PG_BUFFER_PACKING_4BYTE),
    PG_BUFFER_ELEMENT_ALIGNMENT(36),
    PG_BUFFER_ATTRIBUTES_LIST(3,
        PG_BUFFER_ATTRIBUTE("v_position", PG_VEC3),
        PG_BUFFER_ATTRIBUTE("v_normal", PG_VEC3),
        PG_BUFFER_ATTRIBUTE("v_tex_select", PG_VEC3),
    )
);

static const struct pg_buffer_layout index_buffer_layout = PG_BUFFER_LAYOUT_SIMPLE("index", PG_UINT);

void game_terrain_init_buffers(struct gameplay_state* game)
{
    const ivec2 terrain_chunk_size = ivec2(35, 35);
    const int terrain_grid_total_points =
        terrain_chunk_size.x * terrain_chunk_size.y * GAME_RENDER_MAX_CHUNKS;
    const int terrain_grid_total_indices =
        (terrain_chunk_size.x - 1) * (terrain_chunk_size.x - 1) * GAME_RENDER_MAX_CHUNKS * 2 * 3;
    const int chunk_size_in_verts_buffer = terrain_grid_total_points / GAME_RENDER_MAX_CHUNKS;
    const int chunk_size_in_idx_buffer = terrain_grid_total_indices / GAME_RENDER_MAX_CHUNKS;

    struct game_system_render* render = &game->render;
    struct pg_buffer* verts_buf = malloc(sizeof(*verts_buf));
    struct pg_buffer* idx_buf = malloc(sizeof(*idx_buf));
    pg_buffer_init(verts_buf, &terrain_vertex_layout, terrain_grid_total_points);
    pg_buffer_init(idx_buf, &index_buffer_layout, terrain_grid_total_indices);
    render->terrain_vert_buf_gpu = pg_buffer_upload(verts_buf, PG_GPU_VERTEX_BUFFER, true);
    render->terrain_idx_buf_gpu = pg_buffer_upload(idx_buf, PG_GPU_INDEX_BUFFER, true);
    pg_asset_from_data(game->asset_mgr, "pg_gpu_buffer", "GameTerrainVertexData", game->render.terrain_vert_buf_gpu, true);
    pg_asset_from_data(game->asset_mgr, "pg_gpu_buffer", "GameTerrainIndexData", game->render.terrain_idx_buf_gpu, true);

    SARR_INIT(game->render.available_terrain_buf_ranges);
    for(int i = 0; i < GAME_RENDER_MAX_CHUNKS; ++i) {
        ivec2 verts_range = ivec2(i * chunk_size_in_verts_buffer, chunk_size_in_verts_buffer);
        ivec2 idx_range = ivec2(i * chunk_size_in_idx_buffer, chunk_size_in_idx_buffer);
        game->render.terrain_buf_ranges[i].vert_range = verts_range;
        game->render.terrain_buf_ranges[i].idx_range = idx_range;
        game->render.terrain_buf_ranges[i].in_use = false;
        ARR_PUSH(game->render.available_terrain_buf_ranges, i);
    }
}

void game_terrain_build_mesh(struct pg_heightmap* hmap, struct pg_vertex_buffer_builder* builder)
{
    float* data = hmap->map;
    uint32_t* v_idx = calloc(sizeof(*v_idx), hmap->map_size.x * hmap->map_size.y);
    int i, j;
    /*  Generate a bunch of vertices, one per heightmap point   */
    for(i = 1; i < hmap->map_size.x - 1; ++i)
    for(j = 1; j < hmap->map_size.y - 1; ++j) {
        float h = data[i + j * hmap->map_size.x];
        int grid_idx = (i-1) + (j-1) * (hmap->map_size.x-2);
        if(h < 0) {
            v_idx[grid_idx] = 0xFFFFFFFF;
            continue;
        }
        float u, d, r, l;
        u = pg_heightmap_get_height(hmap, i, j - 1) / hmap->cell_size;
        d = pg_heightmap_get_height(hmap, i, j + 1) / hmap->cell_size;
        l = pg_heightmap_get_height(hmap, i - 1, j) / hmap->cell_size;
        r = pg_heightmap_get_height(hmap, i + 1, j) / hmap->cell_size;
        if(u < 0) u = h;
        if(d < 0) d = h;
        if(r < 0) r = h;
        if(l < 0) l = h;
        vec3 normal = vec3_norm(vec3(l-r, u-d, 1.0));
        vec3 tex_select = normal.z > 0.85 ? vec3(1-normal.z,1-normal.z,1) : vec3(fabs(normal.x), fabs(normal.y), 0);
        //tex_select = vec3_scale(vec3_add(normal,vec3(1,1,1)), 0.5);
        tex_select = vec3_tdiv(tex_select, tex_select.x+tex_select.y+tex_select.z);
        //tex_select = vec3(1,0,0);
        ubvec3 tex_select_b = ubvec3(VEC_XYZ(vec3_scale(tex_select, 255)));
        struct terrain_vertex new_vert = {
            .pos = vec3((i) * hmap->cell_size, (j) * hmap->cell_size, h),
            .normal = normal,
            .tex_select = tex_select };
        v_idx[grid_idx] = pg_vertex_buffer_builder_add_vertex(builder, &new_vert);
    }
    /*  Generate faces from all the vertices    */
    for(i = 1; i < hmap->map_size.x - 2; ++i)
    for(j = 1; j < hmap->map_size.y - 2; ++j) {
        int i_ = i-1;
        int j_ = j-1;
        uint32_t vert[4] = {
            v_idx[i_ + j_ * (hmap->map_size.x-2)],
            v_idx[(i_+1) + j_ * (hmap->map_size.x-2)],
            v_idx[i_ + (j_+1) * (hmap->map_size.x-2)],
            v_idx[(i_+1) + (j_+1) * (hmap->map_size.x-2)], };
        if(vert[0] == 0xFFFFFFFF || vert[1] == 0xFFFFFFFF
        || vert[2] == 0xFFFFFFFF || vert[3] == 0xFFFFFFFF) continue;
        pg_vertex_buffer_builder_add_triangle(builder, vert[0], vert[1], vert[2]);
        pg_vertex_buffer_builder_add_triangle(builder, vert[2], vert[1], vert[3]);
    }
    free(v_idx);
}

void game_draw_heightmap(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_heightmap* hmap_ptr = ENT_COMPONENT_PTR(game, ent, heightmap);
    if(!hmap_ptr) return;

    pg_gpu_render_stage_t* terrain_pass = game->render.terrain_pass;
    pg_gpu_command_arr_t* terrain_cmds = pg_gpu_render_stage_get_group_commands(terrain_pass, 0);
    pg_gpu_record_commands(terrain_cmds);

    mat4 tx = mat4_translation(ent->pos);
    //mat4 mvp = mat4_mul(game->render.view_3d.projview_matrix, tx);
    ivec2 idx_range = render->terrain_buf_ranges[hmap_ptr->terrain_chunk_idx].idx_range_built;
    ivec2 vert_range = render->terrain_buf_ranges[hmap_ptr->terrain_chunk_idx].vert_range_built;
    //pg_log(PG_LOG_INFO, "vert range (%d, %d) idx range (%d, %d)", VEC_XY(vert_range), VEC_XY(idx_range));
    //idx_range.x  = 0;
    //vert_range.y = 0;

    pg_gpu_cmd_set_uniform_matrix(0, PG_MAT4, false, 1, &tx);
    //pg_gpu_cmd_set_uniform(1, PG_VEC2, 1, &vec2(0.125,0.125));
    //pg_gpu_cmd_set_uniform(2, PG_IVEC3, 1, &ivec3(0, 1, 2));
    pg_gpu_cmd_draw_triangles(idx_range.x, idx_range.y, .indexed = true, .base_vertex = vert_range.x);
}

