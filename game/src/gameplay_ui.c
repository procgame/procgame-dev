#include "procgame.h"
#include "game.h"

/********************/
/*  UI              */
/********************/

PG_UI_CALLBACK(test_callback)
{
    struct pg_animator* ui_scale = pg_ui_get_property(ctx, elem, PG_UI_SCALE);
    pg_animator_set(ui_scale, &PG_ANIMATION(
        PG_ANIMATION_KEYFRAMES(3,
            PG_KEYFRAME_ABSOLUTE(0, pg_ease_lerp, 1, 1),
            PG_KEYFRAME_ABSOLUTE(20, pg_ease_out_bounce, 2, 2),
            PG_KEYFRAME_ABSOLUTE(100, pg_ease_lerp, 1, 1),
        ),
    ));
    PG_UI_CONSUME_INPUT;
}


void game_build_ui(struct gameplay_state* game)
{
    pg_ui_context_register_callback(game->ui, "test_callback", test_callback);
    if(!pg_asset_multiple_from_file(game->asset_mgr, NULL, "ui/TestUI.json", NULL, 0)) {
        pg_log(PG_LOG_ERROR, "Failed to load game UI definition");
    }
    /*  Add a test UI element   */
    //pg_ui_add_element(game->ui, pg_ui_context_root(game->ui), "foo", PG_UI_PROPERTIES( .pos = vec2(-0.75,0.85),
    //    .draw_order = PG_UI_TEXT_ONLY, .text_pos = vec2(0,0),
    //    .text_scale = vec2(0.05,0.05),
    //    .text = "Hello, world?",
    //));
    game->ui->debug_action_areas = true;
    game->ui->debug_containers = true;
}

