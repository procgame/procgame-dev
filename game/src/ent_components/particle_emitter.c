#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(particle_emitter);

bool game_read_particle_emitter_prototype(struct game_particle_emitter_prototype* out, cJSON* json)
{
    cJSON* type_json, *rate_json, *rate_var_json, *dir_json, *dir_var_json, *area_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(6,
        PG_JSON_LAYOUT_ITEM("type", cJSON_String, &type_json),
        PG_JSON_LAYOUT_ITEM("rate", cJSON_Number, &rate_json),
        PG_JSON_LAYOUT_ITEM("rate_variability", cJSON_Number, &rate_var_json),
        PG_JSON_LAYOUT_ITEM("dir", cJSON_Array, &dir_json),
        PG_JSON_LAYOUT_ITEM("dir_variability", cJSON_Array, &dir_var_json),
        PG_JSON_LAYOUT_ITEM("area", cJSON_Array, &area_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid particle emitter prototype JSON");
        return false;
    }

    strncpy(out->particle_name, type_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    out->rate = (1.0f / rate_json->valuedouble) * 60.0f;
    out->rate_variability = rate_var_json->valuedouble;
    out->dir = json_read_vec3(dir_json);
    out->dir_variability = json_read_vec3(dir_var_json);
    out->area = json_read_float(area_json);
    return true;
}

void game_particle_emitter_from_prototype(struct game_component_particle_emitter* out,
        struct game_particle_emitter_prototype* proto)
{
    out->particle_proto_id = proto->particle_proto_id;
    out->next_particle_tick = 0;
    out->rate = proto->rate;
    out->rate_variability = proto->rate_variability;
    out->area_variability = proto->area;
    out->particle_vel = proto->dir;
    out->vel_variability = proto->dir_variability;
}




void game_component_particle_emitter_step(struct gameplay_state* game,
        struct game_component_particle_emitter* emitter)
{
    struct game_system_particles* particles = &game->particles;
    if(emitter->next_particle_tick <= game->tick) {
        float rate = emitter->rate + (emitter->rate * RANDF2(emitter->rate_variability));
        rate = LM_MAX(0, rate);
        emitter->next_particle_tick = game->tick + (uint64_t)rate;
        vec3 pos_offset = vec3(
            RANDF2(emitter->area_variability),
            RANDF2(emitter->area_variability),
            RANDF2(emitter->area_variability) );
        vec3 vel_offset = vec3(
            RANDF2(emitter->vel_variability.x),
            RANDF2(emitter->vel_variability.y),
            RANDF2(emitter->vel_variability.z) );
        struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, emitter->ent_id);
        if(!ent_ptr) return;
        vec3 pos = vec3_add(ent_ptr->pos, pos_offset);
        vec3 vel = vec3_add(emitter->particle_vel, vel_offset);
        struct game_particle* new_part = game_system_particles_new(game, pos, vel, emitter->particle_proto_id);
        if(!new_part) return;
    }
}
