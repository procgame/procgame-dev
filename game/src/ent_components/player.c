#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(player);

void game_component_player_init(struct game_component_player* player_ptr)
{
    player_ptr->look = vec2(0, M_PI / 2);
    player_ptr->jump_timer = 0;
}
