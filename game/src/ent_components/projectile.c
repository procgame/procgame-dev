#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(projectile);

bool game_read_projectile_prototype(struct game_projectile_prototype* out, cJSON* json)
{
    cJSON* hit_func_json, *is_fast_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_ITEM("hit_func", cJSON_String, &hit_func_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("fast", cJSON_Bool, &is_fast_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid projectile prototype JSON");
        return false;
    }

    strncpy(out->hit_func_name, hit_func_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    out->is_fast = is_fast_json ? cJSON_IsTrue(is_fast_json) : true;
    return true;
}

void game_projectile_from_prototype(struct game_component_projectile* out, struct game_projectile_prototype* proto)
{
    out->hit_fn = proto->hit_func;
    out->fast_mover = proto->is_fast;
}



static int game_projectile_hit_fn_bullet(struct gameplay_state* game, vec3 hit_pos,
        game_entity_id_t proj_ent_id, game_entity_id_t hit_ent_id)
{
    //printf("%lx HITS %lx\n", proj_ent_id, hit_ent_id);
    struct game_entity* proj_ent_ptr = GAME_ENTITY_PTR(game, proj_ent_id);
    if(proj_ent_ptr) game_entity_destroy(game, proj_ent_ptr);
    return 1;
}

void game_register_projectile_hit_funcs(struct game_entity_callbacks* cbs)
{
    HTABLE_SET(cbs->projectile_hit_fn_table, "bullet", game_projectile_hit_fn_bullet);
}
