#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(heightmap);

void game_component_heightmap_init(struct game_component_heightmap* hmap_ptr,
                                   ivec2 size, float cell_size, int mesh_chunk_idx)
{
    pg_heightmap_init(&hmap_ptr->hmap, size, cell_size);
    hmap_ptr->terrain_chunk_idx = mesh_chunk_idx;
}
