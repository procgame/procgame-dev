/*  PROJECTILE  */
typedef int (*game_projectile_hit_fn_t)(struct gameplay_state*, vec3,
                                        game_entity_id_t, game_entity_id_t);
typedef HTABLE_T(game_projectile_hit_fn_t) game_projectile_hit_fn_table_t;

GAME_COMPONENT(projectile, GAME_ENTITY_COMPONENT_PROJECTILE,
    game_entity_id_t src_ent;
    bool fast_mover;
    game_projectile_hit_fn_t hit_fn;
);

struct game_projectile_prototype {
    char hit_func_name[GAME_PROTOTYPE_NAME_MAXLEN];
    game_projectile_hit_fn_t hit_func;
    bool is_fast;
    float speed;
};


bool game_read_projectile_prototype(struct game_projectile_prototype* out, cJSON* json);
void game_projectile_from_prototype(struct game_component_projectile* out, struct game_projectile_prototype* proto);
