/*  COLLIDER        */
GAME_COMPONENT(collider, GAME_ENTITY_COMPONENT_COLLIDER,
    uint32_t query_no, collision_no;
    struct pg_collider phys;
    aabb3D origin_bound, current_bound;
    vec3 last_pos;
    bool active, always_active;
    uint32_t active_ticks;
    float raycast_threshold;
    bool clamp_to_surface, on_surface;
    float clamping_offset, clamping_distance;
);

void game_component_collider_init(struct game_component_collider* coll_ptr, struct pg_collider* phys);
void game_component_collider_update_bound(struct game_component_collider* coll_ptr);
void game_entity_bound_from_collider(struct gameplay_state* game, struct game_entity* ent_ptr);

struct game_collider_prototype {
    enum pg_collider_type type;
    union {
        sphere sphere;
        aabb3D aabb;
        obb3D obb;
    };
    float mass;
    bool friction;
    float clamp_distance, clamp_offset;
};

bool game_read_collider_prototype(struct game_collider_prototype* out, cJSON* json);
void game_collider_from_prototype(struct game_component_collider* out, struct game_collider_prototype* proto);


