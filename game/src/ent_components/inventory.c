#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(inventory);

void game_component_inventory_init(struct game_component_inventory* inv_ptr)
{
    ARR_INIT(inv_ptr->contents);
    inv_ptr->held_index = -1;
}

void game_component_inventory_step(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr)
{
    game_entity_id_t ent_id = inv_ptr->ent_id;
    struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, ent_id);
    if(!ent_ptr) return;
    int i;
    game_entity_id_t cont_ent_id;
    struct game_entity* cont_ent_ptr;
    ARR_FOREACH_REV(inv_ptr->contents, cont_ent_id, i) {
        if(!(cont_ent_ptr = GAME_ENTITY_PTR(game, cont_ent_id))) continue;
        cont_ent_ptr->pos = ent_ptr->pos;
    }
}

void game_component_inventory_add_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id)
{
    struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, ent_id);
    if(!ent_ptr) return;
    ENT_COMPONENT_ENABLE(game, ent_ptr, collider, false);
    ENT_COMPONENT_ENABLE(game, ent_ptr, drawable, false);
    ENT_COMPONENT_ENABLE(game, ent_ptr, particle_emitter, false);
    ARR_PUSH(inv_ptr->contents, ent_id);
}

void game_component_inventory_remove_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id)
{
    int i;
    game_entity_id_t cont_ent_id;
    struct game_entity* cont_ent_ptr;
    ARR_FOREACH(inv_ptr->contents, cont_ent_id, i) {
        if(cont_ent_id == ent_id) break;
    }
    if(i == inv_ptr->contents.len) return;
    cont_ent_ptr = GAME_ENTITY_PTR(game, cont_ent_id);
    if(cont_ent_ptr) {
        ENT_COMPONENT_ENABLE(game, cont_ent_ptr, collider, true);
        ENT_COMPONENT_ENABLE(game, cont_ent_ptr, drawable, true);
        ENT_COMPONENT_ENABLE(game, cont_ent_ptr, particle_emitter, true);
    }
    ARR_SPLICE(inv_ptr->contents, i, 1);
    if(i == inv_ptr->held_index) {
        inv_ptr->held_index = -1;
        inv_ptr->held_id = 0;
    } else if(i < inv_ptr->held_index) {
        --inv_ptr->held_index;
    }
}

void game_component_inventory_use_held_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, int control_held)
{
    if(!inv_ptr->held_id) return;
    game_entity_id_t user_id = inv_ptr->ent_id;
    game_entity_id_t held_id = inv_ptr->held_id;
    struct game_entity* held_ent_ptr = GAME_ENTITY_PTR(game, held_id);
    if(!held_ent_ptr) return;
    struct game_component_item* item_ptr = ENT_COMPONENT_PTR(game, held_ent_ptr, item);
    game_component_item_use(game, item_ptr, user_id, control_held);
}

void game_component_inventory_hold_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id)
{
    int i;
    game_entity_id_t cont_ent_id;
    struct game_entity* cont_ent_ptr;
    ARR_FOREACH(inv_ptr->contents, cont_ent_id, i) {
        if(cont_ent_id == ent_id) break;
    }
    if(i == inv_ptr->contents.len) {
        game_component_inventory_add_item(game, inv_ptr, ent_id);
    }
    inv_ptr->held_index = i;
    inv_ptr->held_id = ent_id;
}

void game_component_inventory_next_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr)
{
    if(!inv_ptr->contents.len) return;
    inv_ptr->held_index = LM_MOD(inv_ptr->held_index + 1, inv_ptr->contents.len);
    inv_ptr->held_id = inv_ptr->contents.data[inv_ptr->held_index];
}

void game_component_inventory_prev_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr)
{
    if(!inv_ptr->contents.len) return;
    inv_ptr->held_index = LM_MOD(inv_ptr->held_index - 1, inv_ptr->contents.len);
    inv_ptr->held_id = inv_ptr->contents.data[inv_ptr->held_index];
}

game_entity_id_t game_component_inventory_get_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, int idx)
{
    if(idx < 0 || idx >= inv_ptr->contents.len) return 0;
    return inv_ptr->contents.data[idx];
}
