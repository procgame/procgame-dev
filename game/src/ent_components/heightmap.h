/*  HEIGHTMAP       */
GAME_COMPONENT(heightmap, GAME_ENTITY_COMPONENT_HEIGHTMAP,
    struct pg_heightmap hmap;
    vec3 pos;
    int terrain_chunk_idx;
);

void game_component_heightmap_init(struct game_component_heightmap* hmap_ptr,
                                   ivec2 size, float cell_size, int mesh_chunk_idx);


