/*  PARTICLE EMITTER    */
GAME_COMPONENT(particle_emitter, GAME_ENTITY_COMPONENT_PARTICLE_EMITTER,
    game_particle_prototype_id_t particle_proto_id;
    uint64_t next_particle_tick;
    float rate;
    float rate_variability;
    float area_variability;
    vec3 particle_vel;
    vec3 vel_variability;
);

void game_component_particle_emitter_step(struct gameplay_state* game,
        struct game_component_particle_emitter* emitter);


struct game_particle_emitter_prototype {
    char particle_name[GAME_PROTOTYPE_NAME_MAXLEN];
    int particle_proto_id;
    float rate, rate_variability;
    vec3 dir, dir_variability;
    float area;
};

bool game_read_particle_emitter_prototype(struct game_particle_emitter_prototype* out, cJSON* json);
void game_particle_emitter_from_prototype(struct game_component_particle_emitter* out,
        struct game_particle_emitter_prototype* proto);
