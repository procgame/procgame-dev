/*  INVENTORY   */
GAME_COMPONENT(inventory, GAME_ENTITY_COMPONENT_INVENTORY,
    int held_index;
    game_entity_id_t held_id;
    game_entity_arr_t contents;
);

void game_component_inventory_init(struct game_component_inventory* inv_ptr);
void game_component_inventory_step(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr);
void game_component_inventory_add_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id);
void game_component_inventory_remove_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id);
void game_component_inventory_use_held_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, int control_held);
void game_component_inventory_hold_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, game_entity_id_t ent_id);
void game_component_inventory_next_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr);
void game_component_inventory_prev_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr);
game_entity_id_t game_component_inventory_get_item(struct gameplay_state* game,
        struct game_component_inventory* inv_ptr, int idx);



