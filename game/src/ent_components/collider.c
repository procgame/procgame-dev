#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(collider);

void game_component_collider_init(struct game_component_collider* coll_ptr, struct pg_collider* phys)
{
    coll_ptr->phys = *phys;
    coll_ptr->clamp_to_surface = false;
    coll_ptr->clamping_distance = 0;
    aabb3D bound = pg_collider_get_bound(phys);
    coll_ptr->origin_bound = bound;
    coll_ptr->raycast_threshold = fabs(vec3_vmin(vec3_sub(bound.b1, bound.b0)));
}

bool game_read_collider_prototype(struct game_collider_prototype* out, cJSON* json)
{
    cJSON* mass_json, *friction_json, *clamp_distance_json, *clamp_offset_json, *sphere_json, *aabb_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(4,
        PG_JSON_LAYOUT_ITEM("mass", cJSON_Number, &mass_json),
        PG_JSON_LAYOUT_OPTIONAL_GROUP("friction", &friction_json, 2,
            PG_JSON_LAYOUT_ITEM("clamp_distance", cJSON_Number, &clamp_distance_json),
            PG_JSON_LAYOUT_ITEM("clamp_offset", cJSON_Number, &clamp_offset_json),
        ),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("sphere", cJSON_Number, &sphere_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("aabb", cJSON_Array, &aabb_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid collider prototype JSON");
        return false;
    }

    out->mass = mass_json->valuedouble;
    if(friction_json) {
        out->friction = true;
        out->clamp_distance = clamp_distance_json->valuedouble;
        out->clamp_offset = clamp_offset_json->valuedouble;
    }
    if(sphere_json) {
        out->type = PG_COLLIDER_SPHERE;
        out->sphere = SPHERE(vec3(0), sphere_json->valuedouble);
    } else if(aabb_json) {
        out->type = PG_COLLIDER_AABB;
        json_read_aabb3D(aabb_json, &out->aabb);
    }
    return true;
}

void game_collider_from_prototype(struct game_component_collider* out, struct game_collider_prototype* proto)
{
    struct pg_collider phys = {
        .flags = PG_COLLIDER_STATIC,
        .type = proto->type,
        .mass = proto->mass };
    if(proto->type == PG_COLLIDER_SPHERE) {
        phys.sph = proto->sphere;
    } else if(proto->type == PG_COLLIDER_AABB) {
        phys.box = proto->aabb;
    } else if(proto->type == PG_COLLIDER_OBB) {
        phys.obb = proto->obb;
    }
    game_component_collider_init(out, &phys);
    if(proto->friction) {
        out->clamp_to_surface = true;
        out->clamping_distance = proto->clamp_distance;
        out->clamping_offset = proto->clamp_offset;
    }
}

void game_entity_bound_from_collider(struct gameplay_state* game, struct game_entity* ent_ptr)
{
}

void game_component_collider_update_bound(struct game_component_collider* coll_ptr)
{
    coll_ptr->origin_bound = pg_collider_get_bound(&coll_ptr->phys);
    coll_ptr->current_bound.b0 = vec3_add(coll_ptr->origin_bound.b0, coll_ptr->phys.pos);
    coll_ptr->current_bound.b1 = vec3_add(coll_ptr->origin_bound.b1, coll_ptr->phys.pos);
}


