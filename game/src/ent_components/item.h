/*  ITEM            */
typedef void (*game_item_use_fn_t)(struct gameplay_state*, game_entity_id_t,
                                   game_entity_id_t, int);
typedef HTABLE_T(game_item_use_fn_t) game_item_use_fn_table_t;

GAME_COMPONENT(item, GAME_ENTITY_COMPONENT_ITEM,
    char name[32];
    game_item_use_fn_t use_fn;
    game_entity_prototype_id_t projectile_proto;
);

void game_component_item_init(struct game_component_item* item_ptr, char* name,
                              game_item_use_fn_t use_fn);
void game_component_item_use(struct gameplay_state* game,
    struct game_component_item* item_ptr, game_entity_id_t user, int control_held);

void game_usefunc_test(struct gameplay_state* game, game_entity_id_t item_ent_id,
                       game_entity_id_t user_ent_id, int control_held);

struct game_item_prototype {
    char use_func_name[GAME_PROTOTYPE_NAME_MAXLEN];
    game_item_use_fn_t use_func;
    char projectile_name[GAME_PROTOTYPE_NAME_MAXLEN];
    game_entity_prototype_id_t projectile_proto_id;
};

bool game_read_item_prototype(struct game_item_prototype* out, cJSON* json);
void game_item_from_prototype(struct game_component_item* out, struct game_item_prototype* proto);
