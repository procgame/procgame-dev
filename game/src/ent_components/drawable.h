/*  DRAWABLE        */
typedef void (*game_draw_fn_t)(struct gameplay_state*, struct game_entity*, float);
typedef HTABLE_T(game_draw_fn_t) game_draw_fn_table_t;

GAME_COMPONENT(drawable, GAME_ENTITY_COMPONENT_DRAWABLE,
    game_draw_fn_t draw_fn;
    uint32_t color_mul, color_add;
    char model_name[128], submodel_name[128];
    pg_tex_frame_t tex;
    vec3 scale;
);

struct game_drawable_prototype {
    char draw_func_name[GAME_PROTOTYPE_NAME_MAXLEN];
    game_draw_fn_t draw_func;
    vec3 scale;
    /*  Quad draw   */
    char image_name[128], frame_name[128];
    pg_tex_frame_t tex_frame;
    vec4 color_mul_f, color_add_f;
    uint32_t color_add, color_mul;
    /*  Model draw  */
    char model_name[128], submodel_name[128];
    int model_id;
    ivec2 model_verts_range, model_idx_range;
};

bool game_read_drawable_prototype(struct game_drawable_prototype* out, cJSON* json);
void game_drawable_from_prototype(struct game_component_drawable* out, struct game_drawable_prototype* proto);


void game_draw_aabb(struct gameplay_state* game, struct game_entity* ent, float dt);
