#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(item);

void game_component_item_init(struct game_component_item* item_ptr, char* name,
                              game_item_use_fn_t use_fn)
{
    strncpy(item_ptr->name, name, 32);
    item_ptr->use_fn = use_fn;
}




bool game_read_item_prototype(struct game_item_prototype* out, cJSON* json)
{
    cJSON* use_func_json, *projectile_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(2,
        PG_JSON_LAYOUT_ITEM("use_func", cJSON_String, &use_func_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("projectile", cJSON_String, &projectile_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid item prototype JSON");
        return false;
    }

    strncpy(out->use_func_name, use_func_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    if(projectile_json) strncpy(out->projectile_name, projectile_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    return true;
}

void game_item_from_prototype(struct game_component_item* out, struct game_item_prototype* proto)
{
    game_component_item_init(out, "item", proto->use_func);
}




void game_component_item_use(struct gameplay_state* game,
    struct game_component_item* item_ptr, game_entity_id_t user, int control_held)
{
    if(!item_ptr->use_fn) return;
    item_ptr->use_fn(game, item_ptr->ent_id, user, control_held);
}





/****************/
/*  GUN ITEM    */

void game_usefunc_test(struct gameplay_state* game, game_entity_id_t item_ent_id,
                       game_entity_id_t user_ent_id, int control_held)
{
    if(control_held) {
        struct game_entity* user_ptr = GAME_ENTITY_PTR(game, user_ent_id);
        vec3 pos = vec3_add(user_ptr->pos, vec3(0,0,0.5));
        vec3 dir = user_ptr->facing;
        struct game_entity* item_ent_ptr = GAME_ENTITY_PTR(game, item_ent_id);
        struct game_component_item* item_ptr = ENT_COMPONENT_PTR(game, item_ent_ptr, item);

        struct game_entity* new_ent_ptr = game_create_entity_from_prototype_id(game, item_ptr->projectile_proto, NULL);
        new_ent_ptr->pos = pos;
        new_ent_ptr->vel = vec3_scale(dir, 2);
        struct game_component_projectile* proj_ptr = ENT_COMPONENT_PTR(game, new_ent_ptr, projectile);
        if(!proj_ptr) {
            printf("the fuck\n");
        } else {
            proj_ptr->src_ent = user_ent_id;
        }
    }
}

void game_register_item_use_funcs(struct game_entity_callbacks* cbs)
{
    HTABLE_SET(cbs->item_use_fn_table, "test", game_usefunc_test);
}
