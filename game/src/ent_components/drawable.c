#include "procgame.h"
#include "game.h"

GAME_COMPONENT_IMPL(drawable);

bool game_read_drawable_prototype(struct game_drawable_prototype* out, cJSON* json)
{
    cJSON* func_json, *image_json, *frame_json, *scale_json, *color_mul_json, *color_add_json,
            *model_json, *submodel_json;
    struct pg_json_layout layout = PG_JSON_LAYOUT(8,
        PG_JSON_LAYOUT_ITEM("draw_func", cJSON_String, &func_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("scale", cJSON_Array, &scale_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("color_mul", cJSON_Array, &color_mul_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("color_add", cJSON_Array, &color_add_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("image", cJSON_String, &image_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("frame", cJSON_String, &frame_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("model", cJSON_String, &model_json),
        PG_JSON_LAYOUT_OPTIONAL_ITEM("submodel", cJSON_String, &submodel_json),
    );
    if(!pg_load_json_layout(json, &layout)) {
        pg_log(PG_LOG_ERROR, "Invalid drawable prototype JSON");
        return false;
    }

    *out = (struct game_drawable_prototype){};

    out->color_mul_f = vec4(1,1,1,1);
    out->scale = vec3(1,1,1);

    strncpy(out->draw_func_name, func_json->valuestring, GAME_PROTOTYPE_NAME_MAXLEN);
    if(image_json) strncpy(out->image_name, image_json->valuestring, 128);
    if(frame_json) strncpy(out->frame_name, frame_json->valuestring, 128);
    if(model_json) strncpy(out->model_name, model_json->valuestring, 128);
    if(submodel_json) strncpy(out->submodel_name, submodel_json->valuestring, 128);
    if(color_mul_json) out->color_mul_f = json_read_vec4(color_mul_json);
    if(color_add_json) out->color_add_f = json_read_vec4(color_add_json);
    if(scale_json) out->scale = json_read_vec3(scale_json);
    return true;
}

void game_drawable_from_prototype(struct game_component_drawable* out, struct game_drawable_prototype* proto)
{
    out->draw_fn = proto->draw_func;
    out->color_mul = proto->color_mul;
    out->color_add = proto->color_add;
    out->tex = proto->tex_frame;
    out->scale = proto->scale;
    strncpy(out->model_name, proto->model_name, 128);
    strncpy(out->submodel_name, proto->submodel_name, 128);
}




void game_draw_aabb(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, ent, collider);
    if(!coll_ptr || coll_ptr->phys.type != PG_COLLIDER_AABB) return;
    vec3 ctr = vec3_add(coll_ptr->phys.pos, aabb3D_center(&coll_ptr->phys.box));
    game_system_debugtools_add_aabb3D(game, true, vec3_add(ctr, coll_ptr->phys.box.b0), vec3_sub(coll_ptr->phys.box.b1, coll_ptr->phys.box.b0), 0.25, 0xFFFFFFFF);
}

void game_draw_quad(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_PTR(game, ent, drawable);
    if(!draw_ptr) return;
    vec3 pos = ent->traits[GAME_ENTITY_MOVES] ?
        vec3_add(ent->pos, vec3_scale(ent->vel, dt)) : ent->pos;
    struct pg_ezquad q = PG_EZQUAD(
        .pos = pos, .scale = vec2(VEC_XY(draw_ptr->scale)),.orientation = ent->rot,
        .color_add = draw_ptr->color_add, .color_mul = draw_ptr->color_mul,
        .uv_v = draw_ptr->tex.v,
        .tex_layer = draw_ptr->tex.layer);
    pg_quadbatch_add_quad(render->world_quads, &q);
}

void game_draw_quad_vertical(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_PTR(game, ent, drawable);
    if(!draw_ptr) return;
    vec3 pos = ent->traits[GAME_ENTITY_MOVES] ?
        vec3_add(ent->pos, vec3_scale(ent->vel, dt)) : ent->pos;
    struct pg_ezquad q = PG_EZQUAD(
        .pos = pos, .scale = vec2(VEC_XY(draw_ptr->scale)),
        .orientation = quat_mul(render->y_axis, ent->rot),
        .color_add = draw_ptr->color_add, .color_mul = draw_ptr->color_mul,
        .uv_v = draw_ptr->tex.v,
        .tex_layer = draw_ptr->tex.layer);
    pg_quadbatch_add_quad(render->world_quads, &q);
}

void game_draw_billboard(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_PTR(game, ent, drawable);
    if(!draw_ptr) return;
    vec3 pos = ent->traits[GAME_ENTITY_MOVES] ?
        vec3_add(ent->pos, vec3_scale(ent->vel, dt)) : ent->pos;
    struct pg_ezquad q = PG_EZQUAD(
        .pos = pos, .scale = vec2(VEC_XY(draw_ptr->scale)), .orientation = render->billboard,
        .color_add = draw_ptr->color_add, .color_mul = draw_ptr->color_mul,
        .uv_v = draw_ptr->tex.v,
        .tex_layer = draw_ptr->tex.layer);
    pg_quadbatch_add_quad(render->world_quads, &q);
}

void game_draw_billboard_vertical(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_PTR(game, ent, drawable);
    if(!draw_ptr) return;
    vec3 pos = ent->traits[GAME_ENTITY_MOVES] ?
        vec3_add(ent->pos, vec3_scale(ent->vel, dt)) : ent->pos;
    struct pg_ezquad q = PG_EZQUAD(
        .pos = pos, .scale = vec2(VEC_XY(draw_ptr->scale)), .orientation = render->billboard_vertical,
        .color_add = draw_ptr->color_add, .color_mul = draw_ptr->color_mul,
        .uv_v = draw_ptr->tex.v,
        .tex_layer = draw_ptr->tex.layer);
    pg_quadbatch_add_quad(render->world_quads, &q);
}

void game_draw_model(struct gameplay_state* game, struct game_entity* ent, float dt)
{
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_PTR(game, ent, drawable);
    if(!draw_ptr) return;

    struct game_system_render* render = &game->render;

    pg_gpu_render_stage_t* models_pass = render->models_pass;
    int models_group = pg_gpu_render_stage_get_input_group(models_pass, draw_ptr->model_name);
    if(models_group == -1) {
        pg_log(PG_LOG_ERROR, "Invalid model name '%s'", draw_ptr->model_name);
        return;
    }
    ivec2 verts_range, idx_range;
    pg_gpu_render_stage_get_input_range(models_pass, models_group, draw_ptr->submodel_name, &verts_range, &idx_range);
    if(ivec2_is_zero(verts_range) || ivec2_is_zero(idx_range)) {
        pg_log(PG_LOG_ERROR, "Invalid submodel name '%s'", draw_ptr->submodel_name);
        return;
    }

    vec3 pos = ent->traits[GAME_ENTITY_MOVES] ? vec3_add(ent->pos, vec3_scale(ent->vel, dt)) : ent->pos;
    mat4 tx = mat4_object_space(pos, draw_ptr->scale, quat_identity());
    int model_matrix_loc = pg_gpu_render_stage_get_uniform_location(models_pass, "model_matrix");
    
    pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_commands(models_pass, models_group);
    pg_gpu_record_commands(commands);
    pg_gpu_cmd_set_uniform_matrix(model_matrix_loc, PG_MAT4, false, 1, &tx);
    pg_gpu_cmd_draw_triangles(idx_range.x, idx_range.y, .indexed = true, .base_vertex = verts_range.x);
}

void game_register_draw_funcs(struct game_entity_callbacks* cbs)
{
    HTABLE_SET(cbs->draw_fn_table, "billboard", game_draw_billboard);
    HTABLE_SET(cbs->draw_fn_table, "billboard_vertical", game_draw_billboard);
    HTABLE_SET(cbs->draw_fn_table, "quad", game_draw_quad);
    HTABLE_SET(cbs->draw_fn_table, "quad_vertical", game_draw_quad_vertical);
    HTABLE_SET(cbs->draw_fn_table, "model", game_draw_model);
}


