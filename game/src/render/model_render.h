
//typedef pg_gfx_render_processor game_model_processor_t;
//
//struct game_model_processor {
//    pg_gpu_render_stage_t* render_stage;
//    int object_matrix_uniform_location;
//
//    SARR_T(32, struct submodel_range {
//        ivec2 vertex_range, index_range;
//    }) submodels;
//
//    ARR_T(struct game_model_processor_handle {
//        int submodel_idx;
//        mat4 tx;
//    }) handles;
//};
//
//uint64_t game_model_processor_create_handle(void* udata, void** handle_data_out)
//{
//    struct game_model_processor* processor = (struct game_model_processor*)udata;
//    struct game_model_processor_handle* new_handle_data = ARR_NEW(processor->handles);
//    new_handle_data->tx = mat4_identity();
//    new_handle_data->submodel_idx;
//    if(handle_data_out) *handle_data_out = new_handle_data;
//    return processor->handles.len - 1;
//}
//
//void* game_model_processor_get_handle_data(void* udata, uint64_t handle_id)
//{
//    struct game_model_processor* processor = (struct game_model_processor*)udata;
//    return &processor->handles.data[handle_id];
//}
//
//void game_model_processor_delete_handle(void* udata, uint64_t handle_id)
//{
//    /*  Do nothing  */
//}
//
//void game_model_processor_process_handle(void* udata, uint64_t handle_id)
//{
//    struct game_model_processor* processor = (struct game_model_processor*)udata;
//    struct game_model_processor_handle* handle_data = &processor->handles.data[handle_id];
//    pg_log(PG_LOG_DEBUG, "Draw model!");
//}
//
//
//
//
//
//
//
//
//game_model_processor_t* game_model_processor_create(pg_gpu_render_stage_t* render_stage, 
