#include "procgame.h"
#include "game.h"

void game_system_projectiles_step(struct gameplay_state* game)
{
    /*  Iterate through all projectile components and do the thing.   */
    int i;
    struct game_entity* ent_ptr;
    struct game_entity* src_ent_ptr;
    game_component_projectile_id_t proj_id;
    struct game_component_projectile* proj_ptr;
    struct game_component_collider* coll_ptr;
    struct game_component_collider* src_coll_ptr;
    ARR_FOREACH(game->projectile_components, proj_id, i) {
        if(!(proj_ptr = GAME_COMPONENT_PTR(game, projectile, proj_id))
        || !(ent_ptr = GAME_ENTITY_PTR(game, proj_ptr->ent_id))) {
            ARR_SWAPSPLICE(game->projectile_components, i, 1);
            --i;
            continue;
        }
        if(!proj_ptr->enabled) continue;
        /*  Exclude this entity and the source entity from the queries  */
        game_grid_new_collider_query(game);
        if((src_ent_ptr = GAME_ENTITY_PTR(game, proj_ptr->src_ent))
        && (src_coll_ptr = ENT_COMPONENT_PTR(game, src_ent_ptr, collider))) {
            src_coll_ptr->query_no = game->grid.query_no;
        }
        if((coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider))) {
            coll_ptr->query_no = game->grid.query_no;
        }

        /********************************/
        /*  Check for entities to hit   */
        float vel_len = vec3_len(ent_ptr->vel);
        vec3 vel_n = vec3_tdiv(ent_ptr->vel, vel_len);
        game_entity_id_t hit_ent_id = 0;
        vec3 hit_pos = ent_ptr->pos;
        if(proj_ptr->fast_mover) {
            /*  Raycast forward for a collider to hit   */
            ray3D ray = RAY3D(ent_ptr->pos, vel_n);
            float ray_len = game_raycast_all_colliders(game, ray, vel_len);
            if(ray_len > 0 && ray_len < vel_len && game->grid.collider_ray_hit) {
                hit_ent_id = game->grid.collider_ray_hit;
                hit_pos = vec3_add(ent_ptr->pos, vec3_scale(vel_n, ray_len));
            }
        } else {
            /*  Find the deepest collision for a hit    */
            vec3 sep = game_query_deepest_collision(game, ent_ptr->pos, 0.25, &hit_ent_id);
        }

        /****************************************************************/
        /*  If there's a hit then call the projectile's hit function    */
        if(hit_ent_id) {
            if(!proj_ptr->hit_fn) {
                game_entity_destroy(game, ent_ptr);
                ARR_SWAPSPLICE(game->projectile_components, i, 1);
                --i;
                continue;
            }
            proj_ptr->hit_fn(game, hit_pos, proj_ptr->ent_id, hit_ent_id);
        }
    }
}
