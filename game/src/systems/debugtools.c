#include <procgame.h>
#include "game.h"

void game_system_debugtools_init(struct gameplay_state* game)
{
    struct game_system_debugtools* debug = &game->debug;
    *debug = (struct game_system_debugtools){
        .graphs = {
            [GAME_DEBUG_GRAPH_FRAMERATE] = {
                .label = "Framerate", .units = " fps", .as_integer = true,
                .red_level = 30, .yellow_level = 50, .higher_is_better = true },
            [GAME_DEBUG_GRAPH_PHYSICS] = {
                .label = "Physics", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_PHYSICS_ACTIVE] = {
                .label = "Active Colliders", .as_integer = true,
                .red_level = 100, .yellow_level = 50 },
            [GAME_DEBUG_GRAPH_PHYSICS_CALCS] = {
                .label = "Collision Checks", .as_integer = true,
                .red_level = 100, .yellow_level = 50 },
            [GAME_DEBUG_GRAPH_WORLDSCROLLING] = {
                .label = "World-scrolling", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_PLAYERCONTROL] = {
                .label = "Player Control", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_PROJECTILES] = {
                .label = "Projectiles Update", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_PROJECTILES_COUNT] = {
                .label = "Projectile Count", .as_integer = true,
                .red_level = 75, .yellow_level = 25 },
            [GAME_DEBUG_GRAPH_PARTICLES] = {
                .label = "Particles Update", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_PARTICLES_COUNT] = {
                .label = "Particle Count", .as_integer = true,
                .red_level = game->particles.max_particles * 0.75,
                .yellow_level = game->particles.max_particles * 0.25 },
            [GAME_DEBUG_GRAPH_RENDER] = {
                .label = "Render Time", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_TOTAL_UPDATE] = {
                .label = "Total Logic Update", .units = " ms",
                .red_level = 0.75, .yellow_level = 0.25 },
            [GAME_DEBUG_GRAPH_TOTAL_FRAME] = {
                .label = "Total Frame Time", .units = " ms",
                .red_level = 1.5, .yellow_level = 0.5 },
        }
    };
    ARR_INIT(debug->gfx_3d);
    ARR_INIT(debug->gfx_2d);
    ARR_INIT(debug->gfx_3d_dynamic);
    ARR_INIT(debug->gfx_2d_dynamic);
    ivec2 screen_res = pg_window_size();
    //debug->text_scale = vec2_all(100 / screen_res.y * 0.75);
    debug->text_scale = vec2(1,1);
    debug->bottom_left = vec2_scale(vec2(VEC_XY(screen_res)), -0.5);
    debug->half_res = vec2_scale(vec2(VEC_XY(screen_res)), 0.5);
    //pg_viewer_ortho(&debug->screen_view, vec2(0,0), vec2(1,1), 0, vec2(VEC_XY(screen_res)));
    debug->quads = pg_asset_get_data_by_name(game->asset_mgr, "DebugQuads");
    debug->render_stage = pg_asset_get_data_by_name(game->asset_mgr, "DebugQuads.render_stage");
}

static uint32_t decide_graph_color(double value, double red, double yellow, bool green_high)
{
    if(green_high) {
        if(value <= red) return 0xFF0000FF;
        else if(value <= yellow) return 0xFFFF00FF;
        else return 0x00FF00FF;
    } else {
        if(value >= red) return 0xFF0000FF;
        else if(value >= yellow) return 0xFFFF00FF;
        else return 0x00FF00FF;
    }
}

static void draw_graph(struct gameplay_state* game, int graph_idx, vec2 bottom_left)
{
    struct game_system_debugtools* debug = &game->debug;
    struct game_debug_graph_data* graph = &debug->graphs[graph_idx];
    double* data = graph->data;
    vec2 graph_center = vec2_add(bottom_left, vec2(240,50));
    /*  Draw background */
    pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
        .pos = vec3(graph_center.x, graph_center.y+20), .scale = vec2(480, 140),
        .orientation = quat_identity(), .color_mul = 0, .color_add = 0x808080C0 ));
    /*  Find graph scale    */
    double max_found = 0;
    int i;
    for(i = 0; i < 240; ++i) {
        double s = data[(i+graph->idx) % 240];
        if(s > max_found) max_found = s;
    }
    /*  Lines and labels    */
    uint32_t line_colors[4] = {
        decide_graph_color(max_found, graph->red_level, graph->yellow_level, graph->higher_is_better),
        decide_graph_color(max_found * 0.75, graph->red_level, graph->yellow_level, graph->higher_is_better),
        decide_graph_color(max_found * 0.5, graph->red_level, graph->yellow_level, graph->higher_is_better),
        decide_graph_color(max_found * 0.25, graph->red_level, graph->yellow_level, graph->higher_is_better) };
    char labels[4][32];
    if(!graph->as_integer) {
        snprintf(labels[0], 32, "%.4f%.8s", max_found, graph->units);
        snprintf(labels[1], 32, "%.4f%.8s", max_found * 0.75, graph->units);
        snprintf(labels[2], 32, "%.4f%.8s", max_found * 0.5, graph->units);
        snprintf(labels[3], 32, "%.4f%.8s", max_found * 0.25, graph->units);
    } else {
        snprintf(labels[0], 32, "%d%.8s", (int)(max_found), graph->units);
        snprintf(labels[1], 32, "%d%.8s", (int)(max_found * 0.75), graph->units);
        snprintf(labels[2], 32, "%d%.8s", (int)(max_found * 0.5), graph->units);
        snprintf(labels[3], 32, "%d%.8s", (int)(max_found * 0.25), graph->units);
    }
    /*  Lines   */
    pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
        .pos = vec3(graph_center.x, bottom_left.y + 100), .scale = vec2(480, 1), .orientation = quat_identity(),
        .color_mul = 0, .color_add = line_colors[0] ));
    pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
        .pos = vec3(graph_center.x, bottom_left.y + 75), .scale = vec2(480, 1), .orientation = quat_identity(),
        .color_mul = 0, .color_add = line_colors[1] ));
    pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
        .pos = vec3(graph_center.x, bottom_left.y + 50), .scale = vec2(480, 1), .orientation = quat_identity(),
        .color_mul = 0, .color_add = line_colors[2] ));
    pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
        .pos = vec3(graph_center.x, bottom_left.y + 25), .scale = vec2(480, 1), .orientation = quat_identity(),
        .color_mul = 0, .color_add = line_colors[3]));
    /*  Draw graph samples  */
    for(i = 0; i < 240; ++i) {
        double s = data[(i+graph->idx) % 240] / max_found;
        vec2 point_pos = vec2(i*2, s * 100);
        point_pos = vec2_add(debug->bottom_left, point_pos);
        pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
            .pos = vec3(VEC_XY(point_pos)), .scale = vec2(2,2),
            .orientation = quat_identity(),
            .color_mul = 0, .color_add = 0xFFFFFFFF ));
    }
    vec3 bottom_left3 = vec3(VEC_XY(bottom_left));
    /*  Labels  */
    pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
        .formatter = &game->res->text_fmt, .str = labels[0], .len = 16,
        .scale = debug->text_scale, .pos = vec3_add(bottom_left3, vec3(0,100)) ));
    pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
        .formatter = &game->res->text_fmt, .str = labels[1], .len = 16,
        .scale = debug->text_scale, .pos = vec3_add(bottom_left3, vec3(0,75)) ));
    pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
        .formatter = &game->res->text_fmt, .str = labels[2], .len = 16,
        .scale = debug->text_scale, .pos = vec3_add(bottom_left3, vec3(0,50)) ));
    pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
        .formatter = &game->res->text_fmt, .str = labels[3], .len = 16,
        .scale = debug->text_scale, .pos = vec3_add(bottom_left3, vec3(0,25)) ));
    pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
        .formatter = &game->res->text_fmt, .str = graph->label, .len = 32,
        .scale = vec2_scale(debug->text_scale, 1.25), .pos = vec3_add(bottom_left3, vec3(150,100)) ));
}

static void draw_3d_gfx(struct gameplay_state* game);
static void draw_2d_gfx(struct gameplay_state* game);

static quat quat_from_mat4_tmp(mat4 mat_)
{
    mat4 mat = (mat_);
    float t;
    quat q;
    if(mat.col[2].v[2] < 0) {
        if(mat.col[0].v[0] >mat.col[1].v[1]) {
            t = 1 + mat.col[0].v[0] -mat.col[1].v[1] -mat.col[2].v[2];
            q = quat( t, mat.col[0].v[1]+mat.col[1].v[0], mat.col[2].v[0]+mat.col[0].v[2], mat.col[1].v[2]-mat.col[2].v[1] );
        } else {
            t = 1 -mat.col[0].v[0] + mat.col[1].v[1] -mat.col[2].v[2];
            q = quat( mat.col[0].v[1]+mat.col[1].v[0], t, mat.col[1].v[2]+mat.col[2].v[1], mat.col[2].v[0]-mat.col[0].v[2] );
        }
    } else {
        if(mat.col[0].v[0] < -mat.col[1].v[1]) {
            t = 1 -mat.col[0].v[0] -mat.col[1].v[1] + mat.col[2].v[2];
            q = quat( mat.col[2].v[0]+mat.col[0].v[2], mat.col[1].v[2]+mat.col[2].v[1], t, mat.col[0].v[1]-mat.col[1].v[0] );
        } else {
            t = 1 + mat.col[0].v[0] + mat.col[1].v[1] + mat.col[2].v[2];
            q = quat( mat.col[1].v[2]-mat.col[2].v[1], mat.col[2].v[0]-mat.col[0].v[2], mat.col[0].v[1]-mat.col[1].v[0], t );
        }
    }
    return quat_scale(q, 0.5 / sqrt(t));
}


void game_system_debugtools_draw(struct gameplay_state* game)
{
    /*  Draw the world scrolling debug stuff    */
    struct game_system_worldscrolling* scrolling = &game->scrolling;
    struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, scrolling->scroller);
    if(ent_ptr) {
        /*  The position of the entity the world scrolls around */
        vec2 center = vec2(VEC_XY(ent_ptr->pos));
        vec2 screen_res = vec2_scale(vec2(VEC_XY(pg_window_size())), 0.5);
        game_grid_draw_cells(game);
        struct game_world_chunk* game_chunk;
        int i;
        ARR_FOREACH_PTR(scrolling->world_chunks, game_chunk, i) {
            vec2 world_pos = vec2_mul(vec2(VEC_XY(game_chunk->region_idx)), vec2(64,64));
            world_pos = vec2_add(world_pos, vec2(32, 32));
            world_pos = vec2_add(world_pos, vec2_scale(screen_res, 0.6));
            game_system_debugtools_add_quad_2d(game, true,
                vec3(VEC_XY(world_pos), 0),
                vec2(62, 62),
                0xFFFFFF88
            );
        }
        vec2 scroller_pos = vec2_mul(vec2(VEC_XY(center)), vec2(0.25, 0.25));
        vec2 scroller_facing = vec2_mul(vec2(VEC_XY(ent_ptr->facing)), vec2(6, 6));
        vec2 scroller_facing_pos = vec2_add(scroller_pos, scroller_facing);
        scroller_pos = vec2_add(scroller_pos, vec2_scale(screen_res, 0.6));
        scroller_facing_pos = vec2_add(scroller_facing_pos, vec2_scale(screen_res, 0.6));
        game_system_debugtools_add_quad_2d(game, true,
            vec3(VEC_XY(scroller_pos), 0),
            vec2(8, 8),
            0xFFFF00FF
        );
        game_system_debugtools_add_quad_2d(game, true,
            vec3(VEC_XY(scroller_facing_pos), 0),
            vec2(4, 4),
            0xFFFF00FF
        );
    }

    int i;
    struct game_world_chunk* loaded_chunk;
    ARR_FOREACH_PTR(scrolling->world_chunks, loaded_chunk, i) {
        vec3 grid_size = vec3(WORLD_REGION_SIZE, WORLD_REGION_SIZE, 128);
        vec3 cell_corner_pos = vec3_scale(vec3(VEC_XY(loaded_chunk->region_idx), 0), WORLD_REGION_SIZE);
        game_system_debugtools_add_aabb3D(game, true, cell_corner_pos, grid_size, 4, 0xFF888888);
        struct game_entity* chunk_entity = GAME_ENTITY_PTR(game, loaded_chunk->heightmap);
        struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, chunk_entity, collider);
        game_system_debugtools_add_aabb3D(game, true, coll_ptr->current_bound.b0,
                vec3_sub(coll_ptr->current_bound.b1, coll_ptr->current_bound.b0), 0.1, 0xFFCCCCFF);
        game_system_debugtools_add_quad(game, true, coll_ptr->current_bound.b0, quat_identity(), vec2(1, 1), 0xFF0000FF);
    }


    struct game_system_debugtools* debug = &game->debug;
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    pg_quadbatch_reset(debug->quads);

    pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_commands(debug->render_stage, 0);
    ARR_TRUNCATE(*commands, 0);
    pg_gpu_record_commands(commands);
    pg_gpu_cmd_depth(false, PG_GPU_LEQUAL, false);

    /*  Draw VR stuff   */
    if(pg_have_vr()) {
        quat y_up = quat_rotation(vec3_X(), -LM_PI_2);
        struct pg_gfx_transform left_tx = playercontrol->player_pose.hand_tx[0];
        struct pg_gfx_transform right_tx = playercontrol->player_pose.hand_tx[1];
        quat left_quat = quat_mul(quat_from_mat4_tmp(left_tx.tx), y_up);
        quat right_quat = quat_mul(quat_from_mat4_tmp(right_tx.tx), y_up);
        vec3 left_pos = mat4_mul_vec3(left_tx.tx, vec3(0,0,0), true);
        vec3 right_pos = mat4_mul_vec3(right_tx.tx, vec3(0,0,0), true);
        game_system_debugtools_add_quad(game, true, left_pos, left_quat, vec2(0.05,0.2), 0xFFCCCCFF);
        game_system_debugtools_add_quad(game, true, right_pos, right_quat, vec2(0.05,0.2), 0xCCFFCCFF);

        vec3 left_dir = vec3_scale(mat4_mul_vec3(left_tx.tx, vec3(0,0,-1), false), 0.1);
        vec3 right_dir = vec3_scale(mat4_mul_vec3(right_tx.tx, vec3(0,0,-1), false), 0.1);
        game_system_debugtools_add_quad(game, true, vec3_add(left_pos, left_dir),
            left_quat, vec2(0.1,0.1), 0xFF0000FF);
        game_system_debugtools_add_quad(game, true, vec3_add(right_pos, right_dir),
            right_quat, vec2(0.1,0.1), 0x00FF00FF);
    }

    /*  Draw 3D debug stuff */
    mat4 projview = mat4_identity();
    pg_quadbatch_next(debug->quads, &projview);
    draw_3d_gfx(game);
    pg_gpu_cmd_draw_quadbatch(debug->quads);

    ///*  Draw 2D debug stuff */
    //pg_quadbatch_next(debug->quads, &debug->screen_view.projview_matrix);
    //draw_graph(game, debug->selected_graph, debug->bottom_left);
    //draw_2d_gfx(game);
    //pg_gpu_cmd_draw_quadbatch(debug->quads);

    pg_quadbatch_upload(debug->quads);

    ARR_TRUNCATE(game->debug.gfx_3d_dynamic, 0);
    ARR_TRUNCATE(game->debug.gfx_2d_dynamic, 0);
}

static void draw_3d_gfx(struct gameplay_state* game)
{
    struct game_system_debugtools* debug = &game->debug;
    int i;
    struct game_debug_gfx* gfx_ptr;
    ARR_FOREACH_PTR(debug->gfx_3d, gfx_ptr, i) {
        switch(gfx_ptr->type) {
        case GAME_DEBUG_LINE: case GAME_DEBUG_QUAD:
            pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
                .pos = gfx_ptr->pos, .scale = gfx_ptr->scale,
                .orientation = gfx_ptr->rot, .color_mul = 0,
                .color_add = gfx_ptr->color));
            break;
        case GAME_DEBUG_LABEL: {
            pg_quadbatch_add_text(debug->quads, &PG_EZDRAW_TEXT(
                .formatter = &PG_TEXT_FORMATTER_MOD(game->res->text_fmt,
                    .color_palette[0] = gfx_ptr->color, .size = debug->text_scale),
                .str = gfx_ptr->label, .len = 32,
                .scale = gfx_ptr->scale, .pos = gfx_ptr->pos, .orientation = gfx_ptr->rot));
            break;
        }
        default: break;
        }
    }
    ARR_FOREACH_PTR(debug->gfx_3d_dynamic, gfx_ptr, i) {
        switch(gfx_ptr->type) {
        case GAME_DEBUG_LINE: case GAME_DEBUG_QUAD:
            pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
                .pos = gfx_ptr->pos, .scale = gfx_ptr->scale,
                .orientation = gfx_ptr->rot, .color_mul = 0,
                .color_add = gfx_ptr->color));
            break;
        default: break;
        }
    }
}

static void draw_2d_gfx(struct gameplay_state* game)
{
    struct game_system_debugtools* debug = &game->debug;
    int i;
    struct game_debug_gfx* gfx_ptr;
    ARR_FOREACH_PTR(debug->gfx_2d, gfx_ptr, i) {
        switch(gfx_ptr->type) {
        case GAME_DEBUG_LINE: case GAME_DEBUG_QUAD:
            pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
                .pos = gfx_ptr->pos, .scale = gfx_ptr->scale,
                .orientation = gfx_ptr->rot, .color_mul = 0,
                .color_add = gfx_ptr->color));
            break;
        default: break;
        }
    }
    ARR_FOREACH_PTR(debug->gfx_2d_dynamic, gfx_ptr, i) {
        switch(gfx_ptr->type) {
        case GAME_DEBUG_LINE: case GAME_DEBUG_QUAD:
            pg_quadbatch_add_quad(debug->quads, &PG_EZQUAD(
                .pos = gfx_ptr->pos, .scale = gfx_ptr->scale,
                .orientation = gfx_ptr->rot, .color_mul = 0,
                .color_add = gfx_ptr->color));
            break;
        default: break;
        }
    }
}


void game_system_debugtools_add_data(struct gameplay_state* game,
        enum game_debug_graph_type graph, double s)
{
    struct game_system_debugtools* debug = &game->debug;
    struct game_debug_graph_data* graph_data = &debug->graphs[graph];
    graph_data->data[graph_data->idx] = s;
    graph_data->idx = (graph_data->idx + 1) % 240;
}

void game_system_debugtools_clear_gfx(struct gameplay_state* game)
{
    ARR_TRUNCATE(game->debug.gfx_3d, 0);
    ARR_TRUNCATE(game->debug.gfx_2d, 0);
}

void game_system_debugtools_add_matrix(struct gameplay_state* game, bool dynamic, mat4 mat)
{
    vec3 start = mat4_mul_vec3(mat, vec3(0,0,0), true);
    vec3 end[3] = {
        mat4_mul_vec3(mat, vec3_X(), true),
        mat4_mul_vec3(mat, vec3_Y(), true),
        mat4_mul_vec3(mat, vec3_Z(), true) };
    game_system_debugtools_add_line(game, dynamic, start, end[0], 0.025, 0xFF0000FF);
    game_system_debugtools_add_line(game, dynamic, start, end[1], 0.025, 0x00FF00FF);
    game_system_debugtools_add_line(game, dynamic, start, end[2], 0.025, 0x0000FFFF);
    game_system_debugtools_add_label(game, dynamic, vec3_add(vec3(-0.1,0,0), end[0]), vec2(0.05,0.05), 0xFFFFFFFF, "x");
    game_system_debugtools_add_label(game, dynamic, vec3_add(vec3(-0.1,0,0), end[1]), vec2(0.05,0.05), 0xFFFFFFFF, "y");
    game_system_debugtools_add_label(game, dynamic, vec3_add(vec3(-0.1,0,0), end[2]), vec2(0.05,0.05), 0xFFFFFFFF, "z");
}

void game_system_debugtools_add_matrix_labeled(struct gameplay_state* game, bool dynamic, mat4 mat, uint32_t color, const char* label)
{
    game_system_debugtools_add_matrix(game, dynamic, mat);
    vec3 pos = mat4_mul_vec3(mat, vec3(-0.1,-0.1,-0.1), true);
    game_system_debugtools_add_label(game, dynamic, pos, vec2(0.05,0.05), color, label);
}

void game_system_debugtools_add_line(struct gameplay_state* game, bool dynamic,
        vec3 start, vec3 end, float width, uint32_t color)
{

    vec3 pos = vec3_midpoint(end, start);
    vec3 start_to_end = vec3_sub(end, start);
    vec2 scale = vec2(width, vec3_len(start_to_end));
    quat rot = quat_from_to(vec3_Y(), vec3_norm(start_to_end));
    struct game_debug_gfx gfx = { .type = GAME_DEBUG_LINE,
        .pos = pos, .rot = rot, .scale = scale, .color = color };
    if(dynamic) ARR_PUSH(game->debug.gfx_3d_dynamic, gfx);
    else ARR_PUSH(game->debug.gfx_3d, gfx);
}

void game_system_debugtools_add_quad(struct gameplay_state* game, bool dynamic,
        vec3 pos, quat rot, vec2 scale, uint32_t color)
{
    struct game_debug_gfx gfx = { .type = GAME_DEBUG_QUAD,
        .pos = pos, .rot = rot, .scale = scale, .color = color };
    if(dynamic) ARR_PUSH(game->debug.gfx_3d_dynamic, gfx);
    else ARR_PUSH(game->debug.gfx_3d, gfx);
}

void game_system_debugtools_add_quad_2d(struct gameplay_state* game, bool dynamic,
        vec3 pos, vec2 scale, uint32_t color)
{
    struct game_debug_gfx gfx = { .type = GAME_DEBUG_QUAD,
        .pos = pos, .rot = quat_identity(), .scale = scale, .color = color };
    if(dynamic) ARR_PUSH(game->debug.gfx_2d_dynamic, gfx);
    else ARR_PUSH(game->debug.gfx_2d, gfx);
}


void game_system_debugtools_add_label(struct gameplay_state* game, bool dynamic,
        vec3 pos, vec2 scale, uint32_t color, const char* label)
{
    struct game_debug_gfx gfx = { .type = GAME_DEBUG_LABEL,
        .pos = pos, .scale = scale, .rot = quat_rotation(vec3_X(), LM_PI_2), .color = color };
    strncpy(gfx.label, label, 32);
    if(dynamic) ARR_PUSH(game->debug.gfx_3d_dynamic, gfx);
    else ARR_PUSH(game->debug.gfx_3d, gfx);
}

void game_system_debugtools_add_aabb3D(struct gameplay_state* game, bool dynamic,
        vec3 corner, vec3 size, float width, uint32_t color)
{
    vec3 a0 = corner;
    vec3 a1 = vec3_add(corner, vec3(size.x, 0, 0));
    vec3 a2 = vec3_add(corner, vec3(size.x, size.y));
    vec3 a3 = vec3_add(corner, vec3(0, size.y, 0));
    vec3 b0 = vec3_add(a0, vec3(0, 0, size.z));
    vec3 b1 = vec3_add(a1, vec3(0, 0, size.z));
    vec3 b2 = vec3_add(a2, vec3(0, 0, size.z));
    vec3 b3 = vec3_add(a3, vec3(0, 0, size.z));
    game_system_debugtools_add_line(game, dynamic, a0, b0, width, color);
    game_system_debugtools_add_line(game, dynamic, a1, b1, width, color);
    game_system_debugtools_add_line(game, dynamic, a2, b2, width, color);
    game_system_debugtools_add_line(game, dynamic, a3, b3, width, color);
    game_system_debugtools_add_line(game, dynamic, a0, a1, width, color);
    game_system_debugtools_add_line(game, dynamic, a1, a2, width, color);
    game_system_debugtools_add_line(game, dynamic, a2, a3, width, color);
    game_system_debugtools_add_line(game, dynamic, a3, a0, width, color);
    game_system_debugtools_add_line(game, dynamic, b0, b1, width, color);
    game_system_debugtools_add_line(game, dynamic, b1, b2, width, color);
    game_system_debugtools_add_line(game, dynamic, b2, b3, width, color);
    game_system_debugtools_add_line(game, dynamic, b3, b0, width, color);
}


#include <lodepng.h>
void game_debug_raycast_image(struct gameplay_state* game, const char* filename, vec2 size, vec2 near_far)
{
    struct game_entity* ent_ptr =
        game_entity_get(&game->entpool, game->plr_id);
    struct game_component_player* player_ptr = ENT_COMPONENT_PTR(game, ent_ptr, player);
    struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider);
    /*  Allocate a temporary buffer */
    uint8_t* buffer = calloc(sizeof(*buffer), size.x * size.y);
    uint64_t start_time = pg_perf_time();
    vec2 halfsize = vec2_scale(size, 0.5);
    /*  Make a perspective viewer   */
    mat4 view_tx = game->playercontrol.player_pose.head_tx.tx;
    mat4 proj = mat4_perspective(LM_PI_2, (size.x / size.y), near_far.x, near_far.y);
    vec3 view_pos = game->playercontrol.player_pose.head_tx.pos;
    /*  Raycast every pixel of the buffer   */
    int i, j;
    uint8_t* iter = buffer;
    for(j = 0; j < size.y; ++j) {
        printf(".");
        fflush(stdout);
        for(i = 0; i < size.x; ++i, ++iter) {
            /*  Calculate ray direction */
            vec3 screen_pos = vec3((i - halfsize.x) / halfsize.x, (j - halfsize.y) / halfsize.y, 1);
            screen_pos.y *= -1;
            vec4 pos_projected = mat4_mul_vec4(proj, vec4(VEC_XYZ(screen_pos), 1));
            pos_projected = mat4_mul_vec4(view_tx, vec4(VEC_XYZ(pos_projected), 1));
            vec3 ray_dir = vec3_norm(vec3_sub(vec3(VEC_XYZ(pos_projected)), view_pos));
            vec3 ray_end_pos = vec3_add(view_pos, vec3_scale(ray_dir, 32));

            
            /*  Cast ray against static colliders and record the results    */
            game_grid_new_collider_query(game);
            coll_ptr->query_no = game->grid.query_no;
            float ray_len = game_raycast_all_colliders(game, RAY3D(ent_ptr->pos, ray_dir), near_far.y);

            /*  Record the result in the output pixel buffer    */
            if(ray_len < 0) *iter = 0;
            else {
                float x = ((near_far.y - ray_len) / near_far.y) * 255;
                *iter = (uint16_t)x;
            }

            /*  Add a debug line every 32nd pixel   */
            if(i % 16 == 0 && j % 16 == 0) {
                game_system_debugtools_add_line(game, false, view_pos, vec3_add(view_pos, vec3_scale(ray_dir, ray_len)), 0.2, 0xFFFFFF44);
                game_system_debugtools_add_quad(game, false, vec3_add(view_pos, vec3_scale(ray_dir, ray_len)), quat_identity(), vec2(1,1), 0xFFFFFFFF);
            }
        }
    }
    uint64_t end_time = pg_perf_time();
    printf("Evaluated %d rays (%dx%d) in %fms.\n",
        (int)(size.x * size.y), (int)size.x, (int)size.y,
        pg_perf_time_diff(start_time, end_time)*1000.0f);
    /*  Save the image  */
    lodepng_encode_file(filename, (unsigned char*)buffer, size.x, size.y, LCT_GREY, 8);
    free(buffer);
}

