/*  PHYSICS game system     */
    
struct game_system_physics {
    uint32_t collision_no;
    /*  Intermediate data for the broad-phase collision checking
        (not kept between steps)    */
    ARR_T(game_component_collider_id_t) phys_pairs;
    int_arr_t phys_pair_lengths;
};
    
void game_system_physics_init(struct gameplay_state* game);
void game_system_physics_deinit(struct gameplay_state* game);
void game_system_physics_step(struct gameplay_state* game);
void game_system_physics_oneoff(struct gameplay_state* game,
                                struct pg_collider* coll, aabb3D bound);
