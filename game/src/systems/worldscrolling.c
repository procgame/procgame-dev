#include "procgame.h"
#include "game.h"
#include "noise1234.h"

#define TERRAIN_RESOLUTION  32
#define TERRAIN_POINT_SIZE  ((float)WORLD_REGION_SIZE / (float)TERRAIN_RESOLUTION)

game_entity_id_t make_heightmap(struct gameplay_state* game, vec2 world_pos,
        struct game_component_heightmap** hmap_out)
{
    int terrain_buf_range = game_system_render_get_terrain_buffer_range(&game->render);
    struct game_entity* ent_ptr;
    game_entity_id_t ent_id;
    ent_ptr = game_entity_create(game, &ent_id);
    ent_ptr->pos = vec3(VEC_XY(world_pos), 0);
    struct game_component_heightmap* hmap_ptr =
        ENT_COMPONENT_NEW(game, ent_ptr, heightmap, NULL);
    game_component_heightmap_init(hmap_ptr, ivec2_all(TERRAIN_RESOLUTION+3), TERRAIN_POINT_SIZE, terrain_buf_range);
    PG_HEIGHTMAP_FOREACH(hmap_ptr->hmap, x, y, h) {
        vec2 world_pos_h = vec2_add(vec2_scale(vec2(x,y), TERRAIN_POINT_SIZE), world_pos);
        world_pos_h.x -= 1;
        world_pos_h.y -= 1;

        const vec4 terrain_frequency = vec4(0.01,   0.2,    0.4,      0);
        const vec4 terrain_amplitude = vec4(20,     1,      4,      0);

        vec2 p0 = vec2_scale(world_pos_h, terrain_frequency.v[0]);
        vec2 p2 = vec2_scale(world_pos_h, terrain_frequency.v[1]);
        vec2 p3 = vec2_scale(world_pos_h, terrain_frequency.v[2]);
        vec2 p4 = vec2_scale(world_pos_h, terrain_frequency.v[3]);
        float d0 = ((perlin2(VEC_XY(p0)) + 1) * 0.5) * terrain_amplitude.v[0];
        float d1 = ((perlin2(VEC_XY(p2)) + 1) * 0.5) * terrain_amplitude.v[1];
        float d2 = ((perlin2(VEC_XY(p3)) + 1) * 0.5) * terrain_amplitude.v[2];
        float d3 = ((perlin2(VEC_XY(p4)) + 1) * 0.5) * terrain_amplitude.v[3];

        float height = (d0 + d1) - 10;
        height = LM_MAX(height, 0);
        height *= 5;
        height += d2;

        *h = height;
    }

    struct game_component_collider* coll_ptr = ENT_COMPONENT_NEW(game, ent_ptr, collider, NULL);
    game_component_collider_init(coll_ptr, &PG_COLLIDER_HEIGHTMAP(hmap_ptr->hmap,
        .flags = PG_COLLIDER_STATIC));
    ent_ptr->pos = vec3(world_pos.x - TERRAIN_POINT_SIZE, world_pos.y - TERRAIN_POINT_SIZE, 0);
    coll_ptr->phys.pos = vec3(world_pos.x - 0 - TERRAIN_POINT_SIZE, world_pos.y - 0 - TERRAIN_POINT_SIZE, 0);
    game_component_collider_update_bound(coll_ptr);
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_NEW(game, ent_ptr, drawable, NULL);
    draw_ptr->draw_fn = game_draw_heightmap;

    ARR_PUSH(game->static_colliders, ENT_COMPONENT_ID(ent_ptr, collider));
    if(hmap_out) *hmap_out = hmap_ptr;
    return ent_id;
}

void game_system_worldscrolling_init(struct gameplay_state* game, vec2 start_pos)
{
    struct game_system_worldscrolling* scrolling = &game->scrolling;
    SARR_INIT(scrolling->world_chunks);
    SARR_INIT(scrolling->want_world_chunks);
    ivec2 region_idx = ivec2(VEC_XY(vec2_floor(vec2_tdiv(start_pos, WORLD_REGION_SIZE))));
    scrolling->region_idx = region_idx;
    int i, j;
    for(i = 0; i < 3; ++i) for(j = 0; j < 3; ++j) {
        ivec2 hmap_idx = ivec2_add(region_idx, ivec2(i-1,j-1));
        ARR_PUSH(scrolling->want_world_chunks, hmap_idx);
    }
    //ARR_PUSH(scrolling->want_world_chunks, ivec2(0,0));
    game->grid.cell_idx = ivec3_all(0xFFFFFFFF);
    game_grid_set_offset(game, vec3(VEC_XY(start_pos)));
}

void game_system_worldscrolling_deinit(struct gameplay_state* game)
{
    
}

void game_system_worldscrolling_scroller(struct gameplay_state* game, game_entity_id_t ent_id)
{
    game->scrolling.scroller = ent_id;
}

void game_system_worldscrolling_step(struct gameplay_state* game)
{
    struct game_system_worldscrolling* scrolling = &game->scrolling;
    struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, scrolling->scroller);
    if(!ent_ptr) return;
    /*  The position of the entity the world scrolls around */
    vec2 center = vec2(VEC_XY(ent_ptr->pos));
    /*  The corner of the chunk containing that position    */
    vec2 region_pos = vec2_sub(center, vec2_vmod(center, WORLD_REGION_SIZE));
    /*  The world coordinates of that chunk (in chunk-wise coordinates) */
    ivec2 region_idx = ivec2(VEC_XY(vec2_floor(vec2_tdiv(center, WORLD_REGION_SIZE))));

    /*  If the current chunk index is different from before then we have moved  */
    if(true && !ivec2_cmp_eq(region_idx, scrolling->region_idx)) {
        scrolling->region_idx = region_idx;

        /*  Unload distant chunks   */
        int i, j;
        struct game_world_chunk* chunk;
        ARR_FOREACH_PTR_REV(scrolling->world_chunks, chunk, i) {
            int chunk_distance = ivec2_distmax(chunk->region_idx, region_idx);
            if(chunk_distance > 1) {
                struct game_entity* chunk_entity = GAME_ENTITY_PTR(game, chunk->heightmap);
                struct game_component_heightmap* hmap = ENT_COMPONENT_PTR(game, chunk_entity, heightmap);
                game_system_render_release_terrain_buffer_range(&game->render, hmap->terrain_chunk_idx);
                game_entity_destroy(game, chunk_entity);
                ARR_SWAPSPLICE(scrolling->world_chunks, i, 1);
                continue;
            }
        }
        
        /*  Delete all entities in distant regions  */
        struct game_entity* ent_ptr;
        game_entity_id_t ent_id;
        ARR_FOREACH_REV(game->all_entities, ent_id, i) {
            if(!(ent_ptr = GAME_ENTITY_PTR(game, ent_id))) {
                ARR_SWAPSPLICE(game->all_entities, i, 1);
                continue;
            }
            vec2 dist = vec2_sub(vec2(VEC_XY(ent_ptr->pos)), region_pos);
            if(dist.x < (-WORLD_REGION_SIZE*2) || dist.x > (2*WORLD_REGION_SIZE)
            || dist.y < (-WORLD_REGION_SIZE*2) || dist.y > (2*WORLD_REGION_SIZE)) {
                game_entity_destroy(game, ent_ptr);
                ARR_SWAPSPLICE(game->all_entities, i, 1);
                continue;
            }
        }

        /*  Add surrounding chunks to the wanted list   */
        for(i = -1; i <= 1; ++i) for(j = -1; j <= 1; ++j) {
            ivec2 want_chunk = ivec2_add(region_idx, ivec2(i, j));
            ARR_PUSH(scrolling->want_world_chunks, want_chunk);
        }
    }

    /*  Load the chunks that we want and aren't already loaded  */
    bool loaded_new_chunks = false;
    int i;
    ivec2 wanted_chunk;
    ARR_FOREACH_REV(scrolling->want_world_chunks, wanted_chunk, i) {
        /*  Check if the chunk is already loaded    */
        bool already_have_chunk = false;
        int j;
        struct game_world_chunk* loaded_chunk;
        ARR_FOREACH_PTR(scrolling->world_chunks, loaded_chunk, j) {
            if(ivec2_cmp_eq(loaded_chunk->region_idx, wanted_chunk)) {
                already_have_chunk = true;
                break;
            }
        }
        if(already_have_chunk) {
            ARR_SWAPSPLICE(scrolling->want_world_chunks, i, 1);
            continue;
        }
        vec2 hmap_pos = vec2_mul(vec2(VEC_XY(wanted_chunk)), vec2_all(WORLD_REGION_SIZE));
        /*  Make a new heightmap entity and add it to the scrolling grid    */
        struct game_component_heightmap* hmap_component;
        game_entity_id_t terrain_id = make_heightmap(game, hmap_pos, &hmap_component);
        /*  Generate terrain mesh for the render system */
        struct pg_vertex_buffer_builder terrain_builder;
        game_system_render_start_terrain_builder(game, hmap_component->terrain_chunk_idx, &terrain_builder);
        game_terrain_build_mesh(&hmap_component->hmap, &terrain_builder);
        game_system_render_finish_terrain_builder(game, hmap_component->terrain_chunk_idx, &terrain_builder);
        /*  Add the new chunk to the list of loaded chunks, remove it from the list of wanted ones  */
        loaded_new_chunks = true;
        ARR_PUSH(scrolling->world_chunks, (struct game_world_chunk){
            .heightmap = terrain_id,
            .region_idx = wanted_chunk,
        });
        ARR_SWAPSPLICE(scrolling->want_world_chunks, i, 1);
    }
    game_grid_set_offset(game, vec3(VEC_XY(center)));

    if(loaded_new_chunks) {
        game_grid_rebuild_static_colliders(game);
    }

}
