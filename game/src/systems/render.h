#define GAME_RENDER_MAX_CHUNKS  12
struct game_system_render {
    /*  Assets  */
    pg_gpu_texture_t* ground_tex;
    quat y_axis;
    /*  Rendering data  */
    float fov;
    float aspect_ratio;
    vec2 near_far;
    quat billboard, billboard_vertical;
    struct pg_viewer view_3d;
    mat4 eye_projview_left, eye_projview_right;

    /*  VR eye projection matrices  */

    /*  Input buffers for the rendering */
    struct pg_quadbatch* world_quads;

    /*  Buffer to contain the terrain meshes    */
    pg_gpu_buffer_t* terrain_vert_buf_gpu;
    pg_gpu_buffer_t* terrain_idx_buf_gpu;
    /*  Ranges in the two buffers allotted to the grid of terrain chunks    */
    struct terrain_buffer_range {
        ivec2 vert_range;
        ivec2 vert_range_built;
        ivec2 idx_range;
        ivec2 idx_range_built;
        bool in_use;
    } terrain_buf_ranges[GAME_RENDER_MAX_CHUNKS];
    SARR_T(GAME_RENDER_MAX_CHUNKS, int) available_terrain_buf_ranges;

    ARR_T(struct loaded_model {
        char name[128];
        pg_asset_handle_t assembly_asset;
        pg_asset_handle_t resources_asset;
        int model_range_idx;
    }) loaded_models;

    /*  Render targets  */
    pg_gpu_texture_t* screen_tex;
    pg_gpu_framebuffer_t* screen;
    pg_gpu_framebuffer_t* gbuffer;
    pg_gpu_framebuffer_t* lighting_buffer;
    pg_gpu_framebuffer_t* framebuffer;

    pg_gpu_framebuffer_t* gbuffer_left;
    pg_gpu_framebuffer_t* gbuffer_right;
    pg_gpu_framebuffer_t* lighting_buffer_left;
    pg_gpu_framebuffer_t* lighting_buffer_right;
    pg_gpu_framebuffer_t* framebuffer_left;
    pg_gpu_framebuffer_t* framebuffer_right;
    /*  Renderer    */
    pg_gpu_renderer_t* renderer;
    pg_gpu_render_stage_t* terrain_pass;
    pg_gpu_render_stage_t* models_pass;
    pg_gpu_render_stage_t* light_volumes_pass;
    pg_gpu_render_stage_t* lighting_post_pass;
    pg_gpu_render_stage_t* fog_pass;
    pg_gpu_render_stage_t* debug_quads_pass;
    pg_gpu_render_stage_t* copy_pass;

    /*  Misc    */
    double framerate;
    pg_ui_t fps_overlay;
};

void game_system_render_init(struct gameplay_state* game);
void game_system_render_deinit(struct gameplay_state* game);
void game_system_render_step(struct gameplay_state* game);

int game_system_render_get_terrain_buffer_range(struct game_system_render* render);
void game_system_render_release_terrain_buffer_range(struct game_system_render* render, int range);
void game_system_render_start_terrain_builder(struct gameplay_state* game, int range,
        struct pg_vertex_buffer_builder* builder);
void game_system_render_finish_terrain_builder(struct gameplay_state* game, int range,
        struct pg_vertex_buffer_builder* builder);
