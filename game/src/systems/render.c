#include "procgame.h"
#include "game.h"

static bool load_models(struct gameplay_state* game)
{
    struct game_system_render* render = &game->render;
    pg_asset_manager_t* asset_mgr = game->asset_mgr;

    cJSON* models_list_json = pg_asset_get_data_by_name(asset_mgr, "GameModelsList");
    if(!models_list_json) {
        pg_log(PG_LOG_ERROR, "Did not find GameModelsList asset");
        return false;
    }

    cJSON* model_source_files = cJSON_GetObjectItem(models_list_json, "source_files");
    if(!model_source_files) {
        pg_log(PG_LOG_ERROR, "Did not find GameModelsList asset with source_files member");
        return false;
    }

    cJSON* model_source_file;
    cJSON_ArrayForEach(model_source_file, model_source_files) {
        const char* model_filename = cJSON_GetStringValue(model_source_file);
        if(!model_filename) {
            pg_log(PG_LOG_ERROR, "Invalid model filename");
            return false;
        }
        pg_asset_handle_t model_assets[8] = {};
        int n_assets = pg_asset_multiple_from_file(asset_mgr, NULL, model_filename, model_assets, 8);
        if(n_assets == -1) {
            pg_log(PG_LOG_ERROR, "Failed to load model from '%s'", model_filename);
            return false;
        }
        int model_name_len = 0;
        char model_name[128] = {};

        int verts_asset = -1, idx_asset = -1, tex_asset = -1;
        pg_log(PG_LOG_DEBUG, "Model includes %d assets", n_assets);
        for(int i = 0; i < n_assets; ++i) {
            const char* asset_name = pg_asset_get_name(model_assets[i]);
            pg_log(PG_LOG_DEBUG, "Model includes asset '%s'", asset_name);
            int name_len = strlen(asset_name);
            int subname_len = 0;
            if((subname_len = pg_str_find(asset_name, name_len, ".texture", 8)) > 0) tex_asset = i;
            else if((subname_len = pg_str_find(asset_name, name_len, ".vertex", 7)) > 0) verts_asset = i;
            else if((subname_len = pg_str_find(asset_name, name_len, ".index", 6)) > 0) idx_asset = i;
            else break;
            if(!model_name[0]) {
                model_name_len = subname_len;
                strncpy(model_name, asset_name, subname_len);
            } else if(subname_len != model_name_len
                || strncmp(model_name, asset_name, model_name_len) != 0) {
                pg_log(PG_LOG_ERROR, "Mismatching model name in assets in model file '%s'", model_filename);
                return false;
            }
        }
        if(verts_asset == -1 || idx_asset == -1 || tex_asset == -1) {
            pg_log(PG_LOG_ERROR, "Failed to load model from '%s'", model_filename);
            return false;
        }

        pg_gpu_vertex_assembly_t* model_assembly = pg_gpu_vertex_assembly_create();
        pg_gpu_resource_set_t* model_resources = pg_gpu_resource_set_create();
        pg_gpu_buffer_t* model_verts = pg_asset_get_data(model_assets[verts_asset]);
        pg_gpu_buffer_t* model_idx = pg_asset_get_data(model_assets[idx_asset]);
        pg_gpu_texture_t* model_tex = pg_asset_get_data(model_assets[tex_asset]);
        pg_gpu_vertex_assembly_set_binding(model_assembly, 0, model_verts, false);
        pg_gpu_vertex_assembly_set_index_buffer(model_assembly, model_idx);
        pg_gpu_vertex_assembly_attribute(model_assembly, 0, 0, "v_position");
        pg_gpu_vertex_assembly_attribute(model_assembly, 0, 1, "v_normal");
        pg_gpu_vertex_assembly_attribute(model_assembly, 0, 2, "v_tangent");
        pg_gpu_vertex_assembly_attribute(model_assembly, 0, 3, "v_bitangent");
        pg_gpu_vertex_assembly_attribute(model_assembly, 0, 4, "v_tex_coord");
        pg_gpu_resource_set_attach_texture(model_resources, 0, model_tex);
        pg_gpu_resource_set_attach_texture_params(model_resources, 0, &PG_GPU_TEXTURE_PARAMS(.filter_mag = PG_GPU_TEXTURE_PARAM_FILTER_LINEAR));
        int range_idx = pg_gpu_render_stage_add_input_group(render->models_pass, PG_TARGET_FILTER_ALL, model_name, model_assembly, model_resources);

        /*  Add the loaded model's assembly and resources to the asset manager  */
        pg_asset_handle_t model_assembly_asset = pg_asset_from_data(asset_mgr, "pg_gpu_vertex_assembly", NULL, model_assembly, true);
        pg_asset_handle_t model_resources_asset = pg_asset_from_data(asset_mgr, "pg_gpu_resource_set", NULL, model_resources, true);
        pg_asset_depends(model_assembly_asset, model_assets[verts_asset]);
        pg_asset_depends(model_assembly_asset, model_assets[idx_asset]);
        pg_asset_depends(model_resources_asset, model_assets[tex_asset]);

        /*  Keep track of the loaded model separately too   */
        struct loaded_model* loaded_model = ARR_NEW(render->loaded_models);
        strncpy(loaded_model->name, model_name, 128);
        loaded_model->assembly_asset = model_assembly_asset;
        loaded_model->resources_asset = model_resources_asset;
        loaded_model->model_range_idx = range_idx;
    }

    return true;
}


void game_system_render_init(struct gameplay_state* game)
{
    struct game_system_render* render = &game->render;
    ARR_INIT(render->loaded_models);
    render->fov = LM_DEG_TO_RAD(70);
    render->fps_overlay = pg_ui_get_child(game->ui, pg_ui_context_root(game->ui), "foo");
    ivec2 win_sz = pg_window_size();

    render->near_far = vec2(0.1,500);
    render->y_axis = quat_rotation(vec3_X(), LM_PI_2);
    render->aspect_ratio = (float)win_sz.x / (float)win_sz.y;

    render->screen_tex = pg_asset_get_data_by_name(game->asset_mgr, "GameRenderColorTexture");
    render->screen = pg_asset_get_data_by_name(game->asset_mgr, "__pg_render_output");
    render->gbuffer = pg_asset_get_data_by_name(game->asset_mgr, "GameGBufferOutput");
    render->lighting_buffer = pg_asset_get_data_by_name(game->asset_mgr, "GameLightVolumeOutput");
    render->framebuffer = pg_asset_get_data_by_name(game->asset_mgr, "GameRenderOutput");

    if(game->vr_enabled) {
        render->gbuffer_left = pg_asset_get_data_by_name(game->asset_mgr, "GameGBufferOutput_VR.stereo_left");
        render->gbuffer_right = pg_asset_get_data_by_name(game->asset_mgr, "GameGBufferOutput_VR.stereo_right");
        render->lighting_buffer_left = pg_asset_get_data_by_name(game->asset_mgr, "GameLightVolumeOutput_VR.stereo_left");
        render->lighting_buffer_right = pg_asset_get_data_by_name(game->asset_mgr, "GameLightVolumeOutput_VR.stereo_right");
        render->framebuffer_left = pg_asset_get_data_by_name(game->asset_mgr, "GameRenderOutput_VR.stereo_left");
        render->framebuffer_right = pg_asset_get_data_by_name(game->asset_mgr, "GameRenderOutput_VR.stereo_right");
    }

    /****************************/
    /*  CREATE RENDER PASSES    */
    game_terrain_init_buffers(game);

    /*  Flat-screen renderer    */
    render->terrain_pass = pg_asset_get_data_by_name(game->asset_mgr, "GameTerrainRenderStage");
    render->models_pass = pg_asset_get_data_by_name(game->asset_mgr, "GameModelsRenderStage");
    render->fog_pass = pg_asset_get_data_by_name(game->asset_mgr, "GameFogRenderStage");
    render->debug_quads_pass = pg_asset_get_data_by_name(game->asset_mgr, "DebugQuads.render_stage");
    render->copy_pass = pg_asset_get_data_by_name(game->asset_mgr, "CopyGameRenderToDisplay");
    render->renderer = pg_asset_get_data_by_name(game->asset_mgr, "GameRenderer");

    /*  Load models */
    load_models(game);
}

void game_system_render_deinit(struct gameplay_state* game)
{
}


void game_system_render_start_terrain_builder(struct gameplay_state* game, int range,
        struct pg_vertex_buffer_builder* builder)
{
    //pg_log(PG_LOG_INFO, "Starting buffer %d", range);
    ivec2 vert_range = game->render.terrain_buf_ranges[range].vert_range;
    ivec2 idx_range = game->render.terrain_buf_ranges[range].idx_range;
    pg_vertex_buffer_builder_init(builder,
            pg_gpu_buffer_get_local_data(game->render.terrain_vert_buf_gpu), vert_range,
            pg_gpu_buffer_get_local_data(game->render.terrain_idx_buf_gpu), idx_range);
}

void game_system_render_finish_terrain_builder(struct gameplay_state* game, int range,
        struct pg_vertex_buffer_builder* builder)
{
    ivec2 vert_range = pg_buffer_builder_get_built_range(&builder->verts_builder);
    ivec2 idx_range = pg_buffer_builder_get_built_range(&builder->idx_builder);
    //pg_log(PG_LOG_INFO, "Finishing buffer %d, vert range (%d, %d) idx range (%d, %d)", range, VEC_XY(vert_range), VEC_XY(idx_range));
    game->render.terrain_buf_ranges[range].vert_range_built = vert_range;
    game->render.terrain_buf_ranges[range].idx_range_built = idx_range;
    pg_gpu_buffer_reupload_range(game->render.terrain_vert_buf_gpu, vert_range.x, vert_range.y);
    pg_gpu_buffer_reupload_range(game->render.terrain_idx_buf_gpu, idx_range.x, idx_range.y);
}

int game_system_render_get_terrain_buffer_range(struct game_system_render* render)
{
    if(!render->available_terrain_buf_ranges.len) {
        pg_log(PG_LOG_ERROR, "No terrain buffer ranges available!");
        return 0;
    }
    int new_range = ARR_POP(render->available_terrain_buf_ranges);
    //pg_log(PG_LOG_INFO, "Allocating terrain buffer range %d", new_range);
    render->terrain_buf_ranges[new_range].in_use = true;
    return new_range;
}

void game_system_render_release_terrain_buffer_range(struct game_system_render* render, int range)
{
    render->terrain_buf_ranges[range].in_use = false;
    //pg_log(PG_LOG_INFO, "Allocating terrain buffer range %d", range);
    ARR_PUSH(render->available_terrain_buf_ranges, range);
}

static void render_set_uniforms(struct gameplay_state* game)
{
    struct game_system_render* render = &game->render;
    pg_data_t near_far_data = pg_data_init(PG_VEC2, 1, &render->near_far);
    pg_gpu_render_stage_set_initial_uniform(render->fog_pass, 2, &near_far_data);

    /*  Set the eye uniforms for the render stages  */
    int models_projview_loc = pg_gpu_render_stage_get_uniform_location(render->models_pass, "projview_matrix");
    int terrain_projview_loc = pg_gpu_render_stage_get_uniform_location(render->terrain_pass, "projview_matrix");
    int debug_projview_loc = pg_gpu_render_stage_get_uniform_location(render->debug_quads_pass, "ProjViewMatrix");

    if(game->vr_enabled) {
        pg_data_t projview_data_left = pg_data_init(PG_MAT4, 1, &render->eye_projview_left);
        pg_data_t projview_data_right = pg_data_init(PG_MAT4, 1, &render->eye_projview_right);
        pg_gpu_render_stage_set_targets_uniform(render->terrain_pass, PG_TARGET_FILTER_VR_LEFT, terrain_projview_loc, &projview_data_left);
        pg_gpu_render_stage_set_targets_uniform(render->terrain_pass, PG_TARGET_FILTER_VR_RIGHT, terrain_projview_loc, &projview_data_right);
        pg_gpu_render_stage_set_targets_uniform(render->debug_quads_pass, PG_TARGET_FILTER_VR_LEFT, debug_projview_loc, &projview_data_left);
        pg_gpu_render_stage_set_targets_uniform(render->debug_quads_pass, PG_TARGET_FILTER_VR_RIGHT, debug_projview_loc, &projview_data_right);
        pg_gpu_render_stage_set_targets_uniform(render->models_pass, PG_TARGET_FILTER_VR_LEFT, models_projview_loc, &projview_data_left);
        pg_gpu_render_stage_set_targets_uniform(render->models_pass, PG_TARGET_FILTER_VR_RIGHT, models_projview_loc, &projview_data_right);
        pg_ui_context_set_view_VR(game->ui, render->eye_projview_left, render->eye_projview_right);
    } else {
        pg_data_t projview_data = pg_data_init(PG_MAT4, 1, &render->view_3d.projview);
        pg_gpu_render_stage_set_initial_uniform(render->terrain_pass, terrain_projview_loc, &projview_data);
        pg_gpu_render_stage_set_initial_uniform(render->debug_quads_pass, debug_projview_loc, &projview_data);
        pg_gpu_render_stage_set_initial_uniform(render->models_pass, models_projview_loc, &projview_data);
        pg_ui_context_set_view_3D(game->ui, render->view_3d.projview);
    }
}


void game_system_render_step(struct gameplay_state* game)
{
    struct game_system_render* render = &game->render;
    pg_frame_timestamp();
    render->framerate = round(pg_framerate());

    /*  Calculate framerate and deltatime   */
    double time = pg_time();
    double dt = pg_timestepper_deltatime(&game->timer, time);


    /*  Update the FPS overlay  */
    char fps_string[64];
    snprintf(fps_string, 64, "FPS: %d", (int)round(render->framerate));

    struct game_entity* player_ent = GAME_ENTITY_PTR(game, game->plr_id);
    vec3 player_pos = player_ent->pos;
    snprintf(fps_string, 64, "%f\n%f\n%f", VEC_XYZ(player_pos));


    pg_ui_set_text(game->ui, render->fps_overlay, fps_string, 64);

    /*  Reset all the render data   */
    pg_gpu_render_stage_clear_commands(render->terrain_pass);
    pg_gpu_render_stage_clear_commands(render->models_pass);
    pg_gpu_framebuffer_clear(render->framebuffer, vec4(0,0,0,0));
    pg_gpu_framebuffer_clear(render->gbuffer, vec4(0,0,0,0));
    pg_gpu_framebuffer_clear(render->lighting_buffer, vec4(0,0,0,0));
    pg_gpu_framebuffer_clear(render->screen, vec4(0,0,0,1));
    if(game->vr_enabled) {
        pg_gpu_framebuffer_clear(render->gbuffer_left, vec4(0,0,0,0));
        pg_gpu_framebuffer_clear(render->gbuffer_right, vec4(0,0,0,0));
        pg_gpu_framebuffer_clear(render->lighting_buffer_left, vec4(0,0,0,0));
        pg_gpu_framebuffer_clear(render->lighting_buffer_right, vec4(0,0,0,0));
        pg_gpu_framebuffer_clear(render->framebuffer_left, vec4(0,0,0,0));
        pg_gpu_framebuffer_clear(render->framebuffer_right, vec4(0,0,0,0));
    }

    /*  Iterate through all drawable components and do the thing.   */
    int i;
    struct game_entity* ent_ptr;
    game_component_drawable_id_t draw_id;
    struct game_component_drawable* draw_ptr;
    ARR_FOREACH_REV(game->drawable_components, draw_id, i) {
        draw_ptr = GAME_COMPONENT_PTR(game, drawable, draw_id);
        if(!draw_ptr) {
            ARR_SWAPSPLICE(game->drawable_components, i, 1);
            continue;
        }
        if(!draw_ptr->draw_fn || !draw_ptr->enabled) {
            pg_log(PG_LOG_INFO, "Drawable component lacks draw function for drawable id %zx", draw_id);
            continue;
        }
        /*  Call per-entity draw functions  */
        ent_ptr = GAME_ENTITY_PTR(game, draw_ptr->ent_id);
        draw_ptr->draw_fn(game, ent_ptr, dt);
    }

    /*  Step the UI */
    pg_ui_context_animate(game->ui, (float)game->tick + dt);
    pg_ui_context_draw(game->ui);

    /*  Get the player-controlled perspective   */
    game_system_playercontrol_perspective(game, dt);
    game_system_debugtools_draw(game);

    /*  Render!     */
    if(game->vr_enabled) {
        render_set_uniforms(game);
        pg_gpu_renderer_execute(render->renderer, PG_TARGET_FILTER_VR_BOTH);
        pg_vr_display_submit();
    } else {
        render_set_uniforms(game);
        pg_gpu_renderer_execute(render->renderer, PG_TARGET_FILTER_MAIN);
        pg_window_swap();
    }
}
