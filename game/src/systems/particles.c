#include <procgame.h>
#include "game.h"
#include "ksort.h"

PG_MEMPOOL_DEFINE(struct game_particle, game_particle, id);

void game_system_particles_init(struct gameplay_state* game)
{
    struct game_system_particles* particles = &game->particles;
    game_particle_pool_init(&particles->particle_pool);
    particles->light_color = vec3(0.1,0.1,0.1);
    ARR_INIT(particles->all_particles);
    //particles->quads = pg_asset_get_data_by_name(game->asset_mgr, "GameParticleQuads");
    //particles->renderpass = pg_asset_get_data_by_name(game->asset_mgr, "GameParticleQuads.render_stage");
}

/*  Depth sorting   */
static inline int particle_cmp(game_particle_id_t a, game_particle_id_t b, struct game_system_particles* p)
{
    struct game_particle* a_ = &p->particle_pool.pool.data[a & 0xFFFFFFFF];
    struct game_particle* b_ = &p->particle_pool.pool.data[b & 0xFFFFFFFF];
    float d_a = vec3_len2(vec3_sub(a_->pos, p->viewer_pos));
    float d_b = vec3_len2(vec3_sub(b_->pos, p->viewer_pos));
    return (d_a > d_b);
}
KSORT_S_DEF(particles_id, game_particle_id_t, struct game_system_particles*, particle_cmp);

void game_system_particles_step(struct gameplay_state* game)
{
    struct game_system_particles* particles = &game->particles;
    /*  Update viewer info  */
    particles->viewer_pos = game->render.view_3d.view_tx.pos;
    particles->viewer_dir = quat_mul_vec3(game->render.view_3d.view_tx.rot, vec3(0,0,1));
    particles->fade_start = game->render.near_far.x;
    particles->fade_dist = game->render.near_far.y;
    particles->billboard = game->render.billboard;
    /*  Iterate over all particle emitters and update them  */
    int i;
    game_component_particle_emitter_id_t emit_id;
    struct game_component_particle_emitter* emit_ptr;
    ARR_FOREACH_REV(game->particle_emitter_components, emit_id, i) {
        if(!(emit_ptr = GAME_COMPONENT_PTR(game, particle_emitter, emit_id))) {
            ARR_SWAPSPLICE(game->particle_emitter_components, i, 1);
            continue;
        }
        if(!emit_ptr->enabled) continue;
        game_component_particle_emitter_step(game, emit_ptr);
    }
    /*  Iterate over all particles and update them  */
    struct game_particle* part_ptr;
    game_particle_id_t part_id;
    ARR_FOREACH_REV(particles->all_particles, part_id, i) {
        if(!(part_ptr = game_particle_get(&particles->particle_pool, part_id))) {
            ARR_SWAPSPLICE(particles->all_particles, i, 1);
            continue;
        }
        const struct game_particle_prototype* base = &game->res->protos.particles.data[part_ptr->proto_id];
        if(game->tick - part_ptr->created_tick >= base->lifetime) {
            ARR_SWAPSPLICE(particles->all_particles, i, 1);
            game_particle_free(&particles->particle_pool, part_id);
            continue;
        }
        part_ptr->pos = vec3_add(part_ptr->pos, part_ptr->vel);
    }
    /*  Sort the list of particles by depth for correct alpha blending.
        Only perform a few bubblesort passes over the list so the list will
        be gradually corrected over several frames, instead of trying to fully
        sort the entire list every frame. Higher depth sort quality means
        better particle blending but worse performance. For unlimited depth
        quality (depth_sort_quality=0), use introsort instead of bubblesort.    */
    //vec3 plane[2] = { particles->viewer_pos, particles->viewer_dir };
    if(particles->depth_sort_quality) {
        ks_bubblesort_ltd_s_particles_id(particles->all_particles.len,
            particles->all_particles.data, particles->depth_sort_quality, particles);
    } else {
        ks_introsort_s_particles_id(particles->all_particles.len,
            particles->all_particles.data, particles);
    }
}

/*  This could be a lot better  */
static inline void draw_particle(struct gameplay_state* game, game_particle_id_t part_id, float dt)
{
    struct game_system_particles* particles = &game->particles;
    struct game_particle* part = &particles->particle_pool.pool.data[part_id & 0xFFFFFFFF];
    const struct game_particle_prototype* base = &game->res->protos.particles.data[part->proto_id];
    vec2 scale;
    vec4 tex_frame, color_mul, color_add;
    int tex_layer;
    float phase_time = ((float)(game->tick - part->created_tick) + dt) / base->lifetime * base->phases.len;
    phase_time = LM_CLAMP(phase_time, 0, base->phases.len - FLT_EPSILON);
    if(base->phases.len == 1) {
        scale = base->phases.data[0].scale;
        tex_frame = base->phases.data[0].image_frame.v;
        tex_layer = base->phases.data[0].image_frame.layer;
        color_mul = base->phases.data[0].color_mul;
        color_add = base->phases.data[0].color_add;
    } else {
        const struct game_particle_phase* phase = &base->phases.data[(int)phase_time];
        const struct game_particle_phase* next_phase = &base->phases.data[(int)phase_time + 1];
        float phase_interp = phase_time - truncf(phase_time);
        scale = vec2_lerp(phase->scale, next_phase->scale, phase_interp);
        tex_frame = phase->image_frame.v;
        tex_layer = phase->image_frame.layer;
        color_mul = vec4_lerp(phase->color_mul, next_phase->color_mul, phase_interp);
        color_mul = vec4_mul(color_mul, vec4(VEC_XYZ(particles->light_color),1));
        color_add = vec4_lerp(phase->color_add, next_phase->color_add, phase_interp);
    }
    float view_dist = vec3_dist(part->pos, particles->viewer_pos);
    float dist_fade = (view_dist - particles->fade_start) / particles->fade_dist;
    dist_fade = 1 - LM_CLAMP(dist_fade, 0, 1);
    color_mul.w *= dist_fade;
    vec3 pos = vec3_add(part->pos, vec3_scale(part->vel, dt));
    pg_quadbatch_add_quad(particles->quads, &PG_EZQUAD(
        .pos = pos, .scale = scale, .orientation = particles->billboard,
        .uv_v = tex_frame, .tex_layer = tex_layer,
        .color_mul = VEC4_TO_UINT(color_mul),
        .color_add = VEC4_TO_UINT(color_add), ));
}

void game_system_particles_draw(struct gameplay_state* game, float dt)
{
    struct game_system_particles* particles = &game->particles;
    pg_gpu_command_arr_t* commands = pg_gpu_render_stage_get_group_commands(particles->renderpass, 0);
    ARR_TRUNCATE(*commands, 0);
    pg_gpu_record_commands(commands);
    pg_quadbatch_reset(particles->quads);
    pg_quadbatch_next(particles->quads, &game->render.view_3d.projview);
    int i;
    game_particle_id_t part_id;
    ARR_FOREACH(particles->all_particles, part_id, i) {
        draw_particle(game, part_id, dt);
    }
    /*  Add the quadbatch to the renderpass, and upload the quadbatch data  */
    pg_gpu_cmd_draw_quadbatch(particles->quads);
    pg_quadbatch_upload(particles->quads);
}

/*  Create a new particle. If the system is at max particle-count, then
    randomly either fail to create a particle, or replace an existing particle. */
struct game_particle* game_system_particles_new(struct gameplay_state* game,
        vec3 pos, vec3 vel, game_particle_prototype_id_t proto_id)
{
    if(game->particles.all_particles.len >= game->particles.max_particles) {
        if(rand() % 2) return NULL;
        game_particle_id_t part_id = game->particles.all_particles.data[rand() % game->particles.max_particles];
        return game_particle_get(&game->particles.particle_pool, part_id);
    }
    struct game_particle* new_part_ptr;
    game_particle_id_t new_part_id = game_particle_alloc(&game->particles.particle_pool, &new_part_ptr);
    new_part_ptr->pos = pos,
    new_part_ptr->vel = vel,
    new_part_ptr->proto_id = proto_id,
    new_part_ptr->created_tick = game->tick,
    ARR_PUSH(game->particles.all_particles, new_part_id);
    return new_part_ptr;
}




