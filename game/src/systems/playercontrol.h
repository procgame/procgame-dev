enum game_system_playercontrol_inputs {
    GAME_CONTROL_MOVE_FORWARD,
    GAME_CONTROL_MOVE_BACK,
    GAME_CONTROL_MOVE_LEFT,
    GAME_CONTROL_MOVE_RIGHT,
    GAME_CONTROL_JUMP,
    GAME_CONTROL_THROW,
    GAME_CONTROL_USE_HOLD,
    GAME_CONTROL_USE_HIT,
    GAME_CONTROL_NEXT_ITEM,
    GAME_CONTROL_PREV_ITEM,
    GAME_CONTROL_INTERACT,
    GAME_CONTROL_DEBUG,
    GAME_CONTROL_DEBUG2,
    GAME_CONTROL_DEBUG3,
    GAME_CONTROL_DEBUG4,
    GAME_CONTROL_DEBUG5,
    GAME_CONTROL_DEBUG6,
    GAME_CONTROL_DEBUG7,
    GAME_NUM_CONTROLS,
};

struct game_fake_vr_state {
    vec3 hand_pos[2];
    vec3 hand_dir[2];
    vec3 hmd_pos;
    vec3 hmd_dir;
};

struct game_player_pose {
    struct pg_gfx_transform origin;
    struct pg_gfx_transform head_tx;
    struct pg_gfx_transform foot_tx;
    struct pg_gfx_transform hand_tx[2];
};

struct game_vr_pose {
    struct pg_gfx_transform hmd_tx;
    struct pg_gfx_transform hand_tx[2];
};

struct game_system_playercontrol {
    game_entity_id_t controlled_ent_id;
    struct pg_gfx_transform vr_global;

    /****************/
    /*  INPUT       */
    /*  Kb/mouse/gpad controls  */
    struct pg_input_wrapper controls;
    vec2 mouse_look_angle;
    /*  VR tracked controls */
    struct game_vr_pose vr_pose;

    /****************************/
    /*  CONTROLLED STATE        */
    struct game_player_pose player_pose;
    ray3D cursor_ray;

    /************/
    /*  Debug   */
    struct game_fake_vr_state fake_vr_state;
};

void game_system_playercontrol_init(struct gameplay_state* game);
void game_system_playercontrol_deinit(struct gameplay_state* game);
void game_system_playercontrol_set(struct gameplay_state* game, game_entity_id_t ent_id);
void game_system_playercontrol_poll(struct gameplay_state* game);
void game_system_playercontrol_step(struct gameplay_state* game);
void game_system_playercontrol_perspective(struct gameplay_state* game, float dt);
