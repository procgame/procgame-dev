#include "procgame.h"
#include "game.h"

static void move_entities(struct gameplay_state* game);
static void velocity_raycast(struct gameplay_state* game, struct game_entity* ent_ptr);
static void clamp_to_surface(struct gameplay_state* game, struct game_component_collider* coll_ptr);
static void get_query_bounds(struct game_grid* grid, vec3 b0, vec3 b1, ivec3* b0_out, ivec3* b1_out);
static int physics_query(struct gameplay_state* game, aabb3D bound);
static void resolve_collisions(struct gameplay_state* game);

void game_system_physics_init(struct gameplay_state* game)
{
    struct game_system_physics* physics = &game->physics;
    ARR_INIT(physics->phys_pairs);
    ARR_INIT(physics->phys_pair_lengths);
}

void game_system_physics_deinit(struct gameplay_state* game)
{
    struct game_system_physics* physics = &game->physics;
    ARR_DEINIT(physics->phys_pairs);
    ARR_DEINIT(physics->phys_pair_lengths);
}

/*  Game physics update overview:

    First, update all entities' positions (move_entities):
        0. (Entity's position/velocity may have been changed by game logic)
        1. If entity is moving slow, add velocity to position
        2. If entity is moving fast, raycast velocity from position
        3. Copy entity position to collider object for physics

    Then for every entity in the active collider list:
        1. If the entity is walking, clamp its position to the surface below it
        2. Apply gravity and/or friction to the collider's velocity
        3. Check if the entity is moving and update the activity flag
            3a. If the entity has become inactive, remove it from active list and add it to inactive list

    Then resolve collisions:
        1. Broad phase: Detect all pairs of potentially colliding objects
        2. Narrow phase: Compute collision results between all pairs
        3. Update: Recalculate the positions/velocities of all colliding objects, according to all detected collisions
            3a. Copy collider position/velocity back up to the game entity
        4. Repeat 1-3 multiple times for more accurate collisions

    Then rebuild the grid contents of all active colliders for querying on the next frame
*/
void game_system_physics_step(struct gameplay_state* game)
{

    int i;
    game_component_collider_id_t coll_id;
    struct game_component_collider* coll_ptr;
    struct game_entity* ent_ptr;
    move_entities(game);

    ARR_FOREACH_REV(game->active_colliders, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))
        || !(ent_ptr = GAME_ENTITY_PTR(game, coll_ptr->ent_id))
        || !coll_ptr->enabled || coll_ptr->phys.pos.z < -32) {
            ARR_SWAPSPLICE(game->active_colliders, i, 1);
            continue;
        }

        /*  Perform surface-walking behavior for entities that want it  */
        if(coll_ptr->clamp_to_surface) clamp_to_surface(game, coll_ptr);
        game_component_collider_update_bound(coll_ptr);

        /*  Apply surface friction or gravity   */
        if(coll_ptr->on_surface) coll_ptr->phys.vel = vec3_mul(coll_ptr->phys.vel, vec3(0.8,0.8,0.8));
        else coll_ptr->phys.vel.z -= 0.007;

        /*  If active, stay active for an extra 30 ticks to be safe.    */
        if(vec3_len2(coll_ptr->phys.vel) > FLT_EPSILON
        || vec3_dist2(coll_ptr->phys.pos, coll_ptr->last_pos) > 0.1) {
            coll_ptr->last_pos = coll_ptr->phys.pos;
            coll_ptr->active_ticks = game->tick+30;
            coll_ptr->active = true;
        }

        /*  If inactive, then push this collider to the inactive list/grid  */
        if(!coll_ptr->always_active && coll_ptr->active_ticks < game->tick) {
            if(coll_ptr->active) {
                coll_ptr->active = false;
                game_grid_add_inactive_collider(game, coll_ptr, coll_id);
                ARR_PUSH(game->inactive_colliders, coll_id);
            }
            ARR_SWAPSPLICE(game->active_colliders, i, 1);
            continue;
        }
    }

    /*  Resolve all the collisions. Add more calls for more accurate collisions
        per frame. Two seems to work fine in most cases.  */
    resolve_collisions(game);
    resolve_collisions(game);

    /********************************************************************************/
    /*  Iterate over inactive colliders grid and remove the ones that are active    */
    int n_found = 0;
    ivec3 iter;
    for(iter.x = 0; iter.x != game->grid.grid_size.x; ++iter.x)
    for(iter.y = 0; iter.y != game->grid.grid_size.y; ++iter.y)
    for(iter.z = 0; iter.z != game->grid.grid_size.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        ARR_FOREACH_REV(game->grid.inactive_colliders[grid_idx], coll_id, i) {
            if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
            ++n_found;
            if(!coll_ptr->enabled) {
                ARR_SWAPSPLICE(game->grid.inactive_colliders[grid_idx], i, 1);
                continue;
            }
            if(coll_ptr->active || coll_ptr->active_ticks > game->tick
            || vec3_len2(coll_ptr->phys.vel) > FLT_EPSILON
            || vec3_dist2(coll_ptr->phys.pos, coll_ptr->last_pos) > 0.1) {
                coll_ptr->active_ticks = game->tick+30;
                if(!coll_ptr->active) {
                    coll_ptr->active = true;
                    ARR_PUSH(game->active_colliders, coll_id);
                }
                ARR_SWAPSPLICE(game->grid.inactive_colliders[grid_idx], i, 1);
            }
        }
    }

    /*  Rebuild the active query grid   */
    game_grid_rebuild_active_colliders(game);
}

void game_system_physics_oneoff(struct gameplay_state* game, struct pg_collider* coll, aabb3D bound)
{
    struct game_grid* grid = &game->grid;
    struct pg_collision result;
    ++grid->query_no;
    ivec3 bound0, bound1;
    get_query_bounds(grid, bound.b0, bound.b1, &bound0, &bound1);
    ivec3 iter;
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(grid->grid_size, iter.x, iter.y, iter.z);
        int i;
        game_component_collider_id_t narrow_coll_id;
        struct game_component_collider* narrow_coll_ptr;
        ARR_FOREACH(game->grid.active_colliders[grid_idx], narrow_coll_id, i) {
            if(!(narrow_coll_ptr = GAME_COMPONENT_PTR(game, collider, narrow_coll_id))) continue;
            if(narrow_coll_ptr->collision_no == game->physics.collision_no
            || narrow_coll_ptr->query_no == game->grid.query_no) continue;
            if(aabb3D_intersects(&narrow_coll_ptr->current_bound, &bound)) {
                pg_collision_calculate(&result, coll, &narrow_coll_ptr->phys);
                pg_collider_reset(&narrow_coll_ptr->phys);
            }
        }
        ARR_FOREACH(game->grid.static_colliders[grid_idx], narrow_coll_id, i) {
            if(!(narrow_coll_ptr = GAME_COMPONENT_PTR(game, collider, narrow_coll_id))) continue;
            if(narrow_coll_ptr->collision_no == game->physics.collision_no
            || narrow_coll_ptr->query_no == game->grid.query_no) continue;
            if(aabb3D_intersects(&narrow_coll_ptr->current_bound, &bound)) {
                pg_collision_calculate(&result, coll, &narrow_coll_ptr->phys);
                pg_collider_reset(&narrow_coll_ptr->phys);
            }
        }
    }
    pg_collider_finish(coll);
}

/************************/
/*  STATIC FUNCS        */
/************************/

static void resolve_collisions(struct gameplay_state* game)
{
    struct game_system_physics* physics = &game->physics;
    game_grid_rebuild_active_colliders(game);
    /*  Clear the list of collider pairs   */
    ++physics->collision_no;
    ARR_TRUNCATE(physics->phys_pairs, 0);
    ARR_TRUNCATE(physics->phys_pair_lengths, 0);
    /********************/
    /*  Broad phase     */
    int i, j;
    game_component_collider_id_t coll_id;
    struct game_component_collider* coll_ptr;
    ARR_FOREACH_REV(game->active_colliders, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
        coll_ptr->collision_no = physics->collision_no;
        coll_ptr->query_no = game->grid.query_no+1;
        /*  Pair this entity with all potential collisions. */
        int query_len = physics_query(game, coll_ptr->current_bound);
        if(!query_len) continue;
        ARR_PUSH(physics->phys_pair_lengths, query_len);
        ARR_PUSH(physics->phys_pairs, coll_id);
    }
    /********************/
    /*  Narrow phase    */
    game_component_collider_id_t narrow_coll_id;
    struct game_component_collider* narrow_coll_ptr;
    struct pg_collision result;
    int pair_length, pair_idx = 0;
    ARR_FOREACH(physics->phys_pair_lengths, pair_length, i) {
        /*  The "source" entity is in the list after all of its paired entities */
        coll_id = physics->phys_pairs.data[pair_idx + pair_length];
        coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id);
        /*  Calculate collisions with all the paired entities   */
        int pair_end = pair_idx + pair_length;
        for(; pair_idx < pair_end; ++pair_idx) {
            narrow_coll_id = physics->phys_pairs.data[pair_idx];
            narrow_coll_ptr = GAME_COMPONENT_PTR(game, collider, narrow_coll_id);
            pg_collision_calculate(&result, &coll_ptr->phys, &narrow_coll_ptr->phys);
        }
        ++pair_idx;
    }
    /********************/
    /*  Update          */
    ARR_FOREACH_REV(physics->phys_pairs, coll_id, i) {
        coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id);
        /*  If static, skip */
        if((coll_ptr->phys.flags & PG_COLLIDER_STATIC)) continue;
        /*  If the collider did collide, then perform the response  */
        if(pg_collider_finish(&coll_ptr->phys)) {
            /*  If this collider wants to walk on surfaces, do that behavior,
                but only for sufficiently shallow collisions (to prevent tunneling
                into narrow corners). Otherwise just regular collision response.    */
            if(coll_ptr->phys.push_len2 < (0.25*0.25) && coll_ptr->phys.push_norm.z > 0.75
            && coll_ptr->clamp_to_surface && coll_ptr->on_surface) {
                pg_collider_respond_walk(&coll_ptr->phys);
            } else {
                pg_collider_respond(&coll_ptr->phys, 1);
            }
            if(vec3_dist2(coll_ptr->phys.pos, coll_ptr->last_pos) > 0.01) {
                coll_ptr->active_ticks = game->tick + 30;
            }
            game_component_collider_update_bound(coll_ptr);
            pg_collider_reset(&coll_ptr->phys);
        }
        /*  Copy collider position back to the game entity  */
        struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, coll_ptr->ent_id);
        ent_ptr->pos = coll_ptr->phys.pos;
        ent_ptr->vel = coll_ptr->phys.vel;
    }
}

static void move_collider(struct gameplay_state* game, struct game_entity* ent_ptr)
{
    struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider);
    if(!coll_ptr) return;

    if(vec3_len2(ent_ptr->vel) < coll_ptr->raycast_threshold) {
        /*  If the entity is moving slowly, just update its position    */
        ent_ptr->pos = vec3_add(ent_ptr->pos, ent_ptr->vel);
    } else {
        /*  Otherwise, do a raycast forward, and only move the distance of the raycast  */
        game_grid_new_collider_query(game);
        coll_ptr->query_no = game->grid.query_no;
        float vel_len = vec3_len(ent_ptr->vel);
        vec3 vel_n = vec3_tdiv(ent_ptr->vel, vel_len);
        float ray_len = game_raycast_all_colliders(game, RAY3D(ent_ptr->pos, vel_n), vel_len);
        if(ray_len > 0  && ray_len < vel_len) {
            ent_ptr->pos = vec3_add(ent_ptr->pos, vec3_scale(vel_n, ray_len - 0.01));
        } else {
            ent_ptr->pos = vec3_add(ent_ptr->pos, ent_ptr->vel);
        }
    }

    /*  Copy the entity's position/velocity down to the collider object */
    coll_ptr->phys.vel = ent_ptr->vel;
    coll_ptr->phys.pos = ent_ptr->pos;
    game_component_collider_update_bound(coll_ptr);
}

static void move_entities(struct gameplay_state* game)
{
    int i;
    game_entity_id_t ent_id;
    struct game_entity* ent_ptr;
    ARR_FOREACH_REV(game->ents_with_trait[GAME_ENTITY_MOVES], ent_id, i) {
        if(!(ent_ptr = GAME_ENTITY_PTR(game, ent_id))
        || !(ent_ptr->traits[GAME_ENTITY_MOVES])) {
            ARR_SWAPSPLICE(game->ents_with_trait[GAME_ENTITY_MOVES], i, 1);
            continue;
        }
        if(ENT_COMPONENT_ID(ent_ptr, collider)
        && ENT_COMPONENT_IS_ENABLED(ent_ptr, collider)) {
            move_collider(game, ent_ptr);
        } else {
            ent_ptr->pos = vec3_add(ent_ptr->pos, ent_ptr->vel);
        }
    }
}

static void clamp_to_surface(struct gameplay_state* game, struct game_component_collider* coll_ptr)
{
    game_grid_new_collider_query(game);
    coll_ptr->query_no = game->grid.query_no;
    float ray_len = game_raycast_all_colliders(game,
        RAY3D(coll_ptr->phys.pos, vec3(0,0,-1)), coll_ptr->clamping_distance);
    if(ray_len > 0 && ray_len < coll_ptr->clamping_distance) {
        coll_ptr->phys.pos.z -= (ray_len - coll_ptr->clamping_offset) + 0.01;
        game_component_collider_update_bound(coll_ptr);
        game_system_physics_oneoff(game, &coll_ptr->phys, coll_ptr->current_bound);
        pg_collider_respond_walk(&coll_ptr->phys);
        pg_collider_reset(&coll_ptr->phys);
        coll_ptr->on_surface = 1;
    } else {
        coll_ptr->on_surface = 0;
    }
}

static inline void get_query_bounds(struct game_grid* grid, vec3 b0, vec3 b1,
                                    ivec3* b0_out, ivec3* b1_out)
{
    b0 = vec3_sub(b0, grid->grid_offset);
    b1 = vec3_sub(b1, grid->grid_offset);
    b0 = vec3_div(b0, grid->cell_size);
    b1 = vec3_div(b1, grid->cell_size);
    *b0_out = ivec3(VEC_XYZ(b0));
    *b1_out = ivec3(VEC_XYZ(b1));
    *b1_out = ivec3_add(*b1_out, ivec3_all(1));
    *b0_out = ivec3_clamp(*b0_out, ivec3(0), ivec3_sub(grid->grid_size, ivec3_all(0)));
    *b1_out = ivec3_clamp(*b1_out, ivec3(0), ivec3_sub(grid->grid_size, ivec3_all(0)));
}

/*  Checks for collider id to prevent redundant collider checks    */
static int physics_query(struct gameplay_state* game, aabb3D bound)
{
    ++game->grid.query_no;
    int n_found = 0;
    ivec3 bound0, bound1, iter;
    get_query_bounds(&game->grid, bound.b0, bound.b1, &bound0, &bound1);
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        int i;
        game_component_collider_id_t coll_id;
        struct game_component_collider* coll_ptr;
        ARR_FOREACH(game->grid.active_colliders[grid_idx], coll_id, i) {
            if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
            if(coll_ptr->collision_no == game->physics.collision_no
            || coll_ptr->query_no == game->grid.query_no) continue;
            coll_ptr->query_no = game->grid.query_no;
            if(aabb3D_intersects(&coll_ptr->current_bound, &bound)) {
                ARR_PUSH(game->physics.phys_pairs, coll_id);
                ++n_found;
            }
        }
        ARR_FOREACH(game->grid.inactive_colliders[grid_idx], coll_id, i) {
            if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
            if(coll_ptr->collision_no == game->physics.collision_no
            || coll_ptr->query_no == game->grid.query_no) continue;
            coll_ptr->query_no = game->grid.query_no;
            if(aabb3D_intersects(&coll_ptr->current_bound, &bound)) {
                ARR_PUSH(game->physics.phys_pairs, coll_id);
                ++n_found;
            }
        }
        ARR_FOREACH(game->grid.static_colliders[grid_idx], coll_id, i) {
            if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
            if(coll_ptr->collision_no == game->physics.collision_no
            || coll_ptr->query_no == game->grid.query_no) continue;
            coll_ptr->query_no = game->grid.query_no;
            if(aabb3D_intersects(&coll_ptr->current_bound, &bound)) {
                ARR_PUSH(game->physics.phys_pairs, coll_id);
                ++n_found;
            }
        }
    }
    return n_found;
}

