/*  Particle Assets     */
struct game_particle_phase {
    char image[32], frame[32];
    pg_tex_frame_t image_frame;
    vec4 color_mul, color_add;
    vec2 scale;
};

struct game_particle_prototype {
    char name[GAME_PROTOTYPE_NAME_MAXLEN];
    SARR_T(16, struct game_particle_phase) phases;
    float lifetime;
};

typedef int game_particle_prototype_id_t;
game_particle_prototype_id_t game_get_particle_prototype(struct game_prototypes* protos, const char* name);




/*  Running particle data   */
struct game_particle {
    pg_mempool_id_t id;
    game_particle_prototype_id_t proto_id;
    vec3 pos, vel;
    uint64_t created_tick;
};

PG_MEMPOOL_DECLARE(struct game_particle, game_particle);
typedef ARR_T(game_particle_id_t) game_particle_arr_t;



/*  Particle system     */
struct game_system_particles {
    /*  Assets  */
    struct game_particle_prototype err_particle_base;
    /*  Runtime particle data   */
    game_particle_pool_t particle_pool;
    game_particle_arr_t all_particles;
    /*  Rendering   */
    struct pg_quadbatch* quads;
    pg_gpu_render_stage_t* renderpass;
    quat billboard;
    vec3 viewer_pos, viewer_dir;
    float fade_start, fade_dist;
    vec3 light_color;
    /*  Performance options     */
    int max_particles;
    int depth_sort_quality;
};

void game_system_particles_init(struct gameplay_state* game);
void game_system_particles_step(struct gameplay_state* game);
void game_system_particles_draw(struct gameplay_state* game, float dt);

struct game_particle* game_system_particles_new(struct gameplay_state* game,
        vec3 pos, vec3 vel, game_particle_prototype_id_t proto_id);

