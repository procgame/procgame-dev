enum game_debug_graph_type {
    GAME_DEBUG_GRAPH_FRAMERATE,
    GAME_DEBUG_GRAPH_PHYSICS,
    GAME_DEBUG_GRAPH_PHYSICS_ACTIVE,
    GAME_DEBUG_GRAPH_PHYSICS_CALCS,
    GAME_DEBUG_GRAPH_WORLDSCROLLING,
    GAME_DEBUG_GRAPH_PLAYERCONTROL,
    GAME_DEBUG_GRAPH_PROJECTILES,
    GAME_DEBUG_GRAPH_PROJECTILES_COUNT,
    GAME_DEBUG_GRAPH_PARTICLES,
    GAME_DEBUG_GRAPH_PARTICLES_COUNT,
    GAME_DEBUG_GRAPH_RENDER,
    GAME_DEBUG_GRAPH_TOTAL_UPDATE,
    GAME_DEBUG_GRAPH_TOTAL_FRAME,
    GAME_DEBUG_GRAPHS_N,
};

struct game_debug_graph_data {
    char label[64];
    double data[240];
    char units[32];
    int idx;
    double red_level;
    double yellow_level;
    bool higher_is_better;
    bool as_integer;
};

struct game_debug_gfx {
    enum { GAME_DEBUG_LINE, GAME_DEBUG_QUAD, GAME_DEBUG_LABEL, GAME_DEBUG_MATRIX } type;
    char label[32];
    vec2 scale;
    vec3 pos;
    quat rot;
    uint32_t color;
    mat4 mat;
};


struct game_system_debugtools {
    vec2 text_scale;
    vec2 half_res;
    vec2 bottom_left;
    struct pg_viewer screen_view;
    struct pg_quadbatch* quads;
    pg_gpu_render_stage_t* render_stage;
    struct game_debug_graph_data graphs[GAME_DEBUG_GRAPHS_N];
    ARR_T(struct game_debug_gfx) gfx_3d;
    ARR_T(struct game_debug_gfx) gfx_2d;
    ARR_T(struct game_debug_gfx) gfx_3d_dynamic;
    ARR_T(struct game_debug_gfx) gfx_2d_dynamic;
    int selected_graph;
};

void game_system_debugtools_init(struct gameplay_state* game);
void game_system_debugtools_draw(struct gameplay_state* game);
void game_system_debugtools_add_data(struct gameplay_state* game,
        enum game_debug_graph_type graph, double s);

void game_system_debugtools_clear_gfx(struct gameplay_state* game);
void game_system_debugtools_add_matrix(struct gameplay_state* game, bool dynamic, mat4 mat);
void game_system_debugtools_add_matrix_labeled(struct gameplay_state* game, bool dynamic,
        mat4 mat, uint32_t color, const char* label);
void game_system_debugtools_add_line(struct gameplay_state* game, bool dynamic,
        vec3 start, vec3 end, float width, uint32_t color);
void game_system_debugtools_add_quad_2d(struct gameplay_state* game, bool dynamic,
        vec3 pos, vec2 scale, uint32_t color);
void game_system_debugtools_add_quad(struct gameplay_state* game, bool dynamic,
        vec3 pos, quat rot, vec2 scale, uint32_t color);
void game_system_debugtools_add_label(struct gameplay_state* game, bool dynamic,
        vec3 pos, vec2 scale, uint32_t color, const char* label);
void game_system_debugtools_add_aabb3D(struct gameplay_state* game, bool dynamic,
        vec3 corner, vec3 size, float width, uint32_t color);


void game_debug_raycast_image(struct gameplay_state* game, const char* filename,
                              vec2 size, vec2 near_far);
