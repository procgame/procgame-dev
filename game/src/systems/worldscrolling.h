#define WORLD_BLOCK_SIZE    32
#define WORLD_REGION_SIZE   256

struct game_system_worldscrolling {
    /*  Current scroll offset of the world  */
    vec2 scroll_offset;
    vec2 region_pos, block_pos;
    ivec2 region_idx, block_idx;
    /*  The controlling entity ie. the world scrolls around the scroller    */
    game_entity_id_t scroller;
    /*  Terrain entity components (3x3 region around the scroller)  */
    SARR_T(GAME_RENDER_MAX_CHUNKS, struct game_world_chunk {
        game_entity_id_t heightmap;
        ivec2 region_idx;
    }) world_chunks;
    SARR_T(GAME_RENDER_MAX_CHUNKS, ivec2) want_world_chunks;
};

void game_system_worldscrolling_init(struct gameplay_state* game, vec2 start_pos);
void game_system_worldscrolling_deinit(struct gameplay_state* game);
void game_system_worldscrolling_scroller(struct gameplay_state* game, game_entity_id_t ent_id);
void game_system_worldscrolling_step(struct gameplay_state* game);
