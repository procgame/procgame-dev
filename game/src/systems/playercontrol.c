#include "procgame.h"
#include "game.h"

static quat game_quat_euler(vec3 angles)
{
    return quat_mul( quat_mul(
                quat_rotation(vec3_X(), -angles.y),
                quat_rotation(vec3_Z(), angles.x) ),
                quat_rotation(vec3_Y(), angles.z) );
}


static quat fake_vr_quat_euler(vec3 angles)
{
    return quat_mul( quat_mul(
                quat_rotation(vec3_X(), -angles.y),
                quat_rotation(vec3_Z(), angles.z) ),
                quat_rotation(vec3_Y(), angles.x) );
}


void game_system_playercontrol_init(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    playercontrol->controlled_ent_id = -1;
    pg_input_mouse_mode(PG_INPUT_MOUSE_CAPTURE);
    playercontrol->fake_vr_state.hand_pos[0] = vec3(-0.75,1.5,-0.5);
    playercontrol->fake_vr_state.hand_pos[1] = vec3(0.75,1.5,-0.5);
    playercontrol->fake_vr_state.hand_dir[0] = vec3(0);
    playercontrol->fake_vr_state.hand_dir[1] = vec3(0);
    playercontrol->fake_vr_state.hmd_pos = vec3(0,1.75,0);
    playercontrol->fake_vr_state.hmd_dir = vec3(0);
    playercontrol->vr_global = pg_gfx_transform(vec3(0,0,0), vec3(1,1,1), quat_rotation(vec3_X(), -LM_PI_2));

    pg_vr_action_set_bindings("/actions/main", 2, PG_VR_BINDINGS(
        PG_VR_BINDING_BOOLEAN(GAME_CONTROL_DEBUG2, "/actions/main/in/debugraycast"),
        PG_VR_BINDING_BOOLEAN(GAME_CONTROL_DEBUG6, "/actions/main/in/teleportraycast")));
}

void game_system_playercontrol_deinit(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    playercontrol->controlled_ent_id = -1;
}

void game_system_playercontrol_set(struct gameplay_state* game,
                                   game_entity_id_t ent_id)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    playercontrol->controlled_ent_id = ent_id;
}




/****************************/
/*  USER INPUT              */

static void poll_kbmouse(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct pg_input_wrapper* mouse = pg_input_mouse();
    struct pg_input_wrapper* keyboard = pg_input_keyboard();

    if(pg_input_user_exit()
    || ((pg_input_get_boolean(keyboard, SDL_SCANCODE_ESCAPE) & PG_INPUT_HIT))) {
        game->user_exit = true;
    }

    /*  Keyboard and mouse controls */
    ivec2 kb_mappings[14] = {
        { GAME_CONTROL_MOVE_FORWARD,    SDL_SCANCODE_W },
        { GAME_CONTROL_MOVE_LEFT,       SDL_SCANCODE_A },
        { GAME_CONTROL_MOVE_BACK,       SDL_SCANCODE_S },
        { GAME_CONTROL_MOVE_RIGHT,      SDL_SCANCODE_D },
        { GAME_CONTROL_JUMP,            SDL_SCANCODE_SPACE },
        { GAME_CONTROL_INTERACT,        SDL_SCANCODE_E },
        { GAME_CONTROL_THROW,           SDL_SCANCODE_G },
        { GAME_CONTROL_DEBUG,           SDL_SCANCODE_1 },
        { GAME_CONTROL_DEBUG2,          SDL_SCANCODE_2 },
        { GAME_CONTROL_DEBUG3,          SDL_SCANCODE_3 },
        { GAME_CONTROL_DEBUG4,          SDL_SCANCODE_4 },
        { GAME_CONTROL_DEBUG5,          SDL_SCANCODE_5 },
        { GAME_CONTROL_DEBUG6,          SDL_SCANCODE_6 },
        { GAME_CONTROL_DEBUG7,          SDL_SCANCODE_7 },
    };
    ivec2 mouse_mappings[4] = {
        { GAME_CONTROL_USE_HIT,         PG_INPUT_MOUSE_LEFT },
        { GAME_CONTROL_USE_HOLD,        PG_INPUT_MOUSE_RIGHT },
        { GAME_CONTROL_NEXT_ITEM,       PG_INPUT_MOUSEWHEEL_UP },
        { GAME_CONTROL_PREV_ITEM,       PG_INPUT_MOUSEWHEEL_DOWN },
    };
    pg_input_wrapper_map_booleans(&playercontrol->controls, keyboard, 14, kb_mappings);
    pg_input_wrapper_map_booleans(&playercontrol->controls, mouse, 4, mouse_mappings);

    vec2 mouse_motion = pg_input_get_analog(mouse, 0);
    vec2 mouse_motion_scaled = vec2_mul(vec2(1,-1), vec2_scale(mouse_motion, 0.005));
    playercontrol->mouse_look_angle = vec2_add(playercontrol->mouse_look_angle, mouse_motion_scaled);
}

static void poll_vr_controls(struct gameplay_state* game)
{
    if(!game->vr_enabled) return;
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    ivec2 vr_gpad_mappings[2] = {
        { GAME_CONTROL_DEBUG2, 33 },
        { GAME_CONTROL_DEBUG6, 32 },
    };
    pg_vr_poll_input(&playercontrol->controls);
    //if(!pg_vr_is_fake()) {
    //    struct pg_vr_device* rh_controller = pg_vr_get_device(pg_vr_get_controller_index(1));
    //    pg_input_wrapper_map_booleans(&playercontrol->controls, rh_controller->controls, 2, vr_gpad_mappings);
    //}

}

static void do_fake_vr_controls(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct pg_input_wrapper* mouse = pg_input_mouse();
    struct pg_input_wrapper* keyboard = pg_input_keyboard();
    /*  Fake VR controls    */
    if(game->vr_enabled && pg_vr_is_fake()) {
        /*  Control HMD rotation    */
        vec2 look = playercontrol->mouse_look_angle;
        playercontrol->fake_vr_state.hmd_dir = vec3(look.x, look.y, 0);
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_Y) & PG_INPUT_HIT)) {
            pg_log(PG_LOG_INFO, "mouse look %f %f", VEC_XY(look));
        }
        /*  Control HMD position    */
        vec3 hmd_pos = vec3(0,0,0);
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_H) & PG_INPUT_HELD)) hmd_pos.x = 0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_L) & PG_INPUT_HELD)) hmd_pos.x = -0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_K) & PG_INPUT_HELD)) hmd_pos.z = 0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_J) & PG_INPUT_HELD)) hmd_pos.z = -0.05;
        playercontrol->fake_vr_state.hmd_pos = vec3_add(playercontrol->fake_vr_state.hmd_pos, hmd_pos);

        /*  Control hand rotation   */
        vec2 hand_rotate = vec2(0,0);
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_LEFT) & PG_INPUT_HELD)) hand_rotate.x = -0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_RIGHT) & PG_INPUT_HELD)) hand_rotate.x = 0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_UP) & PG_INPUT_HELD)) hand_rotate.y = 0.05;
        if((pg_input_get_boolean(keyboard, SDL_SCANCODE_DOWN) & PG_INPUT_HELD)) hand_rotate.y = -0.05;
        playercontrol->fake_vr_state.hand_dir[1] = vec3_add(playercontrol->fake_vr_state.hand_dir[1], vec3(VEC_XY(hand_rotate)));

        /*  Set fake transforms */
        quat hmd_orientation = fake_vr_quat_euler(playercontrol->fake_vr_state.hmd_dir);
        quat lh_orientation = fake_vr_quat_euler(playercontrol->fake_vr_state.hand_dir[0]);
        quat rh_orientation = fake_vr_quat_euler(playercontrol->fake_vr_state.hand_dir[1]);
        pg_vr_set_fake_device_tx(pg_vr_get_hmd_index(), playercontrol->fake_vr_state.hmd_pos, hmd_orientation);
        pg_vr_set_fake_device_tx(pg_vr_get_controller_index(0), playercontrol->fake_vr_state.hand_pos[0], lh_orientation);
        pg_vr_set_fake_device_tx(pg_vr_get_controller_index(1), playercontrol->fake_vr_state.hand_pos[1], rh_orientation);

    }
}




/************************************************/
/*  Player position (head, hands, etc.)         */

static void game_get_vr_pose(struct gameplay_state* game, struct game_vr_pose* pose_out)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    pg_vr_track_devices();
    struct pg_vr_device* hmd = pg_vr_get_device(pg_vr_get_hmd_index());
    struct pg_vr_device* lh_controller = pg_vr_get_device(pg_vr_get_controller_index(0));
    struct pg_vr_device* rh_controller = pg_vr_get_device(pg_vr_get_controller_index(1));
    pose_out->hmd_tx = pg_gfx_transform_apply(&hmd->pg_tx, &playercontrol->vr_global);
    pose_out->hand_tx[0] = pg_gfx_transform_apply(&lh_controller->pg_tx, &playercontrol->vr_global);
    pose_out->hand_tx[1] = pg_gfx_transform_apply(&rh_controller->pg_tx, &playercontrol->vr_global);
}

static void game_calculate_player_pose_VR(struct game_player_pose* pose_out, 
        struct pg_gfx_transform* player_tx, struct game_vr_pose* vr_pose)
{
    struct pg_gfx_transform foot_tx = pg_gfx_transform(vec3(0,0,-1), vec3(1,1,1), quat_identity());
    foot_tx = pg_gfx_transform_apply(&foot_tx, player_tx);

    pose_out->origin = *player_tx;
    pose_out->foot_tx = foot_tx;
    pose_out->hand_tx[0] = pg_gfx_transform_apply(&vr_pose->hand_tx[0], &foot_tx);
    pose_out->hand_tx[1] = pg_gfx_transform_apply(&vr_pose->hand_tx[1], &foot_tx);
    pose_out->head_tx = pg_gfx_transform_apply(&vr_pose->hmd_tx, &foot_tx);
}

static void game_calculate_player_pose_FS(struct game_player_pose* pose_out, 
        struct pg_gfx_transform* player_tx, vec2 mouse_look_angle)
{
    quat look_quat = game_quat_euler(vec3(VEC_XY(mouse_look_angle)));

    struct pg_gfx_transform foot_tx = pg_gfx_transform(vec3(0,0,-1), vec3(1,1,1), quat_identity());
    foot_tx = pg_gfx_transform_apply(&foot_tx, player_tx);
    struct pg_gfx_transform lh_tx = pg_gfx_transform(vec3(-0.5,0,0), vec3(1,1,1), quat_identity());
    lh_tx = pg_gfx_transform_apply(&lh_tx, player_tx);
    struct pg_gfx_transform rh_tx = pg_gfx_transform(vec3(0.5,0,-1), vec3(1,1,1), quat_identity());
    rh_tx = pg_gfx_transform_apply(&rh_tx, player_tx);
    struct pg_gfx_transform head_tx = pg_gfx_transform(vec3(0,0,0.75), vec3(1,1,1), look_quat);
    head_tx = pg_gfx_transform_apply(&head_tx, player_tx);

    pose_out->origin = *player_tx;
    pose_out->foot_tx = foot_tx;
    pose_out->hand_tx[0] = lh_tx;
    pose_out->hand_tx[1] = rh_tx;
    pose_out->head_tx = head_tx;
}

static void game_calculate_player_pose(struct gameplay_state* game,
        struct pg_gfx_transform* player_tx, struct game_player_pose* pose_out)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    if(game->vr_enabled) {
        game_calculate_player_pose_VR(pose_out, player_tx, &playercontrol->vr_pose);
    } else {
        game_calculate_player_pose_FS(pose_out, player_tx, playercontrol->mouse_look_angle);
    }
}

static void calculate_interaction_cursor(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct pg_gfx_transform cursor_tx;
    if(!game->vr_enabled) {
        cursor_tx = playercontrol->player_pose.head_tx;
    } else {
        cursor_tx = playercontrol->player_pose.hand_tx[1];
    }
    vec3 cursor_pos = mat4_mul_vec3(cursor_tx.tx, vec3(0,0,0), true);
    vec3 cursor_dir = mat4_mul_vec3(cursor_tx.tx, vec3(0,0,-1), false);
    playercontrol->cursor_ray = RAY3D(cursor_pos, cursor_dir);
    pg_ui_context_set_cursor_3D(game->ui, playercontrol->cursor_ray.src, playercontrol->cursor_ray.dir);
}




/****************************/
/*  PLAYER ACTIONS          */

static void control_player_movement(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct game_entity* ent_ptr = game_entity_get(&game->entpool, playercontrol->controlled_ent_id);
    struct game_component_player* player_ptr = ENT_COMPONENT_PTR(game, ent_ptr, player);
    struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider);
    struct pg_input_wrapper* controls = &playercontrol->controls;

    float move_speed = 0.05;

    /************/
    /*  Walk    */
    vec2 move = vec2(0);
    if((pg_input_get_boolean(controls, GAME_CONTROL_MOVE_FORWARD) & PG_INPUT_ON)) move.y += 1;
    if((pg_input_get_boolean(controls, GAME_CONTROL_MOVE_BACK) & PG_INPUT_ON)) move.y -= 1;
    if((pg_input_get_boolean(controls, GAME_CONTROL_MOVE_RIGHT) & PG_INPUT_ON)) move.x += 1;
    if((pg_input_get_boolean(controls, GAME_CONTROL_MOVE_LEFT) & PG_INPUT_ON)) move.x -= 1;
    /*  Transform move vector according to head orientation */
    vec3 move3 = vec3(move.x, 0, -move.y);
    move3 = mat4_mul_vec3(playercontrol->player_pose.head_tx.tx, move3, false);
    move = vec2(VEC_XY(move3));
    if(!vec2_is_zero(move)) {
        move = vec2_tolen(move, move_speed);
        if(coll_ptr->on_surface) {
            ent_ptr->vel.x += move.x;
            ent_ptr->vel.y += move.y;
        } else {
            move = vec2_scale(move, 0.025);
            vec2 vel2 = vec2(VEC_XY(coll_ptr->phys.vel));
            if(vec2_len2(vel2) > 0.01) {
                float d = vec2_dot(vec2_norm(vel2), vec2_norm(move));
                d = LM_MIN(0.75, LM_MAX(0, 0.5 - d));
                vec2 new_vel = vec2_add(vel2, vec2_scale(move, d));
                ent_ptr->vel.x = new_vel.x;
                ent_ptr->vel.y = new_vel.y;
            } else {
                vel2 = vec2_add(vel2, vec2_scale(move, 1));
                ent_ptr->vel.x = vel2.x;
                ent_ptr->vel.y = vel2.y;
            }
        }
    }

    ent_ptr->rot = playercontrol->player_pose.head_tx.rot;

    /************/
    /*  Jump    */
    /*  Consider the player to be on the ground even for 10 ticks after they aren't */
    if(coll_ptr->on_surface) player_ptr->on_surface_grace = game->tick + 10;
    if((pg_input_get_boolean(controls, GAME_CONTROL_JUMP) & PG_INPUT_HIT)) {
        if(player_ptr->on_surface_grace > game->tick) {
            player_ptr->jump_timer = game->tick + 10;
            player_ptr->on_surface_grace = 0;
            coll_ptr->on_surface = 0;
            ent_ptr->vel.z = 0.25;
        }
    }
    /*  Disable surface clamping for a few ticks after jumping  */
    if(player_ptr->jump_timer > game->tick) {
        coll_ptr->clamp_to_surface = false;
    } else {
        coll_ptr->clamp_to_surface = true;
    }
}

static void control_player_actions(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct pg_input_wrapper* controls = &playercontrol->controls;
    struct game_entity* ent_ptr = game_entity_get(&game->entpool, playercontrol->controlled_ent_id);
    struct game_component_player* player_ptr = ENT_COMPONENT_PTR(game, ent_ptr, player);

    /************************/
    /*  Items/inventory     */
    if(pg_input_get_boolean(controls, GAME_CONTROL_THROW) & PG_INPUT_HIT) {
    }
    if(pg_input_get_boolean(controls,GAME_CONTROL_INTERACT) & PG_INPUT_HIT) {
    }
    if(pg_input_get_boolean(controls,GAME_CONTROL_NEXT_ITEM) & PG_INPUT_HIT) {
        struct game_component_inventory* plr_inv_ptr = ENT_COMPONENT_PTR(game, ent_ptr, inventory);
        game_component_inventory_next_item(game, plr_inv_ptr);
    } else if(pg_input_get_boolean(controls,GAME_CONTROL_PREV_ITEM) & PG_INPUT_HIT) {
        struct game_component_inventory* plr_inv_ptr = ENT_COMPONENT_PTR(game, ent_ptr, inventory);
        game_component_inventory_prev_item(game, plr_inv_ptr);
    }
    if(pg_input_get_boolean(controls,GAME_CONTROL_USE_HIT) & PG_INPUT_HIT) {
        struct game_component_inventory* plr_inv_ptr = ENT_COMPONENT_PTR(game, ent_ptr, inventory);
        game_component_inventory_use_held_item(game, plr_inv_ptr, 0);
    }
    if(pg_input_get_boolean(controls, GAME_CONTROL_USE_HOLD) & PG_INPUT_HIT) {
        struct game_component_inventory* plr_inv_ptr = ENT_COMPONENT_PTR(game, ent_ptr, inventory);
        game_component_inventory_use_held_item(game, plr_inv_ptr, 1);
    }
}

static void control_debug_stuff(struct gameplay_state* game)
{
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct pg_input_wrapper* controls = &playercontrol->controls;
    struct game_entity* ent_ptr = game_entity_get(&game->entpool, playercontrol->controlled_ent_id);
    struct game_component_player* player_ptr = ENT_COMPONENT_PTR(game, ent_ptr, player);

    /************/
    /*  Debug   */
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG) & PG_INPUT_ON) {
        mat4 tx = playercontrol->player_pose.origin.tx;
        game_system_debugtools_add_matrix_labeled(game, false, playercontrol->player_pose.origin.tx, 0xFFFFFFFF, "body");
        game_system_debugtools_add_matrix_labeled(game, false, playercontrol->player_pose.head_tx.tx, 0xFFFFFFFF, "head");
        if(game->vr_enabled) {
            mat4 eye_left = pg_vr_display_get_eye_transform(0);
            mat4 eye_right = pg_vr_display_get_eye_transform(1);
            eye_left = mat4_mul(playercontrol->player_pose.head_tx.tx, eye_left);
            eye_right = mat4_mul(playercontrol->player_pose.head_tx.tx, eye_right);
            quat eye_left_rot = quat_sph_from_mat4(eye_left);
            quat eye_right_rot = quat_sph_from_mat4(eye_right);
            game_system_debugtools_add_quad(game, false, mat4_mul_vec3(eye_left, vec3(0), true),
                    eye_left_rot, vec2(0.1,0.1), 0xFFFF00FF);
            game_system_debugtools_add_quad(game, false, mat4_mul_vec3(eye_right, vec3(0), true),
                    eye_left_rot, vec2(0.1,0.1), 0x00FFFFFF);
            game_system_debugtools_add_matrix_labeled(game, false, eye_left, 0xFFFFFFFF, "L");
            game_system_debugtools_add_matrix_labeled(game, false, eye_right, 0xFFFFFFFF, "R");
        }
    }

    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG2) & PG_INPUT_HIT) {
        game_grid_new_collider_query(game);
        game->debug_hmap_collision = true;
        struct game_component_collider* player_coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider);
        player_coll_ptr->query_no = game->grid.query_no;
        float foo = game_raycast_all_colliders(game, RAY3D(playercontrol->cursor_ray.src, playercontrol->cursor_ray.dir), 256);
        vec3 end_point = vec3_add(playercontrol->cursor_ray.src, vec3_scale(playercontrol->cursor_ray.dir, foo - 0.01));
        game_grid_new_collider_query(game);
        player_coll_ptr->query_no = game->grid.query_no;
        vec3 end_norm = game_query_deepest_collision(game, end_point, 0.25, NULL);
        game->debug_hmap_collision = false;
        end_norm = vec3_norm(end_norm);
        game_system_debugtools_add_quad(game, false, end_point, quat_from_to(vec3_Z(), end_norm), vec2(1,1), 0xFF00FFFF);
        game_system_debugtools_add_line(game, false, playercontrol->cursor_ray.src, end_point, 0.2, 0xFFFFFFFF);
        //char label[32];
        //snprintf(label, 32, "(%.2f %.2f %.2f) %.2f\n", VEC_XYZ(eye_dir), look_distance);

    }
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG3) & PG_INPUT_HIT) {
        game_system_debugtools_clear_gfx(game);
    }
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG4) & PG_INPUT_HIT) {
        game->vr_enabled = !game->vr_enabled;
    }
    game->ui->debug_all = false;
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG5) & PG_INPUT_HIT) {
        //quat rot = quat_rotation(vec3_Z(), -player_ptr->look.x);
        //rot = quat_mul(quat_rotation(vec3_X(), -LM_PI_2), rot);
        //pg_ui_context_set_origin_3D(game->ui, ent_ptr->pos, vec3(2,2,2), rot);
        //game->ui->debug_all = true;
        //if(game->timer.time_speed < 1) {
        //    game->timer.time_speed = 1;
        //} else {
        //    game->timer.time_speed = 0.2;
        //}
        vec3 player_pos = ent_ptr->pos;
        game_entity_id_t new_ent_id;
        struct game_entity* new_ent_ptr = game_create_entity_from_prototype_name(game, "Barrel", &new_ent_id);
        new_ent_ptr->pos = vec3_add(vec3(5,0,0), player_pos);
        ARR_PUSH(game->active_colliders, ENT_COMPONENT_ID(new_ent_ptr, collider));
    }
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG6) & PG_INPUT_HELD) {
        game_grid_new_collider_query(game);
        float foo = game_raycast_static_colliders(game, RAY3D(playercontrol->cursor_ray.src, playercontrol->cursor_ray.dir), 256);
        game->debug_hmap_collision = false;
        vec3 end_point = vec3_add(playercontrol->cursor_ray.src, vec3_scale(playercontrol->cursor_ray.dir, foo - 0.01));
        vec3 end_norm = game_query_deepest_collision(game, end_point, 0.25, NULL);
        end_norm = vec3_norm(end_norm);
        game_system_debugtools_add_line(game, true, playercontrol->cursor_ray.src, end_point, 1.0, 0xFF00FF44);
        game_system_debugtools_add_quad(game, true, end_point, quat_from_to(vec3_Z(), end_norm), vec2(1,1), 0xFFFFFF44);
    }
    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG6) & PG_INPUT_RELEASED) {
        game_grid_new_collider_query(game);
        float foo = game_raycast_static_colliders(game, RAY3D(playercontrol->cursor_ray.src, playercontrol->cursor_ray.dir), 256);
        game->debug_hmap_collision = false;
        vec3 end_point = vec3_add(playercontrol->cursor_ray.src, vec3_scale(playercontrol->cursor_ray.dir, foo - 0.01));
        vec3 end_norm = game_query_deepest_collision(game, end_point, 0.25, NULL);
        end_norm = vec3_norm(end_norm);
        if(foo < 16.0f && end_norm.z >= 0.75) {
            ent_ptr->pos = vec3_add(end_point, vec3(0,0,1));
        }
        game_system_debugtools_add_line(game, false, playercontrol->cursor_ray.src, end_point, 1.0, 0xFF00FF44);
        game_system_debugtools_add_quad(game, false, end_point, quat_identity(), vec2(0.25,0.25), 0xFFFFFFFF);
    }

    if(pg_input_get_boolean(controls, GAME_CONTROL_DEBUG7) & PG_INPUT_HIT) {
        vec3 player_pos = ent_ptr->pos;
        game_entity_id_t new_ent_id;
        struct game_entity* new_ent_ptr = game_create_entity_from_prototype_name(game, "gun", &new_ent_id);
        new_ent_ptr->pos = vec3_add(vec3(5,0,0), player_pos);
        ARR_PUSH(game->active_colliders, ENT_COMPONENT_ID(new_ent_ptr, collider));
    }
}






void game_system_playercontrol_step(struct gameplay_state* game)
{
    /*  Poll input (or generate fake input) */
    pg_input_poll();
    do_fake_vr_controls(game);
    poll_kbmouse(game);
    poll_vr_controls(game);

    /*  Consume input */
    control_player_movement(game);
    control_player_actions(game);
    control_debug_stuff(game);

    /*  Calculate the player's position on this game step (including head, hands, etc.) */
    calculate_interaction_cursor(game);
}






void game_system_playercontrol_perspective(struct gameplay_state* game, float dt)
{
    struct game_system_render* render = &game->render;
    struct game_system_playercontrol* playercontrol = &game->playercontrol;
    struct game_entity* ent_ptr =
        game_entity_get(&game->entpool, playercontrol->controlled_ent_id);
    struct game_component_player* player_ptr = ENT_COMPONENT_PTR(game, ent_ptr, player);
    struct game_component_collider* coll_ptr = ENT_COMPONENT_PTR(game, ent_ptr, collider);

    /*  Interpolate movement forward, and adjust for collisions at the forward position */
    vec3 player_pos;
    if(dt != 0) {
        struct pg_collider tmp_coll = coll_ptr->phys;
        vec3 move = vec3_scale(tmp_coll.vel, dt);
        tmp_coll.pos = vec3_add(tmp_coll.pos, move);
        aabb3D bound = coll_ptr->current_bound;
        bound.b0 = vec3_add(bound.b0, move);
        bound.b1 = vec3_add(bound.b1, move);
        game_system_physics_oneoff(game, &tmp_coll, bound);
        pg_collider_respond_walk(&tmp_coll);
        player_pos = tmp_coll.pos;
    }
    struct pg_gfx_transform player_tx = pg_gfx_transform(player_pos, vec3(1,1,1), quat_identity());
    struct game_player_pose player_pose;

    /*  Calculate the view transform    */
    if(game->vr_enabled) game_get_vr_pose(game, &playercontrol->vr_pose);
    game_calculate_player_pose(game, &player_tx, &player_pose);
    playercontrol->player_pose = player_pose;

    /*  Create the viewer   */
    if(game->vr_enabled) {
        struct pg_gfx_transform view_tx = player_pose.head_tx;
        mat4 eye_left = pg_vr_display_get_eye_transform(0);
        mat4 eye_right = pg_vr_display_get_eye_transform(1);
        mat4 proj_left = pg_vr_display_get_eye_projection(0, render->near_far);
        mat4 proj_right = pg_vr_display_get_eye_projection(1, render->near_far);
        render->eye_projview_left = mat4_mul( proj_left, mat4_inverse( mat4_mul(view_tx.tx, eye_left) ) );
        render->eye_projview_right = mat4_mul( proj_right, mat4_inverse( mat4_mul(view_tx.tx, eye_right) ) );
    } else {
        struct pg_gfx_transform view_tx = player_pose.head_tx;
        pg_viewer_perspective(&render->view_3d, &view_tx,
                render->aspect_ratio, render->fov, render->near_far);
    }

    /*  Update billboarding quats   */
    render->billboard = quat_from_sph(playercontrol->mouse_look_angle.x + LM_PI, -playercontrol->mouse_look_angle.y);
    render->billboard_vertical = quat_from_sph(playercontrol->mouse_look_angle.x + LM_PI, -LM_PI_2);
}
