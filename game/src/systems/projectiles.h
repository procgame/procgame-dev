struct game_asset_projectile {
    char name[32];
    char image[32], frame[32];
    pg_tex_frame_t tex_frame;
    float speed;
};

typedef HTABLE_T(struct game_asset_projectile*) game_asset_projectile_table_t;

struct game_system_projectiles {
    game_asset_projectile_table_t asset_table;
};

void game_system_projectiles_step(struct gameplay_state* game);
