#include "procgame.h"
#include "game.h"



static void game_entity_callbacks_init(struct game_entity_callbacks* cbs)
{
    HTABLE_INIT(cbs->draw_fn_table, 8);
    HTABLE_INIT(cbs->item_use_fn_table, 8);
    HTABLE_INIT(cbs->projectile_hit_fn_table, 8);
    game_register_draw_funcs(cbs);
    game_register_item_use_funcs(cbs);
    game_register_projectile_hit_funcs(cbs);
}

static void game_entity_callbacks_deinit(struct game_entity_callbacks* cbs)
{
    HTABLE_DEINIT(cbs->draw_fn_table);
    HTABLE_DEINIT(cbs->item_use_fn_table);
    HTABLE_DEINIT(cbs->projectile_hit_fn_table);
}




static bool game_prototypes_init(struct game_prototypes* protos)
{
    /*  Init prototype/function pointer tables   */
    HTABLE_INIT(protos->particles_table, 8);
    HTABLE_INIT(protos->entities_table, 8);
    if(!game_read_entity_prototypes_list(protos, "data/game_defs/entity_prototypes.json")
    || !game_read_particle_prototypes_list(protos, "data/game_defs/particle_prototypes.json")) {
        pg_log(PG_LOG_ERROR, "Failed to read game prototypes");
        return false;
    }
    return true;
}

static void game_prototypes_deinit(struct game_prototypes* protos)
{
    HTABLE_DEINIT(protos->particles_table);
    HTABLE_DEINIT(protos->entities_table);
}




static bool game_load_assets(struct game_resources* res)
{
    /*  Create an asset manager with GPU and graphics asset loaders */
    pg_asset_manager_t* asset_mgr = pg_asset_manager_create();
    /*  Set the asset search path for game resources    */
    pg_asset_manager_add_search_path(asset_mgr, "./data/");
    pg_gpu_asset_loaders(asset_mgr);
    pg_gfx_asset_loaders(asset_mgr);


    /*  Set some render resolution and font-size variables for use in the renderer assets   */
    ivec2 render_resolution = ivec2_scale(pg_window_size(), 1);
    int font_size = render_resolution.y / 20;
    pg_asset_manager_set_variable(asset_mgr, "cfg_GameFontSize", pg_data_init(PG_INT, 1, &font_size));
    pg_asset_manager_set_variable(asset_mgr, "cfg_GameRenderResolution", pg_data_init(PG_IVEC2, 1, &render_resolution));
    pg_vr_add_display_to_asset_manager(asset_mgr);

    /*  Load all game assets from main asset file "data.json"   */
    int loaded_assets = pg_asset_multiple_from_file(asset_mgr, NULL, "data.json", NULL, 0);
    if(loaded_assets == -1) {
        pg_log(PG_LOG_ERROR, "Failed to parse main game assets");
        return false;
    }

    /*  If VR is enabled, there are more assets to load as well */
    if(pg_have_vr()) {
        int loaded_vr_assets = pg_asset_multiple_from_file(asset_mgr, NULL, "renderer/GameRenderOutput_VR.json", NULL, 0);
        if(loaded_vr_assets == -1) {
            pg_log(PG_LOG_ERROR, "Failed to parse main game VR assets");
            return false;
        }
    }

    /*  Output loaded asset data to the resources struct    */
    res->fonts = pg_asset_get_data_by_name(asset_mgr, "GameFonts");
    res->ui_imgs = pg_asset_get_data_by_name(asset_mgr, "GameParticleSheet");
    res->particle_imgs = pg_asset_get_data_by_name(asset_mgr, "GameParticleSheet");
    res->game_imgs = pg_asset_get_data_by_name(asset_mgr, "GameParticleSheet");

    if(!res->fonts || !res->ui_imgs || !res->particle_imgs || !res->game_imgs) {
        return false;
    }

    res->text_fmt = PG_TEXT_FORMATTER(res->fonts);
    res->debug = false;
    res->asset_mgr = asset_mgr;
    return true;
}




/********************/
/*  Game assets     */
bool game_resources_init(struct game_resources* res)
{
    *res = (struct game_resources){0};

    if(!game_load_assets(res)) {
        pg_log(PG_LOG_ERROR, "Failed to load game assets");
        return false;
    }

    game_entity_callbacks_init(&res->entity_callbacks);
    if(!game_prototypes_init(&res->protos)) {
        pg_log(PG_LOG_ERROR, "Failed to load game object prototypes");
        return false;
    }

    game_finalize_entity_prototypes(&res->protos, &res->entity_callbacks, res->game_imgs);
    game_finalize_particle_prototypes(&res->protos, res->particle_imgs);

    return true;
}

void game_resources_deinit(struct game_resources* res)
{
    game_entity_callbacks_deinit(&res->entity_callbacks);
    game_prototypes_deinit(&res->protos);
    pg_asset_manager_destroy(res->asset_mgr);
}
