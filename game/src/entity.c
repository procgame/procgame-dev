#include "procgame.h"
#include "game.h"

PG_MEMPOOL_DEFINE(struct game_entity, game_entity, alloc);

struct game_entity* game_entity_create(struct gameplay_state* game, game_entity_id_t* id_out)
{
    struct game_entity* ent_ptr = NULL;
    game_entity_id_t ent_id = game_entity_alloc(&game->entpool, &ent_ptr);
    if(!ent_ptr) return NULL;
    ARR_PUSH(game->all_entities, ent_id);
    int i;
    for(i = 0; i < GAME_ENTITY_N_COMPONENTS; ++i) {
        ent_ptr->components[i] = 0;
    }
    if(id_out) *id_out = ent_id;
    return ent_ptr;
}

#define FREE_COMPONENT(GAME, ENT, NAME) \
    if(ENT->components[game_component_##NAME##_idx]) \
        { GAME_COMPONENT_FREE(GAME, NAME, ENT->components[game_component_##NAME##_idx]); }

void game_entity_destroy(struct gameplay_state* game, struct game_entity* ent_ptr)
{
    if(!ent_ptr) return;
    int i;
    /*  Free all the components */
    FREE_COMPONENT(game, ent_ptr, collider);
    FREE_COMPONENT(game, ent_ptr, drawable);
    FREE_COMPONENT(game, ent_ptr, player);
    FREE_COMPONENT(game, ent_ptr, heightmap);
    FREE_COMPONENT(game, ent_ptr, item);
    /*  Free the entity */
    game_entity_free(&game->entpool, game_entity_id(&game->entpool, ent_ptr));
}

#undef FREE_COMPONENT

void game_entity_update_aabb(struct game_entity* ent_ptr, vec3 pos)
{
}

void game_entity_set_trait(struct gameplay_state* game, struct game_entity* ent_ptr,
                           enum game_entity_trait trait, bool set)
{
    ent_ptr->traits[trait] = set;
    if(set) ARR_PUSH(game->ents_with_trait[trait], game_entity_id(&game->entpool, ent_ptr));
}
