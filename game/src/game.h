struct gameplay_state;

/*  Game entities   */
#include "entity.h"
/*  Game assets     */
#include "game_prototypes.h"
#include "game_assets.h"
/*  Systems */
#include "systems/physics.h"
#include "systems/render.h"
#include "systems/playercontrol.h"
#include "systems/worldscrolling.h"
#include "systems/particles.h"
#include "systems/debugtools.h"
#include "systems/projectiles.h"
/*  Heightmap rendering shader  */
#include "heightmap_render.h"
/*  Spatial partitioning grid   */
#include "spacegrid.h"


/********************************/
/*  Gameplay (pew pew) state    */
/********************************/

struct gameplay_state {
    /****************/
    /*  Assets      */
    struct game_resources* res;
    pg_asset_manager_t* asset_mgr;

    /************************/
    /*  Time management     */
    uint32_t tick;
    struct pg_timestepper timer;

    /****************************/
    /*  Entities/components     */
    struct game_grid grid;
    game_entity_pool_t entpool;
    game_entity_arr_t all_entities;
    game_entity_arr_t ents_with_trait[GAME_ENTITY_N_TRAITS];
    /*  Colliders   */
    game_component_collider_pool_t collider_pool;
    game_component_collider_arr_t collider_components;
    game_component_collider_arr_t active_colliders;
    game_component_collider_arr_t inactive_colliders;
    game_component_collider_arr_t static_colliders;
    /*  Drawables   */
    game_component_drawable_pool_t drawable_pool;
    game_component_drawable_arr_t drawable_components;
    /*  Player-controlled   */
    game_component_player_pool_t player_pool;
    game_component_player_arr_t player_components;
    /*  Heightmaps  */
    game_component_heightmap_pool_t heightmap_pool;
    game_component_heightmap_arr_t heightmap_components;
    /*  Items       */
    game_component_item_pool_t item_pool;
    game_component_item_arr_t item_components;
    /*  Particle emitters   */
    game_component_particle_emitter_pool_t particle_emitter_pool;
    game_component_particle_emitter_arr_t particle_emitter_components;
    /*  Inventories */
    game_component_inventory_pool_t inventory_pool;
    game_component_inventory_arr_t inventory_components;
    /*  Projectiles */
    game_component_projectile_pool_t projectile_pool;
    game_component_projectile_arr_t projectile_components;

    /****************/
    /*  Systems     */
    int user_exit;
    struct pg_ui_context* ui;
    struct game_system_playercontrol playercontrol;
    struct game_system_physics physics;
    struct game_system_worldscrolling scrolling;
    struct game_system_particles particles;
    struct game_system_projectiles projectiles;
    struct game_system_render render;

    bool vr_enabled;

    /********************/
    /*  Debugging crap  */
    struct game_system_debugtools debug;
    game_entity_id_t plr_id;

    uint32_t debug_hmap_color;
    bool debug_hmap_collision;
    vec3 debug_hmap_offset;
};

void gameplay_init(struct gameplay_state* game, struct game_resources* res);
void game_build_ui(struct gameplay_state* game);
void gameplay_deinit(struct gameplay_state* game);
int gameplay_main(struct gameplay_state* g);


/*  Create/retrieve/destroy entity components   */
#define GAME_ENTITY_PTR(GAME, ID) \
    game_entity_get(&GAME->entpool, ID)

#define GAME_COMPONENT_PTR(GAME, NAME, ID) \
    game_component_##NAME##_get(&GAME->NAME##_pool, ID)

#define GAME_COMPONENT_NEW(GAME, NAME, PTR) \
    game_component_##NAME##_alloc(&GAME->NAME##_pool, PTR)

#define GAME_COMPONENT_FREE(GAME, NAME, ID) \
    game_component_##NAME##_free(&GAME->NAME##_pool, ID)
