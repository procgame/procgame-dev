#include "procgame.h"
#include "game.h"

void gameplay_init(struct gameplay_state* game, struct game_resources* res)
{
    *game = (struct gameplay_state){0};
    game->res = res;
    game->asset_mgr = res->asset_mgr;
    game->vr_enabled = pg_have_vr();

    /*  Building game state     */
    game_entity_pool_init(&game->entpool);
    game_component_collider_pool_init(&game->collider_pool);
    game_component_drawable_pool_init(&game->drawable_pool);
    game_component_player_pool_init(&game->player_pool);
    game_component_heightmap_pool_init(&game->heightmap_pool);
    game_component_item_pool_init(&game->item_pool);
    game_component_particle_emitter_pool_init(&game->particle_emitter_pool);
    game_component_inventory_pool_init(&game->inventory_pool);
    game_component_projectile_pool_init(&game->projectile_pool);
    game_grid_init(game, ivec3(9,9,3), vec3_all(WORLD_BLOCK_SIZE));
    pg_timestepper_init(&game->timer, 60);
    game->timer.time_speed = 1;

    game->ui = pg_asset_get_data_by_name(game->asset_mgr, "GameUI");
    game_build_ui(game);

    /*  Init game systems   */
    game_system_render_init(game);
    game_system_physics_init(game);
    game_system_particles_init(game);
    game->particles.max_particles = 2048;
    game->particles.depth_sort_quality = 2;
    game_system_worldscrolling_init(game, vec2(0,0));
    game_system_playercontrol_init(game);
    game_system_debugtools_init(game);

    /*  Make the player entity  */
    struct game_entity* ent_ptr = game_entity_create(game, &game->plr_id);
    ent_ptr->pos = vec3(4,4,32);
    game_entity_set_trait(game, ent_ptr, GAME_ENTITY_MOVES, true);
    struct game_component_inventory* inv_ptr = ENT_COMPONENT_NEW(game, ent_ptr, inventory, NULL);
    game_component_inventory_init(inv_ptr);
    struct game_component_collider* coll_ptr = ENT_COMPONENT_NEW(game, ent_ptr, collider, NULL);
    game_component_collider_init(coll_ptr, &PG_COLLIDER_SPHERE(vec3(0,0,0), 1));
    coll_ptr->always_active = true;
    coll_ptr->clamp_to_surface = false;
    coll_ptr->clamping_distance = 1.2;
    coll_ptr->clamping_offset = 1;
    coll_ptr->active = true;
    coll_ptr->active_ticks = 100;
    game_component_collider_update_bound(coll_ptr);
    game_entity_bound_from_collider(game, ent_ptr);
    struct game_component_player* player_ptr = ENT_COMPONENT_NEW(game, ent_ptr, player, NULL);
    game_component_player_init(player_ptr);
    game_system_playercontrol_set(game, game->plr_id);
    game_system_worldscrolling_scroller(game, game->plr_id);
    ARR_PUSH(game->active_colliders, ENT_COMPONENT_ID(ent_ptr, collider));

    ent_ptr = game_entity_create(game, NULL);
    ent_ptr->pos = vec3(0,0,10);
    coll_ptr = ENT_COMPONENT_NEW(game, ent_ptr, collider, NULL);
    game_component_collider_init(coll_ptr, &PG_COLLIDER_AABB(vec3(-32,-32,-4), vec3(32,32,4),
        .flags = PG_COLLIDER_STATIC, .mass = 0));
    coll_ptr->phys.pos = vec3(32,-32,2);
    game_component_collider_update_bound(coll_ptr);
    game_entity_bound_from_collider(game, ent_ptr);
    struct game_component_drawable* draw_ptr = ENT_COMPONENT_NEW(game, ent_ptr, drawable, NULL);
    draw_ptr->draw_fn = game_draw_aabb;
    draw_ptr->scale = vec3(1,1,1);
    ARR_PUSH(game->static_colliders, ENT_COMPONENT_ID(ent_ptr, collider));


    pg_timestepper_step(&game->timer, pg_time());
}

#define COMPONENTS_DEINIT(GAME, NAME) \
    game_component_##NAME##_pool_deinit(&GAME->NAME##_pool); \
    ARR_DEINIT(GAME->NAME##_components);

void gameplay_deinit(struct gameplay_state* game)
{
    game_grid_deinit(game);
    game_entity_pool_deinit(&game->entpool);
    COMPONENTS_DEINIT(game, collider);
    COMPONENTS_DEINIT(game, drawable);
    COMPONENTS_DEINIT(game, heightmap);
    COMPONENTS_DEINIT(game, player);
    COMPONENTS_DEINIT(game, item);
    game_system_physics_deinit(game);
    game_system_render_deinit(game);
    game_system_playercontrol_deinit(game);
    game_system_worldscrolling_deinit(game);
}

#undef COMPONENTS_DEINIT

int gameplay_main(struct gameplay_state* game)
{
    double total_update, playercontrol_time, physics_time, projectiles_time,
        scrolling_time, particles_time, render_time;
    uint64_t start_time = pg_perf_time();
    /*  Manage time */
    int steps = pg_timestepper_step(&game->timer, pg_time());
    int i = steps;
    /*  Manage input    */
    game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PLAYERCONTROL, playercontrol_time);
    /*  Do game ticks   */
    while(i--) {
        ++game->tick;
        pg_ui_context_step(game->ui);
        /*  Step all the game systems and log the time each one takes   */
        total_update = 1000 * PG_PERF_DURATION(
            projectiles_time = 1000 * PG_PERF_DURATION(game_system_projectiles_step(game));
            physics_time = 1000 * PG_PERF_DURATION(game_system_physics_step(game));
            scrolling_time = 1000 * PG_PERF_DURATION(game_system_worldscrolling_step(game));
            particles_time = 1000 * PG_PERF_DURATION(game_system_particles_step(game));
        );
        /*  Add all the durations (and some more performance data) to the debug graphs  */
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PHYSICS, physics_time);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_WORLDSCROLLING, scrolling_time);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PARTICLES, particles_time);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PARTICLES_COUNT, game->particles.all_particles.len);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PROJECTILES, projectiles_time);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PROJECTILES_COUNT, game->projectile_components.len);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PHYSICS_ACTIVE, game->active_colliders.len);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_PHYSICS_CALCS, game->physics.phys_pairs.len - game->physics.phys_pair_lengths.len);
        game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_TOTAL_UPDATE, total_update);
    }
    playercontrol_time = 1000 * PG_PERF_DURATION(game_system_playercontrol_step(game));
    render_time = 1000 * PG_PERF_DURATION(game_system_render_step(game));
    game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_FRAMERATE, game->render.framerate);
    game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_RENDER, render_time);
    /*  Flush input if we did any steps */
    if(steps >= 1) {
        //if(pg_input_kb_check(SDL_SCANCODE_P, PG_CONTROL_HIT)) {
        //    game->debug.selected_graph = (game->debug.selected_graph + 1) % GAME_DEBUG_GRAPHS_N;
        //}
    }
    uint64_t end_time = pg_perf_time();
    game_system_debugtools_add_data(game, GAME_DEBUG_GRAPH_TOTAL_FRAME,
        1000 * pg_perf_time_diff(start_time, end_time));
    /*  Exit if PG user exit    */
    return !game->user_exit;
}

