void game_terrain_init_buffers(struct gameplay_state* game);
void game_terrain_build_mesh(struct pg_heightmap* hmap, struct pg_vertex_buffer_builder* builder);
void game_draw_heightmap(struct gameplay_state* game, struct game_entity* ent, float dt);
