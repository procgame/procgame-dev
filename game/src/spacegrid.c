#include "procgame.h"
#include "game.h"

/************************/
/*  Static funcs        */
/************************/

static inline void get_query_bounds(struct game_grid* grid, vec3 b0, vec3 b1,
                                    ivec3* b0_out, ivec3* b1_out)
{
    b0 = vec3_sub(b0, grid->grid_offset);
    b1 = vec3_sub(b1, grid->grid_offset);
    b0 = vec3_div(b0, grid->cell_size);
    b1 = vec3_div(b1, grid->cell_size);
    *b0_out = ivec3(VEC_XYZ(b0));
    *b1_out = ivec3(VEC_XYZ(b1));
    *b1_out = ivec3_add(*b1_out, ivec3_all(1));
    *b0_out = ivec3_clamp(*b0_out, ivec3(0), ivec3_sub(grid->grid_size, ivec3_all(0)));
    *b1_out = ivec3_clamp(*b1_out, ivec3(0), ivec3_sub(grid->grid_size, ivec3_all(0)));
}

static void add_collider_to_grid(struct game_grid* grid, game_component_collider_arr_t* arr,
        struct game_component_collider* coll_ptr, game_component_collider_id_t coll_id)
{
    ivec3 bound0, bound1;
    get_query_bounds(grid, coll_ptr->current_bound.b0, coll_ptr->current_bound.b1, &bound0, &bound1);
    ivec3 iter;
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(grid->grid_size, iter.x, iter.y, iter.z);
        ARR_PUSH(arr[grid_idx], coll_id);
    }
}

/********************/
/*  BOOKKEEPING     */
/********************/

void game_grid_init(struct gameplay_state* game, ivec3 grid_size, vec3 cell_size)
{
    struct game_grid* grid = &game->grid;
    *grid = (struct game_grid){
        .game = game,
        .grid_size = grid_size,
        .cell_size = cell_size,
        .full_size = vec3_mul(vec3(VEC_XYZ(grid_size)), cell_size),
        .n_cells = grid_size.x*grid_size.y*grid_size.z,
    };
    grid->active_colliders = malloc(sizeof(game_component_collider_arr_t) * grid->n_cells);
    grid->inactive_colliders = malloc(sizeof(game_component_collider_arr_t) * grid->n_cells);
    grid->static_colliders = malloc(sizeof(game_component_collider_arr_t) * grid->n_cells);
    int i, j, k;
    for(i = 0; i < grid_size.x; ++i)
    for(j = 0; j < grid_size.y; ++j)
    for(k = 0; k < grid_size.z; ++k) {
        int grid_idx = GRID_IDX(grid_size, i, j, k);
        ARR_INIT(grid->active_colliders[grid_idx]);
        ARR_INIT(grid->inactive_colliders[grid_idx]);
        ARR_INIT(grid->static_colliders[grid_idx]);
    }
    ARR_INIT(grid->collider_query);

}

void game_grid_deinit(struct gameplay_state* game)
{
    struct game_grid* grid = &game->grid;
    int i, j, k;
    for(i = 0; i < grid->grid_size.x; ++i)
    for(j = 0; j < grid->grid_size.y; ++j)
    for(k = 0; k < grid->grid_size.z; ++k) {
        int grid_idx = GRID_IDX(grid->grid_size, i, j, k);
        ARR_DEINIT(grid->active_colliders[grid_idx]);
        ARR_DEINIT(grid->inactive_colliders[grid_idx]);
        ARR_DEINIT(grid->static_colliders[grid_idx]);
    }
    ARR_DEINIT(grid->collider_query);
    free(grid->active_colliders);
    free(grid->static_colliders);
}


/********************/
/*  SCROLLING       */
/********************/

void game_grid_set_offset(struct gameplay_state* game, vec3 center)
{
    struct game_grid* grid = &game->grid;
    ivec3 cell_idx = ivec3(VEC_XYZ(vec3_floor(vec3_div(center, grid->cell_size))));
    if(ivec3_cmp_eq(cell_idx, grid->cell_idx)) return;
    grid->cell_idx = cell_idx;
    ivec3 bottomleft_idx = ivec3_sub(cell_idx, ivec3(grid->grid_size.x/2, grid->grid_size.y/2));
    vec3 grid_offset = vec3_mul(vec3(VEC_XY(bottomleft_idx)), grid->cell_size);
    game->grid.grid_offset = grid_offset;
    game->grid.bound = AABB3D(grid_offset, vec3_add(grid_offset, grid->full_size));
    game_grid_rebuild_active_colliders(game);
    game_grid_rebuild_inactive_colliders(game);
    game_grid_rebuild_static_colliders(game);
}

/************************/
/*  COLLIDER QUERYING   */
/************************/
void game_grid_new_collider_query(struct gameplay_state* game)
{
    ARR_TRUNCATE(game->grid.collider_query, 0);
    game->grid.collider_ray_hit = 0;
    game->grid.collider_ray_len = -1;
    ++game->grid.query_no;
}

void game_grid_add_inactive_collider(struct gameplay_state* game,
        struct game_component_collider* coll_ptr, game_component_collider_id_t coll_id)
{
    add_collider_to_grid(&game->grid, game->grid.inactive_colliders, coll_ptr, coll_id);
}

void game_grid_rebuild_active_colliders(struct gameplay_state* game)
{
    struct game_grid* grid = &game->grid;
    int i, j, k;
    for(i = 0; i < grid->grid_size.x; ++i)
    for(j = 0; j < grid->grid_size.y; ++j)
    for(k = 0; k < grid->grid_size.z; ++k) {
        int grid_idx = GRID_IDX(grid->grid_size, i, j, k);
        ARR_TRUNCATE(grid->active_colliders[grid_idx], 0);
    }
    struct game_component_collider* coll_ptr;
    game_component_collider_id_t coll_id;
    ARR_FOREACH_REV(game->active_colliders, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) {
            ARR_SWAPSPLICE(game->active_colliders, i, 1);
            continue;
        }
        add_collider_to_grid(grid, grid->active_colliders, coll_ptr, coll_id);
    }
}

void game_grid_rebuild_inactive_colliders(struct gameplay_state* game)
{
    struct game_grid* grid = &game->grid;
    int i, j, k;
    for(i = 0; i < grid->grid_size.x; ++i)
    for(j = 0; j < grid->grid_size.y; ++j)
    for(k = 0; k < grid->grid_size.z; ++k) {
        int grid_idx = GRID_IDX(grid->grid_size, i, j, k);
        ARR_TRUNCATE(grid->inactive_colliders[grid_idx], 0);
    }
    struct game_component_collider* coll_ptr;
    game_component_collider_id_t coll_id;
    ARR_FOREACH_REV(game->inactive_colliders, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id)) || coll_ptr->active) {
            ARR_SWAPSPLICE(game->inactive_colliders, i, 1);
            continue;
        }
        add_collider_to_grid(grid, grid->inactive_colliders, coll_ptr, coll_id);
    }
}

void game_grid_rebuild_static_colliders(struct gameplay_state* game)
{
    struct game_grid* grid = &game->grid;
    int i, j, k;
    for(i = 0; i < grid->grid_size.x; ++i)
    for(j = 0; j < grid->grid_size.y; ++j)
    for(k = 0; k < grid->grid_size.z; ++k) {
        int grid_idx = GRID_IDX(grid->grid_size, i, j, k);
        ARR_TRUNCATE(grid->static_colliders[grid_idx], 0);
    }
    struct game_component_collider* coll_ptr;
    game_component_collider_id_t coll_id;
    ARR_FOREACH_REV(game->static_colliders, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) {
            ARR_SWAPSPLICE(game->static_colliders, i, 1);
            continue;
        }
        add_collider_to_grid(grid, grid->static_colliders, coll_ptr, coll_id);
    }
}

static int query_collider_cell(struct gameplay_state* game,
        game_component_collider_arr_t* arr, aabb3D bound)
{
    int n_found = 0;
    int i;
    game_component_collider_id_t coll_id;
    struct game_component_collider* coll_ptr;
    ARR_FOREACH(*arr, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))) continue;
        if(coll_ptr->query_no == game->grid.query_no) continue;
        coll_ptr->query_no = game->grid.query_no;
        if(aabb3D_intersects(&coll_ptr->current_bound, &bound)) {
            ARR_PUSH(game->grid.collider_query, coll_id);
            ++n_found;
        }
    }
    return n_found;
}

int game_query_active_colliders(struct gameplay_state* game, aabb3D bound)
{
    int n_found = 0;
    ivec3 bound0, bound1, iter;
    get_query_bounds(&game->grid, bound.b0, bound.b1, &bound0, &bound1);
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        n_found += query_collider_cell(game, &game->grid.active_colliders[grid_idx], bound);
    }
    return n_found;
}

int game_query_inactive_colliders(struct gameplay_state* game, aabb3D bound)
{
    int n_found = 0;
    ivec3 bound0, bound1, iter;
    get_query_bounds(&game->grid, bound.b0, bound.b1, &bound0, &bound1);
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        n_found += query_collider_cell(game, &game->grid.inactive_colliders[grid_idx], bound);
    }
    return n_found;
}

int game_query_static_colliders(struct gameplay_state* game, aabb3D bound)
{
    int n_found = 0;
    ivec3 bound0, bound1, iter;
    get_query_bounds(&game->grid, bound.b0, bound.b1, &bound0, &bound1);
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        n_found += query_collider_cell(game, &game->grid.static_colliders[grid_idx], bound);
    }
    return n_found;
}

int game_query_all_colliders(struct gameplay_state* game, aabb3D bound)
{
    int n_found = 0;
    ivec3 bound0, bound1, iter;
    get_query_bounds(&game->grid, bound.b0, bound.b1, &bound0, &bound1);
    for(iter.x = bound0.x; iter.x != bound1.x; ++iter.x)
    for(iter.y = bound0.y; iter.y != bound1.y; ++iter.y)
    for(iter.z = bound0.z; iter.z != bound1.z; ++iter.z) {
        int grid_idx = GRID_IDX(game->grid.grid_size, iter.x, iter.y, iter.z);
        n_found += query_collider_cell(game, &game->grid.active_colliders[grid_idx], bound);
        n_found += query_collider_cell(game, &game->grid.inactive_colliders[grid_idx], bound);
        n_found += query_collider_cell(game, &game->grid.static_colliders[grid_idx], bound);
    }
    return n_found;
}




static float debug_raycast_heightmap(struct gameplay_state* game, struct pg_collider* coll_a, vec3 src, vec3 dir, float max_len)
{
    vec3 start3 = vec3_sub(src, coll_a->pos);
    vec3 end3 = vec3_add(start3, vec3_scale(dir, max_len));
    vec2 start = vec2(VEC_XY(start3));
    vec2 end = vec2(VEC_XY(end3));
    vec2 grid_dims = vec2(VEC_XY(coll_a->hmap.map_size));
    //grid_dims = vec2_sub(grid_dims, vec2_all(3));
    float cell_size = coll_a->hmap.cell_size;
    aabb2D bound = AABB2D(vec2(0), vec2_mul(vec2_all(cell_size), vec2_sub(grid_dims, vec2_all(1))));
    struct raycast_grid2D_state rc;
    if(!start_raycast_grid2D(start, end, bound, vec2_all(cell_size), &rc))
        return -1;
    /*  Convenience variables for the raycast loop  */
    int cells_x = coll_a->hmap.map_size.x;
    float* hmap = coll_a->hmap.map;
    ray3D src_ray = RAY3D(start3, dir);
    /*  Do The Thing    */
    do {
        ivec2 iter = ivec2_add(rc.iter, ivec2(1,1));
        /*  Get the height values at the four points of this cell   */
        float h[4] = {
            hmap[rc.iter.x + rc.iter.y * cells_x],
            hmap[(rc.iter.x+1) + rc.iter.y * cells_x],
            hmap[rc.iter.x + (rc.iter.y+1) * cells_x],
            hmap[(rc.iter.x+1) + (rc.iter.y+1) * cells_x] };
        if(h[0] < 0 || h[1] < 0 || h[2] < 0 || h[3] < 0) continue;
        /*  Calculate four vertices */
        vec3 tri[4] = {
            vec3((rc.iter.x) * cell_size, (rc.iter.y) * cell_size, h[0]),
            vec3((rc.iter.x+1) * cell_size, (rc.iter.y) * cell_size, h[1]),
            vec3((rc.iter.x) * cell_size, (rc.iter.y+1) * cell_size, h[2]),
            vec3((rc.iter.x+1) * cell_size, (rc.iter.y+1) * cell_size, h[3]), };
        /*  Check for ray hits on both triangles    */
        float ray_len = raycast_triangle(&src_ray, &TRIANGLE(tri[0], tri[1], tri[2]));
        const float wireframe_width = 0.25;
        game_system_debugtools_add_line(game, false,
            vec3_add(tri[0], game->debug_hmap_offset),
            vec3_add(tri[1], game->debug_hmap_offset),
            wireframe_width,
            game->debug_hmap_color);
        game_system_debugtools_add_line(game, false,
            vec3_add(tri[1], game->debug_hmap_offset),
            vec3_add(tri[2], game->debug_hmap_offset),
            wireframe_width,
            game->debug_hmap_color);
        game_system_debugtools_add_line(game, false,
            vec3_add(tri[2], game->debug_hmap_offset),
            vec3_add(tri[0], game->debug_hmap_offset),
            wireframe_width,
            game->debug_hmap_color);
        game_system_debugtools_add_line(game, false,
            vec3_add(tri[1], game->debug_hmap_offset),
            vec3_add(tri[3], game->debug_hmap_offset),
            wireframe_width,
            game->debug_hmap_color);
        game_system_debugtools_add_line(game, false,
            vec3_add(tri[3], game->debug_hmap_offset),
            vec3_add(tri[2], game->debug_hmap_offset),
            wireframe_width,
            game->debug_hmap_color);
        if(ray_len >= 0) return ray_len;
        ray_len = raycast_triangle(&src_ray, &TRIANGLE(tri[2], tri[1], tri[3]));
        if(ray_len >= 0) return ray_len;
    } while(step_raycast_grid2D(&rc));
    return -1;
}





static inline void raycast_collider_cell(struct gameplay_state* game,
    game_component_collider_arr_t* arr, ray3D ray, float max_len)
{
    float nearest_len = max_len;
    game_component_collider_id_t nearest_id = 0;
    int i;
    game_component_collider_id_t coll_id;
    struct game_component_collider* coll_ptr;
    ARR_FOREACH(*arr, coll_id, i) {
        if(!(coll_ptr = GAME_COMPONENT_PTR(game, collider, coll_id))
        || coll_ptr->query_no == game->grid.query_no
        || coll_ptr->phys.mass < 0) continue;
        struct game_entity* ent_ptr = GAME_ENTITY_PTR(game, coll_ptr->ent_id);
        ubvec4 debug_hmap_color = ubvec4(255, RANDI(255), RANDI(255), RANDI(255));
        game->debug_hmap_color = *((uint32_t*)(&debug_hmap_color));
        game->debug_hmap_offset = coll_ptr->phys.pos;
        coll_ptr->query_no = game->grid.query_no;
        float ray_len = pg_collider_raycast(&coll_ptr->phys, ray.src, ray.dir, max_len);
        if(game->debug_hmap_collision && coll_ptr->phys.type == PG_COLLIDER_HEIGHTMAP) {
            debug_raycast_heightmap(game, &coll_ptr->phys, ray.src, ray.dir, max_len);
        }
        if(ray_len < 0 || ray_len > max_len) continue;
        ARR_PUSH(game->grid.collider_query, coll_id);
        if(ray_len < nearest_len) {
            nearest_len = ray_len;
            nearest_id = coll_id;
        }
    }
    if(nearest_id && nearest_len < game->grid.collider_ray_len) {
        game->grid.collider_ray_len = nearest_len;
        game->grid.collider_ray_hit = nearest_id;
    }
}

float game_raycast_active_colliders(struct gameplay_state* game, ray3D ray, float max_len)
{
    /*  Setup the basic raycast variables   */
    struct game_grid* grid = &game->grid;
    grid->collider_ray_len = max_len;
    grid->collider_ray_hit = -1;
    struct raycast_grid3D_state raycast;
    vec3 start = ray.src;
    vec3 end = vec3_add(ray.src, vec3_scale(ray.dir, max_len));
    if(!start_raycast_grid3D(start, end, grid->bound, grid->cell_size, &raycast)) return -1;
    /*  Iterate through all the cells the ray intersects   */
    do {
        int grid_idx = GRID_IDX(grid->grid_size, raycast.iter.x, raycast.iter.y, raycast.iter.z);
        raycast_collider_cell(game, &grid->active_colliders[grid_idx], ray, max_len);
        if(grid->collider_ray_hit != -1) {
            float grid_ray_len = vec3_len2(raycast.ray_end);
            float ray_len_ratio = grid_ray_len / grid->collider_ray_len;
            if(ray_len_ratio > 0.5f) break;
        }
    } while(step_raycast_grid3D(&raycast));
    return game->grid.collider_ray_len;
}

float game_raycast_inactive_colliders(struct gameplay_state* game, ray3D ray, float max_len)
{
    /*  Setup the basic raycast variables   */
    struct game_grid* grid = &game->grid;
    grid->collider_ray_len = max_len;
    grid->collider_ray_hit = -1;
    struct raycast_grid3D_state raycast;
    vec3 start = ray.src;
    vec3 end = vec3_add(ray.src, vec3_scale(ray.dir, max_len));
    if(!start_raycast_grid3D(start, end, grid->bound, grid->cell_size, &raycast)) return -1;
    /*  Iterate through all the cells the ray intersects   */
    do {
        int grid_idx = GRID_IDX(grid->grid_size, raycast.iter.x, raycast.iter.y, raycast.iter.z);
        raycast_collider_cell(game, &grid->inactive_colliders[grid_idx], ray, max_len);
        if(grid->collider_ray_hit != -1) {
            float grid_ray_len = vec3_len2(raycast.ray_end);
            float ray_len_ratio = grid_ray_len / grid->collider_ray_len;
            if(ray_len_ratio > 0.5f) break;
        }
    } while(step_raycast_grid3D(&raycast));
    return game->grid.collider_ray_len;
}

float game_raycast_static_colliders(struct gameplay_state* game, ray3D ray, float max_len)
{
    /*  Setup the basic raycast variables   */
    struct game_grid* grid = &game->grid;
    grid->collider_ray_len = max_len;
    grid->collider_ray_hit = -1;
    struct raycast_grid3D_state raycast;
    vec3 start = ray.src;
    vec3 end = vec3_add(ray.src, vec3_scale(ray.dir, max_len));
    if(!start_raycast_grid3D(start, end, grid->bound, grid->cell_size, &raycast)) return -1;
    /*  Iterate through all the cells the ray intersects   */
    do {
        int grid_idx = GRID_IDX(grid->grid_size, raycast.iter.x, raycast.iter.y, raycast.iter.z);
        raycast_collider_cell(game, &grid->static_colliders[grid_idx], ray, max_len);
        if(grid->collider_ray_hit != -1) {
            float grid_ray_len = vec3_len2(raycast.ray_end);
            float ray_len_ratio = grid_ray_len / (grid->collider_ray_len * grid->collider_ray_len);
            if(ray_len_ratio > 0.5f) break;
        }
    } while(step_raycast_grid3D(&raycast));
    return game->grid.collider_ray_len;
}

float game_raycast_all_colliders(struct gameplay_state* game, ray3D ray, float max_len)
{
    /*  Setup the basic raycast variables   */
    struct game_grid* grid = &game->grid;
    grid->collider_ray_len = max_len;
    grid->collider_ray_hit = -1;
    struct raycast_grid3D_state raycast;
    vec3 start = ray.src;
    vec3 end = vec3_add(ray.src, vec3_scale(ray.dir, max_len));
    if(!start_raycast_grid3D(start, end, grid->bound, grid->cell_size, &raycast)) return -1;
    /*  Iterate through all the cells the ray intersects   */
    do {
        int grid_idx = GRID_IDX(grid->grid_size, raycast.iter.x, raycast.iter.y, raycast.iter.z);
        raycast_collider_cell(game, &grid->static_colliders[grid_idx], ray, max_len);
        raycast_collider_cell(game, &grid->active_colliders[grid_idx], ray, max_len);
        raycast_collider_cell(game, &grid->inactive_colliders[grid_idx], ray, max_len);
        if(grid->collider_ray_hit != -1) {
            float grid_ray_len = vec3_len2(raycast.ray_end);
            float ray_len_ratio = grid_ray_len / grid->collider_ray_len;
            if(ray_len_ratio > 0.5f) break;
        }
    } while(step_raycast_grid3D(&raycast));
    return game->grid.collider_ray_len;
}

vec3 game_query_deepest_collision(struct gameplay_state* game,
        vec3 pos, float probe_radius, game_entity_id_t* out)
{
    /*  Find the collider with the deepest collision to hit */
    aabb3D bound = AABB3D(vec3_sub(pos, vec3_all(probe_radius)),
                          vec3_add(pos, vec3_all(probe_radius)));
    game_query_all_colliders(game, bound);
    vec3 nearest_sep = vec3(0);
    float nearest_dist = 0;
    game_entity_id_t nearest_ent_id = 0;
    if(game->grid.collider_query.len) {
        float nearest_dist = 0;
        struct pg_collider sph_coll = PG_COLLIDER_SPHERE(pos, probe_radius);
        int j;
        game_component_collider_id_t query_coll_id;
        struct game_component_collider* query_coll_ptr;
        struct game_entity* query_ent_ptr;
        ARR_FOREACH(game->grid.collider_query, query_coll_id, j) {
            if(!(query_coll_ptr = GAME_COMPONENT_PTR(game, collider, query_coll_id))) continue;
            if(!aabb3D_intersects(&bound, &query_coll_ptr->current_bound)) continue;
            vec3 sep = pg_collider_sep_axis(&sph_coll, &query_coll_ptr->phys);
            float len2 = vec3_len2(sep);
            if(len2 > nearest_dist) {
                nearest_dist = len2;
                nearest_sep = sep;
                nearest_ent_id = query_coll_ptr->ent_id;
            }
        }
    }
    if(out) *out = nearest_ent_id;
    return nearest_sep;
}

/********************/
/*  DEBUG           */

void game_grid_draw_cells(struct gameplay_state* game)
{
    struct game_grid* grid = &game->grid;
    int i, j, k;
    for(i = 0; i < grid->grid_size.x+1; ++i)
    for(j = 0; j < grid->grid_size.y+1; ++j)
    for(k = 0; k < grid->grid_size.z+1; ++k) {
        vec3 cell_corner_pos = 
            vec3_add(vec3_mul(vec3(i, j, k), grid->cell_size), grid->grid_offset);
        vec3 grid_size = grid->cell_size;
        game_system_debugtools_add_aabb3D(game, true, cell_corner_pos, grid_size, 0.2, 0xFFFFFF11);
        vec3 cell_center_pos = 
            vec3_add(cell_corner_pos, vec3_scale(grid->cell_size, 0.5));
        game_system_debugtools_add_quad(game, true,
            cell_center_pos,
            quat_identity(),
            vec2(0.5,0.5),
            0xFFFFFF44);
    }

        
}
