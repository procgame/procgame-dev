#define GAME_PROTOTYPE_NAME_MAXLEN   128

typedef int game_entity_prototype_id_t;
typedef int game_particle_prototype_id_t;

enum game_entity_trait {
    GAME_ENTITY_MOVES,
    GAME_ENTITY_N_TRAITS,
};

enum game_entity_component {
    GAME_ENTITY_COMPONENT_COLLIDER,
    GAME_ENTITY_COMPONENT_DRAWABLE,
    GAME_ENTITY_COMPONENT_PLAYER,
    GAME_ENTITY_COMPONENT_HEIGHTMAP,
    GAME_ENTITY_COMPONENT_ITEM,
    GAME_ENTITY_COMPONENT_PARTICLE_EMITTER,
    GAME_ENTITY_COMPONENT_INVENTORY,
    GAME_ENTITY_COMPONENT_PROJECTILE,
    GAME_ENTITY_N_COMPONENTS,
};

struct game_entity {
    /*  Bookkeeping */
    uint64_t alloc;
    int refcount;
    int dead;
    game_entity_prototype_id_t proto_id;
    /*  All entities have these things  */
    vec3 pos, vel;
    quat rot;
    vec3 facing;
    /*  Components  */
    uint64_t components[GAME_ENTITY_N_COMPONENTS];
    bool components_enabled[GAME_ENTITY_N_COMPONENTS];
    bool traits[GAME_ENTITY_N_TRAITS];
};

PG_MEMPOOL_DECLARE(struct game_entity, game_entity);
typedef ARR_T(game_entity_id_t) game_entity_arr_t;

struct game_entity* game_entity_create(struct gameplay_state* game, game_entity_id_t* id_out);
void game_entity_destroy(struct gameplay_state* game, struct game_entity* ent);
void game_entity_update_aabb(struct game_entity* ent, vec3 pos);
void game_entity_set_trait(struct gameplay_state* game, struct game_entity* ent_ptr,
                           enum game_entity_trait trait, bool set);


/*****************/
/*  COMPONENTS   */
/*****************/

/*  Get pointer or ID of an entity's component, by name     */
#define ENT_COMPONENT_ID(ENT, NAME) \
    ((ENT)->components[game_component_##NAME##_idx])

#define ENT_COMPONENT_PTR(GAME, ENT, NAME) \
    game_component_##NAME##_get(&GAME->NAME##_pool, ENT_COMPONENT_ID(ENT, NAME))


/*  Create a new component on an entity     */
#define ENT_COMPONENT_NEW(GAME, ENT, NAME, ID_OUT) ({ \
    struct game_component_##NAME* ptr; \
    game_component_##NAME##_id_t id; \
    if(ENT->components[game_component_##NAME##_idx]) { \
        ptr = ENT_COMPONENT_PTR(GAME, ENT, NAME); \
        id = ENT->components[game_component_##NAME##_idx]; \
    } else { \
        id = game_component_##NAME##_alloc(&GAME->NAME##_pool, &ptr); \
        ENT->components[game_component_##NAME##_idx] = id; \
        ARR_PUSH(GAME->NAME##_components, id); \
    } \
    ptr->ent_id = game_entity_id(&GAME->entpool, ENT); \
    ptr->enabled = true; \
    ENT->components_enabled[game_component_##NAME##_idx] = true; \
    game_component_##NAME##_id_t* out = ID_OUT; \
    if(out) *out = id; ptr; })


/*  Enable/disable components, through their entity     */
#define ENT_COMPONENT_ENABLE(GAME, ENT, NAME, ENABLED) \
    do { \
        struct game_component_##NAME* ptr = \
            GAME_COMPONENT_PTR(GAME, NAME, ENT_COMPONENT_ID(ENT, NAME)); \
        if(ptr) { \
            ptr->enabled = ENABLED; \
            ENT->components_enabled[game_component_##NAME##_idx] = ENABLED; \
        } \
    } while(0)

/*  Enable/disable components, through the component    */
#define GAME_COMPONENT_ENABLE(GAME, COMP, NAME, ENABLED) \
    do { \
        struct game_entity* ent_ptr = GAME_ENTITY_PTR(GAME, COMP->ent_id); \
        if(ent_ptr) { \
            COMP->enabled = ENABLED; \
            ent_ptr->components_enabled[game_component_##NAME##_idx] = ENABLED; \
        } \
    } while(0)

/*  Check if an entity's component is enabled   */
#define ENT_COMPONENT_IS_ENABLED(ENT, NAME) \
    (ENT->components_enabled[game_component_##NAME##_idx])


/*  Defining a component    */
#define GAME_COMPONENT(NAME, INDEX, ...) \
    static const int game_component_##NAME##_idx = INDEX; \
    struct game_component_##NAME { \
        uint64_t alloc; game_entity_id_t ent_id; int ent_dead; bool enabled; __VA_ARGS__ }; \
    PG_MEMPOOL_DECLARE(struct game_component_##NAME, game_component_##NAME); \
    typedef ARR_T(game_component_##NAME##_id_t) game_component_##NAME##_arr_t;

#define GAME_COMPONENT_IMPL(NAME) \
    PG_MEMPOOL_DEFINE(struct game_component_##NAME, game_component_##NAME, alloc);


/*  Component definitions   */
#include "ent_components/collider.h"
#include "ent_components/drawable.h"
#include "ent_components/player.h"
#include "ent_components/heightmap.h"
#include "ent_components/item.h"
#include "ent_components/particle_emitter.h"
#include "ent_components/inventory.h"
#include "ent_components/projectile.h"



