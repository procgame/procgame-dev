#include <stdio.h>
#include <time.h>
#include <procgame.h>

#include "game.h"

#include "handle_crash.h"

int main(int argc, char *argv[])
{
    printf("Procgame Launch\n");
    for(int i = 0; i < argc; ++i) printf("%s ", argv[i]);
    printf("\n");

    program_name = argv[0];
    set_signal_handler();

    pg_init_core();

    bool vr_enabled = false;
    bool vr_fake = false;
    for(int arg_idx = 0; arg_idx < argc; ++arg_idx) {
        if(strcmp("--enable-vr", argv[arg_idx]) == 0) {
            pg_log(PG_LOG_INFO, "VR enabled");
            vr_enabled = true;
        } else if(strcmp("--fake-vr", argv[arg_idx]) == 0) {
            pg_log(PG_LOG_INFO, "Fake VR enabled");
            vr_enabled = true;
            vr_fake = true;
        }
    }

    /*  Load the resolution from a config file (default to 640x480) */
    ivec2 resolution = ivec2(640,480);
    cJSON* config_json = load_json("./data/config.json");
    cJSON* resolution_json = cJSON_GetObjectItem(config_json, "resolution");
    if(resolution_json) resolution = json_read_ivec2(resolution_json);
    cJSON_Delete(config_json);

    /*  Open window and init GPU with the configured resolution */
    pg_init_window(resolution);
    pg_init_gpu();
    if(vr_enabled) {
        if(vr_fake) pg_vr_init_fake();
        else pg_vr_init();
        pg_vr_devices_init();
        pg_vr_display_init(vec2(1,1));
        pg_vr_set_action_manifest("./data/vr_actions.json");
    }

    /*  Init top-level game assets  */
    struct game_resources res;
    if(!game_resources_init(&res)) {
        pg_log(PG_LOG_ERROR, "Failed to initialized game resources");
    } else {
        /*  Do gameplay loop    */
        struct gameplay_state gameplay;
        gameplay_init(&gameplay, &res);
        while(gameplay_main(&gameplay)) {
            //pg_window_swap();
        }
        gameplay_deinit(&gameplay);

        /*  Deinit top-level game assets    */
        game_resources_deinit(&res);
    }

    /*  Deinit procgame */
    if(vr_enabled) pg_vr_deinit();
    pg_deinit_window();
    pg_deinit_all();

    return 0;
}
