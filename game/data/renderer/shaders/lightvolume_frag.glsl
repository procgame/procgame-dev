#version 450

layout(location = 3) uniform vec3 eye_pos;
layout(location = 4) uniform vec3 eye_dir;
layout(location = 5) uniform vec2 depth_planes;

layout(location = 6, binding = 1) uniform sampler2DArray g_normal;
layout(location = 7, binding = 2) uniform sampler2DArray g_depth;

layout(location = 0) in vec4 pos_projected;
layout(location = 1) in vec3 view_ray;
layout(location = 2) flat in vec4 f_light;
layout(location = 3) flat in vec3 f_color;

layout(location = 0) out vec4 frag_color;

void main()
{
    /*  Sample textures     */
    vec2 g_tex_coord2 = (pos_projected.xy / pos_projected.w) * 0.5 + 0.5;
    vec3 g_tex_coord = vec3(g_tex_coord2.xy, 0);
    float depth_sample = texture(g_depth, g_tex_coord).x;
    vec4 norm_sample = texture(g_normal, g_tex_coord);
    norm_sample.xyz = norm_sample.xyz * 2 - 1;
    /*  Reconstructing world-space position from the depth buffer   */
    float lin_depth = (depth_planes[1] / (depth_planes[0] - depth_sample));
    vec3 view_ray = normalize(view_ray);
    float view_ray_z_axis = -dot(eye_dir, view_ray);
    vec3 eye_to_pos = view_ray * (lin_depth / view_ray_z_axis);
    vec3 pos_world = eye_pos + eye_to_pos;
    vec3 light_to_pos = pos_world - f_light.xyz;
    if(length(light_to_pos) > f_light.w) discard;
    /*  Get distance from light source as a ratio of the light's radius */
    float attenuation = 1 - min(1, length(light_to_pos) / f_light.w);
    attenuation = pow(attenuation, 2);
    /*  Get surface shading for light and surface normal    */
    float shade = dot(-normalize(light_to_pos), norm_sample.xyz);
    float lit_intensity = attenuation * shade;
    frag_color = vec4((light_to_pos * attenuation).xyz, 1);
    frag_color = vec4((f_color * lit_intensity).xyz, 1);
}
