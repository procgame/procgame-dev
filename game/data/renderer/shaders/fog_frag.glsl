#version 450

layout(location = 0) uniform vec3 fog_color;
layout(location = 1) uniform vec2 fog_plane;
layout(location = 2) uniform vec2 depth_plane;

layout(location = 3, binding = 0) uniform sampler2DArray gbuffer_depth;

layout(location = 0) in vec2 f_tex_coord;

layout(location = 0) out vec4 frag_color;

void main()
{
    float z_b = texture(gbuffer_depth, vec3(f_tex_coord, 0)).r;
    float z_n = z_b;
    /*  What    */
    float depth = 2 * depth_plane.y * depth_plane.x / (depth_plane.y + depth_plane.x - (depth_plane.y - depth_plane.x) * z_n);
    float fog_depth = (depth - fog_plane.x) / (fog_plane.y - fog_plane.x);
    fog_depth = clamp(fog_depth, 0, 1);
    fog_depth = 1-pow(1-fog_depth, 2);
    frag_color = vec4(fog_color.rgb, fog_depth);
}

