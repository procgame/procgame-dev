#version 450

layout(location = 3) uniform ivec3 tex_layers;
layout(location = 4, binding = 0) uniform sampler2DArray terrain_textures;

layout(location = 0) in vec3 f_normal;
layout(location = 1) in vec3 f_tex_select;
layout(location = 2) in vec2 f_tex_coord;

layout(location = 0) out vec4 frag_color;
layout(location = 1) out vec4 frag_normal;

const vec3 sun_dir = vec3(-1,-1,1);

void main()
{
    vec2 sample_tex = gl_FragCoord.xy;
    vec3 tex_a = texture(terrain_textures, vec3(f_tex_coord.xy, tex_layers[0])).rgb;
    vec3 tex_b = texture(terrain_textures, vec3(f_tex_coord.xy, tex_layers[1])).rgb;
    vec3 tex_c = texture(terrain_textures, vec3(f_tex_coord.xy, tex_layers[2])).rgb;
    vec3 tex_final = (tex_a * f_tex_select.x) +
                     (tex_b * f_tex_select.y) +
                     (tex_c * f_tex_select.z);
    frag_color = vec4(tex_final, 1);
    frag_normal = vec4(f_normal * 0.5 + 0.5, 0);
}




