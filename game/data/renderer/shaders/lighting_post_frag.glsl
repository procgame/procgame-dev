#version 450

layout(location = 0) uniform vec3 ambient_color[3];
layout(location = 3) uniform vec3 ambient_dir[3];

layout(location = 7, binding = 0) uniform sampler2DArray norm;
layout(location = 8, binding = 1) uniform sampler2DArray color;
layout(location = 9, binding = 2) uniform sampler2DArray light;

layout(location = 0) in vec2 f_tex_coord;

layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 norm_spec = texture(norm, vec3(f_tex_coord.xy,0));
    norm_spec.xyz = norm_spec.xyz * 2 - 1;
    vec3 light_sample = texture(light, vec3(f_tex_coord.xy,0)).xyz;
    vec3 color_sample = texture(color, vec3(f_tex_coord.xy,0)).xyz;
    vec3 acc = vec3(0,0,0);
    /*  If sample is fullbright, start with raw color input */
    if(norm_spec.w == 1.0) acc = color_sample.xyz;
    /*  Add ambient lighting to the sampled light value */
    float ambient0 = max(0, -dot(ambient_dir[0], norm_spec.xyz));
    float ambient1 = max(0, -dot(ambient_dir[1], norm_spec.xyz));
    light_sample += ambient0 * ambient_color[0];
    light_sample += ambient1 * ambient_color[1];
    light_sample += ambient_color[2];
    acc += color_sample * light_sample;
    frag_color = vec4((acc + (color_sample * light_sample)).xyz,1);
    //frag_color = vec4(norm_spec.xyz, 1);
}

