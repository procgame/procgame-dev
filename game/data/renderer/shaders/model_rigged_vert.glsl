#version 450

uniform mat4 pg_matrix_mvp;
in vec3 v_position;

//!pg_variant VERTEX_SKINNING
uniform mat4 bone_mats[32];
in vec3 v_rigging;
//!pg_end_variant

//!pg_variant VERTEX_NORMALS
uniform mat4 pg_matrix_model;
in vec3 v_normal;
out vec3 f_normal;
//!pg_end_variant

//!pg_variant VERTEX_UVS
in vec3 v_tex_coord;
out vec3 f_tex_coord;
//!pg_end_variant

//!pg_variant VERTEX_TANSPACE
//!pg_end_variant

void main()
{
    vec3 output_pos = v_position;

    //!pg_variant VERTEX_UVS
    f_tex_coord = v_tex_coord;
    //!pg_end_variant

    //!pg_variant VERTEX_NORMALS
    mat3 norm_mat = mat3(pg_matrix_model);
    vec3 output_normal = norm_mat * v_normal;
    //!pg_end_variant

    //!pg_variant VERTEX_SKINNING
    mat3 bone_norm_mat0 = mat3(bone_mats[int(v_rigging.x)]);
    mat3 bone_norm_mat1 = mat3(bone_mats[int(v_rigging.y)]);
    vec4 skinned_pos = 
        ((bone_mats[int(v_rigging.x)] * v_rigging.z) * vec4(v_position.xyz, 1)) +
        ((bone_mats[int(v_rigging.y)] * (1 - v_rigging.z)) * vec4(v_position.xyz, 1));
    output_pos = skinned_pos.xyz;
        //!pg_variant VERTEX_NORMALS
        vec3 skinned_normal =
            ((bone_norm_mat0 * v_rigging.z) * v_normal) +
            ((bone_norm_mat1 * (1 - v_rigging.z)) * v_normal);
        output_normal = norm_mat * skinned_normal;
        //!pg_end_variant
    //!pg_end_variant

    //!pg_variant VERTEX_NORMALS
    f_normal = normalize(output_normal);
    //!pg_end_variant

    gl_Position = pg_matrix_mvp * vec4(output_pos.xyz, 1);
}
