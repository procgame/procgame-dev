#version 450

layout(location = 0) uniform mat4 projview_matrix;
layout(location = 1) uniform mat4 model_matrix;

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec3 v_tangent;
layout(location = 3) in vec3 v_bitangent;
layout(location = 4) in vec4 v_tex_coord;

layout(location = 0) out vec4 f_tex_coord;
layout(location = 1) out vec3 f_normal;
layout(location = 2) out vec3 f_tangent;
layout(location = 3) out vec3 f_bitangent;

void main()
{
    mat4 mvp_matrix = (projview_matrix * model_matrix);
    f_tex_coord = v_tex_coord;
    f_normal = normalize(mat3(model_matrix) * v_normal);
    f_tangent = normalize(mat3(model_matrix) * v_tangent);
    f_bitangent = normalize(mat3(model_matrix) * v_bitangent);
    gl_Position = mvp_matrix * vec4(v_position.xyz, 1);
}
