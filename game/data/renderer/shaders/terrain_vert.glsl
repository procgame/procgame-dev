#version 450

layout(location = 0) uniform mat4 model_matrix;
layout(location = 1) uniform mat4 projview_matrix;
layout(location = 2) uniform vec2 tex_scale;

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec3 v_tex_select;

layout(location = 0) out vec3 f_normal;
layout(location = 1) out vec3 f_tex_select;
layout(location = 2) out vec2 f_tex_coord;

void main()
{
    f_tex_select = v_tex_select;
    f_tex_coord = v_position.xy * tex_scale;
    f_normal = v_normal;
    gl_Position = (projview_matrix * model_matrix) * vec4(v_position.xyz, 1);
}

