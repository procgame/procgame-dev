#version 450

#define VOLUME_SIZE 1.1

const vec3 light_verts[36] = vec3[](
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE, -VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3(-VOLUME_SIZE,  VOLUME_SIZE,  VOLUME_SIZE),
    vec3( VOLUME_SIZE, -VOLUME_SIZE,  VOLUME_SIZE));

layout(location = 0) uniform int light_buffer_base;
layout(location = 1, binding = 0) uniform samplerBuffer lights;
layout(location = 2) uniform mat4 pg_matrix_projview;
layout(location = 3) uniform vec3 eye_pos;

layout(location = 0) out vec4 pos_projected;
layout(location = 1) out vec3 view_ray;
layout(location = 2) flat out vec4 f_light;
layout(location = 3) flat out vec3 f_color;

void main()
{
    /*  Read light definition from buffer   */
    int buffer_offset = light_buffer_base + ((gl_VertexID / 36) * 2);
    f_light = vec4(texelFetch(lights, buffer_offset));
    f_color = vec3(texelFetch(lights, buffer_offset + 1).xyz);
    /*  Transform cube volume to light radius + position    */
    vec3 pos_world = (light_verts[gl_VertexID % 36] * f_light.w) + f_light.xyz;
    pos_projected = pg_matrix_projview * vec4(pos_world, 1.0);
    /*  Vector from eye to edge of the cube volume  */
    view_ray = pos_world - eye_pos;
    gl_Position = pos_projected;
}

