#version 450

layout(location = 2) uniform float alpha_cutoff;
layout(location = 3, binding = 0) uniform sampler2DArray tex;

layout(location = 0) in vec4 f_tex_coord;
layout(location = 1) in vec3 f_normal;
layout(location = 2) in vec3 f_tangent;
layout(location = 3) in vec3 f_bitangent;

layout(location = 0) out vec4 output_color;
layout(location = 1) out vec4 output_normal;

void main()
{
    vec4 frag_color = vec4(0,0,0,0);
    vec3 frag_normal = f_normal;
    vec4 tex_color = texture(tex, f_tex_coord.xyz);
    frag_color = tex_color;

    vec4 tex_tanspace_normal = texture(tex, vec3(f_tex_coord.xy, f_tex_coord.z + 1));
    tex_tanspace_normal.xyz = tex_tanspace_normal.xyz * 2  - 1;

    mat3 tbn = mat3(f_tangent.x, f_bitangent.x, frag_normal.x,
                    f_tangent.y, f_bitangent.y, frag_normal.y,
                    f_tangent.z, f_bitangent.z, frag_normal.z);
    frag_normal = tex_tanspace_normal.xyz * tbn;

    output_normal = vec4(frag_normal.xyz * 0.5 + 0.5, 0);

    if(frag_color.a < alpha_cutoff) discard;
    output_color = frag_color;
}



